Requestor: Maura Barros
Content: Photon-induced production of two ALPs. One that decays into leptons and other long-lived that does not decay
Analysis team: Model created by request to this analysis https://atlas-glance.cern.ch/atlas/analysis/analyses/details.php?ref_code=ANA-EXOT-2023-13
Author: Mikael Chala
