Requestor: Yoav Afik
Contents: bsll contact interaction
Paper: https://link.springer.com/article/10.1007%2FJHEP08%282018%29056
Source: Jonathan Cohen (private communication)
JIRA: https://its.cern.ch/jira/browse/AGENE-1631