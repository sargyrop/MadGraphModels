# This file was automatically created by FeynRules 2.3.49_fne
# Mathematica version: 13.1.0 for Mac OS X ARM (64-bit) (June 16, 2022)
# Date: Tue 28 Nov 2023 12:50:07


from object_library import all_vertices, Vertex
import particles as P
import couplings as C
import lorentz as L


V_1 = Vertex(name = 'V_1',
             particles = [ P.a, P.a, P.A02, P.A02 ],
             color = [ '1' ],
             lorentz = [ L.VVSS1 ],
             couplings = {(0,0):C.GC_308})

V_2 = Vertex(name = 'V_2',
             particles = [ P.a, P.a, P.A02, P.A02 ],
             color = [ '1' ],
             lorentz = [ L.VVSS1 ],
             couplings = {(0,0):C.GC_119})

V_3 = Vertex(name = 'V_3',
             particles = [ P.a, P.a, P.A02, P.A02 ],
             color = [ '1' ],
             lorentz = [ L.VVSS1 ],
             couplings = {(0,0):C.GC_222})

V_4 = Vertex(name = 'V_4',
             particles = [ P.a, P.a, P.H02, P.H02 ],
             color = [ '1' ],
             lorentz = [ L.VVSS1 ],
             couplings = {(0,0):C.GC_309})

V_5 = Vertex(name = 'V_5',
             particles = [ P.a, P.a, P.H02, P.H02 ],
             color = [ '1' ],
             lorentz = [ L.VVSS1 ],
             couplings = {(0,0):C.GC_120})

V_6 = Vertex(name = 'V_6',
             particles = [ P.a, P.a, P.H02, P.H02 ],
             color = [ '1' ],
             lorentz = [ L.VVSS1 ],
             couplings = {(0,0):C.GC_226})

V_7 = Vertex(name = 'V_7',
             particles = [ P.a, P.a, P.H03, P.H03 ],
             color = [ '1' ],
             lorentz = [ L.VVSS1 ],
             couplings = {(0,0):C.GC_308})

V_8 = Vertex(name = 'V_8',
             particles = [ P.a, P.a, P.H03, P.H03 ],
             color = [ '1' ],
             lorentz = [ L.VVSS1 ],
             couplings = {(0,0):C.GC_119})

V_9 = Vertex(name = 'V_9',
             particles = [ P.a, P.a, P.H03, P.H03 ],
             color = [ '1' ],
             lorentz = [ L.VVSS1 ],
             couplings = {(0,0):C.GC_222})

V_10 = Vertex(name = 'V_10',
              particles = [ P.a, P.a, P.HM1, P.HP1 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_308})

V_11 = Vertex(name = 'V_11',
              particles = [ P.a, P.a, P.HL__minus____minus__, P.HL__plus____plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_308})

V_12 = Vertex(name = 'V_12',
              particles = [ P.a, P.a, P.HL__minus____minus__, P.HL__plus____plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_119})

V_13 = Vertex(name = 'V_13',
              particles = [ P.a, P.a, P.HL__minus____minus__, P.HL__plus____plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_223})

V_14 = Vertex(name = 'V_14',
              particles = [ P.a, P.a, P.HR__minus____minus__, P.HR__plus____plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_308})

V_15 = Vertex(name = 'V_15',
              particles = [ P.a, P.a, P.HR__minus____minus__, P.HR__plus____plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_119})

V_16 = Vertex(name = 'V_16',
              particles = [ P.a, P.a, P.HR__minus____minus__, P.HR__plus____plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_223})

V_17 = Vertex(name = 'V_17',
              particles = [ P.a, P.a, P.H, P.H02 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_345})

V_18 = Vertex(name = 'V_18',
              particles = [ P.a, P.a, P.H, P.H02 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_131})

V_19 = Vertex(name = 'V_19',
              particles = [ P.a, P.a, P.H, P.H02 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_255})

V_20 = Vertex(name = 'V_20',
              particles = [ P.a, P.a, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_348})

V_21 = Vertex(name = 'V_21',
              particles = [ P.a, P.a, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_134})

V_22 = Vertex(name = 'V_22',
              particles = [ P.a, P.a, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_262})

V_23 = Vertex(name = 'V_23',
              particles = [ P.a, P.a, P.HM2, P.HP2 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_559})

V_24 = Vertex(name = 'V_24',
              particles = [ P.a, P.a, P.H03 ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_789})

V_25 = Vertex(name = 'V_25',
              particles = [ P.a, P.a, P.H03 ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_760})

V_26 = Vertex(name = 'V_26',
              particles = [ P.a, P.a, P.H03 ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_766})

V_27 = Vertex(name = 'V_27',
              particles = [ P.a, P.a, P.H02 ],
              color = [ '1' ],
              lorentz = [ L.VVS1, L.VVS2 ],
              couplings = {(0,1):C.GC_71,(0,0):C.GC_847})

V_28 = Vertex(name = 'V_28',
              particles = [ P.a, P.a, P.H02 ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_800})

V_29 = Vertex(name = 'V_29',
              particles = [ P.a, P.a, P.H02 ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_814})

V_30 = Vertex(name = 'V_30',
              particles = [ P.a, P.a, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1, L.VVS2 ],
              couplings = {(0,1):C.GC_1,(0,0):C.GC_852})

V_31 = Vertex(name = 'V_31',
              particles = [ P.a, P.a, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_803})

V_32 = Vertex(name = 'V_32',
              particles = [ P.a, P.a, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_821})

V_33 = Vertex(name = 'V_33',
              particles = [ P.a, P.A02, P.H03 ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_80})

V_34 = Vertex(name = 'V_34',
              particles = [ P.a, P.A02, P.H03 ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_180})

V_35 = Vertex(name = 'V_35',
              particles = [ P.a, P.HM1, P.HP1 ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_179})

V_36 = Vertex(name = 'V_36',
              particles = [ P.a, P.HM2, P.HP2 ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_551})

V_37 = Vertex(name = 'V_37',
              particles = [ P.a, P.HL__minus____minus__, P.HL__plus____plus__ ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_83})

V_38 = Vertex(name = 'V_38',
              particles = [ P.a, P.HL__minus____minus__, P.HL__plus____plus__ ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_179})

V_39 = Vertex(name = 'V_39',
              particles = [ P.a, P.HR__minus____minus__, P.HR__plus____plus__ ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_83})

V_40 = Vertex(name = 'V_40',
              particles = [ P.a, P.HR__minus____minus__, P.HR__plus____plus__ ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_179})

V_41 = Vertex(name = 'V_41',
              particles = [ P.g, P.g, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVS2 ],
              couplings = {(0,0):C.GC_2})

V_42 = Vertex(name = 'V_42',
              particles = [ P.g, P.g, P.H02 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVS2 ],
              couplings = {(0,0):C.GC_72})

V_43 = Vertex(name = 'V_43',
              particles = [ P.g, P.g, P.g ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVV1 ],
              couplings = {(0,0):C.GC_3})

V_44 = Vertex(name = 'V_44',
              particles = [ P.g, P.g, P.g, P.g ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVV1, L.VVVV3, L.VVVV4 ],
              couplings = {(1,1):C.GC_6,(0,0):C.GC_6,(2,2):C.GC_6})

V_45 = Vertex(name = 'V_45',
              particles = [ P.g, P.g, P.g, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVS1 ],
              couplings = {(0,0):C.GC_5})

V_46 = Vertex(name = 'V_46',
              particles = [ P.g, P.g, P.g, P.H02 ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVS1 ],
              couplings = {(0,0):C.GC_73})

V_47 = Vertex(name = 'V_47',
              particles = [ P.g, P.g, P.g, P.g, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
              couplings = {(1,1):C.GC_7,(0,0):C.GC_7,(2,2):C.GC_7})

V_48 = Vertex(name = 'V_48',
              particles = [ P.g, P.g, P.g, P.g, P.H02 ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
              couplings = {(1,1):C.GC_74,(0,0):C.GC_74,(2,2):C.GC_74})

V_49 = Vertex(name = 'V_49',
              particles = [ P.b__tilde__, P.c, P.HM2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_923,(0,1):C.GC_926})

V_50 = Vertex(name = 'V_50',
              particles = [ P.d__tilde__, P.c, P.HM2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_921,(0,1):C.GC_924})

V_51 = Vertex(name = 'V_51',
              particles = [ P.s__tilde__, P.c, P.HM2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_922,(0,1):C.GC_925})

V_52 = Vertex(name = 'V_52',
              particles = [ P.b__tilde__, P.t, P.HM2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_933,(0,1):C.GC_936})

V_53 = Vertex(name = 'V_53',
              particles = [ P.d__tilde__, P.t, P.HM2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_931,(0,1):C.GC_934})

V_54 = Vertex(name = 'V_54',
              particles = [ P.s__tilde__, P.t, P.HM2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_932,(0,1):C.GC_935})

V_55 = Vertex(name = 'V_55',
              particles = [ P.b__tilde__, P.u, P.HM2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_913,(0,1):C.GC_916})

V_56 = Vertex(name = 'V_56',
              particles = [ P.d__tilde__, P.u, P.HM2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_911,(0,1):C.GC_914})

V_57 = Vertex(name = 'V_57',
              particles = [ P.s__tilde__, P.u, P.HM2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_912,(0,1):C.GC_915})

V_58 = Vertex(name = 'V_58',
              particles = [ P.b__tilde__, P.b, P.A01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS2, L.FFS3 ],
              couplings = {(0,1):C.GC_899,(0,0):C.GC_1175,(0,2):C.GC_1104})

V_59 = Vertex(name = 'V_59',
              particles = [ P.d__tilde__, P.d, P.A01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS2, L.FFS3 ],
              couplings = {(0,1):C.GC_891,(0,0):C.GC_1145,(0,2):C.GC_1078})

V_60 = Vertex(name = 'V_60',
              particles = [ P.s__tilde__, P.s, P.A01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS2, L.FFS3 ],
              couplings = {(0,1):C.GC_895,(0,0):C.GC_1157,(0,2):C.GC_1088})

V_61 = Vertex(name = 'V_61',
              particles = [ P.b__tilde__, P.b, P.H01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3, L.FFS4 ],
              couplings = {(0,2):C.GC_898,(0,0):C.GC_1176,(0,1):C.GC_1103})

V_62 = Vertex(name = 'V_62',
              particles = [ P.d__tilde__, P.d, P.H01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3, L.FFS4 ],
              couplings = {(0,2):C.GC_890,(0,0):C.GC_1146,(0,1):C.GC_1077})

V_63 = Vertex(name = 'V_63',
              particles = [ P.s__tilde__, P.s, P.H01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3, L.FFS4 ],
              couplings = {(0,2):C.GC_894,(0,0):C.GC_1158,(0,1):C.GC_1087})

V_64 = Vertex(name = 'V_64',
              particles = [ P.b__tilde__, P.b, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS4 ],
              couplings = {(0,0):C.GC_897})

V_65 = Vertex(name = 'V_65',
              particles = [ P.d__tilde__, P.d, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS4 ],
              couplings = {(0,0):C.GC_889})

V_66 = Vertex(name = 'V_66',
              particles = [ P.s__tilde__, P.s, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS4 ],
              couplings = {(0,0):C.GC_893})

V_67 = Vertex(name = 'V_67',
              particles = [ P.b__tilde__, P.b, P.H02 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS4 ],
              couplings = {(0,0):C.GC_900})

V_68 = Vertex(name = 'V_68',
              particles = [ P.d__tilde__, P.d, P.H02 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS4 ],
              couplings = {(0,0):C.GC_892})

V_69 = Vertex(name = 'V_69',
              particles = [ P.s__tilde__, P.s, P.H02 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS4 ],
              couplings = {(0,0):C.GC_896})

V_70 = Vertex(name = 'V_70',
              particles = [ P.c__tilde__, P.c, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS4 ],
              couplings = {(0,0):C.GC_917})

V_71 = Vertex(name = 'V_71',
              particles = [ P.t__tilde__, P.t, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS4 ],
              couplings = {(0,0):C.GC_927})

V_72 = Vertex(name = 'V_72',
              particles = [ P.u__tilde__, P.u, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS4 ],
              couplings = {(0,0):C.GC_907})

V_73 = Vertex(name = 'V_73',
              particles = [ P.c__tilde__, P.c, P.A01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS2, L.FFS3 ],
              couplings = {(0,1):C.GC_918,(0,0):C.GC_1072,(0,2):C.GC_1136})

V_74 = Vertex(name = 'V_74',
              particles = [ P.t__tilde__, P.c, P.A01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1096,(0,1):C.GC_1166})

V_75 = Vertex(name = 'V_75',
              particles = [ P.u__tilde__, P.c, P.A01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1060,(0,1):C.GC_1118})

V_76 = Vertex(name = 'V_76',
              particles = [ P.c__tilde__, P.t, P.A01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1074,(0,1):C.GC_1138})

V_77 = Vertex(name = 'V_77',
              particles = [ P.t__tilde__, P.t, P.A01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS2, L.FFS3 ],
              couplings = {(0,1):C.GC_928,(0,0):C.GC_1098,(0,2):C.GC_1168})

V_78 = Vertex(name = 'V_78',
              particles = [ P.u__tilde__, P.t, P.A01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1062,(0,1):C.GC_1120})

V_79 = Vertex(name = 'V_79',
              particles = [ P.c__tilde__, P.u, P.A01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1070,(0,1):C.GC_1134})

V_80 = Vertex(name = 'V_80',
              particles = [ P.t__tilde__, P.u, P.A01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1094,(0,1):C.GC_1164})

V_81 = Vertex(name = 'V_81',
              particles = [ P.u__tilde__, P.u, P.A01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS2, L.FFS3 ],
              couplings = {(0,1):C.GC_908,(0,0):C.GC_1058,(0,2):C.GC_1116})

V_82 = Vertex(name = 'V_82',
              particles = [ P.c__tilde__, P.c, P.H01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3, L.FFS4 ],
              couplings = {(0,2):C.GC_919,(0,0):C.GC_1071,(0,1):C.GC_1137})

V_83 = Vertex(name = 'V_83',
              particles = [ P.t__tilde__, P.c, P.H01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1095,(0,1):C.GC_1167})

V_84 = Vertex(name = 'V_84',
              particles = [ P.u__tilde__, P.c, P.H01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1059,(0,1):C.GC_1119})

V_85 = Vertex(name = 'V_85',
              particles = [ P.c__tilde__, P.t, P.H01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1073,(0,1):C.GC_1139})

V_86 = Vertex(name = 'V_86',
              particles = [ P.t__tilde__, P.t, P.H01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3, L.FFS4 ],
              couplings = {(0,2):C.GC_929,(0,0):C.GC_1097,(0,1):C.GC_1169})

V_87 = Vertex(name = 'V_87',
              particles = [ P.u__tilde__, P.t, P.H01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1061,(0,1):C.GC_1121})

V_88 = Vertex(name = 'V_88',
              particles = [ P.c__tilde__, P.u, P.H01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1069,(0,1):C.GC_1135})

V_89 = Vertex(name = 'V_89',
              particles = [ P.t__tilde__, P.u, P.H01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1093,(0,1):C.GC_1165})

V_90 = Vertex(name = 'V_90',
              particles = [ P.u__tilde__, P.u, P.H01 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3, L.FFS4 ],
              couplings = {(0,2):C.GC_909,(0,0):C.GC_1057,(0,1):C.GC_1117})

V_91 = Vertex(name = 'V_91',
              particles = [ P.c__tilde__, P.c, P.H02 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS4 ],
              couplings = {(0,0):C.GC_920})

V_92 = Vertex(name = 'V_92',
              particles = [ P.t__tilde__, P.t, P.H02 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_930,(0,1):C.GC_930})

V_93 = Vertex(name = 'V_93',
              particles = [ P.u__tilde__, P.u, P.H02 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_910,(0,1):C.GC_910})

V_94 = Vertex(name = 'V_94',
              particles = [ P.c__tilde__, P.b, P.HP2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1140,(0,1):C.GC_1133})

V_95 = Vertex(name = 'V_95',
              particles = [ P.t__tilde__, P.b, P.HP2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1170,(0,1):C.GC_1163})

V_96 = Vertex(name = 'V_96',
              particles = [ P.u__tilde__, P.b, P.HP2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1122,(0,1):C.GC_1115})

V_97 = Vertex(name = 'V_97',
              particles = [ P.c__tilde__, P.d, P.HP2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1126,(0,1):C.GC_1125})

V_98 = Vertex(name = 'V_98',
              particles = [ P.t__tilde__, P.d, P.HP2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1144,(0,1):C.GC_1143})

V_99 = Vertex(name = 'V_99',
              particles = [ P.u__tilde__, P.d, P.HP2 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS3 ],
              couplings = {(0,0):C.GC_1108,(0,1):C.GC_1107})

V_100 = Vertex(name = 'V_100',
               particles = [ P.c__tilde__, P.s, P.HP2 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1130,(0,1):C.GC_1129})

V_101 = Vertex(name = 'V_101',
               particles = [ P.t__tilde__, P.s, P.HP2 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1154,(0,1):C.GC_1153})

V_102 = Vertex(name = 'V_102',
               particles = [ P.u__tilde__, P.s, P.HP2 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1112,(0,1):C.GC_1111})

V_103 = Vertex(name = 'V_103',
               particles = [ P.v1, P.v1, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_943,(0,1):C.GC_943})

V_104 = Vertex(name = 'V_104',
               particles = [ P.v1, P.v1, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_963,(0,1):C.GC_963})

V_105 = Vertex(name = 'V_105',
               particles = [ P.v2, P.v2, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_981,(0,1):C.GC_981})

V_106 = Vertex(name = 'V_106',
               particles = [ P.v2, P.v2, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1001,(0,1):C.GC_1001})

V_107 = Vertex(name = 'V_107',
               particles = [ P.v3, P.v3, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1019,(0,1):C.GC_1019})

V_108 = Vertex(name = 'V_108',
               particles = [ P.v3, P.v3, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1039,(0,1):C.GC_1039})

V_109 = Vertex(name = 'V_109',
               particles = [ P.N1, P.N1, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_952,(0,1):C.GC_952})

V_110 = Vertex(name = 'V_110',
               particles = [ P.N1, P.N1, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_965,(0,1):C.GC_965})

V_111 = Vertex(name = 'V_111',
               particles = [ P.v1, P.N1, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_948,(0,1):C.GC_948})

V_112 = Vertex(name = 'V_112',
               particles = [ P.v1, P.N1, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_964,(0,1):C.GC_964})

V_113 = Vertex(name = 'V_113',
               particles = [ P.N2, P.N2, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_990,(0,1):C.GC_990})

V_114 = Vertex(name = 'V_114',
               particles = [ P.N2, P.N2, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1003,(0,1):C.GC_1003})

V_115 = Vertex(name = 'V_115',
               particles = [ P.v2, P.N2, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_986,(0,1):C.GC_986})

V_116 = Vertex(name = 'V_116',
               particles = [ P.v2, P.N2, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1002,(0,1):C.GC_1002})

V_117 = Vertex(name = 'V_117',
               particles = [ P.N3, P.N3, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1028,(0,1):C.GC_1028})

V_118 = Vertex(name = 'V_118',
               particles = [ P.N3, P.N3, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1041,(0,1):C.GC_1041})

V_119 = Vertex(name = 'V_119',
               particles = [ P.v3, P.N3, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1024,(0,1):C.GC_1024})

V_120 = Vertex(name = 'V_120',
               particles = [ P.v3, P.N3, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1040,(0,1):C.GC_1040})

V_121 = Vertex(name = 'V_121',
               particles = [ P.v1, P.v1, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_944,(0,1):C.GC_946})

V_122 = Vertex(name = 'V_122',
               particles = [ P.v2, P.v2, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_982,(0,1):C.GC_984})

V_123 = Vertex(name = 'V_123',
               particles = [ P.v3, P.v3, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1020,(0,1):C.GC_1022})

V_124 = Vertex(name = 'V_124',
               particles = [ P.N1, P.N1, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_953,(0,1):C.GC_955})

V_125 = Vertex(name = 'V_125',
               particles = [ P.v1, P.N1, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_949,(0,1):C.GC_951})

V_126 = Vertex(name = 'V_126',
               particles = [ P.N2, P.N2, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_991,(0,1):C.GC_993})

V_127 = Vertex(name = 'V_127',
               particles = [ P.v2, P.N2, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_987,(0,1):C.GC_989})

V_128 = Vertex(name = 'V_128',
               particles = [ P.N3, P.N3, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1029,(0,1):C.GC_1031})

V_129 = Vertex(name = 'V_129',
               particles = [ P.v3, P.N3, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1025,(0,1):C.GC_1027})

V_130 = Vertex(name = 'V_130',
               particles = [ P.v1, P.v1, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_945,(0,1):C.GC_945})

V_131 = Vertex(name = 'V_131',
               particles = [ P.v2, P.v2, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_983,(0,1):C.GC_983})

V_132 = Vertex(name = 'V_132',
               particles = [ P.v3, P.v3, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1021,(0,1):C.GC_1021})

V_133 = Vertex(name = 'V_133',
               particles = [ P.N1, P.N1, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_954,(0,1):C.GC_954})

V_134 = Vertex(name = 'V_134',
               particles = [ P.v1, P.N1, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_950,(0,1):C.GC_950})

V_135 = Vertex(name = 'V_135',
               particles = [ P.N2, P.N2, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_992,(0,1):C.GC_992})

V_136 = Vertex(name = 'V_136',
               particles = [ P.v2, P.N2, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_988,(0,1):C.GC_988})

V_137 = Vertex(name = 'V_137',
               particles = [ P.N3, P.N3, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1030,(0,1):C.GC_1030})

V_138 = Vertex(name = 'V_138',
               particles = [ P.v3, P.N3, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1026,(0,1):C.GC_1026})

V_139 = Vertex(name = 'V_139',
               particles = [ P.v1, P.v1, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_956,(0,1):C.GC_956})

V_140 = Vertex(name = 'V_140',
               particles = [ P.v1, P.v1, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_960,(0,1):C.GC_960})

V_141 = Vertex(name = 'V_141',
               particles = [ P.v2, P.v2, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_994,(0,1):C.GC_994})

V_142 = Vertex(name = 'V_142',
               particles = [ P.v2, P.v2, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_998,(0,1):C.GC_998})

V_143 = Vertex(name = 'V_143',
               particles = [ P.v3, P.v3, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1032,(0,1):C.GC_1032})

V_144 = Vertex(name = 'V_144',
               particles = [ P.v3, P.v3, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1036,(0,1):C.GC_1036})

V_145 = Vertex(name = 'V_145',
               particles = [ P.N1, P.N1, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_958,(0,1):C.GC_958})

V_146 = Vertex(name = 'V_146',
               particles = [ P.N1, P.N1, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_962,(0,1):C.GC_962})

V_147 = Vertex(name = 'V_147',
               particles = [ P.v1, P.N1, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_957,(0,1):C.GC_957})

V_148 = Vertex(name = 'V_148',
               particles = [ P.v1, P.N1, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_961,(0,1):C.GC_961})

V_149 = Vertex(name = 'V_149',
               particles = [ P.N2, P.N2, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_996,(0,1):C.GC_996})

V_150 = Vertex(name = 'V_150',
               particles = [ P.N2, P.N2, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1000,(0,1):C.GC_1000})

V_151 = Vertex(name = 'V_151',
               particles = [ P.v2, P.N2, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_995,(0,1):C.GC_995})

V_152 = Vertex(name = 'V_152',
               particles = [ P.v2, P.N2, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_999,(0,1):C.GC_999})

V_153 = Vertex(name = 'V_153',
               particles = [ P.N3, P.N3, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1034,(0,1):C.GC_1034})

V_154 = Vertex(name = 'V_154',
               particles = [ P.N3, P.N3, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1038,(0,1):C.GC_1038})

V_155 = Vertex(name = 'V_155',
               particles = [ P.v3, P.N3, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1033,(0,1):C.GC_1033})

V_156 = Vertex(name = 'V_156',
               particles = [ P.v3, P.N3, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1037,(0,1):C.GC_1037})

V_157 = Vertex(name = 'V_157',
               particles = [ P.N1, P.e__minus__, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_941,(0,1):C.GC_947})

V_158 = Vertex(name = 'V_158',
               particles = [ P.v1, P.e__minus__, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_940,(0,1):C.GC_942})

V_159 = Vertex(name = 'V_159',
               particles = [ P.N2, P.mu__minus__, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_979,(0,1):C.GC_985})

V_160 = Vertex(name = 'V_160',
               particles = [ P.v2, P.mu__minus__, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_978,(0,1):C.GC_980})

V_161 = Vertex(name = 'V_161',
               particles = [ P.N3, P.ta__minus__, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1017,(0,1):C.GC_1023})

V_162 = Vertex(name = 'V_162',
               particles = [ P.v3, P.ta__minus__, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1016,(0,1):C.GC_1018})

V_163 = Vertex(name = 'V_163',
               particles = [ P.e__plus__, P.v1, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_942,(0,1):C.GC_940})

V_164 = Vertex(name = 'V_164',
               particles = [ P.mu__plus__, P.v2, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_980,(0,1):C.GC_978})

V_165 = Vertex(name = 'V_165',
               particles = [ P.ta__plus__, P.v3, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1018,(0,1):C.GC_1016})

V_166 = Vertex(name = 'V_166',
               particles = [ P.e__plus__, P.N1, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_947,(0,1):C.GC_941})

V_167 = Vertex(name = 'V_167',
               particles = [ P.mu__plus__, P.N2, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_985,(0,1):C.GC_979})

V_168 = Vertex(name = 'V_168',
               particles = [ P.ta__plus__, P.N3, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1023,(0,1):C.GC_1017})

V_169 = Vertex(name = 'V_169',
               particles = [ P.e__plus__, P.e__minus__, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_937,(0,1):C.GC_939})

V_170 = Vertex(name = 'V_170',
               particles = [ P.mu__plus__, P.mu__minus__, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_975,(0,1):C.GC_977})

V_171 = Vertex(name = 'V_171',
               particles = [ P.ta__plus__, P.ta__minus__, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1013,(0,1):C.GC_1015})

V_172 = Vertex(name = 'V_172',
               particles = [ P.e__plus__, P.e__minus__, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_938,(0,1):C.GC_938})

V_173 = Vertex(name = 'V_173',
               particles = [ P.mu__plus__, P.mu__minus__, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_976,(0,1):C.GC_976})

V_174 = Vertex(name = 'V_174',
               particles = [ P.ta__plus__, P.ta__minus__, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1014,(0,1):C.GC_1014})

V_175 = Vertex(name = 'V_175',
               particles = [ P.e__plus__, P.e__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_901,(0,1):C.GC_901})

V_176 = Vertex(name = 'V_176',
               particles = [ P.mu__plus__, P.mu__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_903,(0,1):C.GC_903})

V_177 = Vertex(name = 'V_177',
               particles = [ P.ta__plus__, P.ta__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_905,(0,1):C.GC_905})

V_178 = Vertex(name = 'V_178',
               particles = [ P.e__plus__, P.e__minus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS4 ],
               couplings = {(0,0):C.GC_902})

V_179 = Vertex(name = 'V_179',
               particles = [ P.mu__plus__, P.mu__minus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS4 ],
               couplings = {(0,0):C.GC_904})

V_180 = Vertex(name = 'V_180',
               particles = [ P.ta__plus__, P.ta__minus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.FFS4 ],
               couplings = {(0,0):C.GC_906})

V_181 = Vertex(name = 'V_181',
               particles = [ P.d__tilde__, P.b, P.A01 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1171,(0,1):C.GC_1100})

V_182 = Vertex(name = 'V_182',
               particles = [ P.s__tilde__, P.b, P.A01 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1173,(0,1):C.GC_1102})

V_183 = Vertex(name = 'V_183',
               particles = [ P.b__tilde__, P.d, P.A01 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1149,(0,1):C.GC_1082})

V_184 = Vertex(name = 'V_184',
               particles = [ P.s__tilde__, P.d, P.A01 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1147,(0,1):C.GC_1080})

V_185 = Vertex(name = 'V_185',
               particles = [ P.b__tilde__, P.s, P.A01 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1159,(0,1):C.GC_1090})

V_186 = Vertex(name = 'V_186',
               particles = [ P.d__tilde__, P.s, P.A01 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1155,(0,1):C.GC_1086})

V_187 = Vertex(name = 'V_187',
               particles = [ P.d__tilde__, P.b, P.H01 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1172,(0,1):C.GC_1099})

V_188 = Vertex(name = 'V_188',
               particles = [ P.s__tilde__, P.b, P.H01 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1174,(0,1):C.GC_1101})

V_189 = Vertex(name = 'V_189',
               particles = [ P.b__tilde__, P.d, P.H01 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1150,(0,1):C.GC_1081})

V_190 = Vertex(name = 'V_190',
               particles = [ P.s__tilde__, P.d, P.H01 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1148,(0,1):C.GC_1079})

V_191 = Vertex(name = 'V_191',
               particles = [ P.b__tilde__, P.s, P.H01 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1160,(0,1):C.GC_1089})

V_192 = Vertex(name = 'V_192',
               particles = [ P.d__tilde__, P.s, P.H01 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS3 ],
               couplings = {(0,0):C.GC_1156,(0,1):C.GC_1085})

V_193 = Vertex(name = 'V_193',
               particles = [ P.e__minus__, P.e__minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_959})

V_194 = Vertex(name = 'V_194',
               particles = [ P.mu__minus__, P.mu__minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_997})

V_195 = Vertex(name = 'V_195',
               particles = [ P.ta__minus__, P.ta__minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1035})

V_196 = Vertex(name = 'V_196',
               particles = [ P.v1, P.v1, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_969})

V_197 = Vertex(name = 'V_197',
               particles = [ P.v2, P.v2, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_1007})

V_198 = Vertex(name = 'V_198',
               particles = [ P.v3, P.v3, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_1045})

V_199 = Vertex(name = 'V_199',
               particles = [ P.v1, P.N1, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_972})

V_200 = Vertex(name = 'V_200',
               particles = [ P.N1, P.N1, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_974})

V_201 = Vertex(name = 'V_201',
               particles = [ P.v2, P.N2, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_1010})

V_202 = Vertex(name = 'V_202',
               particles = [ P.N2, P.N2, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_1012})

V_203 = Vertex(name = 'V_203',
               particles = [ P.v3, P.N3, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_1048})

V_204 = Vertex(name = 'V_204',
               particles = [ P.N3, P.N3, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_1050})

V_205 = Vertex(name = 'V_205',
               particles = [ P.v1, P.v1, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.FFS4 ],
               couplings = {(0,0):C.GC_968})

V_206 = Vertex(name = 'V_206',
               particles = [ P.v2, P.v2, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.FFS4 ],
               couplings = {(0,0):C.GC_1006})

V_207 = Vertex(name = 'V_207',
               particles = [ P.v3, P.v3, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.FFS4 ],
               couplings = {(0,0):C.GC_1044})

V_208 = Vertex(name = 'V_208',
               particles = [ P.v1, P.N1, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.FFS4 ],
               couplings = {(0,0):C.GC_971})

V_209 = Vertex(name = 'V_209',
               particles = [ P.N1, P.N1, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.FFS4 ],
               couplings = {(0,0):C.GC_973})

V_210 = Vertex(name = 'V_210',
               particles = [ P.v2, P.N2, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.FFS4 ],
               couplings = {(0,0):C.GC_1009})

V_211 = Vertex(name = 'V_211',
               particles = [ P.N2, P.N2, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.FFS4 ],
               couplings = {(0,0):C.GC_1011})

V_212 = Vertex(name = 'V_212',
               particles = [ P.v3, P.N3, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.FFS4 ],
               couplings = {(0,0):C.GC_1047})

V_213 = Vertex(name = 'V_213',
               particles = [ P.N3, P.N3, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.FFS4 ],
               couplings = {(0,0):C.GC_1049})

V_214 = Vertex(name = 'V_214',
               particles = [ P.v1, P.e__minus__, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.FFS3 ],
               couplings = {(0,0):C.GC_967})

V_215 = Vertex(name = 'V_215',
               particles = [ P.v2, P.mu__minus__, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.FFS3 ],
               couplings = {(0,0):C.GC_1005})

V_216 = Vertex(name = 'V_216',
               particles = [ P.v3, P.ta__minus__, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.FFS3 ],
               couplings = {(0,0):C.GC_1043})

V_217 = Vertex(name = 'V_217',
               particles = [ P.N1, P.e__minus__, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.FFS3 ],
               couplings = {(0,0):C.GC_970})

V_218 = Vertex(name = 'V_218',
               particles = [ P.N2, P.mu__minus__, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.FFS3 ],
               couplings = {(0,0):C.GC_1008})

V_219 = Vertex(name = 'V_219',
               particles = [ P.N3, P.ta__minus__, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.FFS3 ],
               couplings = {(0,0):C.GC_1046})

V_220 = Vertex(name = 'V_220',
               particles = [ P.e__minus__, P.e__minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS3 ],
               couplings = {(0,0):C.GC_966})

V_221 = Vertex(name = 'V_221',
               particles = [ P.mu__minus__, P.mu__minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS3 ],
               couplings = {(0,0):C.GC_1004})

V_222 = Vertex(name = 'V_222',
               particles = [ P.ta__minus__, P.ta__minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS3 ],
               couplings = {(0,0):C.GC_1042})

V_223 = Vertex(name = 'V_223',
               particles = [ P.a, P.W__minus__, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_95})

V_224 = Vertex(name = 'V_224',
               particles = [ P.a, P.W__minus__, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_186})

V_225 = Vertex(name = 'V_225',
               particles = [ P.a, P.W__minus__, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_94})

V_226 = Vertex(name = 'V_226',
               particles = [ P.a, P.W__minus__, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_187})

V_227 = Vertex(name = 'V_227',
               particles = [ P.a, P.W__minus__, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_92})

V_228 = Vertex(name = 'V_228',
               particles = [ P.a, P.W__minus__, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_185})

V_229 = Vertex(name = 'V_229',
               particles = [ P.a, P.W__minus__, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_552})

V_230 = Vertex(name = 'V_230',
               particles = [ P.a, P.W__minus__, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_727})

V_231 = Vertex(name = 'V_231',
               particles = [ P.a, P.W__minus__, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_553})

V_232 = Vertex(name = 'V_232',
               particles = [ P.a, P.W__minus__, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_728})

V_233 = Vertex(name = 'V_233',
               particles = [ P.a, P.W__minus__, P.H, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_643})

V_234 = Vertex(name = 'V_234',
               particles = [ P.a, P.W__minus__, P.H02, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_644})

V_235 = Vertex(name = 'V_235',
               particles = [ P.a, P.W__minus__, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_688})

V_236 = Vertex(name = 'V_236',
               particles = [ P.a, P.W__minus__, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_752})

V_237 = Vertex(name = 'V_237',
               particles = [ P.a, P.W__minus__, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_764})

V_238 = Vertex(name = 'V_238',
               particles = [ P.W__minus__, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_529})

V_239 = Vertex(name = 'V_239',
               particles = [ P.W__minus__, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_713})

V_240 = Vertex(name = 'V_240',
               particles = [ P.W__minus__, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_15})

V_241 = Vertex(name = 'V_241',
               particles = [ P.W__minus__, P.H, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_629})

V_242 = Vertex(name = 'V_242',
               particles = [ P.W__minus__, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_527})

V_243 = Vertex(name = 'V_243',
               particles = [ P.W__minus__, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_712})

V_244 = Vertex(name = 'V_244',
               particles = [ P.W__minus__, P.H02, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_635})

V_245 = Vertex(name = 'V_245',
               particles = [ P.W__minus__, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_13})

V_246 = Vertex(name = 'V_246',
               particles = [ P.W__minus__, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_12})

V_247 = Vertex(name = 'V_247',
               particles = [ P.a, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_84})

V_248 = Vertex(name = 'V_248',
               particles = [ P.a, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_493})

V_249 = Vertex(name = 'V_249',
               particles = [ P.W__minus__, P.W__minus__, P.A02, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_44})

V_250 = Vertex(name = 'V_250',
               particles = [ P.W__minus__, P.W__minus__, P.H03, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_43})

V_251 = Vertex(name = 'V_251',
               particles = [ P.W__minus__, P.W__minus__, P.H02, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_487})

V_252 = Vertex(name = 'V_252',
               particles = [ P.W__minus__, P.W__minus__, P.H, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_492})

V_253 = Vertex(name = 'V_253',
               particles = [ P.W__minus__, P.W__minus__, P.HP2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_717})

V_254 = Vertex(name = 'V_254',
               particles = [ P.W__minus__, P.W__minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_745})

V_255 = Vertex(name = 'V_255',
               particles = [ P.W__minus__, P.W__minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_846})

V_256 = Vertex(name = 'V_256',
               particles = [ P.a, P.W2__minus__, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_456})

V_257 = Vertex(name = 'V_257',
               particles = [ P.a, P.W2__minus__, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_463})

V_258 = Vertex(name = 'V_258',
               particles = [ P.a, P.W2__minus__, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_455})

V_259 = Vertex(name = 'V_259',
               particles = [ P.a, P.W2__minus__, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_464})

V_260 = Vertex(name = 'V_260',
               particles = [ P.a, P.W2__minus__, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_453})

V_261 = Vertex(name = 'V_261',
               particles = [ P.a, P.W2__minus__, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_462})

V_262 = Vertex(name = 'V_262',
               particles = [ P.a, P.W2__minus__, P.H, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_555})

V_263 = Vertex(name = 'V_263',
               particles = [ P.a, P.W2__minus__, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_710})

V_264 = Vertex(name = 'V_264',
               particles = [ P.a, P.W2__minus__, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_640})

V_265 = Vertex(name = 'V_265',
               particles = [ P.a, P.W2__minus__, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_709})

V_266 = Vertex(name = 'V_266',
               particles = [ P.a, P.W2__minus__, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_641})

V_267 = Vertex(name = 'V_267',
               particles = [ P.a, P.W2__minus__, P.H02, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_556})

V_268 = Vertex(name = 'V_268',
               particles = [ P.a, P.W2__minus__, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_677})

V_269 = Vertex(name = 'V_269',
               particles = [ P.a, P.W2__minus__, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_782})

V_270 = Vertex(name = 'V_270',
               particles = [ P.a, P.W2__minus__, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_784})

V_271 = Vertex(name = 'V_271',
               particles = [ P.W2__minus__, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_694})

V_272 = Vertex(name = 'V_272',
               particles = [ P.W2__minus__, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_627})

V_273 = Vertex(name = 'V_273',
               particles = [ P.W2__minus__, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_397})

V_274 = Vertex(name = 'V_274',
               particles = [ P.W2__minus__, P.H, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_530})

V_275 = Vertex(name = 'V_275',
               particles = [ P.W2__minus__, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_695})

V_276 = Vertex(name = 'V_276',
               particles = [ P.W2__minus__, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_625})

V_277 = Vertex(name = 'V_277',
               particles = [ P.W2__minus__, P.H02, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_540})

V_278 = Vertex(name = 'V_278',
               particles = [ P.W2__minus__, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_395})

V_279 = Vertex(name = 'V_279',
               particles = [ P.W2__minus__, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_394})

V_280 = Vertex(name = 'V_280',
               particles = [ P.a, P.W2__minus__, P.W2__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_84})

V_281 = Vertex(name = 'V_281',
               particles = [ P.a, P.W2__minus__, P.W2__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_493})

V_282 = Vertex(name = 'V_282',
               particles = [ P.W__minus__, P.W2__minus__, P.A02, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_425})

V_283 = Vertex(name = 'V_283',
               particles = [ P.W__minus__, P.W2__minus__, P.H03, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_424})

V_284 = Vertex(name = 'V_284',
               particles = [ P.W__minus__, P.W2__minus__, P.H02, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_426})

V_285 = Vertex(name = 'V_285',
               particles = [ P.W__minus__, P.W2__minus__, P.H, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_446})

V_286 = Vertex(name = 'V_286',
               particles = [ P.W__minus__, P.W2__minus__, P.HP2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_703})

V_287 = Vertex(name = 'V_287',
               particles = [ P.W__minus__, P.W2__minus__, P.HP2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_733})

V_288 = Vertex(name = 'V_288',
               particles = [ P.W__minus__, P.W2__minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_779})

V_289 = Vertex(name = 'V_289',
               particles = [ P.W__minus__, P.W2__minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_845})

V_290 = Vertex(name = 'V_290',
               particles = [ P.W2__minus__, P.W2__minus__, P.H02, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_45})

V_291 = Vertex(name = 'V_291',
               particles = [ P.W2__minus__, P.W2__minus__, P.H, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_75})

V_292 = Vertex(name = 'V_292',
               particles = [ P.W2__minus__, P.W2__minus__, P.A02, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_486})

V_293 = Vertex(name = 'V_293',
               particles = [ P.W2__minus__, P.W2__minus__, P.H03, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_485})

V_294 = Vertex(name = 'V_294',
               particles = [ P.W2__minus__, P.W2__minus__, P.HP2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_716})

V_295 = Vertex(name = 'V_295',
               particles = [ P.W2__minus__, P.W2__minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_788})

V_296 = Vertex(name = 'V_296',
               particles = [ P.W2__minus__, P.W2__minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_796})

V_297 = Vertex(name = 'V_297',
               particles = [ P.a, P.W2__plus__, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_454})

V_298 = Vertex(name = 'V_298',
               particles = [ P.a, P.W2__plus__, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_465})

V_299 = Vertex(name = 'V_299',
               particles = [ P.a, P.W2__plus__, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_455})

V_300 = Vertex(name = 'V_300',
               particles = [ P.a, P.W2__plus__, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_464})

V_301 = Vertex(name = 'V_301',
               particles = [ P.a, P.W2__plus__, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_453})

V_302 = Vertex(name = 'V_302',
               particles = [ P.a, P.W2__plus__, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_462})

V_303 = Vertex(name = 'V_303',
               particles = [ P.a, P.W2__plus__, P.H, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_555})

V_304 = Vertex(name = 'V_304',
               particles = [ P.a, P.W2__plus__, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_708})

V_305 = Vertex(name = 'V_305',
               particles = [ P.a, P.W2__plus__, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_642})

V_306 = Vertex(name = 'V_306',
               particles = [ P.a, P.W2__plus__, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_709})

V_307 = Vertex(name = 'V_307',
               particles = [ P.a, P.W2__plus__, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_641})

V_308 = Vertex(name = 'V_308',
               particles = [ P.a, P.W2__plus__, P.H02, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_556})

V_309 = Vertex(name = 'V_309',
               particles = [ P.a, P.W2__plus__, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_677})

V_310 = Vertex(name = 'V_310',
               particles = [ P.a, P.W2__plus__, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_782})

V_311 = Vertex(name = 'V_311',
               particles = [ P.a, P.W2__plus__, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_784})

V_312 = Vertex(name = 'V_312',
               particles = [ P.W2__plus__, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_694})

V_313 = Vertex(name = 'V_313',
               particles = [ P.W2__plus__, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_627})

V_314 = Vertex(name = 'V_314',
               particles = [ P.W2__plus__, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_397})

V_315 = Vertex(name = 'V_315',
               particles = [ P.W2__plus__, P.H, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_531})

V_316 = Vertex(name = 'V_316',
               particles = [ P.W2__plus__, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_696})

V_317 = Vertex(name = 'V_317',
               particles = [ P.W2__plus__, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_626})

V_318 = Vertex(name = 'V_318',
               particles = [ P.W2__plus__, P.H02, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_541})

V_319 = Vertex(name = 'V_319',
               particles = [ P.W2__plus__, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_396})

V_320 = Vertex(name = 'V_320',
               particles = [ P.W2__plus__, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_393})

V_321 = Vertex(name = 'V_321',
               particles = [ P.W__minus__, P.W2__plus__, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_421})

V_322 = Vertex(name = 'V_322',
               particles = [ P.W__minus__, P.W2__plus__, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_427})

V_323 = Vertex(name = 'V_323',
               particles = [ P.W__minus__, P.W2__plus__, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_707})

V_324 = Vertex(name = 'V_324',
               particles = [ P.W__minus__, P.W2__plus__, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_737})

V_325 = Vertex(name = 'V_325',
               particles = [ P.W__minus__, P.W2__plus__, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_421})

V_326 = Vertex(name = 'V_326',
               particles = [ P.W__minus__, P.W2__plus__, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_422})

V_327 = Vertex(name = 'V_327',
               particles = [ P.W__minus__, P.W2__plus__, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_421})

V_328 = Vertex(name = 'V_328',
               particles = [ P.W__minus__, P.W2__plus__, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_420})

V_329 = Vertex(name = 'V_329',
               particles = [ P.W__minus__, P.W2__plus__, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_447})

V_330 = Vertex(name = 'V_330',
               particles = [ P.W__minus__, P.W2__plus__, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_706})

V_331 = Vertex(name = 'V_331',
               particles = [ P.W__minus__, P.W2__plus__, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_736})

V_332 = Vertex(name = 'V_332',
               particles = [ P.W__minus__, P.W2__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_448})

V_333 = Vertex(name = 'V_333',
               particles = [ P.W__minus__, P.W2__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_705})

V_334 = Vertex(name = 'V_334',
               particles = [ P.W__minus__, P.W2__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_735})

V_335 = Vertex(name = 'V_335',
               particles = [ P.W__minus__, P.W2__plus__, P.A01, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_534})

V_336 = Vertex(name = 'V_336',
               particles = [ P.W__minus__, P.W2__plus__, P.A01, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_658})

V_337 = Vertex(name = 'V_337',
               particles = [ P.W__minus__, P.W2__plus__, P.H, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_535})

V_338 = Vertex(name = 'V_338',
               particles = [ P.W__minus__, P.W2__plus__, P.H, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_659})

V_339 = Vertex(name = 'V_339',
               particles = [ P.W__minus__, P.W2__plus__, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_701})

V_340 = Vertex(name = 'V_340',
               particles = [ P.W__minus__, P.W2__plus__, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_732})

V_341 = Vertex(name = 'V_341',
               particles = [ P.W__minus__, P.W2__plus__, P.A01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_700})

V_342 = Vertex(name = 'V_342',
               particles = [ P.W__minus__, P.W2__plus__, P.A01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_730})

V_343 = Vertex(name = 'V_343',
               particles = [ P.W__minus__, P.W2__plus__, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_702})

V_344 = Vertex(name = 'V_344',
               particles = [ P.W__minus__, P.W2__plus__, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_731})

V_345 = Vertex(name = 'V_345',
               particles = [ P.W__minus__, P.W2__plus__, P.A01, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_543})

V_346 = Vertex(name = 'V_346',
               particles = [ P.W__minus__, P.W2__plus__, P.A01, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_663})

V_347 = Vertex(name = 'V_347',
               particles = [ P.W__minus__, P.W2__plus__, P.H01, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_544})

V_348 = Vertex(name = 'V_348',
               particles = [ P.W__minus__, P.W2__plus__, P.H01, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_664})

V_349 = Vertex(name = 'V_349',
               particles = [ P.W__minus__, P.W2__plus__, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_671})

V_350 = Vertex(name = 'V_350',
               particles = [ P.W__minus__, P.W2__plus__, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_689})

V_351 = Vertex(name = 'V_351',
               particles = [ P.W__minus__, P.W2__plus__, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_672})

V_352 = Vertex(name = 'V_352',
               particles = [ P.W__minus__, P.W2__plus__, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_690})

V_353 = Vertex(name = 'V_353',
               particles = [ P.W__minus__, P.W2__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_886})

V_354 = Vertex(name = 'V_354',
               particles = [ P.W__minus__, P.W2__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_740})

V_355 = Vertex(name = 'V_355',
               particles = [ P.W__minus__, P.W2__plus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_885})

V_356 = Vertex(name = 'V_356',
               particles = [ P.W__minus__, P.W2__plus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_741})

V_357 = Vertex(name = 'V_357',
               particles = [ P.W__minus__, P.W2__plus__, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_778})

V_358 = Vertex(name = 'V_358',
               particles = [ P.W__minus__, P.W2__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_514})

V_359 = Vertex(name = 'V_359',
               particles = [ P.W__minus__, P.W2__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_466})

V_360 = Vertex(name = 'V_360',
               particles = [ P.W__minus__, P.W2__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_516})

V_361 = Vertex(name = 'V_361',
               particles = [ P.W__minus__, P.W2__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_461})

V_362 = Vertex(name = 'V_362',
               particles = [ P.W2__minus__, P.W2__plus__, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_550})

V_363 = Vertex(name = 'V_363',
               particles = [ P.W2__minus__, P.W2__plus__, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_725})

V_364 = Vertex(name = 'V_364',
               particles = [ P.W2__minus__, P.W2__plus__, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_668})

V_365 = Vertex(name = 'V_365',
               particles = [ P.W2__minus__, P.W2__plus__, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_40})

V_366 = Vertex(name = 'V_366',
               particles = [ P.W2__minus__, P.W2__plus__, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_547})

V_367 = Vertex(name = 'V_367',
               particles = [ P.W2__minus__, P.W2__plus__, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_723})

V_368 = Vertex(name = 'V_368',
               particles = [ P.W2__minus__, P.W2__plus__, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_666})

V_369 = Vertex(name = 'V_369',
               particles = [ P.W2__minus__, P.W2__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_538})

V_370 = Vertex(name = 'V_370',
               particles = [ P.W2__minus__, P.W2__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_718})

V_371 = Vertex(name = 'V_371',
               particles = [ P.W2__minus__, P.W2__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_661})

V_372 = Vertex(name = 'V_372',
               particles = [ P.W2__minus__, P.W2__plus__, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_482})

V_373 = Vertex(name = 'V_373',
               particles = [ P.W2__minus__, P.W2__plus__, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_482})

V_374 = Vertex(name = 'V_374',
               particles = [ P.W2__minus__, P.W2__plus__, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_483})

V_375 = Vertex(name = 'V_375',
               particles = [ P.W2__minus__, P.W2__plus__, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_482})

V_376 = Vertex(name = 'V_376',
               particles = [ P.W2__minus__, P.W2__plus__, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_533})

V_377 = Vertex(name = 'V_377',
               particles = [ P.W2__minus__, P.W2__plus__, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_714})

V_378 = Vertex(name = 'V_378',
               particles = [ P.W2__minus__, P.W2__plus__, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_657})

V_379 = Vertex(name = 'V_379',
               particles = [ P.W2__minus__, P.W2__plus__, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_533})

V_380 = Vertex(name = 'V_380',
               particles = [ P.W2__minus__, P.W2__plus__, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_715})

V_381 = Vertex(name = 'V_381',
               particles = [ P.W2__minus__, P.W2__plus__, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_657})

V_382 = Vertex(name = 'V_382',
               particles = [ P.W2__minus__, P.W2__plus__, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_533})

V_383 = Vertex(name = 'V_383',
               particles = [ P.W2__minus__, P.W2__plus__, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_657})

V_384 = Vertex(name = 'V_384',
               particles = [ P.W2__minus__, P.W2__plus__, P.H, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_632})

V_385 = Vertex(name = 'V_385',
               particles = [ P.W2__minus__, P.W2__plus__, P.H01, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_638})

V_386 = Vertex(name = 'V_386',
               particles = [ P.W2__minus__, P.W2__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_674})

V_387 = Vertex(name = 'V_387',
               particles = [ P.W2__minus__, P.W2__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_738})

V_388 = Vertex(name = 'V_388',
               particles = [ P.W2__minus__, P.W2__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_692})

V_389 = Vertex(name = 'V_389',
               particles = [ P.W2__minus__, P.W2__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_798})

V_390 = Vertex(name = 'V_390',
               particles = [ P.W2__minus__, P.W2__plus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_676})

V_391 = Vertex(name = 'V_391',
               particles = [ P.W2__minus__, P.W2__plus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_739})

V_392 = Vertex(name = 'V_392',
               particles = [ P.W2__minus__, P.W2__plus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_693})

V_393 = Vertex(name = 'V_393',
               particles = [ P.W2__minus__, P.W2__plus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_797})

V_394 = Vertex(name = 'V_394',
               particles = [ P.W2__minus__, P.W2__plus__, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_686})

V_395 = Vertex(name = 'V_395',
               particles = [ P.W2__minus__, P.W2__plus__, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_787})

V_396 = Vertex(name = 'V_396',
               particles = [ P.a, P.a, P.W2__minus__, P.W2__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_121})

V_397 = Vertex(name = 'V_397',
               particles = [ P.a, P.a, P.W2__minus__, P.W2__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_497})

V_398 = Vertex(name = 'V_398',
               particles = [ P.W2__minus__, P.W2__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_116})

V_399 = Vertex(name = 'V_399',
               particles = [ P.W2__minus__, P.W2__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_204})

V_400 = Vertex(name = 'V_400',
               particles = [ P.W2__minus__, P.W2__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_481})

V_401 = Vertex(name = 'V_401',
               particles = [ P.W2__minus__, P.W2__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_125})

V_402 = Vertex(name = 'V_402',
               particles = [ P.W2__minus__, P.W2__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_184})

V_403 = Vertex(name = 'V_403',
               particles = [ P.W2__minus__, P.W2__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_489})

V_404 = Vertex(name = 'V_404',
               particles = [ P.W2__plus__, P.W2__plus__, P.H02, P.HR__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_45})

V_405 = Vertex(name = 'V_405',
               particles = [ P.W2__plus__, P.W2__plus__, P.H, P.HR__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_75})

V_406 = Vertex(name = 'V_406',
               particles = [ P.W2__plus__, P.W2__plus__, P.A02, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_484})

V_407 = Vertex(name = 'V_407',
               particles = [ P.W2__plus__, P.W2__plus__, P.H03, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_485})

V_408 = Vertex(name = 'V_408',
               particles = [ P.W2__plus__, P.W2__plus__, P.HM2, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_716})

V_409 = Vertex(name = 'V_409',
               particles = [ P.W2__plus__, P.W2__plus__, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_788})

V_410 = Vertex(name = 'V_410',
               particles = [ P.W2__plus__, P.W2__plus__, P.HR__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_796})

V_411 = Vertex(name = 'V_411',
               particles = [ P.W__minus__, P.W__minus__, P.W2__plus__, P.W2__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_488})

V_412 = Vertex(name = 'V_412',
               particles = [ P.W__minus__, P.W2__minus__, P.W2__plus__, P.W2__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_429})

V_413 = Vertex(name = 'V_413',
               particles = [ P.W__minus__, P.W2__minus__, P.W2__plus__, P.W2__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_511})

V_414 = Vertex(name = 'V_414',
               particles = [ P.W2__minus__, P.W2__minus__, P.W2__plus__, P.W2__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_47})

V_415 = Vertex(name = 'V_415',
               particles = [ P.W2__minus__, P.W2__minus__, P.W2__plus__, P.W2__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_513})

V_416 = Vertex(name = 'V_416',
               particles = [ P.a, P.W__plus__, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_93})

V_417 = Vertex(name = 'V_417',
               particles = [ P.a, P.W__plus__, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_188})

V_418 = Vertex(name = 'V_418',
               particles = [ P.a, P.W__plus__, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_94})

V_419 = Vertex(name = 'V_419',
               particles = [ P.a, P.W__plus__, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_187})

V_420 = Vertex(name = 'V_420',
               particles = [ P.a, P.W__plus__, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_92})

V_421 = Vertex(name = 'V_421',
               particles = [ P.a, P.W__plus__, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_185})

V_422 = Vertex(name = 'V_422',
               particles = [ P.a, P.W__plus__, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_554})

V_423 = Vertex(name = 'V_423',
               particles = [ P.a, P.W__plus__, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_729})

V_424 = Vertex(name = 'V_424',
               particles = [ P.a, P.W__plus__, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_553})

V_425 = Vertex(name = 'V_425',
               particles = [ P.a, P.W__plus__, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_728})

V_426 = Vertex(name = 'V_426',
               particles = [ P.a, P.W__plus__, P.H, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_643})

V_427 = Vertex(name = 'V_427',
               particles = [ P.a, P.W__plus__, P.H02, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_644})

V_428 = Vertex(name = 'V_428',
               particles = [ P.a, P.W__plus__, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_688})

V_429 = Vertex(name = 'V_429',
               particles = [ P.a, P.W__plus__, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_752})

V_430 = Vertex(name = 'V_430',
               particles = [ P.a, P.W__plus__, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_764})

V_431 = Vertex(name = 'V_431',
               particles = [ P.W__plus__, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_529})

V_432 = Vertex(name = 'V_432',
               particles = [ P.W__plus__, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_713})

V_433 = Vertex(name = 'V_433',
               particles = [ P.W__plus__, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_15})

V_434 = Vertex(name = 'V_434',
               particles = [ P.W__plus__, P.H, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_628})

V_435 = Vertex(name = 'V_435',
               particles = [ P.W__plus__, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_528})

V_436 = Vertex(name = 'V_436',
               particles = [ P.W__plus__, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_711})

V_437 = Vertex(name = 'V_437',
               particles = [ P.W__plus__, P.H02, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_634})

V_438 = Vertex(name = 'V_438',
               particles = [ P.W__plus__, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_14})

V_439 = Vertex(name = 'V_439',
               particles = [ P.W__plus__, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_11})

V_440 = Vertex(name = 'V_440',
               particles = [ P.W__minus__, P.W__plus__, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_40})

V_441 = Vertex(name = 'V_441',
               particles = [ P.W__minus__, P.W__plus__, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_40})

V_442 = Vertex(name = 'V_442',
               particles = [ P.W__minus__, P.W__plus__, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_41})

V_443 = Vertex(name = 'V_443',
               particles = [ P.W__minus__, P.W__plus__, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_40})

V_444 = Vertex(name = 'V_444',
               particles = [ P.W__minus__, P.W__plus__, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_669})

V_445 = Vertex(name = 'V_445',
               particles = [ P.W__minus__, P.W__plus__, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_549})

V_446 = Vertex(name = 'V_446',
               particles = [ P.W__minus__, P.W__plus__, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_726})

V_447 = Vertex(name = 'V_447',
               particles = [ P.W__minus__, P.W__plus__, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_482})

V_448 = Vertex(name = 'V_448',
               particles = [ P.W__minus__, P.W__plus__, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_667})

V_449 = Vertex(name = 'V_449',
               particles = [ P.W__minus__, P.W__plus__, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_546})

V_450 = Vertex(name = 'V_450',
               particles = [ P.W__minus__, P.W__plus__, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_724})

V_451 = Vertex(name = 'V_451',
               particles = [ P.W__minus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_662})

V_452 = Vertex(name = 'V_452',
               particles = [ P.W__minus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_537})

V_453 = Vertex(name = 'V_453',
               particles = [ P.W__minus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_719})

V_454 = Vertex(name = 'V_454',
               particles = [ P.W__minus__, P.W__plus__, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_533})

V_455 = Vertex(name = 'V_455',
               particles = [ P.W__minus__, P.W__plus__, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_715})

V_456 = Vertex(name = 'V_456',
               particles = [ P.W__minus__, P.W__plus__, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_657})

V_457 = Vertex(name = 'V_457',
               particles = [ P.W__minus__, P.W__plus__, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_533})

V_458 = Vertex(name = 'V_458',
               particles = [ P.W__minus__, P.W__plus__, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_714})

V_459 = Vertex(name = 'V_459',
               particles = [ P.W__minus__, P.W__plus__, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_657})

V_460 = Vertex(name = 'V_460',
               particles = [ P.W__minus__, P.W__plus__, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_533})

V_461 = Vertex(name = 'V_461',
               particles = [ P.W__minus__, P.W__plus__, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_657})

V_462 = Vertex(name = 'V_462',
               particles = [ P.W__minus__, P.W__plus__, P.H, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_631})

V_463 = Vertex(name = 'V_463',
               particles = [ P.W__minus__, P.W__plus__, P.H01, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_637})

V_464 = Vertex(name = 'V_464',
               particles = [ P.W__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_674})

V_465 = Vertex(name = 'V_465',
               particles = [ P.W__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_888})

V_466 = Vertex(name = 'V_466',
               particles = [ P.W__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_692})

V_467 = Vertex(name = 'V_467',
               particles = [ P.W__minus__, P.W__plus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_676})

V_468 = Vertex(name = 'V_468',
               particles = [ P.W__minus__, P.W__plus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_887})

V_469 = Vertex(name = 'V_469',
               particles = [ P.W__minus__, P.W__plus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_693})

V_470 = Vertex(name = 'V_470',
               particles = [ P.W__minus__, P.W__plus__, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_685})

V_471 = Vertex(name = 'V_471',
               particles = [ P.W__minus__, P.W__plus__, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_744})

V_472 = Vertex(name = 'V_472',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_121})

V_473 = Vertex(name = 'V_473',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_497})

V_474 = Vertex(name = 'V_474',
               particles = [ P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_34})

V_475 = Vertex(name = 'V_475',
               particles = [ P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_496})

V_476 = Vertex(name = 'V_476',
               particles = [ P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_504})

V_477 = Vertex(name = 'V_477',
               particles = [ P.W__minus__, P.W__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_63})

V_478 = Vertex(name = 'V_478',
               particles = [ P.W__minus__, P.W__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_498})

V_479 = Vertex(name = 'V_479',
               particles = [ P.W__minus__, P.W__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_503})

V_480 = Vertex(name = 'V_480',
               particles = [ P.W2__minus__, P.W__plus__, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_421})

V_481 = Vertex(name = 'V_481',
               particles = [ P.W2__minus__, P.W__plus__, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_427})

V_482 = Vertex(name = 'V_482',
               particles = [ P.W2__minus__, P.W__plus__, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_707})

V_483 = Vertex(name = 'V_483',
               particles = [ P.W2__minus__, P.W__plus__, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_737})

V_484 = Vertex(name = 'V_484',
               particles = [ P.W2__minus__, P.W__plus__, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_421})

V_485 = Vertex(name = 'V_485',
               particles = [ P.W2__minus__, P.W__plus__, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_422})

V_486 = Vertex(name = 'V_486',
               particles = [ P.W2__minus__, P.W__plus__, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_421})

V_487 = Vertex(name = 'V_487',
               particles = [ P.W2__minus__, P.W__plus__, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_420})

V_488 = Vertex(name = 'V_488',
               particles = [ P.W2__minus__, P.W__plus__, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_447})

V_489 = Vertex(name = 'V_489',
               particles = [ P.W2__minus__, P.W__plus__, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_706})

V_490 = Vertex(name = 'V_490',
               particles = [ P.W2__minus__, P.W__plus__, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_736})

V_491 = Vertex(name = 'V_491',
               particles = [ P.W2__minus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_448})

V_492 = Vertex(name = 'V_492',
               particles = [ P.W2__minus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_705})

V_493 = Vertex(name = 'V_493',
               particles = [ P.W2__minus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_735})

V_494 = Vertex(name = 'V_494',
               particles = [ P.W2__minus__, P.W__plus__, P.A01, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_536})

V_495 = Vertex(name = 'V_495',
               particles = [ P.W2__minus__, P.W__plus__, P.A01, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_660})

V_496 = Vertex(name = 'V_496',
               particles = [ P.W2__minus__, P.W__plus__, P.H, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_535})

V_497 = Vertex(name = 'V_497',
               particles = [ P.W2__minus__, P.W__plus__, P.H, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_659})

V_498 = Vertex(name = 'V_498',
               particles = [ P.W2__minus__, P.W__plus__, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_701})

V_499 = Vertex(name = 'V_499',
               particles = [ P.W2__minus__, P.W__plus__, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_732})

V_500 = Vertex(name = 'V_500',
               particles = [ P.W2__minus__, P.W__plus__, P.A01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_704})

V_501 = Vertex(name = 'V_501',
               particles = [ P.W2__minus__, P.W__plus__, P.A01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_734})

V_502 = Vertex(name = 'V_502',
               particles = [ P.W2__minus__, P.W__plus__, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_702})

V_503 = Vertex(name = 'V_503',
               particles = [ P.W2__minus__, P.W__plus__, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_731})

V_504 = Vertex(name = 'V_504',
               particles = [ P.W2__minus__, P.W__plus__, P.A01, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_545})

V_505 = Vertex(name = 'V_505',
               particles = [ P.W2__minus__, P.W__plus__, P.A01, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_665})

V_506 = Vertex(name = 'V_506',
               particles = [ P.W2__minus__, P.W__plus__, P.H01, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_544})

V_507 = Vertex(name = 'V_507',
               particles = [ P.W2__minus__, P.W__plus__, P.H01, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_664})

V_508 = Vertex(name = 'V_508',
               particles = [ P.W2__minus__, P.W__plus__, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_673})

V_509 = Vertex(name = 'V_509',
               particles = [ P.W2__minus__, P.W__plus__, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_691})

V_510 = Vertex(name = 'V_510',
               particles = [ P.W2__minus__, P.W__plus__, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_672})

V_511 = Vertex(name = 'V_511',
               particles = [ P.W2__minus__, P.W__plus__, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_690})

V_512 = Vertex(name = 'V_512',
               particles = [ P.W2__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_886})

V_513 = Vertex(name = 'V_513',
               particles = [ P.W2__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_740})

V_514 = Vertex(name = 'V_514',
               particles = [ P.W2__minus__, P.W__plus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_885})

V_515 = Vertex(name = 'V_515',
               particles = [ P.W2__minus__, P.W__plus__, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_741})

V_516 = Vertex(name = 'V_516',
               particles = [ P.W2__minus__, P.W__plus__, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_778})

V_517 = Vertex(name = 'V_517',
               particles = [ P.W2__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_514})

V_518 = Vertex(name = 'V_518',
               particles = [ P.W2__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_466})

V_519 = Vertex(name = 'V_519',
               particles = [ P.W2__minus__, P.W__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_516})

V_520 = Vertex(name = 'V_520',
               particles = [ P.W2__minus__, P.W__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVV1 ],
               couplings = {(0,0):C.GC_461})

V_521 = Vertex(name = 'V_521',
               particles = [ P.W2__plus__, P.W__plus__, P.A02, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_423})

V_522 = Vertex(name = 'V_522',
               particles = [ P.W2__plus__, P.W__plus__, P.H03, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_424})

V_523 = Vertex(name = 'V_523',
               particles = [ P.W2__plus__, P.W__plus__, P.H02, P.HR__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_426})

V_524 = Vertex(name = 'V_524',
               particles = [ P.W2__plus__, P.W__plus__, P.H, P.HR__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_446})

V_525 = Vertex(name = 'V_525',
               particles = [ P.W2__plus__, P.W__plus__, P.HM2, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_703})

V_526 = Vertex(name = 'V_526',
               particles = [ P.W2__plus__, P.W__plus__, P.HM2, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_733})

V_527 = Vertex(name = 'V_527',
               particles = [ P.W2__plus__, P.W__plus__, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_779})

V_528 = Vertex(name = 'V_528',
               particles = [ P.W2__plus__, P.W__plus__, P.HR__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_845})

V_529 = Vertex(name = 'V_529',
               particles = [ P.W__minus__, P.W__minus__, P.W2__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_428})

V_530 = Vertex(name = 'V_530',
               particles = [ P.W__minus__, P.W__minus__, P.W2__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_512})

V_531 = Vertex(name = 'V_531',
               particles = [ P.W__minus__, P.W2__minus__, P.W2__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_488})

V_532 = Vertex(name = 'V_532',
               particles = [ P.W2__minus__, P.W2__minus__, P.W2__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_429})

V_533 = Vertex(name = 'V_533',
               particles = [ P.W2__minus__, P.W2__minus__, P.W2__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_511})

V_534 = Vertex(name = 'V_534',
               particles = [ P.W__plus__, P.W__plus__, P.A02, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_42})

V_535 = Vertex(name = 'V_535',
               particles = [ P.W__plus__, P.W__plus__, P.H03, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_43})

V_536 = Vertex(name = 'V_536',
               particles = [ P.W__plus__, P.W__plus__, P.H02, P.HR__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_487})

V_537 = Vertex(name = 'V_537',
               particles = [ P.W__plus__, P.W__plus__, P.H, P.HR__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_492})

V_538 = Vertex(name = 'V_538',
               particles = [ P.W__plus__, P.W__plus__, P.HM2, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_717})

V_539 = Vertex(name = 'V_539',
               particles = [ P.W__plus__, P.W__plus__, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_745})

V_540 = Vertex(name = 'V_540',
               particles = [ P.W__plus__, P.W__plus__, P.HR__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_846})

V_541 = Vertex(name = 'V_541',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_47})

V_542 = Vertex(name = 'V_542',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_513})

V_543 = Vertex(name = 'V_543',
               particles = [ P.W__minus__, P.W2__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_428})

V_544 = Vertex(name = 'V_544',
               particles = [ P.W__minus__, P.W2__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_512})

V_545 = Vertex(name = 'V_545',
               particles = [ P.W2__minus__, P.W2__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_488})

V_546 = Vertex(name = 'V_546',
               particles = [ P.e__plus__, P.e__plus__, P.HR__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS3 ],
               couplings = {(0,0):C.GC_959})

V_547 = Vertex(name = 'V_547',
               particles = [ P.e__plus__, P.N1, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_970})

V_548 = Vertex(name = 'V_548',
               particles = [ P.e__plus__, P.v1, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_967})

V_549 = Vertex(name = 'V_549',
               particles = [ P.e__plus__, P.e__plus__, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_966})

V_550 = Vertex(name = 'V_550',
               particles = [ P.mu__plus__, P.mu__plus__, P.HR__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS3 ],
               couplings = {(0,0):C.GC_997})

V_551 = Vertex(name = 'V_551',
               particles = [ P.mu__plus__, P.N2, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1008})

V_552 = Vertex(name = 'V_552',
               particles = [ P.mu__plus__, P.v2, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1005})

V_553 = Vertex(name = 'V_553',
               particles = [ P.mu__plus__, P.mu__plus__, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1004})

V_554 = Vertex(name = 'V_554',
               particles = [ P.ta__plus__, P.ta__plus__, P.HR__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS3 ],
               couplings = {(0,0):C.GC_1035})

V_555 = Vertex(name = 'V_555',
               particles = [ P.ta__plus__, P.N3, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1046})

V_556 = Vertex(name = 'V_556',
               particles = [ P.ta__plus__, P.v3, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1043})

V_557 = Vertex(name = 'V_557',
               particles = [ P.ta__plus__, P.ta__plus__, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1042})

V_558 = Vertex(name = 'V_558',
               particles = [ P.a, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_331})

V_559 = Vertex(name = 'V_559',
               particles = [ P.a, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_352})

V_560 = Vertex(name = 'V_560',
               particles = [ P.a, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_137})

V_561 = Vertex(name = 'V_561',
               particles = [ P.a, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_240})

V_562 = Vertex(name = 'V_562',
               particles = [ P.a, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_248})

V_563 = Vertex(name = 'V_563',
               particles = [ P.a, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_271})

V_564 = Vertex(name = 'V_564',
               particles = [ P.a, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_330})

V_565 = Vertex(name = 'V_565',
               particles = [ P.a, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_351})

V_566 = Vertex(name = 'V_566',
               particles = [ P.a, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_136})

V_567 = Vertex(name = 'V_567',
               particles = [ P.a, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_239})

V_568 = Vertex(name = 'V_568',
               particles = [ P.a, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_247})

V_569 = Vertex(name = 'V_569',
               particles = [ P.a, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_270})

V_570 = Vertex(name = 'V_570',
               particles = [ P.a, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_347})

V_571 = Vertex(name = 'V_571',
               particles = [ P.a, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_363})

V_572 = Vertex(name = 'V_572',
               particles = [ P.a, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_142})

V_573 = Vertex(name = 'V_573',
               particles = [ P.a, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_258})

V_574 = Vertex(name = 'V_574',
               particles = [ P.a, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_259})

V_575 = Vertex(name = 'V_575',
               particles = [ P.a, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_289})

V_576 = Vertex(name = 'V_576',
               particles = [ P.a, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_350})

V_577 = Vertex(name = 'V_577',
               particles = [ P.a, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_369})

V_578 = Vertex(name = 'V_578',
               particles = [ P.a, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_144})

V_579 = Vertex(name = 'V_579',
               particles = [ P.a, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_265})

V_580 = Vertex(name = 'V_580',
               particles = [ P.a, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_266})

V_581 = Vertex(name = 'V_581',
               particles = [ P.a, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_295})

V_582 = Vertex(name = 'V_582',
               particles = [ P.a, P.Z, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_351})

V_583 = Vertex(name = 'V_583',
               particles = [ P.a, P.Z, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_91})

V_584 = Vertex(name = 'V_584',
               particles = [ P.a, P.Z, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_126})

V_585 = Vertex(name = 'V_585',
               particles = [ P.a, P.Z, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_386})

V_586 = Vertex(name = 'V_586',
               particles = [ P.a, P.Z, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_239})

V_587 = Vertex(name = 'V_587',
               particles = [ P.a, P.Z, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_351})

V_588 = Vertex(name = 'V_588',
               particles = [ P.a, P.Z, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_91})

V_589 = Vertex(name = 'V_589',
               particles = [ P.a, P.Z, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_126})

V_590 = Vertex(name = 'V_590',
               particles = [ P.a, P.Z, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_386})

V_591 = Vertex(name = 'V_591',
               particles = [ P.a, P.Z, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_239})

V_592 = Vertex(name = 'V_592',
               particles = [ P.a, P.Z, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_351})

V_593 = Vertex(name = 'V_593',
               particles = [ P.a, P.Z, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_239})

V_594 = Vertex(name = 'V_594',
               particles = [ P.a, P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_351})

V_595 = Vertex(name = 'V_595',
               particles = [ P.a, P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_91})

V_596 = Vertex(name = 'V_596',
               particles = [ P.a, P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_127})

V_597 = Vertex(name = 'V_597',
               particles = [ P.a, P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_385})

V_598 = Vertex(name = 'V_598',
               particles = [ P.a, P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_239})

V_599 = Vertex(name = 'V_599',
               particles = [ P.a, P.Z, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_576})

V_600 = Vertex(name = 'V_600',
               particles = [ P.a, P.Z, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_601})

V_601 = Vertex(name = 'V_601',
               particles = [ P.a, P.Z, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_790})

V_602 = Vertex(name = 'V_602',
               particles = [ P.a, P.Z, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_751})

V_603 = Vertex(name = 'V_603',
               particles = [ P.a, P.Z, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_761})

V_604 = Vertex(name = 'V_604',
               particles = [ P.a, P.Z, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_794})

V_605 = Vertex(name = 'V_605',
               particles = [ P.a, P.Z, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_769})

V_606 = Vertex(name = 'V_606',
               particles = [ P.a, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_850})

V_607 = Vertex(name = 'V_607',
               particles = [ P.a, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_857})

V_608 = Vertex(name = 'V_608',
               particles = [ P.a, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_805})

V_609 = Vertex(name = 'V_609',
               particles = [ P.a, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_817})

V_610 = Vertex(name = 'V_610',
               particles = [ P.a, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_818})

V_611 = Vertex(name = 'V_611',
               particles = [ P.a, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_828})

V_612 = Vertex(name = 'V_612',
               particles = [ P.a, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_855})

V_613 = Vertex(name = 'V_613',
               particles = [ P.a, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_863})

V_614 = Vertex(name = 'V_614',
               particles = [ P.a, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_807})

V_615 = Vertex(name = 'V_615',
               particles = [ P.a, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_824})

V_616 = Vertex(name = 'V_616',
               particles = [ P.a, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_825})

V_617 = Vertex(name = 'V_617',
               particles = [ P.a, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_835})

V_618 = Vertex(name = 'V_618',
               particles = [ P.Z, P.A01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_557})

V_619 = Vertex(name = 'V_619',
               particles = [ P.Z, P.A01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_598})

V_620 = Vertex(name = 'V_620',
               particles = [ P.Z, P.A02, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_8})

V_621 = Vertex(name = 'V_621',
               particles = [ P.Z, P.A02, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_100})

V_622 = Vertex(name = 'V_622',
               particles = [ P.Z, P.A02, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_217})

V_623 = Vertex(name = 'V_623',
               particles = [ P.Z, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_99})

V_624 = Vertex(name = 'V_624',
               particles = [ P.Z, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_219})

V_625 = Vertex(name = 'V_625',
               particles = [ P.Z, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_558})

V_626 = Vertex(name = 'V_626',
               particles = [ P.Z, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_599})

V_627 = Vertex(name = 'V_627',
               particles = [ P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_10})

V_628 = Vertex(name = 'V_628',
               particles = [ P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_99})

V_629 = Vertex(name = 'V_629',
               particles = [ P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_219})

V_630 = Vertex(name = 'V_630',
               particles = [ P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_99})

V_631 = Vertex(name = 'V_631',
               particles = [ P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_115})

V_632 = Vertex(name = 'V_632',
               particles = [ P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_203})

V_633 = Vertex(name = 'V_633',
               particles = [ P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_219})

V_634 = Vertex(name = 'V_634',
               particles = [ P.W__minus__, P.Z, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_39})

V_635 = Vertex(name = 'V_635',
               particles = [ P.W__minus__, P.Z, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_104})

V_636 = Vertex(name = 'V_636',
               particles = [ P.W__minus__, P.Z, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_232})

V_637 = Vertex(name = 'V_637',
               particles = [ P.W__minus__, P.Z, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_38})

V_638 = Vertex(name = 'V_638',
               particles = [ P.W__minus__, P.Z, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_105})

V_639 = Vertex(name = 'V_639',
               particles = [ P.W__minus__, P.Z, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_231})

V_640 = Vertex(name = 'V_640',
               particles = [ P.W__minus__, P.Z, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_36})

V_641 = Vertex(name = 'V_641',
               particles = [ P.W__minus__, P.Z, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_103})

V_642 = Vertex(name = 'V_642',
               particles = [ P.W__minus__, P.Z, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_229})

V_643 = Vertex(name = 'V_643',
               particles = [ P.W__minus__, P.Z, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_564})

V_644 = Vertex(name = 'V_644',
               particles = [ P.W__minus__, P.Z, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_622})

V_645 = Vertex(name = 'V_645',
               particles = [ P.W__minus__, P.Z, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_563})

V_646 = Vertex(name = 'V_646',
               particles = [ P.W__minus__, P.Z, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_623})

V_647 = Vertex(name = 'V_647',
               particles = [ P.W__minus__, P.Z, P.H, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_630})

V_648 = Vertex(name = 'V_648',
               particles = [ P.W__minus__, P.Z, P.H02, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_636})

V_649 = Vertex(name = 'V_649',
               particles = [ P.W__minus__, P.Z, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_684})

V_650 = Vertex(name = 'V_650',
               particles = [ P.W__minus__, P.Z, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_743})

V_651 = Vertex(name = 'V_651',
               particles = [ P.W__minus__, P.Z, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_755})

V_652 = Vertex(name = 'V_652',
               particles = [ P.W__minus__, P.Z, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_768})

V_653 = Vertex(name = 'V_653',
               particles = [ P.W2__minus__, P.Z, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_419})

V_654 = Vertex(name = 'V_654',
               particles = [ P.W2__minus__, P.Z, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_458})

V_655 = Vertex(name = 'V_655',
               particles = [ P.W2__minus__, P.Z, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_470})

V_656 = Vertex(name = 'V_656',
               particles = [ P.W2__minus__, P.Z, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_418})

V_657 = Vertex(name = 'V_657',
               particles = [ P.W2__minus__, P.Z, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_459})

V_658 = Vertex(name = 'V_658',
               particles = [ P.W2__minus__, P.Z, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_469})

V_659 = Vertex(name = 'V_659',
               particles = [ P.W2__minus__, P.Z, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_416})

V_660 = Vertex(name = 'V_660',
               particles = [ P.W2__minus__, P.Z, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_457})

V_661 = Vertex(name = 'V_661',
               particles = [ P.W2__minus__, P.Z, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_467})

V_662 = Vertex(name = 'V_662',
               particles = [ P.W2__minus__, P.Z, P.H, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_532})

V_663 = Vertex(name = 'V_663',
               particles = [ P.W2__minus__, P.Z, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_699})

V_664 = Vertex(name = 'V_664',
               particles = [ P.W2__minus__, P.Z, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_647})

V_665 = Vertex(name = 'V_665',
               particles = [ P.W2__minus__, P.Z, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_656})

V_666 = Vertex(name = 'V_666',
               particles = [ P.W2__minus__, P.Z, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_698})

V_667 = Vertex(name = 'V_667',
               particles = [ P.W2__minus__, P.Z, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_646})

V_668 = Vertex(name = 'V_668',
               particles = [ P.W2__minus__, P.Z, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_655})

V_669 = Vertex(name = 'V_669',
               particles = [ P.W2__minus__, P.Z, P.H02, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_542})

V_670 = Vertex(name = 'V_670',
               particles = [ P.W2__minus__, P.Z, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_670})

V_671 = Vertex(name = 'V_671',
               particles = [ P.W2__minus__, P.Z, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_777})

V_672 = Vertex(name = 'V_672',
               particles = [ P.W2__minus__, P.Z, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_783})

V_673 = Vertex(name = 'V_673',
               particles = [ P.W2__minus__, P.Z, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_785})

V_674 = Vertex(name = 'V_674',
               particles = [ P.W2__plus__, P.Z, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_417})

V_675 = Vertex(name = 'V_675',
               particles = [ P.W2__plus__, P.Z, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_460})

V_676 = Vertex(name = 'V_676',
               particles = [ P.W2__plus__, P.Z, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_468})

V_677 = Vertex(name = 'V_677',
               particles = [ P.W2__plus__, P.Z, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_418})

V_678 = Vertex(name = 'V_678',
               particles = [ P.W2__plus__, P.Z, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_459})

V_679 = Vertex(name = 'V_679',
               particles = [ P.W2__plus__, P.Z, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_469})

V_680 = Vertex(name = 'V_680',
               particles = [ P.W2__plus__, P.Z, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_416})

V_681 = Vertex(name = 'V_681',
               particles = [ P.W2__plus__, P.Z, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_457})

V_682 = Vertex(name = 'V_682',
               particles = [ P.W2__plus__, P.Z, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_467})

V_683 = Vertex(name = 'V_683',
               particles = [ P.W2__plus__, P.Z, P.H, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_532})

V_684 = Vertex(name = 'V_684',
               particles = [ P.W2__plus__, P.Z, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_697})

V_685 = Vertex(name = 'V_685',
               particles = [ P.W2__plus__, P.Z, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_645})

V_686 = Vertex(name = 'V_686',
               particles = [ P.W2__plus__, P.Z, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_654})

V_687 = Vertex(name = 'V_687',
               particles = [ P.W2__plus__, P.Z, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_698})

V_688 = Vertex(name = 'V_688',
               particles = [ P.W2__plus__, P.Z, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_646})

V_689 = Vertex(name = 'V_689',
               particles = [ P.W2__plus__, P.Z, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_655})

V_690 = Vertex(name = 'V_690',
               particles = [ P.W2__plus__, P.Z, P.H02, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_542})

V_691 = Vertex(name = 'V_691',
               particles = [ P.W2__plus__, P.Z, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_670})

V_692 = Vertex(name = 'V_692',
               particles = [ P.W2__plus__, P.Z, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_777})

V_693 = Vertex(name = 'V_693',
               particles = [ P.W2__plus__, P.Z, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_783})

V_694 = Vertex(name = 'V_694',
               particles = [ P.W2__plus__, P.Z, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_785})

V_695 = Vertex(name = 'V_695',
               particles = [ P.a, P.W__minus__, P.W2__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_518})

V_696 = Vertex(name = 'V_696',
               particles = [ P.a, P.W__minus__, P.W2__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_476})

V_697 = Vertex(name = 'V_697',
               particles = [ P.a, P.W2__minus__, P.W2__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_138})

V_698 = Vertex(name = 'V_698',
               particles = [ P.a, P.W2__minus__, P.W2__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_249})

V_699 = Vertex(name = 'V_699',
               particles = [ P.a, P.W2__minus__, P.W2__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_494})

V_700 = Vertex(name = 'V_700',
               particles = [ P.W__plus__, P.Z, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_37})

V_701 = Vertex(name = 'V_701',
               particles = [ P.W__plus__, P.Z, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_106})

V_702 = Vertex(name = 'V_702',
               particles = [ P.W__plus__, P.Z, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_230})

V_703 = Vertex(name = 'V_703',
               particles = [ P.W__plus__, P.Z, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_38})

V_704 = Vertex(name = 'V_704',
               particles = [ P.W__plus__, P.Z, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_105})

V_705 = Vertex(name = 'V_705',
               particles = [ P.W__plus__, P.Z, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_231})

V_706 = Vertex(name = 'V_706',
               particles = [ P.W__plus__, P.Z, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_36})

V_707 = Vertex(name = 'V_707',
               particles = [ P.W__plus__, P.Z, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_103})

V_708 = Vertex(name = 'V_708',
               particles = [ P.W__plus__, P.Z, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_229})

V_709 = Vertex(name = 'V_709',
               particles = [ P.W__plus__, P.Z, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_562})

V_710 = Vertex(name = 'V_710',
               particles = [ P.W__plus__, P.Z, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_624})

V_711 = Vertex(name = 'V_711',
               particles = [ P.W__plus__, P.Z, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_563})

V_712 = Vertex(name = 'V_712',
               particles = [ P.W__plus__, P.Z, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_623})

V_713 = Vertex(name = 'V_713',
               particles = [ P.W__plus__, P.Z, P.H, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_630})

V_714 = Vertex(name = 'V_714',
               particles = [ P.W__plus__, P.Z, P.H02, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_636})

V_715 = Vertex(name = 'V_715',
               particles = [ P.W__plus__, P.Z, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_684})

V_716 = Vertex(name = 'V_716',
               particles = [ P.W__plus__, P.Z, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_743})

V_717 = Vertex(name = 'V_717',
               particles = [ P.W__plus__, P.Z, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_755})

V_718 = Vertex(name = 'V_718',
               particles = [ P.W__plus__, P.Z, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_768})

V_719 = Vertex(name = 'V_719',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_96})

V_720 = Vertex(name = 'V_720',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_499})

V_721 = Vertex(name = 'V_721',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_506})

V_722 = Vertex(name = 'V_722',
               particles = [ P.a, P.W2__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_518})

V_723 = Vertex(name = 'V_723',
               particles = [ P.a, P.W2__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_476})

V_724 = Vertex(name = 'V_724',
               particles = [ P.Z, P.Z, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_35})

V_725 = Vertex(name = 'V_725',
               particles = [ P.Z, P.Z, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_101})

V_726 = Vertex(name = 'V_726',
               particles = [ P.Z, P.Z, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_375})

V_727 = Vertex(name = 'V_727',
               particles = [ P.Z, P.Z, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_128})

V_728 = Vertex(name = 'V_728',
               particles = [ P.Z, P.Z, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_225})

V_729 = Vertex(name = 'V_729',
               particles = [ P.Z, P.Z, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_274})

V_730 = Vertex(name = 'V_730',
               particles = [ P.Z, P.Z, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_35})

V_731 = Vertex(name = 'V_731',
               particles = [ P.Z, P.Z, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_101})

V_732 = Vertex(name = 'V_732',
               particles = [ P.Z, P.Z, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_375})

V_733 = Vertex(name = 'V_733',
               particles = [ P.Z, P.Z, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_128})

V_734 = Vertex(name = 'V_734',
               particles = [ P.Z, P.Z, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_225})

V_735 = Vertex(name = 'V_735',
               particles = [ P.Z, P.Z, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_274})

V_736 = Vertex(name = 'V_736',
               particles = [ P.Z, P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_35})

V_737 = Vertex(name = 'V_737',
               particles = [ P.Z, P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_102})

V_738 = Vertex(name = 'V_738',
               particles = [ P.Z, P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_375})

V_739 = Vertex(name = 'V_739',
               particles = [ P.Z, P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_128})

V_740 = Vertex(name = 'V_740',
               particles = [ P.Z, P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_224})

V_741 = Vertex(name = 'V_741',
               particles = [ P.Z, P.Z, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_274})

V_742 = Vertex(name = 'V_742',
               particles = [ P.Z, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_575})

V_743 = Vertex(name = 'V_743',
               particles = [ P.Z, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_360})

V_744 = Vertex(name = 'V_744',
               particles = [ P.Z, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_376})

V_745 = Vertex(name = 'V_745',
               particles = [ P.Z, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_129})

V_746 = Vertex(name = 'V_746',
               particles = [ P.Z, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_590})

V_747 = Vertex(name = 'V_747',
               particles = [ P.Z, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_253})

V_748 = Vertex(name = 'V_748',
               particles = [ P.Z, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_276})

V_749 = Vertex(name = 'V_749',
               particles = [ P.Z, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_620})

V_750 = Vertex(name = 'V_750',
               particles = [ P.Z, P.Z, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_301})

V_751 = Vertex(name = 'V_751',
               particles = [ P.Z, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_344})

V_752 = Vertex(name = 'V_752',
               particles = [ P.Z, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_357})

V_753 = Vertex(name = 'V_753',
               particles = [ P.Z, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_375})

V_754 = Vertex(name = 'V_754',
               particles = [ P.Z, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_128})

V_755 = Vertex(name = 'V_755',
               particles = [ P.Z, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_146})

V_756 = Vertex(name = 'V_756',
               particles = [ P.Z, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_252})

V_757 = Vertex(name = 'V_757',
               particles = [ P.Z, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_274})

V_758 = Vertex(name = 'V_758',
               particles = [ P.Z, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_281})

V_759 = Vertex(name = 'V_759',
               particles = [ P.Z, P.Z, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_300})

V_760 = Vertex(name = 'V_760',
               particles = [ P.Z, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_573})

V_761 = Vertex(name = 'V_761',
               particles = [ P.Z, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_366})

V_762 = Vertex(name = 'V_762',
               particles = [ P.Z, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_381})

V_763 = Vertex(name = 'V_763',
               particles = [ P.Z, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_132})

V_764 = Vertex(name = 'V_764',
               particles = [ P.Z, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_587})

V_765 = Vertex(name = 'V_765',
               particles = [ P.Z, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_260})

V_766 = Vertex(name = 'V_766',
               particles = [ P.Z, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_291})

V_767 = Vertex(name = 'V_767',
               particles = [ P.Z, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_616})

V_768 = Vertex(name = 'V_768',
               particles = [ P.Z, P.Z, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_304})

V_769 = Vertex(name = 'V_769',
               particles = [ P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_571})

V_770 = Vertex(name = 'V_770',
               particles = [ P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_373})

V_771 = Vertex(name = 'V_771',
               particles = [ P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_383})

V_772 = Vertex(name = 'V_772',
               particles = [ P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_135})

V_773 = Vertex(name = 'V_773',
               particles = [ P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_580})

V_774 = Vertex(name = 'V_774',
               particles = [ P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_267})

V_775 = Vertex(name = 'V_775',
               particles = [ P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_296})

V_776 = Vertex(name = 'V_776',
               particles = [ P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_610})

V_777 = Vertex(name = 'V_777',
               particles = [ P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_306})

V_778 = Vertex(name = 'V_778',
               particles = [ P.Z, P.Z, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_375})

V_779 = Vertex(name = 'V_779',
               particles = [ P.Z, P.Z, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_128})

V_780 = Vertex(name = 'V_780',
               particles = [ P.Z, P.Z, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_274})

V_781 = Vertex(name = 'V_781',
               particles = [ P.Z, P.Z, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_579})

V_782 = Vertex(name = 'V_782',
               particles = [ P.Z, P.Z, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_570})

V_783 = Vertex(name = 'V_783',
               particles = [ P.Z, P.Z, P.A01, P.A01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_608})

V_784 = Vertex(name = 'V_784',
               particles = [ P.Z, P.Z, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_579})

V_785 = Vertex(name = 'V_785',
               particles = [ P.Z, P.Z, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_570})

V_786 = Vertex(name = 'V_786',
               particles = [ P.Z, P.Z, P.H01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_608})

V_787 = Vertex(name = 'V_787',
               particles = [ P.Z, P.Z, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_578})

V_788 = Vertex(name = 'V_788',
               particles = [ P.Z, P.Z, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_570})

V_789 = Vertex(name = 'V_789',
               particles = [ P.Z, P.Z, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_607})

V_790 = Vertex(name = 'V_790',
               particles = [ P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_880})

V_791 = Vertex(name = 'V_791',
               particles = [ P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_678})

V_792 = Vertex(name = 'V_792',
               particles = [ P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_856})

V_793 = Vertex(name = 'V_793',
               particles = [ P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_866})

V_794 = Vertex(name = 'V_794',
               particles = [ P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_873})

V_795 = Vertex(name = 'V_795',
               particles = [ P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_804})

V_796 = Vertex(name = 'V_796',
               particles = [ P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_811})

V_797 = Vertex(name = 'V_797',
               particles = [ P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_826})

V_798 = Vertex(name = 'V_798',
               particles = [ P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_838})

V_799 = Vertex(name = 'V_799',
               particles = [ P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_843})

V_800 = Vertex(name = 'V_800',
               particles = [ P.Z, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_878})

V_801 = Vertex(name = 'V_801',
               particles = [ P.Z, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_679})

V_802 = Vertex(name = 'V_802',
               particles = [ P.Z, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_851})

V_803 = Vertex(name = 'V_803',
               particles = [ P.Z, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_861})

V_804 = Vertex(name = 'V_804',
               particles = [ P.Z, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_869})

V_805 = Vertex(name = 'V_805',
               particles = [ P.Z, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_801})

V_806 = Vertex(name = 'V_806',
               particles = [ P.Z, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_809})

V_807 = Vertex(name = 'V_807',
               particles = [ P.Z, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_819})

V_808 = Vertex(name = 'V_808',
               particles = [ P.Z, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_830})

V_809 = Vertex(name = 'V_809',
               particles = [ P.Z, P.Z, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_841})

V_810 = Vertex(name = 'V_810',
               particles = [ P.Z, P.Z, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_742})

V_811 = Vertex(name = 'V_811',
               particles = [ P.Z, P.Z, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_753})

V_812 = Vertex(name = 'V_812',
               particles = [ P.Z, P.Z, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_792})

V_813 = Vertex(name = 'V_813',
               particles = [ P.Z, P.Z, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_762})

V_814 = Vertex(name = 'V_814',
               particles = [ P.Z, P.Z, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_767})

V_815 = Vertex(name = 'V_815',
               particles = [ P.Z, P.Z, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_774})

V_816 = Vertex(name = 'V_816',
               particles = [ P.W__minus__, P.W2__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_520})

V_817 = Vertex(name = 'V_817',
               particles = [ P.W__minus__, P.W2__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_517})

V_818 = Vertex(name = 'V_818',
               particles = [ P.W__minus__, P.W2__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_478})

V_819 = Vertex(name = 'V_819',
               particles = [ P.W2__minus__, P.W2__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_523})

V_820 = Vertex(name = 'V_820',
               particles = [ P.W2__minus__, P.W2__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_147})

V_821 = Vertex(name = 'V_821',
               particles = [ P.W2__minus__, P.W2__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_283})

V_822 = Vertex(name = 'V_822',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_46})

V_823 = Vertex(name = 'V_823',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_525})

V_824 = Vertex(name = 'V_824',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_501})

V_825 = Vertex(name = 'V_825',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_509})

V_826 = Vertex(name = 'V_826',
               particles = [ P.W2__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_520})

V_827 = Vertex(name = 'V_827',
               particles = [ P.W2__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_517})

V_828 = Vertex(name = 'V_828',
               particles = [ P.W2__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV2 ],
               couplings = {(0,0):C.GC_478})

V_829 = Vertex(name = 'V_829',
               particles = [ P.a, P.Z2, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_313})

V_830 = Vertex(name = 'V_830',
               particles = [ P.a, P.Z2, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_356})

V_831 = Vertex(name = 'V_831',
               particles = [ P.a, P.Z2, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_140})

V_832 = Vertex(name = 'V_832',
               particles = [ P.a, P.Z2, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_221})

V_833 = Vertex(name = 'V_833',
               particles = [ P.a, P.Z2, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_234})

V_834 = Vertex(name = 'V_834',
               particles = [ P.a, P.Z2, P.H02, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_279})

V_835 = Vertex(name = 'V_835',
               particles = [ P.a, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_312})

V_836 = Vertex(name = 'V_836',
               particles = [ P.a, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_355})

V_837 = Vertex(name = 'V_837',
               particles = [ P.a, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_139})

V_838 = Vertex(name = 'V_838',
               particles = [ P.a, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_220})

V_839 = Vertex(name = 'V_839',
               particles = [ P.a, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_233})

V_840 = Vertex(name = 'V_840',
               particles = [ P.a, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_278})

V_841 = Vertex(name = 'V_841',
               particles = [ P.a, P.Z2, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_346})

V_842 = Vertex(name = 'V_842',
               particles = [ P.a, P.Z2, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_365})

V_843 = Vertex(name = 'V_843',
               particles = [ P.a, P.Z2, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_143})

V_844 = Vertex(name = 'V_844',
               particles = [ P.a, P.Z2, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_254})

V_845 = Vertex(name = 'V_845',
               particles = [ P.a, P.Z2, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_257})

V_846 = Vertex(name = 'V_846',
               particles = [ P.a, P.Z2, P.H, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_292})

V_847 = Vertex(name = 'V_847',
               particles = [ P.a, P.Z2, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_349})

V_848 = Vertex(name = 'V_848',
               particles = [ P.a, P.Z2, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_371})

V_849 = Vertex(name = 'V_849',
               particles = [ P.a, P.Z2, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_145})

V_850 = Vertex(name = 'V_850',
               particles = [ P.a, P.Z2, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_261})

V_851 = Vertex(name = 'V_851',
               particles = [ P.a, P.Z2, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_264})

V_852 = Vertex(name = 'V_852',
               particles = [ P.a, P.Z2, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_298})

V_853 = Vertex(name = 'V_853',
               particles = [ P.a, P.Z2, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_355})

V_854 = Vertex(name = 'V_854',
               particles = [ P.a, P.Z2, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_107})

V_855 = Vertex(name = 'V_855',
               particles = [ P.a, P.Z2, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_118})

V_856 = Vertex(name = 'V_856',
               particles = [ P.a, P.Z2, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_388})

V_857 = Vertex(name = 'V_857',
               particles = [ P.a, P.Z2, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_220})

V_858 = Vertex(name = 'V_858',
               particles = [ P.a, P.Z2, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_355})

V_859 = Vertex(name = 'V_859',
               particles = [ P.a, P.Z2, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_107})

V_860 = Vertex(name = 'V_860',
               particles = [ P.a, P.Z2, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_118})

V_861 = Vertex(name = 'V_861',
               particles = [ P.a, P.Z2, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_388})

V_862 = Vertex(name = 'V_862',
               particles = [ P.a, P.Z2, P.H03, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_220})

V_863 = Vertex(name = 'V_863',
               particles = [ P.a, P.Z2, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_355})

V_864 = Vertex(name = 'V_864',
               particles = [ P.a, P.Z2, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_220})

V_865 = Vertex(name = 'V_865',
               particles = [ P.a, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_355})

V_866 = Vertex(name = 'V_866',
               particles = [ P.a, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_107})

V_867 = Vertex(name = 'V_867',
               particles = [ P.a, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_117})

V_868 = Vertex(name = 'V_868',
               particles = [ P.a, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_387})

V_869 = Vertex(name = 'V_869',
               particles = [ P.a, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_220})

V_870 = Vertex(name = 'V_870',
               particles = [ P.a, P.Z2, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_577})

V_871 = Vertex(name = 'V_871',
               particles = [ P.a, P.Z2, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_600})

V_872 = Vertex(name = 'V_872',
               particles = [ P.a, P.Z2, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_791})

V_873 = Vertex(name = 'V_873',
               particles = [ P.a, P.Z2, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_756})

V_874 = Vertex(name = 'V_874',
               particles = [ P.a, P.Z2, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_759})

V_875 = Vertex(name = 'V_875',
               particles = [ P.a, P.Z2, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_795})

V_876 = Vertex(name = 'V_876',
               particles = [ P.a, P.Z2, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_765})

V_877 = Vertex(name = 'V_877',
               particles = [ P.a, P.Z2, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_848})

V_878 = Vertex(name = 'V_878',
               particles = [ P.a, P.Z2, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_859})

V_879 = Vertex(name = 'V_879',
               particles = [ P.a, P.Z2, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_806})

V_880 = Vertex(name = 'V_880',
               particles = [ P.a, P.Z2, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_813})

V_881 = Vertex(name = 'V_881',
               particles = [ P.a, P.Z2, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_816})

V_882 = Vertex(name = 'V_882',
               particles = [ P.a, P.Z2, P.H02 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_832})

V_883 = Vertex(name = 'V_883',
               particles = [ P.a, P.Z2, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_853})

V_884 = Vertex(name = 'V_884',
               particles = [ P.a, P.Z2, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_865})

V_885 = Vertex(name = 'V_885',
               particles = [ P.a, P.Z2, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_808})

V_886 = Vertex(name = 'V_886',
               particles = [ P.a, P.Z2, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_820})

V_887 = Vertex(name = 'V_887',
               particles = [ P.a, P.Z2, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_823})

V_888 = Vertex(name = 'V_888',
               particles = [ P.a, P.Z2, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_839})

V_889 = Vertex(name = 'V_889',
               particles = [ P.Z2, P.A01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_565})

V_890 = Vertex(name = 'V_890',
               particles = [ P.Z2, P.A01, P.H01 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_594})

V_891 = Vertex(name = 'V_891',
               particles = [ P.Z2, P.A02, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_60})

V_892 = Vertex(name = 'V_892',
               particles = [ P.Z2, P.A02, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_76})

V_893 = Vertex(name = 'V_893',
               particles = [ P.Z2, P.A02, P.H03 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_236})

V_894 = Vertex(name = 'V_894',
               particles = [ P.Z2, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_79})

V_895 = Vertex(name = 'V_895',
               particles = [ P.Z2, P.HM1, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_238})

V_896 = Vertex(name = 'V_896',
               particles = [ P.Z2, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_566})

V_897 = Vertex(name = 'V_897',
               particles = [ P.Z2, P.HM2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_593})

V_898 = Vertex(name = 'V_898',
               particles = [ P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_62})

V_899 = Vertex(name = 'V_899',
               particles = [ P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_79})

V_900 = Vertex(name = 'V_900',
               particles = [ P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_238})

V_901 = Vertex(name = 'V_901',
               particles = [ P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_79})

V_902 = Vertex(name = 'V_902',
               particles = [ P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_124})

V_903 = Vertex(name = 'V_903',
               particles = [ P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_183})

V_904 = Vertex(name = 'V_904',
               particles = [ P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1 ],
               couplings = {(0,0):C.GC_238})

V_905 = Vertex(name = 'V_905',
               particles = [ P.W__minus__, P.Z2, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_68})

V_906 = Vertex(name = 'V_906',
               particles = [ P.W__minus__, P.Z2, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_90})

V_907 = Vertex(name = 'V_907',
               particles = [ P.W__minus__, P.Z2, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_246})

V_908 = Vertex(name = 'V_908',
               particles = [ P.W__minus__, P.Z2, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_67})

V_909 = Vertex(name = 'V_909',
               particles = [ P.W__minus__, P.Z2, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_89})

V_910 = Vertex(name = 'V_910',
               particles = [ P.W__minus__, P.Z2, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_245})

V_911 = Vertex(name = 'V_911',
               particles = [ P.W__minus__, P.Z2, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_65})

V_912 = Vertex(name = 'V_912',
               particles = [ P.W__minus__, P.Z2, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_87})

V_913 = Vertex(name = 'V_913',
               particles = [ P.W__minus__, P.Z2, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_243})

V_914 = Vertex(name = 'V_914',
               particles = [ P.W__minus__, P.Z2, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_569})

V_915 = Vertex(name = 'V_915',
               particles = [ P.W__minus__, P.Z2, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_595})

V_916 = Vertex(name = 'V_916',
               particles = [ P.W__minus__, P.Z2, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_720})

V_917 = Vertex(name = 'V_917',
               particles = [ P.W__minus__, P.Z2, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_568})

V_918 = Vertex(name = 'V_918',
               particles = [ P.W__minus__, P.Z2, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_596})

V_919 = Vertex(name = 'V_919',
               particles = [ P.W__minus__, P.Z2, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_721})

V_920 = Vertex(name = 'V_920',
               particles = [ P.W__minus__, P.Z2, P.H, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_633})

V_921 = Vertex(name = 'V_921',
               particles = [ P.W__minus__, P.Z2, P.H02, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_639})

V_922 = Vertex(name = 'V_922',
               particles = [ P.W__minus__, P.Z2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_687})

V_923 = Vertex(name = 'V_923',
               particles = [ P.W__minus__, P.Z2, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_747})

V_924 = Vertex(name = 'V_924',
               particles = [ P.W__minus__, P.Z2, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_750})

V_925 = Vertex(name = 'V_925',
               particles = [ P.W__minus__, P.Z2, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_771})

V_926 = Vertex(name = 'V_926',
               particles = [ P.W2__minus__, P.Z2, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_445})

V_927 = Vertex(name = 'V_927',
               particles = [ P.W2__minus__, P.Z2, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_452})

V_928 = Vertex(name = 'V_928',
               particles = [ P.W2__minus__, P.Z2, P.A02, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_475})

V_929 = Vertex(name = 'V_929',
               particles = [ P.W2__minus__, P.Z2, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_444})

V_930 = Vertex(name = 'V_930',
               particles = [ P.W2__minus__, P.Z2, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_451})

V_931 = Vertex(name = 'V_931',
               particles = [ P.W2__minus__, P.Z2, P.H03, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_474})

V_932 = Vertex(name = 'V_932',
               particles = [ P.W2__minus__, P.Z2, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_442})

V_933 = Vertex(name = 'V_933',
               particles = [ P.W2__minus__, P.Z2, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_449})

V_934 = Vertex(name = 'V_934',
               particles = [ P.W2__minus__, P.Z2, P.HM1, P.HL__plus____plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_472})

V_935 = Vertex(name = 'V_935',
               particles = [ P.W2__minus__, P.Z2, P.H, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_539})

V_936 = Vertex(name = 'V_936',
               particles = [ P.W2__minus__, P.Z2, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_651})

V_937 = Vertex(name = 'V_937',
               particles = [ P.W2__minus__, P.Z2, P.A01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_650})

V_938 = Vertex(name = 'V_938',
               particles = [ P.W2__minus__, P.Z2, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_652})

V_939 = Vertex(name = 'V_939',
               particles = [ P.W2__minus__, P.Z2, P.H01, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_649})

V_940 = Vertex(name = 'V_940',
               particles = [ P.W2__minus__, P.Z2, P.H02, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_548})

V_941 = Vertex(name = 'V_941',
               particles = [ P.W2__minus__, P.Z2, P.HP2 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_675})

V_942 = Vertex(name = 'V_942',
               particles = [ P.W2__minus__, P.Z2, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_780})

V_943 = Vertex(name = 'V_943',
               particles = [ P.W2__minus__, P.Z2, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_781})

V_944 = Vertex(name = 'V_944',
               particles = [ P.W2__minus__, P.Z2, P.HP1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_786})

V_945 = Vertex(name = 'V_945',
               particles = [ P.W2__plus__, P.Z2, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_443})

V_946 = Vertex(name = 'V_946',
               particles = [ P.W2__plus__, P.Z2, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_450})

V_947 = Vertex(name = 'V_947',
               particles = [ P.W2__plus__, P.Z2, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_473})

V_948 = Vertex(name = 'V_948',
               particles = [ P.W2__plus__, P.Z2, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_444})

V_949 = Vertex(name = 'V_949',
               particles = [ P.W2__plus__, P.Z2, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_451})

V_950 = Vertex(name = 'V_950',
               particles = [ P.W2__plus__, P.Z2, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_474})

V_951 = Vertex(name = 'V_951',
               particles = [ P.W2__plus__, P.Z2, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_442})

V_952 = Vertex(name = 'V_952',
               particles = [ P.W2__plus__, P.Z2, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_449})

V_953 = Vertex(name = 'V_953',
               particles = [ P.W2__plus__, P.Z2, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_472})

V_954 = Vertex(name = 'V_954',
               particles = [ P.W2__plus__, P.Z2, P.H, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_539})

V_955 = Vertex(name = 'V_955',
               particles = [ P.W2__plus__, P.Z2, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_653})

V_956 = Vertex(name = 'V_956',
               particles = [ P.W2__plus__, P.Z2, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_648})

V_957 = Vertex(name = 'V_957',
               particles = [ P.W2__plus__, P.Z2, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_652})

V_958 = Vertex(name = 'V_958',
               particles = [ P.W2__plus__, P.Z2, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_649})

V_959 = Vertex(name = 'V_959',
               particles = [ P.W2__plus__, P.Z2, P.H02, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_548})

V_960 = Vertex(name = 'V_960',
               particles = [ P.W2__plus__, P.Z2, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_675})

V_961 = Vertex(name = 'V_961',
               particles = [ P.W2__plus__, P.Z2, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_780})

V_962 = Vertex(name = 'V_962',
               particles = [ P.W2__plus__, P.Z2, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_781})

V_963 = Vertex(name = 'V_963',
               particles = [ P.W2__plus__, P.Z2, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_786})

V_964 = Vertex(name = 'V_964',
               particles = [ P.a, P.W__minus__, P.W2__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_519})

V_965 = Vertex(name = 'V_965',
               particles = [ P.a, P.W__minus__, P.W2__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_471})

V_966 = Vertex(name = 'V_966',
               particles = [ P.a, P.W2__minus__, P.W2__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_141})

V_967 = Vertex(name = 'V_967',
               particles = [ P.a, P.W2__minus__, P.W2__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_235})

V_968 = Vertex(name = 'V_968',
               particles = [ P.a, P.W2__minus__, P.W2__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_495})

V_969 = Vertex(name = 'V_969',
               particles = [ P.W__plus__, P.Z2, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_66})

V_970 = Vertex(name = 'V_970',
               particles = [ P.W__plus__, P.Z2, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_88})

V_971 = Vertex(name = 'V_971',
               particles = [ P.W__plus__, P.Z2, P.A02, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_244})

V_972 = Vertex(name = 'V_972',
               particles = [ P.W__plus__, P.Z2, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_67})

V_973 = Vertex(name = 'V_973',
               particles = [ P.W__plus__, P.Z2, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_89})

V_974 = Vertex(name = 'V_974',
               particles = [ P.W__plus__, P.Z2, P.H03, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_245})

V_975 = Vertex(name = 'V_975',
               particles = [ P.W__plus__, P.Z2, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_65})

V_976 = Vertex(name = 'V_976',
               particles = [ P.W__plus__, P.Z2, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_87})

V_977 = Vertex(name = 'V_977',
               particles = [ P.W__plus__, P.Z2, P.HP1, P.HL__minus____minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_243})

V_978 = Vertex(name = 'V_978',
               particles = [ P.W__plus__, P.Z2, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_567})

V_979 = Vertex(name = 'V_979',
               particles = [ P.W__plus__, P.Z2, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_597})

V_980 = Vertex(name = 'V_980',
               particles = [ P.W__plus__, P.Z2, P.A01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_722})

V_981 = Vertex(name = 'V_981',
               particles = [ P.W__plus__, P.Z2, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_568})

V_982 = Vertex(name = 'V_982',
               particles = [ P.W__plus__, P.Z2, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_596})

V_983 = Vertex(name = 'V_983',
               particles = [ P.W__plus__, P.Z2, P.H01, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_721})

V_984 = Vertex(name = 'V_984',
               particles = [ P.W__plus__, P.Z2, P.H, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_633})

V_985 = Vertex(name = 'V_985',
               particles = [ P.W__plus__, P.Z2, P.H02, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_639})

V_986 = Vertex(name = 'V_986',
               particles = [ P.W__plus__, P.Z2, P.HM2 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_687})

V_987 = Vertex(name = 'V_987',
               particles = [ P.W__plus__, P.Z2, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_747})

V_988 = Vertex(name = 'V_988',
               particles = [ P.W__plus__, P.Z2, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_750})

V_989 = Vertex(name = 'V_989',
               particles = [ P.W__plus__, P.Z2, P.HM1 ],
               color = [ '1' ],
               lorentz = [ L.VVS1 ],
               couplings = {(0,0):C.GC_771})

V_990 = Vertex(name = 'V_990',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_108})

V_991 = Vertex(name = 'V_991',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_500})

V_992 = Vertex(name = 'V_992',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_505})

V_993 = Vertex(name = 'V_993',
               particles = [ P.a, P.W2__minus__, P.W__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_519})

V_994 = Vertex(name = 'V_994',
               particles = [ P.a, P.W2__minus__, P.W__plus__, P.Z2 ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_471})

V_995 = Vertex(name = 'V_995',
               particles = [ P.Z, P.Z2, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_64})

V_996 = Vertex(name = 'V_996',
               particles = [ P.Z, P.Z2, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_86})

V_997 = Vertex(name = 'V_997',
               particles = [ P.Z, P.Z2, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_109})

V_998 = Vertex(name = 'V_998',
               particles = [ P.Z, P.Z2, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_148})

V_999 = Vertex(name = 'V_999',
               particles = [ P.Z, P.Z2, P.A02, P.A02 ],
               color = [ '1' ],
               lorentz = [ L.VVSS1 ],
               couplings = {(0,0):C.GC_242})

V_1000 = Vertex(name = 'V_1000',
                particles = [ P.Z, P.Z2, P.A02, P.A02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_268})

V_1001 = Vertex(name = 'V_1001',
                particles = [ P.Z, P.Z2, P.A02, P.A02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_284})

V_1002 = Vertex(name = 'V_1002',
                particles = [ P.Z, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_591})

V_1003 = Vertex(name = 'V_1003',
                particles = [ P.Z, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_354})

V_1004 = Vertex(name = 'V_1004',
                particles = [ P.Z, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_362})

V_1005 = Vertex(name = 'V_1005',
                particles = [ P.Z, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_149})

V_1006 = Vertex(name = 'V_1006',
                particles = [ P.Z, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_390})

V_1007 = Vertex(name = 'V_1007',
                particles = [ P.Z, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_269})

V_1008 = Vertex(name = 'V_1008',
                particles = [ P.Z, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_618})

V_1009 = Vertex(name = 'V_1009',
                particles = [ P.Z, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_285})

V_1010 = Vertex(name = 'V_1010',
                particles = [ P.Z, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_621})

V_1011 = Vertex(name = 'V_1011',
                particles = [ P.Z, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_64})

V_1012 = Vertex(name = 'V_1012',
                particles = [ P.Z, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_86})

V_1013 = Vertex(name = 'V_1013',
                particles = [ P.Z, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_109})

V_1014 = Vertex(name = 'V_1014',
                particles = [ P.Z, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_148})

V_1015 = Vertex(name = 'V_1015',
                particles = [ P.Z, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_242})

V_1016 = Vertex(name = 'V_1016',
                particles = [ P.Z, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_268})

V_1017 = Vertex(name = 'V_1017',
                particles = [ P.Z, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_284})

V_1018 = Vertex(name = 'V_1018',
                particles = [ P.Z, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_64})

V_1019 = Vertex(name = 'V_1019',
                particles = [ P.Z, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_85})

V_1020 = Vertex(name = 'V_1020',
                particles = [ P.Z, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_110})

V_1021 = Vertex(name = 'V_1021',
                particles = [ P.Z, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_148})

V_1022 = Vertex(name = 'V_1022',
                particles = [ P.Z, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_241})

V_1023 = Vertex(name = 'V_1023',
                particles = [ P.Z, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_268})

V_1024 = Vertex(name = 'V_1024',
                particles = [ P.Z, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_284})

V_1025 = Vertex(name = 'V_1025',
                particles = [ P.Z, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_377})

V_1026 = Vertex(name = 'V_1026',
                particles = [ P.Z, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_353})

V_1027 = Vertex(name = 'V_1027',
                particles = [ P.Z, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_361})

V_1028 = Vertex(name = 'V_1028',
                particles = [ P.Z, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_148})

V_1029 = Vertex(name = 'V_1029',
                particles = [ P.Z, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_389})

V_1030 = Vertex(name = 'V_1030',
                particles = [ P.Z, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_268})

V_1031 = Vertex(name = 'V_1031',
                particles = [ P.Z, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_272})

V_1032 = Vertex(name = 'V_1032',
                particles = [ P.Z, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_284})

V_1033 = Vertex(name = 'V_1033',
                particles = [ P.Z, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_286})

V_1034 = Vertex(name = 'V_1034',
                particles = [ P.Z, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_588})

V_1035 = Vertex(name = 'V_1035',
                particles = [ P.Z, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_364})

V_1036 = Vertex(name = 'V_1036',
                particles = [ P.Z, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_368})

V_1037 = Vertex(name = 'V_1037',
                particles = [ P.Z, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_152})

V_1038 = Vertex(name = 'V_1038',
                particles = [ P.Z, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_391})

V_1039 = Vertex(name = 'V_1039',
                particles = [ P.Z, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_288})

V_1040 = Vertex(name = 'V_1040',
                particles = [ P.Z, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_614})

V_1041 = Vertex(name = 'V_1041',
                particles = [ P.Z, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_293})

V_1042 = Vertex(name = 'V_1042',
                particles = [ P.Z, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_617})

V_1043 = Vertex(name = 'V_1043',
                particles = [ P.Z, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_583})

V_1044 = Vertex(name = 'V_1044',
                particles = [ P.Z, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_370})

V_1045 = Vertex(name = 'V_1045',
                particles = [ P.Z, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_374})

V_1046 = Vertex(name = 'V_1046',
                particles = [ P.Z, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_153})

V_1047 = Vertex(name = 'V_1047',
                particles = [ P.Z, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_392})

V_1048 = Vertex(name = 'V_1048',
                particles = [ P.Z, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_294})

V_1049 = Vertex(name = 'V_1049',
                particles = [ P.Z, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_604})

V_1050 = Vertex(name = 'V_1050',
                particles = [ P.Z, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_299})

V_1051 = Vertex(name = 'V_1051',
                particles = [ P.Z, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_613})

V_1052 = Vertex(name = 'V_1052',
                particles = [ P.Z, P.Z2, P.HM1, P.HP1 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_148})

V_1053 = Vertex(name = 'V_1053',
                particles = [ P.Z, P.Z2, P.HM1, P.HP1 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_268})

V_1054 = Vertex(name = 'V_1054',
                particles = [ P.Z, P.Z2, P.HM1, P.HP1 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_284})

V_1055 = Vertex(name = 'V_1055',
                particles = [ P.Z, P.Z2, P.A01, P.A01 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_582})

V_1056 = Vertex(name = 'V_1056',
                particles = [ P.Z, P.Z2, P.A01, P.A01 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_602})

V_1057 = Vertex(name = 'V_1057',
                particles = [ P.Z, P.Z2, P.A01, P.A01 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_612})

V_1058 = Vertex(name = 'V_1058',
                particles = [ P.Z, P.Z2, P.H01, P.H01 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_582})

V_1059 = Vertex(name = 'V_1059',
                particles = [ P.Z, P.Z2, P.H01, P.H01 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_602})

V_1060 = Vertex(name = 'V_1060',
                particles = [ P.Z, P.Z2, P.H01, P.H01 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_612})

V_1061 = Vertex(name = 'V_1061',
                particles = [ P.Z, P.Z2, P.HM2, P.HP2 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_581})

V_1062 = Vertex(name = 'V_1062',
                particles = [ P.Z, P.Z2, P.HM2, P.HP2 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_603})

V_1063 = Vertex(name = 'V_1063',
                particles = [ P.Z, P.Z2, P.HM2, P.HP2 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_611})

V_1064 = Vertex(name = 'V_1064',
                particles = [ P.Z, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_882})

V_1065 = Vertex(name = 'V_1065',
                particles = [ P.Z, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_874})

V_1066 = Vertex(name = 'V_1066',
                particles = [ P.Z, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_682})

V_1067 = Vertex(name = 'V_1067',
                particles = [ P.Z, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_864})

V_1068 = Vertex(name = 'V_1068',
                particles = [ P.Z, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_868})

V_1069 = Vertex(name = 'V_1069',
                particles = [ P.Z, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_812})

V_1070 = Vertex(name = 'V_1070',
                particles = [ P.Z, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_884})

V_1071 = Vertex(name = 'V_1071',
                particles = [ P.Z, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_834})

V_1072 = Vertex(name = 'V_1072',
                particles = [ P.Z, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_836})

V_1073 = Vertex(name = 'V_1073',
                particles = [ P.Z, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_840})

V_1074 = Vertex(name = 'V_1074',
                particles = [ P.Z, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_879})

V_1075 = Vertex(name = 'V_1075',
                particles = [ P.Z, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_870})

V_1076 = Vertex(name = 'V_1076',
                particles = [ P.Z, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_683})

V_1077 = Vertex(name = 'V_1077',
                particles = [ P.Z, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_858})

V_1078 = Vertex(name = 'V_1078',
                particles = [ P.Z, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_862})

V_1079 = Vertex(name = 'V_1079',
                particles = [ P.Z, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_810})

V_1080 = Vertex(name = 'V_1080',
                particles = [ P.Z, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_883})

V_1081 = Vertex(name = 'V_1081',
                particles = [ P.Z, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_827})

V_1082 = Vertex(name = 'V_1082',
                particles = [ P.Z, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_829})

V_1083 = Vertex(name = 'V_1083',
                particles = [ P.Z, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_833})

V_1084 = Vertex(name = 'V_1084',
                particles = [ P.Z, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_746})

V_1085 = Vertex(name = 'V_1085',
                particles = [ P.Z, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_749})

V_1086 = Vertex(name = 'V_1086',
                particles = [ P.Z, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_757})

V_1087 = Vertex(name = 'V_1087',
                particles = [ P.Z, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_763})

V_1088 = Vertex(name = 'V_1088',
                particles = [ P.Z, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_770})

V_1089 = Vertex(name = 'V_1089',
                particles = [ P.Z, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_773})

V_1090 = Vertex(name = 'V_1090',
                particles = [ P.Z, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_776})

V_1091 = Vertex(name = 'V_1091',
                particles = [ P.W__minus__, P.W2__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_521})

V_1092 = Vertex(name = 'V_1092',
                particles = [ P.W__minus__, P.W2__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_477})

V_1093 = Vertex(name = 'V_1093',
                particles = [ P.W__minus__, P.W2__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_480})

V_1094 = Vertex(name = 'V_1094',
                particles = [ P.W2__minus__, P.W2__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_378})

V_1095 = Vertex(name = 'V_1095',
                particles = [ P.W2__minus__, P.W2__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_273})

V_1096 = Vertex(name = 'V_1096',
                particles = [ P.W2__minus__, P.W2__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_287})

V_1097 = Vertex(name = 'V_1097',
                particles = [ P.W2__minus__, P.W2__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_490})

V_1098 = Vertex(name = 'V_1098',
                particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_69})

V_1099 = Vertex(name = 'V_1099',
                particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_526})

V_1100 = Vertex(name = 'V_1100',
                particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_507})

V_1101 = Vertex(name = 'V_1101',
                particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_510})

V_1102 = Vertex(name = 'V_1102',
                particles = [ P.W2__minus__, P.W__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_521})

V_1103 = Vertex(name = 'V_1103',
                particles = [ P.W2__minus__, P.W__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_477})

V_1104 = Vertex(name = 'V_1104',
                particles = [ P.W2__minus__, P.W__plus__, P.Z, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_480})

V_1105 = Vertex(name = 'V_1105',
                particles = [ P.Z2, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_574})

V_1106 = Vertex(name = 'V_1106',
                particles = [ P.Z2, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_359})

V_1107 = Vertex(name = 'V_1107',
                particles = [ P.Z2, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_112})

V_1108 = Vertex(name = 'V_1108',
                particles = [ P.Z2, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_380})

V_1109 = Vertex(name = 'V_1109',
                particles = [ P.Z2, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_592})

V_1110 = Vertex(name = 'V_1110',
                particles = [ P.Z2, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_228})

V_1111 = Vertex(name = 'V_1111',
                particles = [ P.Z2, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_277})

V_1112 = Vertex(name = 'V_1112',
                particles = [ P.Z2, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_619})

V_1113 = Vertex(name = 'V_1113',
                particles = [ P.Z2, P.Z2, P.H02, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_303})

V_1114 = Vertex(name = 'V_1114',
                particles = [ P.Z2, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_314})

V_1115 = Vertex(name = 'V_1115',
                particles = [ P.Z2, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_358})

V_1116 = Vertex(name = 'V_1116',
                particles = [ P.Z2, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_111})

V_1117 = Vertex(name = 'V_1117',
                particles = [ P.Z2, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_379})

V_1118 = Vertex(name = 'V_1118',
                particles = [ P.Z2, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_150})

V_1119 = Vertex(name = 'V_1119',
                particles = [ P.Z2, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_227})

V_1120 = Vertex(name = 'V_1120',
                particles = [ P.Z2, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_275})

V_1121 = Vertex(name = 'V_1121',
                particles = [ P.Z2, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_280})

V_1122 = Vertex(name = 'V_1122',
                particles = [ P.Z2, P.Z2, P.HR__minus____minus__, P.HR__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_302})

V_1123 = Vertex(name = 'V_1123',
                particles = [ P.Z2, P.Z2, P.A02, P.A02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_70})

V_1124 = Vertex(name = 'V_1124',
                particles = [ P.Z2, P.Z2, P.A02, P.A02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_102})

V_1125 = Vertex(name = 'V_1125',
                particles = [ P.Z2, P.Z2, P.A02, P.A02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_111})

V_1126 = Vertex(name = 'V_1126',
                particles = [ P.Z2, P.Z2, P.A02, P.A02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_379})

V_1127 = Vertex(name = 'V_1127',
                particles = [ P.Z2, P.Z2, P.A02, P.A02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_251})

V_1128 = Vertex(name = 'V_1128',
                particles = [ P.Z2, P.Z2, P.A02, P.A02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_275})

V_1129 = Vertex(name = 'V_1129',
                particles = [ P.Z2, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_70})

V_1130 = Vertex(name = 'V_1130',
                particles = [ P.Z2, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_102})

V_1131 = Vertex(name = 'V_1131',
                particles = [ P.Z2, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_111})

V_1132 = Vertex(name = 'V_1132',
                particles = [ P.Z2, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_379})

V_1133 = Vertex(name = 'V_1133',
                particles = [ P.Z2, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_251})

V_1134 = Vertex(name = 'V_1134',
                particles = [ P.Z2, P.Z2, P.H03, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_275})

V_1135 = Vertex(name = 'V_1135',
                particles = [ P.Z2, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_70})

V_1136 = Vertex(name = 'V_1136',
                particles = [ P.Z2, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_101})

V_1137 = Vertex(name = 'V_1137',
                particles = [ P.Z2, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_111})

V_1138 = Vertex(name = 'V_1138',
                particles = [ P.Z2, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_379})

V_1139 = Vertex(name = 'V_1139',
                particles = [ P.Z2, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_250})

V_1140 = Vertex(name = 'V_1140',
                particles = [ P.Z2, P.Z2, P.HL__minus____minus__, P.HL__plus____plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_275})

V_1141 = Vertex(name = 'V_1141',
                particles = [ P.Z2, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_572})

V_1142 = Vertex(name = 'V_1142',
                particles = [ P.Z2, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_367})

V_1143 = Vertex(name = 'V_1143',
                particles = [ P.Z2, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_130})

V_1144 = Vertex(name = 'V_1144',
                particles = [ P.Z2, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_382})

V_1145 = Vertex(name = 'V_1145',
                particles = [ P.Z2, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_589})

V_1146 = Vertex(name = 'V_1146',
                particles = [ P.Z2, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_256})

V_1147 = Vertex(name = 'V_1147',
                particles = [ P.Z2, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_290})

V_1148 = Vertex(name = 'V_1148',
                particles = [ P.Z2, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_615})

V_1149 = Vertex(name = 'V_1149',
                particles = [ P.Z2, P.Z2, P.H, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_305})

V_1150 = Vertex(name = 'V_1150',
                particles = [ P.Z2, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_561})

V_1151 = Vertex(name = 'V_1151',
                particles = [ P.Z2, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_372})

V_1152 = Vertex(name = 'V_1152',
                particles = [ P.Z2, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_133})

V_1153 = Vertex(name = 'V_1153',
                particles = [ P.Z2, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_384})

V_1154 = Vertex(name = 'V_1154',
                particles = [ P.Z2, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_586})

V_1155 = Vertex(name = 'V_1155',
                particles = [ P.Z2, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_263})

V_1156 = Vertex(name = 'V_1156',
                particles = [ P.Z2, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_297})

V_1157 = Vertex(name = 'V_1157',
                particles = [ P.Z2, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_609})

V_1158 = Vertex(name = 'V_1158',
                particles = [ P.Z2, P.Z2, P.H, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_307})

V_1159 = Vertex(name = 'V_1159',
                particles = [ P.Z2, P.Z2, P.HM1, P.HP1 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_111})

V_1160 = Vertex(name = 'V_1160',
                particles = [ P.Z2, P.Z2, P.HM1, P.HP1 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_379})

V_1161 = Vertex(name = 'V_1161',
                particles = [ P.Z2, P.Z2, P.HM1, P.HP1 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_275})

V_1162 = Vertex(name = 'V_1162',
                particles = [ P.Z2, P.Z2, P.A01, P.A01 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_560})

V_1163 = Vertex(name = 'V_1163',
                particles = [ P.Z2, P.Z2, P.A01, P.A01 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_585})

V_1164 = Vertex(name = 'V_1164',
                particles = [ P.Z2, P.Z2, P.A01, P.A01 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_605})

V_1165 = Vertex(name = 'V_1165',
                particles = [ P.Z2, P.Z2, P.H01, P.H01 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_560})

V_1166 = Vertex(name = 'V_1166',
                particles = [ P.Z2, P.Z2, P.H01, P.H01 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_585})

V_1167 = Vertex(name = 'V_1167',
                particles = [ P.Z2, P.Z2, P.H01, P.H01 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_605})

V_1168 = Vertex(name = 'V_1168',
                particles = [ P.Z2, P.Z2, P.HM2, P.HP2 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_560})

V_1169 = Vertex(name = 'V_1169',
                particles = [ P.Z2, P.Z2, P.HM2, P.HP2 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_584})

V_1170 = Vertex(name = 'V_1170',
                particles = [ P.Z2, P.Z2, P.HM2, P.HP2 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_606})

V_1171 = Vertex(name = 'V_1171',
                particles = [ P.Z2, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_881})

V_1172 = Vertex(name = 'V_1172',
                particles = [ P.Z2, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_680})

V_1173 = Vertex(name = 'V_1173',
                particles = [ P.Z2, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_876})

V_1174 = Vertex(name = 'V_1174',
                particles = [ P.Z2, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_854})

V_1175 = Vertex(name = 'V_1175',
                particles = [ P.Z2, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_867})

V_1176 = Vertex(name = 'V_1176',
                particles = [ P.Z2, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_802})

V_1177 = Vertex(name = 'V_1177',
                particles = [ P.Z2, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_875})

V_1178 = Vertex(name = 'V_1178',
                particles = [ P.Z2, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_822})

V_1179 = Vertex(name = 'V_1179',
                particles = [ P.Z2, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_837})

V_1180 = Vertex(name = 'V_1180',
                particles = [ P.Z2, P.Z2, P.H ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_844})

V_1181 = Vertex(name = 'V_1181',
                particles = [ P.Z2, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_877})

V_1182 = Vertex(name = 'V_1182',
                particles = [ P.Z2, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_681})

V_1183 = Vertex(name = 'V_1183',
                particles = [ P.Z2, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_872})

V_1184 = Vertex(name = 'V_1184',
                particles = [ P.Z2, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_849})

V_1185 = Vertex(name = 'V_1185',
                particles = [ P.Z2, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_860})

V_1186 = Vertex(name = 'V_1186',
                particles = [ P.Z2, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_799})

V_1187 = Vertex(name = 'V_1187',
                particles = [ P.Z2, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_871})

V_1188 = Vertex(name = 'V_1188',
                particles = [ P.Z2, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_815})

V_1189 = Vertex(name = 'V_1189',
                particles = [ P.Z2, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_831})

V_1190 = Vertex(name = 'V_1190',
                particles = [ P.Z2, P.Z2, P.H02 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_842})

V_1191 = Vertex(name = 'V_1191',
                particles = [ P.Z2, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_748})

V_1192 = Vertex(name = 'V_1192',
                particles = [ P.Z2, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_754})

V_1193 = Vertex(name = 'V_1193',
                particles = [ P.Z2, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_758})

V_1194 = Vertex(name = 'V_1194',
                particles = [ P.Z2, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_793})

V_1195 = Vertex(name = 'V_1195',
                particles = [ P.Z2, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_772})

V_1196 = Vertex(name = 'V_1196',
                particles = [ P.Z2, P.Z2, P.H03 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_775})

V_1197 = Vertex(name = 'V_1197',
                particles = [ P.W__minus__, P.W2__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_515})

V_1198 = Vertex(name = 'V_1198',
                particles = [ P.W__minus__, P.W2__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_522})

V_1199 = Vertex(name = 'V_1199',
                particles = [ P.W__minus__, P.W2__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_479})

V_1200 = Vertex(name = 'V_1200',
                particles = [ P.W2__minus__, P.W2__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_315})

V_1201 = Vertex(name = 'V_1201',
                particles = [ P.W2__minus__, P.W2__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_151})

V_1202 = Vertex(name = 'V_1202',
                particles = [ P.W2__minus__, P.W2__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_282})

V_1203 = Vertex(name = 'V_1203',
                particles = [ P.W2__minus__, P.W2__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_491})

V_1204 = Vertex(name = 'V_1204',
                particles = [ P.W__minus__, P.W__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_524})

V_1205 = Vertex(name = 'V_1205',
                particles = [ P.W__minus__, P.W__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_502})

V_1206 = Vertex(name = 'V_1206',
                particles = [ P.W__minus__, P.W__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_508})

V_1207 = Vertex(name = 'V_1207',
                particles = [ P.W2__minus__, P.W__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_515})

V_1208 = Vertex(name = 'V_1208',
                particles = [ P.W2__minus__, P.W__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_522})

V_1209 = Vertex(name = 'V_1209',
                particles = [ P.W2__minus__, P.W__plus__, P.Z2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.VVVV2 ],
                couplings = {(0,0):C.GC_479})

V_1210 = Vertex(name = 'V_1210',
                particles = [ P.e__plus__, P.e__minus__, P.a ],
                color = [ '1' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_83})

V_1211 = Vertex(name = 'V_1211',
                particles = [ P.mu__plus__, P.mu__minus__, P.a ],
                color = [ '1' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_83})

V_1212 = Vertex(name = 'V_1212',
                particles = [ P.ta__plus__, P.ta__minus__, P.a ],
                color = [ '1' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_83})

V_1213 = Vertex(name = 'V_1213',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_82})

V_1214 = Vertex(name = 'V_1214',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_82})

V_1215 = Vertex(name = 'V_1215',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_82})

V_1216 = Vertex(name = 'V_1216',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV5 ],
                couplings = {(0,1):C.GC_81,(0,0):C.GC_178})

V_1217 = Vertex(name = 'V_1217',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV5 ],
                couplings = {(0,1):C.GC_81,(0,0):C.GC_178})

V_1218 = Vertex(name = 'V_1218',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV5 ],
                couplings = {(0,1):C.GC_81,(0,0):C.GC_178})

V_1219 = Vertex(name = 'V_1219',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_4})

V_1220 = Vertex(name = 'V_1220',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_4})

V_1221 = Vertex(name = 'V_1221',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_4})

V_1222 = Vertex(name = 'V_1222',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_4})

V_1223 = Vertex(name = 'V_1223',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_4})

V_1224 = Vertex(name = 'V_1224',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_4})

V_1225 = Vertex(name = 'V_1225',
                particles = [ P.e__plus__, P.v1, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_48,(0,1):C.GC_436})

V_1226 = Vertex(name = 'V_1226',
                particles = [ P.mu__plus__, P.v2, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_49,(0,1):C.GC_437})

V_1227 = Vertex(name = 'V_1227',
                particles = [ P.ta__plus__, P.v3, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_50,(0,1):C.GC_438})

V_1228 = Vertex(name = 'V_1228',
                particles = [ P.e__plus__, P.N1, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_51,(0,1):C.GC_439})

V_1229 = Vertex(name = 'V_1229',
                particles = [ P.mu__plus__, P.N2, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_52,(0,1):C.GC_440})

V_1230 = Vertex(name = 'V_1230',
                particles = [ P.ta__plus__, P.N3, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_53,(0,1):C.GC_441})

V_1231 = Vertex(name = 'V_1231',
                particles = [ P.e__plus__, P.v1, P.W2__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_430,(0,1):C.GC_54})

V_1232 = Vertex(name = 'V_1232',
                particles = [ P.mu__plus__, P.v2, P.W2__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_431,(0,1):C.GC_55})

V_1233 = Vertex(name = 'V_1233',
                particles = [ P.ta__plus__, P.v3, P.W2__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_432,(0,1):C.GC_56})

V_1234 = Vertex(name = 'V_1234',
                particles = [ P.e__plus__, P.N1, P.W2__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_433,(0,1):C.GC_57})

V_1235 = Vertex(name = 'V_1235',
                particles = [ P.mu__plus__, P.N2, P.W2__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_434,(0,1):C.GC_58})

V_1236 = Vertex(name = 'V_1236',
                particles = [ P.ta__plus__, P.N3, P.W2__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_435,(0,1):C.GC_59})

V_1237 = Vertex(name = 'V_1237',
                particles = [ P.N1, P.e__minus__, P.W2__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_433,(0,1):C.GC_57})

V_1238 = Vertex(name = 'V_1238',
                particles = [ P.v1, P.e__minus__, P.W2__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_430,(0,1):C.GC_54})

V_1239 = Vertex(name = 'V_1239',
                particles = [ P.N2, P.mu__minus__, P.W2__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_434,(0,1):C.GC_58})

V_1240 = Vertex(name = 'V_1240',
                particles = [ P.v2, P.mu__minus__, P.W2__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_431,(0,1):C.GC_55})

V_1241 = Vertex(name = 'V_1241',
                particles = [ P.N3, P.ta__minus__, P.W2__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_435,(0,1):C.GC_59})

V_1242 = Vertex(name = 'V_1242',
                particles = [ P.v3, P.ta__minus__, P.W2__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_432,(0,1):C.GC_56})

V_1243 = Vertex(name = 'V_1243',
                particles = [ P.N1, P.e__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_51,(0,1):C.GC_439})

V_1244 = Vertex(name = 'V_1244',
                particles = [ P.v1, P.e__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_48,(0,1):C.GC_436})

V_1245 = Vertex(name = 'V_1245',
                particles = [ P.N2, P.mu__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_52,(0,1):C.GC_440})

V_1246 = Vertex(name = 'V_1246',
                particles = [ P.v2, P.mu__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_49,(0,1):C.GC_437})

V_1247 = Vertex(name = 'V_1247',
                particles = [ P.N3, P.ta__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_53,(0,1):C.GC_441})

V_1248 = Vertex(name = 'V_1248',
                particles = [ P.v3, P.ta__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_50,(0,1):C.GC_438})

V_1249 = Vertex(name = 'V_1249',
                particles = [ P.b__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_21,(0,1):C.GC_412})

V_1250 = Vertex(name = 'V_1250',
                particles = [ P.d__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_19,(0,1):C.GC_410})

V_1251 = Vertex(name = 'V_1251',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_20,(0,1):C.GC_411})

V_1252 = Vertex(name = 'V_1252',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_24,(0,1):C.GC_415})

V_1253 = Vertex(name = 'V_1253',
                particles = [ P.d__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_22,(0,1):C.GC_413})

V_1254 = Vertex(name = 'V_1254',
                particles = [ P.s__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_23,(0,1):C.GC_414})

V_1255 = Vertex(name = 'V_1255',
                particles = [ P.b__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_18,(0,1):C.GC_409})

V_1256 = Vertex(name = 'V_1256',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_16,(0,1):C.GC_407})

V_1257 = Vertex(name = 'V_1257',
                particles = [ P.s__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_17,(0,1):C.GC_408})

V_1258 = Vertex(name = 'V_1258',
                particles = [ P.b__tilde__, P.c, P.W2__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_403,(0,1):C.GC_30})

V_1259 = Vertex(name = 'V_1259',
                particles = [ P.d__tilde__, P.c, P.W2__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_401,(0,1):C.GC_28})

V_1260 = Vertex(name = 'V_1260',
                particles = [ P.s__tilde__, P.c, P.W2__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_402,(0,1):C.GC_29})

V_1261 = Vertex(name = 'V_1261',
                particles = [ P.b__tilde__, P.t, P.W2__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_406,(0,1):C.GC_33})

V_1262 = Vertex(name = 'V_1262',
                particles = [ P.d__tilde__, P.t, P.W2__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_404,(0,1):C.GC_31})

V_1263 = Vertex(name = 'V_1263',
                particles = [ P.s__tilde__, P.t, P.W2__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_405,(0,1):C.GC_32})

V_1264 = Vertex(name = 'V_1264',
                particles = [ P.b__tilde__, P.u, P.W2__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_400,(0,1):C.GC_27})

V_1265 = Vertex(name = 'V_1265',
                particles = [ P.d__tilde__, P.u, P.W2__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_398,(0,1):C.GC_25})

V_1266 = Vertex(name = 'V_1266',
                particles = [ P.s__tilde__, P.u, P.W2__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_399,(0,1):C.GC_26})

V_1267 = Vertex(name = 'V_1267',
                particles = [ P.c__tilde__, P.b, P.W2__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1068,(0,1):C.GC_1131})

V_1268 = Vertex(name = 'V_1268',
                particles = [ P.t__tilde__, P.b, P.W2__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1092,(0,1):C.GC_1161})

V_1269 = Vertex(name = 'V_1269',
                particles = [ P.u__tilde__, P.b, P.W2__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1056,(0,1):C.GC_1113})

V_1270 = Vertex(name = 'V_1270',
                particles = [ P.c__tilde__, P.d, P.W2__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1064,(0,1):C.GC_1123})

V_1271 = Vertex(name = 'V_1271',
                particles = [ P.t__tilde__, P.d, P.W2__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1076,(0,1):C.GC_1141})

V_1272 = Vertex(name = 'V_1272',
                particles = [ P.u__tilde__, P.d, P.W2__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1052,(0,1):C.GC_1105})

V_1273 = Vertex(name = 'V_1273',
                particles = [ P.c__tilde__, P.s, P.W2__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1066,(0,1):C.GC_1127})

V_1274 = Vertex(name = 'V_1274',
                particles = [ P.t__tilde__, P.s, P.W2__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1084,(0,1):C.GC_1151})

V_1275 = Vertex(name = 'V_1275',
                particles = [ P.u__tilde__, P.s, P.W2__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1054,(0,1):C.GC_1109})

V_1276 = Vertex(name = 'V_1276',
                particles = [ P.c__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1067,(0,1):C.GC_1132})

V_1277 = Vertex(name = 'V_1277',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1091,(0,1):C.GC_1162})

V_1278 = Vertex(name = 'V_1278',
                particles = [ P.u__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1055,(0,1):C.GC_1114})

V_1279 = Vertex(name = 'V_1279',
                particles = [ P.c__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1063,(0,1):C.GC_1124})

V_1280 = Vertex(name = 'V_1280',
                particles = [ P.t__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1075,(0,1):C.GC_1142})

V_1281 = Vertex(name = 'V_1281',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1051,(0,1):C.GC_1106})

V_1282 = Vertex(name = 'V_1282',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1065,(0,1):C.GC_1128})

V_1283 = Vertex(name = 'V_1283',
                particles = [ P.t__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1083,(0,1):C.GC_1152})

V_1284 = Vertex(name = 'V_1284',
                particles = [ P.u__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_1053,(0,1):C.GC_1110})

V_1285 = Vertex(name = 'V_1285',
                particles = [ P.v1, P.v1, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_316})

V_1286 = Vertex(name = 'V_1286',
                particles = [ P.v1, P.v1, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_166})

V_1287 = Vertex(name = 'V_1287',
                particles = [ P.v1, P.v1, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_205})

V_1288 = Vertex(name = 'V_1288',
                particles = [ P.v2, P.v2, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_317})

V_1289 = Vertex(name = 'V_1289',
                particles = [ P.v2, P.v2, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_167})

V_1290 = Vertex(name = 'V_1290',
                particles = [ P.v2, P.v2, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_206})

V_1291 = Vertex(name = 'V_1291',
                particles = [ P.v3, P.v3, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_318})

V_1292 = Vertex(name = 'V_1292',
                particles = [ P.v3, P.v3, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_168})

V_1293 = Vertex(name = 'V_1293',
                particles = [ P.v3, P.v3, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_207})

V_1294 = Vertex(name = 'V_1294',
                particles = [ P.N1, P.N1, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_321})

V_1295 = Vertex(name = 'V_1295',
                particles = [ P.N1, P.N1, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_171})

V_1296 = Vertex(name = 'V_1296',
                particles = [ P.N1, P.N1, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_210})

V_1297 = Vertex(name = 'V_1297',
                particles = [ P.v1, P.N1, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_320,(0,1):C.GC_319})

V_1298 = Vertex(name = 'V_1298',
                particles = [ P.v1, P.N1, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_170,(0,1):C.GC_169})

V_1299 = Vertex(name = 'V_1299',
                particles = [ P.v1, P.N1, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_209,(0,1):C.GC_208})

V_1300 = Vertex(name = 'V_1300',
                particles = [ P.N2, P.N2, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_324})

V_1301 = Vertex(name = 'V_1301',
                particles = [ P.N2, P.N2, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_174})

V_1302 = Vertex(name = 'V_1302',
                particles = [ P.N2, P.N2, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_213})

V_1303 = Vertex(name = 'V_1303',
                particles = [ P.v2, P.N2, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_323,(0,1):C.GC_322})

V_1304 = Vertex(name = 'V_1304',
                particles = [ P.v2, P.N2, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_173,(0,1):C.GC_172})

V_1305 = Vertex(name = 'V_1305',
                particles = [ P.v2, P.N2, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_212,(0,1):C.GC_211})

V_1306 = Vertex(name = 'V_1306',
                particles = [ P.N3, P.N3, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_327})

V_1307 = Vertex(name = 'V_1307',
                particles = [ P.N3, P.N3, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_177})

V_1308 = Vertex(name = 'V_1308',
                particles = [ P.N3, P.N3, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_216})

V_1309 = Vertex(name = 'V_1309',
                particles = [ P.v3, P.N3, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_326,(0,1):C.GC_325})

V_1310 = Vertex(name = 'V_1310',
                particles = [ P.v3, P.N3, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_176,(0,1):C.GC_175})

V_1311 = Vertex(name = 'V_1311',
                particles = [ P.v3, P.N3, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_215,(0,1):C.GC_214})

V_1312 = Vertex(name = 'V_1312',
                particles = [ P.v1, P.v1, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_332})

V_1313 = Vertex(name = 'V_1313',
                particles = [ P.v1, P.v1, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_154})

V_1314 = Vertex(name = 'V_1314',
                particles = [ P.v1, P.v1, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_189})

V_1315 = Vertex(name = 'V_1315',
                particles = [ P.v2, P.v2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_333})

V_1316 = Vertex(name = 'V_1316',
                particles = [ P.v2, P.v2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_155})

V_1317 = Vertex(name = 'V_1317',
                particles = [ P.v2, P.v2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_190})

V_1318 = Vertex(name = 'V_1318',
                particles = [ P.v3, P.v3, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_334})

V_1319 = Vertex(name = 'V_1319',
                particles = [ P.v3, P.v3, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_156})

V_1320 = Vertex(name = 'V_1320',
                particles = [ P.v3, P.v3, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_191})

V_1321 = Vertex(name = 'V_1321',
                particles = [ P.N1, P.N1, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_337})

V_1322 = Vertex(name = 'V_1322',
                particles = [ P.N1, P.N1, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_159})

V_1323 = Vertex(name = 'V_1323',
                particles = [ P.N1, P.N1, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_194})

V_1324 = Vertex(name = 'V_1324',
                particles = [ P.v1, P.N1, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_336,(0,1):C.GC_335})

V_1325 = Vertex(name = 'V_1325',
                particles = [ P.v1, P.N1, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_157,(0,1):C.GC_158})

V_1326 = Vertex(name = 'V_1326',
                particles = [ P.v1, P.N1, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_192,(0,1):C.GC_193})

V_1327 = Vertex(name = 'V_1327',
                particles = [ P.N2, P.N2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_340})

V_1328 = Vertex(name = 'V_1328',
                particles = [ P.N2, P.N2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_162})

V_1329 = Vertex(name = 'V_1329',
                particles = [ P.N2, P.N2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_1330 = Vertex(name = 'V_1330',
                particles = [ P.v2, P.N2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_339,(0,1):C.GC_338})

V_1331 = Vertex(name = 'V_1331',
                particles = [ P.v2, P.N2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_160,(0,1):C.GC_161})

V_1332 = Vertex(name = 'V_1332',
                particles = [ P.v2, P.N2, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_195,(0,1):C.GC_196})

V_1333 = Vertex(name = 'V_1333',
                particles = [ P.N3, P.N3, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_343})

V_1334 = Vertex(name = 'V_1334',
                particles = [ P.N3, P.N3, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_165})

V_1335 = Vertex(name = 'V_1335',
                particles = [ P.N3, P.N3, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_200})

V_1336 = Vertex(name = 'V_1336',
                particles = [ P.v3, P.N3, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_342,(0,1):C.GC_341})

V_1337 = Vertex(name = 'V_1337',
                particles = [ P.v3, P.N3, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_163,(0,1):C.GC_164})

V_1338 = Vertex(name = 'V_1338',
                particles = [ P.v3, P.N3, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_198,(0,1):C.GC_199})

V_1339 = Vertex(name = 'V_1339',
                particles = [ P.e__plus__, P.e__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_311,(0,1):C.GC_98})

V_1340 = Vertex(name = 'V_1340',
                particles = [ P.e__plus__, P.e__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_98,(0,1):C.GC_115})

V_1341 = Vertex(name = 'V_1341',
                particles = [ P.e__plus__, P.e__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV4 ],
                couplings = {(0,0):C.GC_202})

V_1342 = Vertex(name = 'V_1342',
                particles = [ P.mu__plus__, P.mu__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_311,(0,1):C.GC_98})

V_1343 = Vertex(name = 'V_1343',
                particles = [ P.mu__plus__, P.mu__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_98,(0,1):C.GC_115})

V_1344 = Vertex(name = 'V_1344',
                particles = [ P.mu__plus__, P.mu__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV4 ],
                couplings = {(0,0):C.GC_202})

V_1345 = Vertex(name = 'V_1345',
                particles = [ P.ta__plus__, P.ta__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_311,(0,1):C.GC_98})

V_1346 = Vertex(name = 'V_1346',
                particles = [ P.ta__plus__, P.ta__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_98,(0,1):C.GC_115})

V_1347 = Vertex(name = 'V_1347',
                particles = [ P.ta__plus__, P.ta__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV4 ],
                couplings = {(0,0):C.GC_202})

V_1348 = Vertex(name = 'V_1348',
                particles = [ P.e__plus__, P.e__minus__, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_329,(0,1):C.GC_78})

V_1349 = Vertex(name = 'V_1349',
                particles = [ P.e__plus__, P.e__minus__, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_78,(0,1):C.GC_124})

V_1350 = Vertex(name = 'V_1350',
                particles = [ P.e__plus__, P.e__minus__, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV4 ],
                couplings = {(0,0):C.GC_181})

V_1351 = Vertex(name = 'V_1351',
                particles = [ P.mu__plus__, P.mu__minus__, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_329,(0,1):C.GC_78})

V_1352 = Vertex(name = 'V_1352',
                particles = [ P.mu__plus__, P.mu__minus__, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_78,(0,1):C.GC_124})

V_1353 = Vertex(name = 'V_1353',
                particles = [ P.mu__plus__, P.mu__minus__, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV4 ],
                couplings = {(0,0):C.GC_181})

V_1354 = Vertex(name = 'V_1354',
                particles = [ P.ta__plus__, P.ta__minus__, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_329,(0,1):C.GC_78})

V_1355 = Vertex(name = 'V_1355',
                particles = [ P.ta__plus__, P.ta__minus__, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_78,(0,1):C.GC_124})

V_1356 = Vertex(name = 'V_1356',
                particles = [ P.ta__plus__, P.ta__minus__, P.Z2 ],
                color = [ '1' ],
                lorentz = [ L.FFV4 ],
                couplings = {(0,0):C.GC_181})

V_1357 = Vertex(name = 'V_1357',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_310,(0,1):C.GC_97})

V_1358 = Vertex(name = 'V_1358',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_97,(0,1):C.GC_114})

V_1359 = Vertex(name = 'V_1359',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV4 ],
                couplings = {(0,0):C.GC_201})

V_1360 = Vertex(name = 'V_1360',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_310,(0,1):C.GC_97})

V_1361 = Vertex(name = 'V_1361',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_97,(0,1):C.GC_114})

V_1362 = Vertex(name = 'V_1362',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV4 ],
                couplings = {(0,0):C.GC_201})

V_1363 = Vertex(name = 'V_1363',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_310,(0,1):C.GC_97})

V_1364 = Vertex(name = 'V_1364',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_97,(0,1):C.GC_114})

V_1365 = Vertex(name = 'V_1365',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV4 ],
                couplings = {(0,0):C.GC_201})

V_1366 = Vertex(name = 'V_1366',
                particles = [ P.c__tilde__, P.c, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_328,(0,1):C.GC_77})

V_1367 = Vertex(name = 'V_1367',
                particles = [ P.c__tilde__, P.c, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_77,(0,1):C.GC_123})

V_1368 = Vertex(name = 'V_1368',
                particles = [ P.c__tilde__, P.c, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV4 ],
                couplings = {(0,0):C.GC_182})

V_1369 = Vertex(name = 'V_1369',
                particles = [ P.t__tilde__, P.t, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_328,(0,1):C.GC_77})

V_1370 = Vertex(name = 'V_1370',
                particles = [ P.t__tilde__, P.t, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_77,(0,1):C.GC_123})

V_1371 = Vertex(name = 'V_1371',
                particles = [ P.t__tilde__, P.t, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV4 ],
                couplings = {(0,0):C.GC_182})

V_1372 = Vertex(name = 'V_1372',
                particles = [ P.u__tilde__, P.u, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_328,(0,1):C.GC_77})

V_1373 = Vertex(name = 'V_1373',
                particles = [ P.u__tilde__, P.u, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_77,(0,1):C.GC_123})

V_1374 = Vertex(name = 'V_1374',
                particles = [ P.u__tilde__, P.u, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV4 ],
                couplings = {(0,0):C.GC_182})

V_1375 = Vertex(name = 'V_1375',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_9,(0,1):C.GC_97})

V_1376 = Vertex(name = 'V_1376',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_97,(0,1):C.GC_113})

V_1377 = Vertex(name = 'V_1377',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_218,(0,1):C.GC_202})

V_1378 = Vertex(name = 'V_1378',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_9,(0,1):C.GC_97})

V_1379 = Vertex(name = 'V_1379',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_97,(0,1):C.GC_113})

V_1380 = Vertex(name = 'V_1380',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_218,(0,1):C.GC_202})

V_1381 = Vertex(name = 'V_1381',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_9,(0,1):C.GC_97})

V_1382 = Vertex(name = 'V_1382',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_97,(0,1):C.GC_113})

V_1383 = Vertex(name = 'V_1383',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_218,(0,1):C.GC_202})

V_1384 = Vertex(name = 'V_1384',
                particles = [ P.b__tilde__, P.b, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_61,(0,1):C.GC_77})

V_1385 = Vertex(name = 'V_1385',
                particles = [ P.b__tilde__, P.b, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_77,(0,1):C.GC_122})

V_1386 = Vertex(name = 'V_1386',
                particles = [ P.b__tilde__, P.b, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_237,(0,1):C.GC_181})

V_1387 = Vertex(name = 'V_1387',
                particles = [ P.d__tilde__, P.d, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_61,(0,1):C.GC_77})

V_1388 = Vertex(name = 'V_1388',
                particles = [ P.d__tilde__, P.d, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_77,(0,1):C.GC_122})

V_1389 = Vertex(name = 'V_1389',
                particles = [ P.d__tilde__, P.d, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_237,(0,1):C.GC_181})

V_1390 = Vertex(name = 'V_1390',
                particles = [ P.s__tilde__, P.s, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_61,(0,1):C.GC_77})

V_1391 = Vertex(name = 'V_1391',
                particles = [ P.s__tilde__, P.s, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_77,(0,1):C.GC_122})

V_1392 = Vertex(name = 'V_1392',
                particles = [ P.s__tilde__, P.s, P.Z2 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                couplings = {(0,0):C.GC_237,(0,1):C.GC_181})

