# This file was automatically created by FeynRules 2.3.49_fne
# Mathematica version: 13.1.0 for Mac OS X ARM (64-bit) (June 16, 2022)
# Date: Tue 28 Nov 2023 12:50:07


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(AH*cth1*complex(0,1))',
                order = {'HIW':1})

GC_2 = Coupling(name = 'GC_2',
                value = '-(cth1*complex(0,1)*GH)',
                order = {'HIG':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-gs',
                order = {'QCD':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'complex(0,1)*gs',
                order = {'QCD':1})

GC_5 = Coupling(name = 'GC_5',
                value = '-(cth1*GH*gs)',
                order = {'HIG':1,'QCD':1})

GC_6 = Coupling(name = 'GC_6',
                value = 'complex(0,1)*gs**2',
                order = {'QCD':2})

GC_7 = Coupling(name = 'GC_7',
                value = 'cth1*complex(0,1)*GH*gs**2',
                order = {'HIG':1,'QCD':2})

GC_8 = Coupling(name = 'GC_8',
                value = '-(cphi*cw*gw)',
                order = {'EWL':1})

GC_9 = Coupling(name = 'GC_9',
                value = '-0.5*(cphi*cw*complex(0,1)*gw)',
                order = {'EWL':1})

GC_10 = Coupling(name = 'GC_10',
                 value = '-(cphi*cw*complex(0,1)*gw)',
                 order = {'EWL':1})

GC_11 = Coupling(name = 'GC_11',
                 value = '-(cxi*complex(0,1)*gw)',
                 order = {'EWL':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'cxi*complex(0,1)*gw',
                 order = {'EWL':1})

GC_13 = Coupling(name = 'GC_13',
                 value = '-((cxi*complex(0,1)*gw)/cmath.sqrt(2))',
                 order = {'EWL':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '(cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_15 = Coupling(name = 'GC_15',
                 value = '(cxi*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '(CKML1x1*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '(CKML1x2*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '(CKML1x3*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '(CKML2x1*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '(CKML2x2*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_21 = Coupling(name = 'GC_21',
                 value = '(CKML2x3*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_22 = Coupling(name = 'GC_22',
                 value = '(CKML3x1*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_23 = Coupling(name = 'GC_23',
                 value = '(CKML3x2*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '(CKML3x3*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_25 = Coupling(name = 'GC_25',
                 value = '(CKMR1x1*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '(CKMR1x2*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '(CKMR1x3*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '(CKMR2x1*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '(CKMR2x2*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '(CKMR2x3*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '(CKMR3x1*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '(CKMR3x2*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '(CKMR3x3*cxi*complex(0,1)*gw)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_34 = Coupling(name = 'GC_34',
                 value = 'cphi*cw*cxi**2*complex(0,1)*gw',
                 order = {'EWL':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '2*cphi**2*cw**2*complex(0,1)*gw**2',
                 order = {'EWL':2})

GC_36 = Coupling(name = 'GC_36',
                 value = '-(cphi*cw*cxi*complex(0,1)*gw**2)',
                 order = {'EWL':2})

GC_37 = Coupling(name = 'GC_37',
                 value = '-((cphi*cw*cxi*gw**2)/cmath.sqrt(2))',
                 order = {'EWL':2})

GC_38 = Coupling(name = 'GC_38',
                 value = '-((cphi*cw*cxi*complex(0,1)*gw**2)/cmath.sqrt(2))',
                 order = {'EWL':2})

GC_39 = Coupling(name = 'GC_39',
                 value = '(cphi*cw*cxi*gw**2)/cmath.sqrt(2)',
                 order = {'EWL':2})

GC_40 = Coupling(name = 'GC_40',
                 value = 'cxi**2*complex(0,1)*gw**2',
                 order = {'EWL':2})

GC_41 = Coupling(name = 'GC_41',
                 value = '2*cxi**2*complex(0,1)*gw**2',
                 order = {'EWL':2})

GC_42 = Coupling(name = 'GC_42',
                 value = '-(cxi**2*gw**2*cmath.sqrt(2))',
                 order = {'EWL':2})

GC_43 = Coupling(name = 'GC_43',
                 value = '-(cxi**2*complex(0,1)*gw**2*cmath.sqrt(2))',
                 order = {'EWL':2})

GC_44 = Coupling(name = 'GC_44',
                 value = 'cxi**2*gw**2*cmath.sqrt(2)',
                 order = {'EWL':2})

GC_45 = Coupling(name = 'GC_45',
                 value = '-(cth1*cxi**2*complex(0,1)*gw**2*cmath.sqrt(2))',
                 order = {'EWL':2})

GC_46 = Coupling(name = 'GC_46',
                 value = 'cphi**2*cw**2*cxi**2*complex(0,1)*gw**2',
                 order = {'EWL':2})

GC_47 = Coupling(name = 'GC_47',
                 value = '-(cxi**4*complex(0,1)*gw**2)',
                 order = {'EWL':2})

GC_48 = Coupling(name = 'GC_48',
                 value = '(cxi*complex(0,1)*gw*KL1x1)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '(cxi*complex(0,1)*gw*KL2x2)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '(cxi*complex(0,1)*gw*KL3x3)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '(cxi*complex(0,1)*gw*KL4x1)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '(cxi*complex(0,1)*gw*KL5x2)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '(cxi*complex(0,1)*gw*KL6x3)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '(cxi*complex(0,1)*gw*KR1x1)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '(cxi*complex(0,1)*gw*KR2x2)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_56 = Coupling(name = 'GC_56',
                 value = '(cxi*complex(0,1)*gw*KR3x3)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '(cxi*complex(0,1)*gw*KR4x1)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '(cxi*complex(0,1)*gw*KR5x2)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '(cxi*complex(0,1)*gw*KR6x3)/cmath.sqrt(2)',
                 order = {'EWL':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '-(cw*gw*sphi)',
                 order = {'EWR':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '-0.5*(cw*complex(0,1)*gw*sphi)',
                 order = {'EWR':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '-(cw*complex(0,1)*gw*sphi)',
                 order = {'EWR':1})

GC_63 = Coupling(name = 'GC_63',
                 value = 'cw*cxi**2*complex(0,1)*gw*sphi',
                 order = {'EWR':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '2*cphi*cw**2*complex(0,1)*gw**2*sphi',
                 order = {'EWL':1,'EWR':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '-(cw*cxi*complex(0,1)*gw**2*sphi)',
                 order = {'EWL':1,'EWR':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '-((cw*cxi*gw**2*sphi)/cmath.sqrt(2))',
                 order = {'EWL':1,'EWR':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '-((cw*cxi*complex(0,1)*gw**2*sphi)/cmath.sqrt(2))',
                 order = {'EWL':1,'EWR':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '(cw*cxi*gw**2*sphi)/cmath.sqrt(2)',
                 order = {'EWL':1,'EWR':1})

GC_69 = Coupling(name = 'GC_69',
                 value = 'cphi*cw**2*cxi**2*complex(0,1)*gw**2*sphi',
                 order = {'EWL':1,'EWR':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '2*cw**2*complex(0,1)*gw**2*sphi**2',
                 order = {'EWR':2})

GC_71 = Coupling(name = 'GC_71',
                 value = '-(AD*complex(0,1)*sth1)',
                 order = {'HIW':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '-(complex(0,1)*GD*sth1)',
                 order = {'HIG':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '-(GD*gs*sth1)',
                 order = {'HIG':1,'QCD':1})

GC_74 = Coupling(name = 'GC_74',
                 value = 'complex(0,1)*GD*gs**2*sth1',
                 order = {'HIG':1,'QCD':2})

GC_75 = Coupling(name = 'GC_75',
                 value = 'cxi**2*complex(0,1)*gw**2*sth1*cmath.sqrt(2)',
                 order = {'EWL':2})

GC_76 = Coupling(name = 'GC_76',
                 value = '-((cphi*g1*sw)/cw)',
                 order = {'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '-0.16666666666666666*(cphi*complex(0,1)*g1*sw)/cw',
                 order = {'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '(cphi*complex(0,1)*g1*sw)/(2.*cw)',
                 order = {'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '(cphi*complex(0,1)*g1*sw)/cw',
                 order = {'QED':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '-(gw*sw)',
                 order = {'EWL':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '-0.5*(complex(0,1)*gw*sw)',
                 order = {'EWL':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '(2*complex(0,1)*gw*sw)/3.',
                 order = {'EWL':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '-(complex(0,1)*gw*sw)',
                 order = {'EWL':1})

GC_84 = Coupling(name = 'GC_84',
                 value = 'cxi**2*complex(0,1)*gw*sw',
                 order = {'EWL':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '-2*cphi**2*complex(0,1)*g1*gw*sw',
                 order = {'EWL':1,'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '2*cphi**2*complex(0,1)*g1*gw*sw',
                 order = {'EWL':1,'QED':1})

GC_87 = Coupling(name = 'GC_87',
                 value = '(2*cphi*cxi*complex(0,1)*g1*gw*sw)/cw',
                 order = {'EWL':1,'QED':1})

GC_88 = Coupling(name = 'GC_88',
                 value = '-((cphi*cxi*g1*gw*sw*cmath.sqrt(2))/cw)',
                 order = {'EWL':1,'QED':1})

GC_89 = Coupling(name = 'GC_89',
                 value = '-((cphi*cxi*complex(0,1)*g1*gw*sw*cmath.sqrt(2))/cw)',
                 order = {'EWL':1,'QED':1})

GC_90 = Coupling(name = 'GC_90',
                 value = '(cphi*cxi*g1*gw*sw*cmath.sqrt(2))/cw',
                 order = {'EWL':1,'QED':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '2*cphi*cw*complex(0,1)*gw**2*sw',
                 order = {'EWL':2})

GC_92 = Coupling(name = 'GC_92',
                 value = '-(cxi*complex(0,1)*gw**2*sw)',
                 order = {'EWL':2})

GC_93 = Coupling(name = 'GC_93',
                 value = '-((cxi*gw**2*sw)/cmath.sqrt(2))',
                 order = {'EWL':2})

GC_94 = Coupling(name = 'GC_94',
                 value = '-((cxi*complex(0,1)*gw**2*sw)/cmath.sqrt(2))',
                 order = {'EWL':2})

GC_95 = Coupling(name = 'GC_95',
                 value = '(cxi*gw**2*sw)/cmath.sqrt(2)',
                 order = {'EWL':2})

GC_96 = Coupling(name = 'GC_96',
                 value = '-2*cphi*cw*cxi**2*complex(0,1)*gw**2*sw',
                 order = {'EWL':2})

GC_97 = Coupling(name = 'GC_97',
                 value = '(complex(0,1)*g1*sphi*sw)/(6.*cw)',
                 order = {'EWL':-1,'EWR':1,'QED':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '-0.5*(complex(0,1)*g1*sphi*sw)/cw',
                 order = {'EWL':-1,'EWR':1,'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '-((complex(0,1)*g1*sphi*sw)/cw)',
                 order = {'EWL':-1,'EWR':1,'QED':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '(g1*sphi*sw)/cw',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '-4*cphi*complex(0,1)*g1*gw*sphi*sw',
                  order = {'EWR':1,'QED':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '4*cphi*complex(0,1)*g1*gw*sphi*sw',
                  order = {'EWR':1,'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '(-2*cxi*complex(0,1)*g1*gw*sphi*sw)/cw',
                  order = {'EWR':1,'QED':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '-((cxi*g1*gw*sphi*sw*cmath.sqrt(2))/cw)',
                  order = {'EWR':1,'QED':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '(cxi*complex(0,1)*g1*gw*sphi*sw*cmath.sqrt(2))/cw',
                  order = {'EWR':1,'QED':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '(cxi*g1*gw*sphi*sw*cmath.sqrt(2))/cw',
                  order = {'EWR':1,'QED':1})

GC_107 = Coupling(name = 'GC_107',
                  value = '2*cw*complex(0,1)*gw**2*sphi*sw',
                  order = {'EWL':1,'EWR':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '-2*cw*cxi**2*complex(0,1)*gw**2*sphi*sw',
                  order = {'EWL':1,'EWR':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '-2*complex(0,1)*g1*gw*sphi**2*sw',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_110 = Coupling(name = 'GC_110',
                  value = '2*complex(0,1)*g1*gw*sphi**2*sw',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '(2*cphi**2*complex(0,1)*g1**2*sw**2)/cw**2',
                  order = {'QED':2})

GC_112 = Coupling(name = 'GC_112',
                  value = '(2*cphi**2*cth1**2*complex(0,1)*g1**2*sw**2)/cw**2',
                  order = {'QED':2})

GC_113 = Coupling(name = 'GC_113',
                  value = '(cphi*complex(0,1)*gw*sw**2)/(3.*cw)',
                  order = {'EWL':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '(-2*cphi*complex(0,1)*gw*sw**2)/(3.*cw)',
                  order = {'EWL':1})

GC_115 = Coupling(name = 'GC_115',
                  value = '(cphi*complex(0,1)*gw*sw**2)/cw',
                  order = {'EWL':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '-((cphi*cxi**2*complex(0,1)*gw*sw**2)/cw)',
                  order = {'EWL':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '(-2*cphi*complex(0,1)*g1*gw*sw**2)/cw',
                  order = {'EWL':1,'QED':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '(2*cphi*complex(0,1)*g1*gw*sw**2)/cw',
                  order = {'EWL':1,'QED':1})

GC_119 = Coupling(name = 'GC_119',
                  value = '2*complex(0,1)*gw**2*sw**2',
                  order = {'EWL':2})

GC_120 = Coupling(name = 'GC_120',
                  value = '2*cth1**2*complex(0,1)*gw**2*sw**2',
                  order = {'EWL':2})

GC_121 = Coupling(name = 'GC_121',
                  value = 'cxi**2*complex(0,1)*gw**2*sw**2',
                  order = {'EWL':2})

GC_122 = Coupling(name = 'GC_122',
                  value = '(complex(0,1)*gw*sphi*sw**2)/(3.*cw)',
                  order = {'EWR':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '(-2*complex(0,1)*gw*sphi*sw**2)/(3.*cw)',
                  order = {'EWR':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '(complex(0,1)*gw*sphi*sw**2)/cw',
                  order = {'EWR':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '-((cxi**2*complex(0,1)*gw*sphi*sw**2)/cw)',
                  order = {'EWR':1})

GC_126 = Coupling(name = 'GC_126',
                  value = '(-2*complex(0,1)*g1*gw*sphi*sw**2)/cw',
                  order = {'EWR':1,'QED':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '(2*complex(0,1)*g1*gw*sphi*sw**2)/cw',
                  order = {'EWR':1,'QED':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '(2*complex(0,1)*g1**2*sphi**2*sw**2)/cw**2',
                  order = {'EWL':-2,'EWR':2,'QED':2})

GC_129 = Coupling(name = 'GC_129',
                  value = '(2*cth1**2*complex(0,1)*g1**2*sphi**2*sw**2)/cw**2',
                  order = {'EWL':-2,'EWR':2,'QED':2})

GC_130 = Coupling(name = 'GC_130',
                  value = '(-2*cphi**2*cth1*complex(0,1)*g1**2*sth1*sw**2)/cw**2',
                  order = {'QED':2})

GC_131 = Coupling(name = 'GC_131',
                  value = '-2*cth1*complex(0,1)*gw**2*sth1*sw**2',
                  order = {'EWL':2})

GC_132 = Coupling(name = 'GC_132',
                  value = '(-2*cth1*complex(0,1)*g1**2*sphi**2*sth1*sw**2)/cw**2',
                  order = {'EWL':-2,'EWR':2,'QED':2})

GC_133 = Coupling(name = 'GC_133',
                  value = '(2*cphi**2*complex(0,1)*g1**2*sth1**2*sw**2)/cw**2',
                  order = {'QED':2})

GC_134 = Coupling(name = 'GC_134',
                  value = '2*complex(0,1)*gw**2*sth1**2*sw**2',
                  order = {'EWL':2})

GC_135 = Coupling(name = 'GC_135',
                  value = '(2*complex(0,1)*g1**2*sphi**2*sth1**2*sw**2)/cw**2',
                  order = {'EWL':-2,'EWR':2,'QED':2})

GC_136 = Coupling(name = 'GC_136',
                  value = '(-2*cphi*complex(0,1)*gw**2*sw**3)/cw',
                  order = {'EWL':2})

GC_137 = Coupling(name = 'GC_137',
                  value = '(-2*cphi*cth1**2*complex(0,1)*gw**2*sw**3)/cw',
                  order = {'EWL':2})

GC_138 = Coupling(name = 'GC_138',
                  value = '(2*cphi*cxi**2*complex(0,1)*gw**2*sw**3)/cw',
                  order = {'EWL':2})

GC_139 = Coupling(name = 'GC_139',
                  value = '(-2*complex(0,1)*gw**2*sphi*sw**3)/cw',
                  order = {'EWL':1,'EWR':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '(-2*cth1**2*complex(0,1)*gw**2*sphi*sw**3)/cw',
                  order = {'EWL':1,'EWR':1})

GC_141 = Coupling(name = 'GC_141',
                  value = '(2*cxi**2*complex(0,1)*gw**2*sphi*sw**3)/cw',
                  order = {'EWL':1,'EWR':1})

GC_142 = Coupling(name = 'GC_142',
                  value = '(2*cphi*cth1*complex(0,1)*gw**2*sth1*sw**3)/cw',
                  order = {'EWL':2})

GC_143 = Coupling(name = 'GC_143',
                  value = '(2*cth1*complex(0,1)*gw**2*sphi*sth1*sw**3)/cw',
                  order = {'EWL':1,'EWR':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '(-2*cphi*complex(0,1)*gw**2*sth1**2*sw**3)/cw',
                  order = {'EWL':2})

GC_145 = Coupling(name = 'GC_145',
                  value = '(-2*complex(0,1)*gw**2*sphi*sth1**2*sw**3)/cw',
                  order = {'EWL':1,'EWR':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '(2*cphi**2*complex(0,1)*gw**2*sw**4)/cw**2',
                  order = {'EWL':2})

GC_147 = Coupling(name = 'GC_147',
                  value = '(cphi**2*cxi**2*complex(0,1)*gw**2*sw**4)/cw**2',
                  order = {'EWL':2})

GC_148 = Coupling(name = 'GC_148',
                  value = '(-4*cphi*complex(0,1)*g1**2*sphi*sw**4)/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_149 = Coupling(name = 'GC_149',
                  value = '(-4*cphi*cth1**2*complex(0,1)*g1**2*sphi*sw**4)/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_150 = Coupling(name = 'GC_150',
                  value = '(2*complex(0,1)*gw**2*sphi**2*sw**4)/cw**2',
                  order = {'EWR':2})

GC_151 = Coupling(name = 'GC_151',
                  value = '(cxi**2*complex(0,1)*gw**2*sphi**2*sw**4)/cw**2',
                  order = {'EWR':2})

GC_152 = Coupling(name = 'GC_152',
                  value = '(4*cphi*cth1*complex(0,1)*g1**2*sphi*sth1*sw**4)/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_153 = Coupling(name = 'GC_153',
                  value = '(-4*cphi*complex(0,1)*g1**2*sphi*sth1**2*sw**4)/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_154 = Coupling(name = 'GC_154',
                  value = '-0.5*(cphi*complex(0,1)*g1*KL1x1**2*sw)/cw + (cphi*complex(0,1)*g1*KR1x1**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_155 = Coupling(name = 'GC_155',
                  value = '-0.5*(cphi*complex(0,1)*g1*KL2x2**2*sw)/cw + (cphi*complex(0,1)*g1*KR2x2**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_156 = Coupling(name = 'GC_156',
                  value = '-0.5*(cphi*complex(0,1)*g1*KL3x3**2*sw)/cw + (cphi*complex(0,1)*g1*KR3x3**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_157 = Coupling(name = 'GC_157',
                  value = '(cphi*complex(0,1)*g1*KL1x1*KL4x1*sw)/(2.*cw) - (cphi*complex(0,1)*g1*KR1x1*KR4x1*sw)/(2.*cw)',
                  order = {'QED':1})

GC_158 = Coupling(name = 'GC_158',
                  value = '-0.5*(cphi*complex(0,1)*g1*KL1x1*KL4x1*sw)/cw + (cphi*complex(0,1)*g1*KR1x1*KR4x1*sw)/(2.*cw)',
                  order = {'QED':1})

GC_159 = Coupling(name = 'GC_159',
                  value = '-0.5*(cphi*complex(0,1)*g1*KL4x1**2*sw)/cw + (cphi*complex(0,1)*g1*KR4x1**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_160 = Coupling(name = 'GC_160',
                  value = '(cphi*complex(0,1)*g1*KL2x2*KL5x2*sw)/(2.*cw) - (cphi*complex(0,1)*g1*KR2x2*KR5x2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_161 = Coupling(name = 'GC_161',
                  value = '-0.5*(cphi*complex(0,1)*g1*KL2x2*KL5x2*sw)/cw + (cphi*complex(0,1)*g1*KR2x2*KR5x2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_162 = Coupling(name = 'GC_162',
                  value = '-0.5*(cphi*complex(0,1)*g1*KL5x2**2*sw)/cw + (cphi*complex(0,1)*g1*KR5x2**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_163 = Coupling(name = 'GC_163',
                  value = '(cphi*complex(0,1)*g1*KL3x3*KL6x3*sw)/(2.*cw) - (cphi*complex(0,1)*g1*KR3x3*KR6x3*sw)/(2.*cw)',
                  order = {'QED':1})

GC_164 = Coupling(name = 'GC_164',
                  value = '-0.5*(cphi*complex(0,1)*g1*KL3x3*KL6x3*sw)/cw + (cphi*complex(0,1)*g1*KR3x3*KR6x3*sw)/(2.*cw)',
                  order = {'QED':1})

GC_165 = Coupling(name = 'GC_165',
                  value = '-0.5*(cphi*complex(0,1)*g1*KL6x3**2*sw)/cw + (cphi*complex(0,1)*g1*KR6x3**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_166 = Coupling(name = 'GC_166',
                  value = '(complex(0,1)*g1*KL1x1**2*sphi*sw)/(2.*cw) - (complex(0,1)*g1*KR1x1**2*sphi*sw)/(2.*cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_167 = Coupling(name = 'GC_167',
                  value = '(complex(0,1)*g1*KL2x2**2*sphi*sw)/(2.*cw) - (complex(0,1)*g1*KR2x2**2*sphi*sw)/(2.*cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_168 = Coupling(name = 'GC_168',
                  value = '(complex(0,1)*g1*KL3x3**2*sphi*sw)/(2.*cw) - (complex(0,1)*g1*KR3x3**2*sphi*sw)/(2.*cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_169 = Coupling(name = 'GC_169',
                  value = '(complex(0,1)*g1*KL1x1*KL4x1*sphi*sw)/(2.*cw) - (complex(0,1)*g1*KR1x1*KR4x1*sphi*sw)/(2.*cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '-0.5*(complex(0,1)*g1*KL1x1*KL4x1*sphi*sw)/cw + (complex(0,1)*g1*KR1x1*KR4x1*sphi*sw)/(2.*cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_171 = Coupling(name = 'GC_171',
                  value = '(complex(0,1)*g1*KL4x1**2*sphi*sw)/(2.*cw) - (complex(0,1)*g1*KR4x1**2*sphi*sw)/(2.*cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '(complex(0,1)*g1*KL2x2*KL5x2*sphi*sw)/(2.*cw) - (complex(0,1)*g1*KR2x2*KR5x2*sphi*sw)/(2.*cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_173 = Coupling(name = 'GC_173',
                  value = '-0.5*(complex(0,1)*g1*KL2x2*KL5x2*sphi*sw)/cw + (complex(0,1)*g1*KR2x2*KR5x2*sphi*sw)/(2.*cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_174 = Coupling(name = 'GC_174',
                  value = '(complex(0,1)*g1*KL5x2**2*sphi*sw)/(2.*cw) - (complex(0,1)*g1*KR5x2**2*sphi*sw)/(2.*cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '(complex(0,1)*g1*KL3x3*KL6x3*sphi*sw)/(2.*cw) - (complex(0,1)*g1*KR3x3*KR6x3*sphi*sw)/(2.*cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_176 = Coupling(name = 'GC_176',
                  value = '-0.5*(complex(0,1)*g1*KL3x3*KL6x3*sphi*sw)/cw + (complex(0,1)*g1*KR3x3*KR6x3*sphi*sw)/(2.*cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_177 = Coupling(name = 'GC_177',
                  value = '(complex(0,1)*g1*KL6x3**2*sphi*sw)/(2.*cw) - (complex(0,1)*g1*KR6x3**2*sphi*sw)/(2.*cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_178 = Coupling(name = 'GC_178',
                  value = '(complex(0,1)*g1*cmath.sqrt(1 - 2*sw**2))/6.',
                  order = {'QED':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '-(complex(0,1)*g1*cmath.sqrt(1 - 2*sw**2))',
                  order = {'QED':1})

GC_180 = Coupling(name = 'GC_180',
                  value = 'g1*cmath.sqrt(1 - 2*sw**2)',
                  order = {'QED':1})

GC_181 = Coupling(name = 'GC_181',
                  value = '-0.5*(cphi*complex(0,1)*gw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1})

GC_182 = Coupling(name = 'GC_182',
                  value = '(cphi*complex(0,1)*gw*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWL':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '-((cphi*complex(0,1)*gw*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWL':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '(cphi*cxi**2*complex(0,1)*gw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1})

GC_185 = Coupling(name = 'GC_185',
                  value = '-2*cxi*complex(0,1)*g1*gw*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':1,'QED':1})

GC_186 = Coupling(name = 'GC_186',
                  value = '-(cxi*g1*gw*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))',
                  order = {'EWL':1,'QED':1})

GC_187 = Coupling(name = 'GC_187',
                  value = 'cxi*complex(0,1)*g1*gw*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':1,'QED':1})

GC_188 = Coupling(name = 'GC_188',
                  value = 'cxi*g1*gw*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':1,'QED':1})

GC_189 = Coupling(name = 'GC_189',
                  value = '(cphi*complex(0,1)*gw*KR1x1**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWL':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '(cphi*complex(0,1)*gw*KR2x2**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWL':1})

GC_191 = Coupling(name = 'GC_191',
                  value = '(cphi*complex(0,1)*gw*KR3x3**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWL':1})

GC_192 = Coupling(name = 'GC_192',
                  value = '-0.5*(cphi*complex(0,1)*gw*KR1x1*KR4x1*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '(cphi*complex(0,1)*gw*KR1x1*KR4x1*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWL':1})

GC_194 = Coupling(name = 'GC_194',
                  value = '(cphi*complex(0,1)*gw*KR4x1**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWL':1})

GC_195 = Coupling(name = 'GC_195',
                  value = '-0.5*(cphi*complex(0,1)*gw*KR2x2*KR5x2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1})

GC_196 = Coupling(name = 'GC_196',
                  value = '(cphi*complex(0,1)*gw*KR2x2*KR5x2*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWL':1})

GC_197 = Coupling(name = 'GC_197',
                  value = '(cphi*complex(0,1)*gw*KR5x2**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWL':1})

GC_198 = Coupling(name = 'GC_198',
                  value = '-0.5*(cphi*complex(0,1)*gw*KR3x3*KR6x3*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1})

GC_199 = Coupling(name = 'GC_199',
                  value = '(cphi*complex(0,1)*gw*KR3x3*KR6x3*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWL':1})

GC_200 = Coupling(name = 'GC_200',
                  value = '(cphi*complex(0,1)*gw*KR6x3**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWL':1})

GC_201 = Coupling(name = 'GC_201',
                  value = '-0.5*(complex(0,1)*gw*sphi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1})

GC_202 = Coupling(name = 'GC_202',
                  value = '(complex(0,1)*gw*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWR':1})

GC_203 = Coupling(name = 'GC_203',
                  value = '(complex(0,1)*gw*sphi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1})

GC_204 = Coupling(name = 'GC_204',
                  value = '-((cxi**2*complex(0,1)*gw*sphi*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWR':1})

GC_205 = Coupling(name = 'GC_205',
                  value = '-0.5*(complex(0,1)*gw*KR1x1**2*sphi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1})

GC_206 = Coupling(name = 'GC_206',
                  value = '-0.5*(complex(0,1)*gw*KR2x2**2*sphi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1})

GC_207 = Coupling(name = 'GC_207',
                  value = '-0.5*(complex(0,1)*gw*KR3x3**2*sphi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1})

GC_208 = Coupling(name = 'GC_208',
                  value = '-0.5*(complex(0,1)*gw*KR1x1*KR4x1*sphi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1})

GC_209 = Coupling(name = 'GC_209',
                  value = '(complex(0,1)*gw*KR1x1*KR4x1*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWR':1})

GC_210 = Coupling(name = 'GC_210',
                  value = '-0.5*(complex(0,1)*gw*KR4x1**2*sphi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1})

GC_211 = Coupling(name = 'GC_211',
                  value = '-0.5*(complex(0,1)*gw*KR2x2*KR5x2*sphi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1})

GC_212 = Coupling(name = 'GC_212',
                  value = '(complex(0,1)*gw*KR2x2*KR5x2*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWR':1})

GC_213 = Coupling(name = 'GC_213',
                  value = '-0.5*(complex(0,1)*gw*KR5x2**2*sphi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1})

GC_214 = Coupling(name = 'GC_214',
                  value = '-0.5*(complex(0,1)*gw*KR3x3*KR6x3*sphi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1})

GC_215 = Coupling(name = 'GC_215',
                  value = '(complex(0,1)*gw*KR3x3*KR6x3*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw)',
                  order = {'EWR':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '-0.5*(complex(0,1)*gw*KR6x3**2*sphi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1})

GC_217 = Coupling(name = 'GC_217',
                  value = '-((cphi*g1*sw*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'QED':1})

GC_218 = Coupling(name = 'GC_218',
                  value = '-0.16666666666666666*(cphi*complex(0,1)*g1*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'QED':1})

GC_219 = Coupling(name = 'GC_219',
                  value = '(cphi*complex(0,1)*g1*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'QED':1})

GC_220 = Coupling(name = 'GC_220',
                  value = '(-2*cphi*complex(0,1)*g1**2*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'QED':2})

GC_221 = Coupling(name = 'GC_221',
                  value = '(-2*cphi*cth1**2*complex(0,1)*g1**2*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'QED':2})

GC_222 = Coupling(name = 'GC_222',
                  value = '-4*complex(0,1)*g1*gw*sw*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':1,'QED':1})

GC_223 = Coupling(name = 'GC_223',
                  value = '4*complex(0,1)*g1*gw*sw*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':1,'QED':1})

GC_224 = Coupling(name = 'GC_224',
                  value = '-4*cphi**2*complex(0,1)*g1*gw*sw*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':1,'QED':1})

GC_225 = Coupling(name = 'GC_225',
                  value = '4*cphi**2*complex(0,1)*g1*gw*sw*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':1,'QED':1})

GC_226 = Coupling(name = 'GC_226',
                  value = '-4*cth1**2*complex(0,1)*g1*gw*sw*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':1,'QED':1})

GC_227 = Coupling(name = 'GC_227',
                  value = '(-4*cphi**2*complex(0,1)*g1*gw*sw*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'QED':1})

GC_228 = Coupling(name = 'GC_228',
                  value = '(4*cphi**2*cth1**2*complex(0,1)*g1*gw*sw*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'QED':1})

GC_229 = Coupling(name = 'GC_229',
                  value = '(2*cphi*cxi*complex(0,1)*g1*gw*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'QED':1})

GC_230 = Coupling(name = 'GC_230',
                  value = '-((cphi*cxi*g1*gw*sw*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWL':1,'QED':1})

GC_231 = Coupling(name = 'GC_231',
                  value = '-((cphi*cxi*complex(0,1)*g1*gw*sw*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWL':1,'QED':1})

GC_232 = Coupling(name = 'GC_232',
                  value = '(cphi*cxi*g1*gw*sw*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'QED':1})

GC_233 = Coupling(name = 'GC_233',
                  value = '(2*cphi*complex(0,1)*gw**2*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':2})

GC_234 = Coupling(name = 'GC_234',
                  value = '(2*cphi*cth1**2*complex(0,1)*gw**2*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':2})

GC_235 = Coupling(name = 'GC_235',
                  value = '(-2*cphi*cxi**2*complex(0,1)*gw**2*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':2})

GC_236 = Coupling(name = 'GC_236',
                  value = '-((g1*sphi*sw*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_237 = Coupling(name = 'GC_237',
                  value = '-0.16666666666666666*(complex(0,1)*g1*sphi*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_238 = Coupling(name = 'GC_238',
                  value = '(complex(0,1)*g1*sphi*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_239 = Coupling(name = 'GC_239',
                  value = '(2*complex(0,1)*g1**2*sphi*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_240 = Coupling(name = 'GC_240',
                  value = '(2*cth1**2*complex(0,1)*g1**2*sphi*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_241 = Coupling(name = 'GC_241',
                  value = '-4*cphi*complex(0,1)*g1*gw*sphi*sw*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWR':1,'QED':1})

GC_242 = Coupling(name = 'GC_242',
                  value = '4*cphi*complex(0,1)*g1*gw*sphi*sw*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWR':1,'QED':1})

GC_243 = Coupling(name = 'GC_243',
                  value = '(2*cxi*complex(0,1)*g1*gw*sphi*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1,'QED':1})

GC_244 = Coupling(name = 'GC_244',
                  value = '-((cxi*g1*gw*sphi*sw*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWR':1,'QED':1})

GC_245 = Coupling(name = 'GC_245',
                  value = '-((cxi*complex(0,1)*g1*gw*sphi*sw*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWR':1,'QED':1})

GC_246 = Coupling(name = 'GC_246',
                  value = '(cxi*g1*gw*sphi*sw*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1,'QED':1})

GC_247 = Coupling(name = 'GC_247',
                  value = '(-2*complex(0,1)*gw**2*sphi*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'EWR':1})

GC_248 = Coupling(name = 'GC_248',
                  value = '(-2*cth1**2*complex(0,1)*gw**2*sphi*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'EWR':1})

GC_249 = Coupling(name = 'GC_249',
                  value = '(2*cxi**2*complex(0,1)*gw**2*sphi*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'EWR':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '-4*complex(0,1)*g1*gw*sphi**2*sw*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_251 = Coupling(name = 'GC_251',
                  value = '4*complex(0,1)*g1*gw*sphi**2*sw*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_252 = Coupling(name = 'GC_252',
                  value = '(-4*complex(0,1)*g1*gw*sphi**2*sw*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_253 = Coupling(name = 'GC_253',
                  value = '(4*cth1**2*complex(0,1)*g1*gw*sphi**2*sw*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_254 = Coupling(name = 'GC_254',
                  value = '(2*cphi*cth1*complex(0,1)*g1**2*sth1*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'QED':2})

GC_255 = Coupling(name = 'GC_255',
                  value = '4*cth1*complex(0,1)*g1*gw*sth1*sw*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':1,'QED':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '(-4*cphi**2*cth1*complex(0,1)*g1*gw*sth1*sw*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'QED':1})

GC_257 = Coupling(name = 'GC_257',
                  value = '(-2*cphi*cth1*complex(0,1)*gw**2*sth1*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':2})

GC_258 = Coupling(name = 'GC_258',
                  value = '(-2*cth1*complex(0,1)*g1**2*sphi*sth1*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_259 = Coupling(name = 'GC_259',
                  value = '(2*cth1*complex(0,1)*gw**2*sphi*sth1*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'EWR':1})

GC_260 = Coupling(name = 'GC_260',
                  value = '(-4*cth1*complex(0,1)*g1*gw*sphi**2*sth1*sw*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_261 = Coupling(name = 'GC_261',
                  value = '(-2*cphi*complex(0,1)*g1**2*sth1**2*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'QED':2})

GC_262 = Coupling(name = 'GC_262',
                  value = '-4*complex(0,1)*g1*gw*sth1**2*sw*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':1,'QED':1})

GC_263 = Coupling(name = 'GC_263',
                  value = '(4*cphi**2*complex(0,1)*g1*gw*sth1**2*sw*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'QED':1})

GC_264 = Coupling(name = 'GC_264',
                  value = '(2*cphi*complex(0,1)*gw**2*sth1**2*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':2})

GC_265 = Coupling(name = 'GC_265',
                  value = '(2*complex(0,1)*g1**2*sphi*sth1**2*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_266 = Coupling(name = 'GC_266',
                  value = '(-2*complex(0,1)*gw**2*sphi*sth1**2*sw*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'EWR':1})

GC_267 = Coupling(name = 'GC_267',
                  value = '(4*complex(0,1)*g1*gw*sphi**2*sth1**2*sw*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_268 = Coupling(name = 'GC_268',
                  value = '(2*cphi**2*complex(0,1)*g1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'QED':2})

GC_269 = Coupling(name = 'GC_269',
                  value = '(2*cphi**2*cth1**2*complex(0,1)*g1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'QED':2})

GC_270 = Coupling(name = 'GC_270',
                  value = '(-4*cphi*complex(0,1)*g1*gw*sw**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'QED':1})

GC_271 = Coupling(name = 'GC_271',
                  value = '(4*cphi*cth1**2*complex(0,1)*g1*gw*sw**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'QED':1})

GC_272 = Coupling(name = 'GC_272',
                  value = '(-2*cphi**2*complex(0,1)*gw**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':2})

GC_273 = Coupling(name = 'GC_273',
                  value = '-((cphi**2*cxi**2*complex(0,1)*gw**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2)',
                  order = {'EWL':2})

GC_274 = Coupling(name = 'GC_274',
                  value = '(-4*cphi*complex(0,1)*g1**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_275 = Coupling(name = 'GC_275',
                  value = '(4*cphi*complex(0,1)*g1**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_276 = Coupling(name = 'GC_276',
                  value = '(-4*cphi*cth1**2*complex(0,1)*g1**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_277 = Coupling(name = 'GC_277',
                  value = '(4*cphi*cth1**2*complex(0,1)*g1**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_278 = Coupling(name = 'GC_278',
                  value = '(-4*complex(0,1)*g1*gw*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1,'QED':1})

GC_279 = Coupling(name = 'GC_279',
                  value = '(4*cth1**2*complex(0,1)*g1*gw*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1,'QED':1})

GC_280 = Coupling(name = 'GC_280',
                  value = '(-4*cphi*complex(0,1)*gw**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'EWR':1})

GC_281 = Coupling(name = 'GC_281',
                  value = '(4*cphi*complex(0,1)*gw**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'EWR':1})

GC_282 = Coupling(name = 'GC_282',
                  value = '(-2*cphi*cxi**2*complex(0,1)*gw**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'EWR':1})

GC_283 = Coupling(name = 'GC_283',
                  value = '(2*cphi*cxi**2*complex(0,1)*gw**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'EWR':1})

GC_284 = Coupling(name = 'GC_284',
                  value = '(-2*complex(0,1)*g1**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-2,'EWR':2,'QED':2})

GC_285 = Coupling(name = 'GC_285',
                  value = '(-2*cth1**2*complex(0,1)*g1**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-2,'EWR':2,'QED':2})

GC_286 = Coupling(name = 'GC_286',
                  value = '(2*complex(0,1)*gw**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWR':2})

GC_287 = Coupling(name = 'GC_287',
                  value = '(cxi**2*complex(0,1)*gw**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWR':2})

GC_288 = Coupling(name = 'GC_288',
                  value = '(-2*cphi**2*cth1*complex(0,1)*g1**2*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'QED':2})

GC_289 = Coupling(name = 'GC_289',
                  value = '(-4*cphi*cth1*complex(0,1)*g1*gw*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'QED':1})

GC_290 = Coupling(name = 'GC_290',
                  value = '(-4*cphi*cth1*complex(0,1)*g1**2*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_291 = Coupling(name = 'GC_291',
                  value = '(4*cphi*cth1*complex(0,1)*g1**2*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_292 = Coupling(name = 'GC_292',
                  value = '(-4*cth1*complex(0,1)*g1*gw*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1,'QED':1})

GC_293 = Coupling(name = 'GC_293',
                  value = '(2*cth1*complex(0,1)*g1**2*sphi**2*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-2,'EWR':2,'QED':2})

GC_294 = Coupling(name = 'GC_294',
                  value = '(2*cphi**2*complex(0,1)*g1**2*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'QED':2})

GC_295 = Coupling(name = 'GC_295',
                  value = '(4*cphi*complex(0,1)*g1*gw*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'QED':1})

GC_296 = Coupling(name = 'GC_296',
                  value = '(-4*cphi*complex(0,1)*g1**2*sphi*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_297 = Coupling(name = 'GC_297',
                  value = '(4*cphi*complex(0,1)*g1**2*sphi*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_298 = Coupling(name = 'GC_298',
                  value = '(4*complex(0,1)*g1*gw*sphi*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1,'QED':1})

GC_299 = Coupling(name = 'GC_299',
                  value = '(-2*complex(0,1)*g1**2*sphi**2*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-2,'EWR':2,'QED':2})

GC_300 = Coupling(name = 'GC_300',
                  value = '(4*cphi**2*complex(0,1)*g1*gw*sw**3*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'QED':1})

GC_301 = Coupling(name = 'GC_301',
                  value = '(-4*cphi**2*cth1**2*complex(0,1)*g1*gw*sw**3*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'QED':1})

GC_302 = Coupling(name = 'GC_302',
                  value = '(4*complex(0,1)*g1*gw*sphi**2*sw**3*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_303 = Coupling(name = 'GC_303',
                  value = '(-4*cth1**2*complex(0,1)*g1*gw*sphi**2*sw**3*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_304 = Coupling(name = 'GC_304',
                  value = '(4*cphi**2*cth1*complex(0,1)*g1*gw*sth1*sw**3*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'QED':1})

GC_305 = Coupling(name = 'GC_305',
                  value = '(4*cth1*complex(0,1)*g1*gw*sphi**2*sth1*sw**3*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_306 = Coupling(name = 'GC_306',
                  value = '(-4*cphi**2*complex(0,1)*g1*gw*sth1**2*sw**3*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'QED':1})

GC_307 = Coupling(name = 'GC_307',
                  value = '(-4*complex(0,1)*g1*gw*sphi**2*sth1**2*sw**3*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_308 = Coupling(name = 'GC_308',
                  value = '2*complex(0,1)*g1**2 - 4*complex(0,1)*g1**2*sw**2',
                  order = {'QED':2})

GC_309 = Coupling(name = 'GC_309',
                  value = '2*cth1**2*complex(0,1)*g1**2 - 4*cth1**2*complex(0,1)*g1**2*sw**2',
                  order = {'QED':2})

GC_310 = Coupling(name = 'GC_310',
                  value = '(cphi*cw*complex(0,1)*gw)/2. - (cphi*complex(0,1)*gw*sw**2)/(6.*cw)',
                  order = {'EWL':1})

GC_311 = Coupling(name = 'GC_311',
                  value = '-0.5*(cphi*cw*complex(0,1)*gw) + (cphi*complex(0,1)*gw*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_312 = Coupling(name = 'GC_312',
                  value = '(2*cphi*complex(0,1)*g1*gw)/cw - (6*cphi*complex(0,1)*g1*gw*sw**2)/cw',
                  order = {'EWL':1,'QED':1})

GC_313 = Coupling(name = 'GC_313',
                  value = '(-2*cphi*cth1**2*complex(0,1)*g1*gw)/cw + (6*cphi*cth1**2*complex(0,1)*g1*gw*sw**2)/cw',
                  order = {'EWL':1,'QED':1})

GC_314 = Coupling(name = 'GC_314',
                  value = '(2*cphi**2*complex(0,1)*gw**2)/cw**2 - (4*cphi**2*complex(0,1)*gw**2*sw**2)/cw**2',
                  order = {'EWL':2})

GC_315 = Coupling(name = 'GC_315',
                  value = '(cphi**2*cxi**2*complex(0,1)*gw**2)/cw**2 - (2*cphi**2*cxi**2*complex(0,1)*gw**2*sw**2)/cw**2',
                  order = {'EWL':2})

GC_316 = Coupling(name = 'GC_316',
                  value = '-0.5*(cphi*cw*complex(0,1)*gw*KL1x1**2) - (cphi*complex(0,1)*gw*KL1x1**2*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_317 = Coupling(name = 'GC_317',
                  value = '-0.5*(cphi*cw*complex(0,1)*gw*KL2x2**2) - (cphi*complex(0,1)*gw*KL2x2**2*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_318 = Coupling(name = 'GC_318',
                  value = '-0.5*(cphi*cw*complex(0,1)*gw*KL3x3**2) - (cphi*complex(0,1)*gw*KL3x3**2*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_319 = Coupling(name = 'GC_319',
                  value = '-0.5*(cphi*cw*complex(0,1)*gw*KL1x1*KL4x1) - (cphi*complex(0,1)*gw*KL1x1*KL4x1*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_320 = Coupling(name = 'GC_320',
                  value = '(cphi*cw*complex(0,1)*gw*KL1x1*KL4x1)/2. + (cphi*complex(0,1)*gw*KL1x1*KL4x1*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_321 = Coupling(name = 'GC_321',
                  value = '-0.5*(cphi*cw*complex(0,1)*gw*KL4x1**2) - (cphi*complex(0,1)*gw*KL4x1**2*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_322 = Coupling(name = 'GC_322',
                  value = '-0.5*(cphi*cw*complex(0,1)*gw*KL2x2*KL5x2) - (cphi*complex(0,1)*gw*KL2x2*KL5x2*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_323 = Coupling(name = 'GC_323',
                  value = '(cphi*cw*complex(0,1)*gw*KL2x2*KL5x2)/2. + (cphi*complex(0,1)*gw*KL2x2*KL5x2*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_324 = Coupling(name = 'GC_324',
                  value = '-0.5*(cphi*cw*complex(0,1)*gw*KL5x2**2) - (cphi*complex(0,1)*gw*KL5x2**2*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_325 = Coupling(name = 'GC_325',
                  value = '-0.5*(cphi*cw*complex(0,1)*gw*KL3x3*KL6x3) - (cphi*complex(0,1)*gw*KL3x3*KL6x3*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_326 = Coupling(name = 'GC_326',
                  value = '(cphi*cw*complex(0,1)*gw*KL3x3*KL6x3)/2. + (cphi*complex(0,1)*gw*KL3x3*KL6x3*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_327 = Coupling(name = 'GC_327',
                  value = '-0.5*(cphi*cw*complex(0,1)*gw*KL6x3**2) - (cphi*complex(0,1)*gw*KL6x3**2*sw**2)/(2.*cw)',
                  order = {'EWL':1})

GC_328 = Coupling(name = 'GC_328',
                  value = '(cw*complex(0,1)*gw*sphi)/2. - (complex(0,1)*gw*sphi*sw**2)/(6.*cw)',
                  order = {'EWR':1})

GC_329 = Coupling(name = 'GC_329',
                  value = '-0.5*(cw*complex(0,1)*gw*sphi) + (complex(0,1)*gw*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_330 = Coupling(name = 'GC_330',
                  value = '(-2*complex(0,1)*g1*gw*sphi)/cw + (6*complex(0,1)*g1*gw*sphi*sw**2)/cw',
                  order = {'EWR':1,'QED':1})

GC_331 = Coupling(name = 'GC_331',
                  value = '(2*cth1**2*complex(0,1)*g1*gw*sphi)/cw - (6*cth1**2*complex(0,1)*g1*gw*sphi*sw**2)/cw',
                  order = {'EWR':1,'QED':1})

GC_332 = Coupling(name = 'GC_332',
                  value = '-0.5*(cw*complex(0,1)*gw*KL1x1**2*sphi) - (complex(0,1)*gw*KL1x1**2*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_333 = Coupling(name = 'GC_333',
                  value = '-0.5*(cw*complex(0,1)*gw*KL2x2**2*sphi) - (complex(0,1)*gw*KL2x2**2*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_334 = Coupling(name = 'GC_334',
                  value = '-0.5*(cw*complex(0,1)*gw*KL3x3**2*sphi) - (complex(0,1)*gw*KL3x3**2*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_335 = Coupling(name = 'GC_335',
                  value = '-0.5*(cw*complex(0,1)*gw*KL1x1*KL4x1*sphi) - (complex(0,1)*gw*KL1x1*KL4x1*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_336 = Coupling(name = 'GC_336',
                  value = '(cw*complex(0,1)*gw*KL1x1*KL4x1*sphi)/2. + (complex(0,1)*gw*KL1x1*KL4x1*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_337 = Coupling(name = 'GC_337',
                  value = '-0.5*(cw*complex(0,1)*gw*KL4x1**2*sphi) - (complex(0,1)*gw*KL4x1**2*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_338 = Coupling(name = 'GC_338',
                  value = '-0.5*(cw*complex(0,1)*gw*KL2x2*KL5x2*sphi) - (complex(0,1)*gw*KL2x2*KL5x2*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_339 = Coupling(name = 'GC_339',
                  value = '(cw*complex(0,1)*gw*KL2x2*KL5x2*sphi)/2. + (complex(0,1)*gw*KL2x2*KL5x2*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_340 = Coupling(name = 'GC_340',
                  value = '-0.5*(cw*complex(0,1)*gw*KL5x2**2*sphi) - (complex(0,1)*gw*KL5x2**2*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_341 = Coupling(name = 'GC_341',
                  value = '-0.5*(cw*complex(0,1)*gw*KL3x3*KL6x3*sphi) - (complex(0,1)*gw*KL3x3*KL6x3*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_342 = Coupling(name = 'GC_342',
                  value = '(cw*complex(0,1)*gw*KL3x3*KL6x3*sphi)/2. + (complex(0,1)*gw*KL3x3*KL6x3*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_343 = Coupling(name = 'GC_343',
                  value = '-0.5*(cw*complex(0,1)*gw*KL6x3**2*sphi) - (complex(0,1)*gw*KL6x3**2*sphi*sw**2)/(2.*cw)',
                  order = {'EWR':1})

GC_344 = Coupling(name = 'GC_344',
                  value = '(2*complex(0,1)*gw**2*sphi**2)/cw**2 - (4*complex(0,1)*gw**2*sphi**2*sw**2)/cw**2',
                  order = {'EWR':2})

GC_345 = Coupling(name = 'GC_345',
                  value = '-2*cth1*complex(0,1)*g1**2*sth1 + 4*cth1*complex(0,1)*g1**2*sth1*sw**2',
                  order = {'QED':2})

GC_346 = Coupling(name = 'GC_346',
                  value = '(2*cphi*cth1*complex(0,1)*g1*gw*sth1)/cw - (6*cphi*cth1*complex(0,1)*g1*gw*sth1*sw**2)/cw',
                  order = {'EWL':1,'QED':1})

GC_347 = Coupling(name = 'GC_347',
                  value = '(-2*cth1*complex(0,1)*g1*gw*sphi*sth1)/cw + (6*cth1*complex(0,1)*g1*gw*sphi*sth1*sw**2)/cw',
                  order = {'EWR':1,'QED':1})

GC_348 = Coupling(name = 'GC_348',
                  value = '2*complex(0,1)*g1**2*sth1**2 - 4*complex(0,1)*g1**2*sth1**2*sw**2',
                  order = {'QED':2})

GC_349 = Coupling(name = 'GC_349',
                  value = '(-2*cphi*complex(0,1)*g1*gw*sth1**2)/cw + (6*cphi*complex(0,1)*g1*gw*sth1**2*sw**2)/cw',
                  order = {'EWL':1,'QED':1})

GC_350 = Coupling(name = 'GC_350',
                  value = '(2*complex(0,1)*g1*gw*sphi*sth1**2)/cw - (6*complex(0,1)*g1*gw*sphi*sth1**2*sw**2)/cw',
                  order = {'EWR':1,'QED':1})

GC_351 = Coupling(name = 'GC_351',
                  value = '(-2*cphi*complex(0,1)*g1**2*sw)/cw + (4*cphi*complex(0,1)*g1**2*sw**3)/cw',
                  order = {'QED':2})

GC_352 = Coupling(name = 'GC_352',
                  value = '(-2*cphi*cth1**2*complex(0,1)*g1**2*sw)/cw + (4*cphi*cth1**2*complex(0,1)*g1**2*sw**3)/cw',
                  order = {'QED':2})

GC_353 = Coupling(name = 'GC_353',
                  value = '(-2*cphi**2*complex(0,1)*g1*gw*sw)/cw**2 + (6*cphi**2*complex(0,1)*g1*gw*sw**3)/cw**2',
                  order = {'EWL':1,'QED':1})

GC_354 = Coupling(name = 'GC_354',
                  value = '(2*cphi**2*cth1**2*complex(0,1)*g1*gw*sw)/cw**2 - (6*cphi**2*cth1**2*complex(0,1)*g1*gw*sw**3)/cw**2',
                  order = {'EWL':1,'QED':1})

GC_355 = Coupling(name = 'GC_355',
                  value = '(-2*complex(0,1)*g1**2*sphi*sw)/cw + (4*complex(0,1)*g1**2*sphi*sw**3)/cw',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_356 = Coupling(name = 'GC_356',
                  value = '(-2*cth1**2*complex(0,1)*g1**2*sphi*sw)/cw + (4*cth1**2*complex(0,1)*g1**2*sphi*sw**3)/cw',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_357 = Coupling(name = 'GC_357',
                  value = '(4*cphi*complex(0,1)*g1*gw*sphi*sw)/cw**2 - (12*cphi*complex(0,1)*g1*gw*sphi*sw**3)/cw**2',
                  order = {'EWR':1,'QED':1})

GC_358 = Coupling(name = 'GC_358',
                  value = '(-4*cphi*complex(0,1)*g1*gw*sphi*sw)/cw**2 + (12*cphi*complex(0,1)*g1*gw*sphi*sw**3)/cw**2',
                  order = {'EWR':1,'QED':1})

GC_359 = Coupling(name = 'GC_359',
                  value = '(4*cphi*cth1**2*complex(0,1)*g1*gw*sphi*sw)/cw**2 - (12*cphi*cth1**2*complex(0,1)*g1*gw*sphi*sw**3)/cw**2',
                  order = {'EWR':1,'QED':1})

GC_360 = Coupling(name = 'GC_360',
                  value = '(-4*cphi*cth1**2*complex(0,1)*g1*gw*sphi*sw)/cw**2 + (12*cphi*cth1**2*complex(0,1)*g1*gw*sphi*sw**3)/cw**2',
                  order = {'EWR':1,'QED':1})

GC_361 = Coupling(name = 'GC_361',
                  value = '(2*complex(0,1)*g1*gw*sphi**2*sw)/cw**2 - (6*complex(0,1)*g1*gw*sphi**2*sw**3)/cw**2',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_362 = Coupling(name = 'GC_362',
                  value = '(-2*cth1**2*complex(0,1)*g1*gw*sphi**2*sw)/cw**2 + (6*cth1**2*complex(0,1)*g1*gw*sphi**2*sw**3)/cw**2',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_363 = Coupling(name = 'GC_363',
                  value = '(2*cphi*cth1*complex(0,1)*g1**2*sth1*sw)/cw - (4*cphi*cth1*complex(0,1)*g1**2*sth1*sw**3)/cw',
                  order = {'QED':2})

GC_364 = Coupling(name = 'GC_364',
                  value = '(-2*cphi**2*cth1*complex(0,1)*g1*gw*sth1*sw)/cw**2 + (6*cphi**2*cth1*complex(0,1)*g1*gw*sth1*sw**3)/cw**2',
                  order = {'EWL':1,'QED':1})

GC_365 = Coupling(name = 'GC_365',
                  value = '(2*cth1*complex(0,1)*g1**2*sphi*sth1*sw)/cw - (4*cth1*complex(0,1)*g1**2*sphi*sth1*sw**3)/cw',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_366 = Coupling(name = 'GC_366',
                  value = '(4*cphi*cth1*complex(0,1)*g1*gw*sphi*sth1*sw)/cw**2 - (12*cphi*cth1*complex(0,1)*g1*gw*sphi*sth1*sw**3)/cw**2',
                  order = {'EWR':1,'QED':1})

GC_367 = Coupling(name = 'GC_367',
                  value = '(-4*cphi*cth1*complex(0,1)*g1*gw*sphi*sth1*sw)/cw**2 + (12*cphi*cth1*complex(0,1)*g1*gw*sphi*sth1*sw**3)/cw**2',
                  order = {'EWR':1,'QED':1})

GC_368 = Coupling(name = 'GC_368',
                  value = '(2*cth1*complex(0,1)*g1*gw*sphi**2*sth1*sw)/cw**2 - (6*cth1*complex(0,1)*g1*gw*sphi**2*sth1*sw**3)/cw**2',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_369 = Coupling(name = 'GC_369',
                  value = '(-2*cphi*complex(0,1)*g1**2*sth1**2*sw)/cw + (4*cphi*complex(0,1)*g1**2*sth1**2*sw**3)/cw',
                  order = {'QED':2})

GC_370 = Coupling(name = 'GC_370',
                  value = '(2*cphi**2*complex(0,1)*g1*gw*sth1**2*sw)/cw**2 - (6*cphi**2*complex(0,1)*g1*gw*sth1**2*sw**3)/cw**2',
                  order = {'EWL':1,'QED':1})

GC_371 = Coupling(name = 'GC_371',
                  value = '(-2*complex(0,1)*g1**2*sphi*sth1**2*sw)/cw + (4*complex(0,1)*g1**2*sphi*sth1**2*sw**3)/cw',
                  order = {'EWL':-1,'EWR':1,'QED':2})

GC_372 = Coupling(name = 'GC_372',
                  value = '(4*cphi*complex(0,1)*g1*gw*sphi*sth1**2*sw)/cw**2 - (12*cphi*complex(0,1)*g1*gw*sphi*sth1**2*sw**3)/cw**2',
                  order = {'EWR':1,'QED':1})

GC_373 = Coupling(name = 'GC_373',
                  value = '(-4*cphi*complex(0,1)*g1*gw*sphi*sth1**2*sw)/cw**2 + (12*cphi*complex(0,1)*g1*gw*sphi*sth1**2*sw**3)/cw**2',
                  order = {'EWR':1,'QED':1})

GC_374 = Coupling(name = 'GC_374',
                  value = '(-2*complex(0,1)*g1*gw*sphi**2*sth1**2*sw)/cw**2 + (6*complex(0,1)*g1*gw*sphi**2*sth1**2*sw**3)/cw**2',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_375 = Coupling(name = 'GC_375',
                  value = '(2*cphi**2*complex(0,1)*g1**2*sw**2)/cw**2 - (4*cphi**2*complex(0,1)*g1**2*sw**4)/cw**2',
                  order = {'QED':2})

GC_376 = Coupling(name = 'GC_376',
                  value = '(2*cphi**2*cth1**2*complex(0,1)*g1**2*sw**2)/cw**2 - (4*cphi**2*cth1**2*complex(0,1)*g1**2*sw**4)/cw**2',
                  order = {'QED':2})

GC_377 = Coupling(name = 'GC_377',
                  value = '(-2*cphi*complex(0,1)*gw**2*sphi)/cw**2 + (4*cphi*complex(0,1)*gw**2*sphi*sw**2)/cw**2 + (2*cphi*complex(0,1)*gw**2*sphi*sw**4)/cw**2',
                  order = {'EWL':1,'EWR':1})

GC_378 = Coupling(name = 'GC_378',
                  value = '-((cphi*cxi**2*complex(0,1)*gw**2*sphi)/cw**2) + (2*cphi*cxi**2*complex(0,1)*gw**2*sphi*sw**2)/cw**2 + (cphi*cxi**2*complex(0,1)*gw**2*sphi*sw**4)/cw**2',
                  order = {'EWL':1,'EWR':1})

GC_379 = Coupling(name = 'GC_379',
                  value = '(2*complex(0,1)*g1**2*sphi**2*sw**2)/cw**2 - (4*complex(0,1)*g1**2*sphi**2*sw**4)/cw**2',
                  order = {'EWL':-2,'EWR':2,'QED':2})

GC_380 = Coupling(name = 'GC_380',
                  value = '(2*cth1**2*complex(0,1)*g1**2*sphi**2*sw**2)/cw**2 - (4*cth1**2*complex(0,1)*g1**2*sphi**2*sw**4)/cw**2',
                  order = {'EWL':-2,'EWR':2,'QED':2})

GC_381 = Coupling(name = 'GC_381',
                  value = '(-2*cphi**2*cth1*complex(0,1)*g1**2*sth1*sw**2)/cw**2 + (4*cphi**2*cth1*complex(0,1)*g1**2*sth1*sw**4)/cw**2',
                  order = {'QED':2})

GC_382 = Coupling(name = 'GC_382',
                  value = '(-2*cth1*complex(0,1)*g1**2*sphi**2*sth1*sw**2)/cw**2 + (4*cth1*complex(0,1)*g1**2*sphi**2*sth1*sw**4)/cw**2',
                  order = {'EWL':-2,'EWR':2,'QED':2})

GC_383 = Coupling(name = 'GC_383',
                  value = '(2*cphi**2*complex(0,1)*g1**2*sth1**2*sw**2)/cw**2 - (4*cphi**2*complex(0,1)*g1**2*sth1**2*sw**4)/cw**2',
                  order = {'QED':2})

GC_384 = Coupling(name = 'GC_384',
                  value = '(2*complex(0,1)*g1**2*sphi**2*sth1**2*sw**2)/cw**2 - (4*complex(0,1)*g1**2*sphi**2*sth1**2*sw**4)/cw**2',
                  order = {'EWL':-2,'EWR':2,'QED':2})

GC_385 = Coupling(name = 'GC_385',
                  value = '2*cphi*cw*complex(0,1)*g1*gw*cmath.sqrt(1 - 2*sw**2) - (2*cphi*complex(0,1)*g1*gw*sw**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'QED':1})

GC_386 = Coupling(name = 'GC_386',
                  value = '-2*cphi*cw*complex(0,1)*g1*gw*cmath.sqrt(1 - 2*sw**2) + (2*cphi*complex(0,1)*g1*gw*sw**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'QED':1})

GC_387 = Coupling(name = 'GC_387',
                  value = '2*cw*complex(0,1)*g1*gw*sphi*cmath.sqrt(1 - 2*sw**2) - (2*complex(0,1)*g1*gw*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1,'QED':1})

GC_388 = Coupling(name = 'GC_388',
                  value = '-2*cw*complex(0,1)*g1*gw*sphi*cmath.sqrt(1 - 2*sw**2) + (2*complex(0,1)*g1*gw*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1,'QED':1})

GC_389 = Coupling(name = 'GC_389',
                  value = '(4*cphi*complex(0,1)*g1*gw*sphi*sw*cmath.sqrt(1 - 2*sw**2))/cw**2 + (4*cphi*complex(0,1)*g1*gw*sphi*sw**3*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWR':1,'QED':1})

GC_390 = Coupling(name = 'GC_390',
                  value = '(-4*cphi*cth1**2*complex(0,1)*g1*gw*sphi*sw*cmath.sqrt(1 - 2*sw**2))/cw**2 - (4*cphi*cth1**2*complex(0,1)*g1*gw*sphi*sw**3*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWR':1,'QED':1})

GC_391 = Coupling(name = 'GC_391',
                  value = '(4*cphi*cth1*complex(0,1)*g1*gw*sphi*sth1*sw*cmath.sqrt(1 - 2*sw**2))/cw**2 + (4*cphi*cth1*complex(0,1)*g1*gw*sphi*sth1*sw**3*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWR':1,'QED':1})

GC_392 = Coupling(name = 'GC_392',
                  value = '(-4*cphi*complex(0,1)*g1*gw*sphi*sth1**2*sw*cmath.sqrt(1 - 2*sw**2))/cw**2 - (4*cphi*complex(0,1)*g1*gw*sphi*sth1**2*sw**3*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWR':1,'QED':1})

GC_393 = Coupling(name = 'GC_393',
                  value = '-(complex(0,1)*gw*sxi)',
                  order = {'EWR':1})

GC_394 = Coupling(name = 'GC_394',
                  value = 'complex(0,1)*gw*sxi',
                  order = {'EWR':1})

GC_395 = Coupling(name = 'GC_395',
                  value = '-((complex(0,1)*gw*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_396 = Coupling(name = 'GC_396',
                  value = '(complex(0,1)*gw*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_397 = Coupling(name = 'GC_397',
                  value = '(gw*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_398 = Coupling(name = 'GC_398',
                  value = '(CKML1x1*complex(0,1)*gw*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_399 = Coupling(name = 'GC_399',
                  value = '(CKML1x2*complex(0,1)*gw*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_400 = Coupling(name = 'GC_400',
                  value = '(CKML1x3*complex(0,1)*gw*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_401 = Coupling(name = 'GC_401',
                  value = '(CKML2x1*complex(0,1)*gw*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_402 = Coupling(name = 'GC_402',
                  value = '(CKML2x2*complex(0,1)*gw*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_403 = Coupling(name = 'GC_403',
                  value = '(CKML2x3*complex(0,1)*gw*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_404 = Coupling(name = 'GC_404',
                  value = '(CKML3x1*complex(0,1)*gw*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_405 = Coupling(name = 'GC_405',
                  value = '(CKML3x2*complex(0,1)*gw*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_406 = Coupling(name = 'GC_406',
                  value = '(CKML3x3*complex(0,1)*gw*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_407 = Coupling(name = 'GC_407',
                  value = '-((CKMR1x1*complex(0,1)*gw*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_408 = Coupling(name = 'GC_408',
                  value = '-((CKMR1x2*complex(0,1)*gw*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_409 = Coupling(name = 'GC_409',
                  value = '-((CKMR1x3*complex(0,1)*gw*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_410 = Coupling(name = 'GC_410',
                  value = '-((CKMR2x1*complex(0,1)*gw*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_411 = Coupling(name = 'GC_411',
                  value = '-((CKMR2x2*complex(0,1)*gw*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_412 = Coupling(name = 'GC_412',
                  value = '-((CKMR2x3*complex(0,1)*gw*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_413 = Coupling(name = 'GC_413',
                  value = '-((CKMR3x1*complex(0,1)*gw*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_414 = Coupling(name = 'GC_414',
                  value = '-((CKMR3x2*complex(0,1)*gw*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_415 = Coupling(name = 'GC_415',
                  value = '-((CKMR3x3*complex(0,1)*gw*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_416 = Coupling(name = 'GC_416',
                  value = '-(cphi*cw*complex(0,1)*gw**2*sxi)',
                  order = {'EWL':1,'EWR':1})

GC_417 = Coupling(name = 'GC_417',
                  value = '-((cphi*cw*gw**2*sxi)/cmath.sqrt(2))',
                  order = {'EWL':1,'EWR':1})

GC_418 = Coupling(name = 'GC_418',
                  value = '-((cphi*cw*complex(0,1)*gw**2*sxi)/cmath.sqrt(2))',
                  order = {'EWL':1,'EWR':1})

GC_419 = Coupling(name = 'GC_419',
                  value = '(cphi*cw*gw**2*sxi)/cmath.sqrt(2)',
                  order = {'EWL':1,'EWR':1})

GC_420 = Coupling(name = 'GC_420',
                  value = '-(cxi*complex(0,1)*gw**2*sxi)',
                  order = {'EWL':1,'EWR':1})

GC_421 = Coupling(name = 'GC_421',
                  value = 'cxi*complex(0,1)*gw**2*sxi',
                  order = {'EWL':1,'EWR':1})

GC_422 = Coupling(name = 'GC_422',
                  value = '2*cxi*complex(0,1)*gw**2*sxi',
                  order = {'EWL':1,'EWR':1})

GC_423 = Coupling(name = 'GC_423',
                  value = '-(cxi*gw**2*sxi*cmath.sqrt(2))',
                  order = {'EWL':1,'EWR':1})

GC_424 = Coupling(name = 'GC_424',
                  value = '-(cxi*complex(0,1)*gw**2*sxi*cmath.sqrt(2))',
                  order = {'EWL':1,'EWR':1})

GC_425 = Coupling(name = 'GC_425',
                  value = 'cxi*gw**2*sxi*cmath.sqrt(2)',
                  order = {'EWL':1,'EWR':1})

GC_426 = Coupling(name = 'GC_426',
                  value = 'cth1*cxi*complex(0,1)*gw**2*sxi*cmath.sqrt(2)',
                  order = {'EWL':1,'EWR':1})

GC_427 = Coupling(name = 'GC_427',
                  value = '-(cth1**2*cxi*complex(0,1)*gw**2*sxi)',
                  order = {'EWL':1,'EWR':1})

GC_428 = Coupling(name = 'GC_428',
                  value = '-(cxi**3*complex(0,1)*gw**2*sxi)',
                  order = {'EWL':1,'EWR':1})

GC_429 = Coupling(name = 'GC_429',
                  value = 'cxi**3*complex(0,1)*gw**2*sxi',
                  order = {'EWL':1,'EWR':1})

GC_430 = Coupling(name = 'GC_430',
                  value = '(complex(0,1)*gw*KL1x1*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_431 = Coupling(name = 'GC_431',
                  value = '(complex(0,1)*gw*KL2x2*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_432 = Coupling(name = 'GC_432',
                  value = '(complex(0,1)*gw*KL3x3*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_433 = Coupling(name = 'GC_433',
                  value = '(complex(0,1)*gw*KL4x1*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_434 = Coupling(name = 'GC_434',
                  value = '(complex(0,1)*gw*KL5x2*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_435 = Coupling(name = 'GC_435',
                  value = '(complex(0,1)*gw*KL6x3*sxi)/cmath.sqrt(2)',
                  order = {'EWR':1})

GC_436 = Coupling(name = 'GC_436',
                  value = '-((complex(0,1)*gw*KR1x1*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_437 = Coupling(name = 'GC_437',
                  value = '-((complex(0,1)*gw*KR2x2*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_438 = Coupling(name = 'GC_438',
                  value = '-((complex(0,1)*gw*KR3x3*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_439 = Coupling(name = 'GC_439',
                  value = '-((complex(0,1)*gw*KR4x1*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_440 = Coupling(name = 'GC_440',
                  value = '-((complex(0,1)*gw*KR5x2*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_441 = Coupling(name = 'GC_441',
                  value = '-((complex(0,1)*gw*KR6x3*sxi)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_442 = Coupling(name = 'GC_442',
                  value = '-(cw*complex(0,1)*gw**2*sphi*sxi)',
                  order = {'EWR':2})

GC_443 = Coupling(name = 'GC_443',
                  value = '-((cw*gw**2*sphi*sxi)/cmath.sqrt(2))',
                  order = {'EWR':2})

GC_444 = Coupling(name = 'GC_444',
                  value = '-((cw*complex(0,1)*gw**2*sphi*sxi)/cmath.sqrt(2))',
                  order = {'EWR':2})

GC_445 = Coupling(name = 'GC_445',
                  value = '(cw*gw**2*sphi*sxi)/cmath.sqrt(2)',
                  order = {'EWR':2})

GC_446 = Coupling(name = 'GC_446',
                  value = '-(cxi*complex(0,1)*gw**2*sth1*sxi*cmath.sqrt(2))',
                  order = {'EWL':1,'EWR':1})

GC_447 = Coupling(name = 'GC_447',
                  value = 'cth1*cxi*complex(0,1)*gw**2*sth1*sxi',
                  order = {'EWL':1,'EWR':1})

GC_448 = Coupling(name = 'GC_448',
                  value = '-(cxi*complex(0,1)*gw**2*sth1**2*sxi)',
                  order = {'EWL':1,'EWR':1})

GC_449 = Coupling(name = 'GC_449',
                  value = '(2*cphi*complex(0,1)*g1*gw*sw*sxi)/cw',
                  order = {'EWR':1,'QED':1})

GC_450 = Coupling(name = 'GC_450',
                  value = '-((cphi*g1*gw*sw*sxi*cmath.sqrt(2))/cw)',
                  order = {'EWR':1,'QED':1})

GC_451 = Coupling(name = 'GC_451',
                  value = '-((cphi*complex(0,1)*g1*gw*sw*sxi*cmath.sqrt(2))/cw)',
                  order = {'EWR':1,'QED':1})

GC_452 = Coupling(name = 'GC_452',
                  value = '(cphi*g1*gw*sw*sxi*cmath.sqrt(2))/cw',
                  order = {'EWR':1,'QED':1})

GC_453 = Coupling(name = 'GC_453',
                  value = '-(complex(0,1)*gw**2*sw*sxi)',
                  order = {'EWL':1,'EWR':1})

GC_454 = Coupling(name = 'GC_454',
                  value = '-((gw**2*sw*sxi)/cmath.sqrt(2))',
                  order = {'EWL':1,'EWR':1})

GC_455 = Coupling(name = 'GC_455',
                  value = '-((complex(0,1)*gw**2*sw*sxi)/cmath.sqrt(2))',
                  order = {'EWL':1,'EWR':1})

GC_456 = Coupling(name = 'GC_456',
                  value = '(gw**2*sw*sxi)/cmath.sqrt(2)',
                  order = {'EWL':1,'EWR':1})

GC_457 = Coupling(name = 'GC_457',
                  value = '(-2*complex(0,1)*g1*gw*sphi*sw*sxi)/cw',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_458 = Coupling(name = 'GC_458',
                  value = '-((g1*gw*sphi*sw*sxi*cmath.sqrt(2))/cw)',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_459 = Coupling(name = 'GC_459',
                  value = '(complex(0,1)*g1*gw*sphi*sw*sxi*cmath.sqrt(2))/cw',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_460 = Coupling(name = 'GC_460',
                  value = '(g1*gw*sphi*sw*sxi*cmath.sqrt(2))/cw',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_461 = Coupling(name = 'GC_461',
                  value = '-((cphi*cxi*complex(0,1)*gw*sxi*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWR':1})

GC_462 = Coupling(name = 'GC_462',
                  value = '-2*complex(0,1)*g1*gw*sxi*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWR':1,'QED':1})

GC_463 = Coupling(name = 'GC_463',
                  value = '-(g1*gw*sxi*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))',
                  order = {'EWR':1,'QED':1})

GC_464 = Coupling(name = 'GC_464',
                  value = 'complex(0,1)*g1*gw*sxi*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWR':1,'QED':1})

GC_465 = Coupling(name = 'GC_465',
                  value = 'g1*gw*sxi*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWR':1,'QED':1})

GC_466 = Coupling(name = 'GC_466',
                  value = '(cxi*complex(0,1)*gw*sphi*sxi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'EWR':2})

GC_467 = Coupling(name = 'GC_467',
                  value = '(2*cphi*complex(0,1)*g1*gw*sw*sxi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1,'QED':1})

GC_468 = Coupling(name = 'GC_468',
                  value = '-((cphi*g1*gw*sw*sxi*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWR':1,'QED':1})

GC_469 = Coupling(name = 'GC_469',
                  value = '-((cphi*complex(0,1)*g1*gw*sw*sxi*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWR':1,'QED':1})

GC_470 = Coupling(name = 'GC_470',
                  value = '(cphi*g1*gw*sw*sxi*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':1,'QED':1})

GC_471 = Coupling(name = 'GC_471',
                  value = '(2*cphi*cxi*complex(0,1)*gw**2*sw*sxi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'EWR':1})

GC_472 = Coupling(name = 'GC_472',
                  value = '(2*complex(0,1)*g1*gw*sphi*sw*sxi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_473 = Coupling(name = 'GC_473',
                  value = '-((g1*gw*sphi*sw*sxi*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_474 = Coupling(name = 'GC_474',
                  value = '-((complex(0,1)*g1*gw*sphi*sw*sxi*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_475 = Coupling(name = 'GC_475',
                  value = '(g1*gw*sphi*sw*sxi*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'EWR':2,'QED':1})

GC_476 = Coupling(name = 'GC_476',
                  value = '(-2*cxi*complex(0,1)*gw**2*sphi*sw*sxi*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':2})

GC_477 = Coupling(name = 'GC_477',
                  value = '(cphi**2*cxi*complex(0,1)*gw**2*sw**2*sxi*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'EWR':1})

GC_478 = Coupling(name = 'GC_478',
                  value = '(-2*cphi*cxi*complex(0,1)*gw**2*sphi*sw**2*sxi*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWR':2})

GC_479 = Coupling(name = 'GC_479',
                  value = '(2*cphi*cxi*complex(0,1)*gw**2*sphi*sw**2*sxi*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWR':2})

GC_480 = Coupling(name = 'GC_480',
                  value = '-((cxi*complex(0,1)*gw**2*sphi**2*sw**2*sxi*cmath.sqrt(1 - 2*sw**2))/cw**2)',
                  order = {'EWL':-1,'EWR':3})

GC_481 = Coupling(name = 'GC_481',
                  value = 'cphi*cw*complex(0,1)*gw*sxi**2',
                  order = {'EWL':-1,'EWR':2})

GC_482 = Coupling(name = 'GC_482',
                  value = 'complex(0,1)*gw**2*sxi**2',
                  order = {'EWR':2})

GC_483 = Coupling(name = 'GC_483',
                  value = '2*complex(0,1)*gw**2*sxi**2',
                  order = {'EWR':2})

GC_484 = Coupling(name = 'GC_484',
                  value = '-(gw**2*sxi**2*cmath.sqrt(2))',
                  order = {'EWR':2})

GC_485 = Coupling(name = 'GC_485',
                  value = '-(complex(0,1)*gw**2*sxi**2*cmath.sqrt(2))',
                  order = {'EWR':2})

GC_486 = Coupling(name = 'GC_486',
                  value = 'gw**2*sxi**2*cmath.sqrt(2)',
                  order = {'EWR':2})

GC_487 = Coupling(name = 'GC_487',
                  value = '-(cth1*complex(0,1)*gw**2*sxi**2*cmath.sqrt(2))',
                  order = {'EWR':2})

GC_488 = Coupling(name = 'GC_488',
                  value = '-2*cxi**2*complex(0,1)*gw**2*sxi**2',
                  order = {'EWR':2})

GC_489 = Coupling(name = 'GC_489',
                  value = 'cw*complex(0,1)*gw*sphi*sxi**2',
                  order = {'EWL':-2,'EWR':3})

GC_490 = Coupling(name = 'GC_490',
                  value = 'cphi*cw**2*complex(0,1)*gw**2*sphi*sxi**2',
                  order = {'EWL':-1,'EWR':3})

GC_491 = Coupling(name = 'GC_491',
                  value = 'cw**2*complex(0,1)*gw**2*sphi**2*sxi**2',
                  order = {'EWL':-2,'EWR':4})

GC_492 = Coupling(name = 'GC_492',
                  value = 'complex(0,1)*gw**2*sth1*sxi**2*cmath.sqrt(2)',
                  order = {'EWR':2})

GC_493 = Coupling(name = 'GC_493',
                  value = 'complex(0,1)*gw*sw*sxi**2',
                  order = {'EWL':-1,'EWR':2})

GC_494 = Coupling(name = 'GC_494',
                  value = '-2*cphi*cw*complex(0,1)*gw**2*sw*sxi**2',
                  order = {'EWR':2})

GC_495 = Coupling(name = 'GC_495',
                  value = '-2*cw*complex(0,1)*gw**2*sphi*sw*sxi**2',
                  order = {'EWL':-1,'EWR':3})

GC_496 = Coupling(name = 'GC_496',
                  value = '-((cphi*complex(0,1)*gw*sw**2*sxi**2)/cw)',
                  order = {'EWL':-1,'EWR':2})

GC_497 = Coupling(name = 'GC_497',
                  value = 'complex(0,1)*gw**2*sw**2*sxi**2',
                  order = {'EWR':2})

GC_498 = Coupling(name = 'GC_498',
                  value = '-((complex(0,1)*gw*sphi*sw**2*sxi**2)/cw)',
                  order = {'EWL':-2,'EWR':3})

GC_499 = Coupling(name = 'GC_499',
                  value = '(2*cphi*complex(0,1)*gw**2*sw**3*sxi**2)/cw',
                  order = {'EWR':2})

GC_500 = Coupling(name = 'GC_500',
                  value = '(2*complex(0,1)*gw**2*sphi*sw**3*sxi**2)/cw',
                  order = {'EWL':-1,'EWR':3})

GC_501 = Coupling(name = 'GC_501',
                  value = '(cphi**2*complex(0,1)*gw**2*sw**4*sxi**2)/cw**2',
                  order = {'EWR':2})

GC_502 = Coupling(name = 'GC_502',
                  value = '(complex(0,1)*gw**2*sphi**2*sw**4*sxi**2)/cw**2',
                  order = {'EWL':-2,'EWR':4})

GC_503 = Coupling(name = 'GC_503',
                  value = '(cphi*complex(0,1)*gw*sxi**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'EWR':2})

GC_504 = Coupling(name = 'GC_504',
                  value = '-((complex(0,1)*gw*sphi*sxi**2*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWL':-2,'EWR':3})

GC_505 = Coupling(name = 'GC_505',
                  value = '(-2*cphi*complex(0,1)*gw**2*sw*sxi**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':2})

GC_506 = Coupling(name = 'GC_506',
                  value = '(2*complex(0,1)*gw**2*sphi*sw*sxi**2*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'EWR':3})

GC_507 = Coupling(name = 'GC_507',
                  value = '-((cphi**2*complex(0,1)*gw**2*sw**2*sxi**2*cmath.sqrt(1 - 2*sw**2))/cw**2)',
                  order = {'EWR':2})

GC_508 = Coupling(name = 'GC_508',
                  value = '(-2*cphi*complex(0,1)*gw**2*sphi*sw**2*sxi**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':3})

GC_509 = Coupling(name = 'GC_509',
                  value = '(2*cphi*complex(0,1)*gw**2*sphi*sw**2*sxi**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':3})

GC_510 = Coupling(name = 'GC_510',
                  value = '(complex(0,1)*gw**2*sphi**2*sw**2*sxi**2*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-2,'EWR':4})

GC_511 = Coupling(name = 'GC_511',
                  value = '-(cxi*complex(0,1)*gw**2*sxi**3)',
                  order = {'EWL':-1,'EWR':3})

GC_512 = Coupling(name = 'GC_512',
                  value = 'cxi*complex(0,1)*gw**2*sxi**3',
                  order = {'EWL':-1,'EWR':3})

GC_513 = Coupling(name = 'GC_513',
                  value = '-(complex(0,1)*gw**2*sxi**4)',
                  order = {'EWL':-2,'EWR':4})

GC_514 = Coupling(name = 'GC_514',
                  value = 'cphi*cw*cxi*complex(0,1)*gw*sxi + (cphi*cxi*complex(0,1)*gw*sw**2*sxi)/cw',
                  order = {'EWR':1})

GC_515 = Coupling(name = 'GC_515',
                  value = '-((cphi**2*cxi*complex(0,1)*gw**2*sxi)/cw**2) + (2*cphi**2*cxi*complex(0,1)*gw**2*sw**2*sxi)/cw**2',
                  order = {'EWL':1,'EWR':1})

GC_516 = Coupling(name = 'GC_516',
                  value = 'cw*cxi*complex(0,1)*gw*sphi*sxi + (cxi*complex(0,1)*gw*sphi*sw**2*sxi)/cw',
                  order = {'EWL':-1,'EWR':2})

GC_517 = Coupling(name = 'GC_517',
                  value = '-((cxi*complex(0,1)*gw**2*sphi**2*sxi)/cw**2) + (2*cxi*complex(0,1)*gw**2*sphi**2*sw**2*sxi)/cw**2',
                  order = {'EWL':-1,'EWR':3})

GC_518 = Coupling(name = 'GC_518',
                  value = '-2*cphi*cw*cxi*complex(0,1)*gw**2*sw*sxi - (2*cphi*cxi*complex(0,1)*gw**2*sw**3*sxi)/cw',
                  order = {'EWL':1,'EWR':1})

GC_519 = Coupling(name = 'GC_519',
                  value = '-2*cw*cxi*complex(0,1)*gw**2*sphi*sw*sxi - (2*cxi*complex(0,1)*gw**2*sphi*sw**3*sxi)/cw',
                  order = {'EWR':2})

GC_520 = Coupling(name = 'GC_520',
                  value = 'cphi**2*cw**2*cxi*complex(0,1)*gw**2*sxi - (cphi**2*cxi*complex(0,1)*gw**2*sw**4*sxi)/cw**2',
                  order = {'EWL':1,'EWR':1})

GC_521 = Coupling(name = 'GC_521',
                  value = '(cphi*cxi*complex(0,1)*gw**2*sphi*sxi)/cw**2 + cphi*cw**2*cxi*complex(0,1)*gw**2*sphi*sxi - (2*cphi*cxi*complex(0,1)*gw**2*sphi*sw**2*sxi)/cw**2 - (cphi*cxi*complex(0,1)*gw**2*sphi*sw**4*sxi)/cw**2',
                  order = {'EWR':2})

GC_522 = Coupling(name = 'GC_522',
                  value = 'cw**2*cxi*complex(0,1)*gw**2*sphi**2*sxi - (cxi*complex(0,1)*gw**2*sphi**2*sw**4*sxi)/cw**2',
                  order = {'EWL':-1,'EWR':3})

GC_523 = Coupling(name = 'GC_523',
                  value = '(cxi**2*complex(0,1)*gw**2*sphi**2)/cw**2 - (2*cxi**2*complex(0,1)*gw**2*sphi**2*sw**2)/cw**2 + cphi**2*cw**2*complex(0,1)*gw**2*sxi**2',
                  order = {'EWR':2})

GC_524 = Coupling(name = 'GC_524',
                  value = 'cw**2*cxi**2*complex(0,1)*gw**2*sphi**2 + (cphi**2*complex(0,1)*gw**2*sxi**2)/cw**2 - (2*cphi**2*complex(0,1)*gw**2*sw**2*sxi**2)/cw**2',
                  order = {'EWR':2})

GC_525 = Coupling(name = 'GC_525',
                  value = '(complex(0,1)*gw**2*sphi**2*sxi**2)/cw**2 - (2*complex(0,1)*gw**2*sphi**2*sw**2*sxi**2)/cw**2',
                  order = {'EWL':-2,'EWR':4})

GC_526 = Coupling(name = 'GC_526',
                  value = '-((cphi*complex(0,1)*gw**2*sphi*sxi**2)/cw**2) + (2*cphi*complex(0,1)*gw**2*sphi*sw**2*sxi**2)/cw**2 + (cphi*complex(0,1)*gw**2*sphi*sw**4*sxi**2)/cw**2',
                  order = {'EWL':-1,'EWR':3})

GC_527 = Coupling(name = 'GC_527',
                  value = '-0.5*(cxi*complex(0,1)*gw*k1**2)/vev**2 - (cxi*complex(0,1)*gw*k2**2)/(2.*vev**2)',
                  order = {'EWL':1})

GC_528 = Coupling(name = 'GC_528',
                  value = '(cxi*complex(0,1)*gw*k1**2)/(2.*vev**2) + (cxi*complex(0,1)*gw*k2**2)/(2.*vev**2)',
                  order = {'EWL':1})

GC_529 = Coupling(name = 'GC_529',
                  value = '(cxi*gw*k1**2)/(2.*vev**2) + (cxi*gw*k2**2)/(2.*vev**2)',
                  order = {'EWL':1})

GC_530 = Coupling(name = 'GC_530',
                  value = '(cth1*cxi*complex(0,1)*gw*k1**2)/(2.*vev**2) - (cth1*cxi*complex(0,1)*gw*k2**2)/(2.*vev**2)',
                  order = {'EWL':1})

GC_531 = Coupling(name = 'GC_531',
                  value = '-0.5*(cth1*cxi*complex(0,1)*gw*k1**2)/vev**2 + (cth1*cxi*complex(0,1)*gw*k2**2)/(2.*vev**2)',
                  order = {'EWL':1})

GC_532 = Coupling(name = 'GC_532',
                  value = '-0.5*(cphi*cth1*cw*cxi*complex(0,1)*gw**2*k1**2)/vev**2 + (cphi*cth1*cw*cxi*complex(0,1)*gw**2*k2**2)/(2.*vev**2)',
                  order = {'EWL':2})

GC_533 = Coupling(name = 'GC_533',
                  value = '(cxi**2*complex(0,1)*gw**2*k1**2)/(2.*vev**2) + (cxi**2*complex(0,1)*gw**2*k2**2)/(2.*vev**2)',
                  order = {'EWL':2})

GC_534 = Coupling(name = 'GC_534',
                  value = '(cth1*cxi**2*gw**2*k1**2)/(2.*vev**2) - (cth1*cxi**2*gw**2*k2**2)/(2.*vev**2)',
                  order = {'EWL':2})

GC_535 = Coupling(name = 'GC_535',
                  value = '-0.5*(cth1*cxi**2*complex(0,1)*gw**2*k1**2)/vev**2 + (cth1*cxi**2*complex(0,1)*gw**2*k2**2)/(2.*vev**2)',
                  order = {'EWL':2})

GC_536 = Coupling(name = 'GC_536',
                  value = '-0.5*(cth1*cxi**2*gw**2*k1**2)/vev**2 + (cth1*cxi**2*gw**2*k2**2)/(2.*vev**2)',
                  order = {'EWL':2})

GC_537 = Coupling(name = 'GC_537',
                  value = '(cth1**2*cxi**2*complex(0,1)*gw**2*k1**2)/(2.*vev**2) + (cth1**2*cxi**2*complex(0,1)*gw**2*k2**2)/(2.*vev**2)',
                  order = {'EWL':2})

GC_538 = Coupling(name = 'GC_538',
                  value = 'cxi**2*complex(0,1)*gw**2*sth1**2 + (cth1**2*cxi**2*complex(0,1)*gw**2*k1**2)/(2.*vev**2) + (cth1**2*cxi**2*complex(0,1)*gw**2*k2**2)/(2.*vev**2)',
                  order = {'EWL':2})

GC_539 = Coupling(name = 'GC_539',
                  value = '-0.5*(cth1*cw*cxi*complex(0,1)*gw**2*k1**2*sphi)/vev**2 + (cth1*cw*cxi*complex(0,1)*gw**2*k2**2*sphi)/(2.*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_540 = Coupling(name = 'GC_540',
                  value = '(cxi*complex(0,1)*gw*k1**2*sth1)/(2.*vev**2) - (cxi*complex(0,1)*gw*k2**2*sth1)/(2.*vev**2)',
                  order = {'EWL':1})

GC_541 = Coupling(name = 'GC_541',
                  value = '-0.5*(cxi*complex(0,1)*gw*k1**2*sth1)/vev**2 + (cxi*complex(0,1)*gw*k2**2*sth1)/(2.*vev**2)',
                  order = {'EWL':1})

GC_542 = Coupling(name = 'GC_542',
                  value = '-0.5*(cphi*cw*cxi*complex(0,1)*gw**2*k1**2*sth1)/vev**2 + (cphi*cw*cxi*complex(0,1)*gw**2*k2**2*sth1)/(2.*vev**2)',
                  order = {'EWL':2})

GC_543 = Coupling(name = 'GC_543',
                  value = '(cxi**2*gw**2*k1**2*sth1)/(2.*vev**2) - (cxi**2*gw**2*k2**2*sth1)/(2.*vev**2)',
                  order = {'EWL':2})

GC_544 = Coupling(name = 'GC_544',
                  value = '-0.5*(cxi**2*complex(0,1)*gw**2*k1**2*sth1)/vev**2 + (cxi**2*complex(0,1)*gw**2*k2**2*sth1)/(2.*vev**2)',
                  order = {'EWL':2})

GC_545 = Coupling(name = 'GC_545',
                  value = '-0.5*(cxi**2*gw**2*k1**2*sth1)/vev**2 + (cxi**2*gw**2*k2**2*sth1)/(2.*vev**2)',
                  order = {'EWL':2})

GC_546 = Coupling(name = 'GC_546',
                  value = '(cth1*cxi**2*complex(0,1)*gw**2*k1**2*sth1)/(2.*vev**2) + (cth1*cxi**2*complex(0,1)*gw**2*k2**2*sth1)/(2.*vev**2)',
                  order = {'EWL':2})

GC_547 = Coupling(name = 'GC_547',
                  value = '-(cth1*cxi**2*complex(0,1)*gw**2*sth1) + (cth1*cxi**2*complex(0,1)*gw**2*k1**2*sth1)/(2.*vev**2) + (cth1*cxi**2*complex(0,1)*gw**2*k2**2*sth1)/(2.*vev**2)',
                  order = {'EWL':2})

GC_548 = Coupling(name = 'GC_548',
                  value = '-0.5*(cw*cxi*complex(0,1)*gw**2*k1**2*sphi*sth1)/vev**2 + (cw*cxi*complex(0,1)*gw**2*k2**2*sphi*sth1)/(2.*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_549 = Coupling(name = 'GC_549',
                  value = '(cxi**2*complex(0,1)*gw**2*k1**2*sth1**2)/(2.*vev**2) + (cxi**2*complex(0,1)*gw**2*k2**2*sth1**2)/(2.*vev**2)',
                  order = {'EWL':2})

GC_550 = Coupling(name = 'GC_550',
                  value = 'cth1**2*cxi**2*complex(0,1)*gw**2 + (cxi**2*complex(0,1)*gw**2*k1**2*sth1**2)/(2.*vev**2) + (cxi**2*complex(0,1)*gw**2*k2**2*sth1**2)/(2.*vev**2)',
                  order = {'EWL':2})

GC_551 = Coupling(name = 'GC_551',
                  value = '-((complex(0,1)*gw*k1**2*sw)/vev**2) - (complex(0,1)*gw*k2**2*sw)/vev**2',
                  order = {'EWL':1})

GC_552 = Coupling(name = 'GC_552',
                  value = '-0.5*(cxi*gw**2*k1**2*sw)/vev**2 - (cxi*gw**2*k2**2*sw)/(2.*vev**2)',
                  order = {'EWL':2})

GC_553 = Coupling(name = 'GC_553',
                  value = '(cxi*complex(0,1)*gw**2*k1**2*sw)/(2.*vev**2) + (cxi*complex(0,1)*gw**2*k2**2*sw)/(2.*vev**2)',
                  order = {'EWL':2})

GC_554 = Coupling(name = 'GC_554',
                  value = '(cxi*gw**2*k1**2*sw)/(2.*vev**2) + (cxi*gw**2*k2**2*sw)/(2.*vev**2)',
                  order = {'EWL':2})

GC_555 = Coupling(name = 'GC_555',
                  value = '-0.5*(cth1*cxi*complex(0,1)*gw**2*k1**2*sw)/vev**2 + (cth1*cxi*complex(0,1)*gw**2*k2**2*sw)/(2.*vev**2)',
                  order = {'EWL':2})

GC_556 = Coupling(name = 'GC_556',
                  value = '-0.5*(cxi*complex(0,1)*gw**2*k1**2*sth1*sw)/vev**2 + (cxi*complex(0,1)*gw**2*k2**2*sth1*sw)/(2.*vev**2)',
                  order = {'EWL':2})

GC_557 = Coupling(name = 'GC_557',
                  value = '-0.5*(cphi*cw*gw*k1**2)/vev**2 - (cphi*cw*gw*k2**2)/(2.*vev**2) - (cphi*gw*k1**2*sw**2)/(2.*cw*vev**2) - (cphi*gw*k2**2*sw**2)/(2.*cw*vev**2)',
                  order = {'EWL':1})

GC_558 = Coupling(name = 'GC_558',
                  value = '-0.5*(cphi*cw*complex(0,1)*gw*k1**2)/vev**2 - (cphi*cw*complex(0,1)*gw*k2**2)/(2.*vev**2) + (cphi*complex(0,1)*gw*k1**2*sw**2)/(2.*cw*vev**2) + (cphi*complex(0,1)*gw*k2**2*sw**2)/(2.*cw*vev**2)',
                  order = {'EWL':1})

GC_559 = Coupling(name = 'GC_559',
                  value = '(2*complex(0,1)*gw**2*k1**2*sw**2)/vev**2 + (2*complex(0,1)*gw**2*k2**2*sw**2)/vev**2',
                  order = {'EWL':2})

GC_560 = Coupling(name = 'GC_560',
                  value = '(cphi**2*complex(0,1)*gw**2*k1**2)/(2.*cw**2*vev**2) + (cphi**2*complex(0,1)*gw**2*k2**2)/(2.*cw**2*vev**2) - (cphi**2*complex(0,1)*gw**2*k1**2*sw**2)/(cw**2*vev**2) - (cphi**2*complex(0,1)*gw**2*k2**2*sw**2)/(cw**2*vev**2)',
                  order = {'EWL':2})

GC_561 = Coupling(name = 'GC_561',
                  value = '(2*cphi**2*complex(0,1)*gw**2*sth1**2)/cw**2 - (4*cphi**2*complex(0,1)*gw**2*sth1**2*sw**2)/cw**2 + (cphi**2*cth1**2*complex(0,1)*gw**2*k1**2)/(2.*cw**2*vev**2) + (cphi**2*cth1**2*complex(0,1)*gw**2*k2**2)/(2.*cw**2*vev**2) - (cphi**2*cth1**2*complex(0,1)*gw**2*k1**2*sw**2)/(cw**2*vev**2) - (cphi**2*cth1**2*complex(0,1)*gw**2*k2**2*sw**2)/(cw**2*vev**2)',
                  order = {'EWL':2})

GC_562 = Coupling(name = 'GC_562',
                  value = '-0.5*(cphi*cxi*gw**2*k1**2*sw**2)/(cw*vev**2) - (cphi*cxi*gw**2*k2**2*sw**2)/(2.*cw*vev**2)',
                  order = {'EWL':2})

GC_563 = Coupling(name = 'GC_563',
                  value = '-0.5*(cphi*cxi*complex(0,1)*gw**2*k1**2*sw**2)/(cw*vev**2) - (cphi*cxi*complex(0,1)*gw**2*k2**2*sw**2)/(2.*cw*vev**2)',
                  order = {'EWL':2})

GC_564 = Coupling(name = 'GC_564',
                  value = '(cphi*cxi*gw**2*k1**2*sw**2)/(2.*cw*vev**2) + (cphi*cxi*gw**2*k2**2*sw**2)/(2.*cw*vev**2)',
                  order = {'EWL':2})

GC_565 = Coupling(name = 'GC_565',
                  value = '-0.5*(cw*gw*k1**2*sphi)/vev**2 - (cw*gw*k2**2*sphi)/(2.*vev**2) - (gw*k1**2*sphi*sw**2)/(2.*cw*vev**2) - (gw*k2**2*sphi*sw**2)/(2.*cw*vev**2)',
                  order = {'EWR':1})

GC_566 = Coupling(name = 'GC_566',
                  value = '-0.5*(cw*complex(0,1)*gw*k1**2*sphi)/vev**2 - (cw*complex(0,1)*gw*k2**2*sphi)/(2.*vev**2) + (complex(0,1)*gw*k1**2*sphi*sw**2)/(2.*cw*vev**2) + (complex(0,1)*gw*k2**2*sphi*sw**2)/(2.*cw*vev**2)',
                  order = {'EWR':1})

GC_567 = Coupling(name = 'GC_567',
                  value = '-0.5*(cxi*gw**2*k1**2*sphi*sw**2)/(cw*vev**2) - (cxi*gw**2*k2**2*sphi*sw**2)/(2.*cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_568 = Coupling(name = 'GC_568',
                  value = '-0.5*(cxi*complex(0,1)*gw**2*k1**2*sphi*sw**2)/(cw*vev**2) - (cxi*complex(0,1)*gw**2*k2**2*sphi*sw**2)/(2.*cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_569 = Coupling(name = 'GC_569',
                  value = '(cxi*gw**2*k1**2*sphi*sw**2)/(2.*cw*vev**2) + (cxi*gw**2*k2**2*sphi*sw**2)/(2.*cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_570 = Coupling(name = 'GC_570',
                  value = '(complex(0,1)*gw**2*k1**2*sphi**2)/(2.*cw**2*vev**2) + (complex(0,1)*gw**2*k2**2*sphi**2)/(2.*cw**2*vev**2) - (complex(0,1)*gw**2*k1**2*sphi**2*sw**2)/(cw**2*vev**2) - (complex(0,1)*gw**2*k2**2*sphi**2*sw**2)/(cw**2*vev**2)',
                  order = {'EWR':2})

GC_571 = Coupling(name = 'GC_571',
                  value = '(2*complex(0,1)*gw**2*sphi**2*sth1**2)/cw**2 - (4*complex(0,1)*gw**2*sphi**2*sth1**2*sw**2)/cw**2 + (cth1**2*complex(0,1)*gw**2*k1**2*sphi**2)/(2.*cw**2*vev**2) + (cth1**2*complex(0,1)*gw**2*k2**2*sphi**2)/(2.*cw**2*vev**2) - (cth1**2*complex(0,1)*gw**2*k1**2*sphi**2*sw**2)/(cw**2*vev**2) - (cth1**2*complex(0,1)*gw**2*k2**2*sphi**2*sw**2)/(cw**2*vev**2)',
                  order = {'EWR':2})

GC_572 = Coupling(name = 'GC_572',
                  value = '(-2*cphi**2*cth1*complex(0,1)*gw**2*sth1)/cw**2 + (4*cphi**2*cth1*complex(0,1)*gw**2*sth1*sw**2)/cw**2 + (cphi**2*cth1*complex(0,1)*gw**2*k1**2*sth1)/(2.*cw**2*vev**2) + (cphi**2*cth1*complex(0,1)*gw**2*k2**2*sth1)/(2.*cw**2*vev**2) - (cphi**2*cth1*complex(0,1)*gw**2*k1**2*sth1*sw**2)/(cw**2*vev**2) - (cphi**2*cth1*complex(0,1)*gw**2*k2**2*sth1*sw**2)/(cw**2*vev**2)',
                  order = {'EWL':2})

GC_573 = Coupling(name = 'GC_573',
                  value = '(-2*cth1*complex(0,1)*gw**2*sphi**2*sth1)/cw**2 + (4*cth1*complex(0,1)*gw**2*sphi**2*sth1*sw**2)/cw**2 + (cth1*complex(0,1)*gw**2*k1**2*sphi**2*sth1)/(2.*cw**2*vev**2) + (cth1*complex(0,1)*gw**2*k2**2*sphi**2*sth1)/(2.*cw**2*vev**2) - (cth1*complex(0,1)*gw**2*k1**2*sphi**2*sth1*sw**2)/(cw**2*vev**2) - (cth1*complex(0,1)*gw**2*k2**2*sphi**2*sth1*sw**2)/(cw**2*vev**2)',
                  order = {'EWR':2})

GC_574 = Coupling(name = 'GC_574',
                  value = '(2*cphi**2*cth1**2*complex(0,1)*gw**2)/cw**2 - (4*cphi**2*cth1**2*complex(0,1)*gw**2*sw**2)/cw**2 + (cphi**2*complex(0,1)*gw**2*k1**2*sth1**2)/(2.*cw**2*vev**2) + (cphi**2*complex(0,1)*gw**2*k2**2*sth1**2)/(2.*cw**2*vev**2) - (cphi**2*complex(0,1)*gw**2*k1**2*sth1**2*sw**2)/(cw**2*vev**2) - (cphi**2*complex(0,1)*gw**2*k2**2*sth1**2*sw**2)/(cw**2*vev**2)',
                  order = {'EWL':2})

GC_575 = Coupling(name = 'GC_575',
                  value = '(2*cth1**2*complex(0,1)*gw**2*sphi**2)/cw**2 - (4*cth1**2*complex(0,1)*gw**2*sphi**2*sw**2)/cw**2 + (complex(0,1)*gw**2*k1**2*sphi**2*sth1**2)/(2.*cw**2*vev**2) + (complex(0,1)*gw**2*k2**2*sphi**2*sth1**2)/(2.*cw**2*vev**2) - (complex(0,1)*gw**2*k1**2*sphi**2*sth1**2*sw**2)/(cw**2*vev**2) - (complex(0,1)*gw**2*k2**2*sphi**2*sth1**2*sw**2)/(cw**2*vev**2)',
                  order = {'EWR':2})

GC_576 = Coupling(name = 'GC_576',
                  value = '(cphi*cw*complex(0,1)*gw**2*k1**2*sw)/vev**2 + (cphi*cw*complex(0,1)*gw**2*k2**2*sw)/vev**2 - (cphi*complex(0,1)*gw**2*k1**2*sw**3)/(cw*vev**2) - (cphi*complex(0,1)*gw**2*k2**2*sw**3)/(cw*vev**2)',
                  order = {'EWL':2})

GC_577 = Coupling(name = 'GC_577',
                  value = '(cw*complex(0,1)*gw**2*k1**2*sphi*sw)/vev**2 + (cw*complex(0,1)*gw**2*k2**2*sphi*sw)/vev**2 - (complex(0,1)*gw**2*k1**2*sphi*sw**3)/(cw*vev**2) - (complex(0,1)*gw**2*k2**2*sphi*sw**3)/(cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_578 = Coupling(name = 'GC_578',
                  value = '(cphi**2*cw**2*complex(0,1)*gw**2*k1**2)/(2.*vev**2) + (cphi**2*cw**2*complex(0,1)*gw**2*k2**2)/(2.*vev**2) - (cphi**2*complex(0,1)*gw**2*k1**2*sw**2)/vev**2 - (cphi**2*complex(0,1)*gw**2*k2**2*sw**2)/vev**2 + (cphi**2*complex(0,1)*gw**2*k1**2*sw**4)/(2.*cw**2*vev**2) + (cphi**2*complex(0,1)*gw**2*k2**2*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWL':2})

GC_579 = Coupling(name = 'GC_579',
                  value = '(cphi**2*cw**2*complex(0,1)*gw**2*k1**2)/(2.*vev**2) + (cphi**2*cw**2*complex(0,1)*gw**2*k2**2)/(2.*vev**2) + (cphi**2*complex(0,1)*gw**2*k1**2*sw**2)/vev**2 + (cphi**2*complex(0,1)*gw**2*k2**2*sw**2)/vev**2 + (cphi**2*complex(0,1)*gw**2*k1**2*sw**4)/(2.*cw**2*vev**2) + (cphi**2*complex(0,1)*gw**2*k2**2*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWL':2})

GC_580 = Coupling(name = 'GC_580',
                  value = '(2*cphi**2*complex(0,1)*gw**2*sth1**2*sw**4)/cw**2 + (cphi**2*cth1**2*cw**2*complex(0,1)*gw**2*k1**2)/(2.*vev**2) + (cphi**2*cth1**2*cw**2*complex(0,1)*gw**2*k2**2)/(2.*vev**2) + (cphi**2*cth1**2*complex(0,1)*gw**2*k1**2*sw**2)/vev**2 + (cphi**2*cth1**2*complex(0,1)*gw**2*k2**2*sw**2)/vev**2 + (cphi**2*cth1**2*complex(0,1)*gw**2*k1**2*sw**4)/(2.*cw**2*vev**2) + (cphi**2*cth1**2*complex(0,1)*gw**2*k2**2*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWL':2})

GC_581 = Coupling(name = 'GC_581',
                  value = '-0.5*(cphi*complex(0,1)*gw**2*k1**2*sphi)/(cw**2*vev**2) + (cphi*cw**2*complex(0,1)*gw**2*k1**2*sphi)/(2.*vev**2) - (cphi*complex(0,1)*gw**2*k2**2*sphi)/(2.*cw**2*vev**2) + (cphi*cw**2*complex(0,1)*gw**2*k2**2*sphi)/(2.*vev**2) - (cphi*complex(0,1)*gw**2*k1**2*sphi*sw**2)/vev**2 + (cphi*complex(0,1)*gw**2*k1**2*sphi*sw**2)/(cw**2*vev**2) - (cphi*complex(0,1)*gw**2*k2**2*sphi*sw**2)/vev**2 + (cphi*complex(0,1)*gw**2*k2**2*sphi*sw**2)/(cw**2*vev**2) + (cphi*complex(0,1)*gw**2*k1**2*sphi*sw**4)/(2.*cw**2*vev**2) + (cphi*complex(0,1)*gw**2*k2**2*sphi*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_582 = Coupling(name = 'GC_582',
                  value = '-0.5*(cphi*complex(0,1)*gw**2*k1**2*sphi)/(cw**2*vev**2) + (cphi*cw**2*complex(0,1)*gw**2*k1**2*sphi)/(2.*vev**2) - (cphi*complex(0,1)*gw**2*k2**2*sphi)/(2.*cw**2*vev**2) + (cphi*cw**2*complex(0,1)*gw**2*k2**2*sphi)/(2.*vev**2) + (cphi*complex(0,1)*gw**2*k1**2*sphi*sw**2)/vev**2 + (cphi*complex(0,1)*gw**2*k1**2*sphi*sw**2)/(cw**2*vev**2) + (cphi*complex(0,1)*gw**2*k2**2*sphi*sw**2)/vev**2 + (cphi*complex(0,1)*gw**2*k2**2*sphi*sw**2)/(cw**2*vev**2) + (cphi*complex(0,1)*gw**2*k1**2*sphi*sw**4)/(2.*cw**2*vev**2) + (cphi*complex(0,1)*gw**2*k2**2*sphi*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_583 = Coupling(name = 'GC_583',
                  value = '(-2*cphi*complex(0,1)*gw**2*sphi*sth1**2)/cw**2 + (4*cphi*complex(0,1)*gw**2*sphi*sth1**2*sw**2)/cw**2 + (2*cphi*complex(0,1)*gw**2*sphi*sth1**2*sw**4)/cw**2 - (cphi*cth1**2*complex(0,1)*gw**2*k1**2*sphi)/(2.*cw**2*vev**2) + (cphi*cth1**2*cw**2*complex(0,1)*gw**2*k1**2*sphi)/(2.*vev**2) - (cphi*cth1**2*complex(0,1)*gw**2*k2**2*sphi)/(2.*cw**2*vev**2) + (cphi*cth1**2*cw**2*complex(0,1)*gw**2*k2**2*sphi)/(2.*vev**2) + (cphi*cth1**2*complex(0,1)*gw**2*k1**2*sphi*sw**2)/vev**2 + (cphi*cth1**2*complex(0,1)*gw**2*k1**2*sphi*sw**2)/(cw**2*vev**2) + (cphi*cth1**2*complex(0,1)*gw**2*k2**2*sphi*sw**2)/vev**2 + (cphi*cth1**2*complex(0,1)*gw**2*k2**2*sphi*sw**2)/(cw**2*vev**2) + (cphi*cth1**2*complex(0,1)*gw**2*k1**2*sphi*sw**4)/(2.*cw**2*vev**2) + (cphi*cth1**2*complex(0,1)*gw**2*k2**2*sphi*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_584 = Coupling(name = 'GC_584',
                  value = '(cw**2*complex(0,1)*gw**2*k1**2*sphi**2)/(2.*vev**2) + (cw**2*complex(0,1)*gw**2*k2**2*sphi**2)/(2.*vev**2) - (complex(0,1)*gw**2*k1**2*sphi**2*sw**2)/vev**2 - (complex(0,1)*gw**2*k2**2*sphi**2*sw**2)/vev**2 + (complex(0,1)*gw**2*k1**2*sphi**2*sw**4)/(2.*cw**2*vev**2) + (complex(0,1)*gw**2*k2**2*sphi**2*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWR':2})

GC_585 = Coupling(name = 'GC_585',
                  value = '(cw**2*complex(0,1)*gw**2*k1**2*sphi**2)/(2.*vev**2) + (cw**2*complex(0,1)*gw**2*k2**2*sphi**2)/(2.*vev**2) + (complex(0,1)*gw**2*k1**2*sphi**2*sw**2)/vev**2 + (complex(0,1)*gw**2*k2**2*sphi**2*sw**2)/vev**2 + (complex(0,1)*gw**2*k1**2*sphi**2*sw**4)/(2.*cw**2*vev**2) + (complex(0,1)*gw**2*k2**2*sphi**2*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWR':2})

GC_586 = Coupling(name = 'GC_586',
                  value = '(2*complex(0,1)*gw**2*sphi**2*sth1**2*sw**4)/cw**2 + (cth1**2*cw**2*complex(0,1)*gw**2*k1**2*sphi**2)/(2.*vev**2) + (cth1**2*cw**2*complex(0,1)*gw**2*k2**2*sphi**2)/(2.*vev**2) + (cth1**2*complex(0,1)*gw**2*k1**2*sphi**2*sw**2)/vev**2 + (cth1**2*complex(0,1)*gw**2*k2**2*sphi**2*sw**2)/vev**2 + (cth1**2*complex(0,1)*gw**2*k1**2*sphi**2*sw**4)/(2.*cw**2*vev**2) + (cth1**2*complex(0,1)*gw**2*k2**2*sphi**2*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWR':2})

GC_587 = Coupling(name = 'GC_587',
                  value = '(-2*cphi**2*cth1*complex(0,1)*gw**2*sth1*sw**4)/cw**2 + (cphi**2*cth1*cw**2*complex(0,1)*gw**2*k1**2*sth1)/(2.*vev**2) + (cphi**2*cth1*cw**2*complex(0,1)*gw**2*k2**2*sth1)/(2.*vev**2) + (cphi**2*cth1*complex(0,1)*gw**2*k1**2*sth1*sw**2)/vev**2 + (cphi**2*cth1*complex(0,1)*gw**2*k2**2*sth1*sw**2)/vev**2 + (cphi**2*cth1*complex(0,1)*gw**2*k1**2*sth1*sw**4)/(2.*cw**2*vev**2) + (cphi**2*cth1*complex(0,1)*gw**2*k2**2*sth1*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWL':2})

GC_588 = Coupling(name = 'GC_588',
                  value = '(2*cphi*cth1*complex(0,1)*gw**2*sphi*sth1)/cw**2 - (4*cphi*cth1*complex(0,1)*gw**2*sphi*sth1*sw**2)/cw**2 - (2*cphi*cth1*complex(0,1)*gw**2*sphi*sth1*sw**4)/cw**2 - (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sth1)/(2.*cw**2*vev**2) + (cphi*cth1*cw**2*complex(0,1)*gw**2*k1**2*sphi*sth1)/(2.*vev**2) - (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sth1)/(2.*cw**2*vev**2) + (cphi*cth1*cw**2*complex(0,1)*gw**2*k2**2*sphi*sth1)/(2.*vev**2) + (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sth1*sw**2)/vev**2 + (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sth1*sw**2)/(cw**2*vev**2) + (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sth1*sw**2)/vev**2 + (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sth1*sw**2)/(cw**2*vev**2) + (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sth1*sw**4)/(2.*cw**2*vev**2) + (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sth1*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_589 = Coupling(name = 'GC_589',
                  value = '(-2*cth1*complex(0,1)*gw**2*sphi**2*sth1*sw**4)/cw**2 + (cth1*cw**2*complex(0,1)*gw**2*k1**2*sphi**2*sth1)/(2.*vev**2) + (cth1*cw**2*complex(0,1)*gw**2*k2**2*sphi**2*sth1)/(2.*vev**2) + (cth1*complex(0,1)*gw**2*k1**2*sphi**2*sth1*sw**2)/vev**2 + (cth1*complex(0,1)*gw**2*k2**2*sphi**2*sth1*sw**2)/vev**2 + (cth1*complex(0,1)*gw**2*k1**2*sphi**2*sth1*sw**4)/(2.*cw**2*vev**2) + (cth1*complex(0,1)*gw**2*k2**2*sphi**2*sth1*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWR':2})

GC_590 = Coupling(name = 'GC_590',
                  value = '(2*cphi**2*cth1**2*complex(0,1)*gw**2*sw**4)/cw**2 + (cphi**2*cw**2*complex(0,1)*gw**2*k1**2*sth1**2)/(2.*vev**2) + (cphi**2*cw**2*complex(0,1)*gw**2*k2**2*sth1**2)/(2.*vev**2) + (cphi**2*complex(0,1)*gw**2*k1**2*sth1**2*sw**2)/vev**2 + (cphi**2*complex(0,1)*gw**2*k2**2*sth1**2*sw**2)/vev**2 + (cphi**2*complex(0,1)*gw**2*k1**2*sth1**2*sw**4)/(2.*cw**2*vev**2) + (cphi**2*complex(0,1)*gw**2*k2**2*sth1**2*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWL':2})

GC_591 = Coupling(name = 'GC_591',
                  value = '(-2*cphi*cth1**2*complex(0,1)*gw**2*sphi)/cw**2 + (4*cphi*cth1**2*complex(0,1)*gw**2*sphi*sw**2)/cw**2 + (2*cphi*cth1**2*complex(0,1)*gw**2*sphi*sw**4)/cw**2 - (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1**2)/(2.*cw**2*vev**2) + (cphi*cw**2*complex(0,1)*gw**2*k1**2*sphi*sth1**2)/(2.*vev**2) - (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1**2)/(2.*cw**2*vev**2) + (cphi*cw**2*complex(0,1)*gw**2*k2**2*sphi*sth1**2)/(2.*vev**2) + (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1**2*sw**2)/vev**2 + (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1**2*sw**2)/(cw**2*vev**2) + (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1**2*sw**2)/vev**2 + (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1**2*sw**2)/(cw**2*vev**2) + (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1**2*sw**4)/(2.*cw**2*vev**2) + (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1**2*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_592 = Coupling(name = 'GC_592',
                  value = '(2*cth1**2*complex(0,1)*gw**2*sphi**2*sw**4)/cw**2 + (cw**2*complex(0,1)*gw**2*k1**2*sphi**2*sth1**2)/(2.*vev**2) + (cw**2*complex(0,1)*gw**2*k2**2*sphi**2*sth1**2)/(2.*vev**2) + (complex(0,1)*gw**2*k1**2*sphi**2*sth1**2*sw**2)/vev**2 + (complex(0,1)*gw**2*k2**2*sphi**2*sth1**2*sw**2)/vev**2 + (complex(0,1)*gw**2*k1**2*sphi**2*sth1**2*sw**4)/(2.*cw**2*vev**2) + (complex(0,1)*gw**2*k2**2*sphi**2*sth1**2*sw**4)/(2.*cw**2*vev**2)',
                  order = {'EWR':2})

GC_593 = Coupling(name = 'GC_593',
                  value = '-0.5*(cphi*complex(0,1)*gw*k1**2*cmath.sqrt(1 - 2*sw**2))/(cw*vev**2) - (cphi*complex(0,1)*gw*k2**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWL':1})

GC_594 = Coupling(name = 'GC_594',
                  value = '(cphi*gw*k1**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2) + (cphi*gw*k2**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWL':1})

GC_595 = Coupling(name = 'GC_595',
                  value = '-0.5*(cphi*cxi*gw**2*k1**2*cmath.sqrt(1 - 2*sw**2))/(cw*vev**2) - (cphi*cxi*gw**2*k2**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWL':2})

GC_596 = Coupling(name = 'GC_596',
                  value = '(cphi*cxi*complex(0,1)*gw**2*k1**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2) + (cphi*cxi*complex(0,1)*gw**2*k2**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWL':2})

GC_597 = Coupling(name = 'GC_597',
                  value = '(cphi*cxi*gw**2*k1**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2) + (cphi*cxi*gw**2*k2**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWL':2})

GC_598 = Coupling(name = 'GC_598',
                  value = '-0.5*(gw*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/(cw*vev**2) - (gw*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWR':1})

GC_599 = Coupling(name = 'GC_599',
                  value = '(complex(0,1)*gw*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2) + (complex(0,1)*gw*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWR':1})

GC_600 = Coupling(name = 'GC_600',
                  value = '(cphi*complex(0,1)*gw**2*k1**2*sw*cmath.sqrt(1 - 2*sw**2))/(cw*vev**2) + (cphi*complex(0,1)*gw**2*k2**2*sw*cmath.sqrt(1 - 2*sw**2))/(cw*vev**2)',
                  order = {'EWL':2})

GC_601 = Coupling(name = 'GC_601',
                  value = '-((complex(0,1)*gw**2*k1**2*sphi*sw*cmath.sqrt(1 - 2*sw**2))/(cw*vev**2)) - (complex(0,1)*gw**2*k2**2*sphi*sw*cmath.sqrt(1 - 2*sw**2))/(cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_602 = Coupling(name = 'GC_602',
                  value = '-0.5*(cphi**2*complex(0,1)*gw**2*k1**2*cmath.sqrt(1 - 2*sw**2))/vev**2 - (cphi**2*complex(0,1)*gw**2*k2**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) - (cphi**2*complex(0,1)*gw**2*k1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2) - (cphi**2*complex(0,1)*gw**2*k2**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2)',
                  order = {'EWL':2})

GC_603 = Coupling(name = 'GC_603',
                  value = '(cphi**2*complex(0,1)*gw**2*k1**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) + (cphi**2*complex(0,1)*gw**2*k2**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) - (cphi**2*complex(0,1)*gw**2*k1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2) - (cphi**2*complex(0,1)*gw**2*k2**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2)',
                  order = {'EWL':2})

GC_604 = Coupling(name = 'GC_604',
                  value = '(-2*cphi**2*complex(0,1)*gw**2*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2 - (cphi**2*cth1**2*complex(0,1)*gw**2*k1**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) - (cphi**2*cth1**2*complex(0,1)*gw**2*k2**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) - (cphi**2*cth1**2*complex(0,1)*gw**2*k1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2) - (cphi**2*cth1**2*complex(0,1)*gw**2*k2**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2)',
                  order = {'EWL':2})

GC_605 = Coupling(name = 'GC_605',
                  value = '-((cphi*complex(0,1)*gw**2*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev**2) - (cphi*complex(0,1)*gw**2*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev**2 - (cphi*complex(0,1)*gw**2*k1**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2) - (cphi*complex(0,1)*gw**2*k2**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_606 = Coupling(name = 'GC_606',
                  value = '(cphi*complex(0,1)*gw**2*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev**2 + (cphi*complex(0,1)*gw**2*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev**2 - (cphi*complex(0,1)*gw**2*k1**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2) - (cphi*complex(0,1)*gw**2*k2**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_607 = Coupling(name = 'GC_607',
                  value = '-((cphi*complex(0,1)*gw**2*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev**2) - (cphi*complex(0,1)*gw**2*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev**2 + (cphi*complex(0,1)*gw**2*k1**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2) + (cphi*complex(0,1)*gw**2*k2**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_608 = Coupling(name = 'GC_608',
                  value = '(cphi*complex(0,1)*gw**2*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev**2 + (cphi*complex(0,1)*gw**2*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev**2 + (cphi*complex(0,1)*gw**2*k1**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2) + (cphi*complex(0,1)*gw**2*k2**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_609 = Coupling(name = 'GC_609',
                  value = '(-4*cphi*complex(0,1)*gw**2*sphi*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2 - (cphi*cth1**2*complex(0,1)*gw**2*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev**2 - (cphi*cth1**2*complex(0,1)*gw**2*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev**2 - (cphi*cth1**2*complex(0,1)*gw**2*k1**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2) - (cphi*cth1**2*complex(0,1)*gw**2*k2**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_610 = Coupling(name = 'GC_610',
                  value = '(4*cphi*complex(0,1)*gw**2*sphi*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2 + (cphi*cth1**2*complex(0,1)*gw**2*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev**2 + (cphi*cth1**2*complex(0,1)*gw**2*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev**2 + (cphi*cth1**2*complex(0,1)*gw**2*k1**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2) + (cphi*cth1**2*complex(0,1)*gw**2*k2**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_611 = Coupling(name = 'GC_611',
                  value = '-0.5*(complex(0,1)*gw**2*k1**2*sphi**2*cmath.sqrt(1 - 2*sw**2))/vev**2 - (complex(0,1)*gw**2*k2**2*sphi**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) + (complex(0,1)*gw**2*k1**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2) + (complex(0,1)*gw**2*k2**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2)',
                  order = {'EWR':2})

GC_612 = Coupling(name = 'GC_612',
                  value = '(complex(0,1)*gw**2*k1**2*sphi**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) + (complex(0,1)*gw**2*k2**2*sphi**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) + (complex(0,1)*gw**2*k1**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2) + (complex(0,1)*gw**2*k2**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2)',
                  order = {'EWR':2})

GC_613 = Coupling(name = 'GC_613',
                  value = '(2*complex(0,1)*gw**2*sphi**2*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2 + (cth1**2*complex(0,1)*gw**2*k1**2*sphi**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) + (cth1**2*complex(0,1)*gw**2*k2**2*sphi**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) + (cth1**2*complex(0,1)*gw**2*k1**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2) + (cth1**2*complex(0,1)*gw**2*k2**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2)',
                  order = {'EWR':2})

GC_614 = Coupling(name = 'GC_614',
                  value = '(2*cphi**2*cth1*complex(0,1)*gw**2*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2 - (cphi**2*cth1*complex(0,1)*gw**2*k1**2*sth1*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) - (cphi**2*cth1*complex(0,1)*gw**2*k2**2*sth1*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) - (cphi**2*cth1*complex(0,1)*gw**2*k1**2*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2) - (cphi**2*cth1*complex(0,1)*gw**2*k2**2*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2)',
                  order = {'EWL':2})

GC_615 = Coupling(name = 'GC_615',
                  value = '(4*cphi*cth1*complex(0,1)*gw**2*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2 - (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sth1*cmath.sqrt(1 - 2*sw**2))/vev**2 - (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sth1*cmath.sqrt(1 - 2*sw**2))/vev**2 - (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2) - (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_616 = Coupling(name = 'GC_616',
                  value = '(-4*cphi*cth1*complex(0,1)*gw**2*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2 + (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sth1*cmath.sqrt(1 - 2*sw**2))/vev**2 + (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sth1*cmath.sqrt(1 - 2*sw**2))/vev**2 + (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2) + (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_617 = Coupling(name = 'GC_617',
                  value = '(-2*cth1*complex(0,1)*gw**2*sphi**2*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2 + (cth1*complex(0,1)*gw**2*k1**2*sphi**2*sth1*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) + (cth1*complex(0,1)*gw**2*k2**2*sphi**2*sth1*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) + (cth1*complex(0,1)*gw**2*k1**2*sphi**2*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2) + (cth1*complex(0,1)*gw**2*k2**2*sphi**2*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2)',
                  order = {'EWR':2})

GC_618 = Coupling(name = 'GC_618',
                  value = '(-2*cphi**2*cth1**2*complex(0,1)*gw**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2 - (cphi**2*complex(0,1)*gw**2*k1**2*sth1**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) - (cphi**2*complex(0,1)*gw**2*k2**2*sth1**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) - (cphi**2*complex(0,1)*gw**2*k1**2*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2) - (cphi**2*complex(0,1)*gw**2*k2**2*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2)',
                  order = {'EWL':2})

GC_619 = Coupling(name = 'GC_619',
                  value = '(-4*cphi*cth1**2*complex(0,1)*gw**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2 - (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1**2*cmath.sqrt(1 - 2*sw**2))/vev**2 - (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1**2*cmath.sqrt(1 - 2*sw**2))/vev**2 - (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2) - (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_620 = Coupling(name = 'GC_620',
                  value = '(4*cphi*cth1**2*complex(0,1)*gw**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2 + (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1**2*cmath.sqrt(1 - 2*sw**2))/vev**2 + (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1**2*cmath.sqrt(1 - 2*sw**2))/vev**2 + (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2) + (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_621 = Coupling(name = 'GC_621',
                  value = '(2*cth1**2*complex(0,1)*gw**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/cw**2 + (complex(0,1)*gw**2*k1**2*sphi**2*sth1**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) + (complex(0,1)*gw**2*k2**2*sphi**2*sth1**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev**2) + (complex(0,1)*gw**2*k1**2*sphi**2*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2) + (complex(0,1)*gw**2*k2**2*sphi**2*sth1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev**2)',
                  order = {'EWR':2})

GC_622 = Coupling(name = 'GC_622',
                  value = '-((cphi*cw*gw**2*k1*k2*sxi)/vev**2) + (cxi*gw**2*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2) + (cxi*gw**2*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_623 = Coupling(name = 'GC_623',
                  value = '-((cphi*cw*complex(0,1)*gw**2*k1*k2*sxi)/vev**2) - (cxi*complex(0,1)*gw**2*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2) - (cxi*complex(0,1)*gw**2*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_624 = Coupling(name = 'GC_624',
                  value = '(cphi*cw*gw**2*k1*k2*sxi)/vev**2 - (cxi*gw**2*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2) - (cxi*gw**2*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_625 = Coupling(name = 'GC_625',
                  value = '-0.5*(complex(0,1)*gw*k1**2*sxi)/vev**2 - (complex(0,1)*gw*k2**2*sxi)/(2.*vev**2)',
                  order = {'EWR':1})

GC_626 = Coupling(name = 'GC_626',
                  value = '(complex(0,1)*gw*k1**2*sxi)/(2.*vev**2) + (complex(0,1)*gw*k2**2*sxi)/(2.*vev**2)',
                  order = {'EWR':1})

GC_627 = Coupling(name = 'GC_627',
                  value = '(gw*k1**2*sxi)/(2.*vev**2) + (gw*k2**2*sxi)/(2.*vev**2)',
                  order = {'EWR':1})

GC_628 = Coupling(name = 'GC_628',
                  value = '(cth1*complex(0,1)*gw*k1**2*sxi)/(2.*vev**2) - (cth1*complex(0,1)*gw*k2**2*sxi)/(2.*vev**2)',
                  order = {'EWR':1})

GC_629 = Coupling(name = 'GC_629',
                  value = '-0.5*(cth1*complex(0,1)*gw*k1**2*sxi)/vev**2 + (cth1*complex(0,1)*gw*k2**2*sxi)/(2.*vev**2)',
                  order = {'EWR':1})

GC_630 = Coupling(name = 'GC_630',
                  value = '(cphi*cth1*cw*complex(0,1)*gw**2*k1**2*sxi)/(2.*vev**2) - (cphi*cth1*cw*complex(0,1)*gw**2*k2**2*sxi)/(2.*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_631 = Coupling(name = 'GC_631',
                  value = '(cth1*cxi*complex(0,1)*gw**2*k1**2*sxi)/vev**2 - (cth1*cxi*complex(0,1)*gw**2*k2**2*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_632 = Coupling(name = 'GC_632',
                  value = '-((cth1*cxi*complex(0,1)*gw**2*k1**2*sxi)/vev**2) + (cth1*cxi*complex(0,1)*gw**2*k2**2*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_633 = Coupling(name = 'GC_633',
                  value = '(cth1*cw*complex(0,1)*gw**2*k1**2*sphi*sxi)/(2.*vev**2) - (cth1*cw*complex(0,1)*gw**2*k2**2*sphi*sxi)/(2.*vev**2)',
                  order = {'EWR':2})

GC_634 = Coupling(name = 'GC_634',
                  value = '(complex(0,1)*gw*k1**2*sth1*sxi)/(2.*vev**2) - (complex(0,1)*gw*k2**2*sth1*sxi)/(2.*vev**2)',
                  order = {'EWR':1})

GC_635 = Coupling(name = 'GC_635',
                  value = '-0.5*(complex(0,1)*gw*k1**2*sth1*sxi)/vev**2 + (complex(0,1)*gw*k2**2*sth1*sxi)/(2.*vev**2)',
                  order = {'EWR':1})

GC_636 = Coupling(name = 'GC_636',
                  value = '(cphi*cw*complex(0,1)*gw**2*k1**2*sth1*sxi)/(2.*vev**2) - (cphi*cw*complex(0,1)*gw**2*k2**2*sth1*sxi)/(2.*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_637 = Coupling(name = 'GC_637',
                  value = '(cxi*complex(0,1)*gw**2*k1**2*sth1*sxi)/vev**2 - (cxi*complex(0,1)*gw**2*k2**2*sth1*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_638 = Coupling(name = 'GC_638',
                  value = '-((cxi*complex(0,1)*gw**2*k1**2*sth1*sxi)/vev**2) + (cxi*complex(0,1)*gw**2*k2**2*sth1*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_639 = Coupling(name = 'GC_639',
                  value = '(cw*complex(0,1)*gw**2*k1**2*sphi*sth1*sxi)/(2.*vev**2) - (cw*complex(0,1)*gw**2*k2**2*sphi*sth1*sxi)/(2.*vev**2)',
                  order = {'EWR':2})

GC_640 = Coupling(name = 'GC_640',
                  value = '-0.5*(gw**2*k1**2*sw*sxi)/vev**2 - (gw**2*k2**2*sw*sxi)/(2.*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_641 = Coupling(name = 'GC_641',
                  value = '(complex(0,1)*gw**2*k1**2*sw*sxi)/(2.*vev**2) + (complex(0,1)*gw**2*k2**2*sw*sxi)/(2.*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_642 = Coupling(name = 'GC_642',
                  value = '(gw**2*k1**2*sw*sxi)/(2.*vev**2) + (gw**2*k2**2*sw*sxi)/(2.*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_643 = Coupling(name = 'GC_643',
                  value = '(cth1*complex(0,1)*gw**2*k1**2*sw*sxi)/(2.*vev**2) - (cth1*complex(0,1)*gw**2*k2**2*sw*sxi)/(2.*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_644 = Coupling(name = 'GC_644',
                  value = '(complex(0,1)*gw**2*k1**2*sth1*sw*sxi)/(2.*vev**2) - (complex(0,1)*gw**2*k2**2*sth1*sw*sxi)/(2.*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_645 = Coupling(name = 'GC_645',
                  value = '-0.5*(cphi*gw**2*k1**2*sw**2*sxi)/(cw*vev**2) - (cphi*gw**2*k2**2*sw**2*sxi)/(2.*cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_646 = Coupling(name = 'GC_646',
                  value = '-0.5*(cphi*complex(0,1)*gw**2*k1**2*sw**2*sxi)/(cw*vev**2) - (cphi*complex(0,1)*gw**2*k2**2*sw**2*sxi)/(2.*cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_647 = Coupling(name = 'GC_647',
                  value = '(cphi*gw**2*k1**2*sw**2*sxi)/(2.*cw*vev**2) + (cphi*gw**2*k2**2*sw**2*sxi)/(2.*cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_648 = Coupling(name = 'GC_648',
                  value = '-0.5*(gw**2*k1**2*sphi*sw**2*sxi)/(cw*vev**2) - (gw**2*k2**2*sphi*sw**2*sxi)/(2.*cw*vev**2)',
                  order = {'EWR':2})

GC_649 = Coupling(name = 'GC_649',
                  value = '-0.5*(complex(0,1)*gw**2*k1**2*sphi*sw**2*sxi)/(cw*vev**2) - (complex(0,1)*gw**2*k2**2*sphi*sw**2*sxi)/(2.*cw*vev**2)',
                  order = {'EWR':2})

GC_650 = Coupling(name = 'GC_650',
                  value = '(gw**2*k1**2*sphi*sw**2*sxi)/(2.*cw*vev**2) + (gw**2*k2**2*sphi*sw**2*sxi)/(2.*cw*vev**2)',
                  order = {'EWR':2})

GC_651 = Coupling(name = 'GC_651',
                  value = '(cw*cxi*gw**2*k1*k2*sphi)/vev**2 - (cphi*gw**2*k1**2*sxi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2) - (cphi*gw**2*k2**2*sxi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_652 = Coupling(name = 'GC_652',
                  value = '(cw*cxi*complex(0,1)*gw**2*k1*k2*sphi)/vev**2 + (cphi*complex(0,1)*gw**2*k1**2*sxi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2) + (cphi*complex(0,1)*gw**2*k2**2*sxi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_653 = Coupling(name = 'GC_653',
                  value = '-((cw*cxi*gw**2*k1*k2*sphi)/vev**2) + (cphi*gw**2*k1**2*sxi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2) + (cphi*gw**2*k2**2*sxi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_654 = Coupling(name = 'GC_654',
                  value = '-0.5*(gw**2*k1**2*sphi*sxi*cmath.sqrt(1 - 2*sw**2))/(cw*vev**2) - (gw**2*k2**2*sphi*sxi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWR':2})

GC_655 = Coupling(name = 'GC_655',
                  value = '-0.5*(complex(0,1)*gw**2*k1**2*sphi*sxi*cmath.sqrt(1 - 2*sw**2))/(cw*vev**2) - (complex(0,1)*gw**2*k2**2*sphi*sxi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWR':2})

GC_656 = Coupling(name = 'GC_656',
                  value = '(gw**2*k1**2*sphi*sxi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2) + (gw**2*k2**2*sphi*sxi*cmath.sqrt(1 - 2*sw**2))/(2.*cw*vev**2)',
                  order = {'EWR':2})

GC_657 = Coupling(name = 'GC_657',
                  value = '(complex(0,1)*gw**2*k1**2*sxi**2)/(2.*vev**2) + (complex(0,1)*gw**2*k2**2*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_658 = Coupling(name = 'GC_658',
                  value = '(cth1*gw**2*k1**2*sxi**2)/(2.*vev**2) - (cth1*gw**2*k2**2*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_659 = Coupling(name = 'GC_659',
                  value = '(cth1*complex(0,1)*gw**2*k1**2*sxi**2)/(2.*vev**2) - (cth1*complex(0,1)*gw**2*k2**2*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_660 = Coupling(name = 'GC_660',
                  value = '-0.5*(cth1*gw**2*k1**2*sxi**2)/vev**2 + (cth1*gw**2*k2**2*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_661 = Coupling(name = 'GC_661',
                  value = '(cth1**2*complex(0,1)*gw**2*k1**2*sxi**2)/(2.*vev**2) + (cth1**2*complex(0,1)*gw**2*k2**2*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_662 = Coupling(name = 'GC_662',
                  value = 'complex(0,1)*gw**2*sth1**2*sxi**2 + (cth1**2*complex(0,1)*gw**2*k1**2*sxi**2)/(2.*vev**2) + (cth1**2*complex(0,1)*gw**2*k2**2*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_663 = Coupling(name = 'GC_663',
                  value = '(gw**2*k1**2*sth1*sxi**2)/(2.*vev**2) - (gw**2*k2**2*sth1*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_664 = Coupling(name = 'GC_664',
                  value = '(complex(0,1)*gw**2*k1**2*sth1*sxi**2)/(2.*vev**2) - (complex(0,1)*gw**2*k2**2*sth1*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_665 = Coupling(name = 'GC_665',
                  value = '-0.5*(gw**2*k1**2*sth1*sxi**2)/vev**2 + (gw**2*k2**2*sth1*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_666 = Coupling(name = 'GC_666',
                  value = '(cth1*complex(0,1)*gw**2*k1**2*sth1*sxi**2)/(2.*vev**2) + (cth1*complex(0,1)*gw**2*k2**2*sth1*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_667 = Coupling(name = 'GC_667',
                  value = '-(cth1*complex(0,1)*gw**2*sth1*sxi**2) + (cth1*complex(0,1)*gw**2*k1**2*sth1*sxi**2)/(2.*vev**2) + (cth1*complex(0,1)*gw**2*k2**2*sth1*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_668 = Coupling(name = 'GC_668',
                  value = '(complex(0,1)*gw**2*k1**2*sth1**2*sxi**2)/(2.*vev**2) + (complex(0,1)*gw**2*k2**2*sth1**2*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_669 = Coupling(name = 'GC_669',
                  value = 'cth1**2*complex(0,1)*gw**2*sxi**2 + (complex(0,1)*gw**2*k1**2*sth1**2*sxi**2)/(2.*vev**2) + (complex(0,1)*gw**2*k2**2*sth1**2*sxi**2)/(2.*vev**2)',
                  order = {'EWR':2})

GC_670 = Coupling(name = 'GC_670',
                  value = '-0.5*(cphi*cw*cxi*complex(0,1)*gw**2*k1**2)/vev + (cphi*cw*cxi*complex(0,1)*gw**2*k2**2)/(2.*vev)',
                  order = {'EWL':1})

GC_671 = Coupling(name = 'GC_671',
                  value = '(cxi**2*gw**2*k1**2)/(2.*vev) - (cxi**2*gw**2*k2**2)/(2.*vev)',
                  order = {'EWL':1})

GC_672 = Coupling(name = 'GC_672',
                  value = '-0.5*(cxi**2*complex(0,1)*gw**2*k1**2)/vev + (cxi**2*complex(0,1)*gw**2*k2**2)/(2.*vev)',
                  order = {'EWL':1})

GC_673 = Coupling(name = 'GC_673',
                  value = '-0.5*(cxi**2*gw**2*k1**2)/vev + (cxi**2*gw**2*k2**2)/(2.*vev)',
                  order = {'EWL':1})

GC_674 = Coupling(name = 'GC_674',
                  value = '(cth1*cxi**2*complex(0,1)*gw**2*k1**2)/(2.*vev) + (cth1*cxi**2*complex(0,1)*gw**2*k2**2)/(2.*vev)',
                  order = {'EWL':1})

GC_675 = Coupling(name = 'GC_675',
                  value = '-0.5*(cw*cxi*complex(0,1)*gw**2*k1**2*sphi)/vev + (cw*cxi*complex(0,1)*gw**2*k2**2*sphi)/(2.*vev)',
                  order = {'EWR':1})

GC_676 = Coupling(name = 'GC_676',
                  value = '(cxi**2*complex(0,1)*gw**2*k1**2*sth1)/(2.*vev) + (cxi**2*complex(0,1)*gw**2*k2**2*sth1)/(2.*vev)',
                  order = {'EWL':1})

GC_677 = Coupling(name = 'GC_677',
                  value = '-0.5*(cxi*complex(0,1)*gw**2*k1**2*sw)/vev + (cxi*complex(0,1)*gw**2*k2**2*sw)/(2.*vev)',
                  order = {'EWL':1})

GC_678 = Coupling(name = 'GC_678',
                  value = '(cth1*complex(0,1)*gw**2*k1**2*sphi**2)/(2.*cw**2*vev) + (cth1*complex(0,1)*gw**2*k2**2*sphi**2)/(2.*cw**2*vev) - (cth1*complex(0,1)*gw**2*k1**2*sphi**2*sw**2)/(cw**2*vev) - (cth1*complex(0,1)*gw**2*k2**2*sphi**2*sw**2)/(cw**2*vev)',
                  order = {'EWL':-1,'EWR':2})

GC_679 = Coupling(name = 'GC_679',
                  value = '(complex(0,1)*gw**2*k1**2*sphi**2*sth1)/(2.*cw**2*vev) + (complex(0,1)*gw**2*k2**2*sphi**2*sth1)/(2.*cw**2*vev) - (complex(0,1)*gw**2*k1**2*sphi**2*sth1*sw**2)/(cw**2*vev) - (complex(0,1)*gw**2*k2**2*sphi**2*sth1*sw**2)/(cw**2*vev)',
                  order = {'EWL':-1,'EWR':2})

GC_680 = Coupling(name = 'GC_680',
                  value = '(cth1*cw**2*complex(0,1)*gw**2*k1**2*sphi**2)/(2.*vev) + (cth1*cw**2*complex(0,1)*gw**2*k2**2*sphi**2)/(2.*vev) + (cth1*complex(0,1)*gw**2*k1**2*sphi**2*sw**2)/vev + (cth1*complex(0,1)*gw**2*k2**2*sphi**2*sw**2)/vev + (cth1*complex(0,1)*gw**2*k1**2*sphi**2*sw**4)/(2.*cw**2*vev) + (cth1*complex(0,1)*gw**2*k2**2*sphi**2*sw**4)/(2.*cw**2*vev)',
                  order = {'EWL':-1,'EWR':2})

GC_681 = Coupling(name = 'GC_681',
                  value = '(cw**2*complex(0,1)*gw**2*k1**2*sphi**2*sth1)/(2.*vev) + (cw**2*complex(0,1)*gw**2*k2**2*sphi**2*sth1)/(2.*vev) + (complex(0,1)*gw**2*k1**2*sphi**2*sth1*sw**2)/vev + (complex(0,1)*gw**2*k2**2*sphi**2*sth1*sw**2)/vev + (complex(0,1)*gw**2*k1**2*sphi**2*sth1*sw**4)/(2.*cw**2*vev) + (complex(0,1)*gw**2*k2**2*sphi**2*sth1*sw**4)/(2.*cw**2*vev)',
                  order = {'EWL':-1,'EWR':2})

GC_682 = Coupling(name = 'GC_682',
                  value = '(cth1*complex(0,1)*gw**2*k1**2*sphi**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev) + (cth1*complex(0,1)*gw**2*k2**2*sphi**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev) + (cth1*complex(0,1)*gw**2*k1**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev) + (cth1*complex(0,1)*gw**2*k2**2*sphi**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev)',
                  order = {'EWL':-1,'EWR':2})

GC_683 = Coupling(name = 'GC_683',
                  value = '(complex(0,1)*gw**2*k1**2*sphi**2*sth1*cmath.sqrt(1 - 2*sw**2))/(2.*vev) + (complex(0,1)*gw**2*k2**2*sphi**2*sth1*cmath.sqrt(1 - 2*sw**2))/(2.*vev) + (complex(0,1)*gw**2*k1**2*sphi**2*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev) + (complex(0,1)*gw**2*k2**2*sphi**2*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev)',
                  order = {'EWL':-1,'EWR':2})

GC_684 = Coupling(name = 'GC_684',
                  value = '(cphi*cw*complex(0,1)*gw**2*k1**2*sxi)/(2.*vev) - (cphi*cw*complex(0,1)*gw**2*k2**2*sxi)/(2.*vev)',
                  order = {'EWR':1})

GC_685 = Coupling(name = 'GC_685',
                  value = '(cxi*complex(0,1)*gw**2*k1**2*sxi)/vev - (cxi*complex(0,1)*gw**2*k2**2*sxi)/vev',
                  order = {'EWR':1})

GC_686 = Coupling(name = 'GC_686',
                  value = '-((cxi*complex(0,1)*gw**2*k1**2*sxi)/vev) + (cxi*complex(0,1)*gw**2*k2**2*sxi)/vev',
                  order = {'EWR':1})

GC_687 = Coupling(name = 'GC_687',
                  value = '(cw*complex(0,1)*gw**2*k1**2*sphi*sxi)/(2.*vev) - (cw*complex(0,1)*gw**2*k2**2*sphi*sxi)/(2.*vev)',
                  order = {'EWL':-1,'EWR':2})

GC_688 = Coupling(name = 'GC_688',
                  value = '(complex(0,1)*gw**2*k1**2*sw*sxi)/(2.*vev) - (complex(0,1)*gw**2*k2**2*sw*sxi)/(2.*vev)',
                  order = {'EWR':1})

GC_689 = Coupling(name = 'GC_689',
                  value = '(gw**2*k1**2*sxi**2)/(2.*vev) - (gw**2*k2**2*sxi**2)/(2.*vev)',
                  order = {'EWL':-1,'EWR':2})

GC_690 = Coupling(name = 'GC_690',
                  value = '(complex(0,1)*gw**2*k1**2*sxi**2)/(2.*vev) - (complex(0,1)*gw**2*k2**2*sxi**2)/(2.*vev)',
                  order = {'EWL':-1,'EWR':2})

GC_691 = Coupling(name = 'GC_691',
                  value = '-0.5*(gw**2*k1**2*sxi**2)/vev + (gw**2*k2**2*sxi**2)/(2.*vev)',
                  order = {'EWL':-1,'EWR':2})

GC_692 = Coupling(name = 'GC_692',
                  value = '(cth1*complex(0,1)*gw**2*k1**2*sxi**2)/(2.*vev) + (cth1*complex(0,1)*gw**2*k2**2*sxi**2)/(2.*vev)',
                  order = {'EWL':-1,'EWR':2})

GC_693 = Coupling(name = 'GC_693',
                  value = '(complex(0,1)*gw**2*k1**2*sth1*sxi**2)/(2.*vev) + (complex(0,1)*gw**2*k2**2*sth1*sxi**2)/(2.*vev)',
                  order = {'EWL':-1,'EWR':2})

GC_694 = Coupling(name = 'GC_694',
                  value = '-((cxi*gw*k1*k2)/vev**2)',
                  order = {'EWL':1})

GC_695 = Coupling(name = 'GC_695',
                  value = '-((cxi*complex(0,1)*gw*k1*k2)/vev**2)',
                  order = {'EWL':1})

GC_696 = Coupling(name = 'GC_696',
                  value = '(cxi*complex(0,1)*gw*k1*k2)/vev**2',
                  order = {'EWL':1})

GC_697 = Coupling(name = 'GC_697',
                  value = '-((cphi*cw*cxi*gw**2*k1*k2)/vev**2)',
                  order = {'EWL':2})

GC_698 = Coupling(name = 'GC_698',
                  value = '(cphi*cw*cxi*complex(0,1)*gw**2*k1*k2)/vev**2',
                  order = {'EWL':2})

GC_699 = Coupling(name = 'GC_699',
                  value = '(cphi*cw*cxi*gw**2*k1*k2)/vev**2',
                  order = {'EWL':2})

GC_700 = Coupling(name = 'GC_700',
                  value = '-((cxi**2*gw**2*k1*k2)/vev**2)',
                  order = {'EWL':2})

GC_701 = Coupling(name = 'GC_701',
                  value = '-((cxi**2*complex(0,1)*gw**2*k1*k2)/vev**2)',
                  order = {'EWL':2})

GC_702 = Coupling(name = 'GC_702',
                  value = '(cxi**2*complex(0,1)*gw**2*k1*k2)/vev**2',
                  order = {'EWL':2})

GC_703 = Coupling(name = 'GC_703',
                  value = '(-2*cxi**2*complex(0,1)*gw**2*k1*k2)/vev**2',
                  order = {'EWL':2})

GC_704 = Coupling(name = 'GC_704',
                  value = '(cxi**2*gw**2*k1*k2)/vev**2',
                  order = {'EWL':2})

GC_705 = Coupling(name = 'GC_705',
                  value = '-((cth1**2*cxi**2*complex(0,1)*gw**2*k1*k2)/vev**2)',
                  order = {'EWL':2})

GC_706 = Coupling(name = 'GC_706',
                  value = '-((cth1*cxi**2*complex(0,1)*gw**2*k1*k2*sth1)/vev**2)',
                  order = {'EWL':2})

GC_707 = Coupling(name = 'GC_707',
                  value = '-((cxi**2*complex(0,1)*gw**2*k1*k2*sth1**2)/vev**2)',
                  order = {'EWL':2})

GC_708 = Coupling(name = 'GC_708',
                  value = '-((cxi*gw**2*k1*k2*sw)/vev**2)',
                  order = {'EWL':2})

GC_709 = Coupling(name = 'GC_709',
                  value = '(cxi*complex(0,1)*gw**2*k1*k2*sw)/vev**2',
                  order = {'EWL':2})

GC_710 = Coupling(name = 'GC_710',
                  value = '(cxi*gw**2*k1*k2*sw)/vev**2',
                  order = {'EWL':2})

GC_711 = Coupling(name = 'GC_711',
                  value = '-((complex(0,1)*gw*k1*k2*sxi)/vev**2)',
                  order = {'EWR':1})

GC_712 = Coupling(name = 'GC_712',
                  value = '(complex(0,1)*gw*k1*k2*sxi)/vev**2',
                  order = {'EWR':1})

GC_713 = Coupling(name = 'GC_713',
                  value = '(gw*k1*k2*sxi)/vev**2',
                  order = {'EWR':1})

GC_714 = Coupling(name = 'GC_714',
                  value = '(-2*cxi*complex(0,1)*gw**2*k1*k2*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_715 = Coupling(name = 'GC_715',
                  value = '(2*cxi*complex(0,1)*gw**2*k1*k2*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_716 = Coupling(name = 'GC_716',
                  value = '(-4*cxi*complex(0,1)*gw**2*k1*k2*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_717 = Coupling(name = 'GC_717',
                  value = '(4*cxi*complex(0,1)*gw**2*k1*k2*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_718 = Coupling(name = 'GC_718',
                  value = '(-2*cth1**2*cxi*complex(0,1)*gw**2*k1*k2*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_719 = Coupling(name = 'GC_719',
                  value = '(2*cth1**2*cxi*complex(0,1)*gw**2*k1*k2*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_720 = Coupling(name = 'GC_720',
                  value = '-((cw*gw**2*k1*k2*sphi*sxi)/vev**2)',
                  order = {'EWR':2})

GC_721 = Coupling(name = 'GC_721',
                  value = '-((cw*complex(0,1)*gw**2*k1*k2*sphi*sxi)/vev**2)',
                  order = {'EWR':2})

GC_722 = Coupling(name = 'GC_722',
                  value = '(cw*gw**2*k1*k2*sphi*sxi)/vev**2',
                  order = {'EWR':2})

GC_723 = Coupling(name = 'GC_723',
                  value = '(-2*cth1*cxi*complex(0,1)*gw**2*k1*k2*sth1*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_724 = Coupling(name = 'GC_724',
                  value = '(2*cth1*cxi*complex(0,1)*gw**2*k1*k2*sth1*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_725 = Coupling(name = 'GC_725',
                  value = '(-2*cxi*complex(0,1)*gw**2*k1*k2*sth1**2*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_726 = Coupling(name = 'GC_726',
                  value = '(2*cxi*complex(0,1)*gw**2*k1*k2*sth1**2*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_727 = Coupling(name = 'GC_727',
                  value = '-((gw**2*k1*k2*sw*sxi)/vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_728 = Coupling(name = 'GC_728',
                  value = '-((complex(0,1)*gw**2*k1*k2*sw*sxi)/vev**2)',
                  order = {'EWL':1,'EWR':1})

GC_729 = Coupling(name = 'GC_729',
                  value = '(gw**2*k1*k2*sw*sxi)/vev**2',
                  order = {'EWL':1,'EWR':1})

GC_730 = Coupling(name = 'GC_730',
                  value = '-((gw**2*k1*k2*sxi**2)/vev**2)',
                  order = {'EWR':2})

GC_731 = Coupling(name = 'GC_731',
                  value = '-((complex(0,1)*gw**2*k1*k2*sxi**2)/vev**2)',
                  order = {'EWR':2})

GC_732 = Coupling(name = 'GC_732',
                  value = '(complex(0,1)*gw**2*k1*k2*sxi**2)/vev**2',
                  order = {'EWR':2})

GC_733 = Coupling(name = 'GC_733',
                  value = '(2*complex(0,1)*gw**2*k1*k2*sxi**2)/vev**2',
                  order = {'EWR':2})

GC_734 = Coupling(name = 'GC_734',
                  value = '(gw**2*k1*k2*sxi**2)/vev**2',
                  order = {'EWR':2})

GC_735 = Coupling(name = 'GC_735',
                  value = '(cth1**2*complex(0,1)*gw**2*k1*k2*sxi**2)/vev**2',
                  order = {'EWR':2})

GC_736 = Coupling(name = 'GC_736',
                  value = '(cth1*complex(0,1)*gw**2*k1*k2*sth1*sxi**2)/vev**2',
                  order = {'EWR':2})

GC_737 = Coupling(name = 'GC_737',
                  value = '(complex(0,1)*gw**2*k1*k2*sth1**2*sxi**2)/vev**2',
                  order = {'EWR':2})

GC_738 = Coupling(name = 'GC_738',
                  value = '(-2*cth1*cxi*complex(0,1)*gw**2*k1*k2*sxi)/vev',
                  order = {'EWR':1})

GC_739 = Coupling(name = 'GC_739',
                  value = '(-2*cxi*complex(0,1)*gw**2*k1*k2*sth1*sxi)/vev',
                  order = {'EWR':1})

GC_740 = Coupling(name = 'GC_740',
                  value = '(cth1*complex(0,1)*gw**2*k1*k2*sxi**2)/vev',
                  order = {'EWL':-1,'EWR':2})

GC_741 = Coupling(name = 'GC_741',
                  value = '(complex(0,1)*gw**2*k1*k2*sth1*sxi**2)/vev',
                  order = {'EWL':-1,'EWR':2})

GC_742 = Coupling(name = 'GC_742',
                  value = '2*cphi**2*cw**2*complex(0,1)*gw**2*vL',
                  order = {'EWL':1})

GC_743 = Coupling(name = 'GC_743',
                  value = '-((cphi*cw*cxi*complex(0,1)*gw**2*vL)/cmath.sqrt(2))',
                  order = {'EWL':1})

GC_744 = Coupling(name = 'GC_744',
                  value = 'cxi**2*complex(0,1)*gw**2*vL',
                  order = {'EWL':1})

GC_745 = Coupling(name = 'GC_745',
                  value = '-(cxi**2*complex(0,1)*gw**2*vL*cmath.sqrt(2))',
                  order = {'EWL':1})

GC_746 = Coupling(name = 'GC_746',
                  value = '2*cphi*cw**2*complex(0,1)*gw**2*sphi*vL',
                  order = {'EWR':1})

GC_747 = Coupling(name = 'GC_747',
                  value = '-((cw*cxi*complex(0,1)*gw**2*sphi*vL)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_748 = Coupling(name = 'GC_748',
                  value = '2*cw**2*complex(0,1)*gw**2*sphi**2*vL',
                  order = {'EWL':-1,'EWR':2})

GC_749 = Coupling(name = 'GC_749',
                  value = '2*cphi**2*complex(0,1)*g1*gw*sw*vL',
                  order = {'QED':1})

GC_750 = Coupling(name = 'GC_750',
                  value = '-((cphi*cxi*complex(0,1)*g1*gw*sw*vL*cmath.sqrt(2))/cw)',
                  order = {'QED':1})

GC_751 = Coupling(name = 'GC_751',
                  value = '2*cphi*cw*complex(0,1)*gw**2*sw*vL',
                  order = {'EWL':1})

GC_752 = Coupling(name = 'GC_752',
                  value = '-((cxi*complex(0,1)*gw**2*sw*vL)/cmath.sqrt(2))',
                  order = {'EWL':1})

GC_753 = Coupling(name = 'GC_753',
                  value = '-4*cphi*complex(0,1)*g1*gw*sphi*sw*vL',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_754 = Coupling(name = 'GC_754',
                  value = '4*cphi*complex(0,1)*g1*gw*sphi*sw*vL',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_755 = Coupling(name = 'GC_755',
                  value = '(cxi*complex(0,1)*g1*gw*sphi*sw*vL*cmath.sqrt(2))/cw',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_756 = Coupling(name = 'GC_756',
                  value = '2*cw*complex(0,1)*gw**2*sphi*sw*vL',
                  order = {'EWR':1})

GC_757 = Coupling(name = 'GC_757',
                  value = '-2*complex(0,1)*g1*gw*sphi**2*sw*vL',
                  order = {'EWL':-2,'EWR':2,'QED':1})

GC_758 = Coupling(name = 'GC_758',
                  value = '(2*cphi**2*complex(0,1)*g1**2*sw**2*vL)/cw**2',
                  order = {'EWL':-1,'QED':2})

GC_759 = Coupling(name = 'GC_759',
                  value = '(2*cphi*complex(0,1)*g1*gw*sw**2*vL)/cw',
                  order = {'QED':1})

GC_760 = Coupling(name = 'GC_760',
                  value = '2*complex(0,1)*gw**2*sw**2*vL',
                  order = {'EWL':1})

GC_761 = Coupling(name = 'GC_761',
                  value = '(-2*complex(0,1)*g1*gw*sphi*sw**2*vL)/cw',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_762 = Coupling(name = 'GC_762',
                  value = '(2*complex(0,1)*g1**2*sphi**2*sw**2*vL)/cw**2',
                  order = {'EWL':-3,'EWR':2,'QED':2})

GC_763 = Coupling(name = 'GC_763',
                  value = '(-4*cphi*complex(0,1)*g1**2*sphi*sw**4*vL)/cw**2',
                  order = {'EWL':-2,'EWR':1,'QED':2})

GC_764 = Coupling(name = 'GC_764',
                  value = 'cxi*complex(0,1)*g1*gw*vL*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2)',
                  order = {'QED':1})

GC_765 = Coupling(name = 'GC_765',
                  value = '(-2*cphi*complex(0,1)*g1**2*sw*vL*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'QED':2})

GC_766 = Coupling(name = 'GC_766',
                  value = '-4*complex(0,1)*g1*gw*sw*vL*cmath.sqrt(1 - 2*sw**2)',
                  order = {'QED':1})

GC_767 = Coupling(name = 'GC_767',
                  value = '4*cphi**2*complex(0,1)*g1*gw*sw*vL*cmath.sqrt(1 - 2*sw**2)',
                  order = {'QED':1})

GC_768 = Coupling(name = 'GC_768',
                  value = '-((cphi*cxi*complex(0,1)*g1*gw*sw*vL*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'QED':1})

GC_769 = Coupling(name = 'GC_769',
                  value = '(2*complex(0,1)*g1**2*sphi*sw*vL*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-2,'EWR':1,'QED':2})

GC_770 = Coupling(name = 'GC_770',
                  value = '4*cphi*complex(0,1)*g1*gw*sphi*sw*vL*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_771 = Coupling(name = 'GC_771',
                  value = '-((cxi*complex(0,1)*g1*gw*sphi*sw*vL*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_772 = Coupling(name = 'GC_772',
                  value = '4*complex(0,1)*g1*gw*sphi**2*sw*vL*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':-2,'EWR':2,'QED':1})

GC_773 = Coupling(name = 'GC_773',
                  value = '(2*cphi**2*complex(0,1)*g1**2*sw**2*vL*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'QED':2})

GC_774 = Coupling(name = 'GC_774',
                  value = '(-4*cphi*complex(0,1)*g1**2*sphi*sw**2*vL*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-2,'EWR':1,'QED':2})

GC_775 = Coupling(name = 'GC_775',
                  value = '(4*cphi*complex(0,1)*g1**2*sphi*sw**2*vL*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-2,'EWR':1,'QED':2})

GC_776 = Coupling(name = 'GC_776',
                  value = '(-2*complex(0,1)*g1**2*sphi**2*sw**2*vL*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-3,'EWR':2,'QED':2})

GC_777 = Coupling(name = 'GC_777',
                  value = '-((cphi*cw*complex(0,1)*gw**2*sxi*vL)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_778 = Coupling(name = 'GC_778',
                  value = 'cxi*complex(0,1)*gw**2*sxi*vL',
                  order = {'EWR':1})

GC_779 = Coupling(name = 'GC_779',
                  value = '-(cxi*complex(0,1)*gw**2*sxi*vL*cmath.sqrt(2))',
                  order = {'EWR':1})

GC_780 = Coupling(name = 'GC_780',
                  value = '-((cw*complex(0,1)*gw**2*sphi*sxi*vL)/cmath.sqrt(2))',
                  order = {'EWL':-1,'EWR':2})

GC_781 = Coupling(name = 'GC_781',
                  value = '-((cphi*complex(0,1)*g1*gw*sw*sxi*vL*cmath.sqrt(2))/cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_782 = Coupling(name = 'GC_782',
                  value = '-((complex(0,1)*gw**2*sw*sxi*vL)/cmath.sqrt(2))',
                  order = {'EWR':1})

GC_783 = Coupling(name = 'GC_783',
                  value = '(complex(0,1)*g1*gw*sphi*sw*sxi*vL*cmath.sqrt(2))/cw',
                  order = {'EWL':-2,'EWR':2,'QED':1})

GC_784 = Coupling(name = 'GC_784',
                  value = 'complex(0,1)*g1*gw*sxi*vL*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_785 = Coupling(name = 'GC_785',
                  value = '-((cphi*complex(0,1)*g1*gw*sw*sxi*vL*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_786 = Coupling(name = 'GC_786',
                  value = '-((complex(0,1)*g1*gw*sphi*sw*sxi*vL*cmath.sqrt(2)*cmath.sqrt(1 - 2*sw**2))/cw)',
                  order = {'EWL':-2,'EWR':2,'QED':1})

GC_787 = Coupling(name = 'GC_787',
                  value = 'complex(0,1)*gw**2*sxi**2*vL',
                  order = {'EWL':-1,'EWR':2})

GC_788 = Coupling(name = 'GC_788',
                  value = '-(complex(0,1)*gw**2*sxi**2*vL*cmath.sqrt(2))',
                  order = {'EWL':-1,'EWR':2})

GC_789 = Coupling(name = 'GC_789',
                  value = '2*complex(0,1)*g1**2*vL - 4*complex(0,1)*g1**2*sw**2*vL',
                  order = {'EWL':-1,'QED':2})

GC_790 = Coupling(name = 'GC_790',
                  value = '(-2*cphi*complex(0,1)*g1**2*sw*vL)/cw + (4*cphi*complex(0,1)*g1**2*sw**3*vL)/cw',
                  order = {'EWL':-1,'QED':2})

GC_791 = Coupling(name = 'GC_791',
                  value = '(-2*complex(0,1)*g1**2*sphi*sw*vL)/cw + (4*complex(0,1)*g1**2*sphi*sw**3*vL)/cw',
                  order = {'EWL':-2,'EWR':1,'QED':2})

GC_792 = Coupling(name = 'GC_792',
                  value = '(2*cphi**2*complex(0,1)*g1**2*sw**2*vL)/cw**2 - (4*cphi**2*complex(0,1)*g1**2*sw**4*vL)/cw**2',
                  order = {'EWL':-1,'QED':2})

GC_793 = Coupling(name = 'GC_793',
                  value = '(2*complex(0,1)*g1**2*sphi**2*sw**2*vL)/cw**2 - (4*complex(0,1)*g1**2*sphi**2*sw**4*vL)/cw**2',
                  order = {'EWL':-3,'EWR':2,'QED':2})

GC_794 = Coupling(name = 'GC_794',
                  value = '-2*cphi*cw*complex(0,1)*g1*gw*vL*cmath.sqrt(1 - 2*sw**2) + (2*cphi*complex(0,1)*g1*gw*sw**2*vL*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'QED':1})

GC_795 = Coupling(name = 'GC_795',
                  value = '-2*cw*complex(0,1)*g1*gw*sphi*vL*cmath.sqrt(1 - 2*sw**2) + (2*complex(0,1)*g1*gw*sphi*sw**2*vL*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_796 = Coupling(name = 'GC_796',
                  value = '-(cxi**2*complex(0,1)*gw**2*vR*cmath.sqrt(2))',
                  order = {'EWL':2,'EWR':-1})

GC_797 = Coupling(name = 'GC_797',
                  value = 'cth1*cxi**2*complex(0,1)*gw**2*vR',
                  order = {'EWL':2,'EWR':-1})

GC_798 = Coupling(name = 'GC_798',
                  value = '-(cxi**2*complex(0,1)*gw**2*sth1*vR)',
                  order = {'EWL':2,'EWR':-1})

GC_799 = Coupling(name = 'GC_799',
                  value = '(2*cphi**2*cth1*complex(0,1)*g1**2*sw**2*vR)/cw**2',
                  order = {'EWR':-1,'QED':2})

GC_800 = Coupling(name = 'GC_800',
                  value = '2*cth1*complex(0,1)*gw**2*sw**2*vR',
                  order = {'EWL':2,'EWR':-1})

GC_801 = Coupling(name = 'GC_801',
                  value = '(2*cth1*complex(0,1)*g1**2*sphi**2*sw**2*vR)/cw**2',
                  order = {'EWL':-2,'EWR':1,'QED':2})

GC_802 = Coupling(name = 'GC_802',
                  value = '(-2*cphi**2*complex(0,1)*g1**2*sth1*sw**2*vR)/cw**2',
                  order = {'EWR':-1,'QED':2})

GC_803 = Coupling(name = 'GC_803',
                  value = '-2*complex(0,1)*gw**2*sth1*sw**2*vR',
                  order = {'EWL':2,'EWR':-1})

GC_804 = Coupling(name = 'GC_804',
                  value = '(-2*complex(0,1)*g1**2*sphi**2*sth1*sw**2*vR)/cw**2',
                  order = {'EWL':-2,'EWR':1,'QED':2})

GC_805 = Coupling(name = 'GC_805',
                  value = '(-2*cphi*cth1*complex(0,1)*gw**2*sw**3*vR)/cw',
                  order = {'EWL':2,'EWR':-1})

GC_806 = Coupling(name = 'GC_806',
                  value = '(-2*cth1*complex(0,1)*gw**2*sphi*sw**3*vR)/cw',
                  order = {'EWL':1})

GC_807 = Coupling(name = 'GC_807',
                  value = '(2*cphi*complex(0,1)*gw**2*sth1*sw**3*vR)/cw',
                  order = {'EWL':2,'EWR':-1})

GC_808 = Coupling(name = 'GC_808',
                  value = '(2*complex(0,1)*gw**2*sphi*sth1*sw**3*vR)/cw',
                  order = {'EWL':1})

GC_809 = Coupling(name = 'GC_809',
                  value = '(2*cphi**2*cth1*complex(0,1)*gw**2*sw**4*vR)/cw**2',
                  order = {'EWL':2,'EWR':-1})

GC_810 = Coupling(name = 'GC_810',
                  value = '(-4*cphi*cth1*complex(0,1)*g1**2*sphi*sw**4*vR)/cw**2',
                  order = {'EWL':-1,'QED':2})

GC_811 = Coupling(name = 'GC_811',
                  value = '(-2*cphi**2*complex(0,1)*gw**2*sth1*sw**4*vR)/cw**2',
                  order = {'EWL':2,'EWR':-1})

GC_812 = Coupling(name = 'GC_812',
                  value = '(4*cphi*complex(0,1)*g1**2*sphi*sth1*sw**4*vR)/cw**2',
                  order = {'EWL':-1,'QED':2})

GC_813 = Coupling(name = 'GC_813',
                  value = '(-2*cphi*cth1*complex(0,1)*g1**2*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':-1,'QED':2})

GC_814 = Coupling(name = 'GC_814',
                  value = '-4*cth1*complex(0,1)*g1*gw*sw*vR*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':1,'EWR':-1,'QED':1})

GC_815 = Coupling(name = 'GC_815',
                  value = '(4*cphi**2*cth1*complex(0,1)*g1*gw*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'EWR':-1,'QED':1})

GC_816 = Coupling(name = 'GC_816',
                  value = '(2*cphi*cth1*complex(0,1)*gw**2*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':2,'EWR':-1})

GC_817 = Coupling(name = 'GC_817',
                  value = '(2*cth1*complex(0,1)*g1**2*sphi*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'QED':2})

GC_818 = Coupling(name = 'GC_818',
                  value = '(-2*cth1*complex(0,1)*gw**2*sphi*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1})

GC_819 = Coupling(name = 'GC_819',
                  value = '(4*cth1*complex(0,1)*g1*gw*sphi**2*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_820 = Coupling(name = 'GC_820',
                  value = '(2*cphi*complex(0,1)*g1**2*sth1*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWR':-1,'QED':2})

GC_821 = Coupling(name = 'GC_821',
                  value = '4*complex(0,1)*g1*gw*sth1*sw*vR*cmath.sqrt(1 - 2*sw**2)',
                  order = {'EWL':1,'EWR':-1,'QED':1})

GC_822 = Coupling(name = 'GC_822',
                  value = '(-4*cphi**2*complex(0,1)*g1*gw*sth1*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'EWR':-1,'QED':1})

GC_823 = Coupling(name = 'GC_823',
                  value = '(-2*cphi*complex(0,1)*gw**2*sth1*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':2,'EWR':-1})

GC_824 = Coupling(name = 'GC_824',
                  value = '(-2*complex(0,1)*g1**2*sphi*sth1*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':-1,'QED':2})

GC_825 = Coupling(name = 'GC_825',
                  value = '(2*complex(0,1)*gw**2*sphi*sth1*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1})

GC_826 = Coupling(name = 'GC_826',
                  value = '(-4*complex(0,1)*g1*gw*sphi**2*sth1*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_827 = Coupling(name = 'GC_827',
                  value = '(2*cphi**2*cth1*complex(0,1)*g1**2*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWR':-1,'QED':2})

GC_828 = Coupling(name = 'GC_828',
                  value = '(4*cphi*cth1*complex(0,1)*g1*gw*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'EWR':-1,'QED':1})

GC_829 = Coupling(name = 'GC_829',
                  value = '(-2*cphi**2*cth1*complex(0,1)*gw**2*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':2,'EWR':-1})

GC_830 = Coupling(name = 'GC_830',
                  value = '(-4*cphi*cth1*complex(0,1)*g1**2*sphi*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'QED':2})

GC_831 = Coupling(name = 'GC_831',
                  value = '(4*cphi*cth1*complex(0,1)*g1**2*sphi*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'QED':2})

GC_832 = Coupling(name = 'GC_832',
                  value = '(4*cth1*complex(0,1)*g1*gw*sphi*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'QED':1})

GC_833 = Coupling(name = 'GC_833',
                  value = '(-2*cth1*complex(0,1)*g1**2*sphi**2*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-2,'EWR':1,'QED':2})

GC_834 = Coupling(name = 'GC_834',
                  value = '(-2*cphi**2*complex(0,1)*g1**2*sth1*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWR':-1,'QED':2})

GC_835 = Coupling(name = 'GC_835',
                  value = '(-4*cphi*complex(0,1)*g1*gw*sth1*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'EWL':1,'EWR':-1,'QED':1})

GC_836 = Coupling(name = 'GC_836',
                  value = '(2*cphi**2*complex(0,1)*gw**2*sth1*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':2,'EWR':-1})

GC_837 = Coupling(name = 'GC_837',
                  value = '(-4*cphi*complex(0,1)*g1**2*sphi*sth1*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'QED':2})

GC_838 = Coupling(name = 'GC_838',
                  value = '(4*cphi*complex(0,1)*g1**2*sphi*sth1*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'QED':2})

GC_839 = Coupling(name = 'GC_839',
                  value = '(-4*complex(0,1)*g1*gw*sphi*sth1*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw',
                  order = {'QED':1})

GC_840 = Coupling(name = 'GC_840',
                  value = '(2*complex(0,1)*g1**2*sphi**2*sth1*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-2,'EWR':1,'QED':2})

GC_841 = Coupling(name = 'GC_841',
                  value = '(-4*cphi**2*cth1*complex(0,1)*g1*gw*sw**3*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'EWR':-1,'QED':1})

GC_842 = Coupling(name = 'GC_842',
                  value = '(-4*cth1*complex(0,1)*g1*gw*sphi**2*sw**3*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_843 = Coupling(name = 'GC_843',
                  value = '(4*cphi**2*complex(0,1)*g1*gw*sth1*sw**3*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1,'EWR':-1,'QED':1})

GC_844 = Coupling(name = 'GC_844',
                  value = '(4*complex(0,1)*g1*gw*sphi**2*sth1*sw**3*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_845 = Coupling(name = 'GC_845',
                  value = 'cxi*complex(0,1)*gw**2*sxi*vR*cmath.sqrt(2)',
                  order = {'EWL':1})

GC_846 = Coupling(name = 'GC_846',
                  value = '-(complex(0,1)*gw**2*sxi**2*vR*cmath.sqrt(2))',
                  order = {'EWR':1})

GC_847 = Coupling(name = 'GC_847',
                  value = '2*cth1*complex(0,1)*g1**2*vR - 4*cth1*complex(0,1)*g1**2*sw**2*vR',
                  order = {'EWR':-1,'QED':2})

GC_848 = Coupling(name = 'GC_848',
                  value = '(-2*cphi*cth1*complex(0,1)*g1*gw*vR)/cw + (6*cphi*cth1*complex(0,1)*g1*gw*sw**2*vR)/cw',
                  order = {'EWL':1,'EWR':-1,'QED':1})

GC_849 = Coupling(name = 'GC_849',
                  value = '(2*cphi**2*cth1*complex(0,1)*gw**2*vR)/cw**2 - (4*cphi**2*cth1*complex(0,1)*gw**2*sw**2*vR)/cw**2',
                  order = {'EWL':2,'EWR':-1})

GC_850 = Coupling(name = 'GC_850',
                  value = '(2*cth1*complex(0,1)*g1*gw*sphi*vR)/cw - (6*cth1*complex(0,1)*g1*gw*sphi*sw**2*vR)/cw',
                  order = {'QED':1})

GC_851 = Coupling(name = 'GC_851',
                  value = '(2*cth1*complex(0,1)*gw**2*sphi**2*vR)/cw**2 - (4*cth1*complex(0,1)*gw**2*sphi**2*sw**2*vR)/cw**2 + (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1*cmath.sqrt(1 - 2*sw**2))/vev + (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1*cmath.sqrt(1 - 2*sw**2))/vev + (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev) + (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev)',
                  order = {'EWR':1})

GC_852 = Coupling(name = 'GC_852',
                  value = '-2*complex(0,1)*g1**2*sth1*vR + 4*complex(0,1)*g1**2*sth1*sw**2*vR',
                  order = {'EWR':-1,'QED':2})

GC_853 = Coupling(name = 'GC_853',
                  value = '(2*cphi*complex(0,1)*g1*gw*sth1*vR)/cw - (6*cphi*complex(0,1)*g1*gw*sth1*sw**2*vR)/cw',
                  order = {'EWL':1,'EWR':-1,'QED':1})

GC_854 = Coupling(name = 'GC_854',
                  value = '(-2*cphi**2*complex(0,1)*gw**2*sth1*vR)/cw**2 + (4*cphi**2*complex(0,1)*gw**2*sth1*sw**2*vR)/cw**2',
                  order = {'EWL':2,'EWR':-1})

GC_855 = Coupling(name = 'GC_855',
                  value = '(-2*complex(0,1)*g1*gw*sphi*sth1*vR)/cw + (6*complex(0,1)*g1*gw*sphi*sth1*sw**2*vR)/cw',
                  order = {'QED':1})

GC_856 = Coupling(name = 'GC_856',
                  value = '(-2*complex(0,1)*gw**2*sphi**2*sth1*vR)/cw**2 + (4*complex(0,1)*gw**2*sphi**2*sth1*sw**2*vR)/cw**2 + (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev + (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev + (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev) + (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev)',
                  order = {'EWR':1})

GC_857 = Coupling(name = 'GC_857',
                  value = '(-2*cphi*cth1*complex(0,1)*g1**2*sw*vR)/cw + (4*cphi*cth1*complex(0,1)*g1**2*sw**3*vR)/cw',
                  order = {'EWR':-1,'QED':2})

GC_858 = Coupling(name = 'GC_858',
                  value = '(2*cphi**2*cth1*complex(0,1)*g1*gw*sw*vR)/cw**2 - (6*cphi**2*cth1*complex(0,1)*g1*gw*sw**3*vR)/cw**2',
                  order = {'EWL':1,'EWR':-1,'QED':1})

GC_859 = Coupling(name = 'GC_859',
                  value = '(-2*cth1*complex(0,1)*g1**2*sphi*sw*vR)/cw + (4*cth1*complex(0,1)*g1**2*sphi*sw**3*vR)/cw',
                  order = {'EWL':-1,'QED':2})

GC_860 = Coupling(name = 'GC_860',
                  value = '(4*cphi*cth1*complex(0,1)*g1*gw*sphi*sw*vR)/cw**2 - (12*cphi*cth1*complex(0,1)*g1*gw*sphi*sw**3*vR)/cw**2',
                  order = {'QED':1})

GC_861 = Coupling(name = 'GC_861',
                  value = '(-4*cphi*cth1*complex(0,1)*g1*gw*sphi*sw*vR)/cw**2 + (12*cphi*cth1*complex(0,1)*g1*gw*sphi*sw**3*vR)/cw**2',
                  order = {'QED':1})

GC_862 = Coupling(name = 'GC_862',
                  value = '(-2*cth1*complex(0,1)*g1*gw*sphi**2*sw*vR)/cw**2 + (6*cth1*complex(0,1)*g1*gw*sphi**2*sw**3*vR)/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_863 = Coupling(name = 'GC_863',
                  value = '(2*cphi*complex(0,1)*g1**2*sth1*sw*vR)/cw - (4*cphi*complex(0,1)*g1**2*sth1*sw**3*vR)/cw',
                  order = {'EWR':-1,'QED':2})

GC_864 = Coupling(name = 'GC_864',
                  value = '(-2*cphi**2*complex(0,1)*g1*gw*sth1*sw*vR)/cw**2 + (6*cphi**2*complex(0,1)*g1*gw*sth1*sw**3*vR)/cw**2',
                  order = {'EWL':1,'EWR':-1,'QED':1})

GC_865 = Coupling(name = 'GC_865',
                  value = '(2*complex(0,1)*g1**2*sphi*sth1*sw*vR)/cw - (4*complex(0,1)*g1**2*sphi*sth1*sw**3*vR)/cw',
                  order = {'EWL':-1,'QED':2})

GC_866 = Coupling(name = 'GC_866',
                  value = '(4*cphi*complex(0,1)*g1*gw*sphi*sth1*sw*vR)/cw**2 - (12*cphi*complex(0,1)*g1*gw*sphi*sth1*sw**3*vR)/cw**2',
                  order = {'QED':1})

GC_867 = Coupling(name = 'GC_867',
                  value = '(-4*cphi*complex(0,1)*g1*gw*sphi*sth1*sw*vR)/cw**2 + (12*cphi*complex(0,1)*g1*gw*sphi*sth1*sw**3*vR)/cw**2',
                  order = {'QED':1})

GC_868 = Coupling(name = 'GC_868',
                  value = '(2*complex(0,1)*g1*gw*sphi**2*sth1*sw*vR)/cw**2 - (6*complex(0,1)*g1*gw*sphi**2*sth1*sw**3*vR)/cw**2',
                  order = {'EWL':-1,'EWR':1,'QED':1})

GC_869 = Coupling(name = 'GC_869',
                  value = '(2*cphi**2*cth1*complex(0,1)*g1**2*sw**2*vR)/cw**2 - (4*cphi**2*cth1*complex(0,1)*g1**2*sw**4*vR)/cw**2',
                  order = {'EWR':-1,'QED':2})

GC_870 = Coupling(name = 'GC_870',
                  value = '(-2*cphi*cth1*complex(0,1)*gw**2*sphi*vR)/cw**2 + (4*cphi*cth1*complex(0,1)*gw**2*sphi*sw**2*vR)/cw**2 + (2*cphi*cth1*complex(0,1)*gw**2*sphi*sw**4*vR)/cw**2 - (cphi**2*complex(0,1)*gw**2*k1**2*sth1*cmath.sqrt(1 - 2*sw**2))/(2.*vev) - (cphi**2*complex(0,1)*gw**2*k2**2*sth1*cmath.sqrt(1 - 2*sw**2))/(2.*vev) - (cphi**2*complex(0,1)*gw**2*k1**2*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev) - (cphi**2*complex(0,1)*gw**2*k2**2*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev)',
                  order = {'EWL':1})

GC_871 = Coupling(name = 'GC_871',
                  value = '(2*cth1*complex(0,1)*g1**2*sphi**2*sw**2*vR)/cw**2 - (4*cth1*complex(0,1)*g1**2*sphi**2*sw**4*vR)/cw**2',
                  order = {'EWL':-2,'EWR':1,'QED':2})

GC_872 = Coupling(name = 'GC_872',
                  value = '(2*cth1*complex(0,1)*gw**2*sphi**2*sw**4*vR)/cw**2 - (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1*cmath.sqrt(1 - 2*sw**2))/vev - (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1*cmath.sqrt(1 - 2*sw**2))/vev - (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev) - (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev)',
                  order = {'EWR':1})

GC_873 = Coupling(name = 'GC_873',
                  value = '(-2*cphi**2*complex(0,1)*g1**2*sth1*sw**2*vR)/cw**2 + (4*cphi**2*complex(0,1)*g1**2*sth1*sw**4*vR)/cw**2',
                  order = {'EWR':-1,'QED':2})

GC_874 = Coupling(name = 'GC_874',
                  value = '(2*cphi*complex(0,1)*gw**2*sphi*sth1*vR)/cw**2 - (4*cphi*complex(0,1)*gw**2*sphi*sth1*sw**2*vR)/cw**2 - (2*cphi*complex(0,1)*gw**2*sphi*sth1*sw**4*vR)/cw**2 - (cphi**2*cth1*complex(0,1)*gw**2*k1**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev) - (cphi**2*cth1*complex(0,1)*gw**2*k2**2*cmath.sqrt(1 - 2*sw**2))/(2.*vev) - (cphi**2*cth1*complex(0,1)*gw**2*k1**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev) - (cphi**2*cth1*complex(0,1)*gw**2*k2**2*sw**2*cmath.sqrt(1 - 2*sw**2))/(2.*cw**2*vev)',
                  order = {'EWL':1})

GC_875 = Coupling(name = 'GC_875',
                  value = '(-2*complex(0,1)*g1**2*sphi**2*sth1*sw**2*vR)/cw**2 + (4*complex(0,1)*g1**2*sphi**2*sth1*sw**4*vR)/cw**2',
                  order = {'EWL':-2,'EWR':1,'QED':2})

GC_876 = Coupling(name = 'GC_876',
                  value = '(-2*complex(0,1)*gw**2*sphi**2*sth1*sw**4*vR)/cw**2 - (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev - (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*cmath.sqrt(1 - 2*sw**2))/vev - (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev) - (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sw**2*cmath.sqrt(1 - 2*sw**2))/(cw**2*vev)',
                  order = {'EWR':1})

GC_877 = Coupling(name = 'GC_877',
                  value = '(cphi**2*complex(0,1)*gw**2*k1**2*sth1)/(2.*cw**2*vev) + (cphi**2*complex(0,1)*gw**2*k2**2*sth1)/(2.*cw**2*vev) - (cphi**2*complex(0,1)*gw**2*k1**2*sth1*sw**2)/(cw**2*vev) - (cphi**2*complex(0,1)*gw**2*k2**2*sth1*sw**2)/(cw**2*vev) - (4*cphi*cth1*complex(0,1)*gw**2*sphi*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1})

GC_878 = Coupling(name = 'GC_878',
                  value = '(cphi**2*cw**2*complex(0,1)*gw**2*k1**2*sth1)/(2.*vev) + (cphi**2*cw**2*complex(0,1)*gw**2*k2**2*sth1)/(2.*vev) + (cphi**2*complex(0,1)*gw**2*k1**2*sth1*sw**2)/vev + (cphi**2*complex(0,1)*gw**2*k2**2*sth1*sw**2)/vev + (cphi**2*complex(0,1)*gw**2*k1**2*sth1*sw**4)/(2.*cw**2*vev) + (cphi**2*complex(0,1)*gw**2*k2**2*sth1*sw**4)/(2.*cw**2*vev) + (4*cphi*cth1*complex(0,1)*gw**2*sphi*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1})

GC_879 = Coupling(name = 'GC_879',
                  value = '-0.5*(cphi*complex(0,1)*gw**2*k1**2*sphi*sth1)/(cw**2*vev) + (cphi*cw**2*complex(0,1)*gw**2*k1**2*sphi*sth1)/(2.*vev) - (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1)/(2.*cw**2*vev) + (cphi*cw**2*complex(0,1)*gw**2*k2**2*sphi*sth1)/(2.*vev) + (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1*sw**2)/vev + (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1*sw**2)/(cw**2*vev) + (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1*sw**2)/vev + (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1*sw**2)/(cw**2*vev) + (cphi*complex(0,1)*gw**2*k1**2*sphi*sth1*sw**4)/(2.*cw**2*vev) + (cphi*complex(0,1)*gw**2*k2**2*sphi*sth1*sw**4)/(2.*cw**2*vev) + (2*cth1*complex(0,1)*gw**2*sphi**2*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWR':1})

GC_880 = Coupling(name = 'GC_880',
                  value = '(cphi**2*cth1*cw**2*complex(0,1)*gw**2*k1**2)/(2.*vev) + (cphi**2*cth1*cw**2*complex(0,1)*gw**2*k2**2)/(2.*vev) + (cphi**2*cth1*complex(0,1)*gw**2*k1**2*sw**2)/vev + (cphi**2*cth1*complex(0,1)*gw**2*k2**2*sw**2)/vev + (cphi**2*cth1*complex(0,1)*gw**2*k1**2*sw**4)/(2.*cw**2*vev) + (cphi**2*cth1*complex(0,1)*gw**2*k2**2*sw**4)/(2.*cw**2*vev) - (4*cphi*complex(0,1)*gw**2*sphi*sth1*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1})

GC_881 = Coupling(name = 'GC_881',
                  value = '(cphi**2*cth1*complex(0,1)*gw**2*k1**2)/(2.*cw**2*vev) + (cphi**2*cth1*complex(0,1)*gw**2*k2**2)/(2.*cw**2*vev) - (cphi**2*cth1*complex(0,1)*gw**2*k1**2*sw**2)/(cw**2*vev) - (cphi**2*cth1*complex(0,1)*gw**2*k2**2*sw**2)/(cw**2*vev) + (4*cphi*complex(0,1)*gw**2*sphi*sth1*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWL':1})

GC_882 = Coupling(name = 'GC_882',
                  value = '-0.5*(cphi*cth1*complex(0,1)*gw**2*k1**2*sphi)/(cw**2*vev) + (cphi*cth1*cw**2*complex(0,1)*gw**2*k1**2*sphi)/(2.*vev) - (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi)/(2.*cw**2*vev) + (cphi*cth1*cw**2*complex(0,1)*gw**2*k2**2*sphi)/(2.*vev) + (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sw**2)/vev + (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sw**2)/(cw**2*vev) + (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sw**2)/vev + (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sw**2)/(cw**2*vev) + (cphi*cth1*complex(0,1)*gw**2*k1**2*sphi*sw**4)/(2.*cw**2*vev) + (cphi*cth1*complex(0,1)*gw**2*k2**2*sphi*sw**4)/(2.*cw**2*vev) - (2*complex(0,1)*gw**2*sphi**2*sth1*sw**2*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'EWR':1})

GC_883 = Coupling(name = 'GC_883',
                  value = '(-4*cphi*cth1*complex(0,1)*g1*gw*sphi*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw**2 - (4*cphi*cth1*complex(0,1)*g1*gw*sphi*sw**3*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'QED':1})

GC_884 = Coupling(name = 'GC_884',
                  value = '(4*cphi*complex(0,1)*g1*gw*sphi*sth1*sw*vR*cmath.sqrt(1 - 2*sw**2))/cw**2 + (4*cphi*complex(0,1)*g1*gw*sphi*sth1*sw**3*vR*cmath.sqrt(1 - 2*sw**2))/cw**2',
                  order = {'QED':1})

GC_885 = Coupling(name = 'GC_885',
                  value = '-((cxi**2*complex(0,1)*gw**2*k1*k2*sth1)/vev) - cth1*cxi*complex(0,1)*gw**2*sxi*vR',
                  order = {'EWL':1})

GC_886 = Coupling(name = 'GC_886',
                  value = '-((cth1*cxi**2*complex(0,1)*gw**2*k1*k2)/vev) + cxi*complex(0,1)*gw**2*sth1*sxi*vR',
                  order = {'EWL':1})

GC_887 = Coupling(name = 'GC_887',
                  value = '(2*cxi*complex(0,1)*gw**2*k1*k2*sth1*sxi)/vev + cth1*complex(0,1)*gw**2*sxi**2*vR',
                  order = {'EWR':1})

GC_888 = Coupling(name = 'GC_888',
                  value = '(2*cth1*cxi*complex(0,1)*gw**2*k1*k2*sxi)/vev - complex(0,1)*gw**2*sth1*sxi**2*vR',
                  order = {'EWR':1})

GC_889 = Coupling(name = 'GC_889',
                  value = '-((cth1*complex(0,1)*yDO1x1)/vev)',
                  order = {'EWL':1})

GC_890 = Coupling(name = 'GC_890',
                  value = '(2*complex(0,1)*k1*k2*yDO1x1)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_891 = Coupling(name = 'GC_891',
                  value = '(2*k1*k2*yDO1x1)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_892 = Coupling(name = 'GC_892',
                  value = '-((complex(0,1)*sth1*yDO1x1)/vev)',
                  order = {'EWL':1})

GC_893 = Coupling(name = 'GC_893',
                  value = '-((cth1*complex(0,1)*yDO2x2)/vev)',
                  order = {'EWL':1})

GC_894 = Coupling(name = 'GC_894',
                  value = '(2*complex(0,1)*k1*k2*yDO2x2)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_895 = Coupling(name = 'GC_895',
                  value = '(2*k1*k2*yDO2x2)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_896 = Coupling(name = 'GC_896',
                  value = '-((complex(0,1)*sth1*yDO2x2)/vev)',
                  order = {'EWL':1})

GC_897 = Coupling(name = 'GC_897',
                  value = '-((cth1*complex(0,1)*yDO3x3)/vev)',
                  order = {'EWL':1})

GC_898 = Coupling(name = 'GC_898',
                  value = '(2*complex(0,1)*k1*k2*yDO3x3)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_899 = Coupling(name = 'GC_899',
                  value = '(2*k1*k2*yDO3x3)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_900 = Coupling(name = 'GC_900',
                  value = '-((complex(0,1)*sth1*yDO3x3)/vev)',
                  order = {'EWL':1})

GC_901 = Coupling(name = 'GC_901',
                  value = '-((cth1*complex(0,1)*yML1x1)/vev)',
                  order = {'EWL':1})

GC_902 = Coupling(name = 'GC_902',
                  value = '-((complex(0,1)*sth1*yML1x1)/vev)',
                  order = {'EWL':1})

GC_903 = Coupling(name = 'GC_903',
                  value = '-((cth1*complex(0,1)*yML2x2)/vev)',
                  order = {'EWL':1})

GC_904 = Coupling(name = 'GC_904',
                  value = '-((complex(0,1)*sth1*yML2x2)/vev)',
                  order = {'EWL':1})

GC_905 = Coupling(name = 'GC_905',
                  value = '-((cth1*complex(0,1)*yML3x3)/vev)',
                  order = {'EWL':1})

GC_906 = Coupling(name = 'GC_906',
                  value = '-((complex(0,1)*sth1*yML3x3)/vev)',
                  order = {'EWL':1})

GC_907 = Coupling(name = 'GC_907',
                  value = '-((cth1*complex(0,1)*yMU1x1)/vev)',
                  order = {'EWL':1})

GC_908 = Coupling(name = 'GC_908',
                  value = '(-2*k1*k2*yMU1x1)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_909 = Coupling(name = 'GC_909',
                  value = '(2*complex(0,1)*k1*k2*yMU1x1)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_910 = Coupling(name = 'GC_910',
                  value = '-((complex(0,1)*sth1*yMU1x1)/vev)',
                  order = {'EWL':1})

GC_911 = Coupling(name = 'GC_911',
                  value = '(CKMR1x1*complex(0,1)*k1**2*yDO1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x1*complex(0,1)*k2**2*yDO1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*CKML1x1*complex(0,1)*k1*k2*yMU1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_912 = Coupling(name = 'GC_912',
                  value = '(CKMR1x2*complex(0,1)*k1**2*yDO2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x2*complex(0,1)*k2**2*yDO2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*CKML1x2*complex(0,1)*k1*k2*yMU1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_913 = Coupling(name = 'GC_913',
                  value = '(CKMR1x3*complex(0,1)*k1**2*yDO3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x3*complex(0,1)*k2**2*yDO3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*CKML1x3*complex(0,1)*k1*k2*yMU1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_914 = Coupling(name = 'GC_914',
                  value = '(2*CKML1x1*complex(0,1)*k1*k2*yDO1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x1*complex(0,1)*k1**2*yMU1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x1*complex(0,1)*k2**2*yMU1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_915 = Coupling(name = 'GC_915',
                  value = '(2*CKML1x2*complex(0,1)*k1*k2*yDO2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x2*complex(0,1)*k1**2*yMU1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x2*complex(0,1)*k2**2*yMU1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_916 = Coupling(name = 'GC_916',
                  value = '(2*CKML1x3*complex(0,1)*k1*k2*yDO3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x3*complex(0,1)*k1**2*yMU1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x3*complex(0,1)*k2**2*yMU1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_917 = Coupling(name = 'GC_917',
                  value = '-((cth1*complex(0,1)*yMU2x2)/vev)',
                  order = {'EWL':1})

GC_918 = Coupling(name = 'GC_918',
                  value = '(-2*k1*k2*yMU2x2)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_919 = Coupling(name = 'GC_919',
                  value = '(2*complex(0,1)*k1*k2*yMU2x2)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_920 = Coupling(name = 'GC_920',
                  value = '-((complex(0,1)*sth1*yMU2x2)/vev)',
                  order = {'EWL':1})

GC_921 = Coupling(name = 'GC_921',
                  value = '(CKMR2x1*complex(0,1)*k1**2*yDO1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x1*complex(0,1)*k2**2*yDO1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*CKML2x1*complex(0,1)*k1*k2*yMU2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_922 = Coupling(name = 'GC_922',
                  value = '(CKMR2x2*complex(0,1)*k1**2*yDO2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*complex(0,1)*k2**2*yDO2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*CKML2x2*complex(0,1)*k1*k2*yMU2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_923 = Coupling(name = 'GC_923',
                  value = '(CKMR2x3*complex(0,1)*k1**2*yDO3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*complex(0,1)*k2**2*yDO3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*CKML2x3*complex(0,1)*k1*k2*yMU2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_924 = Coupling(name = 'GC_924',
                  value = '(2*CKML2x1*complex(0,1)*k1*k2*yDO1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x1*complex(0,1)*k1**2*yMU2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x1*complex(0,1)*k2**2*yMU2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_925 = Coupling(name = 'GC_925',
                  value = '(2*CKML2x2*complex(0,1)*k1*k2*yDO2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k1**2*yMU2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k2**2*yMU2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_926 = Coupling(name = 'GC_926',
                  value = '(2*CKML2x3*complex(0,1)*k1*k2*yDO3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k1**2*yMU2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k2**2*yMU2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_927 = Coupling(name = 'GC_927',
                  value = '-((cth1*complex(0,1)*yMU3x3)/vev)',
                  order = {'EWL':1})

GC_928 = Coupling(name = 'GC_928',
                  value = '(-2*k1*k2*yMU3x3)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_929 = Coupling(name = 'GC_929',
                  value = '(2*complex(0,1)*k1*k2*yMU3x3)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_930 = Coupling(name = 'GC_930',
                  value = '-((complex(0,1)*sth1*yMU3x3)/vev)',
                  order = {'EWL':1})

GC_931 = Coupling(name = 'GC_931',
                  value = '(CKMR3x1*complex(0,1)*k1**2*yDO1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x1*complex(0,1)*k2**2*yDO1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*CKML3x1*complex(0,1)*k1*k2*yMU3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_932 = Coupling(name = 'GC_932',
                  value = '(CKMR3x2*complex(0,1)*k1**2*yDO2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*complex(0,1)*k2**2*yDO2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*CKML3x2*complex(0,1)*k1*k2*yMU3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_933 = Coupling(name = 'GC_933',
                  value = '(CKMR3x3*complex(0,1)*k1**2*yDO3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*complex(0,1)*k2**2*yDO3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*CKML3x3*complex(0,1)*k1*k2*yMU3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_934 = Coupling(name = 'GC_934',
                  value = '(2*CKML3x1*complex(0,1)*k1*k2*yDO1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x1*complex(0,1)*k1**2*yMU3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x1*complex(0,1)*k2**2*yMU3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_935 = Coupling(name = 'GC_935',
                  value = '(2*CKML3x2*complex(0,1)*k1*k2*yDO2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k1**2*yMU3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k2**2*yMU3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_936 = Coupling(name = 'GC_936',
                  value = '(2*CKML3x3*complex(0,1)*k1*k2*yDO3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k1**2*yMU3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k2**2*yMU3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_937 = Coupling(name = 'GC_937',
                  value = '(2*k1*k2*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (k1**2*KL1x1*KR1x1*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) - (k2**2*KL1x1*KR1x1*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) - (k1**2*KL4x1*KR4x1*yNL4x4)/((k1 - k2)*(k1 + k2)*vev) - (k2**2*KL4x1*KR4x1*yNL4x4)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_938 = Coupling(name = 'GC_938',
                  value = '(2*complex(0,1)*k1*k2*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL1x1*KR1x1*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL1x1*KR1x1*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL4x1*KR4x1*yNL4x4)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL4x1*KR4x1*yNL4x4)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_939 = Coupling(name = 'GC_939',
                  value = '(-2*k1*k2*yML1x1)/((k1 - k2)*(k1 + k2)*vev) + (k1**2*KL1x1*KR1x1*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) + (k2**2*KL1x1*KR1x1*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) + (k1**2*KL4x1*KR4x1*yNL4x4)/((k1 - k2)*(k1 + k2)*vev) + (k2**2*KL4x1*KR4x1*yNL4x4)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_940 = Coupling(name = 'GC_940',
                  value = '(2*complex(0,1)*k1*k2*KL1x1*yML1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL1x1**2*KR1x1*yNL1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL1x1**2*KR1x1*yNL1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL1x1*KL4x1*KR4x1*yNL4x4*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL1x1*KL4x1*KR4x1*yNL4x4*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_941 = Coupling(name = 'GC_941',
                  value = '(2*complex(0,1)*k1*k2*KL4x1*yML1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL1x1*KL4x1*KR1x1*yNL1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL1x1*KL4x1*KR1x1*yNL1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL4x1**2*KR4x1*yNL4x4*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL4x1**2*KR4x1*yNL4x4*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_942 = Coupling(name = 'GC_942',
                  value = '(complex(0,1)*k1**2*KR1x1*yML1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*KR1x1*yML1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k1*k2*KL1x1*KR1x1**2*yNL1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k1*k2*KL4x1*KR1x1*KR4x1*yNL4x4*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_943 = Coupling(name = 'GC_943',
                  value = '(-2*cth1*complex(0,1)*KL1x1**2*KR1x1**2*yNL1x1)/vev - (2*cth1*complex(0,1)*KL1x1*KL4x1*KR1x1*KR4x1*yNL4x4)/vev',
                  order = {'EWL':1})

GC_944 = Coupling(name = 'GC_944',
                  value = '(2*k1**2*KL1x1*KR1x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) + (2*k2**2*KL1x1*KR1x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (4*k1*k2*KL1x1**2*KR1x1**2*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) - (4*k1*k2*KL1x1*KL4x1*KR1x1*KR4x1*yNL4x4)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_945 = Coupling(name = 'GC_945',
                  value = '(-2*complex(0,1)*k1**2*KL1x1*KR1x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k2**2*KL1x1*KR1x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) + (4*complex(0,1)*k1*k2*KL1x1**2*KR1x1**2*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) + (4*complex(0,1)*k1*k2*KL1x1*KL4x1*KR1x1*KR4x1*yNL4x4)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_946 = Coupling(name = 'GC_946',
                  value = '(-2*k1**2*KL1x1*KR1x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (2*k2**2*KL1x1*KR1x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) + (4*k1*k2*KL1x1**2*KR1x1**2*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) + (4*k1*k2*KL1x1*KL4x1*KR1x1*KR4x1*yNL4x4)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_947 = Coupling(name = 'GC_947',
                  value = '(complex(0,1)*k1**2*KR4x1*yML1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*KR4x1*yML1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k1*k2*KL1x1*KR1x1*KR4x1*yNL1x1*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k1*k2*KL4x1*KR4x1**2*yNL4x4*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_948 = Coupling(name = 'GC_948',
                  value = '-((cth1*complex(0,1)*KL1x1*KL4x1*KR1x1**2*yNL1x1)/vev) - (cth1*complex(0,1)*KL1x1**2*KR1x1*KR4x1*yNL1x1)/vev - (cth1*complex(0,1)*KL4x1**2*KR1x1*KR4x1*yNL4x4)/vev - (cth1*complex(0,1)*KL1x1*KL4x1*KR4x1**2*yNL4x4)/vev',
                  order = {'EWL':1})

GC_949 = Coupling(name = 'GC_949',
                  value = '(k1**2*KL4x1*KR1x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) + (k2**2*KL4x1*KR1x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) + (k1**2*KL1x1*KR4x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) + (k2**2*KL1x1*KR4x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (2*k1*k2*KL1x1*KL4x1*KR1x1**2*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) - (2*k1*k2*KL1x1**2*KR1x1*KR4x1*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) - (2*k1*k2*KL4x1**2*KR1x1*KR4x1*yNL4x4)/((k1 - k2)*(k1 + k2)*vev) - (2*k1*k2*KL1x1*KL4x1*KR4x1**2*yNL4x4)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_950 = Coupling(name = 'GC_950',
                  value = '-((complex(0,1)*k1**2*KL4x1*KR1x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev)) - (complex(0,1)*k2**2*KL4x1*KR1x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL1x1*KR4x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL1x1*KR4x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) + (2*complex(0,1)*k1*k2*KL1x1*KL4x1*KR1x1**2*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) + (2*complex(0,1)*k1*k2*KL1x1**2*KR1x1*KR4x1*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) + (2*complex(0,1)*k1*k2*KL4x1**2*KR1x1*KR4x1*yNL4x4)/((k1 - k2)*(k1 + k2)*vev) + (2*complex(0,1)*k1*k2*KL1x1*KL4x1*KR4x1**2*yNL4x4)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_951 = Coupling(name = 'GC_951',
                  value = '-((k1**2*KL4x1*KR1x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev)) - (k2**2*KL4x1*KR1x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (k1**2*KL1x1*KR4x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (k2**2*KL1x1*KR4x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) + (2*k1*k2*KL1x1*KL4x1*KR1x1**2*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) + (2*k1*k2*KL1x1**2*KR1x1*KR4x1*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) + (2*k1*k2*KL4x1**2*KR1x1*KR4x1*yNL4x4)/((k1 - k2)*(k1 + k2)*vev) + (2*k1*k2*KL1x1*KL4x1*KR4x1**2*yNL4x4)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_952 = Coupling(name = 'GC_952',
                  value = '(-2*cth1*complex(0,1)*KL1x1*KL4x1*KR1x1*KR4x1*yNL1x1)/vev - (2*cth1*complex(0,1)*KL4x1**2*KR4x1**2*yNL4x4)/vev',
                  order = {'EWL':1})

GC_953 = Coupling(name = 'GC_953',
                  value = '(2*k1**2*KL4x1*KR4x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) + (2*k2**2*KL4x1*KR4x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (4*k1*k2*KL1x1*KL4x1*KR1x1*KR4x1*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) - (4*k1*k2*KL4x1**2*KR4x1**2*yNL4x4)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_954 = Coupling(name = 'GC_954',
                  value = '(-2*complex(0,1)*k1**2*KL4x1*KR4x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k2**2*KL4x1*KR4x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) + (4*complex(0,1)*k1*k2*KL1x1*KL4x1*KR1x1*KR4x1*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) + (4*complex(0,1)*k1*k2*KL4x1**2*KR4x1**2*yNL4x4)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_955 = Coupling(name = 'GC_955',
                  value = '(-2*k1**2*KL4x1*KR4x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) - (2*k2**2*KL4x1*KR4x1*yML1x1)/((k1 - k2)*(k1 + k2)*vev) + (4*k1*k2*KL1x1*KL4x1*KR1x1*KR4x1*yNL1x1)/((k1 - k2)*(k1 + k2)*vev) + (4*k1*k2*KL4x1**2*KR4x1**2*yNL4x4)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_956 = Coupling(name = 'GC_956',
                  value = '(-2*complex(0,1)*KL1x1**2*KR1x1**2*sth1*yNL1x1)/vev - (2*complex(0,1)*KL1x1*KL4x1*KR1x1*KR4x1*sth1*yNL4x4)/vev',
                  order = {'EWL':1})

GC_957 = Coupling(name = 'GC_957',
                  value = '-((complex(0,1)*KL1x1*KL4x1*KR1x1**2*sth1*yNL1x1)/vev) - (complex(0,1)*KL1x1**2*KR1x1*KR4x1*sth1*yNL1x1)/vev - (complex(0,1)*KL4x1**2*KR1x1*KR4x1*sth1*yNL4x4)/vev - (complex(0,1)*KL1x1*KL4x1*KR4x1**2*sth1*yNL4x4)/vev',
                  order = {'EWL':1})

GC_958 = Coupling(name = 'GC_958',
                  value = '(-2*complex(0,1)*KL1x1*KL4x1*KR1x1*KR4x1*sth1*yNL1x1)/vev - (2*complex(0,1)*KL4x1**2*KR4x1**2*sth1*yNL4x4)/vev',
                  order = {'EWL':1})

GC_959 = Coupling(name = 'GC_959',
                  value = '(complex(0,1)*KR1x1**2*yNL1x1*cmath.sqrt(2))/vR + (complex(0,1)*KR4x1**2*yNL4x4*cmath.sqrt(2))/vR',
                  order = {'EWR':1})

GC_960 = Coupling(name = 'GC_960',
                  value = '-((cth1*complex(0,1)*KR1x1**4*yNL1x1)/vR) - (cth1*complex(0,1)*KR1x1**2*KR4x1**2*yNL4x4)/vR',
                  order = {'EWR':1})

GC_961 = Coupling(name = 'GC_961',
                  value = '-((cth1*complex(0,1)*KR1x1**3*KR4x1*yNL1x1)/vR) - (cth1*complex(0,1)*KR1x1*KR4x1**3*yNL4x4)/vR',
                  order = {'EWR':1})

GC_962 = Coupling(name = 'GC_962',
                  value = '-((cth1*complex(0,1)*KR1x1**2*KR4x1**2*yNL1x1)/vR) - (cth1*complex(0,1)*KR4x1**4*yNL4x4)/vR',
                  order = {'EWR':1})

GC_963 = Coupling(name = 'GC_963',
                  value = '(complex(0,1)*KR1x1**4*sth1*yNL1x1)/vR + (complex(0,1)*KR1x1**2*KR4x1**2*sth1*yNL4x4)/vR',
                  order = {'EWR':1})

GC_964 = Coupling(name = 'GC_964',
                  value = '(complex(0,1)*KR1x1**3*KR4x1*sth1*yNL1x1)/vR + (complex(0,1)*KR1x1*KR4x1**3*sth1*yNL4x4)/vR',
                  order = {'EWR':1})

GC_965 = Coupling(name = 'GC_965',
                  value = '(complex(0,1)*KR1x1**2*KR4x1**2*sth1*yNL1x1)/vR + (complex(0,1)*KR4x1**4*sth1*yNL4x4)/vR',
                  order = {'EWR':1})

GC_966 = Coupling(name = 'GC_966',
                  value = '(complex(0,1)*KR1x1**2*Wl1x1**2*yNL1x1*cmath.sqrt(2))/vR + (complex(0,1)*KR4x1**2*Wl1x1**2*yNL4x4*cmath.sqrt(2))/vR',
                  order = {'EWR':1})

GC_967 = Coupling(name = 'GC_967',
                  value = '(complex(0,1)*KL1x1*KR1x1**2*Wl1x1**2*yNL1x1)/vR + (complex(0,1)*KL1x1*KR4x1**2*Wl1x1**2*yNL4x4)/vR',
                  order = {'EWR':1})

GC_968 = Coupling(name = 'GC_968',
                  value = '-((complex(0,1)*KL1x1**2*KR1x1**2*Wl1x1**2*yNL1x1)/vR) - (complex(0,1)*KL1x1**2*KR4x1**2*Wl1x1**2*yNL4x4)/vR',
                  order = {'EWR':1})

GC_969 = Coupling(name = 'GC_969',
                  value = '(KL1x1**2*KR1x1**2*Wl1x1**2*yNL1x1)/vR + (KL1x1**2*KR4x1**2*Wl1x1**2*yNL4x4)/vR',
                  order = {'EWR':1})

GC_970 = Coupling(name = 'GC_970',
                  value = '(complex(0,1)*KL4x1*KR1x1**2*Wl1x1**2*yNL1x1)/vR + (complex(0,1)*KL4x1*KR4x1**2*Wl1x1**2*yNL4x4)/vR',
                  order = {'EWR':1})

GC_971 = Coupling(name = 'GC_971',
                  value = '-((complex(0,1)*KL1x1*KL4x1*KR1x1**2*Wl1x1**2*yNL1x1)/vR) - (complex(0,1)*KL1x1*KL4x1*KR4x1**2*Wl1x1**2*yNL4x4)/vR',
                  order = {'EWR':1})

GC_972 = Coupling(name = 'GC_972',
                  value = '(KL1x1*KL4x1*KR1x1**2*Wl1x1**2*yNL1x1)/vR + (KL1x1*KL4x1*KR4x1**2*Wl1x1**2*yNL4x4)/vR',
                  order = {'EWR':1})

GC_973 = Coupling(name = 'GC_973',
                  value = '-((complex(0,1)*KL4x1**2*KR1x1**2*Wl1x1**2*yNL1x1)/vR) - (complex(0,1)*KL4x1**2*KR4x1**2*Wl1x1**2*yNL4x4)/vR',
                  order = {'EWR':1})

GC_974 = Coupling(name = 'GC_974',
                  value = '(KL4x1**2*KR1x1**2*Wl1x1**2*yNL1x1)/vR + (KL4x1**2*KR4x1**2*Wl1x1**2*yNL4x4)/vR',
                  order = {'EWR':1})

GC_975 = Coupling(name = 'GC_975',
                  value = '(2*k1*k2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (k1**2*KL2x2*KR2x2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) - (k2**2*KL2x2*KR2x2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) - (k1**2*KL5x2*KR5x2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev) - (k2**2*KL5x2*KR5x2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_976 = Coupling(name = 'GC_976',
                  value = '(2*complex(0,1)*k1*k2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL2x2*KR2x2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL2x2*KR2x2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL5x2*KR5x2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL5x2*KR5x2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_977 = Coupling(name = 'GC_977',
                  value = '(-2*k1*k2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) + (k1**2*KL2x2*KR2x2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) + (k2**2*KL2x2*KR2x2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) + (k1**2*KL5x2*KR5x2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev) + (k2**2*KL5x2*KR5x2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_978 = Coupling(name = 'GC_978',
                  value = '(2*complex(0,1)*k1*k2*KL2x2*yML2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL2x2**2*KR2x2*yNL2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL2x2**2*KR2x2*yNL2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL2x2*KL5x2*KR5x2*yNL5x5*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL2x2*KL5x2*KR5x2*yNL5x5*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_979 = Coupling(name = 'GC_979',
                  value = '(2*complex(0,1)*k1*k2*KL5x2*yML2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL2x2*KL5x2*KR2x2*yNL2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL2x2*KL5x2*KR2x2*yNL2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL5x2**2*KR5x2*yNL5x5*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL5x2**2*KR5x2*yNL5x5*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_980 = Coupling(name = 'GC_980',
                  value = '(complex(0,1)*k1**2*KR2x2*yML2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*KR2x2*yML2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k1*k2*KL2x2*KR2x2**2*yNL2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k1*k2*KL5x2*KR2x2*KR5x2*yNL5x5*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_981 = Coupling(name = 'GC_981',
                  value = '(-2*cth1*complex(0,1)*KL2x2**2*KR2x2**2*yNL2x2)/vev - (2*cth1*complex(0,1)*KL2x2*KL5x2*KR2x2*KR5x2*yNL5x5)/vev',
                  order = {'EWL':1})

GC_982 = Coupling(name = 'GC_982',
                  value = '(2*k1**2*KL2x2*KR2x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) + (2*k2**2*KL2x2*KR2x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (4*k1*k2*KL2x2**2*KR2x2**2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) - (4*k1*k2*KL2x2*KL5x2*KR2x2*KR5x2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_983 = Coupling(name = 'GC_983',
                  value = '(-2*complex(0,1)*k1**2*KL2x2*KR2x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k2**2*KL2x2*KR2x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) + (4*complex(0,1)*k1*k2*KL2x2**2*KR2x2**2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) + (4*complex(0,1)*k1*k2*KL2x2*KL5x2*KR2x2*KR5x2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_984 = Coupling(name = 'GC_984',
                  value = '(-2*k1**2*KL2x2*KR2x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (2*k2**2*KL2x2*KR2x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) + (4*k1*k2*KL2x2**2*KR2x2**2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) + (4*k1*k2*KL2x2*KL5x2*KR2x2*KR5x2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_985 = Coupling(name = 'GC_985',
                  value = '(complex(0,1)*k1**2*KR5x2*yML2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*KR5x2*yML2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k1*k2*KL2x2*KR2x2*KR5x2*yNL2x2*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k1*k2*KL5x2*KR5x2**2*yNL5x5*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_986 = Coupling(name = 'GC_986',
                  value = '-((cth1*complex(0,1)*KL2x2*KL5x2*KR2x2**2*yNL2x2)/vev) - (cth1*complex(0,1)*KL2x2**2*KR2x2*KR5x2*yNL2x2)/vev - (cth1*complex(0,1)*KL5x2**2*KR2x2*KR5x2*yNL5x5)/vev - (cth1*complex(0,1)*KL2x2*KL5x2*KR5x2**2*yNL5x5)/vev',
                  order = {'EWL':1})

GC_987 = Coupling(name = 'GC_987',
                  value = '(k1**2*KL5x2*KR2x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) + (k2**2*KL5x2*KR2x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) + (k1**2*KL2x2*KR5x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) + (k2**2*KL2x2*KR5x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (2*k1*k2*KL2x2*KL5x2*KR2x2**2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) - (2*k1*k2*KL2x2**2*KR2x2*KR5x2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) - (2*k1*k2*KL5x2**2*KR2x2*KR5x2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev) - (2*k1*k2*KL2x2*KL5x2*KR5x2**2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_988 = Coupling(name = 'GC_988',
                  value = '-((complex(0,1)*k1**2*KL5x2*KR2x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev)) - (complex(0,1)*k2**2*KL5x2*KR2x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL2x2*KR5x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL2x2*KR5x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) + (2*complex(0,1)*k1*k2*KL2x2*KL5x2*KR2x2**2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) + (2*complex(0,1)*k1*k2*KL2x2**2*KR2x2*KR5x2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) + (2*complex(0,1)*k1*k2*KL5x2**2*KR2x2*KR5x2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev) + (2*complex(0,1)*k1*k2*KL2x2*KL5x2*KR5x2**2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_989 = Coupling(name = 'GC_989',
                  value = '-((k1**2*KL5x2*KR2x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev)) - (k2**2*KL5x2*KR2x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (k1**2*KL2x2*KR5x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (k2**2*KL2x2*KR5x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) + (2*k1*k2*KL2x2*KL5x2*KR2x2**2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) + (2*k1*k2*KL2x2**2*KR2x2*KR5x2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) + (2*k1*k2*KL5x2**2*KR2x2*KR5x2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev) + (2*k1*k2*KL2x2*KL5x2*KR5x2**2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_990 = Coupling(name = 'GC_990',
                  value = '(-2*cth1*complex(0,1)*KL2x2*KL5x2*KR2x2*KR5x2*yNL2x2)/vev - (2*cth1*complex(0,1)*KL5x2**2*KR5x2**2*yNL5x5)/vev',
                  order = {'EWL':1})

GC_991 = Coupling(name = 'GC_991',
                  value = '(2*k1**2*KL5x2*KR5x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) + (2*k2**2*KL5x2*KR5x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (4*k1*k2*KL2x2*KL5x2*KR2x2*KR5x2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) - (4*k1*k2*KL5x2**2*KR5x2**2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_992 = Coupling(name = 'GC_992',
                  value = '(-2*complex(0,1)*k1**2*KL5x2*KR5x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k2**2*KL5x2*KR5x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) + (4*complex(0,1)*k1*k2*KL2x2*KL5x2*KR2x2*KR5x2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) + (4*complex(0,1)*k1*k2*KL5x2**2*KR5x2**2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_993 = Coupling(name = 'GC_993',
                  value = '(-2*k1**2*KL5x2*KR5x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) - (2*k2**2*KL5x2*KR5x2*yML2x2)/((k1 - k2)*(k1 + k2)*vev) + (4*k1*k2*KL2x2*KL5x2*KR2x2*KR5x2*yNL2x2)/((k1 - k2)*(k1 + k2)*vev) + (4*k1*k2*KL5x2**2*KR5x2**2*yNL5x5)/((k1 - k2)*(k1 + k2)*vev)',
                  order = {'EWL':1})

GC_994 = Coupling(name = 'GC_994',
                  value = '(-2*complex(0,1)*KL2x2**2*KR2x2**2*sth1*yNL2x2)/vev - (2*complex(0,1)*KL2x2*KL5x2*KR2x2*KR5x2*sth1*yNL5x5)/vev',
                  order = {'EWL':1})

GC_995 = Coupling(name = 'GC_995',
                  value = '-((complex(0,1)*KL2x2*KL5x2*KR2x2**2*sth1*yNL2x2)/vev) - (complex(0,1)*KL2x2**2*KR2x2*KR5x2*sth1*yNL2x2)/vev - (complex(0,1)*KL5x2**2*KR2x2*KR5x2*sth1*yNL5x5)/vev - (complex(0,1)*KL2x2*KL5x2*KR5x2**2*sth1*yNL5x5)/vev',
                  order = {'EWL':1})

GC_996 = Coupling(name = 'GC_996',
                  value = '(-2*complex(0,1)*KL2x2*KL5x2*KR2x2*KR5x2*sth1*yNL2x2)/vev - (2*complex(0,1)*KL5x2**2*KR5x2**2*sth1*yNL5x5)/vev',
                  order = {'EWL':1})

GC_997 = Coupling(name = 'GC_997',
                  value = '(complex(0,1)*KR2x2**2*yNL2x2*cmath.sqrt(2))/vR + (complex(0,1)*KR5x2**2*yNL5x5*cmath.sqrt(2))/vR',
                  order = {'EWR':1})

GC_998 = Coupling(name = 'GC_998',
                  value = '-((cth1*complex(0,1)*KR2x2**4*yNL2x2)/vR) - (cth1*complex(0,1)*KR2x2**2*KR5x2**2*yNL5x5)/vR',
                  order = {'EWR':1})

GC_999 = Coupling(name = 'GC_999',
                  value = '-((cth1*complex(0,1)*KR2x2**3*KR5x2*yNL2x2)/vR) - (cth1*complex(0,1)*KR2x2*KR5x2**3*yNL5x5)/vR',
                  order = {'EWR':1})

GC_1000 = Coupling(name = 'GC_1000',
                   value = '-((cth1*complex(0,1)*KR2x2**2*KR5x2**2*yNL2x2)/vR) - (cth1*complex(0,1)*KR5x2**4*yNL5x5)/vR',
                   order = {'EWR':1})

GC_1001 = Coupling(name = 'GC_1001',
                   value = '(complex(0,1)*KR2x2**4*sth1*yNL2x2)/vR + (complex(0,1)*KR2x2**2*KR5x2**2*sth1*yNL5x5)/vR',
                   order = {'EWR':1})

GC_1002 = Coupling(name = 'GC_1002',
                   value = '(complex(0,1)*KR2x2**3*KR5x2*sth1*yNL2x2)/vR + (complex(0,1)*KR2x2*KR5x2**3*sth1*yNL5x5)/vR',
                   order = {'EWR':1})

GC_1003 = Coupling(name = 'GC_1003',
                   value = '(complex(0,1)*KR2x2**2*KR5x2**2*sth1*yNL2x2)/vR + (complex(0,1)*KR5x2**4*sth1*yNL5x5)/vR',
                   order = {'EWR':1})

GC_1004 = Coupling(name = 'GC_1004',
                   value = '(complex(0,1)*KR2x2**2*Wl2x2**2*yNL2x2*cmath.sqrt(2))/vR + (complex(0,1)*KR5x2**2*Wl2x2**2*yNL5x5*cmath.sqrt(2))/vR',
                   order = {'EWR':1})

GC_1005 = Coupling(name = 'GC_1005',
                   value = '(complex(0,1)*KL2x2*KR2x2**2*Wl2x2**2*yNL2x2)/vR + (complex(0,1)*KL2x2*KR5x2**2*Wl2x2**2*yNL5x5)/vR',
                   order = {'EWR':1})

GC_1006 = Coupling(name = 'GC_1006',
                   value = '-((complex(0,1)*KL2x2**2*KR2x2**2*Wl2x2**2*yNL2x2)/vR) - (complex(0,1)*KL2x2**2*KR5x2**2*Wl2x2**2*yNL5x5)/vR',
                   order = {'EWR':1})

GC_1007 = Coupling(name = 'GC_1007',
                   value = '(KL2x2**2*KR2x2**2*Wl2x2**2*yNL2x2)/vR + (KL2x2**2*KR5x2**2*Wl2x2**2*yNL5x5)/vR',
                   order = {'EWR':1})

GC_1008 = Coupling(name = 'GC_1008',
                   value = '(complex(0,1)*KL5x2*KR2x2**2*Wl2x2**2*yNL2x2)/vR + (complex(0,1)*KL5x2*KR5x2**2*Wl2x2**2*yNL5x5)/vR',
                   order = {'EWR':1})

GC_1009 = Coupling(name = 'GC_1009',
                   value = '-((complex(0,1)*KL2x2*KL5x2*KR2x2**2*Wl2x2**2*yNL2x2)/vR) - (complex(0,1)*KL2x2*KL5x2*KR5x2**2*Wl2x2**2*yNL5x5)/vR',
                   order = {'EWR':1})

GC_1010 = Coupling(name = 'GC_1010',
                   value = '(KL2x2*KL5x2*KR2x2**2*Wl2x2**2*yNL2x2)/vR + (KL2x2*KL5x2*KR5x2**2*Wl2x2**2*yNL5x5)/vR',
                   order = {'EWR':1})

GC_1011 = Coupling(name = 'GC_1011',
                   value = '-((complex(0,1)*KL5x2**2*KR2x2**2*Wl2x2**2*yNL2x2)/vR) - (complex(0,1)*KL5x2**2*KR5x2**2*Wl2x2**2*yNL5x5)/vR',
                   order = {'EWR':1})

GC_1012 = Coupling(name = 'GC_1012',
                   value = '(KL5x2**2*KR2x2**2*Wl2x2**2*yNL2x2)/vR + (KL5x2**2*KR5x2**2*Wl2x2**2*yNL5x5)/vR',
                   order = {'EWR':1})

GC_1013 = Coupling(name = 'GC_1013',
                   value = '(2*k1*k2*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (k1**2*KL3x3*KR3x3*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) - (k2**2*KL3x3*KR3x3*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) - (k1**2*KL6x3*KR6x3*yNL6x6)/((k1 - k2)*(k1 + k2)*vev) - (k2**2*KL6x3*KR6x3*yNL6x6)/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1014 = Coupling(name = 'GC_1014',
                   value = '(2*complex(0,1)*k1*k2*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL3x3*KR3x3*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL3x3*KR3x3*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL6x3*KR6x3*yNL6x6)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL6x3*KR6x3*yNL6x6)/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1015 = Coupling(name = 'GC_1015',
                   value = '(-2*k1*k2*yML3x3)/((k1 - k2)*(k1 + k2)*vev) + (k1**2*KL3x3*KR3x3*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) + (k2**2*KL3x3*KR3x3*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) + (k1**2*KL6x3*KR6x3*yNL6x6)/((k1 - k2)*(k1 + k2)*vev) + (k2**2*KL6x3*KR6x3*yNL6x6)/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1016 = Coupling(name = 'GC_1016',
                   value = '(2*complex(0,1)*k1*k2*KL3x3*yML3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL3x3**2*KR3x3*yNL3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL3x3**2*KR3x3*yNL3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL3x3*KL6x3*KR6x3*yNL6x6*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL3x3*KL6x3*KR6x3*yNL6x6*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1017 = Coupling(name = 'GC_1017',
                   value = '(2*complex(0,1)*k1*k2*KL6x3*yML3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL3x3*KL6x3*KR3x3*yNL3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL3x3*KL6x3*KR3x3*yNL3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL6x3**2*KR6x3*yNL6x6*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL6x3**2*KR6x3*yNL6x6*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1018 = Coupling(name = 'GC_1018',
                   value = '(complex(0,1)*k1**2*KR3x3*yML3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*KR3x3*yML3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k1*k2*KL3x3*KR3x3**2*yNL3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k1*k2*KL6x3*KR3x3*KR6x3*yNL6x6*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1019 = Coupling(name = 'GC_1019',
                   value = '(-2*cth1*complex(0,1)*KL3x3**2*KR3x3**2*yNL3x3)/vev - (2*cth1*complex(0,1)*KL3x3*KL6x3*KR3x3*KR6x3*yNL6x6)/vev',
                   order = {'EWL':1})

GC_1020 = Coupling(name = 'GC_1020',
                   value = '(2*k1**2*KL3x3*KR3x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) + (2*k2**2*KL3x3*KR3x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (4*k1*k2*KL3x3**2*KR3x3**2*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) - (4*k1*k2*KL3x3*KL6x3*KR3x3*KR6x3*yNL6x6)/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1021 = Coupling(name = 'GC_1021',
                   value = '(-2*complex(0,1)*k1**2*KL3x3*KR3x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k2**2*KL3x3*KR3x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) + (4*complex(0,1)*k1*k2*KL3x3**2*KR3x3**2*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) + (4*complex(0,1)*k1*k2*KL3x3*KL6x3*KR3x3*KR6x3*yNL6x6)/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1022 = Coupling(name = 'GC_1022',
                   value = '(-2*k1**2*KL3x3*KR3x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (2*k2**2*KL3x3*KR3x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) + (4*k1*k2*KL3x3**2*KR3x3**2*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) + (4*k1*k2*KL3x3*KL6x3*KR3x3*KR6x3*yNL6x6)/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1023 = Coupling(name = 'GC_1023',
                   value = '(complex(0,1)*k1**2*KR6x3*yML3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*KR6x3*yML3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k1*k2*KL3x3*KR3x3*KR6x3*yNL3x3*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k1*k2*KL6x3*KR6x3**2*yNL6x6*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1024 = Coupling(name = 'GC_1024',
                   value = '-((cth1*complex(0,1)*KL3x3*KL6x3*KR3x3**2*yNL3x3)/vev) - (cth1*complex(0,1)*KL3x3**2*KR3x3*KR6x3*yNL3x3)/vev - (cth1*complex(0,1)*KL6x3**2*KR3x3*KR6x3*yNL6x6)/vev - (cth1*complex(0,1)*KL3x3*KL6x3*KR6x3**2*yNL6x6)/vev',
                   order = {'EWL':1})

GC_1025 = Coupling(name = 'GC_1025',
                   value = '(k1**2*KL6x3*KR3x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) + (k2**2*KL6x3*KR3x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) + (k1**2*KL3x3*KR6x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) + (k2**2*KL3x3*KR6x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (2*k1*k2*KL3x3*KL6x3*KR3x3**2*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) - (2*k1*k2*KL3x3**2*KR3x3*KR6x3*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) - (2*k1*k2*KL6x3**2*KR3x3*KR6x3*yNL6x6)/((k1 - k2)*(k1 + k2)*vev) - (2*k1*k2*KL3x3*KL6x3*KR6x3**2*yNL6x6)/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1026 = Coupling(name = 'GC_1026',
                   value = '-((complex(0,1)*k1**2*KL6x3*KR3x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev)) - (complex(0,1)*k2**2*KL6x3*KR3x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*KL3x3*KR6x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*KL3x3*KR6x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) + (2*complex(0,1)*k1*k2*KL3x3*KL6x3*KR3x3**2*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) + (2*complex(0,1)*k1*k2*KL3x3**2*KR3x3*KR6x3*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) + (2*complex(0,1)*k1*k2*KL6x3**2*KR3x3*KR6x3*yNL6x6)/((k1 - k2)*(k1 + k2)*vev) + (2*complex(0,1)*k1*k2*KL3x3*KL6x3*KR6x3**2*yNL6x6)/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1027 = Coupling(name = 'GC_1027',
                   value = '-((k1**2*KL6x3*KR3x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev)) - (k2**2*KL6x3*KR3x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (k1**2*KL3x3*KR6x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (k2**2*KL3x3*KR6x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) + (2*k1*k2*KL3x3*KL6x3*KR3x3**2*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) + (2*k1*k2*KL3x3**2*KR3x3*KR6x3*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) + (2*k1*k2*KL6x3**2*KR3x3*KR6x3*yNL6x6)/((k1 - k2)*(k1 + k2)*vev) + (2*k1*k2*KL3x3*KL6x3*KR6x3**2*yNL6x6)/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1028 = Coupling(name = 'GC_1028',
                   value = '(-2*cth1*complex(0,1)*KL3x3*KL6x3*KR3x3*KR6x3*yNL3x3)/vev - (2*cth1*complex(0,1)*KL6x3**2*KR6x3**2*yNL6x6)/vev',
                   order = {'EWL':1})

GC_1029 = Coupling(name = 'GC_1029',
                   value = '(2*k1**2*KL6x3*KR6x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) + (2*k2**2*KL6x3*KR6x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (4*k1*k2*KL3x3*KL6x3*KR3x3*KR6x3*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) - (4*k1*k2*KL6x3**2*KR6x3**2*yNL6x6)/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1030 = Coupling(name = 'GC_1030',
                   value = '(-2*complex(0,1)*k1**2*KL6x3*KR6x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (2*complex(0,1)*k2**2*KL6x3*KR6x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) + (4*complex(0,1)*k1*k2*KL3x3*KL6x3*KR3x3*KR6x3*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) + (4*complex(0,1)*k1*k2*KL6x3**2*KR6x3**2*yNL6x6)/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1031 = Coupling(name = 'GC_1031',
                   value = '(-2*k1**2*KL6x3*KR6x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) - (2*k2**2*KL6x3*KR6x3*yML3x3)/((k1 - k2)*(k1 + k2)*vev) + (4*k1*k2*KL3x3*KL6x3*KR3x3*KR6x3*yNL3x3)/((k1 - k2)*(k1 + k2)*vev) + (4*k1*k2*KL6x3**2*KR6x3**2*yNL6x6)/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1032 = Coupling(name = 'GC_1032',
                   value = '(-2*complex(0,1)*KL3x3**2*KR3x3**2*sth1*yNL3x3)/vev - (2*complex(0,1)*KL3x3*KL6x3*KR3x3*KR6x3*sth1*yNL6x6)/vev',
                   order = {'EWL':1})

GC_1033 = Coupling(name = 'GC_1033',
                   value = '-((complex(0,1)*KL3x3*KL6x3*KR3x3**2*sth1*yNL3x3)/vev) - (complex(0,1)*KL3x3**2*KR3x3*KR6x3*sth1*yNL3x3)/vev - (complex(0,1)*KL6x3**2*KR3x3*KR6x3*sth1*yNL6x6)/vev - (complex(0,1)*KL3x3*KL6x3*KR6x3**2*sth1*yNL6x6)/vev',
                   order = {'EWL':1})

GC_1034 = Coupling(name = 'GC_1034',
                   value = '(-2*complex(0,1)*KL3x3*KL6x3*KR3x3*KR6x3*sth1*yNL3x3)/vev - (2*complex(0,1)*KL6x3**2*KR6x3**2*sth1*yNL6x6)/vev',
                   order = {'EWL':1})

GC_1035 = Coupling(name = 'GC_1035',
                   value = '(complex(0,1)*KR3x3**2*yNL3x3*cmath.sqrt(2))/vR + (complex(0,1)*KR6x3**2*yNL6x6*cmath.sqrt(2))/vR',
                   order = {'EWR':1})

GC_1036 = Coupling(name = 'GC_1036',
                   value = '-((cth1*complex(0,1)*KR3x3**4*yNL3x3)/vR) - (cth1*complex(0,1)*KR3x3**2*KR6x3**2*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1037 = Coupling(name = 'GC_1037',
                   value = '-((cth1*complex(0,1)*KR3x3**3*KR6x3*yNL3x3)/vR) - (cth1*complex(0,1)*KR3x3*KR6x3**3*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1038 = Coupling(name = 'GC_1038',
                   value = '-((cth1*complex(0,1)*KR3x3**2*KR6x3**2*yNL3x3)/vR) - (cth1*complex(0,1)*KR6x3**4*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1039 = Coupling(name = 'GC_1039',
                   value = '(complex(0,1)*KR3x3**4*sth1*yNL3x3)/vR + (complex(0,1)*KR3x3**2*KR6x3**2*sth1*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1040 = Coupling(name = 'GC_1040',
                   value = '(complex(0,1)*KR3x3**3*KR6x3*sth1*yNL3x3)/vR + (complex(0,1)*KR3x3*KR6x3**3*sth1*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1041 = Coupling(name = 'GC_1041',
                   value = '(complex(0,1)*KR3x3**2*KR6x3**2*sth1*yNL3x3)/vR + (complex(0,1)*KR6x3**4*sth1*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1042 = Coupling(name = 'GC_1042',
                   value = '(complex(0,1)*KR3x3**2*Wl3x3**2*yNL3x3*cmath.sqrt(2))/vR + (complex(0,1)*KR6x3**2*Wl3x3**2*yNL6x6*cmath.sqrt(2))/vR',
                   order = {'EWR':1})

GC_1043 = Coupling(name = 'GC_1043',
                   value = '(complex(0,1)*KL3x3*KR3x3**2*Wl3x3**2*yNL3x3)/vR + (complex(0,1)*KL3x3*KR6x3**2*Wl3x3**2*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1044 = Coupling(name = 'GC_1044',
                   value = '-((complex(0,1)*KL3x3**2*KR3x3**2*Wl3x3**2*yNL3x3)/vR) - (complex(0,1)*KL3x3**2*KR6x3**2*Wl3x3**2*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1045 = Coupling(name = 'GC_1045',
                   value = '(KL3x3**2*KR3x3**2*Wl3x3**2*yNL3x3)/vR + (KL3x3**2*KR6x3**2*Wl3x3**2*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1046 = Coupling(name = 'GC_1046',
                   value = '(complex(0,1)*KL6x3*KR3x3**2*Wl3x3**2*yNL3x3)/vR + (complex(0,1)*KL6x3*KR6x3**2*Wl3x3**2*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1047 = Coupling(name = 'GC_1047',
                   value = '-((complex(0,1)*KL3x3*KL6x3*KR3x3**2*Wl3x3**2*yNL3x3)/vR) - (complex(0,1)*KL3x3*KL6x3*KR6x3**2*Wl3x3**2*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1048 = Coupling(name = 'GC_1048',
                   value = '(KL3x3*KL6x3*KR3x3**2*Wl3x3**2*yNL3x3)/vR + (KL3x3*KL6x3*KR6x3**2*Wl3x3**2*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1049 = Coupling(name = 'GC_1049',
                   value = '-((complex(0,1)*KL6x3**2*KR3x3**2*Wl3x3**2*yNL3x3)/vR) - (complex(0,1)*KL6x3**2*KR6x3**2*Wl3x3**2*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1050 = Coupling(name = 'GC_1050',
                   value = '(KL6x3**2*KR3x3**2*Wl3x3**2*yNL3x3)/vR + (KL6x3**2*KR6x3**2*Wl3x3**2*yNL6x6)/vR',
                   order = {'EWR':1})

GC_1051 = Coupling(name = 'GC_1051',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKML1x1))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1052 = Coupling(name = 'GC_1052',
                   value = '(complex(0,1)*gw*sxi*complexconjugate(CKML1x1))/cmath.sqrt(2)',
                   order = {'EWR':1})

GC_1053 = Coupling(name = 'GC_1053',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKML1x2))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1054 = Coupling(name = 'GC_1054',
                   value = '(complex(0,1)*gw*sxi*complexconjugate(CKML1x2))/cmath.sqrt(2)',
                   order = {'EWR':1})

GC_1055 = Coupling(name = 'GC_1055',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKML1x3))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1056 = Coupling(name = 'GC_1056',
                   value = '(complex(0,1)*gw*sxi*complexconjugate(CKML1x3))/cmath.sqrt(2)',
                   order = {'EWR':1})

GC_1057 = Coupling(name = 'GC_1057',
                   value = '-((CKMR1x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR1x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1058 = Coupling(name = 'GC_1058',
                   value = '(CKMR1x1*k1**2*yDO1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x1*k2**2*yDO1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x2*k1**2*yDO2x2*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x2*k2**2*yDO2x2*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x3*k1**2*yDO3x3*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x3*k2**2*yDO3x3*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1059 = Coupling(name = 'GC_1059',
                   value = '-((CKMR2x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR2x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1060 = Coupling(name = 'GC_1060',
                   value = '(CKMR2x1*k1**2*yDO1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x1*k2**2*yDO1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*k1**2*yDO2x2*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*k2**2*yDO2x2*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*k1**2*yDO3x3*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*k2**2*yDO3x3*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1061 = Coupling(name = 'GC_1061',
                   value = '-((CKMR3x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR3x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1062 = Coupling(name = 'GC_1062',
                   value = '(CKMR3x1*k1**2*yDO1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x1*k2**2*yDO1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*k1**2*yDO2x2*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*k2**2*yDO2x2*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*k1**2*yDO3x3*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*k2**2*yDO3x3*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1063 = Coupling(name = 'GC_1063',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKML2x1))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1064 = Coupling(name = 'GC_1064',
                   value = '(complex(0,1)*gw*sxi*complexconjugate(CKML2x1))/cmath.sqrt(2)',
                   order = {'EWR':1})

GC_1065 = Coupling(name = 'GC_1065',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKML2x2))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1066 = Coupling(name = 'GC_1066',
                   value = '(complex(0,1)*gw*sxi*complexconjugate(CKML2x2))/cmath.sqrt(2)',
                   order = {'EWR':1})

GC_1067 = Coupling(name = 'GC_1067',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKML2x3))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1068 = Coupling(name = 'GC_1068',
                   value = '(complex(0,1)*gw*sxi*complexconjugate(CKML2x3))/cmath.sqrt(2)',
                   order = {'EWR':1})

GC_1069 = Coupling(name = 'GC_1069',
                   value = '-((CKMR1x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR1x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1070 = Coupling(name = 'GC_1070',
                   value = '(CKMR1x1*k1**2*yDO1x1*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x1*k2**2*yDO1x1*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x2*k1**2*yDO2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x2*k2**2*yDO2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x3*k1**2*yDO3x3*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x3*k2**2*yDO3x3*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1071 = Coupling(name = 'GC_1071',
                   value = '-((CKMR2x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR2x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1072 = Coupling(name = 'GC_1072',
                   value = '(CKMR2x1*k1**2*yDO1x1*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x1*k2**2*yDO1x1*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*k1**2*yDO2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*k2**2*yDO2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*k1**2*yDO3x3*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*k2**2*yDO3x3*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1073 = Coupling(name = 'GC_1073',
                   value = '-((CKMR3x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR3x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1074 = Coupling(name = 'GC_1074',
                   value = '(CKMR3x1*k1**2*yDO1x1*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x1*k2**2*yDO1x1*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*k1**2*yDO2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*k2**2*yDO2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*k1**2*yDO3x3*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*k2**2*yDO3x3*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1075 = Coupling(name = 'GC_1075',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKML3x1))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1076 = Coupling(name = 'GC_1076',
                   value = '(complex(0,1)*gw*sxi*complexconjugate(CKML3x1))/cmath.sqrt(2)',
                   order = {'EWR':1})

GC_1077 = Coupling(name = 'GC_1077',
                   value = '-((CKMR1x1*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR1x1*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x1*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x1*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x1*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x1*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1078 = Coupling(name = 'GC_1078',
                   value = '(CKMR1x1*k1**2*yMU1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x1*k2**2*yMU1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x1*k1**2*yMU2x2*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x1*k2**2*yMU2x2*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x1*k1**2*yMU3x3*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x1*k2**2*yMU3x3*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1079 = Coupling(name = 'GC_1079',
                   value = '-((CKMR1x2*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR1x2*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1080 = Coupling(name = 'GC_1080',
                   value = '(CKMR1x2*k1**2*yMU1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x2*k2**2*yMU1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*k1**2*yMU2x2*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*k2**2*yMU2x2*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*k1**2*yMU3x3*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*k2**2*yMU3x3*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1081 = Coupling(name = 'GC_1081',
                   value = '-((CKMR1x3*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR1x3*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1082 = Coupling(name = 'GC_1082',
                   value = '(CKMR1x3*k1**2*yMU1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x3*k2**2*yMU1x1*complexconjugate(CKML1x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*k1**2*yMU2x2*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*k2**2*yMU2x2*complexconjugate(CKML2x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*k1**2*yMU3x3*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*k2**2*yMU3x3*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1083 = Coupling(name = 'GC_1083',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKML3x2))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1084 = Coupling(name = 'GC_1084',
                   value = '(complex(0,1)*gw*sxi*complexconjugate(CKML3x2))/cmath.sqrt(2)',
                   order = {'EWR':1})

GC_1085 = Coupling(name = 'GC_1085',
                   value = '-((CKMR1x1*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR1x1*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x1*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x1*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x1*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x1*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1086 = Coupling(name = 'GC_1086',
                   value = '(CKMR1x1*k1**2*yMU1x1*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x1*k2**2*yMU1x1*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x1*k1**2*yMU2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x1*k2**2*yMU2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x1*k1**2*yMU3x3*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x1*k2**2*yMU3x3*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1087 = Coupling(name = 'GC_1087',
                   value = '-((CKMR1x2*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR1x2*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1088 = Coupling(name = 'GC_1088',
                   value = '(CKMR1x2*k1**2*yMU1x1*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x2*k2**2*yMU1x1*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*k1**2*yMU2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*k2**2*yMU2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*k1**2*yMU3x3*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*k2**2*yMU3x3*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1089 = Coupling(name = 'GC_1089',
                   value = '-((CKMR1x3*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR1x3*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1090 = Coupling(name = 'GC_1090',
                   value = '(CKMR1x3*k1**2*yMU1x1*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x3*k2**2*yMU1x1*complexconjugate(CKML1x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*k1**2*yMU2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*k2**2*yMU2x2*complexconjugate(CKML2x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*k1**2*yMU3x3*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*k2**2*yMU3x3*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1091 = Coupling(name = 'GC_1091',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKML3x3))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1092 = Coupling(name = 'GC_1092',
                   value = '(complex(0,1)*gw*sxi*complexconjugate(CKML3x3))/cmath.sqrt(2)',
                   order = {'EWR':1})

GC_1093 = Coupling(name = 'GC_1093',
                   value = '-((CKMR1x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR1x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR1x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1094 = Coupling(name = 'GC_1094',
                   value = '(CKMR1x1*k1**2*yDO1x1*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x1*k2**2*yDO1x1*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x2*k1**2*yDO2x2*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x2*k2**2*yDO2x2*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x3*k1**2*yDO3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x3*k2**2*yDO3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1095 = Coupling(name = 'GC_1095',
                   value = '-((CKMR2x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR2x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1096 = Coupling(name = 'GC_1096',
                   value = '(CKMR2x1*k1**2*yDO1x1*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x1*k2**2*yDO1x1*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*k1**2*yDO2x2*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*k2**2*yDO2x2*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*k1**2*yDO3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*k2**2*yDO3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1097 = Coupling(name = 'GC_1097',
                   value = '-((CKMR3x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR3x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1098 = Coupling(name = 'GC_1098',
                   value = '(CKMR3x1*k1**2*yDO1x1*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x1*k2**2*yDO1x1*complexconjugate(CKML3x1))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*k1**2*yDO2x2*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*k2**2*yDO2x2*complexconjugate(CKML3x2))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*k1**2*yDO3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*k2**2*yDO3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1099 = Coupling(name = 'GC_1099',
                   value = '-((CKMR1x1*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR1x1*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x1*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x1*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x1*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x1*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1100 = Coupling(name = 'GC_1100',
                   value = '(CKMR1x1*k1**2*yMU1x1*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x1*k2**2*yMU1x1*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x1*k1**2*yMU2x2*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x1*k2**2*yMU2x2*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x1*k1**2*yMU3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x1*k2**2*yMU3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1101 = Coupling(name = 'GC_1101',
                   value = '-((CKMR1x2*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR1x2*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x2*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x2*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1102 = Coupling(name = 'GC_1102',
                   value = '(CKMR1x2*k1**2*yMU1x1*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x2*k2**2*yMU1x1*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*k1**2*yMU2x2*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x2*k2**2*yMU2x2*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*k1**2*yMU3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x2*k2**2*yMU3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1103 = Coupling(name = 'GC_1103',
                   value = '-((CKMR1x3*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev)) - (CKMR1x3*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR2x3*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKMR3x3*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1104 = Coupling(name = 'GC_1104',
                   value = '(CKMR1x3*k1**2*yMU1x1*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR1x3*k2**2*yMU1x1*complexconjugate(CKML1x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*k1**2*yMU2x2*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR2x3*k2**2*yMU2x2*complexconjugate(CKML2x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*k1**2*yMU3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev) + (CKMR3x3*k2**2*yMU3x3*complexconjugate(CKML3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1105 = Coupling(name = 'GC_1105',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKMR1x1))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1106 = Coupling(name = 'GC_1106',
                   value = '-((complex(0,1)*gw*sxi*complexconjugate(CKMR1x1))/cmath.sqrt(2))',
                   order = {'EWR':1})

GC_1107 = Coupling(name = 'GC_1107',
                   value = '(-2*complex(0,1)*k1*k2*yMU1x1*complexconjugate(CKML1x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k1**2*yDO1x1*complexconjugate(CKMR1x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*yDO1x1*complexconjugate(CKMR1x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1108 = Coupling(name = 'GC_1108',
                   value = '(2*complex(0,1)*k1*k2*yDO1x1*complexconjugate(CKML1x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*yMU1x1*complexconjugate(CKMR1x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*yMU1x1*complexconjugate(CKMR1x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1109 = Coupling(name = 'GC_1109',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKMR1x2))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1110 = Coupling(name = 'GC_1110',
                   value = '-((complex(0,1)*gw*sxi*complexconjugate(CKMR1x2))/cmath.sqrt(2))',
                   order = {'EWR':1})

GC_1111 = Coupling(name = 'GC_1111',
                   value = '(-2*complex(0,1)*k1*k2*yMU1x1*complexconjugate(CKML1x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k1**2*yDO2x2*complexconjugate(CKMR1x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*yDO2x2*complexconjugate(CKMR1x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1112 = Coupling(name = 'GC_1112',
                   value = '(2*complex(0,1)*k1*k2*yDO2x2*complexconjugate(CKML1x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*yMU1x1*complexconjugate(CKMR1x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*yMU1x1*complexconjugate(CKMR1x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1113 = Coupling(name = 'GC_1113',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKMR1x3))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1114 = Coupling(name = 'GC_1114',
                   value = '-((complex(0,1)*gw*sxi*complexconjugate(CKMR1x3))/cmath.sqrt(2))',
                   order = {'EWR':1})

GC_1115 = Coupling(name = 'GC_1115',
                   value = '(-2*complex(0,1)*k1*k2*yMU1x1*complexconjugate(CKML1x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k1**2*yDO3x3*complexconjugate(CKMR1x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*yDO3x3*complexconjugate(CKMR1x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1116 = Coupling(name = 'GC_1116',
                   value = '-((CKML1x1*k1**2*yDO1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x1*k2**2*yDO1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x2*k1**2*yDO2x2*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x2*k2**2*yDO2x2*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x3*k1**2*yDO3x3*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x3*k2**2*yDO3x3*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1117 = Coupling(name = 'GC_1117',
                   value = '-((CKML1x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1118 = Coupling(name = 'GC_1118',
                   value = '-((CKML2x1*k1**2*yDO1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML2x1*k2**2*yDO1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*k1**2*yDO2x2*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*k2**2*yDO2x2*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*k1**2*yDO3x3*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*k2**2*yDO3x3*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1119 = Coupling(name = 'GC_1119',
                   value = '-((CKML2x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML2x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1120 = Coupling(name = 'GC_1120',
                   value = '-((CKML3x1*k1**2*yDO1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML3x1*k2**2*yDO1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*k1**2*yDO2x2*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*k2**2*yDO2x2*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*k1**2*yDO3x3*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*k2**2*yDO3x3*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1121 = Coupling(name = 'GC_1121',
                   value = '-((CKML3x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML3x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1122 = Coupling(name = 'GC_1122',
                   value = '(2*complex(0,1)*k1*k2*yDO3x3*complexconjugate(CKML1x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*yMU1x1*complexconjugate(CKMR1x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*yMU1x1*complexconjugate(CKMR1x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1123 = Coupling(name = 'GC_1123',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKMR2x1))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1124 = Coupling(name = 'GC_1124',
                   value = '-((complex(0,1)*gw*sxi*complexconjugate(CKMR2x1))/cmath.sqrt(2))',
                   order = {'EWR':1})

GC_1125 = Coupling(name = 'GC_1125',
                   value = '(-2*complex(0,1)*k1*k2*yMU2x2*complexconjugate(CKML2x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k1**2*yDO1x1*complexconjugate(CKMR2x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*yDO1x1*complexconjugate(CKMR2x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1126 = Coupling(name = 'GC_1126',
                   value = '(2*complex(0,1)*k1*k2*yDO1x1*complexconjugate(CKML2x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*yMU2x2*complexconjugate(CKMR2x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*yMU2x2*complexconjugate(CKMR2x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1127 = Coupling(name = 'GC_1127',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKMR2x2))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1128 = Coupling(name = 'GC_1128',
                   value = '-((complex(0,1)*gw*sxi*complexconjugate(CKMR2x2))/cmath.sqrt(2))',
                   order = {'EWR':1})

GC_1129 = Coupling(name = 'GC_1129',
                   value = '(-2*complex(0,1)*k1*k2*yMU2x2*complexconjugate(CKML2x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k1**2*yDO2x2*complexconjugate(CKMR2x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*yDO2x2*complexconjugate(CKMR2x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1130 = Coupling(name = 'GC_1130',
                   value = '(2*complex(0,1)*k1*k2*yDO2x2*complexconjugate(CKML2x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*yMU2x2*complexconjugate(CKMR2x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*yMU2x2*complexconjugate(CKMR2x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1131 = Coupling(name = 'GC_1131',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKMR2x3))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1132 = Coupling(name = 'GC_1132',
                   value = '-((complex(0,1)*gw*sxi*complexconjugate(CKMR2x3))/cmath.sqrt(2))',
                   order = {'EWR':1})

GC_1133 = Coupling(name = 'GC_1133',
                   value = '(-2*complex(0,1)*k1*k2*yMU2x2*complexconjugate(CKML2x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k1**2*yDO3x3*complexconjugate(CKMR2x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*yDO3x3*complexconjugate(CKMR2x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1134 = Coupling(name = 'GC_1134',
                   value = '-((CKML1x1*k1**2*yDO1x1*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x1*k2**2*yDO1x1*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x2*k1**2*yDO2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x2*k2**2*yDO2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x3*k1**2*yDO3x3*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x3*k2**2*yDO3x3*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1135 = Coupling(name = 'GC_1135',
                   value = '-((CKML1x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1136 = Coupling(name = 'GC_1136',
                   value = '-((CKML2x1*k1**2*yDO1x1*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML2x1*k2**2*yDO1x1*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*k1**2*yDO2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*k2**2*yDO2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*k1**2*yDO3x3*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*k2**2*yDO3x3*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1137 = Coupling(name = 'GC_1137',
                   value = '-((CKML2x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML2x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1138 = Coupling(name = 'GC_1138',
                   value = '-((CKML3x1*k1**2*yDO1x1*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML3x1*k2**2*yDO1x1*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*k1**2*yDO2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*k2**2*yDO2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*k1**2*yDO3x3*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*k2**2*yDO3x3*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1139 = Coupling(name = 'GC_1139',
                   value = '-((CKML3x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML3x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1140 = Coupling(name = 'GC_1140',
                   value = '(2*complex(0,1)*k1*k2*yDO3x3*complexconjugate(CKML2x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*yMU2x2*complexconjugate(CKMR2x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*yMU2x2*complexconjugate(CKMR2x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1141 = Coupling(name = 'GC_1141',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKMR3x1))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1142 = Coupling(name = 'GC_1142',
                   value = '-((complex(0,1)*gw*sxi*complexconjugate(CKMR3x1))/cmath.sqrt(2))',
                   order = {'EWR':1})

GC_1143 = Coupling(name = 'GC_1143',
                   value = '(-2*complex(0,1)*k1*k2*yMU3x3*complexconjugate(CKML3x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k1**2*yDO1x1*complexconjugate(CKMR3x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*yDO1x1*complexconjugate(CKMR3x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1144 = Coupling(name = 'GC_1144',
                   value = '(2*complex(0,1)*k1*k2*yDO1x1*complexconjugate(CKML3x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*yMU3x3*complexconjugate(CKMR3x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*yMU3x3*complexconjugate(CKMR3x1)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1145 = Coupling(name = 'GC_1145',
                   value = '-((CKML1x1*k1**2*yMU1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x1*k2**2*yMU1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x1*k1**2*yMU2x2*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x1*k2**2*yMU2x2*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x1*k1**2*yMU3x3*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x1*k2**2*yMU3x3*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1146 = Coupling(name = 'GC_1146',
                   value = '-((CKML1x1*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x1*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x1*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x1*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x1*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x1*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1147 = Coupling(name = 'GC_1147',
                   value = '-((CKML1x2*k1**2*yMU1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x2*k2**2*yMU1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*k1**2*yMU2x2*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*k2**2*yMU2x2*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*k1**2*yMU3x3*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*k2**2*yMU3x3*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1148 = Coupling(name = 'GC_1148',
                   value = '-((CKML1x2*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x2*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1149 = Coupling(name = 'GC_1149',
                   value = '-((CKML1x3*k1**2*yMU1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x3*k2**2*yMU1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*k1**2*yMU2x2*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*k2**2*yMU2x2*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*k1**2*yMU3x3*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*k2**2*yMU3x3*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1150 = Coupling(name = 'GC_1150',
                   value = '-((CKML1x3*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x3*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKMR1x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKMR2x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1151 = Coupling(name = 'GC_1151',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKMR3x2))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1152 = Coupling(name = 'GC_1152',
                   value = '-((complex(0,1)*gw*sxi*complexconjugate(CKMR3x2))/cmath.sqrt(2))',
                   order = {'EWR':1})

GC_1153 = Coupling(name = 'GC_1153',
                   value = '(-2*complex(0,1)*k1*k2*yMU3x3*complexconjugate(CKML3x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k1**2*yDO2x2*complexconjugate(CKMR3x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*yDO2x2*complexconjugate(CKMR3x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1154 = Coupling(name = 'GC_1154',
                   value = '(2*complex(0,1)*k1*k2*yDO2x2*complexconjugate(CKML3x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*yMU3x3*complexconjugate(CKMR3x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*yMU3x3*complexconjugate(CKMR3x2)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1155 = Coupling(name = 'GC_1155',
                   value = '-((CKML1x1*k1**2*yMU1x1*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x1*k2**2*yMU1x1*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x1*k1**2*yMU2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x1*k2**2*yMU2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x1*k1**2*yMU3x3*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x1*k2**2*yMU3x3*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1156 = Coupling(name = 'GC_1156',
                   value = '-((CKML1x1*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x1*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x1*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x1*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x1*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x1*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1157 = Coupling(name = 'GC_1157',
                   value = '-((CKML1x2*k1**2*yMU1x1*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x2*k2**2*yMU1x1*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*k1**2*yMU2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*k2**2*yMU2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*k1**2*yMU3x3*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*k2**2*yMU3x3*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1158 = Coupling(name = 'GC_1158',
                   value = '-((CKML1x2*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x2*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1159 = Coupling(name = 'GC_1159',
                   value = '-((CKML1x3*k1**2*yMU1x1*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x3*k2**2*yMU1x1*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*k1**2*yMU2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*k2**2*yMU2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*k1**2*yMU3x3*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*k2**2*yMU3x3*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1160 = Coupling(name = 'GC_1160',
                   value = '-((CKML1x3*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x3*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKMR1x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKMR2x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1161 = Coupling(name = 'GC_1161',
                   value = '(cxi*complex(0,1)*gw*complexconjugate(CKMR3x3))/cmath.sqrt(2)',
                   order = {'EWL':1})

GC_1162 = Coupling(name = 'GC_1162',
                   value = '-((complex(0,1)*gw*sxi*complexconjugate(CKMR3x3))/cmath.sqrt(2))',
                   order = {'EWR':1})

GC_1163 = Coupling(name = 'GC_1163',
                   value = '(-2*complex(0,1)*k1*k2*yMU3x3*complexconjugate(CKML3x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k1**2*yDO3x3*complexconjugate(CKMR3x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) + (complex(0,1)*k2**2*yDO3x3*complexconjugate(CKMR3x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1164 = Coupling(name = 'GC_1164',
                   value = '-((CKML1x1*k1**2*yDO1x1*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x1*k2**2*yDO1x1*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x2*k1**2*yDO2x2*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x2*k2**2*yDO2x2*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x3*k1**2*yDO3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x3*k2**2*yDO3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1165 = Coupling(name = 'GC_1165',
                   value = '-((CKML1x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML1x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1166 = Coupling(name = 'GC_1166',
                   value = '-((CKML2x1*k1**2*yDO1x1*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML2x1*k2**2*yDO1x1*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*k1**2*yDO2x2*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*k2**2*yDO2x2*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*k1**2*yDO3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*k2**2*yDO3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1167 = Coupling(name = 'GC_1167',
                   value = '-((CKML2x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML2x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1168 = Coupling(name = 'GC_1168',
                   value = '-((CKML3x1*k1**2*yDO1x1*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML3x1*k2**2*yDO1x1*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*k1**2*yDO2x2*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*k2**2*yDO2x2*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*k1**2*yDO3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*k2**2*yDO3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1169 = Coupling(name = 'GC_1169',
                   value = '-((CKML3x1*complex(0,1)*k1**2*yDO1x1*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev)) - (CKML3x1*complex(0,1)*k2**2*yDO1x1*complexconjugate(CKMR3x1))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*complex(0,1)*k1**2*yDO2x2*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*complex(0,1)*k2**2*yDO2x2*complexconjugate(CKMR3x2))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*complex(0,1)*k1**2*yDO3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*complex(0,1)*k2**2*yDO3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1170 = Coupling(name = 'GC_1170',
                   value = '(2*complex(0,1)*k1*k2*yDO3x3*complexconjugate(CKML3x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k1**2*yMU3x3*complexconjugate(CKMR3x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev) - (complex(0,1)*k2**2*yMU3x3*complexconjugate(CKMR3x3)*cmath.sqrt(2))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1171 = Coupling(name = 'GC_1171',
                   value = '-((CKML1x1*k1**2*yMU1x1*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x1*k2**2*yMU1x1*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x1*k1**2*yMU2x2*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x1*k2**2*yMU2x2*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x1*k1**2*yMU3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x1*k2**2*yMU3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1172 = Coupling(name = 'GC_1172',
                   value = '-((CKML1x1*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x1*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x1*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x1*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x1*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x1*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1173 = Coupling(name = 'GC_1173',
                   value = '-((CKML1x2*k1**2*yMU1x1*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x2*k2**2*yMU1x1*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*k1**2*yMU2x2*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*k2**2*yMU2x2*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*k1**2*yMU3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*k2**2*yMU3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1174 = Coupling(name = 'GC_1174',
                   value = '-((CKML1x2*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x2*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x2*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x2*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1175 = Coupling(name = 'GC_1175',
                   value = '-((CKML1x3*k1**2*yMU1x1*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x3*k2**2*yMU1x1*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*k1**2*yMU2x2*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*k2**2*yMU2x2*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*k1**2*yMU3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*k2**2*yMU3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

GC_1176 = Coupling(name = 'GC_1176',
                   value = '-((CKML1x3*complex(0,1)*k1**2*yMU1x1*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev)) - (CKML1x3*complex(0,1)*k2**2*yMU1x1*complexconjugate(CKMR1x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*complex(0,1)*k1**2*yMU2x2*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML2x3*complex(0,1)*k2**2*yMU2x2*complexconjugate(CKMR2x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*complex(0,1)*k1**2*yMU3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev) - (CKML3x3*complex(0,1)*k2**2*yMU3x3*complexconjugate(CKMR3x3))/((k1 - k2)*(k1 + k2)*vev)',
                   order = {'EWL':1})

