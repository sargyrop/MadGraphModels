Requestor: Deepak Kar
Content: Left-right symmetric model for boosted lepton-jet final states
Paper: https://arxiv.org/abs/1607.03504
JIRA: https://its.cern.ch/jira/browse/AGENE-1208
