Requestor: Antonio Sidoti
Content: Lepton number violating SM Higgs decay into lepton pair and jets
Paper: https://link.springer.com/article/10.1007/JHEP04(2017)114
Webpage: https://sites.google.com/site/leftrighthep/1-lrsm-feynrules?authuser=0
JIRA: https://its.cern.ch/jira/browse/AGENE-2258
