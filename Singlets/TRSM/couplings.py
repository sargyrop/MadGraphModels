# This file was automatically created by FeynRules 2.3.32
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Wed 25 Sep 2019 11:45:41


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '-G',
                order = {'QCD':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'complex(0,1)*G',
                order = {'QCD':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'complex(0,1)*G**2',
                order = {'QCD':2})

GC_9 = Coupling(name = 'GC_9',
                value = '-(Ah1*complex(0,1)*RR1x1)',
                order = {'HIW':1})

GC_10 = Coupling(name = 'GC_10',
                 value = '-(complex(0,1)*Gh1*RR1x1)',
                 order = {'HIG':1})

GC_11 = Coupling(name = 'GC_11',
                 value = '-(G*Gh1*RR1x1)',
                 order = {'HIG':1,'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'complex(0,1)*G**2*Gh1*RR1x1',
                 order = {'HIG':1,'QCD':2})

GC_13 = Coupling(name = 'GC_13',
                 value = '-6*complex(0,1)*LH*RR1x1**4 - 6*complex(0,1)*LHS*RR1x1**2*RR1x2**2 - 6*complex(0,1)*LS*RR1x2**4 - 6*complex(0,1)*LHX*RR1x1**2*RR1x3**2 - 6*complex(0,1)*LSX*RR1x2**2*RR1x3**2 - 6*complex(0,1)*LX*RR1x3**4',
                 order = {'QED':2})

GC_14 = Coupling(name = 'GC_14',
                 value = '-(Ah2*complex(0,1)*RR2x1)',
                 order = {'HIW':1})

GC_15 = Coupling(name = 'GC_15',
                 value = '-(complex(0,1)*Gh2*RR2x1)',
                 order = {'HIG':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '-(G*Gh2*RR2x1)',
                 order = {'HIG':1,'QCD':1})

GC_17 = Coupling(name = 'GC_17',
                 value = 'complex(0,1)*G**2*Gh2*RR2x1',
                 order = {'HIG':1,'QCD':2})

GC_18 = Coupling(name = 'GC_18',
                 value = '-6*complex(0,1)*LH*RR1x1**3*RR2x1 - 3*complex(0,1)*LHS*RR1x1*RR1x2**2*RR2x1 - 3*complex(0,1)*LHX*RR1x1*RR1x3**2*RR2x1 - 3*complex(0,1)*LHS*RR1x1**2*RR1x2*RR2x2 - 6*complex(0,1)*LS*RR1x2**3*RR2x2 - 3*complex(0,1)*LSX*RR1x2*RR1x3**2*RR2x2 - 3*complex(0,1)*LHX*RR1x1**2*RR1x3*RR2x3 - 3*complex(0,1)*LSX*RR1x2**2*RR1x3*RR2x3 - 6*complex(0,1)*LX*RR1x3**3*RR2x3',
                 order = {'QED':2})

GC_19 = Coupling(name = 'GC_19',
                 value = '-6*complex(0,1)*LH*RR1x1**2*RR2x1**2 - complex(0,1)*LHS*RR1x2**2*RR2x1**2 - complex(0,1)*LHX*RR1x3**2*RR2x1**2 - 4*complex(0,1)*LHS*RR1x1*RR1x2*RR2x1*RR2x2 - complex(0,1)*LHS*RR1x1**2*RR2x2**2 - 6*complex(0,1)*LS*RR1x2**2*RR2x2**2 - complex(0,1)*LSX*RR1x3**2*RR2x2**2 - 4*complex(0,1)*LHX*RR1x1*RR1x3*RR2x1*RR2x3 - 4*complex(0,1)*LSX*RR1x2*RR1x3*RR2x2*RR2x3 - complex(0,1)*LHX*RR1x1**2*RR2x3**2 - complex(0,1)*LSX*RR1x2**2*RR2x3**2 - 6*complex(0,1)*LX*RR1x3**2*RR2x3**2',
                 order = {'QED':2})

GC_20 = Coupling(name = 'GC_20',
                 value = '-6*complex(0,1)*LH*RR1x1*RR2x1**3 - 3*complex(0,1)*LHS*RR1x2*RR2x1**2*RR2x2 - 3*complex(0,1)*LHS*RR1x1*RR2x1*RR2x2**2 - 6*complex(0,1)*LS*RR1x2*RR2x2**3 - 3*complex(0,1)*LHX*RR1x3*RR2x1**2*RR2x3 - 3*complex(0,1)*LSX*RR1x3*RR2x2**2*RR2x3 - 3*complex(0,1)*LHX*RR1x1*RR2x1*RR2x3**2 - 3*complex(0,1)*LSX*RR1x2*RR2x2*RR2x3**2 - 6*complex(0,1)*LX*RR1x3*RR2x3**3',
                 order = {'QED':2})

GC_21 = Coupling(name = 'GC_21',
                 value = '-6*complex(0,1)*LH*RR2x1**4 - 6*complex(0,1)*LHS*RR2x1**2*RR2x2**2 - 6*complex(0,1)*LS*RR2x2**4 - 6*complex(0,1)*LHX*RR2x1**2*RR2x3**2 - 6*complex(0,1)*LSX*RR2x2**2*RR2x3**2 - 6*complex(0,1)*LX*RR2x3**4',
                 order = {'QED':2})

GC_22 = Coupling(name = 'GC_22',
                 value = '-(Ah3*complex(0,1)*RR3x1)',
                 order = {'HIW':1})

GC_23 = Coupling(name = 'GC_23',
                 value = '-(complex(0,1)*Gh3*RR3x1)',
                 order = {'HIG':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '-(G*Gh3*RR3x1)',
                 order = {'HIG':1,'QCD':1})

GC_25 = Coupling(name = 'GC_25',
                 value = 'complex(0,1)*G**2*Gh3*RR3x1',
                 order = {'HIG':1,'QCD':2})

GC_26 = Coupling(name = 'GC_26',
                 value = '-6*complex(0,1)*LH*RR1x1**3*RR3x1 - 3*complex(0,1)*LHS*RR1x1*RR1x2**2*RR3x1 - 3*complex(0,1)*LHX*RR1x1*RR1x3**2*RR3x1 - 3*complex(0,1)*LHS*RR1x1**2*RR1x2*RR3x2 - 6*complex(0,1)*LS*RR1x2**3*RR3x2 - 3*complex(0,1)*LSX*RR1x2*RR1x3**2*RR3x2 - 3*complex(0,1)*LHX*RR1x1**2*RR1x3*RR3x3 - 3*complex(0,1)*LSX*RR1x2**2*RR1x3*RR3x3 - 6*complex(0,1)*LX*RR1x3**3*RR3x3',
                 order = {'QED':2})

GC_27 = Coupling(name = 'GC_27',
                 value = '-6*complex(0,1)*LH*RR1x1**2*RR2x1*RR3x1 - complex(0,1)*LHS*RR1x2**2*RR2x1*RR3x1 - complex(0,1)*LHX*RR1x3**2*RR2x1*RR3x1 - 2*complex(0,1)*LHS*RR1x1*RR1x2*RR2x2*RR3x1 - 2*complex(0,1)*LHX*RR1x1*RR1x3*RR2x3*RR3x1 - 2*complex(0,1)*LHS*RR1x1*RR1x2*RR2x1*RR3x2 - complex(0,1)*LHS*RR1x1**2*RR2x2*RR3x2 - 6*complex(0,1)*LS*RR1x2**2*RR2x2*RR3x2 - complex(0,1)*LSX*RR1x3**2*RR2x2*RR3x2 - 2*complex(0,1)*LSX*RR1x2*RR1x3*RR2x3*RR3x2 - 2*complex(0,1)*LHX*RR1x1*RR1x3*RR2x1*RR3x3 - 2*complex(0,1)*LSX*RR1x2*RR1x3*RR2x2*RR3x3 - complex(0,1)*LHX*RR1x1**2*RR2x3*RR3x3 - complex(0,1)*LSX*RR1x2**2*RR2x3*RR3x3 - 6*complex(0,1)*LX*RR1x3**2*RR2x3*RR3x3',
                 order = {'QED':2})

GC_28 = Coupling(name = 'GC_28',
                 value = '-6*complex(0,1)*LH*RR1x1*RR2x1**2*RR3x1 - 2*complex(0,1)*LHS*RR1x2*RR2x1*RR2x2*RR3x1 - complex(0,1)*LHS*RR1x1*RR2x2**2*RR3x1 - 2*complex(0,1)*LHX*RR1x3*RR2x1*RR2x3*RR3x1 - complex(0,1)*LHX*RR1x1*RR2x3**2*RR3x1 - complex(0,1)*LHS*RR1x2*RR2x1**2*RR3x2 - 2*complex(0,1)*LHS*RR1x1*RR2x1*RR2x2*RR3x2 - 6*complex(0,1)*LS*RR1x2*RR2x2**2*RR3x2 - 2*complex(0,1)*LSX*RR1x3*RR2x2*RR2x3*RR3x2 - complex(0,1)*LSX*RR1x2*RR2x3**2*RR3x2 - complex(0,1)*LHX*RR1x3*RR2x1**2*RR3x3 - complex(0,1)*LSX*RR1x3*RR2x2**2*RR3x3 - 2*complex(0,1)*LHX*RR1x1*RR2x1*RR2x3*RR3x3 - 2*complex(0,1)*LSX*RR1x2*RR2x2*RR2x3*RR3x3 - 6*complex(0,1)*LX*RR1x3*RR2x3**2*RR3x3',
                 order = {'QED':2})

GC_29 = Coupling(name = 'GC_29',
                 value = '-6*complex(0,1)*LH*RR2x1**3*RR3x1 - 3*complex(0,1)*LHS*RR2x1*RR2x2**2*RR3x1 - 3*complex(0,1)*LHX*RR2x1*RR2x3**2*RR3x1 - 3*complex(0,1)*LHS*RR2x1**2*RR2x2*RR3x2 - 6*complex(0,1)*LS*RR2x2**3*RR3x2 - 3*complex(0,1)*LSX*RR2x2*RR2x3**2*RR3x2 - 3*complex(0,1)*LHX*RR2x1**2*RR2x3*RR3x3 - 3*complex(0,1)*LSX*RR2x2**2*RR2x3*RR3x3 - 6*complex(0,1)*LX*RR2x3**3*RR3x3',
                 order = {'QED':2})

GC_30 = Coupling(name = 'GC_30',
                 value = '-6*complex(0,1)*LH*RR1x1**2*RR3x1**2 - complex(0,1)*LHS*RR1x2**2*RR3x1**2 - complex(0,1)*LHX*RR1x3**2*RR3x1**2 - 4*complex(0,1)*LHS*RR1x1*RR1x2*RR3x1*RR3x2 - complex(0,1)*LHS*RR1x1**2*RR3x2**2 - 6*complex(0,1)*LS*RR1x2**2*RR3x2**2 - complex(0,1)*LSX*RR1x3**2*RR3x2**2 - 4*complex(0,1)*LHX*RR1x1*RR1x3*RR3x1*RR3x3 - 4*complex(0,1)*LSX*RR1x2*RR1x3*RR3x2*RR3x3 - complex(0,1)*LHX*RR1x1**2*RR3x3**2 - complex(0,1)*LSX*RR1x2**2*RR3x3**2 - 6*complex(0,1)*LX*RR1x3**2*RR3x3**2',
                 order = {'QED':2})

GC_31 = Coupling(name = 'GC_31',
                 value = '-6*complex(0,1)*LH*RR1x1*RR2x1*RR3x1**2 - complex(0,1)*LHS*RR1x2*RR2x2*RR3x1**2 - complex(0,1)*LHX*RR1x3*RR2x3*RR3x1**2 - 2*complex(0,1)*LHS*RR1x2*RR2x1*RR3x1*RR3x2 - 2*complex(0,1)*LHS*RR1x1*RR2x2*RR3x1*RR3x2 - complex(0,1)*LHS*RR1x1*RR2x1*RR3x2**2 - 6*complex(0,1)*LS*RR1x2*RR2x2*RR3x2**2 - complex(0,1)*LSX*RR1x3*RR2x3*RR3x2**2 - 2*complex(0,1)*LHX*RR1x3*RR2x1*RR3x1*RR3x3 - 2*complex(0,1)*LHX*RR1x1*RR2x3*RR3x1*RR3x3 - 2*complex(0,1)*LSX*RR1x3*RR2x2*RR3x2*RR3x3 - 2*complex(0,1)*LSX*RR1x2*RR2x3*RR3x2*RR3x3 - complex(0,1)*LHX*RR1x1*RR2x1*RR3x3**2 - complex(0,1)*LSX*RR1x2*RR2x2*RR3x3**2 - 6*complex(0,1)*LX*RR1x3*RR2x3*RR3x3**2',
                 order = {'QED':2})

GC_32 = Coupling(name = 'GC_32',
                 value = '-6*complex(0,1)*LH*RR2x1**2*RR3x1**2 - complex(0,1)*LHS*RR2x2**2*RR3x1**2 - complex(0,1)*LHX*RR2x3**2*RR3x1**2 - 4*complex(0,1)*LHS*RR2x1*RR2x2*RR3x1*RR3x2 - complex(0,1)*LHS*RR2x1**2*RR3x2**2 - 6*complex(0,1)*LS*RR2x2**2*RR3x2**2 - complex(0,1)*LSX*RR2x3**2*RR3x2**2 - 4*complex(0,1)*LHX*RR2x1*RR2x3*RR3x1*RR3x3 - 4*complex(0,1)*LSX*RR2x2*RR2x3*RR3x2*RR3x3 - complex(0,1)*LHX*RR2x1**2*RR3x3**2 - complex(0,1)*LSX*RR2x2**2*RR3x3**2 - 6*complex(0,1)*LX*RR2x3**2*RR3x3**2',
                 order = {'QED':2})

GC_33 = Coupling(name = 'GC_33',
                 value = '-6*complex(0,1)*LH*RR1x1*RR3x1**3 - 3*complex(0,1)*LHS*RR1x2*RR3x1**2*RR3x2 - 3*complex(0,1)*LHS*RR1x1*RR3x1*RR3x2**2 - 6*complex(0,1)*LS*RR1x2*RR3x2**3 - 3*complex(0,1)*LHX*RR1x3*RR3x1**2*RR3x3 - 3*complex(0,1)*LSX*RR1x3*RR3x2**2*RR3x3 - 3*complex(0,1)*LHX*RR1x1*RR3x1*RR3x3**2 - 3*complex(0,1)*LSX*RR1x2*RR3x2*RR3x3**2 - 6*complex(0,1)*LX*RR1x3*RR3x3**3',
                 order = {'QED':2})

GC_34 = Coupling(name = 'GC_34',
                 value = '-6*complex(0,1)*LH*RR2x1*RR3x1**3 - 3*complex(0,1)*LHS*RR2x2*RR3x1**2*RR3x2 - 3*complex(0,1)*LHS*RR2x1*RR3x1*RR3x2**2 - 6*complex(0,1)*LS*RR2x2*RR3x2**3 - 3*complex(0,1)*LHX*RR2x3*RR3x1**2*RR3x3 - 3*complex(0,1)*LSX*RR2x3*RR3x2**2*RR3x3 - 3*complex(0,1)*LHX*RR2x1*RR3x1*RR3x3**2 - 3*complex(0,1)*LSX*RR2x2*RR3x2*RR3x3**2 - 6*complex(0,1)*LX*RR2x3*RR3x3**3',
                 order = {'QED':2})

GC_35 = Coupling(name = 'GC_35',
                 value = '-6*complex(0,1)*LH*RR3x1**4 - 6*complex(0,1)*LHS*RR3x1**2*RR3x2**2 - 6*complex(0,1)*LS*RR3x2**4 - 6*complex(0,1)*LHX*RR3x1**2*RR3x3**2 - 6*complex(0,1)*LSX*RR3x2**2*RR3x3**2 - 6*complex(0,1)*LX*RR3x3**4',
                 order = {'QED':2})

GC_36 = Coupling(name = 'GC_36',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_37 = Coupling(name = 'GC_37',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_38 = Coupling(name = 'GC_38',
                 value = '(ee**2*complex(0,1)*RR1x1**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_39 = Coupling(name = 'GC_39',
                 value = '(ee**2*complex(0,1)*RR1x1*RR2x1)/(2.*sw**2)',
                 order = {'QED':2})

GC_40 = Coupling(name = 'GC_40',
                 value = '(ee**2*complex(0,1)*RR2x1**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_41 = Coupling(name = 'GC_41',
                 value = '(ee**2*complex(0,1)*RR1x1*RR3x1)/(2.*sw**2)',
                 order = {'QED':2})

GC_42 = Coupling(name = 'GC_42',
                 value = '(ee**2*complex(0,1)*RR2x1*RR3x1)/(2.*sw**2)',
                 order = {'QED':2})

GC_43 = Coupling(name = 'GC_43',
                 value = '(ee**2*complex(0,1)*RR3x1**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_44 = Coupling(name = 'GC_44',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-(cw*ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '(cw*ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_49 = Coupling(name = 'GC_49',
                 value = '-(ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '(ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_52 = Coupling(name = 'GC_52',
                 value = 'ee**2*complex(0,1)*RR1x1**2 + (cw**2*ee**2*complex(0,1)*RR1x1**2)/(2.*sw**2) + (ee**2*complex(0,1)*RR1x1**2*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = 'ee**2*complex(0,1)*RR1x1*RR2x1 + (cw**2*ee**2*complex(0,1)*RR1x1*RR2x1)/(2.*sw**2) + (ee**2*complex(0,1)*RR1x1*RR2x1*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_54 = Coupling(name = 'GC_54',
                 value = 'ee**2*complex(0,1)*RR2x1**2 + (cw**2*ee**2*complex(0,1)*RR2x1**2)/(2.*sw**2) + (ee**2*complex(0,1)*RR2x1**2*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = 'ee**2*complex(0,1)*RR1x1*RR3x1 + (cw**2*ee**2*complex(0,1)*RR1x1*RR3x1)/(2.*sw**2) + (ee**2*complex(0,1)*RR1x1*RR3x1*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_56 = Coupling(name = 'GC_56',
                 value = 'ee**2*complex(0,1)*RR2x1*RR3x1 + (cw**2*ee**2*complex(0,1)*RR2x1*RR3x1)/(2.*sw**2) + (ee**2*complex(0,1)*RR2x1*RR3x1*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_57 = Coupling(name = 'GC_57',
                 value = 'ee**2*complex(0,1)*RR3x1**2 + (cw**2*ee**2*complex(0,1)*RR3x1**2)/(2.*sw**2) + (ee**2*complex(0,1)*RR3x1**2*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_58 = Coupling(name = 'GC_58',
                 value = '(ee**2*complex(0,1)*RR1x1*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '(ee**2*complex(0,1)*RR2x1*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '(ee**2*complex(0,1)*RR3x1*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = 'ee**2*complex(0,1)*RR1x1*vev + (cw**2*ee**2*complex(0,1)*RR1x1*vev)/(2.*sw**2) + (ee**2*complex(0,1)*RR1x1*sw**2*vev)/(2.*cw**2)',
                 order = {'QED':1})

GC_62 = Coupling(name = 'GC_62',
                 value = 'ee**2*complex(0,1)*RR2x1*vev + (cw**2*ee**2*complex(0,1)*RR2x1*vev)/(2.*sw**2) + (ee**2*complex(0,1)*RR2x1*sw**2*vev)/(2.*cw**2)',
                 order = {'QED':1})

GC_63 = Coupling(name = 'GC_63',
                 value = 'ee**2*complex(0,1)*RR3x1*vev + (cw**2*ee**2*complex(0,1)*RR3x1*vev)/(2.*sw**2) + (ee**2*complex(0,1)*RR3x1*sw**2*vev)/(2.*cw**2)',
                 order = {'QED':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '-6*complex(0,1)*LH*RR1x1**3*vev - 3*complex(0,1)*LHS*RR1x1*RR1x2**2*vev - 3*complex(0,1)*LHX*RR1x1*RR1x3**2*vev - 3*complex(0,1)*LHS*RR1x1**2*RR1x2*vs - 6*complex(0,1)*LS*RR1x2**3*vs - 3*complex(0,1)*LSX*RR1x2*RR1x3**2*vs - 3*complex(0,1)*LHX*RR1x1**2*RR1x3*vx - 3*complex(0,1)*LSX*RR1x2**2*RR1x3*vx - 6*complex(0,1)*LX*RR1x3**3*vx',
                 order = {'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '-6*complex(0,1)*LH*RR1x1**2*RR2x1*vev - complex(0,1)*LHS*RR1x2**2*RR2x1*vev - complex(0,1)*LHX*RR1x3**2*RR2x1*vev - 2*complex(0,1)*LHS*RR1x1*RR1x2*RR2x2*vev - 2*complex(0,1)*LHX*RR1x1*RR1x3*RR2x3*vev - 2*complex(0,1)*LHS*RR1x1*RR1x2*RR2x1*vs - complex(0,1)*LHS*RR1x1**2*RR2x2*vs - 6*complex(0,1)*LS*RR1x2**2*RR2x2*vs - complex(0,1)*LSX*RR1x3**2*RR2x2*vs - 2*complex(0,1)*LSX*RR1x2*RR1x3*RR2x3*vs - 2*complex(0,1)*LHX*RR1x1*RR1x3*RR2x1*vx - 2*complex(0,1)*LSX*RR1x2*RR1x3*RR2x2*vx - complex(0,1)*LHX*RR1x1**2*RR2x3*vx - complex(0,1)*LSX*RR1x2**2*RR2x3*vx - 6*complex(0,1)*LX*RR1x3**2*RR2x3*vx',
                 order = {'QED':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '-6*complex(0,1)*LH*RR1x1*RR2x1**2*vev - 2*complex(0,1)*LHS*RR1x2*RR2x1*RR2x2*vev - complex(0,1)*LHS*RR1x1*RR2x2**2*vev - 2*complex(0,1)*LHX*RR1x3*RR2x1*RR2x3*vev - complex(0,1)*LHX*RR1x1*RR2x3**2*vev - complex(0,1)*LHS*RR1x2*RR2x1**2*vs - 2*complex(0,1)*LHS*RR1x1*RR2x1*RR2x2*vs - 6*complex(0,1)*LS*RR1x2*RR2x2**2*vs - 2*complex(0,1)*LSX*RR1x3*RR2x2*RR2x3*vs - complex(0,1)*LSX*RR1x2*RR2x3**2*vs - complex(0,1)*LHX*RR1x3*RR2x1**2*vx - complex(0,1)*LSX*RR1x3*RR2x2**2*vx - 2*complex(0,1)*LHX*RR1x1*RR2x1*RR2x3*vx - 2*complex(0,1)*LSX*RR1x2*RR2x2*RR2x3*vx - 6*complex(0,1)*LX*RR1x3*RR2x3**2*vx',
                 order = {'QED':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '-6*complex(0,1)*LH*RR2x1**3*vev - 3*complex(0,1)*LHS*RR2x1*RR2x2**2*vev - 3*complex(0,1)*LHX*RR2x1*RR2x3**2*vev - 3*complex(0,1)*LHS*RR2x1**2*RR2x2*vs - 6*complex(0,1)*LS*RR2x2**3*vs - 3*complex(0,1)*LSX*RR2x2*RR2x3**2*vs - 3*complex(0,1)*LHX*RR2x1**2*RR2x3*vx - 3*complex(0,1)*LSX*RR2x2**2*RR2x3*vx - 6*complex(0,1)*LX*RR2x3**3*vx',
                 order = {'QED':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '-6*complex(0,1)*LH*RR1x1**2*RR3x1*vev - complex(0,1)*LHS*RR1x2**2*RR3x1*vev - complex(0,1)*LHX*RR1x3**2*RR3x1*vev - 2*complex(0,1)*LHS*RR1x1*RR1x2*RR3x2*vev - 2*complex(0,1)*LHX*RR1x1*RR1x3*RR3x3*vev - 2*complex(0,1)*LHS*RR1x1*RR1x2*RR3x1*vs - complex(0,1)*LHS*RR1x1**2*RR3x2*vs - 6*complex(0,1)*LS*RR1x2**2*RR3x2*vs - complex(0,1)*LSX*RR1x3**2*RR3x2*vs - 2*complex(0,1)*LSX*RR1x2*RR1x3*RR3x3*vs - 2*complex(0,1)*LHX*RR1x1*RR1x3*RR3x1*vx - 2*complex(0,1)*LSX*RR1x2*RR1x3*RR3x2*vx - complex(0,1)*LHX*RR1x1**2*RR3x3*vx - complex(0,1)*LSX*RR1x2**2*RR3x3*vx - 6*complex(0,1)*LX*RR1x3**2*RR3x3*vx',
                 order = {'QED':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '-6*complex(0,1)*LH*RR1x1*RR2x1*RR3x1*vev - complex(0,1)*LHS*RR1x2*RR2x2*RR3x1*vev - complex(0,1)*LHX*RR1x3*RR2x3*RR3x1*vev - complex(0,1)*LHS*RR1x2*RR2x1*RR3x2*vev - complex(0,1)*LHS*RR1x1*RR2x2*RR3x2*vev - complex(0,1)*LHX*RR1x3*RR2x1*RR3x3*vev - complex(0,1)*LHX*RR1x1*RR2x3*RR3x3*vev - complex(0,1)*LHS*RR1x2*RR2x1*RR3x1*vs - complex(0,1)*LHS*RR1x1*RR2x2*RR3x1*vs - complex(0,1)*LHS*RR1x1*RR2x1*RR3x2*vs - 6*complex(0,1)*LS*RR1x2*RR2x2*RR3x2*vs - complex(0,1)*LSX*RR1x3*RR2x3*RR3x2*vs - complex(0,1)*LSX*RR1x3*RR2x2*RR3x3*vs - complex(0,1)*LSX*RR1x2*RR2x3*RR3x3*vs - complex(0,1)*LHX*RR1x3*RR2x1*RR3x1*vx - complex(0,1)*LHX*RR1x1*RR2x3*RR3x1*vx - complex(0,1)*LSX*RR1x3*RR2x2*RR3x2*vx - complex(0,1)*LSX*RR1x2*RR2x3*RR3x2*vx - complex(0,1)*LHX*RR1x1*RR2x1*RR3x3*vx - complex(0,1)*LSX*RR1x2*RR2x2*RR3x3*vx - 6*complex(0,1)*LX*RR1x3*RR2x3*RR3x3*vx',
                 order = {'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '-6*complex(0,1)*LH*RR2x1**2*RR3x1*vev - complex(0,1)*LHS*RR2x2**2*RR3x1*vev - complex(0,1)*LHX*RR2x3**2*RR3x1*vev - 2*complex(0,1)*LHS*RR2x1*RR2x2*RR3x2*vev - 2*complex(0,1)*LHX*RR2x1*RR2x3*RR3x3*vev - 2*complex(0,1)*LHS*RR2x1*RR2x2*RR3x1*vs - complex(0,1)*LHS*RR2x1**2*RR3x2*vs - 6*complex(0,1)*LS*RR2x2**2*RR3x2*vs - complex(0,1)*LSX*RR2x3**2*RR3x2*vs - 2*complex(0,1)*LSX*RR2x2*RR2x3*RR3x3*vs - 2*complex(0,1)*LHX*RR2x1*RR2x3*RR3x1*vx - 2*complex(0,1)*LSX*RR2x2*RR2x3*RR3x2*vx - complex(0,1)*LHX*RR2x1**2*RR3x3*vx - complex(0,1)*LSX*RR2x2**2*RR3x3*vx - 6*complex(0,1)*LX*RR2x3**2*RR3x3*vx',
                 order = {'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '-6*complex(0,1)*LH*RR1x1*RR3x1**2*vev - 2*complex(0,1)*LHS*RR1x2*RR3x1*RR3x2*vev - complex(0,1)*LHS*RR1x1*RR3x2**2*vev - 2*complex(0,1)*LHX*RR1x3*RR3x1*RR3x3*vev - complex(0,1)*LHX*RR1x1*RR3x3**2*vev - complex(0,1)*LHS*RR1x2*RR3x1**2*vs - 2*complex(0,1)*LHS*RR1x1*RR3x1*RR3x2*vs - 6*complex(0,1)*LS*RR1x2*RR3x2**2*vs - 2*complex(0,1)*LSX*RR1x3*RR3x2*RR3x3*vs - complex(0,1)*LSX*RR1x2*RR3x3**2*vs - complex(0,1)*LHX*RR1x3*RR3x1**2*vx - complex(0,1)*LSX*RR1x3*RR3x2**2*vx - 2*complex(0,1)*LHX*RR1x1*RR3x1*RR3x3*vx - 2*complex(0,1)*LSX*RR1x2*RR3x2*RR3x3*vx - 6*complex(0,1)*LX*RR1x3*RR3x3**2*vx',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '-6*complex(0,1)*LH*RR2x1*RR3x1**2*vev - 2*complex(0,1)*LHS*RR2x2*RR3x1*RR3x2*vev - complex(0,1)*LHS*RR2x1*RR3x2**2*vev - 2*complex(0,1)*LHX*RR2x3*RR3x1*RR3x3*vev - complex(0,1)*LHX*RR2x1*RR3x3**2*vev - complex(0,1)*LHS*RR2x2*RR3x1**2*vs - 2*complex(0,1)*LHS*RR2x1*RR3x1*RR3x2*vs - 6*complex(0,1)*LS*RR2x2*RR3x2**2*vs - 2*complex(0,1)*LSX*RR2x3*RR3x2*RR3x3*vs - complex(0,1)*LSX*RR2x2*RR3x3**2*vs - complex(0,1)*LHX*RR2x3*RR3x1**2*vx - complex(0,1)*LSX*RR2x3*RR3x2**2*vx - 2*complex(0,1)*LHX*RR2x1*RR3x1*RR3x3*vx - 2*complex(0,1)*LSX*RR2x2*RR3x2*RR3x3*vx - 6*complex(0,1)*LX*RR2x3*RR3x3**2*vx',
                 order = {'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '-6*complex(0,1)*LH*RR3x1**3*vev - 3*complex(0,1)*LHS*RR3x1*RR3x2**2*vev - 3*complex(0,1)*LHX*RR3x1*RR3x3**2*vev - 3*complex(0,1)*LHS*RR3x1**2*RR3x2*vs - 6*complex(0,1)*LS*RR3x2**3*vs - 3*complex(0,1)*LSX*RR3x2*RR3x3**2*vs - 3*complex(0,1)*LHX*RR3x1**2*RR3x3*vx - 3*complex(0,1)*LSX*RR3x2**2*RR3x3*vx - 6*complex(0,1)*LX*RR3x3**3*vx',
                 order = {'QED':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '-((complex(0,1)*RR1x1*yb)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '-((complex(0,1)*RR2x1*yb)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '-((complex(0,1)*RR3x1*yb)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '-((complex(0,1)*RR1x1*yt)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '-((complex(0,1)*RR2x1*yt)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '-((complex(0,1)*RR3x1*yt)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '-((complex(0,1)*RR1x1*ytau)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '-((complex(0,1)*RR2x1*ytau)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '-((complex(0,1)*RR3x1*ytau)/cmath.sqrt(2))',
                 order = {'QED':1})

