Requestor: Lars Rickard Strom
Contents: Two-Real Singlet Model
Paper: https://arxiv.org/abs/1908.08554
Source: https://gitlab.cern.ch/atlastrsm/models/-/tree/master
Note: Private communication from paper authors
JIRA: https://its.cern.ch/jira/browse/AGENE-1839