Requestor: Casey Bellgraph
Content: New model including vector-like quarks and leptons alongside a 2HDM.
Source: Radovan Dermisek
Papers: [Most detail] https://arxiv.org/abs/1608.00662, [additional paper and first description of the model] https://arxiv.org/abs/1512.07837, https://arxiv.org/abs/1509.04292
