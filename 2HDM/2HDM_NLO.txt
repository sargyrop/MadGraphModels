Requestor: Julien Donini
Content: NLO 2 Higgs Doublet Model
Link: http://feynrules.irmp.ucl.ac.be/wiki/2HDM
JIRA: https://its.cern.ch/jira/browse/AGENE-1044
