Requestor: Burhani Taher Saifuddin
E-mail: burhani.taher.saifuddin@ou.edu
Content: 2HDM_NLO model with CP Violating terms + QCD and QED corrections. New name 2HDM_NLO_CPV
Link: http://feynrules.irmp.ucl.ac.be/wiki/2HDM
UFO: http://feynrules.irmp.ucl.ac.be/attachment/wiki/2HDM/2HDM_NLO.tar.gz
JIRA: https://its.cern.ch/jira/browse/AGENE-2212

