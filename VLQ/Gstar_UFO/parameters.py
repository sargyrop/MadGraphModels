# This file was automatically created by FeynRules $Revision: 915 $
# Mathematica version: 8.0 for Mac OS X x86 (64-bit) (October 5, 2011)
# Date: Thu 13 Feb 2014 17:43:16



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\text{aS}',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172.,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

Ycom = Parameter(name = 'Ycom',
                 nature = 'external',
                 type = 'real',
                 value = 3.,
                 texname = 'Y_{\\text{com}}',
                 lhablock = 'FRBlock',
                 lhacode = [ 1 ])

g3com = Parameter(name = 'g3com',
                  nature = 'external',
                  type = 'real',
                  value = 3.,
                  texname = 'g_{\\text{com3}}',
                  lhablock = 'FRBlock',
                  lhacode = [ 2 ])

sR = Parameter(name = 'sR',
               nature = 'external',
               type = 'real',
               value = 0.6,
               texname = 's_R',
               lhablock = 'FRBlock',
               lhacode = [ 3 ])

Mph = Parameter(name = 'Mph',
                nature = 'external',
                type = 'real',
                value = 1000.,
                texname = '\\text{Subscript}\\left[M\'\\right]',
                lhablock = 'FRBlock',
                lhacode = [ 4 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MkkG = Parameter(name = 'MkkG',
                 nature = 'external',
                 type = 'real',
                 value = 1500,
                 texname = '\\text{MkkG}',
                 lhablock = 'MASS',
                 lhacode = [ 3000021 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 120,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WTheavy = Parameter(name = 'WTheavy',
                    nature = 'external',
                    type = 'real',
                    value = 20.92,
                    texname = '\\text{WTheavy}',
                    lhablock = 'DECAY',
                    lhacode = [ 4000006 ])

WBheavy = Parameter(name = 'WBheavy',
                    nature = 'external',
                    type = 'real',
                    value = 20.27,
                    texname = '\\text{WBheavy}',
                    lhablock = 'DECAY',
                    lhacode = [ 4000005 ])

WTtilde = Parameter(name = 'WTtilde',
                    nature = 'external',
                    type = 'real',
                    value = 34.45,
                    texname = '\\text{WTtilde}',
                    lhablock = 'DECAY',
                    lhacode = [ 4000008 ])

WBtilde = Parameter(name = 'WBtilde',
                    nature = 'external',
                    type = 'real',
                    value = 0.0307,
                    texname = '\\text{WBtilde}',
                    lhablock = 'DECAY',
                    lhacode = [ 4000007 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WkkG = Parameter(name = 'WkkG',
                 nature = 'external',
                 type = 'real',
                 value = 77.29,
                 texname = '\\text{WkkG}',
                 lhablock = 'DECAY',
                 lhacode = [ 3000021 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00575308848,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

CKM11 = Parameter(name = 'CKM11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cmath.cos(cabi)',
                  texname = '\\text{CKM11}')

CKM12 = Parameter(name = 'CKM12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cmath.sin(cabi)',
                  texname = '\\text{CKM12}')

CKM21 = Parameter(name = 'CKM21',
                  nature = 'internal',
                  type = 'complex',
                  value = '-cmath.sin(cabi)',
                  texname = '\\text{CKM21}')

CKM22 = Parameter(name = 'CKM22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cmath.cos(cabi)',
                  texname = '\\text{CKM22}')

MBh = Parameter(name = 'MBh',
                nature = 'internal',
                type = 'real',
                value = 'Mph',
                texname = 'M\'_B')

MBs = Parameter(name = 'MBs',
                nature = 'internal',
                type = 'real',
                value = 'Mph',
                texname = 'M\'_{\\text{Bs}}')

MTh = Parameter(name = 'MTh',
                nature = 'internal',
                type = 'real',
                value = 'Mph',
                texname = 'M\'_T')

MTs = Parameter(name = 'MTs',
                nature = 'internal',
                type = 'real',
                value = 'Mph',
                texname = 'M\'_{\\text{Ts}}')

s2 = Parameter(name = 's2',
               nature = 'internal',
               type = 'real',
               value = '(sR*ymb)/ymt',
               texname = 's_2')

sT3 = Parameter(name = 'sT3',
                nature = 'internal',
                type = 'real',
                value = '(2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi))/g3com',
                texname = '\\text{sin$\\theta $}_3')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\text{aEW}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

tT3 = Parameter(name = 'tT3',
                nature = 'internal',
                type = 'real',
                value = 'sT3/cmath.sqrt(1 - sT3**2)',
                texname = '\\text{tan$\\theta $}_3')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

v = Parameter(name = 'v',
              nature = 'internal',
              type = 'real',
              value = '(2*MW*sw)/ee',
              texname = 'v')

s1 = Parameter(name = 's1',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/(sR*v*Ycom)',
               texname = 's_1')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*v**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/v',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/v',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/v',
                 texname = '\\text{ytau}')

Mcom = Parameter(name = 'Mcom',
                 nature = 'internal',
                 type = 'real',
                 value = 'Mph*cmath.sqrt(1 - s1**2)',
                 texname = 'M_Q')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*v**2)',
                texname = '\\mu')

