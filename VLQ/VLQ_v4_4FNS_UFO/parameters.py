# This file was automatically created by FeynRules 2.4.68
# Mathematica version: 10.4.1 for Mac OS X x86 (64-bit) (April 11, 2016)
# Date: Thu 22 Aug 2019 12:22:53



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# This is a default parameter object representing the renormalization scale (MU_R).
MU_R = Parameter(name = 'MU_R',
                 nature = 'external',
                 type = 'real',
                 value = 91.188,
                 texname = '\\text{\\mu_r}',
                 lhablock = 'LOOP',
                 lhacode = [1])

# User-defined parameters.
cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

KBLh1 = Parameter(name = 'KBLh1',
                  nature = 'external',
                  type = 'real',
                  value = 0.479336,
                  texname = '\\text{KBLh1}',
                  lhablock = 'KBLH',
                  lhacode = [ 1 ])

KBLh2 = Parameter(name = 'KBLh2',
                  nature = 'external',
                  type = 'real',
                  value = 0.479336,
                  texname = '\\text{KBLh2}',
                  lhablock = 'KBLH',
                  lhacode = [ 2 ])

KBLh3 = Parameter(name = 'KBLh3',
                  nature = 'external',
                  type = 'real',
                  value = 0.639117,
                  texname = '\\text{KBLh3}',
                  lhablock = 'KBLH',
                  lhacode = [ 3 ])

KBLw1 = Parameter(name = 'KBLw1',
                  nature = 'external',
                  type = 'real',
                  value = 0.120112,
                  texname = '\\text{KBLw1}',
                  lhablock = 'KBLW',
                  lhacode = [ 1 ])

KBLw2 = Parameter(name = 'KBLw2',
                  nature = 'external',
                  type = 'real',
                  value = 0.120112,
                  texname = '\\text{KBLw2}',
                  lhablock = 'KBLW',
                  lhacode = [ 2 ])

KBLw3 = Parameter(name = 'KBLw3',
                  nature = 'external',
                  type = 'real',
                  value = 0.160149,
                  texname = '\\text{KBLw3}',
                  lhablock = 'KBLW',
                  lhacode = [ 3 ])

KBLz1 = Parameter(name = 'KBLz1',
                  nature = 'external',
                  type = 'real',
                  value = 0.180284,
                  texname = '\\text{KBLz1}',
                  lhablock = 'KBLZ',
                  lhacode = [ 1 ])

KBLz2 = Parameter(name = 'KBLz2',
                  nature = 'external',
                  type = 'real',
                  value = 0.180284,
                  texname = '\\text{KBLz2}',
                  lhablock = 'KBLZ',
                  lhacode = [ 2 ])

KBLz3 = Parameter(name = 'KBLz3',
                  nature = 'external',
                  type = 'real',
                  value = 0.240379,
                  texname = '\\text{KBLz3}',
                  lhablock = 'KBLZ',
                  lhacode = [ 3 ])

KBRh1 = Parameter(name = 'KBRh1',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KBRh1}',
                  lhablock = 'KBRH',
                  lhacode = [ 1 ])

KBRh2 = Parameter(name = 'KBRh2',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KBRh2}',
                  lhablock = 'KBRH',
                  lhacode = [ 2 ])

KBRh3 = Parameter(name = 'KBRh3',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KBRh3}',
                  lhablock = 'KBRH',
                  lhacode = [ 3 ])

KBRw1 = Parameter(name = 'KBRw1',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KBRw1}',
                  lhablock = 'KBRW',
                  lhacode = [ 1 ])

KBRw2 = Parameter(name = 'KBRw2',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KBRw2}',
                  lhablock = 'KBRW',
                  lhacode = [ 2 ])

KBRw3 = Parameter(name = 'KBRw3',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KBRw3}',
                  lhablock = 'KBRW',
                  lhacode = [ 3 ])

KBRz1 = Parameter(name = 'KBRz1',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KBRz1}',
                  lhablock = 'KBRZ',
                  lhacode = [ 1 ])

KBRz2 = Parameter(name = 'KBRz2',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KBRz2}',
                  lhablock = 'KBRZ',
                  lhacode = [ 2 ])

KBRz3 = Parameter(name = 'KBRz3',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KBRz3}',
                  lhablock = 'KBRZ',
                  lhacode = [ 3 ])

KTLh1 = Parameter(name = 'KTLh1',
                  nature = 'external',
                  type = 'real',
                  value = 0.479336,
                  texname = '\\text{KTLh1}',
                  lhablock = 'KTLH',
                  lhacode = [ 1 ])

KTLh2 = Parameter(name = 'KTLh2',
                  nature = 'external',
                  type = 'real',
                  value = 0.479336,
                  texname = '\\text{KTLh2}',
                  lhablock = 'KTLH',
                  lhacode = [ 2 ])

KTLh3 = Parameter(name = 'KTLh3',
                  nature = 'external',
                  type = 'real',
                  value = 0.639117,
                  texname = '\\text{KTLh3}',
                  lhablock = 'KTLH',
                  lhacode = [ 3 ])

KTLw1 = Parameter(name = 'KTLw1',
                  nature = 'external',
                  type = 'real',
                  value = 0.120112,
                  texname = '\\text{KTLw1}',
                  lhablock = 'KTLW',
                  lhacode = [ 1 ])

KTLw2 = Parameter(name = 'KTLw2',
                  nature = 'external',
                  type = 'real',
                  value = 0.120112,
                  texname = '\\text{KTLw2}',
                  lhablock = 'KTLW',
                  lhacode = [ 2 ])

KTLw3 = Parameter(name = 'KTLw3',
                  nature = 'external',
                  type = 'real',
                  value = 0.160149,
                  texname = '\\text{KTLw3}',
                  lhablock = 'KTLW',
                  lhacode = [ 3 ])

KTLz1 = Parameter(name = 'KTLz1',
                  nature = 'external',
                  type = 'real',
                  value = 0.180284,
                  texname = '\\text{KTLz1}',
                  lhablock = 'KTLZ',
                  lhacode = [ 1 ])

KTLz2 = Parameter(name = 'KTLz2',
                  nature = 'external',
                  type = 'real',
                  value = 0.180284,
                  texname = '\\text{KTLz2}',
                  lhablock = 'KTLZ',
                  lhacode = [ 2 ])

KTLz3 = Parameter(name = 'KTLz3',
                  nature = 'external',
                  type = 'real',
                  value = 0.240379,
                  texname = '\\text{KTLz3}',
                  lhablock = 'KTLZ',
                  lhacode = [ 3 ])

KTRh1 = Parameter(name = 'KTRh1',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KTRh1}',
                  lhablock = 'KTRH',
                  lhacode = [ 1 ])

KTRh2 = Parameter(name = 'KTRh2',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KTRh2}',
                  lhablock = 'KTRH',
                  lhacode = [ 2 ])

KTRh3 = Parameter(name = 'KTRh3',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KTRh3}',
                  lhablock = 'KTRH',
                  lhacode = [ 3 ])

KTRw1 = Parameter(name = 'KTRw1',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KTRw1}',
                  lhablock = 'KTRW',
                  lhacode = [ 1 ])

KTRw2 = Parameter(name = 'KTRw2',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KTRw2}',
                  lhablock = 'KTRW',
                  lhacode = [ 2 ])

KTRw3 = Parameter(name = 'KTRw3',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KTRw3}',
                  lhablock = 'KTRW',
                  lhacode = [ 3 ])

KTRz1 = Parameter(name = 'KTRz1',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KTRz1}',
                  lhablock = 'KTRZ',
                  lhacode = [ 1 ])

KTRz2 = Parameter(name = 'KTRz2',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KTRz2}',
                  lhablock = 'KTRZ',
                  lhacode = [ 2 ])

KTRz3 = Parameter(name = 'KTRz3',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = '\\text{KTRz3}',
                  lhablock = 'KTRZ',
                  lhacode = [ 3 ])

KXL1 = Parameter(name = 'KXL1',
                 nature = 'external',
                 type = 'real',
                 value = 0.300279,
                 texname = '\\text{KXL1}',
                 lhablock = 'KXLW',
                 lhacode = [ 1 ])

KXL2 = Parameter(name = 'KXL2',
                 nature = 'external',
                 type = 'real',
                 value = 0.300279,
                 texname = '\\text{KXL2}',
                 lhablock = 'KXLW',
                 lhacode = [ 2 ])

KXL3 = Parameter(name = 'KXL3',
                 nature = 'external',
                 type = 'real',
                 value = 0.400379,
                 texname = '\\text{KXL3}',
                 lhablock = 'KXLW',
                 lhacode = [ 3 ])

KXR1 = Parameter(name = 'KXR1',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KXR1}',
                 lhablock = 'KXRW',
                 lhacode = [ 1 ])

KXR2 = Parameter(name = 'KXR2',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KXR2}',
                 lhablock = 'KXRW',
                 lhacode = [ 2 ])

KXR3 = Parameter(name = 'KXR3',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KXR3}',
                 lhablock = 'KXRW',
                 lhacode = [ 3 ])

KYL1 = Parameter(name = 'KYL1',
                 nature = 'external',
                 type = 'real',
                 value = 0.300279,
                 texname = '\\text{KYL1}',
                 lhablock = 'KYLW',
                 lhacode = [ 1 ])

KYL2 = Parameter(name = 'KYL2',
                 nature = 'external',
                 type = 'real',
                 value = 0.300279,
                 texname = '\\text{KYL2}',
                 lhablock = 'KYLW',
                 lhacode = [ 2 ])

KYL3 = Parameter(name = 'KYL3',
                 nature = 'external',
                 type = 'real',
                 value = 0.400379,
                 texname = '\\text{KYL3}',
                 lhablock = 'KYLW',
                 lhacode = [ 3 ])

KYR1 = Parameter(name = 'KYR1',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KYR1}',
                 lhablock = 'KYRW',
                 lhacode = [ 1 ])

KYR2 = Parameter(name = 'KYR2',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KYR2}',
                 lhablock = 'KYRW',
                 lhacode = [ 2 ])

KYR3 = Parameter(name = 'KYR3',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KYR3}',
                 lhablock = 'KYRW',
                 lhacode = [ 3 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

MX = Parameter(name = 'MX',
               nature = 'external',
               type = 'real',
               value = 600,
               texname = '\\text{MX}',
               lhablock = 'MASS',
               lhacode = [ 6000005 ])

MTP = Parameter(name = 'MTP',
                nature = 'external',
                type = 'real',
                value = 600,
                texname = '\\text{MTP}',
                lhablock = 'MASS',
                lhacode = [ 6000006 ])

MBP = Parameter(name = 'MBP',
                nature = 'external',
                type = 'real',
                value = 600,
                texname = '\\text{MBP}',
                lhablock = 'MASS',
                lhacode = [ 6000007 ])

MY = Parameter(name = 'MY',
               nature = 'external',
               type = 'real',
               value = 600,
               texname = '\\text{MY}',
               lhablock = 'MASS',
               lhacode = [ 6000008 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WX = Parameter(name = 'WX',
               nature = 'external',
               type = 'real',
               value = 5.,
               texname = '\\text{WX}',
               lhablock = 'DECAY',
               lhacode = [ 6000005 ])

WTP = Parameter(name = 'WTP',
                nature = 'external',
                type = 'real',
                value = 5.,
                texname = '\\text{WTP}',
                lhablock = 'DECAY',
                lhacode = [ 6000006 ])

WBP = Parameter(name = 'WBP',
                nature = 'external',
                type = 'real',
                value = 5.,
                texname = '\\text{WBP}',
                lhablock = 'DECAY',
                lhacode = [ 6000007 ])

WY = Parameter(name = 'WY',
               nature = 'external',
               type = 'real',
               value = 5.,
               texname = '\\text{WY}',
               lhablock = 'DECAY',
               lhacode = [ 6000008 ])

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.sin(cabi)',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-cmath.sin(cabi)',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '1',
                   texname = '\\text{CKM3x3}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

