# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 11.2.0 for Microsoft Windows (64-bit) (September 11, 2017)
# Date: Thu 28 Sep 2023 13:33:30


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



