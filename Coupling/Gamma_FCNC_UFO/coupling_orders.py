# This file was automatically created by FeynRules 2.0.8
# Mathematica version: 9.0 for Linux x86 (64-bit) (November 20, 2012)
# Date: Fri 28 Aug 2015 20:39:46


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

NP = CouplingOrder(name = 'NP',
                   expansion_order = 99,
                   hierarchy = 1)

