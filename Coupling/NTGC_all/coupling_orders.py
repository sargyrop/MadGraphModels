# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 13.2.1 for Microsoft Windows (64-bit) (January 27, 2023)
# Date: Wed 24 May 2023 23:28:48


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

GP = CouplingOrder(name = 'GP',
                   expansion_order = 99,
                   hierarchy = 1)

GM = CouplingOrder(name = 'GM',
                   expansion_order = 99,
                   hierarchy = 1)

BTW = CouplingOrder(name = 'BTW',
                    expansion_order = 99,
                    hierarchy = 1)

BW = CouplingOrder(name = 'BW',
                   expansion_order = 99,
                   hierarchy = 1)

BB = CouplingOrder(name = 'BB',
                   expansion_order = 99,
                   hierarchy = 1)

WW = CouplingOrder(name = 'WW',
                   expansion_order = 99,
                   hierarchy = 1)

