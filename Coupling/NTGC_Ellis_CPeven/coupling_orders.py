# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 12.1.1 for Mac OS X x86 (64-bit) (July 15, 2020)
# Date: Tue 21 Feb 2023 13:23:05


from object_library import all_orders, CouplingOrder


NP = CouplingOrder(name = 'NP',
                   expansion_order = 1,
                   hierarchy = 1)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

BB = CouplingOrder(name = 'BB',
                   expansion_order = 99,
                   hierarchy = 1)

BTW = CouplingOrder(name = 'BTW',
                    expansion_order = 99,
                    hierarchy = 1)

BW = CouplingOrder(name = 'BW',
                   expansion_order = 99,
                   hierarchy = 1)

GM = CouplingOrder(name = 'GM',
                   expansion_order = 99,
                   hierarchy = 1)

GP = CouplingOrder(name = 'GP',
                   expansion_order = 99,
                   hierarchy = 1)

WW = CouplingOrder(name = 'WW',
                   expansion_order = 99,
                   hierarchy = 1)

