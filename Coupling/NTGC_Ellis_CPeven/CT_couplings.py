# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 12.1.1 for Mac OS X x86 (64-bit) (July 15, 2020)
# Date: Tue 21 Feb 2023 13:23:05


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



