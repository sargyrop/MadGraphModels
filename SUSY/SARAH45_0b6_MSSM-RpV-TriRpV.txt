Content: RPV MSSM produced with Sarah 4.5.8 MSSM-RpV and TriRpV modules
Requestor: Romain Kukla
Paper: https://arxiv.org/abs/1611.05850
JIRA: https://its.cern.ch/jira/browse/AGENE-1276