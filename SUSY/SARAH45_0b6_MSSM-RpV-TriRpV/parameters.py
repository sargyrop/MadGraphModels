# ----------------------------------------------------------------------  
# This model file was automatically created by SARAH version4.5.8 
# SARAH References: arXiv:0806.0538, arXiv:0909.2863, arXiv:1002.0840    
# (c) Florian Staub, 2011  
# ----------------------------------------------------------------------  
# File created at 15:5 on 24.10.2016   
# ----------------------------------------------------------------------  
 
 
from object_library import all_parameters,Parameter 
 
from function_library import complexconjugate,re,im,csc,sec,acsc,asec 
 
ZERO=Parameter(name='ZERO', 
                      nature='internal', 
                      type='real', 
                      value='0.0', 
                      texname='0') 
 
Mgo = 	 Parameter(name = 'Mgo', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mgo}', 
	 lhablock = 'MASS', 
	 lhacode = [1000021]) 
 
Wgo = 	 Parameter(name = 'Wgo', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wgo}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000021]) 
 
MN1 = 	 Parameter(name = 'MN1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MN1}', 
	 lhablock = 'MASS', 
	 lhacode = [1000022]) 
 
WN1 = 	 Parameter(name = 'WN1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WN1}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000022]) 
 
MN2 = 	 Parameter(name = 'MN2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MN2}', 
	 lhablock = 'MASS', 
	 lhacode = [1000023]) 
 
WN2 = 	 Parameter(name = 'WN2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WN2}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000023]) 
 
MN3 = 	 Parameter(name = 'MN3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MN3}', 
	 lhablock = 'MASS', 
	 lhacode = [1000025]) 
 
WN3 = 	 Parameter(name = 'WN3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WN3}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000025]) 
 
MN4 = 	 Parameter(name = 'MN4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MN4}', 
	 lhablock = 'MASS', 
	 lhacode = [1000035]) 
 
WN4 = 	 Parameter(name = 'WN4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WN4}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000035]) 
 
MC1 = 	 Parameter(name = 'MC1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MC1}', 
	 lhablock = 'MASS', 
	 lhacode = [1000024]) 
 
WC1 = 	 Parameter(name = 'WC1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WC1}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000024]) 
 
MC2 = 	 Parameter(name = 'MC2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MC2}', 
	 lhablock = 'MASS', 
	 lhacode = [1000037]) 
 
WC2 = 	 Parameter(name = 'WC2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WC2}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000037]) 
 
Me1 = 	 Parameter(name = 'Me1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.000511, 
	 texname = '\\text{Me1}', 
	 lhablock = 'MASS', 
	 lhacode = [11]) 
 
Me2 = 	 Parameter(name = 'Me2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.105, 
	 texname = '\\text{Me2}', 
	 lhablock = 'MASS', 
	 lhacode = [13]) 
 
Me3 = 	 Parameter(name = 'Me3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1.776, 
	 texname = '\\text{Me3}', 
	 lhablock = 'MASS', 
	 lhacode = [15]) 
 
Md1 = 	 Parameter(name = 'Md1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0035, 
	 texname = '\\text{Md1}', 
	 lhablock = 'MASS', 
	 lhacode = [1]) 
 
Md2 = 	 Parameter(name = 'Md2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.104, 
	 texname = '\\text{Md2}', 
	 lhablock = 'MASS', 
	 lhacode = [3]) 
 
Md3 = 	 Parameter(name = 'Md3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 4.2, 
	 texname = '\\text{Md3}', 
	 lhablock = 'MASS', 
	 lhacode = [5]) 
 
Mu1 = 	 Parameter(name = 'Mu1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0015, 
	 texname = '\\text{Mu1}', 
	 lhablock = 'MASS', 
	 lhacode = [2]) 
 
Mu2 = 	 Parameter(name = 'Mu2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1.27, 
	 texname = '\\text{Mu2}', 
	 lhablock = 'MASS', 
	 lhacode = [4]) 
 
Mu3 = 	 Parameter(name = 'Mu3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 171.2, 
	 texname = '\\text{Mu3}', 
	 lhablock = 'MASS', 
	 lhacode = [6]) 
 
Wu3 = 	 Parameter(name = 'Wu3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1.51, 
	 texname = '\\text{Wu3}', 
	 lhablock = 'DECAY', 
	 lhacode = [6]) 
 
Msd1 = 	 Parameter(name = 'Msd1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msd1}', 
	 lhablock = 'MASS', 
	 lhacode = [1000001]) 
 
Wsd1 = 	 Parameter(name = 'Wsd1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsd1}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000001]) 
 
Msd2 = 	 Parameter(name = 'Msd2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msd2}', 
	 lhablock = 'MASS', 
	 lhacode = [1000003]) 
 
Wsd2 = 	 Parameter(name = 'Wsd2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsd2}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000003]) 
 
Msd3 = 	 Parameter(name = 'Msd3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msd3}', 
	 lhablock = 'MASS', 
	 lhacode = [1000005]) 
 
Wsd3 = 	 Parameter(name = 'Wsd3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsd3}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000005]) 
 
Msd4 = 	 Parameter(name = 'Msd4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msd4}', 
	 lhablock = 'MASS', 
	 lhacode = [2000001]) 
 
Wsd4 = 	 Parameter(name = 'Wsd4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsd4}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000001]) 
 
Msd5 = 	 Parameter(name = 'Msd5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msd5}', 
	 lhablock = 'MASS', 
	 lhacode = [2000003]) 
 
Wsd5 = 	 Parameter(name = 'Wsd5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsd5}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000003]) 
 
Msd6 = 	 Parameter(name = 'Msd6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msd6}', 
	 lhablock = 'MASS', 
	 lhacode = [2000005]) 
 
Wsd6 = 	 Parameter(name = 'Wsd6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsd6}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000005]) 
 
Msv1 = 	 Parameter(name = 'Msv1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msv1}', 
	 lhablock = 'MASS', 
	 lhacode = [1000012]) 
 
Wsv1 = 	 Parameter(name = 'Wsv1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsv1}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000012]) 
 
Msv2 = 	 Parameter(name = 'Msv2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msv2}', 
	 lhablock = 'MASS', 
	 lhacode = [1000014]) 
 
Wsv2 = 	 Parameter(name = 'Wsv2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsv2}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000014]) 
 
Msv3 = 	 Parameter(name = 'Msv3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msv3}', 
	 lhablock = 'MASS', 
	 lhacode = [1000016]) 
 
Wsv3 = 	 Parameter(name = 'Wsv3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsv3}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000016]) 
 
Msu1 = 	 Parameter(name = 'Msu1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msu1}', 
	 lhablock = 'MASS', 
	 lhacode = [1000002]) 
 
Wsu1 = 	 Parameter(name = 'Wsu1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsu1}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000002]) 
 
Msu2 = 	 Parameter(name = 'Msu2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msu2}', 
	 lhablock = 'MASS', 
	 lhacode = [1000004]) 
 
Wsu2 = 	 Parameter(name = 'Wsu2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsu2}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000004]) 
 
Msu3 = 	 Parameter(name = 'Msu3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msu3}', 
	 lhablock = 'MASS', 
	 lhacode = [1000006]) 
 
Wsu3 = 	 Parameter(name = 'Wsu3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsu3}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000006]) 
 
Msu4 = 	 Parameter(name = 'Msu4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msu4}', 
	 lhablock = 'MASS', 
	 lhacode = [2000002]) 
 
Wsu4 = 	 Parameter(name = 'Wsu4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsu4}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000002]) 
 
Msu5 = 	 Parameter(name = 'Msu5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msu5}', 
	 lhablock = 'MASS', 
	 lhacode = [2000004]) 
 
Wsu5 = 	 Parameter(name = 'Wsu5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsu5}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000004]) 
 
Msu6 = 	 Parameter(name = 'Msu6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Msu6}', 
	 lhablock = 'MASS', 
	 lhacode = [2000006]) 
 
Wsu6 = 	 Parameter(name = 'Wsu6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wsu6}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000006]) 
 
Mse1 = 	 Parameter(name = 'Mse1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mse1}', 
	 lhablock = 'MASS', 
	 lhacode = [1000011]) 
 
Wse1 = 	 Parameter(name = 'Wse1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wse1}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000011]) 
 
Mse2 = 	 Parameter(name = 'Mse2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mse2}', 
	 lhablock = 'MASS', 
	 lhacode = [1000013]) 
 
Wse2 = 	 Parameter(name = 'Wse2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wse2}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000013]) 
 
Mse3 = 	 Parameter(name = 'Mse3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mse3}', 
	 lhablock = 'MASS', 
	 lhacode = [1000015]) 
 
Wse3 = 	 Parameter(name = 'Wse3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wse3}', 
	 lhablock = 'DECAY', 
	 lhacode = [1000015]) 
 
Mse4 = 	 Parameter(name = 'Mse4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mse4}', 
	 lhablock = 'MASS', 
	 lhacode = [2000011]) 
 
Wse4 = 	 Parameter(name = 'Wse4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wse4}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000011]) 
 
Mse5 = 	 Parameter(name = 'Mse5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mse5}', 
	 lhablock = 'MASS', 
	 lhacode = [2000013]) 
 
Wse5 = 	 Parameter(name = 'Wse5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wse5}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000013]) 
 
Mse6 = 	 Parameter(name = 'Mse6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mse6}', 
	 lhablock = 'MASS', 
	 lhacode = [2000015]) 
 
Wse6 = 	 Parameter(name = 'Wse6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wse6}', 
	 lhablock = 'DECAY', 
	 lhacode = [2000015]) 
 
Mh1 = 	 Parameter(name = 'Mh1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mh1}', 
	 lhablock = 'MASS', 
	 lhacode = [25]) 
 
Wh1 = 	 Parameter(name = 'Wh1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wh1}', 
	 lhablock = 'DECAY', 
	 lhacode = [25]) 
 
Mh2 = 	 Parameter(name = 'Mh2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{Mh2}', 
	 lhablock = 'MASS', 
	 lhacode = [35]) 
 
Wh2 = 	 Parameter(name = 'Wh2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wh2}', 
	 lhablock = 'DECAY', 
	 lhacode = [35]) 
 
MAh2 = 	 Parameter(name = 'MAh2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MAh2}', 
	 lhablock = 'MASS', 
	 lhacode = [36]) 
 
WAh2 = 	 Parameter(name = 'WAh2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WAh2}', 
	 lhablock = 'DECAY', 
	 lhacode = [36]) 
 
MHm2 = 	 Parameter(name = 'MHm2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 100., 
	 texname = '\\text{MHm2}', 
	 lhablock = 'MASS', 
	 lhacode = [37]) 
 
WHm2 = 	 Parameter(name = 'WHm2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WHm2}', 
	 lhablock = 'DECAY', 
	 lhacode = [37]) 
 
MZ = 	 Parameter(name = 'MZ', 
	 nature = 'external', 
	 type = 'real', 
	 value = 91.1876, 
	 texname = '\\text{MZ}', 
	 lhablock = 'MASS', 
	 lhacode = [23]) 
 
WZ = 	 Parameter(name = 'WZ', 
	 nature = 'external', 
	 type = 'real', 
	 value = 2.4952, 
	 texname = '\\text{WZ}', 
	 lhablock = 'DECAY', 
	 lhacode = [23]) 
 
WWm = 	 Parameter(name = 'WWm', 
	 nature = 'external', 
	 type = 'real', 
	 value = 2.141, 
	 texname = '\\text{WWm}', 
	 lhablock = 'DECAY', 
	 lhacode = [24]) 
 
rMu = 	 Parameter(name='rMu', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Mu}', 
	 lhablock = 'HMIX', 
	 lhacode = [1] ) 
 
iMu = 	 Parameter(name='iMu', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Mu}', 
	 lhablock = 'IMHMIX', 
	 lhacode = [1] ) 
 
rYd11 = 	 Parameter(name='rYd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd11}', 
	 lhablock = 'YD', 
	 lhacode = [1, 1] ) 
 
iYd11 = 	 Parameter(name='iYd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd11}', 
	 lhablock = 'IMYD', 
	 lhacode = [1, 1] ) 
 
rYd12 = 	 Parameter(name='rYd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd12}', 
	 lhablock = 'YD', 
	 lhacode = [1, 2] ) 
 
iYd12 = 	 Parameter(name='iYd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd12}', 
	 lhablock = 'IMYD', 
	 lhacode = [1, 2] ) 
 
rYd13 = 	 Parameter(name='rYd13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd13}', 
	 lhablock = 'YD', 
	 lhacode = [1, 3] ) 
 
iYd13 = 	 Parameter(name='iYd13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd13}', 
	 lhablock = 'IMYD', 
	 lhacode = [1, 3] ) 
 
rYd21 = 	 Parameter(name='rYd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd21}', 
	 lhablock = 'YD', 
	 lhacode = [2, 1] ) 
 
iYd21 = 	 Parameter(name='iYd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd21}', 
	 lhablock = 'IMYD', 
	 lhacode = [2, 1] ) 
 
rYd22 = 	 Parameter(name='rYd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd22}', 
	 lhablock = 'YD', 
	 lhacode = [2, 2] ) 
 
iYd22 = 	 Parameter(name='iYd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd22}', 
	 lhablock = 'IMYD', 
	 lhacode = [2, 2] ) 
 
rYd23 = 	 Parameter(name='rYd23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd23}', 
	 lhablock = 'YD', 
	 lhacode = [2, 3] ) 
 
iYd23 = 	 Parameter(name='iYd23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd23}', 
	 lhablock = 'IMYD', 
	 lhacode = [2, 3] ) 
 
rYd31 = 	 Parameter(name='rYd31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd31}', 
	 lhablock = 'YD', 
	 lhacode = [3, 1] ) 
 
iYd31 = 	 Parameter(name='iYd31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd31}', 
	 lhablock = 'IMYD', 
	 lhacode = [3, 1] ) 
 
rYd32 = 	 Parameter(name='rYd32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd32}', 
	 lhablock = 'YD', 
	 lhacode = [3, 2] ) 
 
iYd32 = 	 Parameter(name='iYd32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd32}', 
	 lhablock = 'IMYD', 
	 lhacode = [3, 2] ) 
 
rYd33 = 	 Parameter(name='rYd33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd33}', 
	 lhablock = 'YD', 
	 lhacode = [3, 3] ) 
 
iYd33 = 	 Parameter(name='iYd33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yd33}', 
	 lhablock = 'IMYD', 
	 lhacode = [3, 3] ) 
 
rTd11 = 	 Parameter(name='rTd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td11}', 
	 lhablock = 'TD', 
	 lhacode = [1, 1] ) 
 
iTd11 = 	 Parameter(name='iTd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td11}', 
	 lhablock = 'IMTD', 
	 lhacode = [1, 1] ) 
 
rTd12 = 	 Parameter(name='rTd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td12}', 
	 lhablock = 'TD', 
	 lhacode = [1, 2] ) 
 
iTd12 = 	 Parameter(name='iTd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td12}', 
	 lhablock = 'IMTD', 
	 lhacode = [1, 2] ) 
 
rTd13 = 	 Parameter(name='rTd13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td13}', 
	 lhablock = 'TD', 
	 lhacode = [1, 3] ) 
 
iTd13 = 	 Parameter(name='iTd13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td13}', 
	 lhablock = 'IMTD', 
	 lhacode = [1, 3] ) 
 
rTd21 = 	 Parameter(name='rTd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td21}', 
	 lhablock = 'TD', 
	 lhacode = [2, 1] ) 
 
iTd21 = 	 Parameter(name='iTd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td21}', 
	 lhablock = 'IMTD', 
	 lhacode = [2, 1] ) 
 
rTd22 = 	 Parameter(name='rTd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td22}', 
	 lhablock = 'TD', 
	 lhacode = [2, 2] ) 
 
iTd22 = 	 Parameter(name='iTd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td22}', 
	 lhablock = 'IMTD', 
	 lhacode = [2, 2] ) 
 
rTd23 = 	 Parameter(name='rTd23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td23}', 
	 lhablock = 'TD', 
	 lhacode = [2, 3] ) 
 
iTd23 = 	 Parameter(name='iTd23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td23}', 
	 lhablock = 'IMTD', 
	 lhacode = [2, 3] ) 
 
rTd31 = 	 Parameter(name='rTd31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td31}', 
	 lhablock = 'TD', 
	 lhacode = [3, 1] ) 
 
iTd31 = 	 Parameter(name='iTd31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td31}', 
	 lhablock = 'IMTD', 
	 lhacode = [3, 1] ) 
 
rTd32 = 	 Parameter(name='rTd32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td32}', 
	 lhablock = 'TD', 
	 lhacode = [3, 2] ) 
 
iTd32 = 	 Parameter(name='iTd32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td32}', 
	 lhablock = 'IMTD', 
	 lhacode = [3, 2] ) 
 
rTd33 = 	 Parameter(name='rTd33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td33}', 
	 lhablock = 'TD', 
	 lhacode = [3, 3] ) 
 
iTd33 = 	 Parameter(name='iTd33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Td33}', 
	 lhablock = 'IMTD', 
	 lhacode = [3, 3] ) 
 
rYe11 = 	 Parameter(name='rYe11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye11}', 
	 lhablock = 'YE', 
	 lhacode = [1, 1] ) 
 
iYe11 = 	 Parameter(name='iYe11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye11}', 
	 lhablock = 'IMYE', 
	 lhacode = [1, 1] ) 
 
rYe12 = 	 Parameter(name='rYe12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye12}', 
	 lhablock = 'YE', 
	 lhacode = [1, 2] ) 
 
iYe12 = 	 Parameter(name='iYe12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye12}', 
	 lhablock = 'IMYE', 
	 lhacode = [1, 2] ) 
 
rYe13 = 	 Parameter(name='rYe13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye13}', 
	 lhablock = 'YE', 
	 lhacode = [1, 3] ) 
 
iYe13 = 	 Parameter(name='iYe13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye13}', 
	 lhablock = 'IMYE', 
	 lhacode = [1, 3] ) 
 
rYe21 = 	 Parameter(name='rYe21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye21}', 
	 lhablock = 'YE', 
	 lhacode = [2, 1] ) 
 
iYe21 = 	 Parameter(name='iYe21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye21}', 
	 lhablock = 'IMYE', 
	 lhacode = [2, 1] ) 
 
rYe22 = 	 Parameter(name='rYe22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye22}', 
	 lhablock = 'YE', 
	 lhacode = [2, 2] ) 
 
iYe22 = 	 Parameter(name='iYe22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye22}', 
	 lhablock = 'IMYE', 
	 lhacode = [2, 2] ) 
 
rYe23 = 	 Parameter(name='rYe23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye23}', 
	 lhablock = 'YE', 
	 lhacode = [2, 3] ) 
 
iYe23 = 	 Parameter(name='iYe23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye23}', 
	 lhablock = 'IMYE', 
	 lhacode = [2, 3] ) 
 
rYe31 = 	 Parameter(name='rYe31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye31}', 
	 lhablock = 'YE', 
	 lhacode = [3, 1] ) 
 
iYe31 = 	 Parameter(name='iYe31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye31}', 
	 lhablock = 'IMYE', 
	 lhacode = [3, 1] ) 
 
rYe32 = 	 Parameter(name='rYe32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye32}', 
	 lhablock = 'YE', 
	 lhacode = [3, 2] ) 
 
iYe32 = 	 Parameter(name='iYe32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye32}', 
	 lhablock = 'IMYE', 
	 lhacode = [3, 2] ) 
 
rYe33 = 	 Parameter(name='rYe33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye33}', 
	 lhablock = 'YE', 
	 lhacode = [3, 3] ) 
 
iYe33 = 	 Parameter(name='iYe33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Ye33}', 
	 lhablock = 'IMYE', 
	 lhacode = [3, 3] ) 
 
rTe11 = 	 Parameter(name='rTe11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te11}', 
	 lhablock = 'TE', 
	 lhacode = [1, 1] ) 
 
iTe11 = 	 Parameter(name='iTe11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te11}', 
	 lhablock = 'IMTE', 
	 lhacode = [1, 1] ) 
 
rTe12 = 	 Parameter(name='rTe12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te12}', 
	 lhablock = 'TE', 
	 lhacode = [1, 2] ) 
 
iTe12 = 	 Parameter(name='iTe12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te12}', 
	 lhablock = 'IMTE', 
	 lhacode = [1, 2] ) 
 
rTe13 = 	 Parameter(name='rTe13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te13}', 
	 lhablock = 'TE', 
	 lhacode = [1, 3] ) 
 
iTe13 = 	 Parameter(name='iTe13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te13}', 
	 lhablock = 'IMTE', 
	 lhacode = [1, 3] ) 
 
rTe21 = 	 Parameter(name='rTe21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te21}', 
	 lhablock = 'TE', 
	 lhacode = [2, 1] ) 
 
iTe21 = 	 Parameter(name='iTe21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te21}', 
	 lhablock = 'IMTE', 
	 lhacode = [2, 1] ) 
 
rTe22 = 	 Parameter(name='rTe22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te22}', 
	 lhablock = 'TE', 
	 lhacode = [2, 2] ) 
 
iTe22 = 	 Parameter(name='iTe22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te22}', 
	 lhablock = 'IMTE', 
	 lhacode = [2, 2] ) 
 
rTe23 = 	 Parameter(name='rTe23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te23}', 
	 lhablock = 'TE', 
	 lhacode = [2, 3] ) 
 
iTe23 = 	 Parameter(name='iTe23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te23}', 
	 lhablock = 'IMTE', 
	 lhacode = [2, 3] ) 
 
rTe31 = 	 Parameter(name='rTe31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te31}', 
	 lhablock = 'TE', 
	 lhacode = [3, 1] ) 
 
iTe31 = 	 Parameter(name='iTe31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te31}', 
	 lhablock = 'IMTE', 
	 lhacode = [3, 1] ) 
 
rTe32 = 	 Parameter(name='rTe32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te32}', 
	 lhablock = 'TE', 
	 lhacode = [3, 2] ) 
 
iTe32 = 	 Parameter(name='iTe32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te32}', 
	 lhablock = 'IMTE', 
	 lhacode = [3, 2] ) 
 
rTe33 = 	 Parameter(name='rTe33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te33}', 
	 lhablock = 'TE', 
	 lhacode = [3, 3] ) 
 
iTe33 = 	 Parameter(name='iTe33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Te33}', 
	 lhablock = 'IMTE', 
	 lhacode = [3, 3] ) 
 
rL1111 = 	 Parameter(name='rL1111', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1111}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [1, 1, 1] ) 
 
iL1111 = 	 Parameter(name='iL1111', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1111}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [1, 1, 1] ) 
 
rL1112 = 	 Parameter(name='rL1112', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1112}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [1, 1, 2] ) 
 
iL1112 = 	 Parameter(name='iL1112', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1112}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [1, 1, 2] ) 
 
rL1113 = 	 Parameter(name='rL1113', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1113}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [1, 1, 3] ) 
 
iL1113 = 	 Parameter(name='iL1113', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1113}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [1, 1, 3] ) 
 
rL1121 = 	 Parameter(name='rL1121', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1121}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [1, 2, 1] ) 
 
iL1121 = 	 Parameter(name='iL1121', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1121}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [1, 2, 1] ) 
 
rL1122 = 	 Parameter(name='rL1122', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1122}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [1, 2, 2] ) 
 
iL1122 = 	 Parameter(name='iL1122', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1122}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [1, 2, 2] ) 
 
rL1123 = 	 Parameter(name='rL1123', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1123}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [1, 2, 3] ) 
 
iL1123 = 	 Parameter(name='iL1123', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1123}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [1, 2, 3] ) 
 
rL1131 = 	 Parameter(name='rL1131', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1131}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [1, 3, 1] ) 
 
iL1131 = 	 Parameter(name='iL1131', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1131}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [1, 3, 1] ) 
 
rL1132 = 	 Parameter(name='rL1132', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1132}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [1, 3, 2] ) 
 
iL1132 = 	 Parameter(name='iL1132', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1132}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [1, 3, 2] ) 
 
rL1133 = 	 Parameter(name='rL1133', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1133}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [1, 3, 3] ) 
 
iL1133 = 	 Parameter(name='iL1133', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1133}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [1, 3, 3] ) 
 
rL1211 = 	 Parameter(name='rL1211', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1211}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [2, 1, 1] ) 
 
iL1211 = 	 Parameter(name='iL1211', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1211}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [2, 1, 1] ) 
 
rL1212 = 	 Parameter(name='rL1212', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1212}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [2, 1, 2] ) 
 
iL1212 = 	 Parameter(name='iL1212', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1212}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [2, 1, 2] ) 
 
rL1213 = 	 Parameter(name='rL1213', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1213}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [2, 1, 3] ) 
 
iL1213 = 	 Parameter(name='iL1213', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1213}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [2, 1, 3] ) 
 
rL1221 = 	 Parameter(name='rL1221', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1221}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [2, 2, 1] ) 
 
iL1221 = 	 Parameter(name='iL1221', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1221}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [2, 2, 1] ) 
 
rL1222 = 	 Parameter(name='rL1222', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1222}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [2, 2, 2] ) 
 
iL1222 = 	 Parameter(name='iL1222', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1222}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [2, 2, 2] ) 
 
rL1223 = 	 Parameter(name='rL1223', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1223}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [2, 2, 3] ) 
 
iL1223 = 	 Parameter(name='iL1223', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1223}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [2, 2, 3] ) 
 
rL1231 = 	 Parameter(name='rL1231', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1231}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [2, 3, 1] ) 
 
iL1231 = 	 Parameter(name='iL1231', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1231}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [2, 3, 1] ) 
 
rL1232 = 	 Parameter(name='rL1232', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1232}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [2, 3, 2] ) 
 
iL1232 = 	 Parameter(name='iL1232', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1232}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [2, 3, 2] ) 
 
rL1233 = 	 Parameter(name='rL1233', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1233}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [2, 3, 3] ) 
 
iL1233 = 	 Parameter(name='iL1233', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1233}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [2, 3, 3] ) 
 
rL1311 = 	 Parameter(name='rL1311', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1311}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [3, 1, 1] ) 
 
iL1311 = 	 Parameter(name='iL1311', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1311}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [3, 1, 1] ) 
 
rL1312 = 	 Parameter(name='rL1312', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1312}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [3, 1, 2] ) 
 
iL1312 = 	 Parameter(name='iL1312', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1312}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [3, 1, 2] ) 
 
rL1313 = 	 Parameter(name='rL1313', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1313}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [3, 1, 3] ) 
 
iL1313 = 	 Parameter(name='iL1313', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1313}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [3, 1, 3] ) 
 
rL1321 = 	 Parameter(name='rL1321', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1321}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [3, 2, 1] ) 
 
iL1321 = 	 Parameter(name='iL1321', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1321}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [3, 2, 1] ) 
 
rL1322 = 	 Parameter(name='rL1322', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1322}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [3, 2, 2] ) 
 
iL1322 = 	 Parameter(name='iL1322', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1322}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [3, 2, 2] ) 
 
rL1323 = 	 Parameter(name='rL1323', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1323}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [3, 2, 3] ) 
 
iL1323 = 	 Parameter(name='iL1323', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1323}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [3, 2, 3] ) 
 
rL1331 = 	 Parameter(name='rL1331', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1331}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [3, 3, 1] ) 
 
iL1331 = 	 Parameter(name='iL1331', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1331}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [3, 3, 1] ) 
 
rL1332 = 	 Parameter(name='rL1332', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1332}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [3, 3, 2] ) 
 
iL1332 = 	 Parameter(name='iL1332', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1332}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [3, 3, 2] ) 
 
rL1333 = 	 Parameter(name='rL1333', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1333}', 
	 lhablock = 'RVLAMLLE', 
	 lhacode = [3, 3, 3] ) 
 
iL1333 = 	 Parameter(name='iL1333', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L1333}', 
	 lhablock = 'IMRVLAMLLE', 
	 lhacode = [3, 3, 3] ) 
 
rT1111 = 	 Parameter(name='rT1111', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1111}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [1, 1, 1] ) 
 
iT1111 = 	 Parameter(name='iT1111', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1111}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [1, 1, 1] ) 
 
rT1112 = 	 Parameter(name='rT1112', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1112}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [1, 1, 2] ) 
 
iT1112 = 	 Parameter(name='iT1112', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1112}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [1, 1, 2] ) 
 
rT1113 = 	 Parameter(name='rT1113', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1113}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [1, 1, 3] ) 
 
iT1113 = 	 Parameter(name='iT1113', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1113}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [1, 1, 3] ) 
 
rT1121 = 	 Parameter(name='rT1121', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1121}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [1, 2, 1] ) 
 
iT1121 = 	 Parameter(name='iT1121', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1121}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [1, 2, 1] ) 
 
rT1122 = 	 Parameter(name='rT1122', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1122}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [1, 2, 2] ) 
 
iT1122 = 	 Parameter(name='iT1122', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1122}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [1, 2, 2] ) 
 
rT1123 = 	 Parameter(name='rT1123', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1123}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [1, 2, 3] ) 
 
iT1123 = 	 Parameter(name='iT1123', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1123}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [1, 2, 3] ) 
 
rT1131 = 	 Parameter(name='rT1131', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1131}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [1, 3, 1] ) 
 
iT1131 = 	 Parameter(name='iT1131', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1131}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [1, 3, 1] ) 
 
rT1132 = 	 Parameter(name='rT1132', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1132}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [1, 3, 2] ) 
 
iT1132 = 	 Parameter(name='iT1132', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1132}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [1, 3, 2] ) 
 
rT1133 = 	 Parameter(name='rT1133', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1133}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [1, 3, 3] ) 
 
iT1133 = 	 Parameter(name='iT1133', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1133}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [1, 3, 3] ) 
 
rT1211 = 	 Parameter(name='rT1211', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1211}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [2, 1, 1] ) 
 
iT1211 = 	 Parameter(name='iT1211', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1211}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [2, 1, 1] ) 
 
rT1212 = 	 Parameter(name='rT1212', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1212}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [2, 1, 2] ) 
 
iT1212 = 	 Parameter(name='iT1212', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1212}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [2, 1, 2] ) 
 
rT1213 = 	 Parameter(name='rT1213', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1213}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [2, 1, 3] ) 
 
iT1213 = 	 Parameter(name='iT1213', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1213}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [2, 1, 3] ) 
 
rT1221 = 	 Parameter(name='rT1221', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1221}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [2, 2, 1] ) 
 
iT1221 = 	 Parameter(name='iT1221', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1221}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [2, 2, 1] ) 
 
rT1222 = 	 Parameter(name='rT1222', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1222}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [2, 2, 2] ) 
 
iT1222 = 	 Parameter(name='iT1222', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1222}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [2, 2, 2] ) 
 
rT1223 = 	 Parameter(name='rT1223', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1223}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [2, 2, 3] ) 
 
iT1223 = 	 Parameter(name='iT1223', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1223}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [2, 2, 3] ) 
 
rT1231 = 	 Parameter(name='rT1231', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1231}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [2, 3, 1] ) 
 
iT1231 = 	 Parameter(name='iT1231', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1231}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [2, 3, 1] ) 
 
rT1232 = 	 Parameter(name='rT1232', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1232}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [2, 3, 2] ) 
 
iT1232 = 	 Parameter(name='iT1232', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1232}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [2, 3, 2] ) 
 
rT1233 = 	 Parameter(name='rT1233', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1233}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [2, 3, 3] ) 
 
iT1233 = 	 Parameter(name='iT1233', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1233}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [2, 3, 3] ) 
 
rT1311 = 	 Parameter(name='rT1311', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1311}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [3, 1, 1] ) 
 
iT1311 = 	 Parameter(name='iT1311', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1311}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [3, 1, 1] ) 
 
rT1312 = 	 Parameter(name='rT1312', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1312}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [3, 1, 2] ) 
 
iT1312 = 	 Parameter(name='iT1312', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1312}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [3, 1, 2] ) 
 
rT1313 = 	 Parameter(name='rT1313', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1313}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [3, 1, 3] ) 
 
iT1313 = 	 Parameter(name='iT1313', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1313}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [3, 1, 3] ) 
 
rT1321 = 	 Parameter(name='rT1321', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1321}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [3, 2, 1] ) 
 
iT1321 = 	 Parameter(name='iT1321', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1321}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [3, 2, 1] ) 
 
rT1322 = 	 Parameter(name='rT1322', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1322}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [3, 2, 2] ) 
 
iT1322 = 	 Parameter(name='iT1322', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1322}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [3, 2, 2] ) 
 
rT1323 = 	 Parameter(name='rT1323', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1323}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [3, 2, 3] ) 
 
iT1323 = 	 Parameter(name='iT1323', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1323}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [3, 2, 3] ) 
 
rT1331 = 	 Parameter(name='rT1331', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1331}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [3, 3, 1] ) 
 
iT1331 = 	 Parameter(name='iT1331', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1331}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [3, 3, 1] ) 
 
rT1332 = 	 Parameter(name='rT1332', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1332}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [3, 3, 2] ) 
 
iT1332 = 	 Parameter(name='iT1332', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1332}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [3, 3, 2] ) 
 
rT1333 = 	 Parameter(name='rT1333', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1333}', 
	 lhablock = 'RVTLLE', 
	 lhacode = [3, 3, 3] ) 
 
iT1333 = 	 Parameter(name='iT1333', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T1333}', 
	 lhablock = 'IMRVTLLE', 
	 lhacode = [3, 3, 3] ) 
 
rL2111 = 	 Parameter(name='rL2111', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2111}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [1, 1, 1] ) 
 
iL2111 = 	 Parameter(name='iL2111', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2111}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [1, 1, 1] ) 
 
rL2112 = 	 Parameter(name='rL2112', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2112}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [1, 1, 2] ) 
 
iL2112 = 	 Parameter(name='iL2112', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2112}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [1, 1, 2] ) 
 
rL2113 = 	 Parameter(name='rL2113', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2113}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [1, 1, 3] ) 
 
iL2113 = 	 Parameter(name='iL2113', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2113}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [1, 1, 3] ) 
 
rL2121 = 	 Parameter(name='rL2121', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2121}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [1, 2, 1] ) 
 
iL2121 = 	 Parameter(name='iL2121', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2121}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [1, 2, 1] ) 
 
rL2122 = 	 Parameter(name='rL2122', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2122}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [1, 2, 2] ) 
 
iL2122 = 	 Parameter(name='iL2122', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2122}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [1, 2, 2] ) 
 
rL2123 = 	 Parameter(name='rL2123', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2123}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [1, 2, 3] ) 
 
iL2123 = 	 Parameter(name='iL2123', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2123}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [1, 2, 3] ) 
 
rL2131 = 	 Parameter(name='rL2131', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2131}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [1, 3, 1] ) 
 
iL2131 = 	 Parameter(name='iL2131', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2131}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [1, 3, 1] ) 
 
rL2132 = 	 Parameter(name='rL2132', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2132}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [1, 3, 2] ) 
 
iL2132 = 	 Parameter(name='iL2132', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2132}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [1, 3, 2] ) 
 
rL2133 = 	 Parameter(name='rL2133', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2133}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [1, 3, 3] ) 
 
iL2133 = 	 Parameter(name='iL2133', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2133}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [1, 3, 3] ) 
 
rL2211 = 	 Parameter(name='rL2211', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2211}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [2, 1, 1] ) 
 
iL2211 = 	 Parameter(name='iL2211', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2211}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [2, 1, 1] ) 
 
rL2212 = 	 Parameter(name='rL2212', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2212}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [2, 1, 2] ) 
 
iL2212 = 	 Parameter(name='iL2212', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2212}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [2, 1, 2] ) 
 
rL2213 = 	 Parameter(name='rL2213', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2213}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [2, 1, 3] ) 
 
iL2213 = 	 Parameter(name='iL2213', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2213}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [2, 1, 3] ) 
 
rL2221 = 	 Parameter(name='rL2221', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2221}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [2, 2, 1] ) 
 
iL2221 = 	 Parameter(name='iL2221', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2221}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [2, 2, 1] ) 
 
rL2222 = 	 Parameter(name='rL2222', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2222}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [2, 2, 2] ) 
 
iL2222 = 	 Parameter(name='iL2222', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2222}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [2, 2, 2] ) 
 
rL2223 = 	 Parameter(name='rL2223', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2223}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [2, 2, 3] ) 
 
iL2223 = 	 Parameter(name='iL2223', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2223}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [2, 2, 3] ) 
 
rL2231 = 	 Parameter(name='rL2231', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2231}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [2, 3, 1] ) 
 
iL2231 = 	 Parameter(name='iL2231', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2231}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [2, 3, 1] ) 
 
rL2232 = 	 Parameter(name='rL2232', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2232}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [2, 3, 2] ) 
 
iL2232 = 	 Parameter(name='iL2232', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2232}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [2, 3, 2] ) 
 
rL2233 = 	 Parameter(name='rL2233', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2233}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [2, 3, 3] ) 
 
iL2233 = 	 Parameter(name='iL2233', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2233}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [2, 3, 3] ) 
 
rL2311 = 	 Parameter(name='rL2311', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2311}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [3, 1, 1] ) 
 
iL2311 = 	 Parameter(name='iL2311', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2311}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [3, 1, 1] ) 
 
rL2312 = 	 Parameter(name='rL2312', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2312}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [3, 1, 2] ) 
 
iL2312 = 	 Parameter(name='iL2312', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2312}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [3, 1, 2] ) 
 
rL2313 = 	 Parameter(name='rL2313', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2313}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [3, 1, 3] ) 
 
iL2313 = 	 Parameter(name='iL2313', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2313}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [3, 1, 3] ) 
 
rL2321 = 	 Parameter(name='rL2321', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2321}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [3, 2, 1] ) 
 
iL2321 = 	 Parameter(name='iL2321', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2321}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [3, 2, 1] ) 
 
rL2322 = 	 Parameter(name='rL2322', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2322}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [3, 2, 2] ) 
 
iL2322 = 	 Parameter(name='iL2322', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2322}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [3, 2, 2] ) 
 
rL2323 = 	 Parameter(name='rL2323', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2323}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [3, 2, 3] ) 
 
iL2323 = 	 Parameter(name='iL2323', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2323}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [3, 2, 3] ) 
 
rL2331 = 	 Parameter(name='rL2331', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2331}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [3, 3, 1] ) 
 
iL2331 = 	 Parameter(name='iL2331', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2331}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [3, 3, 1] ) 
 
rL2332 = 	 Parameter(name='rL2332', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2332}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [3, 3, 2] ) 
 
iL2332 = 	 Parameter(name='iL2332', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2332}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [3, 3, 2] ) 
 
rL2333 = 	 Parameter(name='rL2333', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2333}', 
	 lhablock = 'RVLAMLQD', 
	 lhacode = [3, 3, 3] ) 
 
iL2333 = 	 Parameter(name='iL2333', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L2333}', 
	 lhablock = 'IMRVLAMLQD', 
	 lhacode = [3, 3, 3] ) 
 
rT2111 = 	 Parameter(name='rT2111', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2111}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [1, 1, 1] ) 
 
iT2111 = 	 Parameter(name='iT2111', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2111}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [1, 1, 1] ) 
 
rT2112 = 	 Parameter(name='rT2112', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2112}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [1, 1, 2] ) 
 
iT2112 = 	 Parameter(name='iT2112', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2112}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [1, 1, 2] ) 
 
rT2113 = 	 Parameter(name='rT2113', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2113}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [1, 1, 3] ) 
 
iT2113 = 	 Parameter(name='iT2113', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2113}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [1, 1, 3] ) 
 
rT2121 = 	 Parameter(name='rT2121', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2121}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [1, 2, 1] ) 
 
iT2121 = 	 Parameter(name='iT2121', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2121}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [1, 2, 1] ) 
 
rT2122 = 	 Parameter(name='rT2122', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2122}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [1, 2, 2] ) 
 
iT2122 = 	 Parameter(name='iT2122', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2122}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [1, 2, 2] ) 
 
rT2123 = 	 Parameter(name='rT2123', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2123}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [1, 2, 3] ) 
 
iT2123 = 	 Parameter(name='iT2123', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2123}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [1, 2, 3] ) 
 
rT2131 = 	 Parameter(name='rT2131', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2131}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [1, 3, 1] ) 
 
iT2131 = 	 Parameter(name='iT2131', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2131}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [1, 3, 1] ) 
 
rT2132 = 	 Parameter(name='rT2132', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2132}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [1, 3, 2] ) 
 
iT2132 = 	 Parameter(name='iT2132', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2132}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [1, 3, 2] ) 
 
rT2133 = 	 Parameter(name='rT2133', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2133}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [1, 3, 3] ) 
 
iT2133 = 	 Parameter(name='iT2133', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2133}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [1, 3, 3] ) 
 
rT2211 = 	 Parameter(name='rT2211', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2211}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [2, 1, 1] ) 
 
iT2211 = 	 Parameter(name='iT2211', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2211}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [2, 1, 1] ) 
 
rT2212 = 	 Parameter(name='rT2212', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2212}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [2, 1, 2] ) 
 
iT2212 = 	 Parameter(name='iT2212', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2212}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [2, 1, 2] ) 
 
rT2213 = 	 Parameter(name='rT2213', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2213}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [2, 1, 3] ) 
 
iT2213 = 	 Parameter(name='iT2213', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2213}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [2, 1, 3] ) 
 
rT2221 = 	 Parameter(name='rT2221', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2221}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [2, 2, 1] ) 
 
iT2221 = 	 Parameter(name='iT2221', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2221}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [2, 2, 1] ) 
 
rT2222 = 	 Parameter(name='rT2222', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2222}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [2, 2, 2] ) 
 
iT2222 = 	 Parameter(name='iT2222', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2222}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [2, 2, 2] ) 
 
rT2223 = 	 Parameter(name='rT2223', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2223}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [2, 2, 3] ) 
 
iT2223 = 	 Parameter(name='iT2223', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2223}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [2, 2, 3] ) 
 
rT2231 = 	 Parameter(name='rT2231', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2231}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [2, 3, 1] ) 
 
iT2231 = 	 Parameter(name='iT2231', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2231}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [2, 3, 1] ) 
 
rT2232 = 	 Parameter(name='rT2232', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2232}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [2, 3, 2] ) 
 
iT2232 = 	 Parameter(name='iT2232', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2232}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [2, 3, 2] ) 
 
rT2233 = 	 Parameter(name='rT2233', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2233}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [2, 3, 3] ) 
 
iT2233 = 	 Parameter(name='iT2233', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2233}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [2, 3, 3] ) 
 
rT2311 = 	 Parameter(name='rT2311', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2311}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [3, 1, 1] ) 
 
iT2311 = 	 Parameter(name='iT2311', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2311}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [3, 1, 1] ) 
 
rT2312 = 	 Parameter(name='rT2312', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2312}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [3, 1, 2] ) 
 
iT2312 = 	 Parameter(name='iT2312', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2312}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [3, 1, 2] ) 
 
rT2313 = 	 Parameter(name='rT2313', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2313}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [3, 1, 3] ) 
 
iT2313 = 	 Parameter(name='iT2313', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2313}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [3, 1, 3] ) 
 
rT2321 = 	 Parameter(name='rT2321', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2321}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [3, 2, 1] ) 
 
iT2321 = 	 Parameter(name='iT2321', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2321}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [3, 2, 1] ) 
 
rT2322 = 	 Parameter(name='rT2322', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2322}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [3, 2, 2] ) 
 
iT2322 = 	 Parameter(name='iT2322', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2322}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [3, 2, 2] ) 
 
rT2323 = 	 Parameter(name='rT2323', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2323}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [3, 2, 3] ) 
 
iT2323 = 	 Parameter(name='iT2323', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2323}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [3, 2, 3] ) 
 
rT2331 = 	 Parameter(name='rT2331', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2331}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [3, 3, 1] ) 
 
iT2331 = 	 Parameter(name='iT2331', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2331}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [3, 3, 1] ) 
 
rT2332 = 	 Parameter(name='rT2332', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2332}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [3, 3, 2] ) 
 
iT2332 = 	 Parameter(name='iT2332', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2332}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [3, 3, 2] ) 
 
rT2333 = 	 Parameter(name='rT2333', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2333}', 
	 lhablock = 'RVTLQD', 
	 lhacode = [3, 3, 3] ) 
 
iT2333 = 	 Parameter(name='iT2333', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T2333}', 
	 lhablock = 'IMRVTLQD', 
	 lhacode = [3, 3, 3] ) 
 
rL3111 = 	 Parameter(name='rL3111', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3111}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [1, 1, 1] ) 
 
iL3111 = 	 Parameter(name='iL3111', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3111}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [1, 1, 1] ) 
 
rL3112 = 	 Parameter(name='rL3112', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3112}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [1, 1, 2] ) 
 
iL3112 = 	 Parameter(name='iL3112', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3112}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [1, 1, 2] ) 
 
rL3113 = 	 Parameter(name='rL3113', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3113}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [1, 1, 3] ) 
 
iL3113 = 	 Parameter(name='iL3113', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3113}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [1, 1, 3] ) 
 
rL3121 = 	 Parameter(name='rL3121', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3121}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [1, 2, 1] ) 
 
iL3121 = 	 Parameter(name='iL3121', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3121}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [1, 2, 1] ) 
 
rL3122 = 	 Parameter(name='rL3122', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3122}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [1, 2, 2] ) 
 
iL3122 = 	 Parameter(name='iL3122', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3122}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [1, 2, 2] ) 
 
rL3123 = 	 Parameter(name='rL3123', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3123}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [1, 2, 3] ) 
 
iL3123 = 	 Parameter(name='iL3123', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3123}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [1, 2, 3] ) 
 
rL3131 = 	 Parameter(name='rL3131', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3131}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [1, 3, 1] ) 
 
iL3131 = 	 Parameter(name='iL3131', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3131}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [1, 3, 1] ) 
 
rL3132 = 	 Parameter(name='rL3132', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3132}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [1, 3, 2] ) 
 
iL3132 = 	 Parameter(name='iL3132', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3132}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [1, 3, 2] ) 
 
rL3133 = 	 Parameter(name='rL3133', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3133}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [1, 3, 3] ) 
 
iL3133 = 	 Parameter(name='iL3133', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3133}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [1, 3, 3] ) 
 
rL3211 = 	 Parameter(name='rL3211', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3211}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [2, 1, 1] ) 
 
iL3211 = 	 Parameter(name='iL3211', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3211}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [2, 1, 1] ) 
 
rL3212 = 	 Parameter(name='rL3212', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3212}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [2, 1, 2] ) 
 
iL3212 = 	 Parameter(name='iL3212', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3212}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [2, 1, 2] ) 
 
rL3213 = 	 Parameter(name='rL3213', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3213}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [2, 1, 3] ) 
 
iL3213 = 	 Parameter(name='iL3213', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3213}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [2, 1, 3] ) 
 
rL3221 = 	 Parameter(name='rL3221', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3221}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [2, 2, 1] ) 
 
iL3221 = 	 Parameter(name='iL3221', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3221}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [2, 2, 1] ) 
 
rL3222 = 	 Parameter(name='rL3222', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3222}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [2, 2, 2] ) 
 
iL3222 = 	 Parameter(name='iL3222', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3222}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [2, 2, 2] ) 
 
rL3223 = 	 Parameter(name='rL3223', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3223}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [2, 2, 3] ) 
 
iL3223 = 	 Parameter(name='iL3223', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3223}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [2, 2, 3] ) 
 
rL3231 = 	 Parameter(name='rL3231', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3231}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [2, 3, 1] ) 
 
iL3231 = 	 Parameter(name='iL3231', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3231}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [2, 3, 1] ) 
 
rL3232 = 	 Parameter(name='rL3232', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3232}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [2, 3, 2] ) 
 
iL3232 = 	 Parameter(name='iL3232', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3232}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [2, 3, 2] ) 
 
rL3233 = 	 Parameter(name='rL3233', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3233}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [2, 3, 3] ) 
 
iL3233 = 	 Parameter(name='iL3233', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3233}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [2, 3, 3] ) 
 
rL3311 = 	 Parameter(name='rL3311', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3311}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [3, 1, 1] ) 
 
iL3311 = 	 Parameter(name='iL3311', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3311}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [3, 1, 1] ) 
 
rL3312 = 	 Parameter(name='rL3312', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3312}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [3, 1, 2] ) 
 
iL3312 = 	 Parameter(name='iL3312', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3312}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [3, 1, 2] ) 
 
rL3313 = 	 Parameter(name='rL3313', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3313}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [3, 1, 3] ) 
 
iL3313 = 	 Parameter(name='iL3313', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3313}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [3, 1, 3] ) 
 
rL3321 = 	 Parameter(name='rL3321', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3321}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [3, 2, 1] ) 
 
iL3321 = 	 Parameter(name='iL3321', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3321}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [3, 2, 1] ) 
 
rL3322 = 	 Parameter(name='rL3322', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3322}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [3, 2, 2] ) 
 
iL3322 = 	 Parameter(name='iL3322', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3322}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [3, 2, 2] ) 
 
rL3323 = 	 Parameter(name='rL3323', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3323}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [3, 2, 3] ) 
 
iL3323 = 	 Parameter(name='iL3323', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3323}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [3, 2, 3] ) 
 
rL3331 = 	 Parameter(name='rL3331', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3331}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [3, 3, 1] ) 
 
iL3331 = 	 Parameter(name='iL3331', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3331}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [3, 3, 1] ) 
 
rL3332 = 	 Parameter(name='rL3332', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3332}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [3, 3, 2] ) 
 
iL3332 = 	 Parameter(name='iL3332', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3332}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [3, 3, 2] ) 
 
rL3333 = 	 Parameter(name='rL3333', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3333}', 
	 lhablock = 'RVLAMUDD', 
	 lhacode = [3, 3, 3] ) 
 
iL3333 = 	 Parameter(name='iL3333', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{L3333}', 
	 lhablock = 'IMRVLAMUDD', 
	 lhacode = [3, 3, 3] ) 
 
rT3111 = 	 Parameter(name='rT3111', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3111}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [1, 1, 1] ) 
 
iT3111 = 	 Parameter(name='iT3111', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3111}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [1, 1, 1] ) 
 
rT3112 = 	 Parameter(name='rT3112', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3112}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [1, 1, 2] ) 
 
iT3112 = 	 Parameter(name='iT3112', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3112}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [1, 1, 2] ) 
 
rT3113 = 	 Parameter(name='rT3113', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3113}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [1, 1, 3] ) 
 
iT3113 = 	 Parameter(name='iT3113', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3113}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [1, 1, 3] ) 
 
rT3121 = 	 Parameter(name='rT3121', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3121}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [1, 2, 1] ) 
 
iT3121 = 	 Parameter(name='iT3121', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3121}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [1, 2, 1] ) 
 
rT3122 = 	 Parameter(name='rT3122', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3122}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [1, 2, 2] ) 
 
iT3122 = 	 Parameter(name='iT3122', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3122}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [1, 2, 2] ) 
 
rT3123 = 	 Parameter(name='rT3123', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3123}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [1, 2, 3] ) 
 
iT3123 = 	 Parameter(name='iT3123', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3123}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [1, 2, 3] ) 
 
rT3131 = 	 Parameter(name='rT3131', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3131}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [1, 3, 1] ) 
 
iT3131 = 	 Parameter(name='iT3131', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3131}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [1, 3, 1] ) 
 
rT3132 = 	 Parameter(name='rT3132', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3132}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [1, 3, 2] ) 
 
iT3132 = 	 Parameter(name='iT3132', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3132}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [1, 3, 2] ) 
 
rT3133 = 	 Parameter(name='rT3133', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3133}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [1, 3, 3] ) 
 
iT3133 = 	 Parameter(name='iT3133', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3133}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [1, 3, 3] ) 
 
rT3211 = 	 Parameter(name='rT3211', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3211}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [2, 1, 1] ) 
 
iT3211 = 	 Parameter(name='iT3211', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3211}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [2, 1, 1] ) 
 
rT3212 = 	 Parameter(name='rT3212', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3212}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [2, 1, 2] ) 
 
iT3212 = 	 Parameter(name='iT3212', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3212}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [2, 1, 2] ) 
 
rT3213 = 	 Parameter(name='rT3213', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3213}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [2, 1, 3] ) 
 
iT3213 = 	 Parameter(name='iT3213', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3213}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [2, 1, 3] ) 
 
rT3221 = 	 Parameter(name='rT3221', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3221}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [2, 2, 1] ) 
 
iT3221 = 	 Parameter(name='iT3221', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3221}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [2, 2, 1] ) 
 
rT3222 = 	 Parameter(name='rT3222', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3222}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [2, 2, 2] ) 
 
iT3222 = 	 Parameter(name='iT3222', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3222}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [2, 2, 2] ) 
 
rT3223 = 	 Parameter(name='rT3223', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3223}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [2, 2, 3] ) 
 
iT3223 = 	 Parameter(name='iT3223', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3223}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [2, 2, 3] ) 
 
rT3231 = 	 Parameter(name='rT3231', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3231}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [2, 3, 1] ) 
 
iT3231 = 	 Parameter(name='iT3231', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3231}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [2, 3, 1] ) 
 
rT3232 = 	 Parameter(name='rT3232', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3232}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [2, 3, 2] ) 
 
iT3232 = 	 Parameter(name='iT3232', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3232}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [2, 3, 2] ) 
 
rT3233 = 	 Parameter(name='rT3233', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3233}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [2, 3, 3] ) 
 
iT3233 = 	 Parameter(name='iT3233', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3233}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [2, 3, 3] ) 
 
rT3311 = 	 Parameter(name='rT3311', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3311}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [3, 1, 1] ) 
 
iT3311 = 	 Parameter(name='iT3311', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3311}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [3, 1, 1] ) 
 
rT3312 = 	 Parameter(name='rT3312', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3312}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [3, 1, 2] ) 
 
iT3312 = 	 Parameter(name='iT3312', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3312}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [3, 1, 2] ) 
 
rT3313 = 	 Parameter(name='rT3313', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3313}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [3, 1, 3] ) 
 
iT3313 = 	 Parameter(name='iT3313', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3313}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [3, 1, 3] ) 
 
rT3321 = 	 Parameter(name='rT3321', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3321}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [3, 2, 1] ) 
 
iT3321 = 	 Parameter(name='iT3321', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3321}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [3, 2, 1] ) 
 
rT3322 = 	 Parameter(name='rT3322', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3322}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [3, 2, 2] ) 
 
iT3322 = 	 Parameter(name='iT3322', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3322}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [3, 2, 2] ) 
 
rT3323 = 	 Parameter(name='rT3323', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3323}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [3, 2, 3] ) 
 
iT3323 = 	 Parameter(name='iT3323', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3323}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [3, 2, 3] ) 
 
rT3331 = 	 Parameter(name='rT3331', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3331}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [3, 3, 1] ) 
 
iT3331 = 	 Parameter(name='iT3331', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3331}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [3, 3, 1] ) 
 
rT3332 = 	 Parameter(name='rT3332', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3332}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [3, 3, 2] ) 
 
iT3332 = 	 Parameter(name='iT3332', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3332}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [3, 3, 2] ) 
 
rT3333 = 	 Parameter(name='rT3333', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3333}', 
	 lhablock = 'RVTUDD', 
	 lhacode = [3, 3, 3] ) 
 
iT3333 = 	 Parameter(name='iT3333', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{T3333}', 
	 lhablock = 'IMRVTUDD', 
	 lhacode = [3, 3, 3] ) 
 
rYu11 = 	 Parameter(name='rYu11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu11}', 
	 lhablock = 'YU', 
	 lhacode = [1, 1] ) 
 
iYu11 = 	 Parameter(name='iYu11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu11}', 
	 lhablock = 'IMYU', 
	 lhacode = [1, 1] ) 
 
rYu12 = 	 Parameter(name='rYu12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu12}', 
	 lhablock = 'YU', 
	 lhacode = [1, 2] ) 
 
iYu12 = 	 Parameter(name='iYu12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu12}', 
	 lhablock = 'IMYU', 
	 lhacode = [1, 2] ) 
 
rYu13 = 	 Parameter(name='rYu13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu13}', 
	 lhablock = 'YU', 
	 lhacode = [1, 3] ) 
 
iYu13 = 	 Parameter(name='iYu13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu13}', 
	 lhablock = 'IMYU', 
	 lhacode = [1, 3] ) 
 
rYu21 = 	 Parameter(name='rYu21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu21}', 
	 lhablock = 'YU', 
	 lhacode = [2, 1] ) 
 
iYu21 = 	 Parameter(name='iYu21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu21}', 
	 lhablock = 'IMYU', 
	 lhacode = [2, 1] ) 
 
rYu22 = 	 Parameter(name='rYu22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu22}', 
	 lhablock = 'YU', 
	 lhacode = [2, 2] ) 
 
iYu22 = 	 Parameter(name='iYu22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu22}', 
	 lhablock = 'IMYU', 
	 lhacode = [2, 2] ) 
 
rYu23 = 	 Parameter(name='rYu23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu23}', 
	 lhablock = 'YU', 
	 lhacode = [2, 3] ) 
 
iYu23 = 	 Parameter(name='iYu23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu23}', 
	 lhablock = 'IMYU', 
	 lhacode = [2, 3] ) 
 
rYu31 = 	 Parameter(name='rYu31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu31}', 
	 lhablock = 'YU', 
	 lhacode = [3, 1] ) 
 
iYu31 = 	 Parameter(name='iYu31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu31}', 
	 lhablock = 'IMYU', 
	 lhacode = [3, 1] ) 
 
rYu32 = 	 Parameter(name='rYu32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu32}', 
	 lhablock = 'YU', 
	 lhacode = [3, 2] ) 
 
iYu32 = 	 Parameter(name='iYu32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu32}', 
	 lhablock = 'IMYU', 
	 lhacode = [3, 2] ) 
 
rYu33 = 	 Parameter(name='rYu33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu33}', 
	 lhablock = 'YU', 
	 lhacode = [3, 3] ) 
 
iYu33 = 	 Parameter(name='iYu33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Yu33}', 
	 lhablock = 'IMYU', 
	 lhacode = [3, 3] ) 
 
rTu11 = 	 Parameter(name='rTu11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu11}', 
	 lhablock = 'TU', 
	 lhacode = [1, 1] ) 
 
iTu11 = 	 Parameter(name='iTu11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu11}', 
	 lhablock = 'IMTU', 
	 lhacode = [1, 1] ) 
 
rTu12 = 	 Parameter(name='rTu12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu12}', 
	 lhablock = 'TU', 
	 lhacode = [1, 2] ) 
 
iTu12 = 	 Parameter(name='iTu12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu12}', 
	 lhablock = 'IMTU', 
	 lhacode = [1, 2] ) 
 
rTu13 = 	 Parameter(name='rTu13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu13}', 
	 lhablock = 'TU', 
	 lhacode = [1, 3] ) 
 
iTu13 = 	 Parameter(name='iTu13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu13}', 
	 lhablock = 'IMTU', 
	 lhacode = [1, 3] ) 
 
rTu21 = 	 Parameter(name='rTu21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu21}', 
	 lhablock = 'TU', 
	 lhacode = [2, 1] ) 
 
iTu21 = 	 Parameter(name='iTu21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu21}', 
	 lhablock = 'IMTU', 
	 lhacode = [2, 1] ) 
 
rTu22 = 	 Parameter(name='rTu22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu22}', 
	 lhablock = 'TU', 
	 lhacode = [2, 2] ) 
 
iTu22 = 	 Parameter(name='iTu22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu22}', 
	 lhablock = 'IMTU', 
	 lhacode = [2, 2] ) 
 
rTu23 = 	 Parameter(name='rTu23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu23}', 
	 lhablock = 'TU', 
	 lhacode = [2, 3] ) 
 
iTu23 = 	 Parameter(name='iTu23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu23}', 
	 lhablock = 'IMTU', 
	 lhacode = [2, 3] ) 
 
rTu31 = 	 Parameter(name='rTu31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu31}', 
	 lhablock = 'TU', 
	 lhacode = [3, 1] ) 
 
iTu31 = 	 Parameter(name='iTu31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu31}', 
	 lhablock = 'IMTU', 
	 lhacode = [3, 1] ) 
 
rTu32 = 	 Parameter(name='rTu32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu32}', 
	 lhablock = 'TU', 
	 lhacode = [3, 2] ) 
 
iTu32 = 	 Parameter(name='iTu32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu32}', 
	 lhablock = 'IMTU', 
	 lhacode = [3, 2] ) 
 
rTu33 = 	 Parameter(name='rTu33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu33}', 
	 lhablock = 'TU', 
	 lhacode = [3, 3] ) 
 
iTu33 = 	 Parameter(name='iTu33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Tu33}', 
	 lhablock = 'IMTU', 
	 lhacode = [3, 3] ) 
 
rpG = 	 Parameter(name='rpG', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{pG}', 
	 lhablock = 'PHASES', 
	 lhacode = [1] ) 
 
ipG = 	 Parameter(name='ipG', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{pG}', 
	 lhablock = 'IMPHASES', 
	 lhacode = [1] ) 
 
rZD11 = 	 Parameter(name='rZD11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD11}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [1, 1] ) 
 
iZD11 = 	 Parameter(name='iZD11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD11}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [1, 1] ) 
 
rZD12 = 	 Parameter(name='rZD12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD12}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [1, 2] ) 
 
iZD12 = 	 Parameter(name='iZD12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD12}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [1, 2] ) 
 
rZD13 = 	 Parameter(name='rZD13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD13}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [1, 3] ) 
 
iZD13 = 	 Parameter(name='iZD13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD13}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [1, 3] ) 
 
rZD14 = 	 Parameter(name='rZD14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD14}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [1, 4] ) 
 
iZD14 = 	 Parameter(name='iZD14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD14}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [1, 4] ) 
 
rZD15 = 	 Parameter(name='rZD15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD15}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [1, 5] ) 
 
iZD15 = 	 Parameter(name='iZD15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD15}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [1, 5] ) 
 
rZD16 = 	 Parameter(name='rZD16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD16}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [1, 6] ) 
 
iZD16 = 	 Parameter(name='iZD16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD16}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [1, 6] ) 
 
rZD21 = 	 Parameter(name='rZD21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD21}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [2, 1] ) 
 
iZD21 = 	 Parameter(name='iZD21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD21}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [2, 1] ) 
 
rZD22 = 	 Parameter(name='rZD22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD22}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [2, 2] ) 
 
iZD22 = 	 Parameter(name='iZD22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD22}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [2, 2] ) 
 
rZD23 = 	 Parameter(name='rZD23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD23}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [2, 3] ) 
 
iZD23 = 	 Parameter(name='iZD23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD23}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [2, 3] ) 
 
rZD24 = 	 Parameter(name='rZD24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD24}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [2, 4] ) 
 
iZD24 = 	 Parameter(name='iZD24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD24}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [2, 4] ) 
 
rZD25 = 	 Parameter(name='rZD25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD25}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [2, 5] ) 
 
iZD25 = 	 Parameter(name='iZD25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD25}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [2, 5] ) 
 
rZD26 = 	 Parameter(name='rZD26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD26}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [2, 6] ) 
 
iZD26 = 	 Parameter(name='iZD26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD26}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [2, 6] ) 
 
rZD31 = 	 Parameter(name='rZD31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD31}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [3, 1] ) 
 
iZD31 = 	 Parameter(name='iZD31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD31}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [3, 1] ) 
 
rZD32 = 	 Parameter(name='rZD32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD32}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [3, 2] ) 
 
iZD32 = 	 Parameter(name='iZD32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD32}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [3, 2] ) 
 
rZD33 = 	 Parameter(name='rZD33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD33}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [3, 3] ) 
 
iZD33 = 	 Parameter(name='iZD33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD33}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [3, 3] ) 
 
rZD34 = 	 Parameter(name='rZD34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD34}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [3, 4] ) 
 
iZD34 = 	 Parameter(name='iZD34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD34}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [3, 4] ) 
 
rZD35 = 	 Parameter(name='rZD35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD35}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [3, 5] ) 
 
iZD35 = 	 Parameter(name='iZD35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD35}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [3, 5] ) 
 
rZD36 = 	 Parameter(name='rZD36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD36}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [3, 6] ) 
 
iZD36 = 	 Parameter(name='iZD36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD36}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [3, 6] ) 
 
rZD41 = 	 Parameter(name='rZD41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD41}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [4, 1] ) 
 
iZD41 = 	 Parameter(name='iZD41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD41}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [4, 1] ) 
 
rZD42 = 	 Parameter(name='rZD42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD42}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [4, 2] ) 
 
iZD42 = 	 Parameter(name='iZD42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD42}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [4, 2] ) 
 
rZD43 = 	 Parameter(name='rZD43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD43}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [4, 3] ) 
 
iZD43 = 	 Parameter(name='iZD43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD43}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [4, 3] ) 
 
rZD44 = 	 Parameter(name='rZD44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD44}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [4, 4] ) 
 
iZD44 = 	 Parameter(name='iZD44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD44}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [4, 4] ) 
 
rZD45 = 	 Parameter(name='rZD45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD45}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [4, 5] ) 
 
iZD45 = 	 Parameter(name='iZD45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD45}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [4, 5] ) 
 
rZD46 = 	 Parameter(name='rZD46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD46}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [4, 6] ) 
 
iZD46 = 	 Parameter(name='iZD46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD46}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [4, 6] ) 
 
rZD51 = 	 Parameter(name='rZD51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD51}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [5, 1] ) 
 
iZD51 = 	 Parameter(name='iZD51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD51}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [5, 1] ) 
 
rZD52 = 	 Parameter(name='rZD52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD52}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [5, 2] ) 
 
iZD52 = 	 Parameter(name='iZD52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD52}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [5, 2] ) 
 
rZD53 = 	 Parameter(name='rZD53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD53}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [5, 3] ) 
 
iZD53 = 	 Parameter(name='iZD53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD53}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [5, 3] ) 
 
rZD54 = 	 Parameter(name='rZD54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD54}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [5, 4] ) 
 
iZD54 = 	 Parameter(name='iZD54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD54}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [5, 4] ) 
 
rZD55 = 	 Parameter(name='rZD55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD55}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [5, 5] ) 
 
iZD55 = 	 Parameter(name='iZD55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD55}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [5, 5] ) 
 
rZD56 = 	 Parameter(name='rZD56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD56}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [5, 6] ) 
 
iZD56 = 	 Parameter(name='iZD56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD56}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [5, 6] ) 
 
rZD61 = 	 Parameter(name='rZD61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD61}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [6, 1] ) 
 
iZD61 = 	 Parameter(name='iZD61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD61}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [6, 1] ) 
 
rZD62 = 	 Parameter(name='rZD62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD62}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [6, 2] ) 
 
iZD62 = 	 Parameter(name='iZD62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD62}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [6, 2] ) 
 
rZD63 = 	 Parameter(name='rZD63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD63}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [6, 3] ) 
 
iZD63 = 	 Parameter(name='iZD63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD63}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [6, 3] ) 
 
rZD64 = 	 Parameter(name='rZD64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD64}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [6, 4] ) 
 
iZD64 = 	 Parameter(name='iZD64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD64}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [6, 4] ) 
 
rZD65 = 	 Parameter(name='rZD65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD65}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [6, 5] ) 
 
iZD65 = 	 Parameter(name='iZD65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD65}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [6, 5] ) 
 
rZD66 = 	 Parameter(name='rZD66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD66}', 
	 lhablock = 'DSQMIX', 
	 lhacode = [6, 6] ) 
 
iZD66 = 	 Parameter(name='iZD66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZD66}', 
	 lhablock = 'IMDSQMIX', 
	 lhacode = [6, 6] ) 
 
rZV11 = 	 Parameter(name='rZV11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV11}', 
	 lhablock = 'SNUMIX', 
	 lhacode = [1, 1] ) 
 
iZV11 = 	 Parameter(name='iZV11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV11}', 
	 lhablock = 'IMSNUMIX', 
	 lhacode = [1, 1] ) 
 
rZV12 = 	 Parameter(name='rZV12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV12}', 
	 lhablock = 'SNUMIX', 
	 lhacode = [1, 2] ) 
 
iZV12 = 	 Parameter(name='iZV12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV12}', 
	 lhablock = 'IMSNUMIX', 
	 lhacode = [1, 2] ) 
 
rZV13 = 	 Parameter(name='rZV13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV13}', 
	 lhablock = 'SNUMIX', 
	 lhacode = [1, 3] ) 
 
iZV13 = 	 Parameter(name='iZV13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV13}', 
	 lhablock = 'IMSNUMIX', 
	 lhacode = [1, 3] ) 
 
rZV21 = 	 Parameter(name='rZV21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV21}', 
	 lhablock = 'SNUMIX', 
	 lhacode = [2, 1] ) 
 
iZV21 = 	 Parameter(name='iZV21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV21}', 
	 lhablock = 'IMSNUMIX', 
	 lhacode = [2, 1] ) 
 
rZV22 = 	 Parameter(name='rZV22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV22}', 
	 lhablock = 'SNUMIX', 
	 lhacode = [2, 2] ) 
 
iZV22 = 	 Parameter(name='iZV22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV22}', 
	 lhablock = 'IMSNUMIX', 
	 lhacode = [2, 2] ) 
 
rZV23 = 	 Parameter(name='rZV23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV23}', 
	 lhablock = 'SNUMIX', 
	 lhacode = [2, 3] ) 
 
iZV23 = 	 Parameter(name='iZV23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV23}', 
	 lhablock = 'IMSNUMIX', 
	 lhacode = [2, 3] ) 
 
rZV31 = 	 Parameter(name='rZV31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV31}', 
	 lhablock = 'SNUMIX', 
	 lhacode = [3, 1] ) 
 
iZV31 = 	 Parameter(name='iZV31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV31}', 
	 lhablock = 'IMSNUMIX', 
	 lhacode = [3, 1] ) 
 
rZV32 = 	 Parameter(name='rZV32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV32}', 
	 lhablock = 'SNUMIX', 
	 lhacode = [3, 2] ) 
 
iZV32 = 	 Parameter(name='iZV32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV32}', 
	 lhablock = 'IMSNUMIX', 
	 lhacode = [3, 2] ) 
 
rZV33 = 	 Parameter(name='rZV33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV33}', 
	 lhablock = 'SNUMIX', 
	 lhacode = [3, 3] ) 
 
iZV33 = 	 Parameter(name='iZV33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZV33}', 
	 lhablock = 'IMSNUMIX', 
	 lhacode = [3, 3] ) 
 
rZU11 = 	 Parameter(name='rZU11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU11}', 
	 lhablock = 'USQMIX', 
	 lhacode = [1, 1] ) 
 
iZU11 = 	 Parameter(name='iZU11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU11}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [1, 1] ) 
 
rZU12 = 	 Parameter(name='rZU12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU12}', 
	 lhablock = 'USQMIX', 
	 lhacode = [1, 2] ) 
 
iZU12 = 	 Parameter(name='iZU12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU12}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [1, 2] ) 
 
rZU13 = 	 Parameter(name='rZU13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU13}', 
	 lhablock = 'USQMIX', 
	 lhacode = [1, 3] ) 
 
iZU13 = 	 Parameter(name='iZU13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU13}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [1, 3] ) 
 
rZU14 = 	 Parameter(name='rZU14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU14}', 
	 lhablock = 'USQMIX', 
	 lhacode = [1, 4] ) 
 
iZU14 = 	 Parameter(name='iZU14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU14}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [1, 4] ) 
 
rZU15 = 	 Parameter(name='rZU15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU15}', 
	 lhablock = 'USQMIX', 
	 lhacode = [1, 5] ) 
 
iZU15 = 	 Parameter(name='iZU15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU15}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [1, 5] ) 
 
rZU16 = 	 Parameter(name='rZU16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU16}', 
	 lhablock = 'USQMIX', 
	 lhacode = [1, 6] ) 
 
iZU16 = 	 Parameter(name='iZU16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU16}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [1, 6] ) 
 
rZU21 = 	 Parameter(name='rZU21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU21}', 
	 lhablock = 'USQMIX', 
	 lhacode = [2, 1] ) 
 
iZU21 = 	 Parameter(name='iZU21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU21}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [2, 1] ) 
 
rZU22 = 	 Parameter(name='rZU22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU22}', 
	 lhablock = 'USQMIX', 
	 lhacode = [2, 2] ) 
 
iZU22 = 	 Parameter(name='iZU22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU22}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [2, 2] ) 
 
rZU23 = 	 Parameter(name='rZU23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU23}', 
	 lhablock = 'USQMIX', 
	 lhacode = [2, 3] ) 
 
iZU23 = 	 Parameter(name='iZU23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU23}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [2, 3] ) 
 
rZU24 = 	 Parameter(name='rZU24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU24}', 
	 lhablock = 'USQMIX', 
	 lhacode = [2, 4] ) 
 
iZU24 = 	 Parameter(name='iZU24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU24}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [2, 4] ) 
 
rZU25 = 	 Parameter(name='rZU25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU25}', 
	 lhablock = 'USQMIX', 
	 lhacode = [2, 5] ) 
 
iZU25 = 	 Parameter(name='iZU25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU25}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [2, 5] ) 
 
rZU26 = 	 Parameter(name='rZU26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU26}', 
	 lhablock = 'USQMIX', 
	 lhacode = [2, 6] ) 
 
iZU26 = 	 Parameter(name='iZU26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU26}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [2, 6] ) 
 
rZU31 = 	 Parameter(name='rZU31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU31}', 
	 lhablock = 'USQMIX', 
	 lhacode = [3, 1] ) 
 
iZU31 = 	 Parameter(name='iZU31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU31}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [3, 1] ) 
 
rZU32 = 	 Parameter(name='rZU32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU32}', 
	 lhablock = 'USQMIX', 
	 lhacode = [3, 2] ) 
 
iZU32 = 	 Parameter(name='iZU32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU32}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [3, 2] ) 
 
rZU33 = 	 Parameter(name='rZU33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU33}', 
	 lhablock = 'USQMIX', 
	 lhacode = [3, 3] ) 
 
iZU33 = 	 Parameter(name='iZU33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU33}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [3, 3] ) 
 
rZU34 = 	 Parameter(name='rZU34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU34}', 
	 lhablock = 'USQMIX', 
	 lhacode = [3, 4] ) 
 
iZU34 = 	 Parameter(name='iZU34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU34}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [3, 4] ) 
 
rZU35 = 	 Parameter(name='rZU35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU35}', 
	 lhablock = 'USQMIX', 
	 lhacode = [3, 5] ) 
 
iZU35 = 	 Parameter(name='iZU35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU35}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [3, 5] ) 
 
rZU36 = 	 Parameter(name='rZU36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU36}', 
	 lhablock = 'USQMIX', 
	 lhacode = [3, 6] ) 
 
iZU36 = 	 Parameter(name='iZU36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU36}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [3, 6] ) 
 
rZU41 = 	 Parameter(name='rZU41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU41}', 
	 lhablock = 'USQMIX', 
	 lhacode = [4, 1] ) 
 
iZU41 = 	 Parameter(name='iZU41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU41}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [4, 1] ) 
 
rZU42 = 	 Parameter(name='rZU42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU42}', 
	 lhablock = 'USQMIX', 
	 lhacode = [4, 2] ) 
 
iZU42 = 	 Parameter(name='iZU42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU42}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [4, 2] ) 
 
rZU43 = 	 Parameter(name='rZU43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU43}', 
	 lhablock = 'USQMIX', 
	 lhacode = [4, 3] ) 
 
iZU43 = 	 Parameter(name='iZU43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU43}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [4, 3] ) 
 
rZU44 = 	 Parameter(name='rZU44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU44}', 
	 lhablock = 'USQMIX', 
	 lhacode = [4, 4] ) 
 
iZU44 = 	 Parameter(name='iZU44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU44}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [4, 4] ) 
 
rZU45 = 	 Parameter(name='rZU45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU45}', 
	 lhablock = 'USQMIX', 
	 lhacode = [4, 5] ) 
 
iZU45 = 	 Parameter(name='iZU45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU45}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [4, 5] ) 
 
rZU46 = 	 Parameter(name='rZU46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU46}', 
	 lhablock = 'USQMIX', 
	 lhacode = [4, 6] ) 
 
iZU46 = 	 Parameter(name='iZU46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU46}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [4, 6] ) 
 
rZU51 = 	 Parameter(name='rZU51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU51}', 
	 lhablock = 'USQMIX', 
	 lhacode = [5, 1] ) 
 
iZU51 = 	 Parameter(name='iZU51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU51}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [5, 1] ) 
 
rZU52 = 	 Parameter(name='rZU52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU52}', 
	 lhablock = 'USQMIX', 
	 lhacode = [5, 2] ) 
 
iZU52 = 	 Parameter(name='iZU52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU52}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [5, 2] ) 
 
rZU53 = 	 Parameter(name='rZU53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU53}', 
	 lhablock = 'USQMIX', 
	 lhacode = [5, 3] ) 
 
iZU53 = 	 Parameter(name='iZU53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU53}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [5, 3] ) 
 
rZU54 = 	 Parameter(name='rZU54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU54}', 
	 lhablock = 'USQMIX', 
	 lhacode = [5, 4] ) 
 
iZU54 = 	 Parameter(name='iZU54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU54}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [5, 4] ) 
 
rZU55 = 	 Parameter(name='rZU55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU55}', 
	 lhablock = 'USQMIX', 
	 lhacode = [5, 5] ) 
 
iZU55 = 	 Parameter(name='iZU55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU55}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [5, 5] ) 
 
rZU56 = 	 Parameter(name='rZU56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU56}', 
	 lhablock = 'USQMIX', 
	 lhacode = [5, 6] ) 
 
iZU56 = 	 Parameter(name='iZU56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU56}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [5, 6] ) 
 
rZU61 = 	 Parameter(name='rZU61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU61}', 
	 lhablock = 'USQMIX', 
	 lhacode = [6, 1] ) 
 
iZU61 = 	 Parameter(name='iZU61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU61}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [6, 1] ) 
 
rZU62 = 	 Parameter(name='rZU62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU62}', 
	 lhablock = 'USQMIX', 
	 lhacode = [6, 2] ) 
 
iZU62 = 	 Parameter(name='iZU62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU62}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [6, 2] ) 
 
rZU63 = 	 Parameter(name='rZU63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU63}', 
	 lhablock = 'USQMIX', 
	 lhacode = [6, 3] ) 
 
iZU63 = 	 Parameter(name='iZU63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU63}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [6, 3] ) 
 
rZU64 = 	 Parameter(name='rZU64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU64}', 
	 lhablock = 'USQMIX', 
	 lhacode = [6, 4] ) 
 
iZU64 = 	 Parameter(name='iZU64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU64}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [6, 4] ) 
 
rZU65 = 	 Parameter(name='rZU65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU65}', 
	 lhablock = 'USQMIX', 
	 lhacode = [6, 5] ) 
 
iZU65 = 	 Parameter(name='iZU65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU65}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [6, 5] ) 
 
rZU66 = 	 Parameter(name='rZU66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU66}', 
	 lhablock = 'USQMIX', 
	 lhacode = [6, 6] ) 
 
iZU66 = 	 Parameter(name='iZU66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZU66}', 
	 lhablock = 'IMUSQMIX', 
	 lhacode = [6, 6] ) 
 
rZE11 = 	 Parameter(name='rZE11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE11}', 
	 lhablock = 'SELMIX', 
	 lhacode = [1, 1] ) 
 
iZE11 = 	 Parameter(name='iZE11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE11}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [1, 1] ) 
 
rZE12 = 	 Parameter(name='rZE12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE12}', 
	 lhablock = 'SELMIX', 
	 lhacode = [1, 2] ) 
 
iZE12 = 	 Parameter(name='iZE12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE12}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [1, 2] ) 
 
rZE13 = 	 Parameter(name='rZE13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE13}', 
	 lhablock = 'SELMIX', 
	 lhacode = [1, 3] ) 
 
iZE13 = 	 Parameter(name='iZE13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE13}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [1, 3] ) 
 
rZE14 = 	 Parameter(name='rZE14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE14}', 
	 lhablock = 'SELMIX', 
	 lhacode = [1, 4] ) 
 
iZE14 = 	 Parameter(name='iZE14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE14}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [1, 4] ) 
 
rZE15 = 	 Parameter(name='rZE15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE15}', 
	 lhablock = 'SELMIX', 
	 lhacode = [1, 5] ) 
 
iZE15 = 	 Parameter(name='iZE15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE15}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [1, 5] ) 
 
rZE16 = 	 Parameter(name='rZE16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE16}', 
	 lhablock = 'SELMIX', 
	 lhacode = [1, 6] ) 
 
iZE16 = 	 Parameter(name='iZE16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE16}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [1, 6] ) 
 
rZE21 = 	 Parameter(name='rZE21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE21}', 
	 lhablock = 'SELMIX', 
	 lhacode = [2, 1] ) 
 
iZE21 = 	 Parameter(name='iZE21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE21}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [2, 1] ) 
 
rZE22 = 	 Parameter(name='rZE22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE22}', 
	 lhablock = 'SELMIX', 
	 lhacode = [2, 2] ) 
 
iZE22 = 	 Parameter(name='iZE22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE22}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [2, 2] ) 
 
rZE23 = 	 Parameter(name='rZE23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE23}', 
	 lhablock = 'SELMIX', 
	 lhacode = [2, 3] ) 
 
iZE23 = 	 Parameter(name='iZE23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE23}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [2, 3] ) 
 
rZE24 = 	 Parameter(name='rZE24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE24}', 
	 lhablock = 'SELMIX', 
	 lhacode = [2, 4] ) 
 
iZE24 = 	 Parameter(name='iZE24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE24}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [2, 4] ) 
 
rZE25 = 	 Parameter(name='rZE25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE25}', 
	 lhablock = 'SELMIX', 
	 lhacode = [2, 5] ) 
 
iZE25 = 	 Parameter(name='iZE25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE25}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [2, 5] ) 
 
rZE26 = 	 Parameter(name='rZE26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE26}', 
	 lhablock = 'SELMIX', 
	 lhacode = [2, 6] ) 
 
iZE26 = 	 Parameter(name='iZE26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE26}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [2, 6] ) 
 
rZE31 = 	 Parameter(name='rZE31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE31}', 
	 lhablock = 'SELMIX', 
	 lhacode = [3, 1] ) 
 
iZE31 = 	 Parameter(name='iZE31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE31}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [3, 1] ) 
 
rZE32 = 	 Parameter(name='rZE32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE32}', 
	 lhablock = 'SELMIX', 
	 lhacode = [3, 2] ) 
 
iZE32 = 	 Parameter(name='iZE32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE32}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [3, 2] ) 
 
rZE33 = 	 Parameter(name='rZE33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE33}', 
	 lhablock = 'SELMIX', 
	 lhacode = [3, 3] ) 
 
iZE33 = 	 Parameter(name='iZE33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE33}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [3, 3] ) 
 
rZE34 = 	 Parameter(name='rZE34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE34}', 
	 lhablock = 'SELMIX', 
	 lhacode = [3, 4] ) 
 
iZE34 = 	 Parameter(name='iZE34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE34}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [3, 4] ) 
 
rZE35 = 	 Parameter(name='rZE35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE35}', 
	 lhablock = 'SELMIX', 
	 lhacode = [3, 5] ) 
 
iZE35 = 	 Parameter(name='iZE35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE35}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [3, 5] ) 
 
rZE36 = 	 Parameter(name='rZE36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE36}', 
	 lhablock = 'SELMIX', 
	 lhacode = [3, 6] ) 
 
iZE36 = 	 Parameter(name='iZE36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE36}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [3, 6] ) 
 
rZE41 = 	 Parameter(name='rZE41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE41}', 
	 lhablock = 'SELMIX', 
	 lhacode = [4, 1] ) 
 
iZE41 = 	 Parameter(name='iZE41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE41}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [4, 1] ) 
 
rZE42 = 	 Parameter(name='rZE42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE42}', 
	 lhablock = 'SELMIX', 
	 lhacode = [4, 2] ) 
 
iZE42 = 	 Parameter(name='iZE42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE42}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [4, 2] ) 
 
rZE43 = 	 Parameter(name='rZE43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE43}', 
	 lhablock = 'SELMIX', 
	 lhacode = [4, 3] ) 
 
iZE43 = 	 Parameter(name='iZE43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE43}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [4, 3] ) 
 
rZE44 = 	 Parameter(name='rZE44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE44}', 
	 lhablock = 'SELMIX', 
	 lhacode = [4, 4] ) 
 
iZE44 = 	 Parameter(name='iZE44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE44}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [4, 4] ) 
 
rZE45 = 	 Parameter(name='rZE45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE45}', 
	 lhablock = 'SELMIX', 
	 lhacode = [4, 5] ) 
 
iZE45 = 	 Parameter(name='iZE45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE45}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [4, 5] ) 
 
rZE46 = 	 Parameter(name='rZE46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE46}', 
	 lhablock = 'SELMIX', 
	 lhacode = [4, 6] ) 
 
iZE46 = 	 Parameter(name='iZE46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE46}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [4, 6] ) 
 
rZE51 = 	 Parameter(name='rZE51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE51}', 
	 lhablock = 'SELMIX', 
	 lhacode = [5, 1] ) 
 
iZE51 = 	 Parameter(name='iZE51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE51}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [5, 1] ) 
 
rZE52 = 	 Parameter(name='rZE52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE52}', 
	 lhablock = 'SELMIX', 
	 lhacode = [5, 2] ) 
 
iZE52 = 	 Parameter(name='iZE52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE52}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [5, 2] ) 
 
rZE53 = 	 Parameter(name='rZE53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE53}', 
	 lhablock = 'SELMIX', 
	 lhacode = [5, 3] ) 
 
iZE53 = 	 Parameter(name='iZE53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE53}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [5, 3] ) 
 
rZE54 = 	 Parameter(name='rZE54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE54}', 
	 lhablock = 'SELMIX', 
	 lhacode = [5, 4] ) 
 
iZE54 = 	 Parameter(name='iZE54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE54}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [5, 4] ) 
 
rZE55 = 	 Parameter(name='rZE55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE55}', 
	 lhablock = 'SELMIX', 
	 lhacode = [5, 5] ) 
 
iZE55 = 	 Parameter(name='iZE55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE55}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [5, 5] ) 
 
rZE56 = 	 Parameter(name='rZE56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE56}', 
	 lhablock = 'SELMIX', 
	 lhacode = [5, 6] ) 
 
iZE56 = 	 Parameter(name='iZE56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE56}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [5, 6] ) 
 
rZE61 = 	 Parameter(name='rZE61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE61}', 
	 lhablock = 'SELMIX', 
	 lhacode = [6, 1] ) 
 
iZE61 = 	 Parameter(name='iZE61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE61}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [6, 1] ) 
 
rZE62 = 	 Parameter(name='rZE62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE62}', 
	 lhablock = 'SELMIX', 
	 lhacode = [6, 2] ) 
 
iZE62 = 	 Parameter(name='iZE62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE62}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [6, 2] ) 
 
rZE63 = 	 Parameter(name='rZE63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE63}', 
	 lhablock = 'SELMIX', 
	 lhacode = [6, 3] ) 
 
iZE63 = 	 Parameter(name='iZE63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE63}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [6, 3] ) 
 
rZE64 = 	 Parameter(name='rZE64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE64}', 
	 lhablock = 'SELMIX', 
	 lhacode = [6, 4] ) 
 
iZE64 = 	 Parameter(name='iZE64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE64}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [6, 4] ) 
 
rZE65 = 	 Parameter(name='rZE65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE65}', 
	 lhablock = 'SELMIX', 
	 lhacode = [6, 5] ) 
 
iZE65 = 	 Parameter(name='iZE65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE65}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [6, 5] ) 
 
rZE66 = 	 Parameter(name='rZE66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE66}', 
	 lhablock = 'SELMIX', 
	 lhacode = [6, 6] ) 
 
iZE66 = 	 Parameter(name='iZE66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZE66}', 
	 lhablock = 'IMSELMIX', 
	 lhacode = [6, 6] ) 
 
rZN11 = 	 Parameter(name='rZN11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN11}', 
	 lhablock = 'NMIX', 
	 lhacode = [1, 1] ) 
 
iZN11 = 	 Parameter(name='iZN11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN11}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [1, 1] ) 
 
rZN12 = 	 Parameter(name='rZN12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN12}', 
	 lhablock = 'NMIX', 
	 lhacode = [1, 2] ) 
 
iZN12 = 	 Parameter(name='iZN12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN12}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [1, 2] ) 
 
rZN13 = 	 Parameter(name='rZN13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN13}', 
	 lhablock = 'NMIX', 
	 lhacode = [1, 3] ) 
 
iZN13 = 	 Parameter(name='iZN13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN13}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [1, 3] ) 
 
rZN14 = 	 Parameter(name='rZN14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN14}', 
	 lhablock = 'NMIX', 
	 lhacode = [1, 4] ) 
 
iZN14 = 	 Parameter(name='iZN14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN14}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [1, 4] ) 
 
rZN21 = 	 Parameter(name='rZN21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN21}', 
	 lhablock = 'NMIX', 
	 lhacode = [2, 1] ) 
 
iZN21 = 	 Parameter(name='iZN21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN21}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [2, 1] ) 
 
rZN22 = 	 Parameter(name='rZN22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN22}', 
	 lhablock = 'NMIX', 
	 lhacode = [2, 2] ) 
 
iZN22 = 	 Parameter(name='iZN22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN22}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [2, 2] ) 
 
rZN23 = 	 Parameter(name='rZN23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN23}', 
	 lhablock = 'NMIX', 
	 lhacode = [2, 3] ) 
 
iZN23 = 	 Parameter(name='iZN23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN23}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [2, 3] ) 
 
rZN24 = 	 Parameter(name='rZN24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN24}', 
	 lhablock = 'NMIX', 
	 lhacode = [2, 4] ) 
 
iZN24 = 	 Parameter(name='iZN24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN24}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [2, 4] ) 
 
rZN31 = 	 Parameter(name='rZN31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN31}', 
	 lhablock = 'NMIX', 
	 lhacode = [3, 1] ) 
 
iZN31 = 	 Parameter(name='iZN31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN31}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [3, 1] ) 
 
rZN32 = 	 Parameter(name='rZN32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN32}', 
	 lhablock = 'NMIX', 
	 lhacode = [3, 2] ) 
 
iZN32 = 	 Parameter(name='iZN32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN32}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [3, 2] ) 
 
rZN33 = 	 Parameter(name='rZN33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN33}', 
	 lhablock = 'NMIX', 
	 lhacode = [3, 3] ) 
 
iZN33 = 	 Parameter(name='iZN33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN33}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [3, 3] ) 
 
rZN34 = 	 Parameter(name='rZN34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN34}', 
	 lhablock = 'NMIX', 
	 lhacode = [3, 4] ) 
 
iZN34 = 	 Parameter(name='iZN34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN34}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [3, 4] ) 
 
rZN41 = 	 Parameter(name='rZN41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN41}', 
	 lhablock = 'NMIX', 
	 lhacode = [4, 1] ) 
 
iZN41 = 	 Parameter(name='iZN41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN41}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [4, 1] ) 
 
rZN42 = 	 Parameter(name='rZN42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN42}', 
	 lhablock = 'NMIX', 
	 lhacode = [4, 2] ) 
 
iZN42 = 	 Parameter(name='iZN42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN42}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [4, 2] ) 
 
rZN43 = 	 Parameter(name='rZN43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN43}', 
	 lhablock = 'NMIX', 
	 lhacode = [4, 3] ) 
 
iZN43 = 	 Parameter(name='iZN43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN43}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [4, 3] ) 
 
rZN44 = 	 Parameter(name='rZN44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN44}', 
	 lhablock = 'NMIX', 
	 lhacode = [4, 4] ) 
 
iZN44 = 	 Parameter(name='iZN44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZN44}', 
	 lhablock = 'IMNMIX', 
	 lhacode = [4, 4] ) 
 
rUM11 = 	 Parameter(name='rUM11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UM11}', 
	 lhablock = 'UMIX', 
	 lhacode = [1, 1] ) 
 
iUM11 = 	 Parameter(name='iUM11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UM11}', 
	 lhablock = 'IMUMIX', 
	 lhacode = [1, 1] ) 
 
rUM12 = 	 Parameter(name='rUM12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UM12}', 
	 lhablock = 'UMIX', 
	 lhacode = [1, 2] ) 
 
iUM12 = 	 Parameter(name='iUM12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UM12}', 
	 lhablock = 'IMUMIX', 
	 lhacode = [1, 2] ) 
 
rUM21 = 	 Parameter(name='rUM21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UM21}', 
	 lhablock = 'UMIX', 
	 lhacode = [2, 1] ) 
 
iUM21 = 	 Parameter(name='iUM21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UM21}', 
	 lhablock = 'IMUMIX', 
	 lhacode = [2, 1] ) 
 
rUM22 = 	 Parameter(name='rUM22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UM22}', 
	 lhablock = 'UMIX', 
	 lhacode = [2, 2] ) 
 
iUM22 = 	 Parameter(name='iUM22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UM22}', 
	 lhablock = 'IMUMIX', 
	 lhacode = [2, 2] ) 
 
rUP11 = 	 Parameter(name='rUP11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UP11}', 
	 lhablock = 'VMIX', 
	 lhacode = [1, 1] ) 
 
iUP11 = 	 Parameter(name='iUP11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UP11}', 
	 lhablock = 'IMVMIX', 
	 lhacode = [1, 1] ) 
 
rUP12 = 	 Parameter(name='rUP12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UP12}', 
	 lhablock = 'VMIX', 
	 lhacode = [1, 2] ) 
 
iUP12 = 	 Parameter(name='iUP12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UP12}', 
	 lhablock = 'IMVMIX', 
	 lhacode = [1, 2] ) 
 
rUP21 = 	 Parameter(name='rUP21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UP21}', 
	 lhablock = 'VMIX', 
	 lhacode = [2, 1] ) 
 
iUP21 = 	 Parameter(name='iUP21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UP21}', 
	 lhablock = 'IMVMIX', 
	 lhacode = [2, 1] ) 
 
rUP22 = 	 Parameter(name='rUP22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UP22}', 
	 lhablock = 'VMIX', 
	 lhacode = [2, 2] ) 
 
iUP22 = 	 Parameter(name='iUP22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{UP22}', 
	 lhablock = 'IMVMIX', 
	 lhacode = [2, 2] ) 
 
rZEL11 = 	 Parameter(name='rZEL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL11}', 
	 lhablock = 'UELMIX', 
	 lhacode = [1, 1] ) 
 
iZEL11 = 	 Parameter(name='iZEL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL11}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [1, 1] ) 
 
rZEL12 = 	 Parameter(name='rZEL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL12}', 
	 lhablock = 'UELMIX', 
	 lhacode = [1, 2] ) 
 
iZEL12 = 	 Parameter(name='iZEL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL12}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [1, 2] ) 
 
rZEL13 = 	 Parameter(name='rZEL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL13}', 
	 lhablock = 'UELMIX', 
	 lhacode = [1, 3] ) 
 
iZEL13 = 	 Parameter(name='iZEL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL13}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [1, 3] ) 
 
rZEL21 = 	 Parameter(name='rZEL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL21}', 
	 lhablock = 'UELMIX', 
	 lhacode = [2, 1] ) 
 
iZEL21 = 	 Parameter(name='iZEL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL21}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [2, 1] ) 
 
rZEL22 = 	 Parameter(name='rZEL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL22}', 
	 lhablock = 'UELMIX', 
	 lhacode = [2, 2] ) 
 
iZEL22 = 	 Parameter(name='iZEL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL22}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [2, 2] ) 
 
rZEL23 = 	 Parameter(name='rZEL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL23}', 
	 lhablock = 'UELMIX', 
	 lhacode = [2, 3] ) 
 
iZEL23 = 	 Parameter(name='iZEL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL23}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [2, 3] ) 
 
rZEL31 = 	 Parameter(name='rZEL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL31}', 
	 lhablock = 'UELMIX', 
	 lhacode = [3, 1] ) 
 
iZEL31 = 	 Parameter(name='iZEL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL31}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [3, 1] ) 
 
rZEL32 = 	 Parameter(name='rZEL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL32}', 
	 lhablock = 'UELMIX', 
	 lhacode = [3, 2] ) 
 
iZEL32 = 	 Parameter(name='iZEL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL32}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [3, 2] ) 
 
rZEL33 = 	 Parameter(name='rZEL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL33}', 
	 lhablock = 'UELMIX', 
	 lhacode = [3, 3] ) 
 
iZEL33 = 	 Parameter(name='iZEL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL33}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [3, 3] ) 
 
rZER11 = 	 Parameter(name='rZER11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER11}', 
	 lhablock = 'UERMIX', 
	 lhacode = [1, 1] ) 
 
iZER11 = 	 Parameter(name='iZER11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER11}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [1, 1] ) 
 
rZER12 = 	 Parameter(name='rZER12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER12}', 
	 lhablock = 'UERMIX', 
	 lhacode = [1, 2] ) 
 
iZER12 = 	 Parameter(name='iZER12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER12}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [1, 2] ) 
 
rZER13 = 	 Parameter(name='rZER13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER13}', 
	 lhablock = 'UERMIX', 
	 lhacode = [1, 3] ) 
 
iZER13 = 	 Parameter(name='iZER13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER13}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [1, 3] ) 
 
rZER21 = 	 Parameter(name='rZER21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER21}', 
	 lhablock = 'UERMIX', 
	 lhacode = [2, 1] ) 
 
iZER21 = 	 Parameter(name='iZER21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER21}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [2, 1] ) 
 
rZER22 = 	 Parameter(name='rZER22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER22}', 
	 lhablock = 'UERMIX', 
	 lhacode = [2, 2] ) 
 
iZER22 = 	 Parameter(name='iZER22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER22}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [2, 2] ) 
 
rZER23 = 	 Parameter(name='rZER23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER23}', 
	 lhablock = 'UERMIX', 
	 lhacode = [2, 3] ) 
 
iZER23 = 	 Parameter(name='iZER23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER23}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [2, 3] ) 
 
rZER31 = 	 Parameter(name='rZER31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER31}', 
	 lhablock = 'UERMIX', 
	 lhacode = [3, 1] ) 
 
iZER31 = 	 Parameter(name='iZER31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER31}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [3, 1] ) 
 
rZER32 = 	 Parameter(name='rZER32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER32}', 
	 lhablock = 'UERMIX', 
	 lhacode = [3, 2] ) 
 
iZER32 = 	 Parameter(name='iZER32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER32}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [3, 2] ) 
 
rZER33 = 	 Parameter(name='rZER33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER33}', 
	 lhablock = 'UERMIX', 
	 lhacode = [3, 3] ) 
 
iZER33 = 	 Parameter(name='iZER33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER33}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [3, 3] ) 
 
rZDL11 = 	 Parameter(name='rZDL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL11}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [1, 1] ) 
 
iZDL11 = 	 Parameter(name='iZDL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL11}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [1, 1] ) 
 
rZDL12 = 	 Parameter(name='rZDL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL12}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [1, 2] ) 
 
iZDL12 = 	 Parameter(name='iZDL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL12}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [1, 2] ) 
 
rZDL13 = 	 Parameter(name='rZDL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL13}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [1, 3] ) 
 
iZDL13 = 	 Parameter(name='iZDL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL13}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [1, 3] ) 
 
rZDL21 = 	 Parameter(name='rZDL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL21}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [2, 1] ) 
 
iZDL21 = 	 Parameter(name='iZDL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL21}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [2, 1] ) 
 
rZDL22 = 	 Parameter(name='rZDL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL22}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [2, 2] ) 
 
iZDL22 = 	 Parameter(name='iZDL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL22}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [2, 2] ) 
 
rZDL23 = 	 Parameter(name='rZDL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL23}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [2, 3] ) 
 
iZDL23 = 	 Parameter(name='iZDL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL23}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [2, 3] ) 
 
rZDL31 = 	 Parameter(name='rZDL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL31}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [3, 1] ) 
 
iZDL31 = 	 Parameter(name='iZDL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL31}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [3, 1] ) 
 
rZDL32 = 	 Parameter(name='rZDL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL32}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [3, 2] ) 
 
iZDL32 = 	 Parameter(name='iZDL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL32}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [3, 2] ) 
 
rZDL33 = 	 Parameter(name='rZDL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL33}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [3, 3] ) 
 
iZDL33 = 	 Parameter(name='iZDL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL33}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [3, 3] ) 
 
rZDR11 = 	 Parameter(name='rZDR11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR11}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [1, 1] ) 
 
iZDR11 = 	 Parameter(name='iZDR11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR11}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [1, 1] ) 
 
rZDR12 = 	 Parameter(name='rZDR12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR12}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [1, 2] ) 
 
iZDR12 = 	 Parameter(name='iZDR12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR12}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [1, 2] ) 
 
rZDR13 = 	 Parameter(name='rZDR13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR13}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [1, 3] ) 
 
iZDR13 = 	 Parameter(name='iZDR13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR13}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [1, 3] ) 
 
rZDR21 = 	 Parameter(name='rZDR21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR21}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [2, 1] ) 
 
iZDR21 = 	 Parameter(name='iZDR21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR21}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [2, 1] ) 
 
rZDR22 = 	 Parameter(name='rZDR22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR22}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [2, 2] ) 
 
iZDR22 = 	 Parameter(name='iZDR22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR22}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [2, 2] ) 
 
rZDR23 = 	 Parameter(name='rZDR23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR23}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [2, 3] ) 
 
iZDR23 = 	 Parameter(name='iZDR23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR23}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [2, 3] ) 
 
rZDR31 = 	 Parameter(name='rZDR31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR31}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [3, 1] ) 
 
iZDR31 = 	 Parameter(name='iZDR31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR31}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [3, 1] ) 
 
rZDR32 = 	 Parameter(name='rZDR32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR32}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [3, 2] ) 
 
iZDR32 = 	 Parameter(name='iZDR32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR32}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [3, 2] ) 
 
rZDR33 = 	 Parameter(name='rZDR33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR33}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [3, 3] ) 
 
iZDR33 = 	 Parameter(name='iZDR33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR33}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [3, 3] ) 
 
rZUL11 = 	 Parameter(name='rZUL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL11}', 
	 lhablock = 'UULMIX', 
	 lhacode = [1, 1] ) 
 
iZUL11 = 	 Parameter(name='iZUL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL11}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [1, 1] ) 
 
rZUL12 = 	 Parameter(name='rZUL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL12}', 
	 lhablock = 'UULMIX', 
	 lhacode = [1, 2] ) 
 
iZUL12 = 	 Parameter(name='iZUL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL12}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [1, 2] ) 
 
rZUL13 = 	 Parameter(name='rZUL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL13}', 
	 lhablock = 'UULMIX', 
	 lhacode = [1, 3] ) 
 
iZUL13 = 	 Parameter(name='iZUL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL13}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [1, 3] ) 
 
rZUL21 = 	 Parameter(name='rZUL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL21}', 
	 lhablock = 'UULMIX', 
	 lhacode = [2, 1] ) 
 
iZUL21 = 	 Parameter(name='iZUL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL21}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [2, 1] ) 
 
rZUL22 = 	 Parameter(name='rZUL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL22}', 
	 lhablock = 'UULMIX', 
	 lhacode = [2, 2] ) 
 
iZUL22 = 	 Parameter(name='iZUL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL22}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [2, 2] ) 
 
rZUL23 = 	 Parameter(name='rZUL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL23}', 
	 lhablock = 'UULMIX', 
	 lhacode = [2, 3] ) 
 
iZUL23 = 	 Parameter(name='iZUL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL23}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [2, 3] ) 
 
rZUL31 = 	 Parameter(name='rZUL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL31}', 
	 lhablock = 'UULMIX', 
	 lhacode = [3, 1] ) 
 
iZUL31 = 	 Parameter(name='iZUL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL31}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [3, 1] ) 
 
rZUL32 = 	 Parameter(name='rZUL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL32}', 
	 lhablock = 'UULMIX', 
	 lhacode = [3, 2] ) 
 
iZUL32 = 	 Parameter(name='iZUL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL32}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [3, 2] ) 
 
rZUL33 = 	 Parameter(name='rZUL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL33}', 
	 lhablock = 'UULMIX', 
	 lhacode = [3, 3] ) 
 
iZUL33 = 	 Parameter(name='iZUL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL33}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [3, 3] ) 
 
rZUR11 = 	 Parameter(name='rZUR11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR11}', 
	 lhablock = 'UURMIX', 
	 lhacode = [1, 1] ) 
 
iZUR11 = 	 Parameter(name='iZUR11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR11}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [1, 1] ) 
 
rZUR12 = 	 Parameter(name='rZUR12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR12}', 
	 lhablock = 'UURMIX', 
	 lhacode = [1, 2] ) 
 
iZUR12 = 	 Parameter(name='iZUR12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR12}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [1, 2] ) 
 
rZUR13 = 	 Parameter(name='rZUR13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR13}', 
	 lhablock = 'UURMIX', 
	 lhacode = [1, 3] ) 
 
iZUR13 = 	 Parameter(name='iZUR13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR13}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [1, 3] ) 
 
rZUR21 = 	 Parameter(name='rZUR21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR21}', 
	 lhablock = 'UURMIX', 
	 lhacode = [2, 1] ) 
 
iZUR21 = 	 Parameter(name='iZUR21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR21}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [2, 1] ) 
 
rZUR22 = 	 Parameter(name='rZUR22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR22}', 
	 lhablock = 'UURMIX', 
	 lhacode = [2, 2] ) 
 
iZUR22 = 	 Parameter(name='iZUR22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR22}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [2, 2] ) 
 
rZUR23 = 	 Parameter(name='rZUR23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR23}', 
	 lhablock = 'UURMIX', 
	 lhacode = [2, 3] ) 
 
iZUR23 = 	 Parameter(name='iZUR23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR23}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [2, 3] ) 
 
rZUR31 = 	 Parameter(name='rZUR31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR31}', 
	 lhablock = 'UURMIX', 
	 lhacode = [3, 1] ) 
 
iZUR31 = 	 Parameter(name='iZUR31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR31}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [3, 1] ) 
 
rZUR32 = 	 Parameter(name='rZUR32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR32}', 
	 lhablock = 'UURMIX', 
	 lhacode = [3, 2] ) 
 
iZUR32 = 	 Parameter(name='iZUR32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR32}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [3, 2] ) 
 
rZUR33 = 	 Parameter(name='rZUR33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR33}', 
	 lhablock = 'UURMIX', 
	 lhacode = [3, 3] ) 
 
iZUR33 = 	 Parameter(name='iZUR33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR33}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [3, 3] ) 
 
alphaH = 	 Parameter(name='alphaH', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{alphaH}', 
	 lhablock = 'HMIX', 
	 lhacode = [11] ) 
 
betaH = 	 Parameter(name='betaH', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{betaH}', 
	 lhablock = 'HMIX', 
	 lhacode = [10] ) 
 
aS = 	 Parameter(name='aS', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.119, 
	 texname = '\\text{aS}', 
	 lhablock = 'SMINPUTS', 
	 lhacode = [3] ) 
 
aEWM1 = 	 Parameter(name='aEWM1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 137.035999679, 
	 texname = '\\text{aEWM1}', 
	 lhablock = 'SMINPUTS', 
	 lhacode = [1] ) 
 
Gf = 	 Parameter(name='Gf', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0000116639, 
	 texname = '\\text{Gf}', 
	 lhablock = 'SMINPUTS', 
	 lhacode = [2] ) 
 
Mu = 	 Parameter(name='Mu', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rMu + complex(0,1)*iMu', 
	 texname = '\\text{Mu}' ) 
 
Yd11 = 	 Parameter(name='Yd11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd11 + complex(0,1)*iYd11', 
	 texname = '\\text{Yd11}' ) 
 
Yd12 = 	 Parameter(name='Yd12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd12 + complex(0,1)*iYd12', 
	 texname = '\\text{Yd12}' ) 
 
Yd13 = 	 Parameter(name='Yd13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd13 + complex(0,1)*iYd13', 
	 texname = '\\text{Yd13}' ) 
 
Yd21 = 	 Parameter(name='Yd21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd21 + complex(0,1)*iYd21', 
	 texname = '\\text{Yd21}' ) 
 
Yd22 = 	 Parameter(name='Yd22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd22 + complex(0,1)*iYd22', 
	 texname = '\\text{Yd22}' ) 
 
Yd23 = 	 Parameter(name='Yd23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd23 + complex(0,1)*iYd23', 
	 texname = '\\text{Yd23}' ) 
 
Yd31 = 	 Parameter(name='Yd31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd31 + complex(0,1)*iYd31', 
	 texname = '\\text{Yd31}' ) 
 
Yd32 = 	 Parameter(name='Yd32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd32 + complex(0,1)*iYd32', 
	 texname = '\\text{Yd32}' ) 
 
Yd33 = 	 Parameter(name='Yd33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYd33 + complex(0,1)*iYd33', 
	 texname = '\\text{Yd33}' ) 
 
Td11 = 	 Parameter(name='Td11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd11 + complex(0,1)*iTd11', 
	 texname = '\\text{Td11}' ) 
 
Td12 = 	 Parameter(name='Td12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd12 + complex(0,1)*iTd12', 
	 texname = '\\text{Td12}' ) 
 
Td13 = 	 Parameter(name='Td13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd13 + complex(0,1)*iTd13', 
	 texname = '\\text{Td13}' ) 
 
Td21 = 	 Parameter(name='Td21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd21 + complex(0,1)*iTd21', 
	 texname = '\\text{Td21}' ) 
 
Td22 = 	 Parameter(name='Td22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd22 + complex(0,1)*iTd22', 
	 texname = '\\text{Td22}' ) 
 
Td23 = 	 Parameter(name='Td23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd23 + complex(0,1)*iTd23', 
	 texname = '\\text{Td23}' ) 
 
Td31 = 	 Parameter(name='Td31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd31 + complex(0,1)*iTd31', 
	 texname = '\\text{Td31}' ) 
 
Td32 = 	 Parameter(name='Td32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd32 + complex(0,1)*iTd32', 
	 texname = '\\text{Td32}' ) 
 
Td33 = 	 Parameter(name='Td33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTd33 + complex(0,1)*iTd33', 
	 texname = '\\text{Td33}' ) 
 
Ye11 = 	 Parameter(name='Ye11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYe11 + complex(0,1)*iYe11', 
	 texname = '\\text{Ye11}' ) 
 
Ye12 = 	 Parameter(name='Ye12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYe12 + complex(0,1)*iYe12', 
	 texname = '\\text{Ye12}' ) 
 
Ye13 = 	 Parameter(name='Ye13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYe13 + complex(0,1)*iYe13', 
	 texname = '\\text{Ye13}' ) 
 
Ye21 = 	 Parameter(name='Ye21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYe21 + complex(0,1)*iYe21', 
	 texname = '\\text{Ye21}' ) 
 
Ye22 = 	 Parameter(name='Ye22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYe22 + complex(0,1)*iYe22', 
	 texname = '\\text{Ye22}' ) 
 
Ye23 = 	 Parameter(name='Ye23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYe23 + complex(0,1)*iYe23', 
	 texname = '\\text{Ye23}' ) 
 
Ye31 = 	 Parameter(name='Ye31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYe31 + complex(0,1)*iYe31', 
	 texname = '\\text{Ye31}' ) 
 
Ye32 = 	 Parameter(name='Ye32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYe32 + complex(0,1)*iYe32', 
	 texname = '\\text{Ye32}' ) 
 
Ye33 = 	 Parameter(name='Ye33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYe33 + complex(0,1)*iYe33', 
	 texname = '\\text{Ye33}' ) 
 
Te11 = 	 Parameter(name='Te11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe11 + complex(0,1)*iTe11', 
	 texname = '\\text{Te11}' ) 
 
Te12 = 	 Parameter(name='Te12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe12 + complex(0,1)*iTe12', 
	 texname = '\\text{Te12}' ) 
 
Te13 = 	 Parameter(name='Te13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe13 + complex(0,1)*iTe13', 
	 texname = '\\text{Te13}' ) 
 
Te21 = 	 Parameter(name='Te21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe21 + complex(0,1)*iTe21', 
	 texname = '\\text{Te21}' ) 
 
Te22 = 	 Parameter(name='Te22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe22 + complex(0,1)*iTe22', 
	 texname = '\\text{Te22}' ) 
 
Te23 = 	 Parameter(name='Te23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe23 + complex(0,1)*iTe23', 
	 texname = '\\text{Te23}' ) 
 
Te31 = 	 Parameter(name='Te31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe31 + complex(0,1)*iTe31', 
	 texname = '\\text{Te31}' ) 
 
Te32 = 	 Parameter(name='Te32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe32 + complex(0,1)*iTe32', 
	 texname = '\\text{Te32}' ) 
 
Te33 = 	 Parameter(name='Te33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTe33 + complex(0,1)*iTe33', 
	 texname = '\\text{Te33}' ) 
 
L1111 = 	 Parameter(name='L1111', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1111 + complex(0,1)*iL1111', 
	 texname = '\\text{L1111}' ) 
 
L1112 = 	 Parameter(name='L1112', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1112 + complex(0,1)*iL1112', 
	 texname = '\\text{L1112}' ) 
 
L1113 = 	 Parameter(name='L1113', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1113 + complex(0,1)*iL1113', 
	 texname = '\\text{L1113}' ) 
 
L1121 = 	 Parameter(name='L1121', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1121 + complex(0,1)*iL1121', 
	 texname = '\\text{L1121}' ) 
 
L1122 = 	 Parameter(name='L1122', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1122 + complex(0,1)*iL1122', 
	 texname = '\\text{L1122}' ) 
 
L1123 = 	 Parameter(name='L1123', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1123 + complex(0,1)*iL1123', 
	 texname = '\\text{L1123}' ) 
 
L1131 = 	 Parameter(name='L1131', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1131 + complex(0,1)*iL1131', 
	 texname = '\\text{L1131}' ) 
 
L1132 = 	 Parameter(name='L1132', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1132 + complex(0,1)*iL1132', 
	 texname = '\\text{L1132}' ) 
 
L1133 = 	 Parameter(name='L1133', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1133 + complex(0,1)*iL1133', 
	 texname = '\\text{L1133}' ) 
 
L1211 = 	 Parameter(name='L1211', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1211 + complex(0,1)*iL1211', 
	 texname = '\\text{L1211}' ) 
 
L1212 = 	 Parameter(name='L1212', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1212 + complex(0,1)*iL1212', 
	 texname = '\\text{L1212}' ) 
 
L1213 = 	 Parameter(name='L1213', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1213 + complex(0,1)*iL1213', 
	 texname = '\\text{L1213}' ) 
 
L1221 = 	 Parameter(name='L1221', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1221 + complex(0,1)*iL1221', 
	 texname = '\\text{L1221}' ) 
 
L1222 = 	 Parameter(name='L1222', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1222 + complex(0,1)*iL1222', 
	 texname = '\\text{L1222}' ) 
 
L1223 = 	 Parameter(name='L1223', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1223 + complex(0,1)*iL1223', 
	 texname = '\\text{L1223}' ) 
 
L1231 = 	 Parameter(name='L1231', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1231 + complex(0,1)*iL1231', 
	 texname = '\\text{L1231}' ) 
 
L1232 = 	 Parameter(name='L1232', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1232 + complex(0,1)*iL1232', 
	 texname = '\\text{L1232}' ) 
 
L1233 = 	 Parameter(name='L1233', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1233 + complex(0,1)*iL1233', 
	 texname = '\\text{L1233}' ) 
 
L1311 = 	 Parameter(name='L1311', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1311 + complex(0,1)*iL1311', 
	 texname = '\\text{L1311}' ) 
 
L1312 = 	 Parameter(name='L1312', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1312 + complex(0,1)*iL1312', 
	 texname = '\\text{L1312}' ) 
 
L1313 = 	 Parameter(name='L1313', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1313 + complex(0,1)*iL1313', 
	 texname = '\\text{L1313}' ) 
 
L1321 = 	 Parameter(name='L1321', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1321 + complex(0,1)*iL1321', 
	 texname = '\\text{L1321}' ) 
 
L1322 = 	 Parameter(name='L1322', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1322 + complex(0,1)*iL1322', 
	 texname = '\\text{L1322}' ) 
 
L1323 = 	 Parameter(name='L1323', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1323 + complex(0,1)*iL1323', 
	 texname = '\\text{L1323}' ) 
 
L1331 = 	 Parameter(name='L1331', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1331 + complex(0,1)*iL1331', 
	 texname = '\\text{L1331}' ) 
 
L1332 = 	 Parameter(name='L1332', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1332 + complex(0,1)*iL1332', 
	 texname = '\\text{L1332}' ) 
 
L1333 = 	 Parameter(name='L1333', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL1333 + complex(0,1)*iL1333', 
	 texname = '\\text{L1333}' ) 
 
T1111 = 	 Parameter(name='T1111', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1111 + complex(0,1)*iT1111', 
	 texname = '\\text{T1111}' ) 
 
T1112 = 	 Parameter(name='T1112', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1112 + complex(0,1)*iT1112', 
	 texname = '\\text{T1112}' ) 
 
T1113 = 	 Parameter(name='T1113', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1113 + complex(0,1)*iT1113', 
	 texname = '\\text{T1113}' ) 
 
T1121 = 	 Parameter(name='T1121', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1121 + complex(0,1)*iT1121', 
	 texname = '\\text{T1121}' ) 
 
T1122 = 	 Parameter(name='T1122', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1122 + complex(0,1)*iT1122', 
	 texname = '\\text{T1122}' ) 
 
T1123 = 	 Parameter(name='T1123', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1123 + complex(0,1)*iT1123', 
	 texname = '\\text{T1123}' ) 
 
T1131 = 	 Parameter(name='T1131', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1131 + complex(0,1)*iT1131', 
	 texname = '\\text{T1131}' ) 
 
T1132 = 	 Parameter(name='T1132', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1132 + complex(0,1)*iT1132', 
	 texname = '\\text{T1132}' ) 
 
T1133 = 	 Parameter(name='T1133', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1133 + complex(0,1)*iT1133', 
	 texname = '\\text{T1133}' ) 
 
T1211 = 	 Parameter(name='T1211', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1211 + complex(0,1)*iT1211', 
	 texname = '\\text{T1211}' ) 
 
T1212 = 	 Parameter(name='T1212', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1212 + complex(0,1)*iT1212', 
	 texname = '\\text{T1212}' ) 
 
T1213 = 	 Parameter(name='T1213', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1213 + complex(0,1)*iT1213', 
	 texname = '\\text{T1213}' ) 
 
T1221 = 	 Parameter(name='T1221', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1221 + complex(0,1)*iT1221', 
	 texname = '\\text{T1221}' ) 
 
T1222 = 	 Parameter(name='T1222', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1222 + complex(0,1)*iT1222', 
	 texname = '\\text{T1222}' ) 
 
T1223 = 	 Parameter(name='T1223', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1223 + complex(0,1)*iT1223', 
	 texname = '\\text{T1223}' ) 
 
T1231 = 	 Parameter(name='T1231', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1231 + complex(0,1)*iT1231', 
	 texname = '\\text{T1231}' ) 
 
T1232 = 	 Parameter(name='T1232', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1232 + complex(0,1)*iT1232', 
	 texname = '\\text{T1232}' ) 
 
T1233 = 	 Parameter(name='T1233', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1233 + complex(0,1)*iT1233', 
	 texname = '\\text{T1233}' ) 
 
T1311 = 	 Parameter(name='T1311', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1311 + complex(0,1)*iT1311', 
	 texname = '\\text{T1311}' ) 
 
T1312 = 	 Parameter(name='T1312', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1312 + complex(0,1)*iT1312', 
	 texname = '\\text{T1312}' ) 
 
T1313 = 	 Parameter(name='T1313', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1313 + complex(0,1)*iT1313', 
	 texname = '\\text{T1313}' ) 
 
T1321 = 	 Parameter(name='T1321', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1321 + complex(0,1)*iT1321', 
	 texname = '\\text{T1321}' ) 
 
T1322 = 	 Parameter(name='T1322', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1322 + complex(0,1)*iT1322', 
	 texname = '\\text{T1322}' ) 
 
T1323 = 	 Parameter(name='T1323', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1323 + complex(0,1)*iT1323', 
	 texname = '\\text{T1323}' ) 
 
T1331 = 	 Parameter(name='T1331', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1331 + complex(0,1)*iT1331', 
	 texname = '\\text{T1331}' ) 
 
T1332 = 	 Parameter(name='T1332', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1332 + complex(0,1)*iT1332', 
	 texname = '\\text{T1332}' ) 
 
T1333 = 	 Parameter(name='T1333', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT1333 + complex(0,1)*iT1333', 
	 texname = '\\text{T1333}' ) 
 
L2111 = 	 Parameter(name='L2111', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2111 + complex(0,1)*iL2111', 
	 texname = '\\text{L2111}' ) 
 
L2112 = 	 Parameter(name='L2112', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2112 + complex(0,1)*iL2112', 
	 texname = '\\text{L2112}' ) 
 
L2113 = 	 Parameter(name='L2113', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2113 + complex(0,1)*iL2113', 
	 texname = '\\text{L2113}' ) 
 
L2121 = 	 Parameter(name='L2121', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2121 + complex(0,1)*iL2121', 
	 texname = '\\text{L2121}' ) 
 
L2122 = 	 Parameter(name='L2122', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2122 + complex(0,1)*iL2122', 
	 texname = '\\text{L2122}' ) 
 
L2123 = 	 Parameter(name='L2123', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2123 + complex(0,1)*iL2123', 
	 texname = '\\text{L2123}' ) 
 
L2131 = 	 Parameter(name='L2131', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2131 + complex(0,1)*iL2131', 
	 texname = '\\text{L2131}' ) 
 
L2132 = 	 Parameter(name='L2132', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2132 + complex(0,1)*iL2132', 
	 texname = '\\text{L2132}' ) 
 
L2133 = 	 Parameter(name='L2133', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2133 + complex(0,1)*iL2133', 
	 texname = '\\text{L2133}' ) 
 
L2211 = 	 Parameter(name='L2211', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2211 + complex(0,1)*iL2211', 
	 texname = '\\text{L2211}' ) 
 
L2212 = 	 Parameter(name='L2212', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2212 + complex(0,1)*iL2212', 
	 texname = '\\text{L2212}' ) 
 
L2213 = 	 Parameter(name='L2213', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2213 + complex(0,1)*iL2213', 
	 texname = '\\text{L2213}' ) 
 
L2221 = 	 Parameter(name='L2221', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2221 + complex(0,1)*iL2221', 
	 texname = '\\text{L2221}' ) 
 
L2222 = 	 Parameter(name='L2222', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2222 + complex(0,1)*iL2222', 
	 texname = '\\text{L2222}' ) 
 
L2223 = 	 Parameter(name='L2223', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2223 + complex(0,1)*iL2223', 
	 texname = '\\text{L2223}' ) 
 
L2231 = 	 Parameter(name='L2231', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2231 + complex(0,1)*iL2231', 
	 texname = '\\text{L2231}' ) 
 
L2232 = 	 Parameter(name='L2232', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2232 + complex(0,1)*iL2232', 
	 texname = '\\text{L2232}' ) 
 
L2233 = 	 Parameter(name='L2233', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2233 + complex(0,1)*iL2233', 
	 texname = '\\text{L2233}' ) 
 
L2311 = 	 Parameter(name='L2311', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2311 + complex(0,1)*iL2311', 
	 texname = '\\text{L2311}' ) 
 
L2312 = 	 Parameter(name='L2312', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2312 + complex(0,1)*iL2312', 
	 texname = '\\text{L2312}' ) 
 
L2313 = 	 Parameter(name='L2313', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2313 + complex(0,1)*iL2313', 
	 texname = '\\text{L2313}' ) 
 
L2321 = 	 Parameter(name='L2321', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2321 + complex(0,1)*iL2321', 
	 texname = '\\text{L2321}' ) 
 
L2322 = 	 Parameter(name='L2322', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2322 + complex(0,1)*iL2322', 
	 texname = '\\text{L2322}' ) 
 
L2323 = 	 Parameter(name='L2323', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2323 + complex(0,1)*iL2323', 
	 texname = '\\text{L2323}' ) 
 
L2331 = 	 Parameter(name='L2331', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2331 + complex(0,1)*iL2331', 
	 texname = '\\text{L2331}' ) 
 
L2332 = 	 Parameter(name='L2332', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2332 + complex(0,1)*iL2332', 
	 texname = '\\text{L2332}' ) 
 
L2333 = 	 Parameter(name='L2333', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL2333 + complex(0,1)*iL2333', 
	 texname = '\\text{L2333}' ) 
 
T2111 = 	 Parameter(name='T2111', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2111 + complex(0,1)*iT2111', 
	 texname = '\\text{T2111}' ) 
 
T2112 = 	 Parameter(name='T2112', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2112 + complex(0,1)*iT2112', 
	 texname = '\\text{T2112}' ) 
 
T2113 = 	 Parameter(name='T2113', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2113 + complex(0,1)*iT2113', 
	 texname = '\\text{T2113}' ) 
 
T2121 = 	 Parameter(name='T2121', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2121 + complex(0,1)*iT2121', 
	 texname = '\\text{T2121}' ) 
 
T2122 = 	 Parameter(name='T2122', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2122 + complex(0,1)*iT2122', 
	 texname = '\\text{T2122}' ) 
 
T2123 = 	 Parameter(name='T2123', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2123 + complex(0,1)*iT2123', 
	 texname = '\\text{T2123}' ) 
 
T2131 = 	 Parameter(name='T2131', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2131 + complex(0,1)*iT2131', 
	 texname = '\\text{T2131}' ) 
 
T2132 = 	 Parameter(name='T2132', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2132 + complex(0,1)*iT2132', 
	 texname = '\\text{T2132}' ) 
 
T2133 = 	 Parameter(name='T2133', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2133 + complex(0,1)*iT2133', 
	 texname = '\\text{T2133}' ) 
 
T2211 = 	 Parameter(name='T2211', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2211 + complex(0,1)*iT2211', 
	 texname = '\\text{T2211}' ) 
 
T2212 = 	 Parameter(name='T2212', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2212 + complex(0,1)*iT2212', 
	 texname = '\\text{T2212}' ) 
 
T2213 = 	 Parameter(name='T2213', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2213 + complex(0,1)*iT2213', 
	 texname = '\\text{T2213}' ) 
 
T2221 = 	 Parameter(name='T2221', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2221 + complex(0,1)*iT2221', 
	 texname = '\\text{T2221}' ) 
 
T2222 = 	 Parameter(name='T2222', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2222 + complex(0,1)*iT2222', 
	 texname = '\\text{T2222}' ) 
 
T2223 = 	 Parameter(name='T2223', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2223 + complex(0,1)*iT2223', 
	 texname = '\\text{T2223}' ) 
 
T2231 = 	 Parameter(name='T2231', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2231 + complex(0,1)*iT2231', 
	 texname = '\\text{T2231}' ) 
 
T2232 = 	 Parameter(name='T2232', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2232 + complex(0,1)*iT2232', 
	 texname = '\\text{T2232}' ) 
 
T2233 = 	 Parameter(name='T2233', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2233 + complex(0,1)*iT2233', 
	 texname = '\\text{T2233}' ) 
 
T2311 = 	 Parameter(name='T2311', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2311 + complex(0,1)*iT2311', 
	 texname = '\\text{T2311}' ) 
 
T2312 = 	 Parameter(name='T2312', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2312 + complex(0,1)*iT2312', 
	 texname = '\\text{T2312}' ) 
 
T2313 = 	 Parameter(name='T2313', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2313 + complex(0,1)*iT2313', 
	 texname = '\\text{T2313}' ) 
 
T2321 = 	 Parameter(name='T2321', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2321 + complex(0,1)*iT2321', 
	 texname = '\\text{T2321}' ) 
 
T2322 = 	 Parameter(name='T2322', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2322 + complex(0,1)*iT2322', 
	 texname = '\\text{T2322}' ) 
 
T2323 = 	 Parameter(name='T2323', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2323 + complex(0,1)*iT2323', 
	 texname = '\\text{T2323}' ) 
 
T2331 = 	 Parameter(name='T2331', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2331 + complex(0,1)*iT2331', 
	 texname = '\\text{T2331}' ) 
 
T2332 = 	 Parameter(name='T2332', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2332 + complex(0,1)*iT2332', 
	 texname = '\\text{T2332}' ) 
 
T2333 = 	 Parameter(name='T2333', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT2333 + complex(0,1)*iT2333', 
	 texname = '\\text{T2333}' ) 
 
L3111 = 	 Parameter(name='L3111', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3111 + complex(0,1)*iL3111', 
	 texname = '\\text{L3111}' ) 
 
L3112 = 	 Parameter(name='L3112', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3112 + complex(0,1)*iL3112', 
	 texname = '\\text{L3112}' ) 
 
L3113 = 	 Parameter(name='L3113', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3113 + complex(0,1)*iL3113', 
	 texname = '\\text{L3113}' ) 
 
L3121 = 	 Parameter(name='L3121', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3121 + complex(0,1)*iL3121', 
	 texname = '\\text{L3121}' ) 
 
L3122 = 	 Parameter(name='L3122', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3122 + complex(0,1)*iL3122', 
	 texname = '\\text{L3122}' ) 
 
L3123 = 	 Parameter(name='L3123', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3123 + complex(0,1)*iL3123', 
	 texname = '\\text{L3123}' ) 
 
L3131 = 	 Parameter(name='L3131', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3131 + complex(0,1)*iL3131', 
	 texname = '\\text{L3131}' ) 
 
L3132 = 	 Parameter(name='L3132', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3132 + complex(0,1)*iL3132', 
	 texname = '\\text{L3132}' ) 
 
L3133 = 	 Parameter(name='L3133', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3133 + complex(0,1)*iL3133', 
	 texname = '\\text{L3133}' ) 
 
L3211 = 	 Parameter(name='L3211', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3211 + complex(0,1)*iL3211', 
	 texname = '\\text{L3211}' ) 
 
L3212 = 	 Parameter(name='L3212', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3212 + complex(0,1)*iL3212', 
	 texname = '\\text{L3212}' ) 
 
L3213 = 	 Parameter(name='L3213', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3213 + complex(0,1)*iL3213', 
	 texname = '\\text{L3213}' ) 
 
L3221 = 	 Parameter(name='L3221', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3221 + complex(0,1)*iL3221', 
	 texname = '\\text{L3221}' ) 
 
L3222 = 	 Parameter(name='L3222', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3222 + complex(0,1)*iL3222', 
	 texname = '\\text{L3222}' ) 
 
L3223 = 	 Parameter(name='L3223', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3223 + complex(0,1)*iL3223', 
	 texname = '\\text{L3223}' ) 
 
L3231 = 	 Parameter(name='L3231', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3231 + complex(0,1)*iL3231', 
	 texname = '\\text{L3231}' ) 
 
L3232 = 	 Parameter(name='L3232', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3232 + complex(0,1)*iL3232', 
	 texname = '\\text{L3232}' ) 
 
L3233 = 	 Parameter(name='L3233', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3233 + complex(0,1)*iL3233', 
	 texname = '\\text{L3233}' ) 
 
L3311 = 	 Parameter(name='L3311', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3311 + complex(0,1)*iL3311', 
	 texname = '\\text{L3311}' ) 
 
L3312 = 	 Parameter(name='L3312', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3312 + complex(0,1)*iL3312', 
	 texname = '\\text{L3312}' ) 
 
L3313 = 	 Parameter(name='L3313', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3313 + complex(0,1)*iL3313', 
	 texname = '\\text{L3313}' ) 
 
L3321 = 	 Parameter(name='L3321', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3321 + complex(0,1)*iL3321', 
	 texname = '\\text{L3321}' ) 
 
L3322 = 	 Parameter(name='L3322', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3322 + complex(0,1)*iL3322', 
	 texname = '\\text{L3322}' ) 
 
L3323 = 	 Parameter(name='L3323', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3323 + complex(0,1)*iL3323', 
	 texname = '\\text{L3323}' ) 
 
L3331 = 	 Parameter(name='L3331', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3331 + complex(0,1)*iL3331', 
	 texname = '\\text{L3331}' ) 
 
L3332 = 	 Parameter(name='L3332', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3332 + complex(0,1)*iL3332', 
	 texname = '\\text{L3332}' ) 
 
L3333 = 	 Parameter(name='L3333', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rL3333 + complex(0,1)*iL3333', 
	 texname = '\\text{L3333}' ) 
 
T3111 = 	 Parameter(name='T3111', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3111 + complex(0,1)*iT3111', 
	 texname = '\\text{T3111}' ) 
 
T3112 = 	 Parameter(name='T3112', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3112 + complex(0,1)*iT3112', 
	 texname = '\\text{T3112}' ) 
 
T3113 = 	 Parameter(name='T3113', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3113 + complex(0,1)*iT3113', 
	 texname = '\\text{T3113}' ) 
 
T3121 = 	 Parameter(name='T3121', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3121 + complex(0,1)*iT3121', 
	 texname = '\\text{T3121}' ) 
 
T3122 = 	 Parameter(name='T3122', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3122 + complex(0,1)*iT3122', 
	 texname = '\\text{T3122}' ) 
 
T3123 = 	 Parameter(name='T3123', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3123 + complex(0,1)*iT3123', 
	 texname = '\\text{T3123}' ) 
 
T3131 = 	 Parameter(name='T3131', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3131 + complex(0,1)*iT3131', 
	 texname = '\\text{T3131}' ) 
 
T3132 = 	 Parameter(name='T3132', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3132 + complex(0,1)*iT3132', 
	 texname = '\\text{T3132}' ) 
 
T3133 = 	 Parameter(name='T3133', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3133 + complex(0,1)*iT3133', 
	 texname = '\\text{T3133}' ) 
 
T3211 = 	 Parameter(name='T3211', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3211 + complex(0,1)*iT3211', 
	 texname = '\\text{T3211}' ) 
 
T3212 = 	 Parameter(name='T3212', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3212 + complex(0,1)*iT3212', 
	 texname = '\\text{T3212}' ) 
 
T3213 = 	 Parameter(name='T3213', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3213 + complex(0,1)*iT3213', 
	 texname = '\\text{T3213}' ) 
 
T3221 = 	 Parameter(name='T3221', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3221 + complex(0,1)*iT3221', 
	 texname = '\\text{T3221}' ) 
 
T3222 = 	 Parameter(name='T3222', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3222 + complex(0,1)*iT3222', 
	 texname = '\\text{T3222}' ) 
 
T3223 = 	 Parameter(name='T3223', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3223 + complex(0,1)*iT3223', 
	 texname = '\\text{T3223}' ) 
 
T3231 = 	 Parameter(name='T3231', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3231 + complex(0,1)*iT3231', 
	 texname = '\\text{T3231}' ) 
 
T3232 = 	 Parameter(name='T3232', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3232 + complex(0,1)*iT3232', 
	 texname = '\\text{T3232}' ) 
 
T3233 = 	 Parameter(name='T3233', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3233 + complex(0,1)*iT3233', 
	 texname = '\\text{T3233}' ) 
 
T3311 = 	 Parameter(name='T3311', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3311 + complex(0,1)*iT3311', 
	 texname = '\\text{T3311}' ) 
 
T3312 = 	 Parameter(name='T3312', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3312 + complex(0,1)*iT3312', 
	 texname = '\\text{T3312}' ) 
 
T3313 = 	 Parameter(name='T3313', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3313 + complex(0,1)*iT3313', 
	 texname = '\\text{T3313}' ) 
 
T3321 = 	 Parameter(name='T3321', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3321 + complex(0,1)*iT3321', 
	 texname = '\\text{T3321}' ) 
 
T3322 = 	 Parameter(name='T3322', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3322 + complex(0,1)*iT3322', 
	 texname = '\\text{T3322}' ) 
 
T3323 = 	 Parameter(name='T3323', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3323 + complex(0,1)*iT3323', 
	 texname = '\\text{T3323}' ) 
 
T3331 = 	 Parameter(name='T3331', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3331 + complex(0,1)*iT3331', 
	 texname = '\\text{T3331}' ) 
 
T3332 = 	 Parameter(name='T3332', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3332 + complex(0,1)*iT3332', 
	 texname = '\\text{T3332}' ) 
 
T3333 = 	 Parameter(name='T3333', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rT3333 + complex(0,1)*iT3333', 
	 texname = '\\text{T3333}' ) 
 
Yu11 = 	 Parameter(name='Yu11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu11 + complex(0,1)*iYu11', 
	 texname = '\\text{Yu11}' ) 
 
Yu12 = 	 Parameter(name='Yu12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu12 + complex(0,1)*iYu12', 
	 texname = '\\text{Yu12}' ) 
 
Yu13 = 	 Parameter(name='Yu13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu13 + complex(0,1)*iYu13', 
	 texname = '\\text{Yu13}' ) 
 
Yu21 = 	 Parameter(name='Yu21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu21 + complex(0,1)*iYu21', 
	 texname = '\\text{Yu21}' ) 
 
Yu22 = 	 Parameter(name='Yu22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu22 + complex(0,1)*iYu22', 
	 texname = '\\text{Yu22}' ) 
 
Yu23 = 	 Parameter(name='Yu23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu23 + complex(0,1)*iYu23', 
	 texname = '\\text{Yu23}' ) 
 
Yu31 = 	 Parameter(name='Yu31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu31 + complex(0,1)*iYu31', 
	 texname = '\\text{Yu31}' ) 
 
Yu32 = 	 Parameter(name='Yu32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu32 + complex(0,1)*iYu32', 
	 texname = '\\text{Yu32}' ) 
 
Yu33 = 	 Parameter(name='Yu33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rYu33 + complex(0,1)*iYu33', 
	 texname = '\\text{Yu33}' ) 
 
Tu11 = 	 Parameter(name='Tu11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu11 + complex(0,1)*iTu11', 
	 texname = '\\text{Tu11}' ) 
 
Tu12 = 	 Parameter(name='Tu12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu12 + complex(0,1)*iTu12', 
	 texname = '\\text{Tu12}' ) 
 
Tu13 = 	 Parameter(name='Tu13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu13 + complex(0,1)*iTu13', 
	 texname = '\\text{Tu13}' ) 
 
Tu21 = 	 Parameter(name='Tu21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu21 + complex(0,1)*iTu21', 
	 texname = '\\text{Tu21}' ) 
 
Tu22 = 	 Parameter(name='Tu22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu22 + complex(0,1)*iTu22', 
	 texname = '\\text{Tu22}' ) 
 
Tu23 = 	 Parameter(name='Tu23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu23 + complex(0,1)*iTu23', 
	 texname = '\\text{Tu23}' ) 
 
Tu31 = 	 Parameter(name='Tu31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu31 + complex(0,1)*iTu31', 
	 texname = '\\text{Tu31}' ) 
 
Tu32 = 	 Parameter(name='Tu32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu32 + complex(0,1)*iTu32', 
	 texname = '\\text{Tu32}' ) 
 
Tu33 = 	 Parameter(name='Tu33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rTu33 + complex(0,1)*iTu33', 
	 texname = '\\text{Tu33}' ) 
 
pG = 	 Parameter(name='pG', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rpG + complex(0,1)*ipG', 
	 texname = '\\text{pG}' ) 
 
ZD11 = 	 Parameter(name='ZD11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD11 + complex(0,1)*iZD11', 
	 texname = '\\text{ZD11}' ) 
 
ZD12 = 	 Parameter(name='ZD12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD12 + complex(0,1)*iZD12', 
	 texname = '\\text{ZD12}' ) 
 
ZD13 = 	 Parameter(name='ZD13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD13 + complex(0,1)*iZD13', 
	 texname = '\\text{ZD13}' ) 
 
ZD14 = 	 Parameter(name='ZD14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD14 + complex(0,1)*iZD14', 
	 texname = '\\text{ZD14}' ) 
 
ZD15 = 	 Parameter(name='ZD15', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD15 + complex(0,1)*iZD15', 
	 texname = '\\text{ZD15}' ) 
 
ZD16 = 	 Parameter(name='ZD16', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD16 + complex(0,1)*iZD16', 
	 texname = '\\text{ZD16}' ) 
 
ZD21 = 	 Parameter(name='ZD21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD21 + complex(0,1)*iZD21', 
	 texname = '\\text{ZD21}' ) 
 
ZD22 = 	 Parameter(name='ZD22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD22 + complex(0,1)*iZD22', 
	 texname = '\\text{ZD22}' ) 
 
ZD23 = 	 Parameter(name='ZD23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD23 + complex(0,1)*iZD23', 
	 texname = '\\text{ZD23}' ) 
 
ZD24 = 	 Parameter(name='ZD24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD24 + complex(0,1)*iZD24', 
	 texname = '\\text{ZD24}' ) 
 
ZD25 = 	 Parameter(name='ZD25', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD25 + complex(0,1)*iZD25', 
	 texname = '\\text{ZD25}' ) 
 
ZD26 = 	 Parameter(name='ZD26', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD26 + complex(0,1)*iZD26', 
	 texname = '\\text{ZD26}' ) 
 
ZD31 = 	 Parameter(name='ZD31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD31 + complex(0,1)*iZD31', 
	 texname = '\\text{ZD31}' ) 
 
ZD32 = 	 Parameter(name='ZD32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD32 + complex(0,1)*iZD32', 
	 texname = '\\text{ZD32}' ) 
 
ZD33 = 	 Parameter(name='ZD33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD33 + complex(0,1)*iZD33', 
	 texname = '\\text{ZD33}' ) 
 
ZD34 = 	 Parameter(name='ZD34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD34 + complex(0,1)*iZD34', 
	 texname = '\\text{ZD34}' ) 
 
ZD35 = 	 Parameter(name='ZD35', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD35 + complex(0,1)*iZD35', 
	 texname = '\\text{ZD35}' ) 
 
ZD36 = 	 Parameter(name='ZD36', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD36 + complex(0,1)*iZD36', 
	 texname = '\\text{ZD36}' ) 
 
ZD41 = 	 Parameter(name='ZD41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD41 + complex(0,1)*iZD41', 
	 texname = '\\text{ZD41}' ) 
 
ZD42 = 	 Parameter(name='ZD42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD42 + complex(0,1)*iZD42', 
	 texname = '\\text{ZD42}' ) 
 
ZD43 = 	 Parameter(name='ZD43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD43 + complex(0,1)*iZD43', 
	 texname = '\\text{ZD43}' ) 
 
ZD44 = 	 Parameter(name='ZD44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD44 + complex(0,1)*iZD44', 
	 texname = '\\text{ZD44}' ) 
 
ZD45 = 	 Parameter(name='ZD45', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD45 + complex(0,1)*iZD45', 
	 texname = '\\text{ZD45}' ) 
 
ZD46 = 	 Parameter(name='ZD46', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD46 + complex(0,1)*iZD46', 
	 texname = '\\text{ZD46}' ) 
 
ZD51 = 	 Parameter(name='ZD51', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD51 + complex(0,1)*iZD51', 
	 texname = '\\text{ZD51}' ) 
 
ZD52 = 	 Parameter(name='ZD52', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD52 + complex(0,1)*iZD52', 
	 texname = '\\text{ZD52}' ) 
 
ZD53 = 	 Parameter(name='ZD53', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD53 + complex(0,1)*iZD53', 
	 texname = '\\text{ZD53}' ) 
 
ZD54 = 	 Parameter(name='ZD54', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD54 + complex(0,1)*iZD54', 
	 texname = '\\text{ZD54}' ) 
 
ZD55 = 	 Parameter(name='ZD55', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD55 + complex(0,1)*iZD55', 
	 texname = '\\text{ZD55}' ) 
 
ZD56 = 	 Parameter(name='ZD56', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD56 + complex(0,1)*iZD56', 
	 texname = '\\text{ZD56}' ) 
 
ZD61 = 	 Parameter(name='ZD61', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD61 + complex(0,1)*iZD61', 
	 texname = '\\text{ZD61}' ) 
 
ZD62 = 	 Parameter(name='ZD62', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD62 + complex(0,1)*iZD62', 
	 texname = '\\text{ZD62}' ) 
 
ZD63 = 	 Parameter(name='ZD63', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD63 + complex(0,1)*iZD63', 
	 texname = '\\text{ZD63}' ) 
 
ZD64 = 	 Parameter(name='ZD64', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD64 + complex(0,1)*iZD64', 
	 texname = '\\text{ZD64}' ) 
 
ZD65 = 	 Parameter(name='ZD65', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD65 + complex(0,1)*iZD65', 
	 texname = '\\text{ZD65}' ) 
 
ZD66 = 	 Parameter(name='ZD66', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZD66 + complex(0,1)*iZD66', 
	 texname = '\\text{ZD66}' ) 
 
ZV11 = 	 Parameter(name='ZV11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZV11 + complex(0,1)*iZV11', 
	 texname = '\\text{ZV11}' ) 
 
ZV12 = 	 Parameter(name='ZV12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZV12 + complex(0,1)*iZV12', 
	 texname = '\\text{ZV12}' ) 
 
ZV13 = 	 Parameter(name='ZV13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZV13 + complex(0,1)*iZV13', 
	 texname = '\\text{ZV13}' ) 
 
ZV21 = 	 Parameter(name='ZV21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZV21 + complex(0,1)*iZV21', 
	 texname = '\\text{ZV21}' ) 
 
ZV22 = 	 Parameter(name='ZV22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZV22 + complex(0,1)*iZV22', 
	 texname = '\\text{ZV22}' ) 
 
ZV23 = 	 Parameter(name='ZV23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZV23 + complex(0,1)*iZV23', 
	 texname = '\\text{ZV23}' ) 
 
ZV31 = 	 Parameter(name='ZV31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZV31 + complex(0,1)*iZV31', 
	 texname = '\\text{ZV31}' ) 
 
ZV32 = 	 Parameter(name='ZV32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZV32 + complex(0,1)*iZV32', 
	 texname = '\\text{ZV32}' ) 
 
ZV33 = 	 Parameter(name='ZV33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZV33 + complex(0,1)*iZV33', 
	 texname = '\\text{ZV33}' ) 
 
ZU11 = 	 Parameter(name='ZU11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU11 + complex(0,1)*iZU11', 
	 texname = '\\text{ZU11}' ) 
 
ZU12 = 	 Parameter(name='ZU12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU12 + complex(0,1)*iZU12', 
	 texname = '\\text{ZU12}' ) 
 
ZU13 = 	 Parameter(name='ZU13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU13 + complex(0,1)*iZU13', 
	 texname = '\\text{ZU13}' ) 
 
ZU14 = 	 Parameter(name='ZU14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU14 + complex(0,1)*iZU14', 
	 texname = '\\text{ZU14}' ) 
 
ZU15 = 	 Parameter(name='ZU15', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU15 + complex(0,1)*iZU15', 
	 texname = '\\text{ZU15}' ) 
 
ZU16 = 	 Parameter(name='ZU16', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU16 + complex(0,1)*iZU16', 
	 texname = '\\text{ZU16}' ) 
 
ZU21 = 	 Parameter(name='ZU21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU21 + complex(0,1)*iZU21', 
	 texname = '\\text{ZU21}' ) 
 
ZU22 = 	 Parameter(name='ZU22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU22 + complex(0,1)*iZU22', 
	 texname = '\\text{ZU22}' ) 
 
ZU23 = 	 Parameter(name='ZU23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU23 + complex(0,1)*iZU23', 
	 texname = '\\text{ZU23}' ) 
 
ZU24 = 	 Parameter(name='ZU24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU24 + complex(0,1)*iZU24', 
	 texname = '\\text{ZU24}' ) 
 
ZU25 = 	 Parameter(name='ZU25', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU25 + complex(0,1)*iZU25', 
	 texname = '\\text{ZU25}' ) 
 
ZU26 = 	 Parameter(name='ZU26', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU26 + complex(0,1)*iZU26', 
	 texname = '\\text{ZU26}' ) 
 
ZU31 = 	 Parameter(name='ZU31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU31 + complex(0,1)*iZU31', 
	 texname = '\\text{ZU31}' ) 
 
ZU32 = 	 Parameter(name='ZU32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU32 + complex(0,1)*iZU32', 
	 texname = '\\text{ZU32}' ) 
 
ZU33 = 	 Parameter(name='ZU33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU33 + complex(0,1)*iZU33', 
	 texname = '\\text{ZU33}' ) 
 
ZU34 = 	 Parameter(name='ZU34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU34 + complex(0,1)*iZU34', 
	 texname = '\\text{ZU34}' ) 
 
ZU35 = 	 Parameter(name='ZU35', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU35 + complex(0,1)*iZU35', 
	 texname = '\\text{ZU35}' ) 
 
ZU36 = 	 Parameter(name='ZU36', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU36 + complex(0,1)*iZU36', 
	 texname = '\\text{ZU36}' ) 
 
ZU41 = 	 Parameter(name='ZU41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU41 + complex(0,1)*iZU41', 
	 texname = '\\text{ZU41}' ) 
 
ZU42 = 	 Parameter(name='ZU42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU42 + complex(0,1)*iZU42', 
	 texname = '\\text{ZU42}' ) 
 
ZU43 = 	 Parameter(name='ZU43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU43 + complex(0,1)*iZU43', 
	 texname = '\\text{ZU43}' ) 
 
ZU44 = 	 Parameter(name='ZU44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU44 + complex(0,1)*iZU44', 
	 texname = '\\text{ZU44}' ) 
 
ZU45 = 	 Parameter(name='ZU45', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU45 + complex(0,1)*iZU45', 
	 texname = '\\text{ZU45}' ) 
 
ZU46 = 	 Parameter(name='ZU46', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU46 + complex(0,1)*iZU46', 
	 texname = '\\text{ZU46}' ) 
 
ZU51 = 	 Parameter(name='ZU51', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU51 + complex(0,1)*iZU51', 
	 texname = '\\text{ZU51}' ) 
 
ZU52 = 	 Parameter(name='ZU52', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU52 + complex(0,1)*iZU52', 
	 texname = '\\text{ZU52}' ) 
 
ZU53 = 	 Parameter(name='ZU53', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU53 + complex(0,1)*iZU53', 
	 texname = '\\text{ZU53}' ) 
 
ZU54 = 	 Parameter(name='ZU54', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU54 + complex(0,1)*iZU54', 
	 texname = '\\text{ZU54}' ) 
 
ZU55 = 	 Parameter(name='ZU55', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU55 + complex(0,1)*iZU55', 
	 texname = '\\text{ZU55}' ) 
 
ZU56 = 	 Parameter(name='ZU56', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU56 + complex(0,1)*iZU56', 
	 texname = '\\text{ZU56}' ) 
 
ZU61 = 	 Parameter(name='ZU61', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU61 + complex(0,1)*iZU61', 
	 texname = '\\text{ZU61}' ) 
 
ZU62 = 	 Parameter(name='ZU62', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU62 + complex(0,1)*iZU62', 
	 texname = '\\text{ZU62}' ) 
 
ZU63 = 	 Parameter(name='ZU63', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU63 + complex(0,1)*iZU63', 
	 texname = '\\text{ZU63}' ) 
 
ZU64 = 	 Parameter(name='ZU64', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU64 + complex(0,1)*iZU64', 
	 texname = '\\text{ZU64}' ) 
 
ZU65 = 	 Parameter(name='ZU65', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU65 + complex(0,1)*iZU65', 
	 texname = '\\text{ZU65}' ) 
 
ZU66 = 	 Parameter(name='ZU66', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZU66 + complex(0,1)*iZU66', 
	 texname = '\\text{ZU66}' ) 
 
ZE11 = 	 Parameter(name='ZE11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE11 + complex(0,1)*iZE11', 
	 texname = '\\text{ZE11}' ) 
 
ZE12 = 	 Parameter(name='ZE12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE12 + complex(0,1)*iZE12', 
	 texname = '\\text{ZE12}' ) 
 
ZE13 = 	 Parameter(name='ZE13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE13 + complex(0,1)*iZE13', 
	 texname = '\\text{ZE13}' ) 
 
ZE14 = 	 Parameter(name='ZE14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE14 + complex(0,1)*iZE14', 
	 texname = '\\text{ZE14}' ) 
 
ZE15 = 	 Parameter(name='ZE15', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE15 + complex(0,1)*iZE15', 
	 texname = '\\text{ZE15}' ) 
 
ZE16 = 	 Parameter(name='ZE16', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE16 + complex(0,1)*iZE16', 
	 texname = '\\text{ZE16}' ) 
 
ZE21 = 	 Parameter(name='ZE21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE21 + complex(0,1)*iZE21', 
	 texname = '\\text{ZE21}' ) 
 
ZE22 = 	 Parameter(name='ZE22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE22 + complex(0,1)*iZE22', 
	 texname = '\\text{ZE22}' ) 
 
ZE23 = 	 Parameter(name='ZE23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE23 + complex(0,1)*iZE23', 
	 texname = '\\text{ZE23}' ) 
 
ZE24 = 	 Parameter(name='ZE24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE24 + complex(0,1)*iZE24', 
	 texname = '\\text{ZE24}' ) 
 
ZE25 = 	 Parameter(name='ZE25', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE25 + complex(0,1)*iZE25', 
	 texname = '\\text{ZE25}' ) 
 
ZE26 = 	 Parameter(name='ZE26', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE26 + complex(0,1)*iZE26', 
	 texname = '\\text{ZE26}' ) 
 
ZE31 = 	 Parameter(name='ZE31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE31 + complex(0,1)*iZE31', 
	 texname = '\\text{ZE31}' ) 
 
ZE32 = 	 Parameter(name='ZE32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE32 + complex(0,1)*iZE32', 
	 texname = '\\text{ZE32}' ) 
 
ZE33 = 	 Parameter(name='ZE33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE33 + complex(0,1)*iZE33', 
	 texname = '\\text{ZE33}' ) 
 
ZE34 = 	 Parameter(name='ZE34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE34 + complex(0,1)*iZE34', 
	 texname = '\\text{ZE34}' ) 
 
ZE35 = 	 Parameter(name='ZE35', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE35 + complex(0,1)*iZE35', 
	 texname = '\\text{ZE35}' ) 
 
ZE36 = 	 Parameter(name='ZE36', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE36 + complex(0,1)*iZE36', 
	 texname = '\\text{ZE36}' ) 
 
ZE41 = 	 Parameter(name='ZE41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE41 + complex(0,1)*iZE41', 
	 texname = '\\text{ZE41}' ) 
 
ZE42 = 	 Parameter(name='ZE42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE42 + complex(0,1)*iZE42', 
	 texname = '\\text{ZE42}' ) 
 
ZE43 = 	 Parameter(name='ZE43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE43 + complex(0,1)*iZE43', 
	 texname = '\\text{ZE43}' ) 
 
ZE44 = 	 Parameter(name='ZE44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE44 + complex(0,1)*iZE44', 
	 texname = '\\text{ZE44}' ) 
 
ZE45 = 	 Parameter(name='ZE45', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE45 + complex(0,1)*iZE45', 
	 texname = '\\text{ZE45}' ) 
 
ZE46 = 	 Parameter(name='ZE46', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE46 + complex(0,1)*iZE46', 
	 texname = '\\text{ZE46}' ) 
 
ZE51 = 	 Parameter(name='ZE51', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE51 + complex(0,1)*iZE51', 
	 texname = '\\text{ZE51}' ) 
 
ZE52 = 	 Parameter(name='ZE52', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE52 + complex(0,1)*iZE52', 
	 texname = '\\text{ZE52}' ) 
 
ZE53 = 	 Parameter(name='ZE53', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE53 + complex(0,1)*iZE53', 
	 texname = '\\text{ZE53}' ) 
 
ZE54 = 	 Parameter(name='ZE54', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE54 + complex(0,1)*iZE54', 
	 texname = '\\text{ZE54}' ) 
 
ZE55 = 	 Parameter(name='ZE55', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE55 + complex(0,1)*iZE55', 
	 texname = '\\text{ZE55}' ) 
 
ZE56 = 	 Parameter(name='ZE56', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE56 + complex(0,1)*iZE56', 
	 texname = '\\text{ZE56}' ) 
 
ZE61 = 	 Parameter(name='ZE61', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE61 + complex(0,1)*iZE61', 
	 texname = '\\text{ZE61}' ) 
 
ZE62 = 	 Parameter(name='ZE62', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE62 + complex(0,1)*iZE62', 
	 texname = '\\text{ZE62}' ) 
 
ZE63 = 	 Parameter(name='ZE63', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE63 + complex(0,1)*iZE63', 
	 texname = '\\text{ZE63}' ) 
 
ZE64 = 	 Parameter(name='ZE64', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE64 + complex(0,1)*iZE64', 
	 texname = '\\text{ZE64}' ) 
 
ZE65 = 	 Parameter(name='ZE65', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE65 + complex(0,1)*iZE65', 
	 texname = '\\text{ZE65}' ) 
 
ZE66 = 	 Parameter(name='ZE66', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZE66 + complex(0,1)*iZE66', 
	 texname = '\\text{ZE66}' ) 
 
ZN11 = 	 Parameter(name='ZN11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN11 + complex(0,1)*iZN11', 
	 texname = '\\text{ZN11}' ) 
 
ZN12 = 	 Parameter(name='ZN12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN12 + complex(0,1)*iZN12', 
	 texname = '\\text{ZN12}' ) 
 
ZN13 = 	 Parameter(name='ZN13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN13 + complex(0,1)*iZN13', 
	 texname = '\\text{ZN13}' ) 
 
ZN14 = 	 Parameter(name='ZN14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN14 + complex(0,1)*iZN14', 
	 texname = '\\text{ZN14}' ) 
 
ZN21 = 	 Parameter(name='ZN21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN21 + complex(0,1)*iZN21', 
	 texname = '\\text{ZN21}' ) 
 
ZN22 = 	 Parameter(name='ZN22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN22 + complex(0,1)*iZN22', 
	 texname = '\\text{ZN22}' ) 
 
ZN23 = 	 Parameter(name='ZN23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN23 + complex(0,1)*iZN23', 
	 texname = '\\text{ZN23}' ) 
 
ZN24 = 	 Parameter(name='ZN24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN24 + complex(0,1)*iZN24', 
	 texname = '\\text{ZN24}' ) 
 
ZN31 = 	 Parameter(name='ZN31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN31 + complex(0,1)*iZN31', 
	 texname = '\\text{ZN31}' ) 
 
ZN32 = 	 Parameter(name='ZN32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN32 + complex(0,1)*iZN32', 
	 texname = '\\text{ZN32}' ) 
 
ZN33 = 	 Parameter(name='ZN33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN33 + complex(0,1)*iZN33', 
	 texname = '\\text{ZN33}' ) 
 
ZN34 = 	 Parameter(name='ZN34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN34 + complex(0,1)*iZN34', 
	 texname = '\\text{ZN34}' ) 
 
ZN41 = 	 Parameter(name='ZN41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN41 + complex(0,1)*iZN41', 
	 texname = '\\text{ZN41}' ) 
 
ZN42 = 	 Parameter(name='ZN42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN42 + complex(0,1)*iZN42', 
	 texname = '\\text{ZN42}' ) 
 
ZN43 = 	 Parameter(name='ZN43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN43 + complex(0,1)*iZN43', 
	 texname = '\\text{ZN43}' ) 
 
ZN44 = 	 Parameter(name='ZN44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZN44 + complex(0,1)*iZN44', 
	 texname = '\\text{ZN44}' ) 
 
UM11 = 	 Parameter(name='UM11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUM11 + complex(0,1)*iUM11', 
	 texname = '\\text{UM11}' ) 
 
UM12 = 	 Parameter(name='UM12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUM12 + complex(0,1)*iUM12', 
	 texname = '\\text{UM12}' ) 
 
UM21 = 	 Parameter(name='UM21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUM21 + complex(0,1)*iUM21', 
	 texname = '\\text{UM21}' ) 
 
UM22 = 	 Parameter(name='UM22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUM22 + complex(0,1)*iUM22', 
	 texname = '\\text{UM22}' ) 
 
UP11 = 	 Parameter(name='UP11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUP11 + complex(0,1)*iUP11', 
	 texname = '\\text{UP11}' ) 
 
UP12 = 	 Parameter(name='UP12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUP12 + complex(0,1)*iUP12', 
	 texname = '\\text{UP12}' ) 
 
UP21 = 	 Parameter(name='UP21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUP21 + complex(0,1)*iUP21', 
	 texname = '\\text{UP21}' ) 
 
UP22 = 	 Parameter(name='UP22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rUP22 + complex(0,1)*iUP22', 
	 texname = '\\text{UP22}' ) 
 
ZEL11 = 	 Parameter(name='ZEL11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL11 + complex(0,1)*iZEL11', 
	 texname = '\\text{ZEL11}' ) 
 
ZEL12 = 	 Parameter(name='ZEL12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL12 + complex(0,1)*iZEL12', 
	 texname = '\\text{ZEL12}' ) 
 
ZEL13 = 	 Parameter(name='ZEL13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL13 + complex(0,1)*iZEL13', 
	 texname = '\\text{ZEL13}' ) 
 
ZEL21 = 	 Parameter(name='ZEL21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL21 + complex(0,1)*iZEL21', 
	 texname = '\\text{ZEL21}' ) 
 
ZEL22 = 	 Parameter(name='ZEL22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL22 + complex(0,1)*iZEL22', 
	 texname = '\\text{ZEL22}' ) 
 
ZEL23 = 	 Parameter(name='ZEL23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL23 + complex(0,1)*iZEL23', 
	 texname = '\\text{ZEL23}' ) 
 
ZEL31 = 	 Parameter(name='ZEL31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL31 + complex(0,1)*iZEL31', 
	 texname = '\\text{ZEL31}' ) 
 
ZEL32 = 	 Parameter(name='ZEL32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL32 + complex(0,1)*iZEL32', 
	 texname = '\\text{ZEL32}' ) 
 
ZEL33 = 	 Parameter(name='ZEL33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL33 + complex(0,1)*iZEL33', 
	 texname = '\\text{ZEL33}' ) 
 
ZER11 = 	 Parameter(name='ZER11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER11 + complex(0,1)*iZER11', 
	 texname = '\\text{ZER11}' ) 
 
ZER12 = 	 Parameter(name='ZER12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER12 + complex(0,1)*iZER12', 
	 texname = '\\text{ZER12}' ) 
 
ZER13 = 	 Parameter(name='ZER13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER13 + complex(0,1)*iZER13', 
	 texname = '\\text{ZER13}' ) 
 
ZER21 = 	 Parameter(name='ZER21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER21 + complex(0,1)*iZER21', 
	 texname = '\\text{ZER21}' ) 
 
ZER22 = 	 Parameter(name='ZER22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER22 + complex(0,1)*iZER22', 
	 texname = '\\text{ZER22}' ) 
 
ZER23 = 	 Parameter(name='ZER23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER23 + complex(0,1)*iZER23', 
	 texname = '\\text{ZER23}' ) 
 
ZER31 = 	 Parameter(name='ZER31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER31 + complex(0,1)*iZER31', 
	 texname = '\\text{ZER31}' ) 
 
ZER32 = 	 Parameter(name='ZER32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER32 + complex(0,1)*iZER32', 
	 texname = '\\text{ZER32}' ) 
 
ZER33 = 	 Parameter(name='ZER33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER33 + complex(0,1)*iZER33', 
	 texname = '\\text{ZER33}' ) 
 
ZDL11 = 	 Parameter(name='ZDL11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL11 + complex(0,1)*iZDL11', 
	 texname = '\\text{ZDL11}' ) 
 
ZDL12 = 	 Parameter(name='ZDL12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL12 + complex(0,1)*iZDL12', 
	 texname = '\\text{ZDL12}' ) 
 
ZDL13 = 	 Parameter(name='ZDL13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL13 + complex(0,1)*iZDL13', 
	 texname = '\\text{ZDL13}' ) 
 
ZDL21 = 	 Parameter(name='ZDL21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL21 + complex(0,1)*iZDL21', 
	 texname = '\\text{ZDL21}' ) 
 
ZDL22 = 	 Parameter(name='ZDL22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL22 + complex(0,1)*iZDL22', 
	 texname = '\\text{ZDL22}' ) 
 
ZDL23 = 	 Parameter(name='ZDL23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL23 + complex(0,1)*iZDL23', 
	 texname = '\\text{ZDL23}' ) 
 
ZDL31 = 	 Parameter(name='ZDL31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL31 + complex(0,1)*iZDL31', 
	 texname = '\\text{ZDL31}' ) 
 
ZDL32 = 	 Parameter(name='ZDL32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL32 + complex(0,1)*iZDL32', 
	 texname = '\\text{ZDL32}' ) 
 
ZDL33 = 	 Parameter(name='ZDL33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL33 + complex(0,1)*iZDL33', 
	 texname = '\\text{ZDL33}' ) 
 
ZDR11 = 	 Parameter(name='ZDR11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR11 + complex(0,1)*iZDR11', 
	 texname = '\\text{ZDR11}' ) 
 
ZDR12 = 	 Parameter(name='ZDR12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR12 + complex(0,1)*iZDR12', 
	 texname = '\\text{ZDR12}' ) 
 
ZDR13 = 	 Parameter(name='ZDR13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR13 + complex(0,1)*iZDR13', 
	 texname = '\\text{ZDR13}' ) 
 
ZDR21 = 	 Parameter(name='ZDR21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR21 + complex(0,1)*iZDR21', 
	 texname = '\\text{ZDR21}' ) 
 
ZDR22 = 	 Parameter(name='ZDR22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR22 + complex(0,1)*iZDR22', 
	 texname = '\\text{ZDR22}' ) 
 
ZDR23 = 	 Parameter(name='ZDR23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR23 + complex(0,1)*iZDR23', 
	 texname = '\\text{ZDR23}' ) 
 
ZDR31 = 	 Parameter(name='ZDR31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR31 + complex(0,1)*iZDR31', 
	 texname = '\\text{ZDR31}' ) 
 
ZDR32 = 	 Parameter(name='ZDR32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR32 + complex(0,1)*iZDR32', 
	 texname = '\\text{ZDR32}' ) 
 
ZDR33 = 	 Parameter(name='ZDR33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR33 + complex(0,1)*iZDR33', 
	 texname = '\\text{ZDR33}' ) 
 
ZUL11 = 	 Parameter(name='ZUL11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL11 + complex(0,1)*iZUL11', 
	 texname = '\\text{ZUL11}' ) 
 
ZUL12 = 	 Parameter(name='ZUL12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL12 + complex(0,1)*iZUL12', 
	 texname = '\\text{ZUL12}' ) 
 
ZUL13 = 	 Parameter(name='ZUL13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL13 + complex(0,1)*iZUL13', 
	 texname = '\\text{ZUL13}' ) 
 
ZUL21 = 	 Parameter(name='ZUL21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL21 + complex(0,1)*iZUL21', 
	 texname = '\\text{ZUL21}' ) 
 
ZUL22 = 	 Parameter(name='ZUL22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL22 + complex(0,1)*iZUL22', 
	 texname = '\\text{ZUL22}' ) 
 
ZUL23 = 	 Parameter(name='ZUL23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL23 + complex(0,1)*iZUL23', 
	 texname = '\\text{ZUL23}' ) 
 
ZUL31 = 	 Parameter(name='ZUL31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL31 + complex(0,1)*iZUL31', 
	 texname = '\\text{ZUL31}' ) 
 
ZUL32 = 	 Parameter(name='ZUL32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL32 + complex(0,1)*iZUL32', 
	 texname = '\\text{ZUL32}' ) 
 
ZUL33 = 	 Parameter(name='ZUL33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL33 + complex(0,1)*iZUL33', 
	 texname = '\\text{ZUL33}' ) 
 
ZUR11 = 	 Parameter(name='ZUR11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR11 + complex(0,1)*iZUR11', 
	 texname = '\\text{ZUR11}' ) 
 
ZUR12 = 	 Parameter(name='ZUR12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR12 + complex(0,1)*iZUR12', 
	 texname = '\\text{ZUR12}' ) 
 
ZUR13 = 	 Parameter(name='ZUR13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR13 + complex(0,1)*iZUR13', 
	 texname = '\\text{ZUR13}' ) 
 
ZUR21 = 	 Parameter(name='ZUR21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR21 + complex(0,1)*iZUR21', 
	 texname = '\\text{ZUR21}' ) 
 
ZUR22 = 	 Parameter(name='ZUR22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR22 + complex(0,1)*iZUR22', 
	 texname = '\\text{ZUR22}' ) 
 
ZUR23 = 	 Parameter(name='ZUR23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR23 + complex(0,1)*iZUR23', 
	 texname = '\\text{ZUR23}' ) 
 
ZUR31 = 	 Parameter(name='ZUR31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR31 + complex(0,1)*iZUR31', 
	 texname = '\\text{ZUR31}' ) 
 
ZUR32 = 	 Parameter(name='ZUR32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR32 + complex(0,1)*iZUR32', 
	 texname = '\\text{ZUR32}' ) 
 
ZUR33 = 	 Parameter(name='ZUR33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR33 + complex(0,1)*iZUR33', 
	 texname = '\\text{ZUR33}' ) 
 
G = 	 Parameter(name='G', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)', 
	 texname = 'G') 
 
ZH11 = 	 Parameter(name='ZH11', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '-cmath.sin(alphaH)', 
	 texname = 'ZH11') 
 
ZH12 = 	 Parameter(name='ZH12', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.cos(alphaH)', 
	 texname = 'ZH12') 
 
ZH21 = 	 Parameter(name='ZH21', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.cos(alphaH)', 
	 texname = 'ZH21') 
 
ZH22 = 	 Parameter(name='ZH22', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.sin(alphaH)', 
	 texname = 'ZH22') 
 
ZA11 = 	 Parameter(name='ZA11', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '-cmath.cos(betaH)', 
	 texname = 'ZA11') 
 
ZA12 = 	 Parameter(name='ZA12', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.sin(betaH)', 
	 texname = 'ZA12') 
 
ZA21 = 	 Parameter(name='ZA21', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.sin(betaH)', 
	 texname = 'ZA21') 
 
ZA22 = 	 Parameter(name='ZA22', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.cos(betaH)', 
	 texname = 'ZA22') 
 
ZP11 = 	 Parameter(name='ZP11', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '-cmath.cos(betaH)', 
	 texname = 'ZP11') 
 
ZP12 = 	 Parameter(name='ZP12', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.sin(betaH)', 
	 texname = 'ZP12') 
 
ZP21 = 	 Parameter(name='ZP21', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.sin(betaH)', 
	 texname = 'ZP21') 
 
ZP22 = 	 Parameter(name='ZP22', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.cos(betaH)', 
	 texname = 'ZP22') 
 
el = 	 Parameter(name='el', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '2*cmath.sqrt(1/aEWM1)*cmath.sqrt(cmath.pi)', 
	 texname = 'el') 
 
MWm = 	 Parameter(name='MWm', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (MZ**2*cmath.pi)/(cmath.sqrt(2)*aEWM1*Gf)))', 
	 texname = 'MWm') 
 
TW = 	 Parameter(name='TW', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.asin(cmath.sqrt(1 - MWm**2/MZ**2))', 
	 texname = 'TW') 
 
g1 = 	 Parameter(name='g1', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'el*1./cmath.cos(TW)', 
	 texname = 'g1') 
 
g2 = 	 Parameter(name='g2', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'el*1./cmath.sin(TW)', 
	 texname = 'g2') 
 
v = 	 Parameter(name='v', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '2*cmath.sqrt(MWm**2/g2**2)', 
	 texname = 'v') 
 
vd = 	 Parameter(name='vd', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'v*cmath.cos(betaH)', 
	 texname = 'vd') 
 
vu = 	 Parameter(name='vu', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'v*cmath.sin(betaH)', 
	 texname = 'vu') 
 
RXiWm = 	 Parameter(name='RXiWm', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '1.', 
	 texname = 'RXiWm') 
 
RXiZ = 	 Parameter(name='RXiZ', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '1.', 
	 texname = 'RXiZ') 
 
