# This file was automatically created by FeynRules 2.0.0
# Mathematica version: 8.0 for Mac OS X x86 (64-bit) (February 23, 2011)
# Date: Fri 11 Oct 2013 22:27:55


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

QGR = CouplingOrder(name = 'QGR',
                    expansion_order = 99,
                    hierarchy = 1)

