# This file was automatically created by FeynRules 2.4.43
# Mathematica version: 10.3.0 for Mac OS X x86 (64-bit) (October 9, 2015)
# Date: Wed 30 Nov 2016 08:56:02


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_1000_1 = Coupling(name = 'R2GC_1000_1',
                       value = '-(cw*ee*complex(0,1)*G**2*NN1x1)/(72.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*NN1x2)/(24.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*NN1x2*sw)/(24.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_1002_2 = Coupling(name = 'R2GC_1002_2',
                       value = '-(cw*ee*complex(0,1)*G**2*NN2x1)/(72.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*NN2x2)/(24.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*NN2x2*sw)/(24.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_1005_3 = Coupling(name = 'R2GC_1005_3',
                       value = '-(cw*ee*complex(0,1)*G**2*NN2x1)/(72.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*NN2x2)/(24.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*NN2x2*sw)/(24.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_1008_4 = Coupling(name = 'R2GC_1008_4',
                       value = '-(cw*ee*complex(0,1)*G**2*NN3x1)/(72.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*NN3x2)/(24.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*NN3x2*sw)/(24.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_1011_5 = Coupling(name = 'R2GC_1011_5',
                       value = '-(cw*ee*complex(0,1)*G**2*NN3x1)/(72.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*NN3x2)/(24.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*NN3x2*sw)/(24.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_1014_6 = Coupling(name = 'R2GC_1014_6',
                       value = '-(cw*ee*complex(0,1)*G**2*NN4x1)/(72.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*NN4x2)/(24.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*NN4x2*sw)/(24.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_1017_7 = Coupling(name = 'R2GC_1017_7',
                       value = '-(cw*ee*complex(0,1)*G**2*NN4x1)/(72.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*NN4x2)/(24.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*NN4x2*sw)/(24.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_1020_8 = Coupling(name = 'R2GC_1020_8',
                       value = '(ee*complex(0,1)*G**2*UU1x1)/(24.*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1})

R2GC_1023_9 = Coupling(name = 'R2GC_1023_9',
                       value = '(ee*complex(0,1)*G**2*UU2x1)/(24.*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1})

R2GC_1026_10 = Coupling(name = 'R2GC_1026_10',
                        value = '(ee*complex(0,1)*G**2*VV1x1)/(24.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':1})

R2GC_1029_11 = Coupling(name = 'R2GC_1029_11',
                        value = '(ee*complex(0,1)*G**2*VV2x1)/(24.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':1})

R2GC_1032_12 = Coupling(name = 'R2GC_1032_12',
                        value = '-(complex(0,1)*G**2*NN1x4*yu3x3)/(24.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*NN1x4*sw**2*yu3x3)/(24.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_1032_13 = Coupling(name = 'R2GC_1032_13',
                        value = '-(complex(0,1)*G**2*NN1x4*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*NN1x4*sw**2*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_1034_14 = Coupling(name = 'R2GC_1034_14',
                        value = '-(complex(0,1)*G**2*NN2x4*yu3x3)/(24.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*NN2x4*sw**2*yu3x3)/(24.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_1034_15 = Coupling(name = 'R2GC_1034_15',
                        value = '-(complex(0,1)*G**2*NN2x4*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*NN2x4*sw**2*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_1036_16 = Coupling(name = 'R2GC_1036_16',
                        value = '-(complex(0,1)*G**2*NN3x4*yu3x3)/(24.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*NN3x4*sw**2*yu3x3)/(24.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_1036_17 = Coupling(name = 'R2GC_1036_17',
                        value = '-(complex(0,1)*G**2*NN3x4*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*NN3x4*sw**2*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_1038_18 = Coupling(name = 'R2GC_1038_18',
                        value = '-(complex(0,1)*G**2*NN4x4*yu3x3)/(24.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*NN4x4*sw**2*yu3x3)/(24.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_1038_19 = Coupling(name = 'R2GC_1038_19',
                        value = '-(complex(0,1)*G**2*NN4x4*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*NN4x4*sw**2*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_1040_20 = Coupling(name = 'R2GC_1040_20',
                        value = '-(complex(0,1)*G**2*VV1x2*yu3x3)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_1040_21 = Coupling(name = 'R2GC_1040_21',
                        value = '-(complex(0,1)*G**2*VV1x2*yu3x3)/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_1042_22 = Coupling(name = 'R2GC_1042_22',
                        value = '-(complex(0,1)*G**2*VV2x2*yu3x3)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_1042_23 = Coupling(name = 'R2GC_1042_23',
                        value = '-(complex(0,1)*G**2*VV2x2*yu3x3)/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_247_24 = Coupling(name = 'R2GC_247_24',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_248_25 = Coupling(name = 'R2GC_248_25',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_249_26 = Coupling(name = 'R2GC_249_26',
                       value = '-G**3/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_250_27 = Coupling(name = 'R2GC_250_27',
                       value = '(complex(0,1)*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_251_28 = Coupling(name = 'R2GC_251_28',
                       value = '(-3*complex(0,1)*G**2*Mgo**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_251_29 = Coupling(name = 'R2GC_251_29',
                       value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_252_30 = Coupling(name = 'R2GC_252_30',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_252_31 = Coupling(name = 'R2GC_252_31',
                       value = '(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_253_32 = Coupling(name = 'R2GC_253_32',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_253_33 = Coupling(name = 'R2GC_253_33',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_254_34 = Coupling(name = 'R2GC_254_34',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_254_35 = Coupling(name = 'R2GC_254_35',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_255_36 = Coupling(name = 'R2GC_255_36',
                       value = '-(cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                       order = {'QCD':2,'QED':2})

R2GC_255_37 = Coupling(name = 'R2GC_255_37',
                       value = '-(cw**2*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                       order = {'QCD':2,'QED':2})

R2GC_256_38 = Coupling(name = 'R2GC_256_38',
                       value = '(cw*ee*G**2)/(48.*cmath.pi**2*sw*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_256_39 = Coupling(name = 'R2GC_256_39',
                       value = '-(cw*ee*G**2)/(48.*cmath.pi**2*sw*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_257_40 = Coupling(name = 'R2GC_257_40',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw*(-1 + sw**2))',
                       order = {'QCD':3,'QED':1})

R2GC_257_41 = Coupling(name = 'R2GC_257_41',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw*(-1 + sw**2))',
                       order = {'QCD':3,'QED':1})

R2GC_258_42 = Coupling(name = 'R2GC_258_42',
                       value = '-(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*sw)/(216.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':2})

R2GC_258_43 = Coupling(name = 'R2GC_258_43',
                       value = '-(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*sw)/(54.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':2})

R2GC_259_44 = Coupling(name = 'R2GC_259_44',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**3*sw)/(144.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':3,'QED':1})

R2GC_259_45 = Coupling(name = 'R2GC_259_45',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**3*sw)/(72.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':3,'QED':1})

R2GC_260_46 = Coupling(name = 'R2GC_260_46',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_262_47 = Coupling(name = 'R2GC_262_47',
                       value = '-(complex(0,1)*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_264_48 = Coupling(name = 'R2GC_264_48',
                       value = '(ee*complex(0,1)*G**2)/(96.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_264_49 = Coupling(name = 'R2GC_264_49',
                       value = '-(ee*complex(0,1)*G**2)/(96.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_264_50 = Coupling(name = 'R2GC_264_50',
                       value = '-(ee*complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_264_51 = Coupling(name = 'R2GC_264_51',
                       value = '(ee*complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_265_52 = Coupling(name = 'R2GC_265_52',
                       value = '-G**3/(32.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_266_53 = Coupling(name = 'R2GC_266_53',
                       value = '(cw*ee*complex(0,1)*G**2*NN1x1)/(192.*cmath.pi**2*(-1 + sw**2)) - (ee*complex(0,1)*G**2*NN1x2)/(64.*cmath.pi**2*sw*(-1 + sw**2)) + (ee*complex(0,1)*G**2*NN1x2*sw)/(64.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_266_54 = Coupling(name = 'R2GC_266_54',
                       value = '(cw*ee*complex(0,1)*G**2*NN1x1)/(96.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_266_55 = Coupling(name = 'R2GC_266_55',
                       value = '(cw*ee*complex(0,1)*G**2*NN1x1)/(192.*cmath.pi**2*(-1 + sw**2)) + (ee*complex(0,1)*G**2*NN1x2)/(64.*cmath.pi**2*sw*(-1 + sw**2)) - (ee*complex(0,1)*G**2*NN1x2*sw)/(64.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_266_56 = Coupling(name = 'R2GC_266_56',
                       value = '-(cw*ee*complex(0,1)*G**2*NN1x1)/(48.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_267_57 = Coupling(name = 'R2GC_267_57',
                       value = '(cw*ee*complex(0,1)*G**2*NN2x1)/(192.*cmath.pi**2*(-1 + sw**2)) - (ee*complex(0,1)*G**2*NN2x2)/(64.*cmath.pi**2*sw*(-1 + sw**2)) + (ee*complex(0,1)*G**2*NN2x2*sw)/(64.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_267_58 = Coupling(name = 'R2GC_267_58',
                       value = '(cw*ee*complex(0,1)*G**2*NN2x1)/(96.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_267_59 = Coupling(name = 'R2GC_267_59',
                       value = '(cw*ee*complex(0,1)*G**2*NN2x1)/(192.*cmath.pi**2*(-1 + sw**2)) + (ee*complex(0,1)*G**2*NN2x2)/(64.*cmath.pi**2*sw*(-1 + sw**2)) - (ee*complex(0,1)*G**2*NN2x2*sw)/(64.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_267_60 = Coupling(name = 'R2GC_267_60',
                       value = '-(cw*ee*complex(0,1)*G**2*NN2x1)/(48.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_268_61 = Coupling(name = 'R2GC_268_61',
                       value = '(cw*ee*complex(0,1)*G**2*NN3x1)/(192.*cmath.pi**2*(-1 + sw**2)) - (ee*complex(0,1)*G**2*NN3x2)/(64.*cmath.pi**2*sw*(-1 + sw**2)) + (ee*complex(0,1)*G**2*NN3x2*sw)/(64.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_268_62 = Coupling(name = 'R2GC_268_62',
                       value = '(cw*ee*complex(0,1)*G**2*NN3x1)/(96.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_268_63 = Coupling(name = 'R2GC_268_63',
                       value = '(cw*ee*complex(0,1)*G**2*NN3x1)/(192.*cmath.pi**2*(-1 + sw**2)) + (ee*complex(0,1)*G**2*NN3x2)/(64.*cmath.pi**2*sw*(-1 + sw**2)) - (ee*complex(0,1)*G**2*NN3x2*sw)/(64.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_268_64 = Coupling(name = 'R2GC_268_64',
                       value = '-(cw*ee*complex(0,1)*G**2*NN3x1)/(48.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_269_65 = Coupling(name = 'R2GC_269_65',
                       value = '(cw*ee*complex(0,1)*G**2*NN4x1)/(192.*cmath.pi**2*(-1 + sw**2)) - (ee*complex(0,1)*G**2*NN4x2)/(64.*cmath.pi**2*sw*(-1 + sw**2)) + (ee*complex(0,1)*G**2*NN4x2*sw)/(64.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_269_66 = Coupling(name = 'R2GC_269_66',
                       value = '(cw*ee*complex(0,1)*G**2*NN4x1)/(96.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_269_67 = Coupling(name = 'R2GC_269_67',
                       value = '(cw*ee*complex(0,1)*G**2*NN4x1)/(192.*cmath.pi**2*(-1 + sw**2)) + (ee*complex(0,1)*G**2*NN4x2)/(64.*cmath.pi**2*sw*(-1 + sw**2)) - (ee*complex(0,1)*G**2*NN4x2*sw)/(64.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_269_68 = Coupling(name = 'R2GC_269_68',
                       value = '-(cw*ee*complex(0,1)*G**2*NN4x1)/(48.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_270_69 = Coupling(name = 'R2GC_270_69',
                       value = '-(cw*ee*complex(0,1)*G**2)/(64.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*sw)/(96.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_270_70 = Coupling(name = 'R2GC_270_70',
                       value = '-(cw*ee*complex(0,1)*G**2*sw)/(96.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_270_71 = Coupling(name = 'R2GC_270_71',
                       value = '(cw*ee*complex(0,1)*G**2)/(64.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*sw)/(48.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_270_72 = Coupling(name = 'R2GC_270_72',
                       value = '(cw*ee*complex(0,1)*G**2*sw)/(48.*cmath.pi**2*(-1 + sw**2))',
                       order = {'QCD':2,'QED':1})

R2GC_297_73 = Coupling(name = 'R2GC_297_73',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_297_74 = Coupling(name = 'R2GC_297_74',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_297_75 = Coupling(name = 'R2GC_297_75',
                       value = '-G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_298_76 = Coupling(name = 'R2GC_298_76',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_298_77 = Coupling(name = 'R2GC_298_77',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_298_78 = Coupling(name = 'R2GC_298_78',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_299_79 = Coupling(name = 'R2GC_299_79',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_300_80 = Coupling(name = 'R2GC_300_80',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_300_81 = Coupling(name = 'R2GC_300_81',
                       value = '(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_301_82 = Coupling(name = 'R2GC_301_82',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_301_83 = Coupling(name = 'R2GC_301_83',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_301_84 = Coupling(name = 'R2GC_301_84',
                       value = '(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_302_85 = Coupling(name = 'R2GC_302_85',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_303_86 = Coupling(name = 'R2GC_303_86',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_305_87 = Coupling(name = 'R2GC_305_87',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_305_88 = Coupling(name = 'R2GC_305_88',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_305_89 = Coupling(name = 'R2GC_305_89',
                       value = 'G**3/(8.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_306_90 = Coupling(name = 'R2GC_306_90',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_306_91 = Coupling(name = 'R2GC_306_91',
                       value = '(-3*complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_307_92 = Coupling(name = 'R2GC_307_92',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_307_93 = Coupling(name = 'R2GC_307_93',
                       value = '(-9*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_308_94 = Coupling(name = 'R2GC_308_94',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_310_95 = Coupling(name = 'R2GC_310_95',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_310_96 = Coupling(name = 'R2GC_310_96',
                       value = '(23*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_311_97 = Coupling(name = 'R2GC_311_97',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_311_98 = Coupling(name = 'R2GC_311_98',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_311_99 = Coupling(name = 'R2GC_311_99',
                       value = '(5*complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_313_100 = Coupling(name = 'R2GC_313_100',
                        value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_314_101 = Coupling(name = 'R2GC_314_101',
                        value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_316_102 = Coupling(name = 'R2GC_316_102',
                        value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_321_103 = Coupling(name = 'R2GC_321_103',
                        value = '(13*complex(0,1)*G**4)/(384.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_333_104 = Coupling(name = 'R2GC_333_104',
                        value = '-(complex(0,1)*G**2)/(18.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_333_105 = Coupling(name = 'R2GC_333_105',
                        value = '-(complex(0,1)*G**2)/(72.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_335_106 = Coupling(name = 'R2GC_335_106',
                        value = '-(ee*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_335_107 = Coupling(name = 'R2GC_335_107',
                        value = '-(ee*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_337_108 = Coupling(name = 'R2GC_337_108',
                        value = '(-4*ee**2*complex(0,1)*G**2)/(81.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_337_109 = Coupling(name = 'R2GC_337_109',
                        value = '-(ee**2*complex(0,1)*G**2)/(324.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_339_110 = Coupling(name = 'R2GC_339_110',
                        value = '(complex(0,1)*G**3)/(9.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_339_111 = Coupling(name = 'R2GC_339_111',
                        value = '(53*complex(0,1)*G**3)/(576.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_341_112 = Coupling(name = 'R2GC_341_112',
                        value = '(37*ee*complex(0,1)*G**3)/(432.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_341_113 = Coupling(name = 'R2GC_341_113',
                        value = '(53*ee*complex(0,1)*G**3)/(864.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_343_114 = Coupling(name = 'R2GC_343_114',
                        value = '(-3*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_343_115 = Coupling(name = 'R2GC_343_115',
                        value = '(-2*complex(0,1)*G**4)/(9.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_343_116 = Coupling(name = 'R2GC_343_116',
                        value = '(-79*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_345_117 = Coupling(name = 'R2GC_345_117',
                        value = '(complex(0,1)*G**2*Mgo**2)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_345_118 = Coupling(name = 'R2GC_345_118',
                        value = '(complex(0,1)*G**2*MsbL**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_346_119 = Coupling(name = 'R2GC_346_119',
                        value = '(complex(0,1)*G**2*MsbR**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_347_120 = Coupling(name = 'R2GC_347_120',
                        value = '(ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_348_121 = Coupling(name = 'R2GC_348_121',
                        value = '(-4*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(81.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_348_122 = Coupling(name = 'R2GC_348_122',
                        value = '-(ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(162.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_349_123 = Coupling(name = 'R2GC_349_123',
                        value = '(-13*complex(0,1)*G**4)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (13*complex(0,1)*G**4*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (13*complex(0,1)*G**4*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_349_124 = Coupling(name = 'R2GC_349_124',
                        value = '(55*complex(0,1)*G**4)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (55*complex(0,1)*G**4*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (55*complex(0,1)*G**4*sw**4)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_349_125 = Coupling(name = 'R2GC_349_125',
                        value = '(61*complex(0,1)*G**4)/(1728.*cmath.pi**2*(-1 + sw**2)**2) - (61*complex(0,1)*G**4*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**2) + (61*complex(0,1)*G**4*sw**4)/(1728.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_351_126 = Coupling(name = 'R2GC_351_126',
                        value = '-(ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_352_127 = Coupling(name = 'R2GC_352_127',
                        value = '-(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_353_128 = Coupling(name = 'R2GC_353_128',
                        value = '(ee**2*complex(0,1)*G**2*Rtau1x1**2)/(144.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*Rtau1x2**2)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_354_129 = Coupling(name = 'R2GC_354_129',
                        value = '(ee**2*complex(0,1)*G**2*Rtau1x1*Rtau2x1)/(144.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*Rtau1x2*Rtau2x2)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_355_130 = Coupling(name = 'R2GC_355_130',
                        value = '(ee**2*complex(0,1)*G**2*Rtau2x1**2)/(144.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*Rtau2x2**2)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_356_131 = Coupling(name = 'R2GC_356_131',
                        value = '-(ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_357_132 = Coupling(name = 'R2GC_357_132',
                        value = '(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_358_133 = Coupling(name = 'R2GC_358_133',
                        value = '(ee**2*complex(0,1)*G**2*Rtau1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*Rtau1x2**2)/(144.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*Rtau1x1**2)/(96.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_359_134 = Coupling(name = 'R2GC_359_134',
                        value = '(ee**2*complex(0,1)*G**2*Rtau1x1*Rtau2x1)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*Rtau1x2*Rtau2x2)/(144.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*Rtau1x1*Rtau2x1)/(96.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_360_135 = Coupling(name = 'R2GC_360_135',
                        value = '(ee**2*complex(0,1)*G**2*Rtau2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*Rtau2x2**2)/(144.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*Rtau2x1**2)/(96.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_361_136 = Coupling(name = 'R2GC_361_136',
                        value = '(cw*ee*complex(0,1)*G**2)/(18.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_361_137 = Coupling(name = 'R2GC_361_137',
                        value = '(cw*ee*complex(0,1)*G**2)/(144.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*sw)/(216.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_362_138 = Coupling(name = 'R2GC_362_138',
                        value = '-(cw*ee*complex(0,1)*G**2*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_362_139 = Coupling(name = 'R2GC_362_139',
                        value = '-(cw*ee*complex(0,1)*G**2*sw)/(216.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_363_140 = Coupling(name = 'R2GC_363_140',
                        value = '(2*cw*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2*sw*(-1 + sw**2)) - (4*cw*ee**2*complex(0,1)*G**2*sw)/(81.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_363_141 = Coupling(name = 'R2GC_363_141',
                        value = '(cw*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*sw)/(324.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_364_142 = Coupling(name = 'R2GC_364_142',
                        value = '(-4*cw*ee**2*complex(0,1)*G**2*sw)/(81.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_364_143 = Coupling(name = 'R2GC_364_143',
                        value = '-(cw*ee**2*complex(0,1)*G**2*sw)/(324.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_365_144 = Coupling(name = 'R2GC_365_144',
                        value = '(37*cw*ee*complex(0,1)*G**3*sw)/(432.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

R2GC_365_145 = Coupling(name = 'R2GC_365_145',
                        value = '(53*cw*ee*complex(0,1)*G**3*sw)/(864.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

R2GC_366_146 = Coupling(name = 'R2GC_366_146',
                        value = '(-37*cw*ee*complex(0,1)*G**3)/(288.*cmath.pi**2*sw*(-1 + sw**2)) + (37*cw*ee*complex(0,1)*G**3*sw)/(432.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

R2GC_366_147 = Coupling(name = 'R2GC_366_147',
                        value = '(-53*cw*ee*complex(0,1)*G**3)/(576.*cmath.pi**2*sw*(-1 + sw**2)) + (53*cw*ee*complex(0,1)*G**3*sw)/(864.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

R2GC_367_148 = Coupling(name = 'R2GC_367_148',
                        value = '(4*cw**2*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (4*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(81.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_367_149 = Coupling(name = 'R2GC_367_149',
                        value = '(7*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(162.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_370_150 = Coupling(name = 'R2GC_370_150',
                        value = '(2*ee*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_370_151 = Coupling(name = 'R2GC_370_151',
                        value = '(ee*complex(0,1)*G**2)/(108.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_372_152 = Coupling(name = 'R2GC_372_152',
                        value = '(-16*ee**2*complex(0,1)*G**2)/(81.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_372_153 = Coupling(name = 'R2GC_372_153',
                        value = '-(ee**2*complex(0,1)*G**2)/(81.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_376_154 = Coupling(name = 'R2GC_376_154',
                        value = '(-37*ee*complex(0,1)*G**3)/(216.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_376_155 = Coupling(name = 'R2GC_376_155',
                        value = '(-53*ee*complex(0,1)*G**3)/(432.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_380_156 = Coupling(name = 'R2GC_380_156',
                        value = '(complex(0,1)*G**2*MscL**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_381_157 = Coupling(name = 'R2GC_381_157',
                        value = '(complex(0,1)*G**2*MscR**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_382_158 = Coupling(name = 'R2GC_382_158',
                        value = '(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_383_159 = Coupling(name = 'R2GC_383_159',
                        value = '(-16*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(81.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_383_160 = Coupling(name = 'R2GC_383_160',
                        value = '-(ee**2*complex(0,1)*G**2*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (2*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(81.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_388_161 = Coupling(name = 'R2GC_388_161',
                        value = '-(ee**2*complex(0,1)*G**2*Rtau1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*Rtau1x2**2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_389_162 = Coupling(name = 'R2GC_389_162',
                        value = '-(ee**2*complex(0,1)*G**2*Rtau1x1*Rtau2x1)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*Rtau1x2*Rtau2x2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_390_163 = Coupling(name = 'R2GC_390_163',
                        value = '-(ee**2*complex(0,1)*G**2*Rtau2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*Rtau2x2**2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_393_164 = Coupling(name = 'R2GC_393_164',
                        value = '-(ee**2*complex(0,1)*G**2*Rtau1x1**2)/(144.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*Rtau1x2**2)/(144.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*Rtau1x1**2)/(96.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_394_165 = Coupling(name = 'R2GC_394_165',
                        value = '-(ee**2*complex(0,1)*G**2*Rtau1x1*Rtau2x1)/(144.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*Rtau1x2*Rtau2x2)/(144.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*Rtau1x1*Rtau2x1)/(96.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_395_166 = Coupling(name = 'R2GC_395_166',
                        value = '-(ee**2*complex(0,1)*G**2*Rtau2x1**2)/(144.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*Rtau2x2**2)/(144.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*Rtau2x1**2)/(96.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_396_167 = Coupling(name = 'R2GC_396_167',
                        value = '-(cw*ee*complex(0,1)*G**2)/(18.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee*complex(0,1)*G**2*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_396_168 = Coupling(name = 'R2GC_396_168',
                        value = '-(cw*ee*complex(0,1)*G**2)/(144.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*sw)/(108.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_397_169 = Coupling(name = 'R2GC_397_169',
                        value = '(2*cw*ee*complex(0,1)*G**2*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_397_170 = Coupling(name = 'R2GC_397_170',
                        value = '(cw*ee*complex(0,1)*G**2*sw)/(108.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_398_171 = Coupling(name = 'R2GC_398_171',
                        value = '(4*cw*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2*sw*(-1 + sw**2)) - (16*cw*ee**2*complex(0,1)*G**2*sw)/(81.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_398_172 = Coupling(name = 'R2GC_398_172',
                        value = '(cw*ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*sw)/(81.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_399_173 = Coupling(name = 'R2GC_399_173',
                        value = '(-16*cw*ee**2*complex(0,1)*G**2*sw)/(81.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_399_174 = Coupling(name = 'R2GC_399_174',
                        value = '-(cw*ee**2*complex(0,1)*G**2*sw)/(81.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_400_175 = Coupling(name = 'R2GC_400_175',
                        value = '(-37*cw*ee*complex(0,1)*G**3*sw)/(216.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

R2GC_400_176 = Coupling(name = 'R2GC_400_176',
                        value = '(-53*cw*ee*complex(0,1)*G**3*sw)/(432.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

R2GC_401_177 = Coupling(name = 'R2GC_401_177',
                        value = '(37*cw*ee*complex(0,1)*G**3)/(288.*cmath.pi**2*sw*(-1 + sw**2)) - (37*cw*ee*complex(0,1)*G**3*sw)/(216.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

R2GC_401_178 = Coupling(name = 'R2GC_401_178',
                        value = '(53*cw*ee*complex(0,1)*G**3)/(576.*cmath.pi**2*sw*(-1 + sw**2)) - (53*cw*ee*complex(0,1)*G**3*sw)/(432.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

R2GC_402_179 = Coupling(name = 'R2GC_402_179',
                        value = '(8*cw**2*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (16*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(81.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_402_180 = Coupling(name = 'R2GC_402_180',
                        value = '(11*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (2*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(81.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_415_181 = Coupling(name = 'R2GC_415_181',
                        value = '(complex(0,1)*G**2*MsdL**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_416_182 = Coupling(name = 'R2GC_416_182',
                        value = '(complex(0,1)*G**2*MsdR**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_438_183 = Coupling(name = 'R2GC_438_183',
                        value = '(3*complex(0,1)*G**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_451_184 = Coupling(name = 'R2GC_451_184',
                        value = '(complex(0,1)*G**2*MssL**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_452_185 = Coupling(name = 'R2GC_452_185',
                        value = '(complex(0,1)*G**2*MssR**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_476_186 = Coupling(name = 'R2GC_476_186',
                        value = '(-3*complex(0,1)*G**3)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_477_187 = Coupling(name = 'R2GC_477_187',
                        value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_481_188 = Coupling(name = 'R2GC_481_188',
                        value = '(cw*ee*complex(0,1)*G**2*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_485_189 = Coupling(name = 'R2GC_485_189',
                        value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_489_190 = Coupling(name = 'R2GC_489_190',
                        value = '-(cw*ee*complex(0,1)*G**2*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

R2GC_519_191 = Coupling(name = 'R2GC_519_191',
                        value = '(complex(0,1)*G**2*MstL**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_519_192 = Coupling(name = 'R2GC_519_192',
                        value = '(complex(0,1)*G**2*Mgo**2)/(6.*cmath.pi**2) + (complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_520_193 = Coupling(name = 'R2GC_520_193',
                        value = '(complex(0,1)*G**2*MstR**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_577_194 = Coupling(name = 'R2GC_577_194',
                        value = '(complex(0,1)*G**2*MsuL**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_578_195 = Coupling(name = 'R2GC_578_195',
                        value = '(complex(0,1)*G**2*MsuR**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_619_196 = Coupling(name = 'R2GC_619_196',
                        value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_634_197 = Coupling(name = 'R2GC_634_197',
                        value = '(3*complex(0,1)*G**2*Mgo)/(8.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_636_198 = Coupling(name = 'R2GC_636_198',
                        value = '(-3*G**3)/(8.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_637_199 = Coupling(name = 'R2GC_637_199',
                        value = '(-11*complex(0,1)*G**4)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_637_200 = Coupling(name = 'R2GC_637_200',
                        value = '(25*complex(0,1)*G**4)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (25*complex(0,1)*G**4*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (25*complex(0,1)*G**4*sw**4)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_637_201 = Coupling(name = 'R2GC_637_201',
                        value = '(5*complex(0,1)*G**4)/(576.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*sw**4)/(576.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_637_202 = Coupling(name = 'R2GC_637_202',
                        value = '-(complex(0,1)*G**4)/(3456.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*sw**2)/(1728.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*sw**4)/(3456.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_638_203 = Coupling(name = 'R2GC_638_203',
                        value = '(-5*complex(0,1)*G**4)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_638_204 = Coupling(name = 'R2GC_638_204',
                        value = '(-5*complex(0,1)*G**4)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*sw**4)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_638_205 = Coupling(name = 'R2GC_638_205',
                        value = '(complex(0,1)*G**4)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_638_206 = Coupling(name = 'R2GC_638_206',
                        value = '(-79*complex(0,1)*G**4)/(1152.*cmath.pi**2*(-1 + sw**2)**2) + (79*complex(0,1)*G**4*sw**2)/(576.*cmath.pi**2*(-1 + sw**2)**2) - (79*complex(0,1)*G**4*sw**4)/(1152.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_641_207 = Coupling(name = 'R2GC_641_207',
                        value = '(ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_642_208 = Coupling(name = 'R2GC_642_208',
                        value = '(ee**2*complex(0,1)*G**2*Rtau1x1)/(48.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_643_209 = Coupling(name = 'R2GC_643_209',
                        value = '(ee**2*complex(0,1)*G**2*Rtau2x1)/(48.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_658_210 = Coupling(name = 'R2GC_658_210',
                        value = '(complex(0,1)*G**4)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_658_211 = Coupling(name = 'R2GC_658_211',
                        value = '(5*complex(0,1)*G**4)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*sw**4)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_658_212 = Coupling(name = 'R2GC_658_212',
                        value = '(-109*complex(0,1)*G**4)/(3456.*cmath.pi**2*(-1 + sw**2)**2) + (109*complex(0,1)*G**4*sw**2)/(1728.*cmath.pi**2*(-1 + sw**2)**2) - (109*complex(0,1)*G**4*sw**4)/(3456.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_661_213 = Coupling(name = 'R2GC_661_213',
                        value = '(35*complex(0,1)*G**4)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (35*complex(0,1)*G**4*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (35*complex(0,1)*G**4*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_661_214 = Coupling(name = 'R2GC_661_214',
                        value = '(29*complex(0,1)*G**4)/(1152.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*sw**2)/(576.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*sw**4)/(1152.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

R2GC_676_215 = Coupling(name = 'R2GC_676_215',
                        value = '-(ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_676_216 = Coupling(name = 'R2GC_676_216',
                        value = '-(ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_676_217 = Coupling(name = 'R2GC_676_217',
                        value = '(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_678_218 = Coupling(name = 'R2GC_678_218',
                        value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_678_219 = Coupling(name = 'R2GC_678_219',
                        value = '-(ee*complex(0,1)*G**2)/(72.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_679_220 = Coupling(name = 'R2GC_679_220',
                        value = '(ee*complex(0,1)*G**2)/(9.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_679_221 = Coupling(name = 'R2GC_679_221',
                        value = '(ee*complex(0,1)*G**2)/(72.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_680_222 = Coupling(name = 'R2GC_680_222',
                        value = '-(ee**2*complex(0,1)*G**2*cmath.sqrt(2))/(27.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':2})

R2GC_680_223 = Coupling(name = 'R2GC_680_223',
                        value = '-(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_681_224 = Coupling(name = 'R2GC_681_224',
                        value = '(-3*ee*complex(0,1)*G**3)/(32.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':3,'QED':1})

R2GC_681_225 = Coupling(name = 'R2GC_681_225',
                        value = '(-37*ee*complex(0,1)*G**3)/(144.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':3,'QED':1})

R2GC_681_226 = Coupling(name = 'R2GC_681_226',
                        value = '(ee*complex(0,1)*G**3)/(288.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':3,'QED':1})

R2GC_688_227 = Coupling(name = 'R2GC_688_227',
                        value = '-(cw*ee**2*complex(0,1)*G**2*cmath.sqrt(2))/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

R2GC_688_228 = Coupling(name = 'R2GC_688_228',
                        value = '-(cw*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_721_229 = Coupling(name = 'R2GC_721_229',
                        value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_802_230 = Coupling(name = 'R2GC_802_230',
                        value = '-(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**4)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_802_231 = Coupling(name = 'R2GC_802_231',
                        value = '(ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_802_232 = Coupling(name = 'R2GC_802_232',
                        value = '(-17*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_802_233 = Coupling(name = 'R2GC_802_233',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_802_234 = Coupling(name = 'R2GC_802_234',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_802_235 = Coupling(name = 'R2GC_802_235',
                        value = '-(ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_802_236 = Coupling(name = 'R2GC_802_236',
                        value = '(ee**2*complex(0,1)*G**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(324.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(648.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_802_237 = Coupling(name = 'R2GC_802_237',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(72.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_802_238 = Coupling(name = 'R2GC_802_238',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(72.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_802_239 = Coupling(name = 'R2GC_802_239',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(72.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_802_240 = Coupling(name = 'R2GC_802_240',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(72.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_802_241 = Coupling(name = 'R2GC_802_241',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(648.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_803_242 = Coupling(name = 'R2GC_803_242',
                        value = '(ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_803_243 = Coupling(name = 'R2GC_803_243',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_803_244 = Coupling(name = 'R2GC_803_244',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_803_245 = Coupling(name = 'R2GC_803_245',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(162.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_803_246 = Coupling(name = 'R2GC_803_246',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(162.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_803_247 = Coupling(name = 'R2GC_803_247',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(162.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_803_248 = Coupling(name = 'R2GC_803_248',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(162.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_803_249 = Coupling(name = 'R2GC_803_249',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(648.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_804_250 = Coupling(name = 'R2GC_804_250',
                        value = '(-2*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (4*ee**2*complex(0,1)*G**2*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_804_251 = Coupling(name = 'R2GC_804_251',
                        value = '(ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_804_252 = Coupling(name = 'R2GC_804_252',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (2*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_804_253 = Coupling(name = 'R2GC_804_253',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_804_254 = Coupling(name = 'R2GC_804_254',
                        value = '(ee**2*complex(0,1)*G**2)/(162.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(81.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(162.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_804_255 = Coupling(name = 'R2GC_804_255',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(72.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_804_256 = Coupling(name = 'R2GC_804_256',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(72.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_804_257 = Coupling(name = 'R2GC_804_257',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(72.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_804_258 = Coupling(name = 'R2GC_804_258',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(72.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_804_259 = Coupling(name = 'R2GC_804_259',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(162.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_805_260 = Coupling(name = 'R2GC_805_260',
                        value = '(ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_805_261 = Coupling(name = 'R2GC_805_261',
                        value = '(-2*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_805_262 = Coupling(name = 'R2GC_805_262',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_805_263 = Coupling(name = 'R2GC_805_263',
                        value = '(10*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(81.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_805_264 = Coupling(name = 'R2GC_805_264',
                        value = '(10*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(81.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_805_265 = Coupling(name = 'R2GC_805_265',
                        value = '(10*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(81.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_805_266 = Coupling(name = 'R2GC_805_266',
                        value = '(10*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(81.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_805_267 = Coupling(name = 'R2GC_805_267',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(162.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_814_268 = Coupling(name = 'R2GC_814_268',
                        value = '(5*ee*complex(0,1)*G**2*VV1x1*VV1x2*yu3x3)/(144.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':2})

R2GC_814_269 = Coupling(name = 'R2GC_814_269',
                        value = '(5*ee*complex(0,1)*G**2*VV2x1*VV2x2*yu3x3)/(144.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':2})

R2GC_814_270 = Coupling(name = 'R2GC_814_270',
                        value = '(-5*ee*complex(0,1)*G**2*VV1x1*VV1x2*yu3x3)/(144.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':2})

R2GC_814_271 = Coupling(name = 'R2GC_814_271',
                        value = '(-5*ee*complex(0,1)*G**2*VV2x1*VV2x2*yu3x3)/(144.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':2})

R2GC_815_272 = Coupling(name = 'R2GC_815_272',
                        value = '(-5*ee*complex(0,1)*G**2*VV1x1*VV1x2*yu3x3)/(48.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':2})

R2GC_815_273 = Coupling(name = 'R2GC_815_273',
                        value = '(-5*ee*complex(0,1)*G**2*VV2x1*VV2x2*yu3x3)/(48.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':2})

R2GC_815_274 = Coupling(name = 'R2GC_815_274',
                        value = '(5*ee*complex(0,1)*G**2*VV1x1*VV1x2*yu3x3)/(48.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':2})

R2GC_815_275 = Coupling(name = 'R2GC_815_275',
                        value = '(5*ee*complex(0,1)*G**2*VV2x1*VV2x2*yu3x3)/(48.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':2})

R2GC_820_276 = Coupling(name = 'R2GC_820_276',
                        value = '(ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_820_277 = Coupling(name = 'R2GC_820_277',
                        value = '(ee**2*complex(0,1)*G**2)/(384.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_820_278 = Coupling(name = 'R2GC_820_278',
                        value = '-(ee**2*complex(0,1)*G**2)/(384.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_820_279 = Coupling(name = 'R2GC_820_279',
                        value = '-(ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_820_280 = Coupling(name = 'R2GC_820_280',
                        value = '(-5*ee**2*complex(0,1)*G**2*VV1x1**2)/(144.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_820_281 = Coupling(name = 'R2GC_820_281',
                        value = '(-5*ee**2*complex(0,1)*G**2*VV2x1**2)/(144.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_820_282 = Coupling(name = 'R2GC_820_282',
                        value = '(-5*ee**2*complex(0,1)*G**2*UU1x1**2)/(144.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_820_283 = Coupling(name = 'R2GC_820_283',
                        value = '(-5*ee**2*complex(0,1)*G**2*UU2x1**2)/(144.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_820_284 = Coupling(name = 'R2GC_820_284',
                        value = '(5*ee**2*complex(0,1)*G**2)/(1152.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_821_285 = Coupling(name = 'R2GC_821_285',
                        value = '-(ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_821_286 = Coupling(name = 'R2GC_821_286',
                        value = '-(ee**2*complex(0,1)*G**2)/(128.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_821_287 = Coupling(name = 'R2GC_821_287',
                        value = '(ee**2*complex(0,1)*G**2)/(128.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_821_288 = Coupling(name = 'R2GC_821_288',
                        value = '(ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_821_289 = Coupling(name = 'R2GC_821_289',
                        value = '(5*ee**2*complex(0,1)*G**2*VV1x1**2)/(48.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_821_290 = Coupling(name = 'R2GC_821_290',
                        value = '(5*ee**2*complex(0,1)*G**2*VV2x1**2)/(48.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_821_291 = Coupling(name = 'R2GC_821_291',
                        value = '(5*ee**2*complex(0,1)*G**2*UU1x1**2)/(48.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_821_292 = Coupling(name = 'R2GC_821_292',
                        value = '(5*ee**2*complex(0,1)*G**2*UU2x1**2)/(48.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_821_293 = Coupling(name = 'R2GC_821_293',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(384.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_294 = Coupling(name = 'R2GC_826_294',
                        value = '(ee**2*complex(0,1)*G**2)/(1728.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(1728.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_295 = Coupling(name = 'R2GC_826_295',
                        value = '(ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_296 = Coupling(name = 'R2GC_826_296',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_297 = Coupling(name = 'R2GC_826_297',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(768.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_298 = Coupling(name = 'R2GC_826_298',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_299 = Coupling(name = 'R2GC_826_299',
                        value = '-(ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(384.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_300 = Coupling(name = 'R2GC_826_300',
                        value = '-(ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**4)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_301 = Coupling(name = 'R2GC_826_301',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_302 = Coupling(name = 'R2GC_826_302',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_303 = Coupling(name = 'R2GC_826_303',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_304 = Coupling(name = 'R2GC_826_304',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_305 = Coupling(name = 'R2GC_826_305',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_306 = Coupling(name = 'R2GC_826_306',
                        value = '(5*ee**2*complex(0,1)*G**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*sw**4)/(1296.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_826_307 = Coupling(name = 'R2GC_826_307',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2)/(864.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_308 = Coupling(name = 'R2GC_827_308',
                        value = '-(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_309 = Coupling(name = 'R2GC_827_309',
                        value = '-(ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**4)/(576.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_310 = Coupling(name = 'R2GC_827_310',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_311 = Coupling(name = 'R2GC_827_311',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(256.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(576.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_312 = Coupling(name = 'R2GC_827_312',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(576.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_313 = Coupling(name = 'R2GC_827_313',
                        value = '(ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(128.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_314 = Coupling(name = 'R2GC_827_314',
                        value = '(ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_315 = Coupling(name = 'R2GC_827_315',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_316 = Coupling(name = 'R2GC_827_316',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_317 = Coupling(name = 'R2GC_827_317',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_318 = Coupling(name = 'R2GC_827_318',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_319 = Coupling(name = 'R2GC_827_319',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_320 = Coupling(name = 'R2GC_827_320',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**4)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_827_321 = Coupling(name = 'R2GC_827_321',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_322 = Coupling(name = 'R2GC_832_322',
                        value = '-(ee**2*complex(0,1)*G**2)/(1728.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**4)/(1728.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_323 = Coupling(name = 'R2GC_832_323',
                        value = '(19*ee**2*complex(0,1)*G**2)/(864.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_324 = Coupling(name = 'R2GC_832_324',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_325 = Coupling(name = 'R2GC_832_325',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(768.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_326 = Coupling(name = 'R2GC_832_326',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(768.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_327 = Coupling(name = 'R2GC_832_327',
                        value = '(ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(384.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_328 = Coupling(name = 'R2GC_832_328',
                        value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_329 = Coupling(name = 'R2GC_832_329',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_330 = Coupling(name = 'R2GC_832_330',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(324.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**4)/(648.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_331 = Coupling(name = 'R2GC_832_331',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_332 = Coupling(name = 'R2GC_832_332',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_333 = Coupling(name = 'R2GC_832_333',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_334 = Coupling(name = 'R2GC_832_334',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_832_335 = Coupling(name = 'R2GC_832_335',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(648.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_336 = Coupling(name = 'R2GC_833_336',
                        value = '(ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_337 = Coupling(name = 'R2GC_833_337',
                        value = '(ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(576.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_338 = Coupling(name = 'R2GC_833_338',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(16.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_339 = Coupling(name = 'R2GC_833_339',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(256.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(576.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_340 = Coupling(name = 'R2GC_833_340',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(256.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_341 = Coupling(name = 'R2GC_833_341',
                        value = '-(ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(128.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_342 = Coupling(name = 'R2GC_833_342',
                        value = '-(ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**4)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_343 = Coupling(name = 'R2GC_833_343',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_344 = Coupling(name = 'R2GC_833_344',
                        value = '(5*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*sw**4)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_345 = Coupling(name = 'R2GC_833_345',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN1x2**2*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_346 = Coupling(name = 'R2GC_833_346',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN2x2**2*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_347 = Coupling(name = 'R2GC_833_347',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN3x2**2*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_348 = Coupling(name = 'R2GC_833_348',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN4x2**2*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_833_349 = Coupling(name = 'R2GC_833_349',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_834_350 = Coupling(name = 'R2GC_834_350',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_834_351 = Coupling(name = 'R2GC_834_351',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(768.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_834_352 = Coupling(name = 'R2GC_834_352',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_834_353 = Coupling(name = 'R2GC_834_353',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_834_354 = Coupling(name = 'R2GC_834_354',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_834_355 = Coupling(name = 'R2GC_834_355',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_834_356 = Coupling(name = 'R2GC_834_356',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(648.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_834_357 = Coupling(name = 'R2GC_834_357',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2)/(864.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(648.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_835_358 = Coupling(name = 'R2GC_835_358',
                        value = '-(ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_835_359 = Coupling(name = 'R2GC_835_359',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_835_360 = Coupling(name = 'R2GC_835_360',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(256.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_835_361 = Coupling(name = 'R2GC_835_361',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_835_362 = Coupling(name = 'R2GC_835_362',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_835_363 = Coupling(name = 'R2GC_835_363',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_835_364 = Coupling(name = 'R2GC_835_364',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_835_365 = Coupling(name = 'R2GC_835_365',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_835_366 = Coupling(name = 'R2GC_835_366',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_836_367 = Coupling(name = 'R2GC_836_367',
                        value = '(ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_836_368 = Coupling(name = 'R2GC_836_368',
                        value = '-(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_836_369 = Coupling(name = 'R2GC_836_369',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_836_370 = Coupling(name = 'R2GC_836_370',
                        value = '-(ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**4)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_836_371 = Coupling(name = 'R2GC_836_371',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(324.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_836_372 = Coupling(name = 'R2GC_836_372',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(324.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_836_373 = Coupling(name = 'R2GC_836_373',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(324.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_836_374 = Coupling(name = 'R2GC_836_374',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(324.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_836_375 = Coupling(name = 'R2GC_836_375',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_836_376 = Coupling(name = 'R2GC_836_376',
                        value = '(5*ee**2*complex(0,1)*G**2)/(324.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(162.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*sw**4)/(324.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_836_377 = Coupling(name = 'R2GC_836_377',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(324.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_837_378 = Coupling(name = 'R2GC_837_378',
                        value = '-(ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*complex(0,1)*G**2*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**4)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_837_379 = Coupling(name = 'R2GC_837_379',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_837_380 = Coupling(name = 'R2GC_837_380',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_837_381 = Coupling(name = 'R2GC_837_381',
                        value = '(ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_837_382 = Coupling(name = 'R2GC_837_382',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_837_383 = Coupling(name = 'R2GC_837_383',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_837_384 = Coupling(name = 'R2GC_837_384',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_837_385 = Coupling(name = 'R2GC_837_385',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_837_386 = Coupling(name = 'R2GC_837_386',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_837_387 = Coupling(name = 'R2GC_837_387',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**4)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_837_388 = Coupling(name = 'R2GC_837_388',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_842_389 = Coupling(name = 'R2GC_842_389',
                        value = '(11*ee**2*complex(0,1)*G**2)/(1728.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (13*ee**2*complex(0,1)*G**2*sw**2)/(3456.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_842_390 = Coupling(name = 'R2GC_842_390',
                        value = '(ee**2*complex(0,1)*G**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_842_391 = Coupling(name = 'R2GC_842_391',
                        value = '-(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_842_392 = Coupling(name = 'R2GC_842_392',
                        value = '(-5*ee**2*complex(0,1)*G**2*UU1x1**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*UU1x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*sw**2*UU1x1**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_842_393 = Coupling(name = 'R2GC_842_393',
                        value = '(-5*ee**2*complex(0,1)*G**2*UU2x1**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*UU2x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*sw**2*UU2x1**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_842_394 = Coupling(name = 'R2GC_842_394',
                        value = '(-5*ee**2*complex(0,1)*G**2*VV1x1**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*VV1x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*sw**2*VV1x1**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_842_395 = Coupling(name = 'R2GC_842_395',
                        value = '(-5*ee**2*complex(0,1)*G**2*VV2x1**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*VV2x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*sw**2*VV2x1**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_842_396 = Coupling(name = 'R2GC_842_396',
                        value = '(ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_842_397 = Coupling(name = 'R2GC_842_397',
                        value = '(-7*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (7*ee**2*complex(0,1)*G**2)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (7*ee**2*complex(0,1)*G**2*sw**2)/(384.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_842_398 = Coupling(name = 'R2GC_842_398',
                        value = '(5*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**2)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_843_399 = Coupling(name = 'R2GC_843_399',
                        value = '-(ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_843_400 = Coupling(name = 'R2GC_843_400',
                        value = '(5*ee**2*complex(0,1)*G**2*UU1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*UU1x1**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**2*UU1x1**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_843_401 = Coupling(name = 'R2GC_843_401',
                        value = '(5*ee**2*complex(0,1)*G**2*UU2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*UU2x1**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**2*UU2x1**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_843_402 = Coupling(name = 'R2GC_843_402',
                        value = '(5*ee**2*complex(0,1)*G**2*VV1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*VV1x1**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**2*VV1x1**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_843_403 = Coupling(name = 'R2GC_843_403',
                        value = '(5*ee**2*complex(0,1)*G**2*VV2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*VV2x1**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*sw**2*VV2x1**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_843_404 = Coupling(name = 'R2GC_843_404',
                        value = '(-3*ee**2*complex(0,1)*G**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (3*ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (3*ee**2*complex(0,1)*G**2*sw**2)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_843_405 = Coupling(name = 'R2GC_843_405',
                        value = '(7*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*(-1 + sw**2)**2) - (7*ee**2*complex(0,1)*G**2)/(1152.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (7*ee**2*complex(0,1)*G**2*sw**2)/(1152.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_843_406 = Coupling(name = 'R2GC_843_406',
                        value = '(-5*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(576.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_846_407 = Coupling(name = 'R2GC_846_407',
                        value = '(-17*ee**2*complex(0,1)*G**2)/(864.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_846_408 = Coupling(name = 'R2GC_846_408',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_846_409 = Coupling(name = 'R2GC_846_409',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_846_410 = Coupling(name = 'R2GC_846_410',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN1x2**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_846_411 = Coupling(name = 'R2GC_846_411',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN2x2**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_846_412 = Coupling(name = 'R2GC_846_412',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN3x2**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_846_413 = Coupling(name = 'R2GC_846_413',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN4x2**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_846_414 = Coupling(name = 'R2GC_846_414',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(324.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_847_415 = Coupling(name = 'R2GC_847_415',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(16.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_847_416 = Coupling(name = 'R2GC_847_416',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_847_417 = Coupling(name = 'R2GC_847_417',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_847_418 = Coupling(name = 'R2GC_847_418',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_847_419 = Coupling(name = 'R2GC_847_419',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_847_420 = Coupling(name = 'R2GC_847_420',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_847_421 = Coupling(name = 'R2GC_847_421',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_852_422 = Coupling(name = 'R2GC_852_422',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_852_423 = Coupling(name = 'R2GC_852_423',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_852_424 = Coupling(name = 'R2GC_852_424',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(324.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_852_425 = Coupling(name = 'R2GC_852_425',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(324.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_852_426 = Coupling(name = 'R2GC_852_426',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(324.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_852_427 = Coupling(name = 'R2GC_852_427',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(324.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(108.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_852_428 = Coupling(name = 'R2GC_852_428',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(648.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_853_429 = Coupling(name = 'R2GC_853_429',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_853_430 = Coupling(name = 'R2GC_853_430',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_853_431 = Coupling(name = 'R2GC_853_431',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_853_432 = Coupling(name = 'R2GC_853_432',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_853_433 = Coupling(name = 'R2GC_853_433',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_853_434 = Coupling(name = 'R2GC_853_434',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_853_435 = Coupling(name = 'R2GC_853_435',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_854_436 = Coupling(name = 'R2GC_854_436',
                        value = '-(ee**2*complex(0,1)*G**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_854_437 = Coupling(name = 'R2GC_854_437',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_854_438 = Coupling(name = 'R2GC_854_438',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_854_439 = Coupling(name = 'R2GC_854_439',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_854_440 = Coupling(name = 'R2GC_854_440',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(648.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_855_441 = Coupling(name = 'R2GC_855_441',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_855_442 = Coupling(name = 'R2GC_855_442',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(576.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_855_443 = Coupling(name = 'R2GC_855_443',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_855_444 = Coupling(name = 'R2GC_855_444',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_855_445 = Coupling(name = 'R2GC_855_445',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_855_446 = Coupling(name = 'R2GC_855_446',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_855_447 = Coupling(name = 'R2GC_855_447',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_855_448 = Coupling(name = 'R2GC_855_448',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_855_449 = Coupling(name = 'R2GC_855_449',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_864_450 = Coupling(name = 'R2GC_864_450',
                        value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_864_451 = Coupling(name = 'R2GC_864_451',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_864_452 = Coupling(name = 'R2GC_864_452',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_864_453 = Coupling(name = 'R2GC_864_453',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(81.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_864_454 = Coupling(name = 'R2GC_864_454',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(81.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_864_455 = Coupling(name = 'R2GC_864_455',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(81.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_864_456 = Coupling(name = 'R2GC_864_456',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(81.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_864_457 = Coupling(name = 'R2GC_864_457',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(324.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_865_458 = Coupling(name = 'R2GC_865_458',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_865_459 = Coupling(name = 'R2GC_865_459',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_865_460 = Coupling(name = 'R2GC_865_460',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_865_461 = Coupling(name = 'R2GC_865_461',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_865_462 = Coupling(name = 'R2GC_865_462',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_865_463 = Coupling(name = 'R2GC_865_463',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_865_464 = Coupling(name = 'R2GC_865_464',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_866_465 = Coupling(name = 'R2GC_866_465',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_866_466 = Coupling(name = 'R2GC_866_466',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_866_467 = Coupling(name = 'R2GC_866_467',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN1x2**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_866_468 = Coupling(name = 'R2GC_866_468',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN2x2**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_866_469 = Coupling(name = 'R2GC_866_469',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN3x2**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_866_470 = Coupling(name = 'R2GC_866_470',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(144.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(216.*cmath.pi**2*sw*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN4x2**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_866_471 = Coupling(name = 'R2GC_866_471',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (5*cw**2*ee**2*complex(0,1)*G**2)/(576.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_867_472 = Coupling(name = 'R2GC_867_472',
                        value = '(cw**2*ee**2*complex(0,1)*G**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2)/(16.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_867_473 = Coupling(name = 'R2GC_867_473',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2)/(64.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_867_474 = Coupling(name = 'R2GC_867_474',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN1x1*NN1x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN1x2**2*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_867_475 = Coupling(name = 'R2GC_867_475',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN2x1*NN2x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN2x2**2*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_867_476 = Coupling(name = 'R2GC_867_476',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN3x1*NN3x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN3x2**2*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_867_477 = Coupling(name = 'R2GC_867_477',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2)/(72.*cmath.pi**2*sw*(-1 + sw**2)**2) + (5*cw*ee**2*complex(0,1)*G**2*NN4x1*NN4x2*sw)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*NN4x2**2*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_867_478 = Coupling(name = 'R2GC_867_478',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (5*cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_876_479 = Coupling(name = 'R2GC_876_479',
                        value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_876_480 = Coupling(name = 'R2GC_876_480',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(324.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_876_481 = Coupling(name = 'R2GC_876_481',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(324.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_876_482 = Coupling(name = 'R2GC_876_482',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(324.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_876_483 = Coupling(name = 'R2GC_876_483',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(324.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_876_484 = Coupling(name = 'R2GC_876_484',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(1296.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_877_485 = Coupling(name = 'R2GC_877_485',
                        value = '-(cw**2*ee**2*complex(0,1)*G**2*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_877_486 = Coupling(name = 'R2GC_877_486',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN1x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_877_487 = Coupling(name = 'R2GC_877_487',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN2x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_877_488 = Coupling(name = 'R2GC_877_488',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN3x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_877_489 = Coupling(name = 'R2GC_877_489',
                        value = '(5*cw**2*ee**2*complex(0,1)*G**2*NN4x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_877_490 = Coupling(name = 'R2GC_877_490',
                        value = '(-5*cw**2*ee**2*complex(0,1)*G**2*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_904_491 = Coupling(name = 'R2GC_904_491',
                        value = '-(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*yu3x3**2)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*sw**2*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*sw**4*yu3x3**2)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_904_492 = Coupling(name = 'R2GC_904_492',
                        value = '-(complex(0,1)*G**2*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*sw**2*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*sw**4*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_905_493 = Coupling(name = 'R2GC_905_493',
                        value = '-(complex(0,1)*G**2*yu3x3**2)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*sw**2*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*sw**4*yu3x3**2)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_905_494 = Coupling(name = 'R2GC_905_494',
                        value = '(3*complex(0,1)*G**2*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (3*complex(0,1)*G**2*sw**2*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (3*complex(0,1)*G**2*sw**4*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_908_495 = Coupling(name = 'R2GC_908_495',
                        value = '(5*complex(0,1)*G**2*VV1x2**2*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**2*sw**2*VV1x2**2*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**2*sw**4*VV1x2**2*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_908_496 = Coupling(name = 'R2GC_908_496',
                        value = '(5*complex(0,1)*G**2*VV2x2**2*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**2*sw**2*VV2x2**2*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**2*sw**4*VV2x2**2*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_908_497 = Coupling(name = 'R2GC_908_497',
                        value = '(-5*complex(0,1)*G**2*VV1x2**2*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**2*sw**2*VV1x2**2*yu3x3**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**2*sw**4*VV1x2**2*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_908_498 = Coupling(name = 'R2GC_908_498',
                        value = '(-5*complex(0,1)*G**2*VV2x2**2*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**2*sw**2*VV2x2**2*yu3x3**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**2*sw**4*VV2x2**2*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_909_499 = Coupling(name = 'R2GC_909_499',
                        value = '(-5*complex(0,1)*G**2*VV1x2**2*yu3x3**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**2*sw**2*VV1x2**2*yu3x3**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**2*sw**4*VV1x2**2*yu3x3**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_909_500 = Coupling(name = 'R2GC_909_500',
                        value = '(-5*complex(0,1)*G**2*VV2x2**2*yu3x3**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**2*sw**2*VV2x2**2*yu3x3**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**2*sw**4*VV2x2**2*yu3x3**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_909_501 = Coupling(name = 'R2GC_909_501',
                        value = '(5*complex(0,1)*G**2*VV1x2**2*yu3x3**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**2*sw**2*VV1x2**2*yu3x3**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**2*sw**4*VV1x2**2*yu3x3**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_909_502 = Coupling(name = 'R2GC_909_502',
                        value = '(5*complex(0,1)*G**2*VV2x2**2*yu3x3**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**2*sw**2*VV2x2**2*yu3x3**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**2*sw**4*VV2x2**2*yu3x3**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

R2GC_960_503 = Coupling(name = 'R2GC_960_503',
                        value = '(3*complex(0,1)*G**3)/(4.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':3})

R2GC_960_504 = Coupling(name = 'R2GC_960_504',
                        value = '-(complex(0,1)*G**3)/(96.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':3})

R2GC_960_505 = Coupling(name = 'R2GC_960_505',
                        value = '(3*complex(0,1)*G**3)/(32.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':3})

R2GC_966_506 = Coupling(name = 'R2GC_966_506',
                        value = '(-3*complex(0,1)*G**3)/(4.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':3})

R2GC_966_507 = Coupling(name = 'R2GC_966_507',
                        value = '(complex(0,1)*G**3)/(96.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':3})

R2GC_966_508 = Coupling(name = 'R2GC_966_508',
                        value = '(-3*complex(0,1)*G**3)/(32.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':3})

R2GC_972_509 = Coupling(name = 'R2GC_972_509',
                        value = '-(cw*ee*complex(0,1)*G**2*NN1x1)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_975_510 = Coupling(name = 'R2GC_975_510',
                        value = '(cw*ee*complex(0,1)*G**2*NN1x1)/(18.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_978_511 = Coupling(name = 'R2GC_978_511',
                        value = '-(cw*ee*complex(0,1)*G**2*NN2x1)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_981_512 = Coupling(name = 'R2GC_981_512',
                        value = '(cw*ee*complex(0,1)*G**2*NN2x1)/(18.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_984_513 = Coupling(name = 'R2GC_984_513',
                        value = '-(cw*ee*complex(0,1)*G**2*NN3x1)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_987_514 = Coupling(name = 'R2GC_987_514',
                        value = '(cw*ee*complex(0,1)*G**2*NN3x1)/(18.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_990_515 = Coupling(name = 'R2GC_990_515',
                        value = '-(cw*ee*complex(0,1)*G**2*NN4x1)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_993_516 = Coupling(name = 'R2GC_993_516',
                        value = '(cw*ee*complex(0,1)*G**2*NN4x1)/(18.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_996_517 = Coupling(name = 'R2GC_996_517',
                        value = '-(cw*ee*complex(0,1)*G**2*NN1x1)/(72.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*NN1x2)/(24.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*NN1x2*sw)/(24.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1000_1 = Coupling(name = 'UVGC_1000_1',
                       value = '(cw*ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_1000_2 = Coupling(name = 'UVGC_1000_2',
                       value = '(cw*ee*FRCTdeltaZxttLxtG*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxttLxtG*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxttLxtG*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_1000_3 = Coupling(name = 'UVGC_1000_3',
                       value = '(cw*ee*FRCTdeltaZxttLxgostL*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxttLxgostL*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxttLxgostL*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_1000_4 = Coupling(name = 'UVGC_1000_4',
                       value = '(cw*ee*FRCTdeltaZxttLxgostR*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxttLxgostR*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxttLxgostR*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_1000_5 = Coupling(name = 'UVGC_1000_5',
                       value = '(FRCTdeltaZxstRstLxgot*complex(0,1)*NN1x4*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstRstLxgot*complex(0,1)*NN1x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) + (cw*ee*FRCTdeltaZxstLstLxgot*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxstLstLxgot*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxstLstLxgot*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_1000_6 = Coupling(name = 'UVGC_1000_6',
                       value = '-(cw*ee*complex(0,1)*G**2*invFREps*NN1x1)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*invFREps*NN1x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*invFREps*NN1x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_1001_7 = Coupling(name = 'UVGC_1001_7',
                       value = '(cw*ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_1001_8 = Coupling(name = 'UVGC_1001_8',
                       value = '(cw*ee*FRCTdeltaZxuuLxuG*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxuuLxuG*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxuuLxuG*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_1001_9 = Coupling(name = 'UVGC_1001_9',
                       value = '(cw*ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_1001_10 = Coupling(name = 'UVGC_1001_10',
                        value = '(cw*ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1002_11 = Coupling(name = 'UVGC_1002_11',
                        value = '(cw*ee*FRCTdeltaZxbbLxbG*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxbbLxbG*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxbbLxbG*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1002_12 = Coupling(name = 'UVGC_1002_12',
                        value = '(cw*ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1002_13 = Coupling(name = 'UVGC_1002_13',
                        value = '(cw*ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1002_14 = Coupling(name = 'UVGC_1002_14',
                        value = '(cw*ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1002_15 = Coupling(name = 'UVGC_1002_15',
                        value = '-(cw*ee*complex(0,1)*G**2*invFREps*NN2x1)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*invFREps*NN2x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*invFREps*NN2x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1003_16 = Coupling(name = 'UVGC_1003_16',
                        value = '(cw*ee*FRCTdeltaZxddLxdG*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxddLxdG*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxddLxdG*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1003_17 = Coupling(name = 'UVGC_1003_17',
                        value = '(cw*ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1003_18 = Coupling(name = 'UVGC_1003_18',
                        value = '(cw*ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1003_19 = Coupling(name = 'UVGC_1003_19',
                        value = '(cw*ee*FRCTdeltaZxddLxgosdL*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxddLxgosdL*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxddLxgosdL*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1004_20 = Coupling(name = 'UVGC_1004_20',
                        value = '(cw*ee*FRCTdeltaZxssLxsG*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLxsG*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLxsG*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1004_21 = Coupling(name = 'UVGC_1004_21',
                        value = '(cw*ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1004_22 = Coupling(name = 'UVGC_1004_22',
                        value = '(cw*ee*FRCTdeltaZxssLssLxgos*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLssLxgos*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLssLxgos*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1004_23 = Coupling(name = 'UVGC_1004_23',
                        value = '(cw*ee*FRCTdeltaZxssLxgossL*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLxgossL*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLxgossL*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1005_24 = Coupling(name = 'UVGC_1005_24',
                        value = '(cw*ee*FRCTdeltaZxccLxcG*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxccLxcG*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxccLxcG*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1005_25 = Coupling(name = 'UVGC_1005_25',
                        value = '(cw*ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1005_26 = Coupling(name = 'UVGC_1005_26',
                        value = '(cw*ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1005_27 = Coupling(name = 'UVGC_1005_27',
                        value = '(cw*ee*FRCTdeltaZxccLxgoscL*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxccLxgoscL*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxccLxgoscL*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1005_28 = Coupling(name = 'UVGC_1005_28',
                        value = '-(cw*ee*complex(0,1)*G**2*invFREps*NN2x1)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*invFREps*NN2x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*invFREps*NN2x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1006_29 = Coupling(name = 'UVGC_1006_29',
                        value = '(cw*ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1006_30 = Coupling(name = 'UVGC_1006_30',
                        value = '(cw*ee*FRCTdeltaZxttLxtG*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxttLxtG*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxttLxtG*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1006_31 = Coupling(name = 'UVGC_1006_31',
                        value = '(cw*ee*FRCTdeltaZxttLxgostL*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxttLxgostL*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxttLxgostL*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1006_32 = Coupling(name = 'UVGC_1006_32',
                        value = '(cw*ee*FRCTdeltaZxttLxgostR*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxttLxgostR*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxttLxgostR*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1006_33 = Coupling(name = 'UVGC_1006_33',
                        value = '(FRCTdeltaZxstRstLxgot*complex(0,1)*NN2x4*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstRstLxgot*complex(0,1)*NN2x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) + (cw*ee*FRCTdeltaZxstLstLxgot*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxstLstLxgot*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxstLstLxgot*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1007_34 = Coupling(name = 'UVGC_1007_34',
                        value = '(cw*ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1007_35 = Coupling(name = 'UVGC_1007_35',
                        value = '(cw*ee*FRCTdeltaZxuuLxuG*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxuuLxuG*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxuuLxuG*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1007_36 = Coupling(name = 'UVGC_1007_36',
                        value = '(cw*ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1007_37 = Coupling(name = 'UVGC_1007_37',
                        value = '(cw*ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*NN2x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*NN2x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1008_38 = Coupling(name = 'UVGC_1008_38',
                        value = '(cw*ee*FRCTdeltaZxbbLxbG*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxbbLxbG*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxbbLxbG*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1008_39 = Coupling(name = 'UVGC_1008_39',
                        value = '(cw*ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1008_40 = Coupling(name = 'UVGC_1008_40',
                        value = '(cw*ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1008_41 = Coupling(name = 'UVGC_1008_41',
                        value = '(cw*ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1008_42 = Coupling(name = 'UVGC_1008_42',
                        value = '-(cw*ee*complex(0,1)*G**2*invFREps*NN3x1)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*invFREps*NN3x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*invFREps*NN3x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1009_43 = Coupling(name = 'UVGC_1009_43',
                        value = '(cw*ee*FRCTdeltaZxddLxdG*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxddLxdG*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxddLxdG*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1009_44 = Coupling(name = 'UVGC_1009_44',
                        value = '(cw*ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1009_45 = Coupling(name = 'UVGC_1009_45',
                        value = '(cw*ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1009_46 = Coupling(name = 'UVGC_1009_46',
                        value = '(cw*ee*FRCTdeltaZxddLxgosdL*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxddLxgosdL*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxddLxgosdL*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1010_47 = Coupling(name = 'UVGC_1010_47',
                        value = '(cw*ee*FRCTdeltaZxssLxsG*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLxsG*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLxsG*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1010_48 = Coupling(name = 'UVGC_1010_48',
                        value = '(cw*ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1010_49 = Coupling(name = 'UVGC_1010_49',
                        value = '(cw*ee*FRCTdeltaZxssLssLxgos*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLssLxgos*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLssLxgos*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1010_50 = Coupling(name = 'UVGC_1010_50',
                        value = '(cw*ee*FRCTdeltaZxssLxgossL*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLxgossL*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLxgossL*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1011_51 = Coupling(name = 'UVGC_1011_51',
                        value = '(cw*ee*FRCTdeltaZxccLxcG*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxccLxcG*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxccLxcG*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1011_52 = Coupling(name = 'UVGC_1011_52',
                        value = '(cw*ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1011_53 = Coupling(name = 'UVGC_1011_53',
                        value = '(cw*ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1011_54 = Coupling(name = 'UVGC_1011_54',
                        value = '(cw*ee*FRCTdeltaZxccLxgoscL*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxccLxgoscL*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxccLxgoscL*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1011_55 = Coupling(name = 'UVGC_1011_55',
                        value = '-(cw*ee*complex(0,1)*G**2*invFREps*NN3x1)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*invFREps*NN3x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*invFREps*NN3x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1012_56 = Coupling(name = 'UVGC_1012_56',
                        value = '(cw*ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1012_57 = Coupling(name = 'UVGC_1012_57',
                        value = '(cw*ee*FRCTdeltaZxttLxtG*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxttLxtG*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxttLxtG*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1012_58 = Coupling(name = 'UVGC_1012_58',
                        value = '(cw*ee*FRCTdeltaZxttLxgostL*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxttLxgostL*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxttLxgostL*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1012_59 = Coupling(name = 'UVGC_1012_59',
                        value = '(cw*ee*FRCTdeltaZxttLxgostR*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxttLxgostR*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxttLxgostR*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1012_60 = Coupling(name = 'UVGC_1012_60',
                        value = '(FRCTdeltaZxstRstLxgot*complex(0,1)*NN3x4*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstRstLxgot*complex(0,1)*NN3x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) + (cw*ee*FRCTdeltaZxstLstLxgot*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxstLstLxgot*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxstLstLxgot*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1013_61 = Coupling(name = 'UVGC_1013_61',
                        value = '(cw*ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1013_62 = Coupling(name = 'UVGC_1013_62',
                        value = '(cw*ee*FRCTdeltaZxuuLxuG*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxuuLxuG*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxuuLxuG*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1013_63 = Coupling(name = 'UVGC_1013_63',
                        value = '(cw*ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1013_64 = Coupling(name = 'UVGC_1013_64',
                        value = '(cw*ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*NN3x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*NN3x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1014_65 = Coupling(name = 'UVGC_1014_65',
                        value = '(cw*ee*FRCTdeltaZxbbLxbG*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxbbLxbG*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxbbLxbG*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1014_66 = Coupling(name = 'UVGC_1014_66',
                        value = '(cw*ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1014_67 = Coupling(name = 'UVGC_1014_67',
                        value = '(cw*ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1014_68 = Coupling(name = 'UVGC_1014_68',
                        value = '(cw*ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1014_69 = Coupling(name = 'UVGC_1014_69',
                        value = '-(cw*ee*complex(0,1)*G**2*invFREps*NN4x1)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*invFREps*NN4x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*invFREps*NN4x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1015_70 = Coupling(name = 'UVGC_1015_70',
                        value = '(cw*ee*FRCTdeltaZxddLxdG*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxddLxdG*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxddLxdG*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1015_71 = Coupling(name = 'UVGC_1015_71',
                        value = '(cw*ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1015_72 = Coupling(name = 'UVGC_1015_72',
                        value = '(cw*ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1015_73 = Coupling(name = 'UVGC_1015_73',
                        value = '(cw*ee*FRCTdeltaZxddLxgosdL*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxddLxgosdL*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxddLxgosdL*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1016_74 = Coupling(name = 'UVGC_1016_74',
                        value = '(cw*ee*FRCTdeltaZxssLxsG*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLxsG*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLxsG*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1016_75 = Coupling(name = 'UVGC_1016_75',
                        value = '(cw*ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1016_76 = Coupling(name = 'UVGC_1016_76',
                        value = '(cw*ee*FRCTdeltaZxssLssLxgos*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLssLxgos*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLssLxgos*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1016_77 = Coupling(name = 'UVGC_1016_77',
                        value = '(cw*ee*FRCTdeltaZxssLxgossL*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLxgossL*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLxgossL*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1017_78 = Coupling(name = 'UVGC_1017_78',
                        value = '(cw*ee*FRCTdeltaZxccLxcG*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxccLxcG*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxccLxcG*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1017_79 = Coupling(name = 'UVGC_1017_79',
                        value = '(cw*ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1017_80 = Coupling(name = 'UVGC_1017_80',
                        value = '(cw*ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1017_81 = Coupling(name = 'UVGC_1017_81',
                        value = '(cw*ee*FRCTdeltaZxccLxgoscL*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxccLxgoscL*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxccLxgoscL*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1017_82 = Coupling(name = 'UVGC_1017_82',
                        value = '-(cw*ee*complex(0,1)*G**2*invFREps*NN4x1)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*invFREps*NN4x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*invFREps*NN4x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1018_83 = Coupling(name = 'UVGC_1018_83',
                        value = '(cw*ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1018_84 = Coupling(name = 'UVGC_1018_84',
                        value = '(cw*ee*FRCTdeltaZxttLxtG*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxttLxtG*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxttLxtG*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1018_85 = Coupling(name = 'UVGC_1018_85',
                        value = '(cw*ee*FRCTdeltaZxttLxgostL*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxttLxgostL*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxttLxgostL*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1018_86 = Coupling(name = 'UVGC_1018_86',
                        value = '(cw*ee*FRCTdeltaZxttLxgostR*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxttLxgostR*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxttLxgostR*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1018_87 = Coupling(name = 'UVGC_1018_87',
                        value = '(FRCTdeltaZxstRstLxgot*complex(0,1)*NN4x4*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstRstLxgot*complex(0,1)*NN4x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) + (cw*ee*FRCTdeltaZxstLstLxgot*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxstLstLxgot*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxstLstLxgot*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1019_88 = Coupling(name = 'UVGC_1019_88',
                        value = '(cw*ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1019_89 = Coupling(name = 'UVGC_1019_89',
                        value = '(cw*ee*FRCTdeltaZxuuLxuG*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxuuLxuG*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxuuLxuG*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1019_90 = Coupling(name = 'UVGC_1019_90',
                        value = '(cw*ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1019_91 = Coupling(name = 'UVGC_1019_91',
                        value = '(cw*ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*NN4x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*NN4x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1020_92 = Coupling(name = 'UVGC_1020_92',
                        value = '-(ee*FRCTdeltaZxccLxcG*complex(0,1)*UU1x1)/(2.*sw)',
                        order = {'QCD':2,'QED':1})

UVGC_1020_93 = Coupling(name = 'UVGC_1020_93',
                        value = '-(ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*UU1x1)/(2.*sw)',
                        order = {'QCD':2,'QED':1})

UVGC_1020_94 = Coupling(name = 'UVGC_1020_94',
                        value = '-(ee*FRCTdeltaZxssLssLxgos*complex(0,1)*UU1x1)/(2.*sw)',
                        order = {'QCD':2,'QED':1})

UVGC_1020_95 = Coupling(name = 'UVGC_1020_95',
                        value = '-(ee*FRCTdeltaZxccLxgoscL*complex(0,1)*UU1x1)/(2.*sw)',
                        order = {'QCD':2,'QED':1})

UVGC_1020_96 = Coupling(name = 'UVGC_1020_96',
                        value = '(ee*complex(0,1)*G**2*invFREps*UU1x1)/(12.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':1})

UVGC_1021_97 = Coupling(name = 'UVGC_1021_97',
                        value = '-(ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*UU1x1)/(2.*sw)',
                        order = {'QCD':2,'QED':1})

UVGC_1021_98 = Coupling(name = 'UVGC_1021_98',
                        value = '-(ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*UU1x1)/(2.*sw)',
                        order = {'QCD':2,'QED':1})

UVGC_1021_99 = Coupling(name = 'UVGC_1021_99',
                        value = '-(ee*FRCTdeltaZxttLxtG*complex(0,1)*UU1x1)/(2.*sw)',
                        order = {'QCD':2,'QED':1})

UVGC_1021_100 = Coupling(name = 'UVGC_1021_100',
                         value = '-(ee*FRCTdeltaZxttLxgostL*complex(0,1)*UU1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1021_101 = Coupling(name = 'UVGC_1021_101',
                         value = '-(ee*FRCTdeltaZxttLxgostR*complex(0,1)*UU1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1022_102 = Coupling(name = 'UVGC_1022_102',
                         value = '-(ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*UU1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1022_103 = Coupling(name = 'UVGC_1022_103',
                         value = '-(ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*UU1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1022_104 = Coupling(name = 'UVGC_1022_104',
                         value = '-(ee*FRCTdeltaZxuuLxuG*complex(0,1)*UU1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1022_105 = Coupling(name = 'UVGC_1022_105',
                         value = '-(ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*UU1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1023_106 = Coupling(name = 'UVGC_1023_106',
                         value = '-(ee*FRCTdeltaZxccLxcG*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1023_107 = Coupling(name = 'UVGC_1023_107',
                         value = '-(ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1023_108 = Coupling(name = 'UVGC_1023_108',
                         value = '-(ee*FRCTdeltaZxssLssLxgos*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1023_109 = Coupling(name = 'UVGC_1023_109',
                         value = '-(ee*FRCTdeltaZxccLxgoscL*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1023_110 = Coupling(name = 'UVGC_1023_110',
                         value = '(ee*complex(0,1)*G**2*invFREps*UU2x1)/(12.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1024_111 = Coupling(name = 'UVGC_1024_111',
                         value = '-(ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1024_112 = Coupling(name = 'UVGC_1024_112',
                         value = '-(ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1024_113 = Coupling(name = 'UVGC_1024_113',
                         value = '-(ee*FRCTdeltaZxttLxtG*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1024_114 = Coupling(name = 'UVGC_1024_114',
                         value = '-(ee*FRCTdeltaZxttLxgostL*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1024_115 = Coupling(name = 'UVGC_1024_115',
                         value = '-(ee*FRCTdeltaZxttLxgostR*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1025_116 = Coupling(name = 'UVGC_1025_116',
                         value = '-(ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1025_117 = Coupling(name = 'UVGC_1025_117',
                         value = '-(ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1025_118 = Coupling(name = 'UVGC_1025_118',
                         value = '-(ee*FRCTdeltaZxuuLxuG*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1025_119 = Coupling(name = 'UVGC_1025_119',
                         value = '-(ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*UU2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1026_120 = Coupling(name = 'UVGC_1026_120',
                         value = '-(ee*FRCTdeltaZxbbLxbG*complex(0,1)*VV1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1026_121 = Coupling(name = 'UVGC_1026_121',
                         value = '-(ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*VV1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1026_122 = Coupling(name = 'UVGC_1026_122',
                         value = '-(ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*VV1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1026_123 = Coupling(name = 'UVGC_1026_123',
                         value = '-(ee*FRCTdeltaZxstLstLxgot*complex(0,1)*VV1x1)/(2.*sw) + (FRCTdeltaZxstRstLxgot*complex(0,1)*VV1x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1026_124 = Coupling(name = 'UVGC_1026_124',
                         value = '(ee*complex(0,1)*G**2*invFREps*VV1x1)/(12.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1027_125 = Coupling(name = 'UVGC_1027_125',
                         value = '-(ee*FRCTdeltaZxddLxdG*complex(0,1)*VV1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1027_126 = Coupling(name = 'UVGC_1027_126',
                         value = '-(ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*VV1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1027_127 = Coupling(name = 'UVGC_1027_127',
                         value = '-(ee*FRCTdeltaZxddLxgosdL*complex(0,1)*VV1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1027_128 = Coupling(name = 'UVGC_1027_128',
                         value = '-(ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*VV1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1028_129 = Coupling(name = 'UVGC_1028_129',
                         value = '-(ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*VV1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1028_130 = Coupling(name = 'UVGC_1028_130',
                         value = '-(ee*FRCTdeltaZxssLxsG*complex(0,1)*VV1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1028_131 = Coupling(name = 'UVGC_1028_131',
                         value = '-(ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*VV1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1028_132 = Coupling(name = 'UVGC_1028_132',
                         value = '-(ee*FRCTdeltaZxssLxgossL*complex(0,1)*VV1x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1029_133 = Coupling(name = 'UVGC_1029_133',
                         value = '-(ee*FRCTdeltaZxbbLxbG*complex(0,1)*VV2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1029_134 = Coupling(name = 'UVGC_1029_134',
                         value = '-(ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*VV2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1029_135 = Coupling(name = 'UVGC_1029_135',
                         value = '-(ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*VV2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1029_136 = Coupling(name = 'UVGC_1029_136',
                         value = '-(ee*FRCTdeltaZxstLstLxgot*complex(0,1)*VV2x1)/(2.*sw) + (FRCTdeltaZxstRstLxgot*complex(0,1)*VV2x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1029_137 = Coupling(name = 'UVGC_1029_137',
                         value = '(ee*complex(0,1)*G**2*invFREps*VV2x1)/(12.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1030_138 = Coupling(name = 'UVGC_1030_138',
                         value = '-(ee*FRCTdeltaZxddLxdG*complex(0,1)*VV2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1030_139 = Coupling(name = 'UVGC_1030_139',
                         value = '-(ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*VV2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1030_140 = Coupling(name = 'UVGC_1030_140',
                         value = '-(ee*FRCTdeltaZxddLxgosdL*complex(0,1)*VV2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1030_141 = Coupling(name = 'UVGC_1030_141',
                         value = '-(ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*VV2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1031_142 = Coupling(name = 'UVGC_1031_142',
                         value = '-(ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*VV2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1031_143 = Coupling(name = 'UVGC_1031_143',
                         value = '-(ee*FRCTdeltaZxssLxsG*complex(0,1)*VV2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1031_144 = Coupling(name = 'UVGC_1031_144',
                         value = '-(ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*VV2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1031_145 = Coupling(name = 'UVGC_1031_145',
                         value = '-(ee*FRCTdeltaZxssLxgossL*complex(0,1)*VV2x1)/(2.*sw)',
                         order = {'QCD':2,'QED':1})

UVGC_1032_146 = Coupling(name = 'UVGC_1032_146',
                         value = '(FRCTdeltaZxstLstLxGstL*complex(0,1)*NN1x4*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstLstLxGstL*complex(0,1)*NN1x4*sw**2*yu3x3)/(2.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1032_147 = Coupling(name = 'UVGC_1032_147',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*NN1x4*yu3x3)/(2.*(-1 + sw**2)) + (FRCTdeltaxMTxtG*complex(0,1)*NN1x4*yu3x3)/(MT*(-1 + sw**2)) - (FRCTdeltaZxttRxtG*complex(0,1)*NN1x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaxMTxtG*complex(0,1)*NN1x4*sw**2*yu3x3)/(MT*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1032_148 = Coupling(name = 'UVGC_1032_148',
                         value = '(FRCTdeltaZxttRxgostL*complex(0,1)*NN1x4*yu3x3)/(2.*(-1 + sw**2)) + (FRCTdeltaxMTxgostL*complex(0,1)*NN1x4*yu3x3)/(MT*(-1 + sw**2)) - (FRCTdeltaZxttRxgostL*complex(0,1)*NN1x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaxMTxgostL*complex(0,1)*NN1x4*sw**2*yu3x3)/(MT*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1032_149 = Coupling(name = 'UVGC_1032_149',
                         value = '(FRCTdeltaZxttRxgostR*complex(0,1)*NN1x4*yu3x3)/(2.*(-1 + sw**2)) + (FRCTdeltaxMTxgostR*complex(0,1)*NN1x4*yu3x3)/(MT*(-1 + sw**2)) - (FRCTdeltaZxttRxgostR*complex(0,1)*NN1x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaxMTxgostR*complex(0,1)*NN1x4*sw**2*yu3x3)/(MT*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1032_150 = Coupling(name = 'UVGC_1032_150',
                         value = '(FRCTdeltaZxstLstLxgot*complex(0,1)*NN1x4*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstLstLxgot*complex(0,1)*NN1x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (cw*ee*FRCTdeltaZxstRstLxgot*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1032_151 = Coupling(name = 'UVGC_1032_151',
                         value = '-(complex(0,1)*G**2*invFREps*NN1x4*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*invFREps*NN1x4*sw**2*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1032_152 = Coupling(name = 'UVGC_1032_152',
                         value = '-(complex(0,1)*G**2*invFREps*NN1x4*yu3x3)/(6.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*invFREps*NN1x4*sw**2*yu3x3)/(6.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1033_153 = Coupling(name = 'UVGC_1033_153',
                         value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*NN1x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxGstR*complex(0,1)*NN1x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1033_154 = Coupling(name = 'UVGC_1033_154',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*NN1x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaxMTxtG*complex(0,1)*NN1x4*yu3x3)/(MT*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxttLxtG*complex(0,1)*NN1x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaxMTxtG*complex(0,1)*NN1x4*sw**2*yu3x3)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1033_155 = Coupling(name = 'UVGC_1033_155',
                         value = '(FRCTdeltaZxttLxgostL*complex(0,1)*NN1x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaxMTxgostL*complex(0,1)*NN1x4*yu3x3)/(MT*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxttLxgostL*complex(0,1)*NN1x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaxMTxgostL*complex(0,1)*NN1x4*sw**2*yu3x3)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1033_156 = Coupling(name = 'UVGC_1033_156',
                         value = '(FRCTdeltaZxttLxgostR*complex(0,1)*NN1x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaxMTxgostR*complex(0,1)*NN1x4*yu3x3)/(MT*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxttLxgostR*complex(0,1)*NN1x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaxMTxgostR*complex(0,1)*NN1x4*sw**2*yu3x3)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1033_157 = Coupling(name = 'UVGC_1033_157',
                         value = '(FRCTdeltaZxstRstRxgot*complex(0,1)*NN1x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxgot*complex(0,1)*NN1x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (cw*ee*FRCTdeltaZxstLstRxgot*complex(0,1)*NN1x1)/(6.*(-1 + sw)*(1 + sw)*cmath.sqrt(2)) + (ee*FRCTdeltaZxstLstRxgot*complex(0,1)*NN1x2)/(2.*(-1 + sw)*sw*(1 + sw)*cmath.sqrt(2)) - (ee*FRCTdeltaZxstLstRxgot*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_1034_158 = Coupling(name = 'UVGC_1034_158',
                         value = '(FRCTdeltaZxstLstLxGstL*complex(0,1)*NN2x4*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstLstLxGstL*complex(0,1)*NN2x4*sw**2*yu3x3)/(2.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1034_159 = Coupling(name = 'UVGC_1034_159',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*NN2x4*yu3x3)/(2.*(-1 + sw**2)) + (FRCTdeltaxMTxtG*complex(0,1)*NN2x4*yu3x3)/(MT*(-1 + sw**2)) - (FRCTdeltaZxttRxtG*complex(0,1)*NN2x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaxMTxtG*complex(0,1)*NN2x4*sw**2*yu3x3)/(MT*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1034_160 = Coupling(name = 'UVGC_1034_160',
                         value = '(FRCTdeltaZxttRxgostL*complex(0,1)*NN2x4*yu3x3)/(2.*(-1 + sw**2)) + (FRCTdeltaxMTxgostL*complex(0,1)*NN2x4*yu3x3)/(MT*(-1 + sw**2)) - (FRCTdeltaZxttRxgostL*complex(0,1)*NN2x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaxMTxgostL*complex(0,1)*NN2x4*sw**2*yu3x3)/(MT*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1034_161 = Coupling(name = 'UVGC_1034_161',
                         value = '(FRCTdeltaZxttRxgostR*complex(0,1)*NN2x4*yu3x3)/(2.*(-1 + sw**2)) + (FRCTdeltaxMTxgostR*complex(0,1)*NN2x4*yu3x3)/(MT*(-1 + sw**2)) - (FRCTdeltaZxttRxgostR*complex(0,1)*NN2x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaxMTxgostR*complex(0,1)*NN2x4*sw**2*yu3x3)/(MT*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1034_162 = Coupling(name = 'UVGC_1034_162',
                         value = '(FRCTdeltaZxstLstLxgot*complex(0,1)*NN2x4*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstLstLxgot*complex(0,1)*NN2x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (cw*ee*FRCTdeltaZxstRstLxgot*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1034_163 = Coupling(name = 'UVGC_1034_163',
                         value = '-(complex(0,1)*G**2*invFREps*NN2x4*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*invFREps*NN2x4*sw**2*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1034_164 = Coupling(name = 'UVGC_1034_164',
                         value = '-(complex(0,1)*G**2*invFREps*NN2x4*yu3x3)/(6.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*invFREps*NN2x4*sw**2*yu3x3)/(6.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1035_165 = Coupling(name = 'UVGC_1035_165',
                         value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*NN2x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxGstR*complex(0,1)*NN2x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1035_166 = Coupling(name = 'UVGC_1035_166',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*NN2x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaxMTxtG*complex(0,1)*NN2x4*yu3x3)/(MT*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxttLxtG*complex(0,1)*NN2x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaxMTxtG*complex(0,1)*NN2x4*sw**2*yu3x3)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1035_167 = Coupling(name = 'UVGC_1035_167',
                         value = '(FRCTdeltaZxttLxgostL*complex(0,1)*NN2x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaxMTxgostL*complex(0,1)*NN2x4*yu3x3)/(MT*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxttLxgostL*complex(0,1)*NN2x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaxMTxgostL*complex(0,1)*NN2x4*sw**2*yu3x3)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1035_168 = Coupling(name = 'UVGC_1035_168',
                         value = '(FRCTdeltaZxttLxgostR*complex(0,1)*NN2x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaxMTxgostR*complex(0,1)*NN2x4*yu3x3)/(MT*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxttLxgostR*complex(0,1)*NN2x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaxMTxgostR*complex(0,1)*NN2x4*sw**2*yu3x3)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1035_169 = Coupling(name = 'UVGC_1035_169',
                         value = '(FRCTdeltaZxstRstRxgot*complex(0,1)*NN2x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxgot*complex(0,1)*NN2x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (cw*ee*FRCTdeltaZxstLstRxgot*complex(0,1)*NN2x1)/(6.*(-1 + sw)*(1 + sw)*cmath.sqrt(2)) + (ee*FRCTdeltaZxstLstRxgot*complex(0,1)*NN2x2)/(2.*(-1 + sw)*sw*(1 + sw)*cmath.sqrt(2)) - (ee*FRCTdeltaZxstLstRxgot*complex(0,1)*NN2x2*sw)/(2.*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_1036_170 = Coupling(name = 'UVGC_1036_170',
                         value = '(FRCTdeltaZxstLstLxGstL*complex(0,1)*NN3x4*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstLstLxGstL*complex(0,1)*NN3x4*sw**2*yu3x3)/(2.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1036_171 = Coupling(name = 'UVGC_1036_171',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*NN3x4*yu3x3)/(2.*(-1 + sw**2)) + (FRCTdeltaxMTxtG*complex(0,1)*NN3x4*yu3x3)/(MT*(-1 + sw**2)) - (FRCTdeltaZxttRxtG*complex(0,1)*NN3x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaxMTxtG*complex(0,1)*NN3x4*sw**2*yu3x3)/(MT*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1036_172 = Coupling(name = 'UVGC_1036_172',
                         value = '(FRCTdeltaZxttRxgostL*complex(0,1)*NN3x4*yu3x3)/(2.*(-1 + sw**2)) + (FRCTdeltaxMTxgostL*complex(0,1)*NN3x4*yu3x3)/(MT*(-1 + sw**2)) - (FRCTdeltaZxttRxgostL*complex(0,1)*NN3x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaxMTxgostL*complex(0,1)*NN3x4*sw**2*yu3x3)/(MT*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1036_173 = Coupling(name = 'UVGC_1036_173',
                         value = '(FRCTdeltaZxttRxgostR*complex(0,1)*NN3x4*yu3x3)/(2.*(-1 + sw**2)) + (FRCTdeltaxMTxgostR*complex(0,1)*NN3x4*yu3x3)/(MT*(-1 + sw**2)) - (FRCTdeltaZxttRxgostR*complex(0,1)*NN3x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaxMTxgostR*complex(0,1)*NN3x4*sw**2*yu3x3)/(MT*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1036_174 = Coupling(name = 'UVGC_1036_174',
                         value = '(FRCTdeltaZxstLstLxgot*complex(0,1)*NN3x4*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstLstLxgot*complex(0,1)*NN3x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (cw*ee*FRCTdeltaZxstRstLxgot*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1036_175 = Coupling(name = 'UVGC_1036_175',
                         value = '-(complex(0,1)*G**2*invFREps*NN3x4*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*invFREps*NN3x4*sw**2*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1036_176 = Coupling(name = 'UVGC_1036_176',
                         value = '-(complex(0,1)*G**2*invFREps*NN3x4*yu3x3)/(6.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*invFREps*NN3x4*sw**2*yu3x3)/(6.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1037_177 = Coupling(name = 'UVGC_1037_177',
                         value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*NN3x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxGstR*complex(0,1)*NN3x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1037_178 = Coupling(name = 'UVGC_1037_178',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*NN3x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaxMTxtG*complex(0,1)*NN3x4*yu3x3)/(MT*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxttLxtG*complex(0,1)*NN3x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaxMTxtG*complex(0,1)*NN3x4*sw**2*yu3x3)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1037_179 = Coupling(name = 'UVGC_1037_179',
                         value = '(FRCTdeltaZxttLxgostL*complex(0,1)*NN3x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaxMTxgostL*complex(0,1)*NN3x4*yu3x3)/(MT*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxttLxgostL*complex(0,1)*NN3x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaxMTxgostL*complex(0,1)*NN3x4*sw**2*yu3x3)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1037_180 = Coupling(name = 'UVGC_1037_180',
                         value = '(FRCTdeltaZxttLxgostR*complex(0,1)*NN3x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaxMTxgostR*complex(0,1)*NN3x4*yu3x3)/(MT*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxttLxgostR*complex(0,1)*NN3x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaxMTxgostR*complex(0,1)*NN3x4*sw**2*yu3x3)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1037_181 = Coupling(name = 'UVGC_1037_181',
                         value = '(FRCTdeltaZxstRstRxgot*complex(0,1)*NN3x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxgot*complex(0,1)*NN3x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (cw*ee*FRCTdeltaZxstLstRxgot*complex(0,1)*NN3x1)/(6.*(-1 + sw)*(1 + sw)*cmath.sqrt(2)) + (ee*FRCTdeltaZxstLstRxgot*complex(0,1)*NN3x2)/(2.*(-1 + sw)*sw*(1 + sw)*cmath.sqrt(2)) - (ee*FRCTdeltaZxstLstRxgot*complex(0,1)*NN3x2*sw)/(2.*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_1038_182 = Coupling(name = 'UVGC_1038_182',
                         value = '(FRCTdeltaZxstLstLxGstL*complex(0,1)*NN4x4*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstLstLxGstL*complex(0,1)*NN4x4*sw**2*yu3x3)/(2.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1038_183 = Coupling(name = 'UVGC_1038_183',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*NN4x4*yu3x3)/(2.*(-1 + sw**2)) + (FRCTdeltaxMTxtG*complex(0,1)*NN4x4*yu3x3)/(MT*(-1 + sw**2)) - (FRCTdeltaZxttRxtG*complex(0,1)*NN4x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaxMTxtG*complex(0,1)*NN4x4*sw**2*yu3x3)/(MT*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1038_184 = Coupling(name = 'UVGC_1038_184',
                         value = '(FRCTdeltaZxttRxgostL*complex(0,1)*NN4x4*yu3x3)/(2.*(-1 + sw**2)) + (FRCTdeltaxMTxgostL*complex(0,1)*NN4x4*yu3x3)/(MT*(-1 + sw**2)) - (FRCTdeltaZxttRxgostL*complex(0,1)*NN4x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaxMTxgostL*complex(0,1)*NN4x4*sw**2*yu3x3)/(MT*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1038_185 = Coupling(name = 'UVGC_1038_185',
                         value = '(FRCTdeltaZxttRxgostR*complex(0,1)*NN4x4*yu3x3)/(2.*(-1 + sw**2)) + (FRCTdeltaxMTxgostR*complex(0,1)*NN4x4*yu3x3)/(MT*(-1 + sw**2)) - (FRCTdeltaZxttRxgostR*complex(0,1)*NN4x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaxMTxgostR*complex(0,1)*NN4x4*sw**2*yu3x3)/(MT*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1038_186 = Coupling(name = 'UVGC_1038_186',
                         value = '(FRCTdeltaZxstLstLxgot*complex(0,1)*NN4x4*yu3x3)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstLstLxgot*complex(0,1)*NN4x4*sw**2*yu3x3)/(2.*(-1 + sw**2)) - (cw*ee*FRCTdeltaZxstRstLxgot*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1038_187 = Coupling(name = 'UVGC_1038_187',
                         value = '-(complex(0,1)*G**2*invFREps*NN4x4*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*invFREps*NN4x4*sw**2*yu3x3)/(12.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1038_188 = Coupling(name = 'UVGC_1038_188',
                         value = '-(complex(0,1)*G**2*invFREps*NN4x4*yu3x3)/(6.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*invFREps*NN4x4*sw**2*yu3x3)/(6.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_1039_189 = Coupling(name = 'UVGC_1039_189',
                         value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*NN4x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxGstR*complex(0,1)*NN4x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1039_190 = Coupling(name = 'UVGC_1039_190',
                         value = '(FRCTdeltaZxttLxtG*complex(0,1)*NN4x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaxMTxtG*complex(0,1)*NN4x4*yu3x3)/(MT*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxttLxtG*complex(0,1)*NN4x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaxMTxtG*complex(0,1)*NN4x4*sw**2*yu3x3)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1039_191 = Coupling(name = 'UVGC_1039_191',
                         value = '(FRCTdeltaZxttLxgostL*complex(0,1)*NN4x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaxMTxgostL*complex(0,1)*NN4x4*yu3x3)/(MT*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxttLxgostL*complex(0,1)*NN4x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaxMTxgostL*complex(0,1)*NN4x4*sw**2*yu3x3)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1039_192 = Coupling(name = 'UVGC_1039_192',
                         value = '(FRCTdeltaZxttLxgostR*complex(0,1)*NN4x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaxMTxgostR*complex(0,1)*NN4x4*yu3x3)/(MT*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxttLxgostR*complex(0,1)*NN4x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaxMTxgostR*complex(0,1)*NN4x4*sw**2*yu3x3)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_1039_193 = Coupling(name = 'UVGC_1039_193',
                         value = '(FRCTdeltaZxstRstRxgot*complex(0,1)*NN4x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxgot*complex(0,1)*NN4x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) + (cw*ee*FRCTdeltaZxstLstRxgot*complex(0,1)*NN4x1)/(6.*(-1 + sw)*(1 + sw)*cmath.sqrt(2)) + (ee*FRCTdeltaZxstLstRxgot*complex(0,1)*NN4x2)/(2.*(-1 + sw)*sw*(1 + sw)*cmath.sqrt(2)) - (ee*FRCTdeltaZxstLstRxgot*complex(0,1)*NN4x2*sw)/(2.*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_1040_194 = Coupling(name = 'UVGC_1040_194',
                         value = '(FRCTdeltaZxbbLxbG*complex(0,1)*VV1x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1040_195 = Coupling(name = 'UVGC_1040_195',
                         value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*VV1x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1040_196 = Coupling(name = 'UVGC_1040_196',
                         value = '(FRCTdeltaxMTxtG*complex(0,1)*VV1x2*yu3x3)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_1040_197 = Coupling(name = 'UVGC_1040_197',
                         value = '(FRCTdeltaZxbbLxgosbL*complex(0,1)*VV1x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1040_198 = Coupling(name = 'UVGC_1040_198',
                         value = '(FRCTdeltaxMTxgostL*complex(0,1)*VV1x2*yu3x3)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_1040_199 = Coupling(name = 'UVGC_1040_199',
                         value = '(FRCTdeltaxMTxgostR*complex(0,1)*VV1x2*yu3x3)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_1040_200 = Coupling(name = 'UVGC_1040_200',
                         value = '-(ee*FRCTdeltaZxstLstRxgot*complex(0,1)*VV1x1)/(2.*sw) + (FRCTdeltaZxstRstRxgot*complex(0,1)*VV1x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1040_201 = Coupling(name = 'UVGC_1040_201',
                         value = '-(complex(0,1)*G**2*invFREps*VV1x2*yu3x3)/(12.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_1040_202 = Coupling(name = 'UVGC_1040_202',
                         value = '-(complex(0,1)*G**2*invFREps*VV1x2*yu3x3)/(6.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_1041_203 = Coupling(name = 'UVGC_1041_203',
                         value = '(FRCTdeltaZxsbLsbLxgob*complex(0,1)*VV1x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1041_204 = Coupling(name = 'UVGC_1041_204',
                         value = '(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*VV1x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1041_205 = Coupling(name = 'UVGC_1041_205',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*VV1x2*yu3x3)/2. + (FRCTdeltaxMTxtG*complex(0,1)*VV1x2*yu3x3)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_1041_206 = Coupling(name = 'UVGC_1041_206',
                         value = '(FRCTdeltaZxttRxgostL*complex(0,1)*VV1x2*yu3x3)/2. + (FRCTdeltaxMTxgostL*complex(0,1)*VV1x2*yu3x3)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_1041_207 = Coupling(name = 'UVGC_1041_207',
                         value = '(FRCTdeltaZxttRxgostR*complex(0,1)*VV1x2*yu3x3)/2. + (FRCTdeltaxMTxgostR*complex(0,1)*VV1x2*yu3x3)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_1042_208 = Coupling(name = 'UVGC_1042_208',
                         value = '(FRCTdeltaZxbbLxbG*complex(0,1)*VV2x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1042_209 = Coupling(name = 'UVGC_1042_209',
                         value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*VV2x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1042_210 = Coupling(name = 'UVGC_1042_210',
                         value = '(FRCTdeltaxMTxtG*complex(0,1)*VV2x2*yu3x3)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_1042_211 = Coupling(name = 'UVGC_1042_211',
                         value = '(FRCTdeltaZxbbLxgosbL*complex(0,1)*VV2x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1042_212 = Coupling(name = 'UVGC_1042_212',
                         value = '(FRCTdeltaxMTxgostL*complex(0,1)*VV2x2*yu3x3)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_1042_213 = Coupling(name = 'UVGC_1042_213',
                         value = '(FRCTdeltaxMTxgostR*complex(0,1)*VV2x2*yu3x3)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_1042_214 = Coupling(name = 'UVGC_1042_214',
                         value = '-(ee*FRCTdeltaZxstLstRxgot*complex(0,1)*VV2x1)/(2.*sw) + (FRCTdeltaZxstRstRxgot*complex(0,1)*VV2x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1042_215 = Coupling(name = 'UVGC_1042_215',
                         value = '-(complex(0,1)*G**2*invFREps*VV2x2*yu3x3)/(12.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_1042_216 = Coupling(name = 'UVGC_1042_216',
                         value = '-(complex(0,1)*G**2*invFREps*VV2x2*yu3x3)/(6.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_1043_217 = Coupling(name = 'UVGC_1043_217',
                         value = '(FRCTdeltaZxsbLsbLxgob*complex(0,1)*VV2x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1043_218 = Coupling(name = 'UVGC_1043_218',
                         value = '(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*VV2x2*yu3x3)/2.',
                         order = {'QCD':2,'QED':1})

UVGC_1043_219 = Coupling(name = 'UVGC_1043_219',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*VV2x2*yu3x3)/2. + (FRCTdeltaxMTxtG*complex(0,1)*VV2x2*yu3x3)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_1043_220 = Coupling(name = 'UVGC_1043_220',
                         value = '(FRCTdeltaZxttRxgostL*complex(0,1)*VV2x2*yu3x3)/2. + (FRCTdeltaxMTxgostL*complex(0,1)*VV2x2*yu3x3)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_1043_221 = Coupling(name = 'UVGC_1043_221',
                         value = '(FRCTdeltaZxttRxgostR*complex(0,1)*VV2x2*yu3x3)/2. + (FRCTdeltaxMTxgostR*complex(0,1)*VV2x2*yu3x3)/MT',
                         order = {'QCD':2,'QED':1})

UVGC_271_222 = Coupling(name = 'UVGC_271_222',
                        value = '(11*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_272_223 = Coupling(name = 'UVGC_272_223',
                        value = '-(complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_273_224 = Coupling(name = 'UVGC_273_224',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_274_225 = Coupling(name = 'UVGC_274_225',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_285_226 = Coupling(name = 'UVGC_285_226',
                        value = '-((FRCTdeltaZxstLstRxgot*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_286_227 = Coupling(name = 'UVGC_286_227',
                        value = '(FRCTdeltaZxstRstLxgot*complex(0,1)*G)/cmath.sqrt(2)',
                        order = {'QCD':3})

UVGC_287_228 = Coupling(name = 'UVGC_287_228',
                        value = '-(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(4.*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_288_229 = Coupling(name = 'UVGC_288_229',
                        value = '(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(4.*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_289_230 = Coupling(name = 'UVGC_289_230',
                        value = '-(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_290_231 = Coupling(name = 'UVGC_290_231',
                        value = '-(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_291_232 = Coupling(name = 'UVGC_291_232',
                        value = '-(ee*FRCTdeltaZxstLstRxgot*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_292_233 = Coupling(name = 'UVGC_292_233',
                        value = '(ee*FRCTdeltaZxstLstRxgot*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_293_234 = Coupling(name = 'UVGC_293_234',
                        value = '(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(6.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

UVGC_294_235 = Coupling(name = 'UVGC_294_235',
                        value = '(ee*FRCTdeltaZxstLstRxgot*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                        order = {'QCD':3,'QED':1})

UVGC_295_236 = Coupling(name = 'UVGC_295_236',
                        value = '(cw*ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

UVGC_296_237 = Coupling(name = 'UVGC_296_237',
                        value = '(complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_297_238 = Coupling(name = 'UVGC_297_238',
                        value = '(3*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_297_239 = Coupling(name = 'UVGC_297_239',
                        value = '(-3*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_298_240 = Coupling(name = 'UVGC_298_240',
                        value = '(3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_298_241 = Coupling(name = 'UVGC_298_241',
                        value = '(-3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_300_242 = Coupling(name = 'UVGC_300_242',
                        value = '-(complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_300_243 = Coupling(name = 'UVGC_300_243',
                        value = '(complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_301_244 = Coupling(name = 'UVGC_301_244',
                        value = '(-3*complex(0,1)*G**4*invFREps)/(256.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_301_245 = Coupling(name = 'UVGC_301_245',
                        value = '(3*complex(0,1)*G**4*invFREps)/(256.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_246 = Coupling(name = 'UVGC_302_246',
                        value = '-2*FRCTdeltaZxGGxb*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_302_247 = Coupling(name = 'UVGC_302_247',
                        value = '-2*FRCTdeltaZxGGxc*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_302_248 = Coupling(name = 'UVGC_302_248',
                        value = '-2*FRCTdeltaZxGGxd*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_302_249 = Coupling(name = 'UVGC_302_249',
                        value = '-2*FRCTdeltaZxGGxG*complex(0,1)*G**2 + (31*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_250 = Coupling(name = 'UVGC_302_250',
                        value = '-2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_251 = Coupling(name = 'UVGC_302_251',
                        value = '-((FRCTdeltaxaSxgo*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxgo*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_302_252 = Coupling(name = 'UVGC_302_252',
                        value = '-2*FRCTdeltaZxGGxs*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_302_253 = Coupling(name = 'UVGC_302_253',
                        value = '-((FRCTdeltaxaSxsbL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsbL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_254 = Coupling(name = 'UVGC_302_254',
                        value = '-((FRCTdeltaxaSxsbR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsbR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_255 = Coupling(name = 'UVGC_302_255',
                        value = '-((FRCTdeltaxaSxscL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxscL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_256 = Coupling(name = 'UVGC_302_256',
                        value = '-((FRCTdeltaxaSxscR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxscR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_257 = Coupling(name = 'UVGC_302_257',
                        value = '-((FRCTdeltaxaSxsdL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsdL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_258 = Coupling(name = 'UVGC_302_258',
                        value = '-((FRCTdeltaxaSxsdR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsdR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_259 = Coupling(name = 'UVGC_302_259',
                        value = '-((FRCTdeltaxaSxssL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxssL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_260 = Coupling(name = 'UVGC_302_260',
                        value = '-((FRCTdeltaxaSxssR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxssR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_261 = Coupling(name = 'UVGC_302_261',
                        value = '-((FRCTdeltaxaSxstL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxstL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_262 = Coupling(name = 'UVGC_302_262',
                        value = '-((FRCTdeltaxaSxstR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxstR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_263 = Coupling(name = 'UVGC_302_263',
                        value = '-((FRCTdeltaxaSxsuL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsuL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_264 = Coupling(name = 'UVGC_302_264',
                        value = '-((FRCTdeltaxaSxsuR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsuR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_302_265 = Coupling(name = 'UVGC_302_265',
                        value = '-((FRCTdeltaxaSxt*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxt*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_302_266 = Coupling(name = 'UVGC_302_266',
                        value = '-2*FRCTdeltaZxGGxu*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_303_267 = Coupling(name = 'UVGC_303_267',
                        value = '2*FRCTdeltaZxGGxb*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_303_268 = Coupling(name = 'UVGC_303_268',
                        value = '2*FRCTdeltaZxGGxc*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_303_269 = Coupling(name = 'UVGC_303_269',
                        value = '2*FRCTdeltaZxGGxd*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_303_270 = Coupling(name = 'UVGC_303_270',
                        value = '2*FRCTdeltaZxGGxG*complex(0,1)*G**2 - (37*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_271 = Coupling(name = 'UVGC_303_271',
                        value = '2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_272 = Coupling(name = 'UVGC_303_272',
                        value = '(FRCTdeltaxaSxgo*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxgo*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_303_273 = Coupling(name = 'UVGC_303_273',
                        value = '2*FRCTdeltaZxGGxs*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_303_274 = Coupling(name = 'UVGC_303_274',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsbL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_275 = Coupling(name = 'UVGC_303_275',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsbR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_276 = Coupling(name = 'UVGC_303_276',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxscL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_277 = Coupling(name = 'UVGC_303_277',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxscR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_278 = Coupling(name = 'UVGC_303_278',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsdL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_279 = Coupling(name = 'UVGC_303_279',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsdR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_280 = Coupling(name = 'UVGC_303_280',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxssL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_281 = Coupling(name = 'UVGC_303_281',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxssR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_282 = Coupling(name = 'UVGC_303_282',
                        value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxstL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_283 = Coupling(name = 'UVGC_303_283',
                        value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxstR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_284 = Coupling(name = 'UVGC_303_284',
                        value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsuL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_285 = Coupling(name = 'UVGC_303_285',
                        value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsuR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_303_286 = Coupling(name = 'UVGC_303_286',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxt*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_303_287 = Coupling(name = 'UVGC_303_287',
                        value = '2*FRCTdeltaZxGGxu*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_304_288 = Coupling(name = 'UVGC_304_288',
                        value = 'FRCTdeltaZxGGxb*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_289 = Coupling(name = 'UVGC_304_289',
                        value = 'FRCTdeltaZxGGxc*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_290 = Coupling(name = 'UVGC_304_290',
                        value = 'FRCTdeltaZxGGxd*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_291 = Coupling(name = 'UVGC_304_291',
                        value = 'FRCTdeltaZxGGxG*complex(0,1)',
                        order = {'QCD':2})

UVGC_304_292 = Coupling(name = 'UVGC_304_292',
                        value = 'FRCTdeltaZxGGxghG*complex(0,1)',
                        order = {'QCD':2})

UVGC_304_293 = Coupling(name = 'UVGC_304_293',
                        value = 'FRCTdeltaZxGGxgo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_294 = Coupling(name = 'UVGC_304_294',
                        value = 'FRCTdeltaZxGGxs*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_295 = Coupling(name = 'UVGC_304_295',
                        value = 'FRCTdeltaZxGGxsbL*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_296 = Coupling(name = 'UVGC_304_296',
                        value = 'FRCTdeltaZxGGxsbR*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_297 = Coupling(name = 'UVGC_304_297',
                        value = 'FRCTdeltaZxGGxscL*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_298 = Coupling(name = 'UVGC_304_298',
                        value = 'FRCTdeltaZxGGxscR*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_299 = Coupling(name = 'UVGC_304_299',
                        value = 'FRCTdeltaZxGGxsdL*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_300 = Coupling(name = 'UVGC_304_300',
                        value = 'FRCTdeltaZxGGxsdR*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_301 = Coupling(name = 'UVGC_304_301',
                        value = 'FRCTdeltaZxGGxssL*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_302 = Coupling(name = 'UVGC_304_302',
                        value = 'FRCTdeltaZxGGxssR*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_303 = Coupling(name = 'UVGC_304_303',
                        value = 'FRCTdeltaZxGGxstL*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_304 = Coupling(name = 'UVGC_304_304',
                        value = 'FRCTdeltaZxGGxstR*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_305 = Coupling(name = 'UVGC_304_305',
                        value = 'FRCTdeltaZxGGxsuL*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_306 = Coupling(name = 'UVGC_304_306',
                        value = 'FRCTdeltaZxGGxsuR*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_307 = Coupling(name = 'UVGC_304_307',
                        value = 'FRCTdeltaZxGGxt*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_304_308 = Coupling(name = 'UVGC_304_308',
                        value = 'FRCTdeltaZxGGxu*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_305_309 = Coupling(name = 'UVGC_305_309',
                        value = '(-3*FRCTdeltaZxGGxb*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_310 = Coupling(name = 'UVGC_305_310',
                        value = '(-3*FRCTdeltaZxGGxc*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_311 = Coupling(name = 'UVGC_305_311',
                        value = '(-3*FRCTdeltaZxGGxd*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_312 = Coupling(name = 'UVGC_305_312',
                        value = '(-3*FRCTdeltaZxGGxG*G)/2. - (15*G**3*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_313 = Coupling(name = 'UVGC_305_313',
                        value = '(-3*FRCTdeltaZxGGxghG*G)/2. - (G**3*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_314 = Coupling(name = 'UVGC_305_314',
                        value = '-(FRCTdeltaxaSxgo*G)/(2.*aS) - (3*FRCTdeltaZxGGxgo*G)/2. + (G**3*invFREps)/(8.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_315 = Coupling(name = 'UVGC_305_315',
                        value = '(-3*FRCTdeltaZxGGxs*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_316 = Coupling(name = 'UVGC_305_316',
                        value = '-(FRCTdeltaxaSxsbL*G)/(2.*aS) - (3*FRCTdeltaZxGGxsbL*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_317 = Coupling(name = 'UVGC_305_317',
                        value = '-(FRCTdeltaxaSxsbR*G)/(2.*aS) - (3*FRCTdeltaZxGGxsbR*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_318 = Coupling(name = 'UVGC_305_318',
                        value = '-(FRCTdeltaxaSxscL*G)/(2.*aS) - (3*FRCTdeltaZxGGxscL*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_319 = Coupling(name = 'UVGC_305_319',
                        value = '-(FRCTdeltaxaSxscR*G)/(2.*aS) - (3*FRCTdeltaZxGGxscR*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_320 = Coupling(name = 'UVGC_305_320',
                        value = '-(FRCTdeltaxaSxsdL*G)/(2.*aS) - (3*FRCTdeltaZxGGxsdL*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_321 = Coupling(name = 'UVGC_305_321',
                        value = '-(FRCTdeltaxaSxsdR*G)/(2.*aS) - (3*FRCTdeltaZxGGxsdR*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_322 = Coupling(name = 'UVGC_305_322',
                        value = '-(FRCTdeltaxaSxssL*G)/(2.*aS) - (3*FRCTdeltaZxGGxssL*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_323 = Coupling(name = 'UVGC_305_323',
                        value = '-(FRCTdeltaxaSxssR*G)/(2.*aS) - (3*FRCTdeltaZxGGxssR*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_324 = Coupling(name = 'UVGC_305_324',
                        value = '-(FRCTdeltaxaSxstL*G)/(2.*aS) - (3*FRCTdeltaZxGGxstL*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_325 = Coupling(name = 'UVGC_305_325',
                        value = '-(FRCTdeltaxaSxstR*G)/(2.*aS) - (3*FRCTdeltaZxGGxstR*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_326 = Coupling(name = 'UVGC_305_326',
                        value = '-(FRCTdeltaxaSxsuL*G)/(2.*aS) - (3*FRCTdeltaZxGGxsuL*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_327 = Coupling(name = 'UVGC_305_327',
                        value = '-(FRCTdeltaxaSxsuR*G)/(2.*aS) - (3*FRCTdeltaZxGGxsuR*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_328 = Coupling(name = 'UVGC_305_328',
                        value = '-(FRCTdeltaxaSxt*G)/(2.*aS) - (3*FRCTdeltaZxGGxt*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_305_329 = Coupling(name = 'UVGC_305_329',
                        value = '(-3*FRCTdeltaZxGGxu*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_306_330 = Coupling(name = 'UVGC_306_330',
                        value = '-(complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_306_331 = Coupling(name = 'UVGC_306_331',
                        value = '-(complex(0,1)*G**4*invFREps)/(8.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_306_332 = Coupling(name = 'UVGC_306_332',
                        value = '(complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_307_333 = Coupling(name = 'UVGC_307_333',
                        value = '(5*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_334 = Coupling(name = 'UVGC_308_334',
                        value = '2*FRCTdeltaZxGGxb*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_335 = Coupling(name = 'UVGC_308_335',
                        value = '2*FRCTdeltaZxGGxc*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_336 = Coupling(name = 'UVGC_308_336',
                        value = '2*FRCTdeltaZxGGxd*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_337 = Coupling(name = 'UVGC_308_337',
                        value = '2*FRCTdeltaZxGGxG*complex(0,1)*G**2 - (7*complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_338 = Coupling(name = 'UVGC_308_338',
                        value = '2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_339 = Coupling(name = 'UVGC_308_339',
                        value = '(FRCTdeltaxaSxgo*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxgo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(8.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_340 = Coupling(name = 'UVGC_308_340',
                        value = '2*FRCTdeltaZxGGxs*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_341 = Coupling(name = 'UVGC_308_341',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsbL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_342 = Coupling(name = 'UVGC_308_342',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsbR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_343 = Coupling(name = 'UVGC_308_343',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxscL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_344 = Coupling(name = 'UVGC_308_344',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxscR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_345 = Coupling(name = 'UVGC_308_345',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsdL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_346 = Coupling(name = 'UVGC_308_346',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsdR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_347 = Coupling(name = 'UVGC_308_347',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxssL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_348 = Coupling(name = 'UVGC_308_348',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxssR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_349 = Coupling(name = 'UVGC_308_349',
                        value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxstL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_350 = Coupling(name = 'UVGC_308_350',
                        value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxstR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_351 = Coupling(name = 'UVGC_308_351',
                        value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsuL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_352 = Coupling(name = 'UVGC_308_352',
                        value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsuR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_353 = Coupling(name = 'UVGC_308_353',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxt*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_308_354 = Coupling(name = 'UVGC_308_354',
                        value = '2*FRCTdeltaZxGGxu*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_309_355 = Coupling(name = 'UVGC_309_355',
                        value = '2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_356 = Coupling(name = 'UVGC_310_356',
                        value = '-2*FRCTdeltaZxGGxb*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_357 = Coupling(name = 'UVGC_310_357',
                        value = '-2*FRCTdeltaZxGGxc*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_358 = Coupling(name = 'UVGC_310_358',
                        value = '-2*FRCTdeltaZxGGxd*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_359 = Coupling(name = 'UVGC_310_359',
                        value = '-2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_360 = Coupling(name = 'UVGC_310_360',
                        value = '-((FRCTdeltaxaSxgo*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxgo*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(4.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_361 = Coupling(name = 'UVGC_310_361',
                        value = '-2*FRCTdeltaZxGGxs*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_362 = Coupling(name = 'UVGC_310_362',
                        value = '-((FRCTdeltaxaSxsbL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsbL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_363 = Coupling(name = 'UVGC_310_363',
                        value = '-((FRCTdeltaxaSxsbR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsbR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_364 = Coupling(name = 'UVGC_310_364',
                        value = '-((FRCTdeltaxaSxscL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxscL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_365 = Coupling(name = 'UVGC_310_365',
                        value = '-((FRCTdeltaxaSxscR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxscR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_366 = Coupling(name = 'UVGC_310_366',
                        value = '-((FRCTdeltaxaSxsdL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsdL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_367 = Coupling(name = 'UVGC_310_367',
                        value = '-((FRCTdeltaxaSxsdR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsdR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_368 = Coupling(name = 'UVGC_310_368',
                        value = '-((FRCTdeltaxaSxssL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxssL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_369 = Coupling(name = 'UVGC_310_369',
                        value = '-((FRCTdeltaxaSxssR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxssR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_370 = Coupling(name = 'UVGC_310_370',
                        value = '-((FRCTdeltaxaSxstL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxstL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_371 = Coupling(name = 'UVGC_310_371',
                        value = '-((FRCTdeltaxaSxstR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxstR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_372 = Coupling(name = 'UVGC_310_372',
                        value = '-((FRCTdeltaxaSxsuL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsuL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_373 = Coupling(name = 'UVGC_310_373',
                        value = '-((FRCTdeltaxaSxsuR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsuR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_374 = Coupling(name = 'UVGC_310_374',
                        value = '-((FRCTdeltaxaSxt*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxt*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_310_375 = Coupling(name = 'UVGC_310_375',
                        value = '-2*FRCTdeltaZxGGxu*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_376 = Coupling(name = 'UVGC_311_376',
                        value = '-2*FRCTdeltaZxGGxG*complex(0,1)*G**2 + (7*complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_377 = Coupling(name = 'UVGC_311_377',
                        value = '-2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_378 = Coupling(name = 'UVGC_311_378',
                        value = '-((FRCTdeltaxaSxsbL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsbL*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_379 = Coupling(name = 'UVGC_311_379',
                        value = '-((FRCTdeltaxaSxsbR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsbR*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_380 = Coupling(name = 'UVGC_311_380',
                        value = '-((FRCTdeltaxaSxscL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxscL*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_381 = Coupling(name = 'UVGC_311_381',
                        value = '-((FRCTdeltaxaSxscR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxscR*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_382 = Coupling(name = 'UVGC_311_382',
                        value = '-((FRCTdeltaxaSxsdL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsdL*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_383 = Coupling(name = 'UVGC_311_383',
                        value = '-((FRCTdeltaxaSxsdR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsdR*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_384 = Coupling(name = 'UVGC_311_384',
                        value = '-((FRCTdeltaxaSxssL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxssL*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_385 = Coupling(name = 'UVGC_311_385',
                        value = '-((FRCTdeltaxaSxssR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxssR*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_386 = Coupling(name = 'UVGC_311_386',
                        value = '-((FRCTdeltaxaSxstL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxstL*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_387 = Coupling(name = 'UVGC_311_387',
                        value = '-((FRCTdeltaxaSxstR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxstR*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_388 = Coupling(name = 'UVGC_311_388',
                        value = '-((FRCTdeltaxaSxsuL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsuL*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_311_389 = Coupling(name = 'UVGC_311_389',
                        value = '-((FRCTdeltaxaSxsuR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsuR*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_313_390 = Coupling(name = 'UVGC_313_390',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_314_391 = Coupling(name = 'UVGC_314_391',
                        value = '(-13*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_316_392 = Coupling(name = 'UVGC_316_392',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_321_393 = Coupling(name = 'UVGC_321_393',
                        value = '(-3*complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_321_394 = Coupling(name = 'UVGC_321_394',
                        value = '(3*complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_333_395 = Coupling(name = 'UVGC_333_395',
                        value = 'FRCTdeltaZxsbLsbLxgob*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_333_396 = Coupling(name = 'UVGC_333_396',
                        value = 'FRCTdeltaZxsbLsbLxGsbL*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_334_397 = Coupling(name = 'UVGC_334_397',
                        value = 'FRCTdeltaZxsbRsbRxgob*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_334_398 = Coupling(name = 'UVGC_334_398',
                        value = 'FRCTdeltaZxsbRsbRxGsbR*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_335_399 = Coupling(name = 'UVGC_335_399',
                        value = '(ee*FRCTdeltaZxsbLsbLxgob*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_335_400 = Coupling(name = 'UVGC_335_400',
                        value = '(ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_336_401 = Coupling(name = 'UVGC_336_401',
                        value = '(ee*FRCTdeltaZxsbRsbRxgob*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_336_402 = Coupling(name = 'UVGC_336_402',
                        value = '(ee*FRCTdeltaZxsbRsbRxGsbR*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_337_403 = Coupling(name = 'UVGC_337_403',
                        value = '(2*ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/9. - (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_337_404 = Coupling(name = 'UVGC_337_404',
                        value = '(2*ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/9. + (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_338_405 = Coupling(name = 'UVGC_338_405',
                        value = '(2*ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1))/9. - (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_338_406 = Coupling(name = 'UVGC_338_406',
                        value = '(2*ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1))/9. + (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_339_407 = Coupling(name = 'UVGC_339_407',
                        value = '-(FRCTdeltaZxGGxb*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_408 = Coupling(name = 'UVGC_339_408',
                        value = '-(FRCTdeltaZxGGxc*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_409 = Coupling(name = 'UVGC_339_409',
                        value = '-(FRCTdeltaZxGGxd*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_410 = Coupling(name = 'UVGC_339_410',
                        value = '-(FRCTdeltaZxGGxG*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_411 = Coupling(name = 'UVGC_339_411',
                        value = '-(FRCTdeltaZxGGxghG*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_412 = Coupling(name = 'UVGC_339_412',
                        value = '-(FRCTdeltaxaSxgo*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxgo*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_413 = Coupling(name = 'UVGC_339_413',
                        value = '-(FRCTdeltaZxGGxs*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_414 = Coupling(name = 'UVGC_339_414',
                        value = '-(FRCTdeltaxaSxsbL*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxsbL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_415 = Coupling(name = 'UVGC_339_415',
                        value = '-(FRCTdeltaxaSxsbR*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxsbR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_416 = Coupling(name = 'UVGC_339_416',
                        value = '-(FRCTdeltaxaSxscL*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxscL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_417 = Coupling(name = 'UVGC_339_417',
                        value = '-(FRCTdeltaxaSxscR*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxscR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_418 = Coupling(name = 'UVGC_339_418',
                        value = '-(FRCTdeltaxaSxsdL*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxsdL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_419 = Coupling(name = 'UVGC_339_419',
                        value = '-(FRCTdeltaxaSxsdR*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxsdR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_420 = Coupling(name = 'UVGC_339_420',
                        value = '-(FRCTdeltaxaSxssL*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxssL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_421 = Coupling(name = 'UVGC_339_421',
                        value = '-(FRCTdeltaxaSxssR*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxssR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_422 = Coupling(name = 'UVGC_339_422',
                        value = '-(FRCTdeltaxaSxstL*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxstL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_423 = Coupling(name = 'UVGC_339_423',
                        value = '-(FRCTdeltaxaSxstR*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxstR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_424 = Coupling(name = 'UVGC_339_424',
                        value = '-(FRCTdeltaxaSxsuL*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxsuL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_425 = Coupling(name = 'UVGC_339_425',
                        value = '-(FRCTdeltaxaSxsuR*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxsuR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_426 = Coupling(name = 'UVGC_339_426',
                        value = '-(FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxt*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_427 = Coupling(name = 'UVGC_339_427',
                        value = '-(FRCTdeltaZxGGxu*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_339_428 = Coupling(name = 'UVGC_339_428',
                        value = '-(FRCTdeltaZxsbLsbLxgob*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_339_429 = Coupling(name = 'UVGC_339_429',
                        value = '-(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_340_430 = Coupling(name = 'UVGC_340_430',
                        value = '-(FRCTdeltaZxsbRsbRxgob*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_340_431 = Coupling(name = 'UVGC_340_431',
                        value = '-(FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_341_432 = Coupling(name = 'UVGC_341_432',
                        value = '-(ee*FRCTdeltaZxGGxb*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_433 = Coupling(name = 'UVGC_341_433',
                        value = '-(ee*FRCTdeltaZxGGxc*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_434 = Coupling(name = 'UVGC_341_434',
                        value = '-(ee*FRCTdeltaZxGGxd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_435 = Coupling(name = 'UVGC_341_435',
                        value = '-(ee*FRCTdeltaZxGGxG*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_436 = Coupling(name = 'UVGC_341_436',
                        value = '-(ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_437 = Coupling(name = 'UVGC_341_437',
                        value = '-(ee*FRCTdeltaxaSxgo*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxgo*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_438 = Coupling(name = 'UVGC_341_438',
                        value = '-(ee*FRCTdeltaZxGGxs*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_439 = Coupling(name = 'UVGC_341_439',
                        value = '-(ee*FRCTdeltaxaSxsbL*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxsbL*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_440 = Coupling(name = 'UVGC_341_440',
                        value = '-(ee*FRCTdeltaxaSxsbR*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxsbR*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_441 = Coupling(name = 'UVGC_341_441',
                        value = '-(ee*FRCTdeltaxaSxscL*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxscL*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_442 = Coupling(name = 'UVGC_341_442',
                        value = '-(ee*FRCTdeltaxaSxscR*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxscR*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_443 = Coupling(name = 'UVGC_341_443',
                        value = '-(ee*FRCTdeltaxaSxsdL*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxsdL*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_444 = Coupling(name = 'UVGC_341_444',
                        value = '-(ee*FRCTdeltaxaSxsdR*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxsdR*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_445 = Coupling(name = 'UVGC_341_445',
                        value = '-(ee*FRCTdeltaxaSxssL*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxssL*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_446 = Coupling(name = 'UVGC_341_446',
                        value = '-(ee*FRCTdeltaxaSxssR*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxssR*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_447 = Coupling(name = 'UVGC_341_447',
                        value = '-(ee*FRCTdeltaxaSxstL*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxstL*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_448 = Coupling(name = 'UVGC_341_448',
                        value = '-(ee*FRCTdeltaxaSxstR*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxstR*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_449 = Coupling(name = 'UVGC_341_449',
                        value = '-(ee*FRCTdeltaxaSxsuL*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxsuL*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_450 = Coupling(name = 'UVGC_341_450',
                        value = '-(ee*FRCTdeltaxaSxsuR*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxsuR*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_451 = Coupling(name = 'UVGC_341_451',
                        value = '-(ee*FRCTdeltaxaSxt*complex(0,1)*G)/(3.*aS) - (ee*FRCTdeltaZxGGxt*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_452 = Coupling(name = 'UVGC_341_452',
                        value = '-(ee*FRCTdeltaZxGGxu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_341_453 = Coupling(name = 'UVGC_341_453',
                        value = '(-2*ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_341_454 = Coupling(name = 'UVGC_341_454',
                        value = '(-2*ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_342_455 = Coupling(name = 'UVGC_342_455',
                        value = '(-2*ee*FRCTdeltaZxsbRsbRxgob*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_342_456 = Coupling(name = 'UVGC_342_456',
                        value = '(-2*ee*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_343_457 = Coupling(name = 'UVGC_343_457',
                        value = 'FRCTdeltaZxGGxb*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_458 = Coupling(name = 'UVGC_343_458',
                        value = 'FRCTdeltaZxGGxc*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_459 = Coupling(name = 'UVGC_343_459',
                        value = 'FRCTdeltaZxGGxd*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_460 = Coupling(name = 'UVGC_343_460',
                        value = 'FRCTdeltaZxGGxG*complex(0,1)*G**2 - (9*complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_343_461 = Coupling(name = 'UVGC_343_461',
                        value = 'FRCTdeltaZxGGxghG*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_462 = Coupling(name = 'UVGC_343_462',
                        value = '(FRCTdeltaxaSxgo*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxgo*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_463 = Coupling(name = 'UVGC_343_463',
                        value = 'FRCTdeltaZxGGxs*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_464 = Coupling(name = 'UVGC_343_464',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxsbL*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_465 = Coupling(name = 'UVGC_343_465',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxsbR*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_466 = Coupling(name = 'UVGC_343_466',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxscL*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_467 = Coupling(name = 'UVGC_343_467',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxscR*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_468 = Coupling(name = 'UVGC_343_468',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxsdL*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_469 = Coupling(name = 'UVGC_343_469',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxsdR*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_470 = Coupling(name = 'UVGC_343_470',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxssL*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_471 = Coupling(name = 'UVGC_343_471',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxssR*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_472 = Coupling(name = 'UVGC_343_472',
                        value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxstL*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_473 = Coupling(name = 'UVGC_343_473',
                        value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxstR*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_474 = Coupling(name = 'UVGC_343_474',
                        value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxsuL*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_475 = Coupling(name = 'UVGC_343_475',
                        value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxsuR*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_476 = Coupling(name = 'UVGC_343_476',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxt*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_477 = Coupling(name = 'UVGC_343_477',
                        value = 'FRCTdeltaZxGGxu*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_343_478 = Coupling(name = 'UVGC_343_478',
                        value = 'FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_343_479 = Coupling(name = 'UVGC_343_479',
                        value = 'FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_344_480 = Coupling(name = 'UVGC_344_480',
                        value = 'FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_344_481 = Coupling(name = 'UVGC_344_481',
                        value = 'FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_345_482 = Coupling(name = 'UVGC_345_482',
                        value = '-2*FRCTdeltaxMsbLxsbL*complex(0,1)*MsbL - (complex(0,1)*G**2*invFREps*MsbL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_345_483 = Coupling(name = 'UVGC_345_483',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMsbLxgob*complex(0,1)*MsbL - FRCTdeltaZxsbLsbLxgob*complex(0,1)*MsbL**2',
                        order = {'QCD':2})

UVGC_345_484 = Coupling(name = 'UVGC_345_484',
                        value = '-2*FRCTdeltaxMsbLxGsbL*complex(0,1)*MsbL - FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*MsbL**2 + (complex(0,1)*G**2*invFREps*MsbL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_346_485 = Coupling(name = 'UVGC_346_485',
                        value = '-2*FRCTdeltaxMsbRxsbR*complex(0,1)*MsbR - (complex(0,1)*G**2*invFREps*MsbR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_346_486 = Coupling(name = 'UVGC_346_486',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMsbRxgob*complex(0,1)*MsbR - FRCTdeltaZxsbRsbRxgob*complex(0,1)*MsbR**2',
                        order = {'QCD':2})

UVGC_346_487 = Coupling(name = 'UVGC_346_487',
                        value = '-2*FRCTdeltaxMsbRxGsbR*complex(0,1)*MsbR - FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*MsbR**2 + (complex(0,1)*G**2*invFREps*MsbR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_347_488 = Coupling(name = 'UVGC_347_488',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_347_489 = Coupling(name = 'UVGC_347_489',
                        value = '(ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1))/(6 - 6*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_347_490 = Coupling(name = 'UVGC_347_490',
                        value = '(ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1))/(6 - 6*sw**2) + (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_348_491 = Coupling(name = 'UVGC_348_491',
                        value = '(ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

UVGC_348_492 = Coupling(name = 'UVGC_348_492',
                        value = '(2*ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1)*sw**2)/(9 - 9*sw**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

UVGC_348_493 = Coupling(name = 'UVGC_348_493',
                        value = '(2*ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*sw**2)/(9 - 9*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

UVGC_349_494 = Coupling(name = 'UVGC_349_494',
                        value = '(-13*complex(0,1)*G**4*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (13*complex(0,1)*G**4*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (13*complex(0,1)*G**4*invFREps*sw**4)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_495 = Coupling(name = 'UVGC_349_495',
                        value = '(FRCTdeltaxaSxgo*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxgo*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw))',
                        order = {'QCD':4})

UVGC_349_496 = Coupling(name = 'UVGC_349_496',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsbL*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_497 = Coupling(name = 'UVGC_349_497',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsbR*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_498 = Coupling(name = 'UVGC_349_498',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxscL*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_499 = Coupling(name = 'UVGC_349_499',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxscR*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_500 = Coupling(name = 'UVGC_349_500',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsdL*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_501 = Coupling(name = 'UVGC_349_501',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsdR*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_502 = Coupling(name = 'UVGC_349_502',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxssL*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_503 = Coupling(name = 'UVGC_349_503',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxssR*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_504 = Coupling(name = 'UVGC_349_504',
                        value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxstL*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_505 = Coupling(name = 'UVGC_349_505',
                        value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxstR*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_506 = Coupling(name = 'UVGC_349_506',
                        value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsuL*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_507 = Coupling(name = 'UVGC_349_507',
                        value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsuR*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_508 = Coupling(name = 'UVGC_349_508',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxt*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw))',
                        order = {'QCD':4})

UVGC_349_509 = Coupling(name = 'UVGC_349_509',
                        value = '(2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_349_510 = Coupling(name = 'UVGC_349_510',
                        value = '(2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_350_511 = Coupling(name = 'UVGC_350_511',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsbL*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_350_512 = Coupling(name = 'UVGC_350_512',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsbR*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_350_513 = Coupling(name = 'UVGC_350_513',
                        value = '(2*FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_350_514 = Coupling(name = 'UVGC_350_514',
                        value = '(2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_351_515 = Coupling(name = 'UVGC_351_515',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_351_516 = Coupling(name = 'UVGC_351_516',
                        value = '(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(6.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_351_517 = Coupling(name = 'UVGC_351_517',
                        value = '(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_352_518 = Coupling(name = 'UVGC_352_518',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_352_519 = Coupling(name = 'UVGC_352_519',
                        value = '(ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1))/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_352_520 = Coupling(name = 'UVGC_352_520',
                        value = '(ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_353_521 = Coupling(name = 'UVGC_353_521',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_353_522 = Coupling(name = 'UVGC_353_522',
                        value = '-(ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_353_523 = Coupling(name = 'UVGC_353_523',
                        value = '-(ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_354_524 = Coupling(name = 'UVGC_354_524',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_354_525 = Coupling(name = 'UVGC_354_525',
                        value = '-(ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_354_526 = Coupling(name = 'UVGC_354_526',
                        value = '-(ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_355_527 = Coupling(name = 'UVGC_355_527',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_355_528 = Coupling(name = 'UVGC_355_528',
                        value = '-(ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_355_529 = Coupling(name = 'UVGC_355_529',
                        value = '-(ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_356_530 = Coupling(name = 'UVGC_356_530',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_356_531 = Coupling(name = 'UVGC_356_531',
                        value = '(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_356_532 = Coupling(name = 'UVGC_356_532',
                        value = '(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_357_533 = Coupling(name = 'UVGC_357_533',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_357_534 = Coupling(name = 'UVGC_357_534',
                        value = '-(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_357_535 = Coupling(name = 'UVGC_357_535',
                        value = '-(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_358_536 = Coupling(name = 'UVGC_358_536',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_358_537 = Coupling(name = 'UVGC_358_537',
                        value = '-(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*Rtau1x1**2)/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*Rtau1x2**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*Rtau1x1**2)/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_358_538 = Coupling(name = 'UVGC_358_538',
                        value = '-(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*Rtau1x1**2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*Rtau1x2**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*Rtau1x1**2)/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_359_539 = Coupling(name = 'UVGC_359_539',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_359_540 = Coupling(name = 'UVGC_359_540',
                        value = '-(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*Rtau1x1*Rtau2x1)/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*Rtau1x2*Rtau2x2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*Rtau1x1*Rtau2x1)/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_359_541 = Coupling(name = 'UVGC_359_541',
                        value = '-(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*Rtau1x1*Rtau2x1)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*Rtau1x2*Rtau2x2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*Rtau1x1*Rtau2x1)/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_360_542 = Coupling(name = 'UVGC_360_542',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_360_543 = Coupling(name = 'UVGC_360_543',
                        value = '-(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*Rtau2x1**2)/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*Rtau2x2**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*Rtau2x1**2)/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_360_544 = Coupling(name = 'UVGC_360_544',
                        value = '-(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*Rtau2x1**2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*Rtau2x2**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*Rtau2x1**2)/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_361_545 = Coupling(name = 'UVGC_361_545',
                        value = '-(cw*ee*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(2.*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_361_546 = Coupling(name = 'UVGC_361_546',
                        value = '-(cw*ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(2.*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_362_547 = Coupling(name = 'UVGC_362_547',
                        value = '(cw*ee*FRCTdeltaZxsbRsbRxgob*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_362_548 = Coupling(name = 'UVGC_362_548',
                        value = '(cw*ee*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_363_549 = Coupling(name = 'UVGC_363_549',
                        value = '-(cw*ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(3.*sw*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*sw)/(9.*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_363_550 = Coupling(name = 'UVGC_363_550',
                        value = '-(cw*ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(3.*sw*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_364_551 = Coupling(name = 'UVGC_364_551',
                        value = '(2*cw*ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1)*sw)/(9.*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_364_552 = Coupling(name = 'UVGC_364_552',
                        value = '(2*cw*ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_365_553 = Coupling(name = 'UVGC_365_553',
                        value = '-(cw*ee*FRCTdeltaZxGGxb*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_554 = Coupling(name = 'UVGC_365_554',
                        value = '-(cw*ee*FRCTdeltaZxGGxc*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_555 = Coupling(name = 'UVGC_365_555',
                        value = '-(cw*ee*FRCTdeltaZxGGxd*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_556 = Coupling(name = 'UVGC_365_556',
                        value = '-(cw*ee*FRCTdeltaZxGGxG*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_557 = Coupling(name = 'UVGC_365_557',
                        value = '-(cw*ee*FRCTdeltaZxGGxghG*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_558 = Coupling(name = 'UVGC_365_558',
                        value = '-(cw*ee*FRCTdeltaxaSxgo*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxgo*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_559 = Coupling(name = 'UVGC_365_559',
                        value = '-(cw*ee*FRCTdeltaZxGGxs*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_560 = Coupling(name = 'UVGC_365_560',
                        value = '-(cw*ee*FRCTdeltaxaSxsbL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsbL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_561 = Coupling(name = 'UVGC_365_561',
                        value = '-(cw*ee*FRCTdeltaxaSxsbR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsbR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_562 = Coupling(name = 'UVGC_365_562',
                        value = '-(cw*ee*FRCTdeltaxaSxscL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxscL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_563 = Coupling(name = 'UVGC_365_563',
                        value = '-(cw*ee*FRCTdeltaxaSxscR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxscR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_564 = Coupling(name = 'UVGC_365_564',
                        value = '-(cw*ee*FRCTdeltaxaSxsdL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsdL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_565 = Coupling(name = 'UVGC_365_565',
                        value = '-(cw*ee*FRCTdeltaxaSxsdR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsdR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_566 = Coupling(name = 'UVGC_365_566',
                        value = '-(cw*ee*FRCTdeltaxaSxssL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxssL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_567 = Coupling(name = 'UVGC_365_567',
                        value = '-(cw*ee*FRCTdeltaxaSxssR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxssR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_568 = Coupling(name = 'UVGC_365_568',
                        value = '-(cw*ee*FRCTdeltaxaSxstL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxstL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_569 = Coupling(name = 'UVGC_365_569',
                        value = '-(cw*ee*FRCTdeltaxaSxstR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxstR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_570 = Coupling(name = 'UVGC_365_570',
                        value = '-(cw*ee*FRCTdeltaxaSxsuL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsuL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_571 = Coupling(name = 'UVGC_365_571',
                        value = '-(cw*ee*FRCTdeltaxaSxsuR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsuR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_572 = Coupling(name = 'UVGC_365_572',
                        value = '-(cw*ee*FRCTdeltaxaSxt*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxt*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_573 = Coupling(name = 'UVGC_365_573',
                        value = '-(cw*ee*FRCTdeltaZxGGxu*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_365_574 = Coupling(name = 'UVGC_365_574',
                        value = '(-2*cw*ee*FRCTdeltaZxsbRsbRxgob*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) + (cw*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_365_575 = Coupling(name = 'UVGC_365_575',
                        value = '(-2*cw*ee*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) + (cw*ee*complex(0,1)*G**3*invFREps*sw)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_366_576 = Coupling(name = 'UVGC_366_576',
                        value = '(cw*ee*FRCTdeltaZxGGxb*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxb*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_577 = Coupling(name = 'UVGC_366_577',
                        value = '(cw*ee*FRCTdeltaZxGGxc*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxc*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_578 = Coupling(name = 'UVGC_366_578',
                        value = '(cw*ee*FRCTdeltaZxGGxd*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxd*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_579 = Coupling(name = 'UVGC_366_579',
                        value = '(cw*ee*FRCTdeltaZxGGxG*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxG*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_580 = Coupling(name = 'UVGC_366_580',
                        value = '(cw*ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxghG*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_581 = Coupling(name = 'UVGC_366_581',
                        value = '(cw*ee*FRCTdeltaxaSxgo*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxgo*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxgo*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxgo*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_582 = Coupling(name = 'UVGC_366_582',
                        value = '(cw*ee*FRCTdeltaZxGGxs*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxs*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_583 = Coupling(name = 'UVGC_366_583',
                        value = '(cw*ee*FRCTdeltaxaSxsbL*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxsbL*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxsbL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsbL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_584 = Coupling(name = 'UVGC_366_584',
                        value = '(cw*ee*FRCTdeltaxaSxsbR*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxsbR*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxsbR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsbR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_585 = Coupling(name = 'UVGC_366_585',
                        value = '(cw*ee*FRCTdeltaxaSxscL*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxscL*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxscL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxscL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_586 = Coupling(name = 'UVGC_366_586',
                        value = '(cw*ee*FRCTdeltaxaSxscR*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxscR*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxscR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxscR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_587 = Coupling(name = 'UVGC_366_587',
                        value = '(cw*ee*FRCTdeltaxaSxsdL*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxsdL*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxsdL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsdL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_588 = Coupling(name = 'UVGC_366_588',
                        value = '(cw*ee*FRCTdeltaxaSxsdR*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxsdR*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxsdR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsdR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_589 = Coupling(name = 'UVGC_366_589',
                        value = '(cw*ee*FRCTdeltaxaSxssL*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxssL*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxssL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxssL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_590 = Coupling(name = 'UVGC_366_590',
                        value = '(cw*ee*FRCTdeltaxaSxssR*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxssR*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxssR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxssR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_591 = Coupling(name = 'UVGC_366_591',
                        value = '(cw*ee*FRCTdeltaxaSxstL*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxstL*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxstL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxstL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_592 = Coupling(name = 'UVGC_366_592',
                        value = '(cw*ee*FRCTdeltaxaSxstR*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxstR*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxstR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxstR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_593 = Coupling(name = 'UVGC_366_593',
                        value = '(cw*ee*FRCTdeltaxaSxsuL*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxsuL*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxsuL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsuL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_594 = Coupling(name = 'UVGC_366_594',
                        value = '(cw*ee*FRCTdeltaxaSxsuR*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxsuR*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxsuR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsuR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_595 = Coupling(name = 'UVGC_366_595',
                        value = '(cw*ee*FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) + (cw*ee*FRCTdeltaZxGGxt*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaxaSxt*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxt*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_596 = Coupling(name = 'UVGC_366_596',
                        value = '(cw*ee*FRCTdeltaZxGGxu*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxu*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_366_597 = Coupling(name = 'UVGC_366_597',
                        value = '(cw*ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*G)/((-1 + sw)*sw*(1 + sw)) - (2*cw*ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) - (cw*ee*complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_366_598 = Coupling(name = 'UVGC_366_598',
                        value = '(cw*ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G)/((-1 + sw)*sw*(1 + sw)) - (2*cw*ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) - (cw*ee*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**3*invFREps*sw)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_367_599 = Coupling(name = 'UVGC_367_599',
                        value = '(-7*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

UVGC_367_600 = Coupling(name = 'UVGC_367_600',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(2.*sw**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_367_601 = Coupling(name = 'UVGC_367_601',
                        value = '(7*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(2.*sw**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_368_602 = Coupling(name = 'UVGC_368_602',
                        value = 'FRCTdeltaZxscLscLxgoc*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_368_603 = Coupling(name = 'UVGC_368_603',
                        value = 'FRCTdeltaZxscLscLxGscL*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_369_604 = Coupling(name = 'UVGC_369_604',
                        value = 'FRCTdeltaZxscRscRxgoc*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_369_605 = Coupling(name = 'UVGC_369_605',
                        value = 'FRCTdeltaZxscRscRxGscR*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_370_606 = Coupling(name = 'UVGC_370_606',
                        value = '(-2*ee*FRCTdeltaZxscLscLxgoc*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_370_607 = Coupling(name = 'UVGC_370_607',
                        value = '(-2*ee*FRCTdeltaZxscLscLxGscL*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_371_608 = Coupling(name = 'UVGC_371_608',
                        value = '(-2*ee*FRCTdeltaZxscRscRxgoc*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_371_609 = Coupling(name = 'UVGC_371_609',
                        value = '(-2*ee*FRCTdeltaZxscRscRxGscR*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_372_610 = Coupling(name = 'UVGC_372_610',
                        value = '(8*ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/9. - (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_372_611 = Coupling(name = 'UVGC_372_611',
                        value = '(8*ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/9. + (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_373_612 = Coupling(name = 'UVGC_373_612',
                        value = '(8*ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1))/9. - (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_373_613 = Coupling(name = 'UVGC_373_613',
                        value = '(8*ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1))/9. + (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_374_614 = Coupling(name = 'UVGC_374_614',
                        value = '-(FRCTdeltaZxscLscLxgoc*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_374_615 = Coupling(name = 'UVGC_374_615',
                        value = '-(FRCTdeltaZxscLscLxGscL*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_375_616 = Coupling(name = 'UVGC_375_616',
                        value = '-(FRCTdeltaZxscRscRxgoc*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_375_617 = Coupling(name = 'UVGC_375_617',
                        value = '-(FRCTdeltaZxscRscRxGscR*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_376_618 = Coupling(name = 'UVGC_376_618',
                        value = '(2*ee*FRCTdeltaZxGGxb*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_619 = Coupling(name = 'UVGC_376_619',
                        value = '(2*ee*FRCTdeltaZxGGxc*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_620 = Coupling(name = 'UVGC_376_620',
                        value = '(2*ee*FRCTdeltaZxGGxd*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_621 = Coupling(name = 'UVGC_376_621',
                        value = '(2*ee*FRCTdeltaZxGGxG*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_622 = Coupling(name = 'UVGC_376_622',
                        value = '(2*ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_623 = Coupling(name = 'UVGC_376_623',
                        value = '(2*ee*FRCTdeltaxaSxgo*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxgo*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_624 = Coupling(name = 'UVGC_376_624',
                        value = '(2*ee*FRCTdeltaZxGGxs*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_625 = Coupling(name = 'UVGC_376_625',
                        value = '(2*ee*FRCTdeltaxaSxsbL*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxsbL*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_626 = Coupling(name = 'UVGC_376_626',
                        value = '(2*ee*FRCTdeltaxaSxsbR*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxsbR*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_627 = Coupling(name = 'UVGC_376_627',
                        value = '(2*ee*FRCTdeltaxaSxscL*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxscL*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_628 = Coupling(name = 'UVGC_376_628',
                        value = '(2*ee*FRCTdeltaxaSxscR*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxscR*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_629 = Coupling(name = 'UVGC_376_629',
                        value = '(2*ee*FRCTdeltaxaSxsdL*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxsdL*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_630 = Coupling(name = 'UVGC_376_630',
                        value = '(2*ee*FRCTdeltaxaSxsdR*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxsdR*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_631 = Coupling(name = 'UVGC_376_631',
                        value = '(2*ee*FRCTdeltaxaSxssL*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxssL*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_632 = Coupling(name = 'UVGC_376_632',
                        value = '(2*ee*FRCTdeltaxaSxssR*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxssR*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_633 = Coupling(name = 'UVGC_376_633',
                        value = '(2*ee*FRCTdeltaxaSxstL*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxstL*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_634 = Coupling(name = 'UVGC_376_634',
                        value = '(2*ee*FRCTdeltaxaSxstR*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxstR*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_635 = Coupling(name = 'UVGC_376_635',
                        value = '(2*ee*FRCTdeltaxaSxsuL*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxsuL*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_636 = Coupling(name = 'UVGC_376_636',
                        value = '(2*ee*FRCTdeltaxaSxsuR*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxsuR*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_637 = Coupling(name = 'UVGC_376_637',
                        value = '(2*ee*FRCTdeltaxaSxt*complex(0,1)*G)/(3.*aS) + (2*ee*FRCTdeltaZxGGxt*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_638 = Coupling(name = 'UVGC_376_638',
                        value = '(2*ee*FRCTdeltaZxGGxu*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_376_639 = Coupling(name = 'UVGC_376_639',
                        value = '(4*ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*G)/3. - (2*ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_376_640 = Coupling(name = 'UVGC_376_640',
                        value = '(4*ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*G)/3. - (ee*complex(0,1)*G**3*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_377_641 = Coupling(name = 'UVGC_377_641',
                        value = '(4*ee*FRCTdeltaZxscRscRxgoc*complex(0,1)*G)/3. - (2*ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_377_642 = Coupling(name = 'UVGC_377_642',
                        value = '(4*ee*FRCTdeltaZxscRscRxGscR*complex(0,1)*G)/3. - (ee*complex(0,1)*G**3*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_378_643 = Coupling(name = 'UVGC_378_643',
                        value = 'FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_378_644 = Coupling(name = 'UVGC_378_644',
                        value = 'FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_379_645 = Coupling(name = 'UVGC_379_645',
                        value = 'FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_379_646 = Coupling(name = 'UVGC_379_646',
                        value = 'FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_380_647 = Coupling(name = 'UVGC_380_647',
                        value = '-2*FRCTdeltaxMscLxscL*complex(0,1)*MscL - (complex(0,1)*G**2*invFREps*MscL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_380_648 = Coupling(name = 'UVGC_380_648',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMscLxgoc*complex(0,1)*MscL - FRCTdeltaZxscLscLxgoc*complex(0,1)*MscL**2',
                        order = {'QCD':2})

UVGC_380_649 = Coupling(name = 'UVGC_380_649',
                        value = '-2*FRCTdeltaxMscLxGscL*complex(0,1)*MscL - FRCTdeltaZxscLscLxGscL*complex(0,1)*MscL**2 + (complex(0,1)*G**2*invFREps*MscL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_381_650 = Coupling(name = 'UVGC_381_650',
                        value = '-2*FRCTdeltaxMscRxscR*complex(0,1)*MscR - (complex(0,1)*G**2*invFREps*MscR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_381_651 = Coupling(name = 'UVGC_381_651',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMscRxgoc*complex(0,1)*MscR - FRCTdeltaZxscRscRxgoc*complex(0,1)*MscR**2',
                        order = {'QCD':2})

UVGC_381_652 = Coupling(name = 'UVGC_381_652',
                        value = '-2*FRCTdeltaxMscRxGscR*complex(0,1)*MscR - FRCTdeltaZxscRscRxGscR*complex(0,1)*MscR**2 + (complex(0,1)*G**2*invFREps*MscR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_382_653 = Coupling(name = 'UVGC_382_653',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_382_654 = Coupling(name = 'UVGC_382_654',
                        value = '(2*ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1))/(3 - 3*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_382_655 = Coupling(name = 'UVGC_382_655',
                        value = '(2*ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1))/(3 - 3*sw**2) + (ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_383_656 = Coupling(name = 'UVGC_383_656',
                        value = '(2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

UVGC_383_657 = Coupling(name = 'UVGC_383_657',
                        value = '(8*ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1)*sw**2)/(9 - 9*sw**2) - (4*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

UVGC_383_658 = Coupling(name = 'UVGC_383_658',
                        value = '(8*ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1)*sw**2)/(9 - 9*sw**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

UVGC_384_659 = Coupling(name = 'UVGC_384_659',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxscL*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_384_660 = Coupling(name = 'UVGC_384_660',
                        value = '(2*FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_384_661 = Coupling(name = 'UVGC_384_661',
                        value = '(2*FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_385_662 = Coupling(name = 'UVGC_385_662',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxscR*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_385_663 = Coupling(name = 'UVGC_385_663',
                        value = '(2*FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_385_664 = Coupling(name = 'UVGC_385_664',
                        value = '(2*FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_386_665 = Coupling(name = 'UVGC_386_665',
                        value = '(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(6.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_386_666 = Coupling(name = 'UVGC_386_666',
                        value = '(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_387_667 = Coupling(name = 'UVGC_387_667',
                        value = '(ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1))/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_387_668 = Coupling(name = 'UVGC_387_668',
                        value = '(ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_388_669 = Coupling(name = 'UVGC_388_669',
                        value = '(ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_388_670 = Coupling(name = 'UVGC_388_670',
                        value = '(ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1)*Rtau1x1**2)/(3.*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_388_671 = Coupling(name = 'UVGC_388_671',
                        value = '(ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1)*Rtau1x1**2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_389_672 = Coupling(name = 'UVGC_389_672',
                        value = '(ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(36.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_389_673 = Coupling(name = 'UVGC_389_673',
                        value = '(ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1)*Rtau1x1*Rtau2x1)/(3.*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_389_674 = Coupling(name = 'UVGC_389_674',
                        value = '(ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1)*Rtau1x1*Rtau2x1)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(36.*cmath.pi**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_390_675 = Coupling(name = 'UVGC_390_675',
                        value = '(ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_390_676 = Coupling(name = 'UVGC_390_676',
                        value = '(ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1)*Rtau2x1**2)/(3.*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_390_677 = Coupling(name = 'UVGC_390_677',
                        value = '(ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1)*Rtau2x1**2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_391_678 = Coupling(name = 'UVGC_391_678',
                        value = '(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_391_679 = Coupling(name = 'UVGC_391_679',
                        value = '(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_392_680 = Coupling(name = 'UVGC_392_680',
                        value = '-(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_392_681 = Coupling(name = 'UVGC_392_681',
                        value = '-(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_393_682 = Coupling(name = 'UVGC_393_682',
                        value = '(ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_393_683 = Coupling(name = 'UVGC_393_683',
                        value = '(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*Rtau1x2**2)/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*Rtau1x1**2)/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_393_684 = Coupling(name = 'UVGC_393_684',
                        value = '(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*Rtau1x2**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*Rtau1x1**2)/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_394_685 = Coupling(name = 'UVGC_394_685',
                        value = '(ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_394_686 = Coupling(name = 'UVGC_394_686',
                        value = '(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*Rtau1x2*Rtau2x2)/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*Rtau1x1*Rtau2x1)/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_394_687 = Coupling(name = 'UVGC_394_687',
                        value = '(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*Rtau1x2*Rtau2x2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*Rtau1x1*Rtau2x1)/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_395_688 = Coupling(name = 'UVGC_395_688',
                        value = '(ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_395_689 = Coupling(name = 'UVGC_395_689',
                        value = '(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*Rtau2x2**2)/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*Rtau2x1**2)/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_395_690 = Coupling(name = 'UVGC_395_690',
                        value = '(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*Rtau2x2**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*Rtau2x1**2)/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_396_691 = Coupling(name = 'UVGC_396_691',
                        value = '(cw*ee*FRCTdeltaZxscLscLxgoc*complex(0,1))/(2.*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*(-1 + sw**2)) - (2*cw*ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_396_692 = Coupling(name = 'UVGC_396_692',
                        value = '(cw*ee*FRCTdeltaZxscLscLxGscL*complex(0,1))/(2.*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*(-1 + sw**2)) - (2*cw*ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_397_693 = Coupling(name = 'UVGC_397_693',
                        value = '(-2*cw*ee*FRCTdeltaZxscRscRxgoc*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_397_694 = Coupling(name = 'UVGC_397_694',
                        value = '(-2*cw*ee*FRCTdeltaZxscRscRxGscR*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_398_695 = Coupling(name = 'UVGC_398_695',
                        value = '(-2*cw*ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(3.*sw*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*sw*(-1 + sw**2)) + (8*cw*ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*sw)/(9.*(-1 + sw**2)) - (4*cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_398_696 = Coupling(name = 'UVGC_398_696',
                        value = '(-2*cw*ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(3.*sw*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*sw*(-1 + sw**2)) + (8*cw*ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (4*cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_399_697 = Coupling(name = 'UVGC_399_697',
                        value = '(8*cw*ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1)*sw)/(9.*(-1 + sw**2)) - (4*cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_399_698 = Coupling(name = 'UVGC_399_698',
                        value = '(8*cw*ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (4*cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_400_699 = Coupling(name = 'UVGC_400_699',
                        value = '(2*cw*ee*FRCTdeltaZxGGxb*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_700 = Coupling(name = 'UVGC_400_700',
                        value = '(2*cw*ee*FRCTdeltaZxGGxc*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_701 = Coupling(name = 'UVGC_400_701',
                        value = '(2*cw*ee*FRCTdeltaZxGGxd*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_702 = Coupling(name = 'UVGC_400_702',
                        value = '(2*cw*ee*FRCTdeltaZxGGxG*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_703 = Coupling(name = 'UVGC_400_703',
                        value = '(2*cw*ee*FRCTdeltaZxGGxghG*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_704 = Coupling(name = 'UVGC_400_704',
                        value = '(2*cw*ee*FRCTdeltaxaSxgo*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxgo*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_705 = Coupling(name = 'UVGC_400_705',
                        value = '(2*cw*ee*FRCTdeltaZxGGxs*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_706 = Coupling(name = 'UVGC_400_706',
                        value = '(2*cw*ee*FRCTdeltaxaSxsbL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxsbL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_707 = Coupling(name = 'UVGC_400_707',
                        value = '(2*cw*ee*FRCTdeltaxaSxsbR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxsbR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_708 = Coupling(name = 'UVGC_400_708',
                        value = '(2*cw*ee*FRCTdeltaxaSxscL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxscL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_709 = Coupling(name = 'UVGC_400_709',
                        value = '(2*cw*ee*FRCTdeltaxaSxscR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxscR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_710 = Coupling(name = 'UVGC_400_710',
                        value = '(2*cw*ee*FRCTdeltaxaSxsdL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxsdL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_711 = Coupling(name = 'UVGC_400_711',
                        value = '(2*cw*ee*FRCTdeltaxaSxsdR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxsdR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_712 = Coupling(name = 'UVGC_400_712',
                        value = '(2*cw*ee*FRCTdeltaxaSxssL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxssL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_713 = Coupling(name = 'UVGC_400_713',
                        value = '(2*cw*ee*FRCTdeltaxaSxssR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxssR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_714 = Coupling(name = 'UVGC_400_714',
                        value = '(2*cw*ee*FRCTdeltaxaSxstL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxstL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_715 = Coupling(name = 'UVGC_400_715',
                        value = '(2*cw*ee*FRCTdeltaxaSxstR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxstR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_716 = Coupling(name = 'UVGC_400_716',
                        value = '(2*cw*ee*FRCTdeltaxaSxsuL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxsuL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_717 = Coupling(name = 'UVGC_400_717',
                        value = '(2*cw*ee*FRCTdeltaxaSxsuR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxsuR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_718 = Coupling(name = 'UVGC_400_718',
                        value = '(2*cw*ee*FRCTdeltaxaSxt*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxt*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_719 = Coupling(name = 'UVGC_400_719',
                        value = '(2*cw*ee*FRCTdeltaZxGGxu*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_400_720 = Coupling(name = 'UVGC_400_720',
                        value = '(4*cw*ee*FRCTdeltaZxscRscRxgoc*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) - (2*cw*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_400_721 = Coupling(name = 'UVGC_400_721',
                        value = '(4*cw*ee*FRCTdeltaZxscRscRxGscR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) - (cw*ee*complex(0,1)*G**3*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_401_722 = Coupling(name = 'UVGC_401_722',
                        value = '-(cw*ee*FRCTdeltaZxGGxb*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxb*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_723 = Coupling(name = 'UVGC_401_723',
                        value = '-(cw*ee*FRCTdeltaZxGGxc*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxc*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_724 = Coupling(name = 'UVGC_401_724',
                        value = '-(cw*ee*FRCTdeltaZxGGxd*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxd*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_725 = Coupling(name = 'UVGC_401_725',
                        value = '-(cw*ee*FRCTdeltaZxGGxG*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxG*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_726 = Coupling(name = 'UVGC_401_726',
                        value = '-(cw*ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxghG*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_727 = Coupling(name = 'UVGC_401_727',
                        value = '-(cw*ee*FRCTdeltaxaSxgo*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxgo*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxgo*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxgo*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_728 = Coupling(name = 'UVGC_401_728',
                        value = '-(cw*ee*FRCTdeltaZxGGxs*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxs*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_729 = Coupling(name = 'UVGC_401_729',
                        value = '-(cw*ee*FRCTdeltaxaSxsbL*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsbL*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxsbL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxsbL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_730 = Coupling(name = 'UVGC_401_730',
                        value = '-(cw*ee*FRCTdeltaxaSxsbR*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsbR*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxsbR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxsbR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_731 = Coupling(name = 'UVGC_401_731',
                        value = '-(cw*ee*FRCTdeltaxaSxscL*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxscL*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxscL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxscL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_732 = Coupling(name = 'UVGC_401_732',
                        value = '-(cw*ee*FRCTdeltaxaSxscR*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxscR*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxscR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxscR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_733 = Coupling(name = 'UVGC_401_733',
                        value = '-(cw*ee*FRCTdeltaxaSxsdL*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsdL*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxsdL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxsdL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_734 = Coupling(name = 'UVGC_401_734',
                        value = '-(cw*ee*FRCTdeltaxaSxsdR*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsdR*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxsdR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxsdR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_735 = Coupling(name = 'UVGC_401_735',
                        value = '-(cw*ee*FRCTdeltaxaSxssL*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxssL*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxssL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxssL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_736 = Coupling(name = 'UVGC_401_736',
                        value = '-(cw*ee*FRCTdeltaxaSxssR*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxssR*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxssR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxssR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_737 = Coupling(name = 'UVGC_401_737',
                        value = '-(cw*ee*FRCTdeltaxaSxstL*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxstL*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxstL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxstL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_738 = Coupling(name = 'UVGC_401_738',
                        value = '-(cw*ee*FRCTdeltaxaSxstR*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxstR*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxstR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxstR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_739 = Coupling(name = 'UVGC_401_739',
                        value = '-(cw*ee*FRCTdeltaxaSxsuL*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsuL*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxsuL*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxsuL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_740 = Coupling(name = 'UVGC_401_740',
                        value = '-(cw*ee*FRCTdeltaxaSxsuR*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxsuR*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxsuR*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxsuR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_741 = Coupling(name = 'UVGC_401_741',
                        value = '-(cw*ee*FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS*(-1 + sw)*sw*(1 + sw)) - (cw*ee*FRCTdeltaZxGGxt*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaxaSxt*complex(0,1)*G*sw)/(3.*aS*(-1 + sw)*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxt*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_742 = Coupling(name = 'UVGC_401_742',
                        value = '-(cw*ee*FRCTdeltaZxGGxu*complex(0,1)*G)/(2.*(-1 + sw)*sw*(1 + sw)) + (2*cw*ee*FRCTdeltaZxGGxu*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw))',
                        order = {'QCD':3,'QED':1})

UVGC_401_743 = Coupling(name = 'UVGC_401_743',
                        value = '-((cw*ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*G)/((-1 + sw)*sw*(1 + sw))) + (4*cw*ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) + (cw*ee*complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2*sw*(-1 + sw**2)) - (2*cw*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_401_744 = Coupling(name = 'UVGC_401_744',
                        value = '-((cw*ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*G)/((-1 + sw)*sw*(1 + sw))) + (4*cw*ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) + (cw*ee*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**3*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_402_745 = Coupling(name = 'UVGC_402_745',
                        value = '(-11*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

UVGC_402_746 = Coupling(name = 'UVGC_402_746',
                        value = '(2*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (4*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (4*ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(2.*sw**2*(-1 + sw**2)) - (8*ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_402_747 = Coupling(name = 'UVGC_402_747',
                        value = '(11*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (4*ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(2.*sw**2*(-1 + sw**2)) - (8*ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_403_748 = Coupling(name = 'UVGC_403_748',
                        value = 'FRCTdeltaZxsdLsdLxgod*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_403_749 = Coupling(name = 'UVGC_403_749',
                        value = 'FRCTdeltaZxsdLsdLxGsdL*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_404_750 = Coupling(name = 'UVGC_404_750',
                        value = 'FRCTdeltaZxsdRsdRxgod*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_404_751 = Coupling(name = 'UVGC_404_751',
                        value = 'FRCTdeltaZxsdRsdRxGsdR*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_405_752 = Coupling(name = 'UVGC_405_752',
                        value = '(ee*FRCTdeltaZxsdLsdLxgod*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_405_753 = Coupling(name = 'UVGC_405_753',
                        value = '(ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_406_754 = Coupling(name = 'UVGC_406_754',
                        value = '(ee*FRCTdeltaZxsdRsdRxgod*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_406_755 = Coupling(name = 'UVGC_406_755',
                        value = '(ee*FRCTdeltaZxsdRsdRxGsdR*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_407_756 = Coupling(name = 'UVGC_407_756',
                        value = '(2*ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/9. - (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_407_757 = Coupling(name = 'UVGC_407_757',
                        value = '(2*ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/9. + (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_408_758 = Coupling(name = 'UVGC_408_758',
                        value = '(2*ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1))/9. - (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_408_759 = Coupling(name = 'UVGC_408_759',
                        value = '(2*ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1))/9. + (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_409_760 = Coupling(name = 'UVGC_409_760',
                        value = '-(FRCTdeltaZxsdLsdLxgod*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_409_761 = Coupling(name = 'UVGC_409_761',
                        value = '-(FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_410_762 = Coupling(name = 'UVGC_410_762',
                        value = '-(FRCTdeltaZxsdRsdRxgod*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_410_763 = Coupling(name = 'UVGC_410_763',
                        value = '-(FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_411_764 = Coupling(name = 'UVGC_411_764',
                        value = '(-2*ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_411_765 = Coupling(name = 'UVGC_411_765',
                        value = '(-2*ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_412_766 = Coupling(name = 'UVGC_412_766',
                        value = '(-2*ee*FRCTdeltaZxsdRsdRxgod*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_412_767 = Coupling(name = 'UVGC_412_767',
                        value = '(-2*ee*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_413_768 = Coupling(name = 'UVGC_413_768',
                        value = 'FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_413_769 = Coupling(name = 'UVGC_413_769',
                        value = 'FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_414_770 = Coupling(name = 'UVGC_414_770',
                        value = 'FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_414_771 = Coupling(name = 'UVGC_414_771',
                        value = 'FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_415_772 = Coupling(name = 'UVGC_415_772',
                        value = '-2*FRCTdeltaxMsdLxsdL*complex(0,1)*MsdL - (complex(0,1)*G**2*invFREps*MsdL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_415_773 = Coupling(name = 'UVGC_415_773',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMsdLxgod*complex(0,1)*MsdL - FRCTdeltaZxsdLsdLxgod*complex(0,1)*MsdL**2',
                        order = {'QCD':2})

UVGC_415_774 = Coupling(name = 'UVGC_415_774',
                        value = '-2*FRCTdeltaxMsdLxGsdL*complex(0,1)*MsdL - FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*MsdL**2 + (complex(0,1)*G**2*invFREps*MsdL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_416_775 = Coupling(name = 'UVGC_416_775',
                        value = '-2*FRCTdeltaxMsdRxsdR*complex(0,1)*MsdR - (complex(0,1)*G**2*invFREps*MsdR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_416_776 = Coupling(name = 'UVGC_416_776',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMsdRxgod*complex(0,1)*MsdR - FRCTdeltaZxsdRsdRxgod*complex(0,1)*MsdR**2',
                        order = {'QCD':2})

UVGC_416_777 = Coupling(name = 'UVGC_416_777',
                        value = '-2*FRCTdeltaxMsdRxGsdR*complex(0,1)*MsdR - FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*MsdR**2 + (complex(0,1)*G**2*invFREps*MsdR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_417_778 = Coupling(name = 'UVGC_417_778',
                        value = '(ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1))/(6 - 6*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_417_779 = Coupling(name = 'UVGC_417_779',
                        value = '(ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1))/(6 - 6*sw**2) + (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_418_780 = Coupling(name = 'UVGC_418_780',
                        value = '(2*ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1)*sw**2)/(9 - 9*sw**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

UVGC_418_781 = Coupling(name = 'UVGC_418_781',
                        value = '(2*ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*sw**2)/(9 - 9*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

UVGC_419_782 = Coupling(name = 'UVGC_419_782',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsdL*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_419_783 = Coupling(name = 'UVGC_419_783',
                        value = '(2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_419_784 = Coupling(name = 'UVGC_419_784',
                        value = '(2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_420_785 = Coupling(name = 'UVGC_420_785',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsdR*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_420_786 = Coupling(name = 'UVGC_420_786',
                        value = '(2*FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_420_787 = Coupling(name = 'UVGC_420_787',
                        value = '(2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_421_788 = Coupling(name = 'UVGC_421_788',
                        value = '(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(6.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_421_789 = Coupling(name = 'UVGC_421_789',
                        value = '(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_422_790 = Coupling(name = 'UVGC_422_790',
                        value = '(ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1))/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_422_791 = Coupling(name = 'UVGC_422_791',
                        value = '(ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_423_792 = Coupling(name = 'UVGC_423_792',
                        value = '-(ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_423_793 = Coupling(name = 'UVGC_423_793',
                        value = '-(ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_424_794 = Coupling(name = 'UVGC_424_794',
                        value = '-(ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_424_795 = Coupling(name = 'UVGC_424_795',
                        value = '-(ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_425_796 = Coupling(name = 'UVGC_425_796',
                        value = '-(ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_425_797 = Coupling(name = 'UVGC_425_797',
                        value = '-(ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_426_798 = Coupling(name = 'UVGC_426_798',
                        value = '(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_426_799 = Coupling(name = 'UVGC_426_799',
                        value = '(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_427_800 = Coupling(name = 'UVGC_427_800',
                        value = '-(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_427_801 = Coupling(name = 'UVGC_427_801',
                        value = '-(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_428_802 = Coupling(name = 'UVGC_428_802',
                        value = '-(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*Rtau1x1**2)/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*Rtau1x2**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*Rtau1x1**2)/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_428_803 = Coupling(name = 'UVGC_428_803',
                        value = '-(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*Rtau1x1**2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*Rtau1x2**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*Rtau1x1**2)/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_429_804 = Coupling(name = 'UVGC_429_804',
                        value = '-(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*Rtau1x1*Rtau2x1)/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*Rtau1x2*Rtau2x2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*Rtau1x1*Rtau2x1)/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_429_805 = Coupling(name = 'UVGC_429_805',
                        value = '-(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*Rtau1x1*Rtau2x1)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*Rtau1x2*Rtau2x2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*Rtau1x1*Rtau2x1)/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_430_806 = Coupling(name = 'UVGC_430_806',
                        value = '-(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*Rtau2x1**2)/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*Rtau2x2**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*Rtau2x1**2)/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_430_807 = Coupling(name = 'UVGC_430_807',
                        value = '-(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*Rtau2x1**2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*Rtau2x2**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*Rtau2x1**2)/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_431_808 = Coupling(name = 'UVGC_431_808',
                        value = '-(cw*ee*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(2.*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_431_809 = Coupling(name = 'UVGC_431_809',
                        value = '-(cw*ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(2.*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_432_810 = Coupling(name = 'UVGC_432_810',
                        value = '(cw*ee*FRCTdeltaZxsdRsdRxgod*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_432_811 = Coupling(name = 'UVGC_432_811',
                        value = '(cw*ee*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_433_812 = Coupling(name = 'UVGC_433_812',
                        value = '-(cw*ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(3.*sw*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*sw)/(9.*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_433_813 = Coupling(name = 'UVGC_433_813',
                        value = '-(cw*ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(3.*sw*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_434_814 = Coupling(name = 'UVGC_434_814',
                        value = '(2*cw*ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1)*sw)/(9.*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_434_815 = Coupling(name = 'UVGC_434_815',
                        value = '(2*cw*ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_435_816 = Coupling(name = 'UVGC_435_816',
                        value = '(-2*cw*ee*FRCTdeltaZxsdRsdRxgod*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) + (cw*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_435_817 = Coupling(name = 'UVGC_435_817',
                        value = '(-2*cw*ee*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) + (cw*ee*complex(0,1)*G**3*invFREps*sw)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_436_818 = Coupling(name = 'UVGC_436_818',
                        value = '(cw*ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*G)/((-1 + sw)*sw*(1 + sw)) - (2*cw*ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) - (cw*ee*complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_436_819 = Coupling(name = 'UVGC_436_819',
                        value = '(cw*ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G)/((-1 + sw)*sw*(1 + sw)) - (2*cw*ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) - (cw*ee*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**3*invFREps*sw)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_437_820 = Coupling(name = 'UVGC_437_820',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(2.*sw**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_437_821 = Coupling(name = 'UVGC_437_821',
                        value = '(7*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(2.*sw**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_438_822 = Coupling(name = 'UVGC_438_822',
                        value = '(3*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_439_823 = Coupling(name = 'UVGC_439_823',
                        value = 'FRCTdeltaZxssLssLxGssL*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_439_824 = Coupling(name = 'UVGC_439_824',
                        value = 'FRCTdeltaZxssLssLxgos*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_440_825 = Coupling(name = 'UVGC_440_825',
                        value = 'FRCTdeltaZxssRssRxGssR*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_440_826 = Coupling(name = 'UVGC_440_826',
                        value = 'FRCTdeltaZxssRssRxgos*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_441_827 = Coupling(name = 'UVGC_441_827',
                        value = '(ee*FRCTdeltaZxssLssLxGssL*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_441_828 = Coupling(name = 'UVGC_441_828',
                        value = '(ee*FRCTdeltaZxssLssLxgos*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_442_829 = Coupling(name = 'UVGC_442_829',
                        value = '(ee*FRCTdeltaZxssRssRxGssR*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_442_830 = Coupling(name = 'UVGC_442_830',
                        value = '(ee*FRCTdeltaZxssRssRxgos*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_443_831 = Coupling(name = 'UVGC_443_831',
                        value = '(2*ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/9. + (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_443_832 = Coupling(name = 'UVGC_443_832',
                        value = '(2*ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/9. - (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_444_833 = Coupling(name = 'UVGC_444_833',
                        value = '(2*ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1))/9. + (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_444_834 = Coupling(name = 'UVGC_444_834',
                        value = '(2*ee**2*FRCTdeltaZxssRssRxgos*complex(0,1))/9. - (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_445_835 = Coupling(name = 'UVGC_445_835',
                        value = '-(FRCTdeltaZxssLssLxGssL*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_445_836 = Coupling(name = 'UVGC_445_836',
                        value = '-(FRCTdeltaZxssLssLxgos*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_446_837 = Coupling(name = 'UVGC_446_837',
                        value = '-(FRCTdeltaZxssRssRxGssR*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_446_838 = Coupling(name = 'UVGC_446_838',
                        value = '-(FRCTdeltaZxssRssRxgos*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_447_839 = Coupling(name = 'UVGC_447_839',
                        value = '(-2*ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_447_840 = Coupling(name = 'UVGC_447_840',
                        value = '(-2*ee*FRCTdeltaZxssLssLxgos*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_448_841 = Coupling(name = 'UVGC_448_841',
                        value = '(-2*ee*FRCTdeltaZxssRssRxGssR*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_448_842 = Coupling(name = 'UVGC_448_842',
                        value = '(-2*ee*FRCTdeltaZxssRssRxgos*complex(0,1)*G)/3. + (ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_449_843 = Coupling(name = 'UVGC_449_843',
                        value = 'FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_449_844 = Coupling(name = 'UVGC_449_844',
                        value = 'FRCTdeltaZxssLssLxgos*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_450_845 = Coupling(name = 'UVGC_450_845',
                        value = 'FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_450_846 = Coupling(name = 'UVGC_450_846',
                        value = 'FRCTdeltaZxssRssRxgos*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_451_847 = Coupling(name = 'UVGC_451_847',
                        value = '-2*FRCTdeltaxMssLxssL*complex(0,1)*MssL - (complex(0,1)*G**2*invFREps*MssL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_451_848 = Coupling(name = 'UVGC_451_848',
                        value = '-2*FRCTdeltaxMssLxGssL*complex(0,1)*MssL - FRCTdeltaZxssLssLxGssL*complex(0,1)*MssL**2 + (complex(0,1)*G**2*invFREps*MssL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_451_849 = Coupling(name = 'UVGC_451_849',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMssLxgos*complex(0,1)*MssL - FRCTdeltaZxssLssLxgos*complex(0,1)*MssL**2',
                        order = {'QCD':2})

UVGC_452_850 = Coupling(name = 'UVGC_452_850',
                        value = '-2*FRCTdeltaxMssRxssR*complex(0,1)*MssR - (complex(0,1)*G**2*invFREps*MssR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_452_851 = Coupling(name = 'UVGC_452_851',
                        value = '-2*FRCTdeltaxMssRxGssR*complex(0,1)*MssR - FRCTdeltaZxssRssRxGssR*complex(0,1)*MssR**2 + (complex(0,1)*G**2*invFREps*MssR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_452_852 = Coupling(name = 'UVGC_452_852',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMssRxgos*complex(0,1)*MssR - FRCTdeltaZxssRssRxgos*complex(0,1)*MssR**2',
                        order = {'QCD':2})

UVGC_453_853 = Coupling(name = 'UVGC_453_853',
                        value = '(ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1))/(6 - 6*sw**2) + (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_453_854 = Coupling(name = 'UVGC_453_854',
                        value = '(ee**2*FRCTdeltaZxssRssRxgos*complex(0,1))/(6 - 6*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_454_855 = Coupling(name = 'UVGC_454_855',
                        value = '(2*ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1)*sw**2)/(9 - 9*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

UVGC_454_856 = Coupling(name = 'UVGC_454_856',
                        value = '(2*ee**2*FRCTdeltaZxssRssRxgos*complex(0,1)*sw**2)/(9 - 9*sw**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':2,'QED':2})

UVGC_455_857 = Coupling(name = 'UVGC_455_857',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxssL*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_455_858 = Coupling(name = 'UVGC_455_858',
                        value = '(2*FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_455_859 = Coupling(name = 'UVGC_455_859',
                        value = '(2*FRCTdeltaZxssLssLxgos*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxssLssLxgos*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_456_860 = Coupling(name = 'UVGC_456_860',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxssR*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_456_861 = Coupling(name = 'UVGC_456_861',
                        value = '(2*FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_456_862 = Coupling(name = 'UVGC_456_862',
                        value = '(2*FRCTdeltaZxssRssRxgos*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxssRssRxgos*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                        order = {'QCD':4})

UVGC_457_863 = Coupling(name = 'UVGC_457_863',
                        value = '(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_457_864 = Coupling(name = 'UVGC_457_864',
                        value = '(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(6.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_458_865 = Coupling(name = 'UVGC_458_865',
                        value = '(ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_458_866 = Coupling(name = 'UVGC_458_866',
                        value = '(ee**2*FRCTdeltaZxssRssRxgos*complex(0,1))/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_459_867 = Coupling(name = 'UVGC_459_867',
                        value = '-(ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_459_868 = Coupling(name = 'UVGC_459_868',
                        value = '-(ee**2*FRCTdeltaZxssRssRxgos*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssRssRxgos*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_460_869 = Coupling(name = 'UVGC_460_869',
                        value = '-(ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_460_870 = Coupling(name = 'UVGC_460_870',
                        value = '-(ee**2*FRCTdeltaZxssRssRxgos*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssRssRxgos*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_461_871 = Coupling(name = 'UVGC_461_871',
                        value = '-(ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_461_872 = Coupling(name = 'UVGC_461_872',
                        value = '-(ee**2*FRCTdeltaZxssRssRxgos*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssRssRxgos*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_462_873 = Coupling(name = 'UVGC_462_873',
                        value = '(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_462_874 = Coupling(name = 'UVGC_462_874',
                        value = '(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_463_875 = Coupling(name = 'UVGC_463_875',
                        value = '-(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_463_876 = Coupling(name = 'UVGC_463_876',
                        value = '-(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_464_877 = Coupling(name = 'UVGC_464_877',
                        value = '-(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*Rtau1x1**2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*Rtau1x2**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*Rtau1x1**2)/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_464_878 = Coupling(name = 'UVGC_464_878',
                        value = '-(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*Rtau1x1**2)/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*Rtau1x2**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*Rtau1x1**2)/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_465_879 = Coupling(name = 'UVGC_465_879',
                        value = '-(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*Rtau1x1*Rtau2x1)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*Rtau1x2*Rtau2x2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*Rtau1x1*Rtau2x1)/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_465_880 = Coupling(name = 'UVGC_465_880',
                        value = '-(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*Rtau1x1*Rtau2x1)/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*Rtau1x2*Rtau2x2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*Rtau1x1*Rtau2x1)/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_466_881 = Coupling(name = 'UVGC_466_881',
                        value = '-(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*Rtau2x1**2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*Rtau2x2**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*Rtau2x1**2)/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_466_882 = Coupling(name = 'UVGC_466_882',
                        value = '-(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*Rtau2x1**2)/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*Rtau2x2**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*Rtau2x1**2)/(4.*sw**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_467_883 = Coupling(name = 'UVGC_467_883',
                        value = '-(cw*ee*FRCTdeltaZxssLssLxGssL*complex(0,1))/(2.*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_467_884 = Coupling(name = 'UVGC_467_884',
                        value = '-(cw*ee*FRCTdeltaZxssLssLxgos*complex(0,1))/(2.*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*FRCTdeltaZxssLssLxgos*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_468_885 = Coupling(name = 'UVGC_468_885',
                        value = '(cw*ee*FRCTdeltaZxssRssRxGssR*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_468_886 = Coupling(name = 'UVGC_468_886',
                        value = '(cw*ee*FRCTdeltaZxssRssRxgos*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_469_887 = Coupling(name = 'UVGC_469_887',
                        value = '-(cw*ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(3.*sw*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_469_888 = Coupling(name = 'UVGC_469_888',
                        value = '-(cw*ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(3.*sw*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*sw)/(9.*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_470_889 = Coupling(name = 'UVGC_470_889',
                        value = '(2*cw*ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_470_890 = Coupling(name = 'UVGC_470_890',
                        value = '(2*cw*ee**2*FRCTdeltaZxssRssRxgos*complex(0,1)*sw)/(9.*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_471_891 = Coupling(name = 'UVGC_471_891',
                        value = '(-2*cw*ee*FRCTdeltaZxssRssRxGssR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) + (cw*ee*complex(0,1)*G**3*invFREps*sw)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_471_892 = Coupling(name = 'UVGC_471_892',
                        value = '(-2*cw*ee*FRCTdeltaZxssRssRxgos*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) + (cw*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_472_893 = Coupling(name = 'UVGC_472_893',
                        value = '(cw*ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*G)/((-1 + sw)*sw*(1 + sw)) - (2*cw*ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) - (cw*ee*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**3*invFREps*sw)/(72.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_472_894 = Coupling(name = 'UVGC_472_894',
                        value = '(cw*ee*FRCTdeltaZxssLssLxgos*complex(0,1)*G)/((-1 + sw)*sw*(1 + sw)) - (2*cw*ee*FRCTdeltaZxssLssLxgos*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) - (cw*ee*complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':3,'QED':1})

UVGC_473_895 = Coupling(name = 'UVGC_473_895',
                        value = '(7*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(2.*sw**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_473_896 = Coupling(name = 'UVGC_473_896',
                        value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(2.*sw**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                        order = {'QCD':2,'QED':2})

UVGC_474_897 = Coupling(name = 'UVGC_474_897',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1))',
                        order = {'QCD':2})

UVGC_474_898 = Coupling(name = 'UVGC_474_898',
                        value = '-(FRCTdeltaZxbbLxgosbL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_475_899 = Coupling(name = 'UVGC_475_899',
                        value = '-(ee*FRCTdeltaZxbbLxbG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_475_900 = Coupling(name = 'UVGC_475_900',
                        value = '-(ee*FRCTdeltaZxbbLxgosbL*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_476_901 = Coupling(name = 'UVGC_476_901',
                        value = '(FRCTdeltaZxGGxb*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_902 = Coupling(name = 'UVGC_476_902',
                        value = '(FRCTdeltaZxGGxc*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_903 = Coupling(name = 'UVGC_476_903',
                        value = '(FRCTdeltaZxGGxd*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_904 = Coupling(name = 'UVGC_476_904',
                        value = '(FRCTdeltaZxGGxG*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_905 = Coupling(name = 'UVGC_476_905',
                        value = '(FRCTdeltaZxGGxghG*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_906 = Coupling(name = 'UVGC_476_906',
                        value = '(FRCTdeltaxaSxgo*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxgo*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_907 = Coupling(name = 'UVGC_476_907',
                        value = '(FRCTdeltaZxGGxs*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_908 = Coupling(name = 'UVGC_476_908',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxsbL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_909 = Coupling(name = 'UVGC_476_909',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxsbR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_910 = Coupling(name = 'UVGC_476_910',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxscL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_911 = Coupling(name = 'UVGC_476_911',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxscR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_912 = Coupling(name = 'UVGC_476_912',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxsdL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_913 = Coupling(name = 'UVGC_476_913',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxsdR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_914 = Coupling(name = 'UVGC_476_914',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxssL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_915 = Coupling(name = 'UVGC_476_915',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxssR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_916 = Coupling(name = 'UVGC_476_916',
                        value = '(FRCTdeltaxaSxstL*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxstL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_917 = Coupling(name = 'UVGC_476_917',
                        value = '(FRCTdeltaxaSxstR*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxstR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_918 = Coupling(name = 'UVGC_476_918',
                        value = '(FRCTdeltaxaSxsuL*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxsuL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_919 = Coupling(name = 'UVGC_476_919',
                        value = '(FRCTdeltaxaSxsuR*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxsuR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_920 = Coupling(name = 'UVGC_476_920',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxt*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_921 = Coupling(name = 'UVGC_476_921',
                        value = '(FRCTdeltaZxGGxu*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_476_922 = Coupling(name = 'UVGC_476_922',
                        value = 'FRCTdeltaZxbbLxbG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_476_923 = Coupling(name = 'UVGC_476_923',
                        value = 'FRCTdeltaZxbbLxgosbL*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_477_924 = Coupling(name = 'UVGC_477_924',
                        value = '(cw*ee*FRCTdeltaZxbbLxbG*complex(0,1))/(2.*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*FRCTdeltaZxbbLxbG*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_477_925 = Coupling(name = 'UVGC_477_925',
                        value = '(cw*ee*FRCTdeltaZxbbLxgosbL*complex(0,1))/(2.*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_478_926 = Coupling(name = 'UVGC_478_926',
                        value = '-(FRCTdeltaZxbbRxbG*complex(0,1))',
                        order = {'QCD':2})

UVGC_478_927 = Coupling(name = 'UVGC_478_927',
                        value = '-(FRCTdeltaZxbbRxgosbR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_479_928 = Coupling(name = 'UVGC_479_928',
                        value = '-(ee*FRCTdeltaZxbbRxbG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_479_929 = Coupling(name = 'UVGC_479_929',
                        value = '-(ee*FRCTdeltaZxbbRxgosbR*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_480_930 = Coupling(name = 'UVGC_480_930',
                        value = 'FRCTdeltaZxbbRxbG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_480_931 = Coupling(name = 'UVGC_480_931',
                        value = 'FRCTdeltaZxbbRxgosbR*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_481_932 = Coupling(name = 'UVGC_481_932',
                        value = '-(cw*ee*FRCTdeltaZxbbRxbG*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_481_933 = Coupling(name = 'UVGC_481_933',
                        value = '-(cw*ee*FRCTdeltaZxbbRxgosbR*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_482_934 = Coupling(name = 'UVGC_482_934',
                        value = '-(FRCTdeltaZxccLxcG*complex(0,1))',
                        order = {'QCD':2})

UVGC_482_935 = Coupling(name = 'UVGC_482_935',
                        value = '-(FRCTdeltaZxccLxgoscL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_483_936 = Coupling(name = 'UVGC_483_936',
                        value = '(2*ee*FRCTdeltaZxccLxcG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_483_937 = Coupling(name = 'UVGC_483_937',
                        value = '(2*ee*FRCTdeltaZxccLxgoscL*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_484_938 = Coupling(name = 'UVGC_484_938',
                        value = 'FRCTdeltaZxccLxcG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_484_939 = Coupling(name = 'UVGC_484_939',
                        value = 'FRCTdeltaZxccLxgoscL*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_485_940 = Coupling(name = 'UVGC_485_940',
                        value = '-(cw*ee*FRCTdeltaZxccLxcG*complex(0,1))/(2.*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee*FRCTdeltaZxccLxcG*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_485_941 = Coupling(name = 'UVGC_485_941',
                        value = '-(cw*ee*FRCTdeltaZxccLxgoscL*complex(0,1))/(2.*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee*FRCTdeltaZxccLxgoscL*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_486_942 = Coupling(name = 'UVGC_486_942',
                        value = '-(FRCTdeltaZxccRxcG*complex(0,1))',
                        order = {'QCD':2})

UVGC_486_943 = Coupling(name = 'UVGC_486_943',
                        value = '-(FRCTdeltaZxccRxgoscR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_487_944 = Coupling(name = 'UVGC_487_944',
                        value = '(2*ee*FRCTdeltaZxccRxcG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_487_945 = Coupling(name = 'UVGC_487_945',
                        value = '(2*ee*FRCTdeltaZxccRxgoscR*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_488_946 = Coupling(name = 'UVGC_488_946',
                        value = 'FRCTdeltaZxccRxcG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_488_947 = Coupling(name = 'UVGC_488_947',
                        value = 'FRCTdeltaZxccRxgoscR*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_489_948 = Coupling(name = 'UVGC_489_948',
                        value = '(2*cw*ee*FRCTdeltaZxccRxcG*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_489_949 = Coupling(name = 'UVGC_489_949',
                        value = '(2*cw*ee*FRCTdeltaZxccRxgoscR*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_490_950 = Coupling(name = 'UVGC_490_950',
                        value = '-(FRCTdeltaZxddLxdG*complex(0,1))',
                        order = {'QCD':2})

UVGC_490_951 = Coupling(name = 'UVGC_490_951',
                        value = '-(FRCTdeltaZxddLxgosdL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_491_952 = Coupling(name = 'UVGC_491_952',
                        value = '-(ee*FRCTdeltaZxddLxdG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_491_953 = Coupling(name = 'UVGC_491_953',
                        value = '-(ee*FRCTdeltaZxddLxgosdL*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_492_954 = Coupling(name = 'UVGC_492_954',
                        value = 'FRCTdeltaZxddLxdG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_492_955 = Coupling(name = 'UVGC_492_955',
                        value = 'FRCTdeltaZxddLxgosdL*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_493_956 = Coupling(name = 'UVGC_493_956',
                        value = '(cw*ee*FRCTdeltaZxddLxdG*complex(0,1))/(2.*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*FRCTdeltaZxddLxdG*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_493_957 = Coupling(name = 'UVGC_493_957',
                        value = '(cw*ee*FRCTdeltaZxddLxgosdL*complex(0,1))/(2.*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*FRCTdeltaZxddLxgosdL*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_494_958 = Coupling(name = 'UVGC_494_958',
                        value = '-(FRCTdeltaZxddRxdG*complex(0,1))',
                        order = {'QCD':2})

UVGC_494_959 = Coupling(name = 'UVGC_494_959',
                        value = '-(FRCTdeltaZxddRxgosdR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_495_960 = Coupling(name = 'UVGC_495_960',
                        value = '-(ee*FRCTdeltaZxddRxdG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_495_961 = Coupling(name = 'UVGC_495_961',
                        value = '-(ee*FRCTdeltaZxddRxgosdR*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_496_962 = Coupling(name = 'UVGC_496_962',
                        value = 'FRCTdeltaZxddRxdG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_496_963 = Coupling(name = 'UVGC_496_963',
                        value = 'FRCTdeltaZxddRxgosdR*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_497_964 = Coupling(name = 'UVGC_497_964',
                        value = '-(cw*ee*FRCTdeltaZxddRxdG*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_497_965 = Coupling(name = 'UVGC_497_965',
                        value = '-(cw*ee*FRCTdeltaZxddRxgosdR*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                        order = {'QCD':2,'QED':1})

UVGC_498_966 = Coupling(name = 'UVGC_498_966',
                        value = '(FRCTdeltaZxstLstRxgot*complex(0,1))/2. + (FRCTdeltaZxstRstLxgot*complex(0,1))/2.',
                        order = {'QCD':2})

UVGC_499_967 = Coupling(name = 'UVGC_499_967',
                        value = '-(ee*FRCTdeltaZxstLstRxgot*complex(0,1))/3. - (ee*FRCTdeltaZxstRstLxgot*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_500_968 = Coupling(name = 'UVGC_500_968',
                        value = '(ee*FRCTdeltaZxstLstRxgot*complex(0,1))/3. + (ee*FRCTdeltaZxstRstLxgot*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_501_969 = Coupling(name = 'UVGC_501_969',
                        value = '(4*ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/9. + (4*ee**2*FRCTdeltaZxstRstLxgot*complex(0,1))/9.',
                        order = {'QCD':2,'QED':2})

UVGC_502_970 = Coupling(name = 'UVGC_502_970',
                        value = '-(FRCTdeltaZxstLstRxgot*complex(0,1)*G)/2. - (FRCTdeltaZxstRstLxgot*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_503_971 = Coupling(name = 'UVGC_503_971',
                        value = '(FRCTdeltaZxstLstRxgot*complex(0,1)*G)/2. + (FRCTdeltaZxstRstLxgot*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_504_972 = Coupling(name = 'UVGC_504_972',
                        value = '(2*ee*FRCTdeltaZxstLstRxgot*complex(0,1)*G)/3. + (2*ee*FRCTdeltaZxstRstLxgot*complex(0,1)*G)/3.',
                        order = {'QCD':3,'QED':1})

UVGC_505_973 = Coupling(name = 'UVGC_505_973',
                        value = '(FRCTdeltaZxstLstRxgot*complex(0,1)*G**2)/2. + (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_506_974 = Coupling(name = 'UVGC_506_974',
                        value = 'FRCTdeltaZxstLstLxGstL*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_506_975 = Coupling(name = 'UVGC_506_975',
                        value = 'FRCTdeltaZxstLstLxgot*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_507_976 = Coupling(name = 'UVGC_507_976',
                        value = 'FRCTdeltaZxstRstRxGstR*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_507_977 = Coupling(name = 'UVGC_507_977',
                        value = 'FRCTdeltaZxstRstRxgot*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_508_978 = Coupling(name = 'UVGC_508_978',
                        value = '(-2*ee*FRCTdeltaZxstLstLxGstL*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_508_979 = Coupling(name = 'UVGC_508_979',
                        value = '(-2*ee*FRCTdeltaZxstLstLxgot*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_509_980 = Coupling(name = 'UVGC_509_980',
                        value = '(-2*ee*FRCTdeltaZxstRstRxGstR*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_509_981 = Coupling(name = 'UVGC_509_981',
                        value = '(-2*ee*FRCTdeltaZxstRstRxgot*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_510_982 = Coupling(name = 'UVGC_510_982',
                        value = '(8*ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/9. + (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_510_983 = Coupling(name = 'UVGC_510_983',
                        value = '(8*ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/9. - (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_511_984 = Coupling(name = 'UVGC_511_984',
                        value = '(8*ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1))/9. + (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_511_985 = Coupling(name = 'UVGC_511_985',
                        value = '(8*ee**2*FRCTdeltaZxstRstRxgot*complex(0,1))/9. - (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_512_986 = Coupling(name = 'UVGC_512_986',
                        value = '-(FRCTdeltaZxstLstLxGstL*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_512_987 = Coupling(name = 'UVGC_512_987',
                        value = '-(FRCTdeltaZxstLstLxgot*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_513_988 = Coupling(name = 'UVGC_513_988',
                        value = '-(FRCTdeltaZxstRstRxGstR*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_513_989 = Coupling(name = 'UVGC_513_989',
                        value = '-(FRCTdeltaZxstRstRxgot*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_514_990 = Coupling(name = 'UVGC_514_990',
                        value = '(4*ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*G)/3. - (ee*complex(0,1)*G**3*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_514_991 = Coupling(name = 'UVGC_514_991',
                        value = '(4*ee*FRCTdeltaZxstLstLxgot*complex(0,1)*G)/3. - (2*ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_515_992 = Coupling(name = 'UVGC_515_992',
                        value = '(4*ee*FRCTdeltaZxstRstRxGstR*complex(0,1)*G)/3. - (ee*complex(0,1)*G**3*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_515_993 = Coupling(name = 'UVGC_515_993',
                        value = '(4*ee*FRCTdeltaZxstRstRxgot*complex(0,1)*G)/3. - (2*ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_516_994 = Coupling(name = 'UVGC_516_994',
                        value = 'FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_516_995 = Coupling(name = 'UVGC_516_995',
                        value = 'FRCTdeltaZxstLstLxgot*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_517_996 = Coupling(name = 'UVGC_517_996',
                        value = 'FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_517_997 = Coupling(name = 'UVGC_517_997',
                        value = 'FRCTdeltaZxstRstRxgot*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_518_998 = Coupling(name = 'UVGC_518_998',
                        value = '-(FRCTdeltaxMstLRxgot*complex(0,1)) - (FRCTdeltaZxstLstRxgot*complex(0,1)*MstL**2)/2. - (FRCTdeltaZxstRstLxgot*complex(0,1)*MstR**2)/2. - (complex(0,1)*G**2*invFREps*Mgo*MT)/(3.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_519_999 = Coupling(name = 'UVGC_519_999',
                        value = '-2*FRCTdeltaxMstLxstL*complex(0,1)*MstL - (complex(0,1)*G**2*invFREps*MstL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_519_1000 = Coupling(name = 'UVGC_519_1000',
                         value = '-2*FRCTdeltaxMstLxGstL*complex(0,1)*MstL - FRCTdeltaZxstLstLxGstL*complex(0,1)*MstL**2 + (complex(0,1)*G**2*invFREps*MstL**2)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_519_1001 = Coupling(name = 'UVGC_519_1001',
                         value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMstLxgot*complex(0,1)*MstL - FRCTdeltaZxstLstLxgot*complex(0,1)*MstL**2 + (complex(0,1)*G**2*invFREps*MT**2)/(3.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_520_1002 = Coupling(name = 'UVGC_520_1002',
                         value = '-2*FRCTdeltaxMstRxstR*complex(0,1)*MstR - (complex(0,1)*G**2*invFREps*MstR**2)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_520_1003 = Coupling(name = 'UVGC_520_1003',
                         value = '-2*FRCTdeltaxMstRxGstR*complex(0,1)*MstR - FRCTdeltaZxstRstRxGstR*complex(0,1)*MstR**2 + (complex(0,1)*G**2*invFREps*MstR**2)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_520_1004 = Coupling(name = 'UVGC_520_1004',
                         value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMstRxgot*complex(0,1)*MstR - FRCTdeltaZxstRstRxgot*complex(0,1)*MstR**2 + (complex(0,1)*G**2*invFREps*MT**2)/(3.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_521_1005 = Coupling(name = 'UVGC_521_1005',
                         value = '(2*ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1))/(3 - 3*sw**2) + (ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_521_1006 = Coupling(name = 'UVGC_521_1006',
                         value = '(2*ee**2*FRCTdeltaZxstRstRxgot*complex(0,1))/(3 - 3*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_522_1007 = Coupling(name = 'UVGC_522_1007',
                         value = '(8*ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1)*sw**2)/(9 - 9*sw**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_522_1008 = Coupling(name = 'UVGC_522_1008',
                         value = '(8*ee**2*FRCTdeltaZxstRstRxgot*complex(0,1)*sw**2)/(9 - 9*sw**2) - (4*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_523_1009 = Coupling(name = 'UVGC_523_1009',
                         value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxstL*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_523_1010 = Coupling(name = 'UVGC_523_1010',
                         value = '(2*FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_523_1011 = Coupling(name = 'UVGC_523_1011',
                         value = '(2*FRCTdeltaZxstLstLxgot*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxstLstLxgot*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_524_1012 = Coupling(name = 'UVGC_524_1012',
                         value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxstR*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_524_1013 = Coupling(name = 'UVGC_524_1013',
                         value = '(2*FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_524_1014 = Coupling(name = 'UVGC_524_1014',
                         value = '(2*FRCTdeltaZxstRstRxgot*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxstRstRxgot*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_525_1015 = Coupling(name = 'UVGC_525_1015',
                         value = '(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_525_1016 = Coupling(name = 'UVGC_525_1016',
                         value = '(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(6.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_526_1017 = Coupling(name = 'UVGC_526_1017',
                         value = '(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(36.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1))/(9.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_527_1018 = Coupling(name = 'UVGC_527_1018',
                         value = '-(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(18.*(-1 + sw**2)) + (2*ee**2*FRCTdeltaZxstRstLxgot*complex(0,1))/(9.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_528_1019 = Coupling(name = 'UVGC_528_1019',
                         value = '(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(12.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_529_1020 = Coupling(name = 'UVGC_529_1020',
                         value = '(ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_529_1021 = Coupling(name = 'UVGC_529_1021',
                         value = '(ee**2*FRCTdeltaZxstRstRxgot*complex(0,1))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_530_1022 = Coupling(name = 'UVGC_530_1022',
                         value = '(ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1)*Rtau1x1**2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_530_1023 = Coupling(name = 'UVGC_530_1023',
                         value = '(ee**2*FRCTdeltaZxstRstRxgot*complex(0,1)*Rtau1x1**2)/(3.*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxstRstRxgot*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_531_1024 = Coupling(name = 'UVGC_531_1024',
                         value = '(ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1)*Rtau1x1*Rtau2x1)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(36.*cmath.pi**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_531_1025 = Coupling(name = 'UVGC_531_1025',
                         value = '(ee**2*FRCTdeltaZxstRstRxgot*complex(0,1)*Rtau1x1*Rtau2x1)/(3.*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxstRstRxgot*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_532_1026 = Coupling(name = 'UVGC_532_1026',
                         value = '(ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1)*Rtau2x1**2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_532_1027 = Coupling(name = 'UVGC_532_1027',
                         value = '(ee**2*FRCTdeltaZxstRstRxgot*complex(0,1)*Rtau2x1**2)/(3.*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxstRstRxgot*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_533_1028 = Coupling(name = 'UVGC_533_1028',
                         value = '(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_533_1029 = Coupling(name = 'UVGC_533_1029',
                         value = '(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(4.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_534_1030 = Coupling(name = 'UVGC_534_1030',
                         value = '-(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_534_1031 = Coupling(name = 'UVGC_534_1031',
                         value = '-(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(4.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_535_1032 = Coupling(name = 'UVGC_535_1032',
                         value = '(5*ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(36.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1))/(18.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(8.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_536_1033 = Coupling(name = 'UVGC_536_1033',
                         value = '(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(12.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(8.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_537_1034 = Coupling(name = 'UVGC_537_1034',
                         value = '-(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(9.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1))/(18.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(8.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_538_1035 = Coupling(name = 'UVGC_538_1035',
                         value = '-(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1))/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(8.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_539_1036 = Coupling(name = 'UVGC_539_1036',
                         value = '(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*Rtau1x2**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*Rtau1x1**2)/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_539_1037 = Coupling(name = 'UVGC_539_1037',
                         value = '(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*Rtau1x2**2)/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*Rtau1x1**2)/(4.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_540_1038 = Coupling(name = 'UVGC_540_1038',
                         value = '(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*Rtau1x1**2)/(12.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*Rtau1x2**2)/(12.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*Rtau1x1**2)/(8.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_541_1039 = Coupling(name = 'UVGC_541_1039',
                         value = '(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*Rtau1x2*Rtau2x2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*Rtau1x1*Rtau2x1)/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_541_1040 = Coupling(name = 'UVGC_541_1040',
                         value = '(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*Rtau1x2*Rtau2x2)/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*Rtau1x1*Rtau2x1)/(4.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_542_1041 = Coupling(name = 'UVGC_542_1041',
                         value = '(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*Rtau1x1*Rtau2x1)/(12.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*Rtau1x2*Rtau2x2)/(12.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*Rtau1x1*Rtau2x1)/(8.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_543_1042 = Coupling(name = 'UVGC_543_1042',
                         value = '(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*Rtau2x2**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*Rtau2x1**2)/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_543_1043 = Coupling(name = 'UVGC_543_1043',
                         value = '(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*Rtau2x2**2)/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*Rtau2x1**2)/(4.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_544_1044 = Coupling(name = 'UVGC_544_1044',
                         value = '(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*Rtau2x1**2)/(12.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*Rtau2x2**2)/(12.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*Rtau2x1**2)/(8.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_545_1045 = Coupling(name = 'UVGC_545_1045',
                         value = '(cw*ee*FRCTdeltaZxstLstRxgot*complex(0,1))/(4.*sw*(-1 + sw**2)) - (cw*ee*FRCTdeltaZxstLstRxgot*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*FRCTdeltaZxstRstLxgot*complex(0,1)*sw)/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_546_1046 = Coupling(name = 'UVGC_546_1046',
                         value = '-(cw*ee*FRCTdeltaZxstLstRxgot*complex(0,1))/(4.*sw*(-1 + sw**2)) + (cw*ee*FRCTdeltaZxstLstRxgot*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*FRCTdeltaZxstRstLxgot*complex(0,1)*sw)/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_547_1047 = Coupling(name = 'UVGC_547_1047',
                         value = '-(cw*ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(3.*sw*(-1 + sw**2)) + (4*cw*ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (4*cw*ee**2*FRCTdeltaZxstRstLxgot*complex(0,1)*sw)/(9.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_548_1048 = Coupling(name = 'UVGC_548_1048',
                         value = '-(cw*ee*FRCTdeltaZxstLstRxgot*complex(0,1)*G)/(2.*sw*(-1 + sw**2)) + (2*cw*ee*FRCTdeltaZxstLstRxgot*complex(0,1)*G*sw)/(3.*(-1 + sw**2)) + (2*cw*ee*FRCTdeltaZxstRstLxgot*complex(0,1)*G*sw)/(3.*(-1 + sw**2))',
                         order = {'QCD':3,'QED':1})

UVGC_549_1049 = Coupling(name = 'UVGC_549_1049',
                         value = '(cw*ee*FRCTdeltaZxstLstLxGstL*complex(0,1))/(2.*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*(-1 + sw**2)) - (2*cw*ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_549_1050 = Coupling(name = 'UVGC_549_1050',
                         value = '(cw*ee*FRCTdeltaZxstLstLxgot*complex(0,1))/(2.*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*(-1 + sw**2)) - (2*cw*ee*FRCTdeltaZxstLstLxgot*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_550_1051 = Coupling(name = 'UVGC_550_1051',
                         value = '(-2*cw*ee*FRCTdeltaZxstRstRxGstR*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_550_1052 = Coupling(name = 'UVGC_550_1052',
                         value = '(-2*cw*ee*FRCTdeltaZxstRstRxgot*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_551_1053 = Coupling(name = 'UVGC_551_1053',
                         value = '(-2*cw*ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(3.*sw*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*sw*(-1 + sw**2)) + (8*cw*ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (4*cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_551_1054 = Coupling(name = 'UVGC_551_1054',
                         value = '(-2*cw*ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(3.*sw*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*sw*(-1 + sw**2)) + (8*cw*ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*sw)/(9.*(-1 + sw**2)) - (4*cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_552_1055 = Coupling(name = 'UVGC_552_1055',
                         value = '(8*cw*ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (4*cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_552_1056 = Coupling(name = 'UVGC_552_1056',
                         value = '(8*cw*ee**2*FRCTdeltaZxstRstRxgot*complex(0,1)*sw)/(9.*(-1 + sw**2)) - (4*cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_553_1057 = Coupling(name = 'UVGC_553_1057',
                         value = '(4*cw*ee*FRCTdeltaZxstRstRxGstR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) - (cw*ee*complex(0,1)*G**3*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':3,'QED':1})

UVGC_553_1058 = Coupling(name = 'UVGC_553_1058',
                         value = '(4*cw*ee*FRCTdeltaZxstRstRxgot*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) - (2*cw*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':3,'QED':1})

UVGC_554_1059 = Coupling(name = 'UVGC_554_1059',
                         value = '-((cw*ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*G)/((-1 + sw)*sw*(1 + sw))) + (4*cw*ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) + (cw*ee*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**3*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':3,'QED':1})

UVGC_554_1060 = Coupling(name = 'UVGC_554_1060',
                         value = '-((cw*ee*FRCTdeltaZxstLstLxgot*complex(0,1)*G)/((-1 + sw)*sw*(1 + sw))) + (4*cw*ee*FRCTdeltaZxstLstLxgot*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) + (cw*ee*complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2*sw*(-1 + sw**2)) - (2*cw*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':3,'QED':1})

UVGC_555_1061 = Coupling(name = 'UVGC_555_1061',
                         value = '(11*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (4*ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(2.*sw**2*(-1 + sw**2)) - (8*ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_555_1062 = Coupling(name = 'UVGC_555_1062',
                         value = '(2*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (4*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (4*ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(2.*sw**2*(-1 + sw**2)) - (8*ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_556_1063 = Coupling(name = 'UVGC_556_1063',
                         value = '(2*ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(4.*sw**2*(-1 + sw**2)) - (4*ee**2*FRCTdeltaZxstLstRxgot*complex(0,1)*sw**2)/(9.*(-1 + sw**2)) - (4*ee**2*FRCTdeltaZxstRstLxgot*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_557_1064 = Coupling(name = 'UVGC_557_1064',
                         value = '-(FRCTdeltaZxstLstRxgot*complex(0,1)*G**2)/(12.*(-1 + sw**2)) + (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2)/(12.*(-1 + sw**2)) + (FRCTdeltaZxstLstRxgot*complex(0,1)*G**2*sw**2)/(12.*(-1 + sw**2)) - (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2*sw**2)/(12.*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_558_1065 = Coupling(name = 'UVGC_558_1065',
                         value = '(FRCTdeltaZxstLstRxgot*complex(0,1)*G**2)/(12.*(-1 + sw**2)) - (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2)/(12.*(-1 + sw**2)) - (FRCTdeltaZxstLstRxgot*complex(0,1)*G**2*sw**2)/(12.*(-1 + sw**2)) + (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2*sw**2)/(12.*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_559_1066 = Coupling(name = 'UVGC_559_1066',
                         value = '-(FRCTdeltaZxstLstRxgot*complex(0,1)*G**2)/(6.*(-1 + sw**2)) + (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2)/(6.*(-1 + sw**2)) + (FRCTdeltaZxstLstRxgot*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw**2)) - (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_560_1067 = Coupling(name = 'UVGC_560_1067',
                         value = '(FRCTdeltaZxstLstRxgot*complex(0,1)*G**2)/(6.*(-1 + sw**2)) - (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2)/(6.*(-1 + sw**2)) - (FRCTdeltaZxstLstRxgot*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw**2)) + (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_561_1068 = Coupling(name = 'UVGC_561_1068',
                         value = '-(FRCTdeltaZxstLstRxgot*complex(0,1)*G**2)/(4.*(-1 + sw**2)) + (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2)/(4.*(-1 + sw**2)) + (FRCTdeltaZxstLstRxgot*complex(0,1)*G**2*sw**2)/(4.*(-1 + sw**2)) - (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2*sw**2)/(4.*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_562_1069 = Coupling(name = 'UVGC_562_1069',
                         value = '(FRCTdeltaZxstLstRxgot*complex(0,1)*G**2)/(4.*(-1 + sw**2)) - (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2)/(4.*(-1 + sw**2)) - (FRCTdeltaZxstLstRxgot*complex(0,1)*G**2*sw**2)/(4.*(-1 + sw**2)) + (FRCTdeltaZxstRstLxgot*complex(0,1)*G**2*sw**2)/(4.*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_563_1070 = Coupling(name = 'UVGC_563_1070',
                         value = '-(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(18.*(-1 + sw**2)) + (2*ee**2*FRCTdeltaZxstRstLxgot*complex(0,1))/(9.*(-1 + sw**2)) + (FRCTdeltaZxstLstRxgot*complex(0,1)*yu3x3**2)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstLstRxgot*complex(0,1)*sw**2*yu3x3**2)/(2.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_564_1071 = Coupling(name = 'UVGC_564_1071',
                         value = '-(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(9.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxstRstLxgot*complex(0,1))/(18.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(8.*sw**2*(-1 + sw**2)) + (FRCTdeltaZxstRstLxgot*complex(0,1)*yu3x3**2)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstRstLxgot*complex(0,1)*sw**2*yu3x3**2)/(2.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_565_1072 = Coupling(name = 'UVGC_565_1072',
                         value = 'FRCTdeltaZxsuLsuLxGsuL*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_565_1073 = Coupling(name = 'UVGC_565_1073',
                         value = 'FRCTdeltaZxsuLsuLxgou*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_566_1074 = Coupling(name = 'UVGC_566_1074',
                         value = 'FRCTdeltaZxsuRsuRxGsuR*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_566_1075 = Coupling(name = 'UVGC_566_1075',
                         value = 'FRCTdeltaZxsuRsuRxgou*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_567_1076 = Coupling(name = 'UVGC_567_1076',
                         value = '(-2*ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_567_1077 = Coupling(name = 'UVGC_567_1077',
                         value = '(-2*ee*FRCTdeltaZxsuLsuLxgou*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_568_1078 = Coupling(name = 'UVGC_568_1078',
                         value = '(-2*ee*FRCTdeltaZxsuRsuRxGsuR*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_568_1079 = Coupling(name = 'UVGC_568_1079',
                         value = '(-2*ee*FRCTdeltaZxsuRsuRxgou*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_569_1080 = Coupling(name = 'UVGC_569_1080',
                         value = '(8*ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/9. + (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_569_1081 = Coupling(name = 'UVGC_569_1081',
                         value = '(8*ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/9. - (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_570_1082 = Coupling(name = 'UVGC_570_1082',
                         value = '(8*ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1))/9. + (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_570_1083 = Coupling(name = 'UVGC_570_1083',
                         value = '(8*ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1))/9. - (4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_571_1084 = Coupling(name = 'UVGC_571_1084',
                         value = '-(FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_571_1085 = Coupling(name = 'UVGC_571_1085',
                         value = '-(FRCTdeltaZxsuLsuLxgou*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_572_1086 = Coupling(name = 'UVGC_572_1086',
                         value = '-(FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_572_1087 = Coupling(name = 'UVGC_572_1087',
                         value = '-(FRCTdeltaZxsuRsuRxgou*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_573_1088 = Coupling(name = 'UVGC_573_1088',
                         value = '(4*ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G)/3. - (ee*complex(0,1)*G**3*invFREps)/(36.*cmath.pi**2)',
                         order = {'QCD':3,'QED':1})

UVGC_573_1089 = Coupling(name = 'UVGC_573_1089',
                         value = '(4*ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*G)/3. - (2*ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                         order = {'QCD':3,'QED':1})

UVGC_574_1090 = Coupling(name = 'UVGC_574_1090',
                         value = '(4*ee*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G)/3. - (ee*complex(0,1)*G**3*invFREps)/(36.*cmath.pi**2)',
                         order = {'QCD':3,'QED':1})

UVGC_574_1091 = Coupling(name = 'UVGC_574_1091',
                         value = '(4*ee*FRCTdeltaZxsuRsuRxgou*complex(0,1)*G)/3. - (2*ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                         order = {'QCD':3,'QED':1})

UVGC_575_1092 = Coupling(name = 'UVGC_575_1092',
                         value = 'FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_575_1093 = Coupling(name = 'UVGC_575_1093',
                         value = 'FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_576_1094 = Coupling(name = 'UVGC_576_1094',
                         value = 'FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_576_1095 = Coupling(name = 'UVGC_576_1095',
                         value = 'FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_577_1096 = Coupling(name = 'UVGC_577_1096',
                         value = '-2*FRCTdeltaxMsuLxsuL*complex(0,1)*MsuL - (complex(0,1)*G**2*invFREps*MsuL**2)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_577_1097 = Coupling(name = 'UVGC_577_1097',
                         value = '-2*FRCTdeltaxMsuLxGsuL*complex(0,1)*MsuL - FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*MsuL**2 + (complex(0,1)*G**2*invFREps*MsuL**2)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_577_1098 = Coupling(name = 'UVGC_577_1098',
                         value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMsuLxgou*complex(0,1)*MsuL - FRCTdeltaZxsuLsuLxgou*complex(0,1)*MsuL**2',
                         order = {'QCD':2})

UVGC_578_1099 = Coupling(name = 'UVGC_578_1099',
                         value = '-2*FRCTdeltaxMsuRxsuR*complex(0,1)*MsuR - (complex(0,1)*G**2*invFREps*MsuR**2)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_578_1100 = Coupling(name = 'UVGC_578_1100',
                         value = '-2*FRCTdeltaxMsuRxGsuR*complex(0,1)*MsuR - FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*MsuR**2 + (complex(0,1)*G**2*invFREps*MsuR**2)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_578_1101 = Coupling(name = 'UVGC_578_1101',
                         value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMsuRxgou*complex(0,1)*MsuR - FRCTdeltaZxsuRsuRxgou*complex(0,1)*MsuR**2',
                         order = {'QCD':2})

UVGC_579_1102 = Coupling(name = 'UVGC_579_1102',
                         value = '(2*ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1))/(3 - 3*sw**2) + (ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_579_1103 = Coupling(name = 'UVGC_579_1103',
                         value = '(2*ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1))/(3 - 3*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_580_1104 = Coupling(name = 'UVGC_580_1104',
                         value = '(8*ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*sw**2)/(9 - 9*sw**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_580_1105 = Coupling(name = 'UVGC_580_1105',
                         value = '(8*ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1)*sw**2)/(9 - 9*sw**2) - (4*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_581_1106 = Coupling(name = 'UVGC_581_1106',
                         value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsuL*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_581_1107 = Coupling(name = 'UVGC_581_1107',
                         value = '(2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_581_1108 = Coupling(name = 'UVGC_581_1108',
                         value = '(2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_582_1109 = Coupling(name = 'UVGC_582_1109',
                         value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsuR*complex(0,1)*G**2*sw**2)/(3.*aS*(-1 + sw)*(1 + sw)) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_582_1110 = Coupling(name = 'UVGC_582_1110',
                         value = '(2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_582_1111 = Coupling(name = 'UVGC_582_1111',
                         value = '(2*FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2)/(3.*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2*sw**2)/(3.*(-1 + sw)*(1 + sw)) + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_583_1112 = Coupling(name = 'UVGC_583_1112',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_583_1113 = Coupling(name = 'UVGC_583_1113',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(6.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_584_1114 = Coupling(name = 'UVGC_584_1114',
                         value = '(ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_584_1115 = Coupling(name = 'UVGC_584_1115',
                         value = '(ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_585_1116 = Coupling(name = 'UVGC_585_1116',
                         value = '(ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*Rtau1x1**2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_585_1117 = Coupling(name = 'UVGC_585_1117',
                         value = '(ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1)*Rtau1x1**2)/(3.*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1)*Rtau1x2**2)/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_586_1118 = Coupling(name = 'UVGC_586_1118',
                         value = '(ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*Rtau1x1*Rtau2x1)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(36.*cmath.pi**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_586_1119 = Coupling(name = 'UVGC_586_1119',
                         value = '(ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1)*Rtau1x1*Rtau2x1)/(3.*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1)*Rtau1x2*Rtau2x2)/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_587_1120 = Coupling(name = 'UVGC_587_1120',
                         value = '(ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*Rtau2x1**2)/(3.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(36.*cmath.pi**2*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_587_1121 = Coupling(name = 'UVGC_587_1121',
                         value = '(ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1)*Rtau2x1**2)/(3.*(-1 + sw**2)) - (2*ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1)*Rtau2x2**2)/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_588_1122 = Coupling(name = 'UVGC_588_1122',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_588_1123 = Coupling(name = 'UVGC_588_1123',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(4.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_589_1124 = Coupling(name = 'UVGC_589_1124',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(3.*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(4.*sw**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_589_1125 = Coupling(name = 'UVGC_589_1125',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(3.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(4.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_590_1126 = Coupling(name = 'UVGC_590_1126',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*Rtau1x2**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*Rtau1x1**2)/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_590_1127 = Coupling(name = 'UVGC_590_1127',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*Rtau1x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*Rtau1x2**2)/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*Rtau1x1**2)/(4.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_591_1128 = Coupling(name = 'UVGC_591_1128',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*Rtau1x2*Rtau2x2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau1x2*Rtau2x2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*Rtau1x1*Rtau2x1)/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau1x1*Rtau2x1)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_591_1129 = Coupling(name = 'UVGC_591_1129',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*Rtau1x1*Rtau2x1)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*Rtau1x2*Rtau2x2)/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*Rtau1x1*Rtau2x1)/(4.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_592_1130 = Coupling(name = 'UVGC_592_1130',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*Rtau2x2**2)/(6.*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*Rtau2x2**2)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*Rtau2x1**2)/(4.*sw**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*Rtau2x1**2)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_592_1131 = Coupling(name = 'UVGC_592_1131',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*Rtau2x1**2)/(6.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*Rtau2x2**2)/(6.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*Rtau2x1**2)/(4.*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_593_1132 = Coupling(name = 'UVGC_593_1132',
                         value = '(cw*ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(2.*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*(-1 + sw**2)) - (2*cw*ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_593_1133 = Coupling(name = 'UVGC_593_1133',
                         value = '(cw*ee*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(2.*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*(-1 + sw**2)) - (2*cw*ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_594_1134 = Coupling(name = 'UVGC_594_1134',
                         value = '(-2*cw*ee*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_594_1135 = Coupling(name = 'UVGC_594_1135',
                         value = '(-2*cw*ee*FRCTdeltaZxsuRsuRxgou*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_595_1136 = Coupling(name = 'UVGC_595_1136',
                         value = '(-2*cw*ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(3.*sw*(-1 + sw**2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*sw*(-1 + sw**2)) + (8*cw*ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (4*cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_595_1137 = Coupling(name = 'UVGC_595_1137',
                         value = '(-2*cw*ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(3.*sw*(-1 + sw**2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*sw*(-1 + sw**2)) + (8*cw*ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*sw)/(9.*(-1 + sw**2)) - (4*cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_596_1138 = Coupling(name = 'UVGC_596_1138',
                         value = '(8*cw*ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*sw)/(9.*(-1 + sw**2)) + (4*cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_596_1139 = Coupling(name = 'UVGC_596_1139',
                         value = '(8*cw*ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1)*sw)/(9.*(-1 + sw**2)) - (4*cw*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_597_1140 = Coupling(name = 'UVGC_597_1140',
                         value = '(4*cw*ee*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) - (cw*ee*complex(0,1)*G**3*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':3,'QED':1})

UVGC_597_1141 = Coupling(name = 'UVGC_597_1141',
                         value = '(4*cw*ee*FRCTdeltaZxsuRsuRxgou*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) - (2*cw*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':3,'QED':1})

UVGC_598_1142 = Coupling(name = 'UVGC_598_1142',
                         value = '-((cw*ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G)/((-1 + sw)*sw*(1 + sw))) + (4*cw*ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) + (cw*ee*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**3*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':3,'QED':1})

UVGC_598_1143 = Coupling(name = 'UVGC_598_1143',
                         value = '-((cw*ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*G)/((-1 + sw)*sw*(1 + sw))) + (4*cw*ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*G*sw)/(3.*(-1 + sw)*(1 + sw)) + (cw*ee*complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2*sw*(-1 + sw**2)) - (2*cw*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':3,'QED':1})

UVGC_599_1144 = Coupling(name = 'UVGC_599_1144',
                         value = '(11*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (4*ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(2.*sw**2*(-1 + sw**2)) - (8*ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_599_1145 = Coupling(name = 'UVGC_599_1145',
                         value = '(2*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (4*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (4*ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(3.*(-1 + sw**2)) - (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(2.*sw**2*(-1 + sw**2)) - (8*ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*sw**2)/(9.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_600_1146 = Coupling(name = 'UVGC_600_1146',
                         value = '-(FRCTdeltaZxssLxsG*complex(0,1))',
                         order = {'QCD':2})

UVGC_600_1147 = Coupling(name = 'UVGC_600_1147',
                         value = '-(FRCTdeltaZxssLxgossL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_601_1148 = Coupling(name = 'UVGC_601_1148',
                         value = '-(ee*FRCTdeltaZxssLxsG*complex(0,1))/3.',
                         order = {'QCD':2,'QED':1})

UVGC_601_1149 = Coupling(name = 'UVGC_601_1149',
                         value = '-(ee*FRCTdeltaZxssLxgossL*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_602_1150 = Coupling(name = 'UVGC_602_1150',
                         value = '-(FRCTdeltaZxssRxsG*complex(0,1))',
                         order = {'QCD':2})

UVGC_602_1151 = Coupling(name = 'UVGC_602_1151',
                         value = '-(FRCTdeltaZxssRxgossR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_603_1152 = Coupling(name = 'UVGC_603_1152',
                         value = '-(ee*FRCTdeltaZxssRxsG*complex(0,1))/3.',
                         order = {'QCD':2,'QED':1})

UVGC_603_1153 = Coupling(name = 'UVGC_603_1153',
                         value = '-(ee*FRCTdeltaZxssRxgossR*complex(0,1))/3. + (ee*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_604_1154 = Coupling(name = 'UVGC_604_1154',
                         value = 'FRCTdeltaZxssLxsG*complex(0,1)*G',
                         order = {'QCD':3})

UVGC_604_1155 = Coupling(name = 'UVGC_604_1155',
                         value = 'FRCTdeltaZxssLxgossL*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_605_1156 = Coupling(name = 'UVGC_605_1156',
                         value = 'FRCTdeltaZxssRxsG*complex(0,1)*G',
                         order = {'QCD':3})

UVGC_605_1157 = Coupling(name = 'UVGC_605_1157',
                         value = 'FRCTdeltaZxssRxgossR*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_609_1158 = Coupling(name = 'UVGC_609_1158',
                         value = '(cw*ee*FRCTdeltaZxssLxsG*complex(0,1))/(2.*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*FRCTdeltaZxssLxsG*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_609_1159 = Coupling(name = 'UVGC_609_1159',
                         value = '(cw*ee*FRCTdeltaZxssLxgossL*complex(0,1))/(2.*sw*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw*(-1 + sw**2)) - (cw*ee*FRCTdeltaZxssLxgossL*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_610_1160 = Coupling(name = 'UVGC_610_1160',
                         value = '-(cw*ee*FRCTdeltaZxssRxsG*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_610_1161 = Coupling(name = 'UVGC_610_1161',
                         value = '-(cw*ee*FRCTdeltaZxssRxgossR*complex(0,1)*sw)/(3.*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps*sw)/(36.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_611_1162 = Coupling(name = 'UVGC_611_1162',
                         value = '-(FRCTdeltaZxttLxtG*complex(0,1))',
                         order = {'QCD':2})

UVGC_611_1163 = Coupling(name = 'UVGC_611_1163',
                         value = '-(FRCTdeltaZxttLxgostL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_611_1164 = Coupling(name = 'UVGC_611_1164',
                         value = '-(FRCTdeltaZxttLxgostR*complex(0,1))',
                         order = {'QCD':2})

UVGC_612_1165 = Coupling(name = 'UVGC_612_1165',
                         value = '(2*ee*FRCTdeltaZxttLxtG*complex(0,1))/3.',
                         order = {'QCD':2,'QED':1})

UVGC_612_1166 = Coupling(name = 'UVGC_612_1166',
                         value = '(2*ee*FRCTdeltaZxttLxgostL*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_612_1167 = Coupling(name = 'UVGC_612_1167',
                         value = '(2*ee*FRCTdeltaZxttLxgostR*complex(0,1))/3.',
                         order = {'QCD':2,'QED':1})

UVGC_613_1168 = Coupling(name = 'UVGC_613_1168',
                         value = '-(FRCTdeltaZxttRxtG*complex(0,1))',
                         order = {'QCD':2})

UVGC_613_1169 = Coupling(name = 'UVGC_613_1169',
                         value = '-(FRCTdeltaZxttRxgostL*complex(0,1))',
                         order = {'QCD':2})

UVGC_613_1170 = Coupling(name = 'UVGC_613_1170',
                         value = '-(FRCTdeltaZxttRxgostR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_614_1171 = Coupling(name = 'UVGC_614_1171',
                         value = '(2*ee*FRCTdeltaZxttRxtG*complex(0,1))/3.',
                         order = {'QCD':2,'QED':1})

UVGC_614_1172 = Coupling(name = 'UVGC_614_1172',
                         value = '(2*ee*FRCTdeltaZxttRxgostL*complex(0,1))/3.',
                         order = {'QCD':2,'QED':1})

UVGC_614_1173 = Coupling(name = 'UVGC_614_1173',
                         value = '(2*ee*FRCTdeltaZxttRxgostR*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_615_1174 = Coupling(name = 'UVGC_615_1174',
                         value = 'FRCTdeltaZxttLxtG*complex(0,1)*G',
                         order = {'QCD':3})

UVGC_615_1175 = Coupling(name = 'UVGC_615_1175',
                         value = 'FRCTdeltaZxttLxgostL*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_615_1176 = Coupling(name = 'UVGC_615_1176',
                         value = 'FRCTdeltaZxttLxgostR*complex(0,1)*G',
                         order = {'QCD':3})

UVGC_616_1177 = Coupling(name = 'UVGC_616_1177',
                         value = 'FRCTdeltaZxttRxtG*complex(0,1)*G',
                         order = {'QCD':3})

UVGC_616_1178 = Coupling(name = 'UVGC_616_1178',
                         value = 'FRCTdeltaZxttRxgostL*complex(0,1)*G',
                         order = {'QCD':3})

UVGC_616_1179 = Coupling(name = 'UVGC_616_1179',
                         value = 'FRCTdeltaZxttRxgostR*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_619_1180 = Coupling(name = 'UVGC_619_1180',
                         value = '-(FRCTdeltaxMTxtG*complex(0,1)) - (FRCTdeltaZxttLxtG*complex(0,1)*MT)/2. - (FRCTdeltaZxttRxtG*complex(0,1)*MT)/2. + (complex(0,1)*G**2*invFREps*MT)/(3.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_619_1181 = Coupling(name = 'UVGC_619_1181',
                         value = '-(FRCTdeltaxMTxgostL*complex(0,1)) - (FRCTdeltaZxttLxgostL*complex(0,1)*MT)/2. - (FRCTdeltaZxttRxgostL*complex(0,1)*MT)/2.',
                         order = {'QCD':2})

UVGC_619_1182 = Coupling(name = 'UVGC_619_1182',
                         value = '-(FRCTdeltaxMTxgostR*complex(0,1)) - (FRCTdeltaZxttLxgostR*complex(0,1)*MT)/2. - (FRCTdeltaZxttRxgostR*complex(0,1)*MT)/2.',
                         order = {'QCD':2})

UVGC_620_1183 = Coupling(name = 'UVGC_620_1183',
                         value = '-(cw*ee*FRCTdeltaZxttLxtG*complex(0,1))/(2.*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee*FRCTdeltaZxttLxtG*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_620_1184 = Coupling(name = 'UVGC_620_1184',
                         value = '-(cw*ee*FRCTdeltaZxttLxgostL*complex(0,1))/(2.*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee*FRCTdeltaZxttLxgostL*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_620_1185 = Coupling(name = 'UVGC_620_1185',
                         value = '-(cw*ee*FRCTdeltaZxttLxgostR*complex(0,1))/(2.*sw*(-1 + sw**2)) + (2*cw*ee*FRCTdeltaZxttLxgostR*complex(0,1)*sw)/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_621_1186 = Coupling(name = 'UVGC_621_1186',
                         value = '(2*cw*ee*FRCTdeltaZxttRxtG*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_621_1187 = Coupling(name = 'UVGC_621_1187',
                         value = '(2*cw*ee*FRCTdeltaZxttRxgostL*complex(0,1)*sw)/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_621_1188 = Coupling(name = 'UVGC_621_1188',
                         value = '(2*cw*ee*FRCTdeltaZxttRxgostR*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_622_1189 = Coupling(name = 'UVGC_622_1189',
                         value = '-(FRCTdeltaZxuuLxuG*complex(0,1))',
                         order = {'QCD':2})

UVGC_622_1190 = Coupling(name = 'UVGC_622_1190',
                         value = '-(FRCTdeltaZxuuLxgosuL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_623_1191 = Coupling(name = 'UVGC_623_1191',
                         value = '(2*ee*FRCTdeltaZxuuLxuG*complex(0,1))/3.',
                         order = {'QCD':2,'QED':1})

UVGC_623_1192 = Coupling(name = 'UVGC_623_1192',
                         value = '(2*ee*FRCTdeltaZxuuLxgosuL*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_624_1193 = Coupling(name = 'UVGC_624_1193',
                         value = '-(FRCTdeltaZxuuRxuG*complex(0,1))',
                         order = {'QCD':2})

UVGC_624_1194 = Coupling(name = 'UVGC_624_1194',
                         value = '-(FRCTdeltaZxuuRxgosuR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_625_1195 = Coupling(name = 'UVGC_625_1195',
                         value = '(2*ee*FRCTdeltaZxuuRxuG*complex(0,1))/3.',
                         order = {'QCD':2,'QED':1})

UVGC_625_1196 = Coupling(name = 'UVGC_625_1196',
                         value = '(2*ee*FRCTdeltaZxuuRxgosuR*complex(0,1))/3. - (ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                         order = {'QCD':2,'QED':1})

UVGC_626_1197 = Coupling(name = 'UVGC_626_1197',
                         value = 'FRCTdeltaZxuuLxuG*complex(0,1)*G',
                         order = {'QCD':3})

UVGC_626_1198 = Coupling(name = 'UVGC_626_1198',
                         value = 'FRCTdeltaZxuuLxgosuL*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_627_1199 = Coupling(name = 'UVGC_627_1199',
                         value = 'FRCTdeltaZxuuRxuG*complex(0,1)*G',
                         order = {'QCD':3})

UVGC_627_1200 = Coupling(name = 'UVGC_627_1200',
                         value = 'FRCTdeltaZxuuRxgosuR*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_631_1201 = Coupling(name = 'UVGC_631_1201',
                         value = '-(cw*ee*FRCTdeltaZxuuLxuG*complex(0,1))/(2.*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee*FRCTdeltaZxuuLxuG*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_631_1202 = Coupling(name = 'UVGC_631_1202',
                         value = '-(cw*ee*FRCTdeltaZxuuLxgosuL*complex(0,1))/(2.*sw*(-1 + sw**2)) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw*(-1 + sw**2)) + (2*cw*ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_632_1203 = Coupling(name = 'UVGC_632_1203',
                         value = '(2*cw*ee*FRCTdeltaZxuuRxuG*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_632_1204 = Coupling(name = 'UVGC_632_1204',
                         value = '(2*cw*ee*FRCTdeltaZxuuRxgosuR*complex(0,1)*sw)/(3.*(-1 + sw**2)) - (cw*ee*complex(0,1)*G**2*invFREps*sw)/(18.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_633_1205 = Coupling(name = 'UVGC_633_1205',
                         value = '-(FRCTdeltaxMgoxbsbL*complex(0,1))',
                         order = {'QCD':2})

UVGC_633_1206 = Coupling(name = 'UVGC_633_1206',
                         value = '-(FRCTdeltaxMgoxbsbR*complex(0,1))',
                         order = {'QCD':2})

UVGC_633_1207 = Coupling(name = 'UVGC_633_1207',
                         value = '-(FRCTdeltaxMgoxcscL*complex(0,1))',
                         order = {'QCD':2})

UVGC_633_1208 = Coupling(name = 'UVGC_633_1208',
                         value = '-(FRCTdeltaxMgoxcscR*complex(0,1))',
                         order = {'QCD':2})

UVGC_633_1209 = Coupling(name = 'UVGC_633_1209',
                         value = '-(FRCTdeltaxMgoxdsdL*complex(0,1))',
                         order = {'QCD':2})

UVGC_633_1210 = Coupling(name = 'UVGC_633_1210',
                         value = '-(FRCTdeltaxMgoxdsdR*complex(0,1))',
                         order = {'QCD':2})

UVGC_633_1211 = Coupling(name = 'UVGC_633_1211',
                         value = '-(FRCTdeltaxMgoxgoG*complex(0,1))',
                         order = {'QCD':2})

UVGC_633_1212 = Coupling(name = 'UVGC_633_1212',
                         value = '-(FRCTdeltaxMgoxsssL*complex(0,1))',
                         order = {'QCD':2})

UVGC_633_1213 = Coupling(name = 'UVGC_633_1213',
                         value = '-(FRCTdeltaxMgoxsssR*complex(0,1))',
                         order = {'QCD':2})

UVGC_633_1214 = Coupling(name = 'UVGC_633_1214',
                         value = '-(FRCTdeltaxMgoxtstL*complex(0,1))',
                         order = {'QCD':2})

UVGC_633_1215 = Coupling(name = 'UVGC_633_1215',
                         value = '-(FRCTdeltaxMgoxtstR*complex(0,1))',
                         order = {'QCD':2})

UVGC_633_1216 = Coupling(name = 'UVGC_633_1216',
                         value = '-(FRCTdeltaxMgoxusuL*complex(0,1))',
                         order = {'QCD':2})

UVGC_633_1217 = Coupling(name = 'UVGC_633_1217',
                         value = '-(FRCTdeltaxMgoxusuR*complex(0,1))',
                         order = {'QCD':2})

UVGC_634_1218 = Coupling(name = 'UVGC_634_1218',
                         value = '-(FRCTdeltaZxgogoLxbsbL*complex(0,1)*Mgo)',
                         order = {'QCD':2})

UVGC_634_1219 = Coupling(name = 'UVGC_634_1219',
                         value = '-(FRCTdeltaZxgogoLxbsbR*complex(0,1)*Mgo)',
                         order = {'QCD':2})

UVGC_634_1220 = Coupling(name = 'UVGC_634_1220',
                         value = '-(FRCTdeltaZxgogoLxcscL*complex(0,1)*Mgo)',
                         order = {'QCD':2})

UVGC_634_1221 = Coupling(name = 'UVGC_634_1221',
                         value = '-(FRCTdeltaZxgogoLxcscR*complex(0,1)*Mgo)',
                         order = {'QCD':2})

UVGC_634_1222 = Coupling(name = 'UVGC_634_1222',
                         value = '-(FRCTdeltaZxgogoLxdsdL*complex(0,1)*Mgo)',
                         order = {'QCD':2})

UVGC_634_1223 = Coupling(name = 'UVGC_634_1223',
                         value = '-(FRCTdeltaZxgogoLxdsdR*complex(0,1)*Mgo)',
                         order = {'QCD':2})

UVGC_634_1224 = Coupling(name = 'UVGC_634_1224',
                         value = '-(FRCTdeltaZxgogoLxgoG*complex(0,1)*Mgo) + (3*complex(0,1)*G**2*invFREps*Mgo)/(4.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_634_1225 = Coupling(name = 'UVGC_634_1225',
                         value = '-(FRCTdeltaZxgogoLxsssL*complex(0,1)*Mgo)',
                         order = {'QCD':2})

UVGC_634_1226 = Coupling(name = 'UVGC_634_1226',
                         value = '-(FRCTdeltaZxgogoLxsssR*complex(0,1)*Mgo)',
                         order = {'QCD':2})

UVGC_634_1227 = Coupling(name = 'UVGC_634_1227',
                         value = '-(FRCTdeltaZxgogoLxtstL*complex(0,1)*Mgo)',
                         order = {'QCD':2})

UVGC_634_1228 = Coupling(name = 'UVGC_634_1228',
                         value = '-(FRCTdeltaZxgogoLxtstR*complex(0,1)*Mgo)',
                         order = {'QCD':2})

UVGC_634_1229 = Coupling(name = 'UVGC_634_1229',
                         value = '-(FRCTdeltaZxgogoLxusuL*complex(0,1)*Mgo)',
                         order = {'QCD':2})

UVGC_634_1230 = Coupling(name = 'UVGC_634_1230',
                         value = '-(FRCTdeltaZxgogoLxusuR*complex(0,1)*Mgo)',
                         order = {'QCD':2})

UVGC_635_1231 = Coupling(name = 'UVGC_635_1231',
                         value = '-(FRCTdeltaZxgogoLxbsbL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_635_1232 = Coupling(name = 'UVGC_635_1232',
                         value = '-(FRCTdeltaZxgogoLxbsbR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_635_1233 = Coupling(name = 'UVGC_635_1233',
                         value = '-(FRCTdeltaZxgogoLxcscL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_635_1234 = Coupling(name = 'UVGC_635_1234',
                         value = '-(FRCTdeltaZxgogoLxcscR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_635_1235 = Coupling(name = 'UVGC_635_1235',
                         value = '-(FRCTdeltaZxgogoLxdsdL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_635_1236 = Coupling(name = 'UVGC_635_1236',
                         value = '-(FRCTdeltaZxgogoLxdsdR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_635_1237 = Coupling(name = 'UVGC_635_1237',
                         value = '-(FRCTdeltaZxgogoLxgoG*complex(0,1))',
                         order = {'QCD':2})

UVGC_635_1238 = Coupling(name = 'UVGC_635_1238',
                         value = '-(FRCTdeltaZxgogoLxsssL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_635_1239 = Coupling(name = 'UVGC_635_1239',
                         value = '-(FRCTdeltaZxgogoLxsssR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_635_1240 = Coupling(name = 'UVGC_635_1240',
                         value = '-(FRCTdeltaZxgogoLxtstL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_635_1241 = Coupling(name = 'UVGC_635_1241',
                         value = '-(FRCTdeltaZxgogoLxtstR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_635_1242 = Coupling(name = 'UVGC_635_1242',
                         value = '-(FRCTdeltaZxgogoLxusuL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_635_1243 = Coupling(name = 'UVGC_635_1243',
                         value = '-(FRCTdeltaZxgogoLxusuR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':2})

UVGC_636_1244 = Coupling(name = 'UVGC_636_1244',
                         value = '(FRCTdeltaZxGGxb*G)/2.',
                         order = {'QCD':3})

UVGC_636_1245 = Coupling(name = 'UVGC_636_1245',
                         value = '(FRCTdeltaZxGGxc*G)/2.',
                         order = {'QCD':3})

UVGC_636_1246 = Coupling(name = 'UVGC_636_1246',
                         value = '(FRCTdeltaZxGGxd*G)/2.',
                         order = {'QCD':3})

UVGC_636_1247 = Coupling(name = 'UVGC_636_1247',
                         value = '(FRCTdeltaZxGGxG*G)/2.',
                         order = {'QCD':3})

UVGC_636_1248 = Coupling(name = 'UVGC_636_1248',
                         value = '(FRCTdeltaZxGGxghG*G)/2.',
                         order = {'QCD':3})

UVGC_636_1249 = Coupling(name = 'UVGC_636_1249',
                         value = '(FRCTdeltaxaSxgo*G)/(2.*aS) + (FRCTdeltaZxGGxgo*G)/2.',
                         order = {'QCD':3})

UVGC_636_1250 = Coupling(name = 'UVGC_636_1250',
                         value = '(FRCTdeltaZxGGxs*G)/2.',
                         order = {'QCD':3})

UVGC_636_1251 = Coupling(name = 'UVGC_636_1251',
                         value = '(FRCTdeltaxaSxsbL*G)/(2.*aS) + (FRCTdeltaZxGGxsbL*G)/2.',
                         order = {'QCD':3})

UVGC_636_1252 = Coupling(name = 'UVGC_636_1252',
                         value = '(FRCTdeltaxaSxsbR*G)/(2.*aS) + (FRCTdeltaZxGGxsbR*G)/2.',
                         order = {'QCD':3})

UVGC_636_1253 = Coupling(name = 'UVGC_636_1253',
                         value = '(FRCTdeltaxaSxscL*G)/(2.*aS) + (FRCTdeltaZxGGxscL*G)/2.',
                         order = {'QCD':3})

UVGC_636_1254 = Coupling(name = 'UVGC_636_1254',
                         value = '(FRCTdeltaxaSxscR*G)/(2.*aS) + (FRCTdeltaZxGGxscR*G)/2.',
                         order = {'QCD':3})

UVGC_636_1255 = Coupling(name = 'UVGC_636_1255',
                         value = '(FRCTdeltaxaSxsdL*G)/(2.*aS) + (FRCTdeltaZxGGxsdL*G)/2.',
                         order = {'QCD':3})

UVGC_636_1256 = Coupling(name = 'UVGC_636_1256',
                         value = '(FRCTdeltaxaSxsdR*G)/(2.*aS) + (FRCTdeltaZxGGxsdR*G)/2.',
                         order = {'QCD':3})

UVGC_636_1257 = Coupling(name = 'UVGC_636_1257',
                         value = '(FRCTdeltaxaSxssL*G)/(2.*aS) + (FRCTdeltaZxGGxssL*G)/2.',
                         order = {'QCD':3})

UVGC_636_1258 = Coupling(name = 'UVGC_636_1258',
                         value = '(FRCTdeltaxaSxssR*G)/(2.*aS) + (FRCTdeltaZxGGxssR*G)/2.',
                         order = {'QCD':3})

UVGC_636_1259 = Coupling(name = 'UVGC_636_1259',
                         value = '(FRCTdeltaxaSxstL*G)/(2.*aS) + (FRCTdeltaZxGGxstL*G)/2.',
                         order = {'QCD':3})

UVGC_636_1260 = Coupling(name = 'UVGC_636_1260',
                         value = '(FRCTdeltaxaSxstR*G)/(2.*aS) + (FRCTdeltaZxGGxstR*G)/2.',
                         order = {'QCD':3})

UVGC_636_1261 = Coupling(name = 'UVGC_636_1261',
                         value = '(FRCTdeltaxaSxsuL*G)/(2.*aS) + (FRCTdeltaZxGGxsuL*G)/2.',
                         order = {'QCD':3})

UVGC_636_1262 = Coupling(name = 'UVGC_636_1262',
                         value = '(FRCTdeltaxaSxsuR*G)/(2.*aS) + (FRCTdeltaZxGGxsuR*G)/2.',
                         order = {'QCD':3})

UVGC_636_1263 = Coupling(name = 'UVGC_636_1263',
                         value = '(FRCTdeltaxaSxt*G)/(2.*aS) + (FRCTdeltaZxGGxt*G)/2.',
                         order = {'QCD':3})

UVGC_636_1264 = Coupling(name = 'UVGC_636_1264',
                         value = '(FRCTdeltaZxGGxu*G)/2.',
                         order = {'QCD':3})

UVGC_636_1265 = Coupling(name = 'UVGC_636_1265',
                         value = 'FRCTdeltaZxgogoLxbsbL*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_636_1266 = Coupling(name = 'UVGC_636_1266',
                         value = 'FRCTdeltaZxgogoLxbsbR*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_636_1267 = Coupling(name = 'UVGC_636_1267',
                         value = 'FRCTdeltaZxgogoLxcscL*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_636_1268 = Coupling(name = 'UVGC_636_1268',
                         value = 'FRCTdeltaZxgogoLxcscR*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_636_1269 = Coupling(name = 'UVGC_636_1269',
                         value = 'FRCTdeltaZxgogoLxdsdL*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_636_1270 = Coupling(name = 'UVGC_636_1270',
                         value = 'FRCTdeltaZxgogoLxdsdR*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_636_1271 = Coupling(name = 'UVGC_636_1271',
                         value = 'FRCTdeltaZxgogoLxgoG*G - (3*G**3*invFREps)/(8.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_636_1272 = Coupling(name = 'UVGC_636_1272',
                         value = 'FRCTdeltaZxgogoLxsssL*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_636_1273 = Coupling(name = 'UVGC_636_1273',
                         value = 'FRCTdeltaZxgogoLxsssR*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_636_1274 = Coupling(name = 'UVGC_636_1274',
                         value = 'FRCTdeltaZxgogoLxtstL*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_636_1275 = Coupling(name = 'UVGC_636_1275',
                         value = 'FRCTdeltaZxgogoLxtstR*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_636_1276 = Coupling(name = 'UVGC_636_1276',
                         value = 'FRCTdeltaZxgogoLxusuL*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_636_1277 = Coupling(name = 'UVGC_636_1277',
                         value = 'FRCTdeltaZxgogoLxusuR*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                         order = {'QCD':3})

UVGC_637_1278 = Coupling(name = 'UVGC_637_1278',
                         value = '(-11*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_637_1279 = Coupling(name = 'UVGC_637_1279',
                         value = '(FRCTdeltaxaSxgo*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxgo*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_637_1280 = Coupling(name = 'UVGC_637_1280',
                         value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsbL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1281 = Coupling(name = 'UVGC_637_1281',
                         value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsbR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1282 = Coupling(name = 'UVGC_637_1282',
                         value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxscL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1283 = Coupling(name = 'UVGC_637_1283',
                         value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxscR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1284 = Coupling(name = 'UVGC_637_1284',
                         value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsdL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1285 = Coupling(name = 'UVGC_637_1285',
                         value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsdR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1286 = Coupling(name = 'UVGC_637_1286',
                         value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxssL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1287 = Coupling(name = 'UVGC_637_1287',
                         value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxssR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1288 = Coupling(name = 'UVGC_637_1288',
                         value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxstL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1289 = Coupling(name = 'UVGC_637_1289',
                         value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxstR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1290 = Coupling(name = 'UVGC_637_1290',
                         value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsuL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1291 = Coupling(name = 'UVGC_637_1291',
                         value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsuR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1292 = Coupling(name = 'UVGC_637_1292',
                         value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxt*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_637_1293 = Coupling(name = 'UVGC_637_1293',
                         value = '(5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2) + (FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_637_1294 = Coupling(name = 'UVGC_637_1294',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) + (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_637_1295 = Coupling(name = 'UVGC_637_1295',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) + (FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_637_1296 = Coupling(name = 'UVGC_637_1296',
                         value = '(-11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**4)/(576.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_637_1297 = Coupling(name = 'UVGC_637_1297',
                         value = '(-11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) - (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_638_1298 = Coupling(name = 'UVGC_638_1298',
                         value = '(-5*complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_638_1299 = Coupling(name = 'UVGC_638_1299',
                         value = '-(FRCTdeltaxaSxgo*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxgo*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_638_1300 = Coupling(name = 'UVGC_638_1300',
                         value = '-(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsbL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1301 = Coupling(name = 'UVGC_638_1301',
                         value = '-(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsbR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1302 = Coupling(name = 'UVGC_638_1302',
                         value = '-(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxscL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1303 = Coupling(name = 'UVGC_638_1303',
                         value = '-(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxscR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1304 = Coupling(name = 'UVGC_638_1304',
                         value = '-(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsdL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1305 = Coupling(name = 'UVGC_638_1305',
                         value = '-(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsdR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1306 = Coupling(name = 'UVGC_638_1306',
                         value = '-(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxssL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1307 = Coupling(name = 'UVGC_638_1307',
                         value = '-(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxssR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1308 = Coupling(name = 'UVGC_638_1308',
                         value = '-(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxstL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1309 = Coupling(name = 'UVGC_638_1309',
                         value = '-(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxstR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1310 = Coupling(name = 'UVGC_638_1310',
                         value = '-(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsuL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1311 = Coupling(name = 'UVGC_638_1311',
                         value = '-(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsuR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1312 = Coupling(name = 'UVGC_638_1312',
                         value = '-(FRCTdeltaxaSxt*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxt*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_638_1313 = Coupling(name = 'UVGC_638_1313',
                         value = '-(complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2) - (FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_638_1314 = Coupling(name = 'UVGC_638_1314',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) - (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_638_1315 = Coupling(name = 'UVGC_638_1315',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) - (FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_638_1316 = Coupling(name = 'UVGC_638_1316',
                         value = '(-5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_638_1317 = Coupling(name = 'UVGC_638_1317',
                         value = '(-5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) + (3*complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)) - (3*complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_639_1318 = Coupling(name = 'UVGC_639_1318',
                         value = '(-11*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (11*complex(0,1)*G**4*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (11*complex(0,1)*G**4*invFREps*sw**4)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_639_1319 = Coupling(name = 'UVGC_639_1319',
                         value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsbL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_639_1320 = Coupling(name = 'UVGC_639_1320',
                         value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsbR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_639_1321 = Coupling(name = 'UVGC_639_1321',
                         value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxscL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_639_1322 = Coupling(name = 'UVGC_639_1322',
                         value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxscR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_639_1323 = Coupling(name = 'UVGC_639_1323',
                         value = '(FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_639_1324 = Coupling(name = 'UVGC_639_1324',
                         value = '(FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_639_1325 = Coupling(name = 'UVGC_639_1325',
                         value = '(FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_639_1326 = Coupling(name = 'UVGC_639_1326',
                         value = '(7*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**4)/(576.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_640_1327 = Coupling(name = 'UVGC_640_1327',
                         value = '(-5*complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_640_1328 = Coupling(name = 'UVGC_640_1328',
                         value = '-(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsbL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_640_1329 = Coupling(name = 'UVGC_640_1329',
                         value = '-(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsbR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_640_1330 = Coupling(name = 'UVGC_640_1330',
                         value = '-(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxscL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_640_1331 = Coupling(name = 'UVGC_640_1331',
                         value = '-(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxscR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_640_1332 = Coupling(name = 'UVGC_640_1332',
                         value = '-(FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_640_1333 = Coupling(name = 'UVGC_640_1333',
                         value = '-(FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_640_1334 = Coupling(name = 'UVGC_640_1334',
                         value = '-(FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_640_1335 = Coupling(name = 'UVGC_640_1335',
                         value = '(-23*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (23*complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (23*complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_641_1336 = Coupling(name = 'UVGC_641_1336',
                         value = '-(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_641_1337 = Coupling(name = 'UVGC_641_1337',
                         value = '-(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_641_1338 = Coupling(name = 'UVGC_641_1338',
                         value = '-(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_641_1339 = Coupling(name = 'UVGC_641_1339',
                         value = '-(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_641_1340 = Coupling(name = 'UVGC_641_1340',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_641_1341 = Coupling(name = 'UVGC_641_1341',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_642_1342 = Coupling(name = 'UVGC_642_1342',
                         value = '-(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_642_1343 = Coupling(name = 'UVGC_642_1343',
                         value = '-(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_642_1344 = Coupling(name = 'UVGC_642_1344',
                         value = '-(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_642_1345 = Coupling(name = 'UVGC_642_1345',
                         value = '-(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_642_1346 = Coupling(name = 'UVGC_642_1346',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*Rtau1x1)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_642_1347 = Coupling(name = 'UVGC_642_1347',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*Rtau1x1)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_643_1348 = Coupling(name = 'UVGC_643_1348',
                         value = '-(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_643_1349 = Coupling(name = 'UVGC_643_1349',
                         value = '-(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_643_1350 = Coupling(name = 'UVGC_643_1350',
                         value = '-(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_643_1351 = Coupling(name = 'UVGC_643_1351',
                         value = '-(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_643_1352 = Coupling(name = 'UVGC_643_1352',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*Rtau2x1)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_643_1353 = Coupling(name = 'UVGC_643_1353',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*Rtau2x1)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_644_1354 = Coupling(name = 'UVGC_644_1354',
                         value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsdL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_644_1355 = Coupling(name = 'UVGC_644_1355',
                         value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsdR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_644_1356 = Coupling(name = 'UVGC_644_1356',
                         value = '(FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_644_1357 = Coupling(name = 'UVGC_644_1357',
                         value = '(FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_644_1358 = Coupling(name = 'UVGC_644_1358',
                         value = '(FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_645_1359 = Coupling(name = 'UVGC_645_1359',
                         value = '-(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsdL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_645_1360 = Coupling(name = 'UVGC_645_1360',
                         value = '-(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsdR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_645_1361 = Coupling(name = 'UVGC_645_1361',
                         value = '-(FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_645_1362 = Coupling(name = 'UVGC_645_1362',
                         value = '-(FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_645_1363 = Coupling(name = 'UVGC_645_1363',
                         value = '-(FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_646_1364 = Coupling(name = 'UVGC_646_1364',
                         value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxssL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_646_1365 = Coupling(name = 'UVGC_646_1365',
                         value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxssR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_646_1366 = Coupling(name = 'UVGC_646_1366',
                         value = '(FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_646_1367 = Coupling(name = 'UVGC_646_1367',
                         value = '(FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_646_1368 = Coupling(name = 'UVGC_646_1368',
                         value = '(FRCTdeltaZxssLssLxgos*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssRssRxgos*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssLssLxgos*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssRssRxgos*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_647_1369 = Coupling(name = 'UVGC_647_1369',
                         value = '-(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxssL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_647_1370 = Coupling(name = 'UVGC_647_1370',
                         value = '-(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxssR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_647_1371 = Coupling(name = 'UVGC_647_1371',
                         value = '-(FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_647_1372 = Coupling(name = 'UVGC_647_1372',
                         value = '-(FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_647_1373 = Coupling(name = 'UVGC_647_1373',
                         value = '-(FRCTdeltaZxssLssLxgos*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssRssRxgos*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssLssLxgos*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssRssRxgos*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_648_1374 = Coupling(name = 'UVGC_648_1374',
                         value = '-(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_648_1375 = Coupling(name = 'UVGC_648_1375',
                         value = '-(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_648_1376 = Coupling(name = 'UVGC_648_1376',
                         value = '-(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_648_1377 = Coupling(name = 'UVGC_648_1377',
                         value = '-(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_649_1378 = Coupling(name = 'UVGC_649_1378',
                         value = '-(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_649_1379 = Coupling(name = 'UVGC_649_1379',
                         value = '-(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_649_1380 = Coupling(name = 'UVGC_649_1380',
                         value = '-(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_649_1381 = Coupling(name = 'UVGC_649_1381',
                         value = '-(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_650_1382 = Coupling(name = 'UVGC_650_1382',
                         value = '-(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_650_1383 = Coupling(name = 'UVGC_650_1383',
                         value = '-(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_650_1384 = Coupling(name = 'UVGC_650_1384',
                         value = '-(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_650_1385 = Coupling(name = 'UVGC_650_1385',
                         value = '-(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_651_1386 = Coupling(name = 'UVGC_651_1386',
                         value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxstL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_651_1387 = Coupling(name = 'UVGC_651_1387',
                         value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxstR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_651_1388 = Coupling(name = 'UVGC_651_1388',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) + (FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_651_1389 = Coupling(name = 'UVGC_651_1389',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) + (FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_651_1390 = Coupling(name = 'UVGC_651_1390',
                         value = '(5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2) + (FRCTdeltaZxstLstLxgot*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstRstRxgot*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstLxgot*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxgot*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_652_1391 = Coupling(name = 'UVGC_652_1391',
                         value = '-(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxstL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_652_1392 = Coupling(name = 'UVGC_652_1392',
                         value = '-(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxstR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_652_1393 = Coupling(name = 'UVGC_652_1393',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) - (FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_652_1394 = Coupling(name = 'UVGC_652_1394',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) - (FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_652_1395 = Coupling(name = 'UVGC_652_1395',
                         value = '-(complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2) - (FRCTdeltaZxstLstLxgot*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxgot*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstLstLxgot*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstRstRxgot*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_653_1396 = Coupling(name = 'UVGC_653_1396',
                         value = '-(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_653_1397 = Coupling(name = 'UVGC_653_1397',
                         value = '-(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_653_1398 = Coupling(name = 'UVGC_653_1398',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_653_1399 = Coupling(name = 'UVGC_653_1399',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_654_1400 = Coupling(name = 'UVGC_654_1400',
                         value = '-(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_654_1401 = Coupling(name = 'UVGC_654_1401',
                         value = '-(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_654_1402 = Coupling(name = 'UVGC_654_1402',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_654_1403 = Coupling(name = 'UVGC_654_1403',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*Rtau1x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_655_1404 = Coupling(name = 'UVGC_655_1404',
                         value = '-(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_655_1405 = Coupling(name = 'UVGC_655_1405',
                         value = '-(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_655_1406 = Coupling(name = 'UVGC_655_1406',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_655_1407 = Coupling(name = 'UVGC_655_1407',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1)*Rtau2x1)/(4.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_656_1408 = Coupling(name = 'UVGC_656_1408',
                         value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsuL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_656_1409 = Coupling(name = 'UVGC_656_1409',
                         value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsuR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_656_1410 = Coupling(name = 'UVGC_656_1410',
                         value = '(FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_656_1411 = Coupling(name = 'UVGC_656_1411',
                         value = '(FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_656_1412 = Coupling(name = 'UVGC_656_1412',
                         value = '(FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_657_1413 = Coupling(name = 'UVGC_657_1413',
                         value = '-(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsuL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_657_1414 = Coupling(name = 'UVGC_657_1414',
                         value = '-(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsuR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_657_1415 = Coupling(name = 'UVGC_657_1415',
                         value = '-(FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_657_1416 = Coupling(name = 'UVGC_657_1416',
                         value = '-(FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_657_1417 = Coupling(name = 'UVGC_657_1417',
                         value = '-(FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1418 = Coupling(name = 'UVGC_658_1418',
                         value = '-(FRCTdeltaxaSxgo*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxgo*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_658_1419 = Coupling(name = 'UVGC_658_1419',
                         value = '-(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsbL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1420 = Coupling(name = 'UVGC_658_1420',
                         value = '-(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsbR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1421 = Coupling(name = 'UVGC_658_1421',
                         value = '-(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxscL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1422 = Coupling(name = 'UVGC_658_1422',
                         value = '-(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxscR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1423 = Coupling(name = 'UVGC_658_1423',
                         value = '-(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsdL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1424 = Coupling(name = 'UVGC_658_1424',
                         value = '-(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsdR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1425 = Coupling(name = 'UVGC_658_1425',
                         value = '-(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxssL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1426 = Coupling(name = 'UVGC_658_1426',
                         value = '-(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxssR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1427 = Coupling(name = 'UVGC_658_1427',
                         value = '-(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxstL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1428 = Coupling(name = 'UVGC_658_1428',
                         value = '-(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxstR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1429 = Coupling(name = 'UVGC_658_1429',
                         value = '-(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsuL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1430 = Coupling(name = 'UVGC_658_1430',
                         value = '-(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsuR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_658_1431 = Coupling(name = 'UVGC_658_1431',
                         value = '-(FRCTdeltaxaSxt*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxt*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_658_1432 = Coupling(name = 'UVGC_658_1432',
                         value = '-(FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_658_1433 = Coupling(name = 'UVGC_658_1433',
                         value = '-(FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_658_1434 = Coupling(name = 'UVGC_658_1434',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) - (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_658_1435 = Coupling(name = 'UVGC_658_1435',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) - (FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_658_1436 = Coupling(name = 'UVGC_658_1436',
                         value = '(complex(0,1)*G**4*invFREps)/(72.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_658_1437 = Coupling(name = 'UVGC_658_1437',
                         value = '(-11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_659_1438 = Coupling(name = 'UVGC_659_1438',
                         value = '-(FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_659_1439 = Coupling(name = 'UVGC_659_1439',
                         value = '-(FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_659_1440 = Coupling(name = 'UVGC_659_1440',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) - (FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_659_1441 = Coupling(name = 'UVGC_659_1441',
                         value = '-(complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_660_1442 = Coupling(name = 'UVGC_660_1442',
                         value = '(FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_660_1443 = Coupling(name = 'UVGC_660_1443',
                         value = '(FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_660_1444 = Coupling(name = 'UVGC_660_1444',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) + (FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_660_1445 = Coupling(name = 'UVGC_660_1445',
                         value = '(5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_661_1446 = Coupling(name = 'UVGC_661_1446',
                         value = '(FRCTdeltaxaSxgo*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxgo*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_661_1447 = Coupling(name = 'UVGC_661_1447',
                         value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsbL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_661_1448 = Coupling(name = 'UVGC_661_1448',
                         value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsbR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_661_1449 = Coupling(name = 'UVGC_661_1449',
                         value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxscL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_661_1450 = Coupling(name = 'UVGC_661_1450',
                         value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxscR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_661_1451 = Coupling(name = 'UVGC_661_1451',
                         value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsdL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_661_1452 = Coupling(name = 'UVGC_661_1452',
                         value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsdR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_661_1453 = Coupling(name = 'UVGC_661_1453',
                         value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxssL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_661_1454 = Coupling(name = 'UVGC_661_1454',
                         value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxssR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_661_1455 = Coupling(name = 'UVGC_661_1455',
                         value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxstL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_661_1456 = Coupling(name = 'UVGC_661_1456',
                         value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxstR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_661_1457 = Coupling(name = 'UVGC_661_1457',
                         value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsuL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_661_1458 = Coupling(name = 'UVGC_661_1458',
                         value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsuR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_661_1459 = Coupling(name = 'UVGC_661_1459',
                         value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxt*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_661_1460 = Coupling(name = 'UVGC_661_1460',
                         value = '(FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbLsbLxgob*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_661_1461 = Coupling(name = 'UVGC_661_1461',
                         value = '(FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_661_1462 = Coupling(name = 'UVGC_661_1462',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) + (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_661_1463 = Coupling(name = 'UVGC_661_1463',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) + (FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_661_1464 = Coupling(name = 'UVGC_661_1464',
                         value = '(7*complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                         order = {'QCD':4})

UVGC_661_1465 = Coupling(name = 'UVGC_661_1465',
                         value = '(-5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) - (3*complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)) + (3*complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_662_1466 = Coupling(name = 'UVGC_662_1466',
                         value = '-(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsbL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_662_1467 = Coupling(name = 'UVGC_662_1467',
                         value = '-(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsbR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_662_1468 = Coupling(name = 'UVGC_662_1468',
                         value = '-(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxscL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_662_1469 = Coupling(name = 'UVGC_662_1469',
                         value = '-(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxscR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_662_1470 = Coupling(name = 'UVGC_662_1470',
                         value = '-(FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_662_1471 = Coupling(name = 'UVGC_662_1471',
                         value = '-(FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_662_1472 = Coupling(name = 'UVGC_662_1472',
                         value = '-(FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_662_1473 = Coupling(name = 'UVGC_662_1473',
                         value = '-(FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_662_1474 = Coupling(name = 'UVGC_662_1474',
                         value = '(complex(0,1)*G**4*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_662_1475 = Coupling(name = 'UVGC_662_1475',
                         value = '(-29*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)**2) + (29*complex(0,1)*G**4*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (29*complex(0,1)*G**4*invFREps*sw**4)/(576.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_663_1476 = Coupling(name = 'UVGC_663_1476',
                         value = '-(FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_663_1477 = Coupling(name = 'UVGC_663_1477',
                         value = '-(FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_663_1478 = Coupling(name = 'UVGC_663_1478',
                         value = '-(FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_663_1479 = Coupling(name = 'UVGC_663_1479',
                         value = '-(complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_664_1480 = Coupling(name = 'UVGC_664_1480',
                         value = '(FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_664_1481 = Coupling(name = 'UVGC_664_1481',
                         value = '(FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscLscLxgoc*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_664_1482 = Coupling(name = 'UVGC_664_1482',
                         value = '(FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_664_1483 = Coupling(name = 'UVGC_664_1483',
                         value = '(5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_665_1484 = Coupling(name = 'UVGC_665_1484',
                         value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsbL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_665_1485 = Coupling(name = 'UVGC_665_1485',
                         value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsbR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_665_1486 = Coupling(name = 'UVGC_665_1486',
                         value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxscL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_665_1487 = Coupling(name = 'UVGC_665_1487',
                         value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxscR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_665_1488 = Coupling(name = 'UVGC_665_1488',
                         value = '(FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbRsbRxgob*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_665_1489 = Coupling(name = 'UVGC_665_1489',
                         value = '(FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscRscRxgoc*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_665_1490 = Coupling(name = 'UVGC_665_1490',
                         value = '(FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_665_1491 = Coupling(name = 'UVGC_665_1491',
                         value = '(FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_665_1492 = Coupling(name = 'UVGC_665_1492',
                         value = '(7*complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**4*invFREps*sw**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**4*invFREps*sw**4)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_665_1493 = Coupling(name = 'UVGC_665_1493',
                         value = '(13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)**2) - (13*complex(0,1)*G**4*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (13*complex(0,1)*G**4*invFREps*sw**4)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_666_1494 = Coupling(name = 'UVGC_666_1494',
                         value = '-(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsdL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_666_1495 = Coupling(name = 'UVGC_666_1495',
                         value = '-(FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_666_1496 = Coupling(name = 'UVGC_666_1496',
                         value = '-(FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_666_1497 = Coupling(name = 'UVGC_666_1497',
                         value = '-(FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_667_1498 = Coupling(name = 'UVGC_667_1498',
                         value = '-(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsdR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_667_1499 = Coupling(name = 'UVGC_667_1499',
                         value = '-(FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_667_1500 = Coupling(name = 'UVGC_667_1500',
                         value = '-(FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_668_1501 = Coupling(name = 'UVGC_668_1501',
                         value = '-(FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_669_1502 = Coupling(name = 'UVGC_669_1502',
                         value = '-(FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_670_1503 = Coupling(name = 'UVGC_670_1503',
                         value = '(FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_671_1504 = Coupling(name = 'UVGC_671_1504',
                         value = '(FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_672_1505 = Coupling(name = 'UVGC_672_1505',
                         value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsdL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_672_1506 = Coupling(name = 'UVGC_672_1506',
                         value = '(FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsdLsdLxgod*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_672_1507 = Coupling(name = 'UVGC_672_1507',
                         value = '(FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_672_1508 = Coupling(name = 'UVGC_672_1508',
                         value = '(FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_673_1509 = Coupling(name = 'UVGC_673_1509',
                         value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsdR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_673_1510 = Coupling(name = 'UVGC_673_1510',
                         value = '(FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsdRsdRxgod*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_673_1511 = Coupling(name = 'UVGC_673_1511',
                         value = '(FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_674_1512 = Coupling(name = 'UVGC_674_1512',
                         value = '-(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxssL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_674_1513 = Coupling(name = 'UVGC_674_1513',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) - (FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_674_1514 = Coupling(name = 'UVGC_674_1514',
                         value = '-(FRCTdeltaZxssLssLxgos*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssLssLxgos*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_675_1515 = Coupling(name = 'UVGC_675_1515',
                         value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxssL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_675_1516 = Coupling(name = 'UVGC_675_1516',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) + (FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_675_1517 = Coupling(name = 'UVGC_675_1517',
                         value = '(FRCTdeltaZxssLssLxgos*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssLssLxgos*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_676_1518 = Coupling(name = 'UVGC_676_1518',
                         value = '(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(2.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_676_1519 = Coupling(name = 'UVGC_676_1519',
                         value = '(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_676_1520 = Coupling(name = 'UVGC_676_1520',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_676_1521 = Coupling(name = 'UVGC_676_1521',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_677_1522 = Coupling(name = 'UVGC_677_1522',
                         value = '(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_677_1523 = Coupling(name = 'UVGC_677_1523',
                         value = '(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(2.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_678_1524 = Coupling(name = 'UVGC_678_1524',
                         value = '(ee*FRCTdeltaZxscLscLxgoc*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_678_1525 = Coupling(name = 'UVGC_678_1525',
                         value = '(ee*FRCTdeltaZxscLscLxGscL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_678_1526 = Coupling(name = 'UVGC_678_1526',
                         value = '(ee*FRCTdeltaZxssLssLxGssL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_678_1527 = Coupling(name = 'UVGC_678_1527',
                         value = '(ee*FRCTdeltaZxssLssLxgos*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_678_1528 = Coupling(name = 'UVGC_678_1528',
                         value = '-(ee*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_678_1529 = Coupling(name = 'UVGC_678_1529',
                         value = '-(ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_679_1530 = Coupling(name = 'UVGC_679_1530',
                         value = '-(ee*FRCTdeltaZxscLscLxgoc*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_679_1531 = Coupling(name = 'UVGC_679_1531',
                         value = '-(ee*FRCTdeltaZxscLscLxGscL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_679_1532 = Coupling(name = 'UVGC_679_1532',
                         value = '-(ee*FRCTdeltaZxssLssLxGssL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_679_1533 = Coupling(name = 'UVGC_679_1533',
                         value = '-(ee*FRCTdeltaZxssLssLxgos*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_679_1534 = Coupling(name = 'UVGC_679_1534',
                         value = '(ee*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_679_1535 = Coupling(name = 'UVGC_679_1535',
                         value = '(ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_680_1536 = Coupling(name = 'UVGC_680_1536',
                         value = '(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(6.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_680_1537 = Coupling(name = 'UVGC_680_1537',
                         value = '(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(6.*sw*cmath.sqrt(2)) + (ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_680_1538 = Coupling(name = 'UVGC_680_1538',
                         value = '(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(6.*sw*cmath.sqrt(2)) - (ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_680_1539 = Coupling(name = 'UVGC_680_1539',
                         value = '(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(6.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_680_1540 = Coupling(name = 'UVGC_680_1540',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_680_1541 = Coupling(name = 'UVGC_680_1541',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_681_1542 = Coupling(name = 'UVGC_681_1542',
                         value = '(ee*FRCTdeltaZxGGxb*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1543 = Coupling(name = 'UVGC_681_1543',
                         value = '(ee*FRCTdeltaZxGGxc*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1544 = Coupling(name = 'UVGC_681_1544',
                         value = '(ee*FRCTdeltaZxGGxd*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1545 = Coupling(name = 'UVGC_681_1545',
                         value = '(ee*FRCTdeltaZxGGxG*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1546 = Coupling(name = 'UVGC_681_1546',
                         value = '(ee*FRCTdeltaZxGGxghG*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1547 = Coupling(name = 'UVGC_681_1547',
                         value = '(ee*FRCTdeltaxaSxgo*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxgo*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1548 = Coupling(name = 'UVGC_681_1548',
                         value = '(ee*FRCTdeltaZxGGxs*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1549 = Coupling(name = 'UVGC_681_1549',
                         value = '(ee*FRCTdeltaxaSxsbL*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxsbL*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1550 = Coupling(name = 'UVGC_681_1550',
                         value = '(ee*FRCTdeltaxaSxsbR*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxsbR*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1551 = Coupling(name = 'UVGC_681_1551',
                         value = '(ee*FRCTdeltaxaSxscL*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxscL*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1552 = Coupling(name = 'UVGC_681_1552',
                         value = '(ee*FRCTdeltaxaSxscR*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxscR*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1553 = Coupling(name = 'UVGC_681_1553',
                         value = '(ee*FRCTdeltaxaSxsdL*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxsdL*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1554 = Coupling(name = 'UVGC_681_1554',
                         value = '(ee*FRCTdeltaxaSxsdR*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxsdR*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1555 = Coupling(name = 'UVGC_681_1555',
                         value = '(ee*FRCTdeltaxaSxssL*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxssL*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1556 = Coupling(name = 'UVGC_681_1556',
                         value = '(ee*FRCTdeltaxaSxssR*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxssR*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1557 = Coupling(name = 'UVGC_681_1557',
                         value = '(ee*FRCTdeltaxaSxstL*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxstL*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1558 = Coupling(name = 'UVGC_681_1558',
                         value = '(ee*FRCTdeltaxaSxstR*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxstR*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1559 = Coupling(name = 'UVGC_681_1559',
                         value = '(ee*FRCTdeltaxaSxsuL*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxsuL*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1560 = Coupling(name = 'UVGC_681_1560',
                         value = '(ee*FRCTdeltaxaSxsuR*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxsuR*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1561 = Coupling(name = 'UVGC_681_1561',
                         value = '(ee*FRCTdeltaxaSxt*complex(0,1)*G)/(aS*sw*cmath.sqrt(2)) + (ee*FRCTdeltaZxGGxt*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1562 = Coupling(name = 'UVGC_681_1562',
                         value = '(ee*FRCTdeltaZxGGxu*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1563 = Coupling(name = 'UVGC_681_1563',
                         value = '(ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1564 = Coupling(name = 'UVGC_681_1564',
                         value = '(ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*G)/(sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1565 = Coupling(name = 'UVGC_681_1565',
                         value = '(ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*G)/(sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1566 = Coupling(name = 'UVGC_681_1566',
                         value = '(ee*FRCTdeltaZxssLssLxgos*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1567 = Coupling(name = 'UVGC_681_1567',
                         value = '-(ee*complex(0,1)*G**3*invFREps)/(3.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_681_1568 = Coupling(name = 'UVGC_681_1568',
                         value = '(-7*ee*complex(0,1)*G**3*invFREps)/(96.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_682_1569 = Coupling(name = 'UVGC_682_1569',
                         value = '-(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxssR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_682_1570 = Coupling(name = 'UVGC_682_1570',
                         value = '-(FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_682_1571 = Coupling(name = 'UVGC_682_1571',
                         value = '-(FRCTdeltaZxssRssRxgos*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssRssRxgos*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_683_1572 = Coupling(name = 'UVGC_683_1572',
                         value = '-(FRCTdeltaZxssRssRxgos*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssRssRxgos*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_684_1573 = Coupling(name = 'UVGC_684_1573',
                         value = '-(FRCTdeltaZxssLssLxgos*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssLssLxgos*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_685_1574 = Coupling(name = 'UVGC_685_1574',
                         value = '(FRCTdeltaZxssRssRxgos*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssRssRxgos*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_686_1575 = Coupling(name = 'UVGC_686_1575',
                         value = '(FRCTdeltaZxssLssLxgos*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssLssLxgos*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_687_1576 = Coupling(name = 'UVGC_687_1576',
                         value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxssR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_687_1577 = Coupling(name = 'UVGC_687_1577',
                         value = '(FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_687_1578 = Coupling(name = 'UVGC_687_1578',
                         value = '(FRCTdeltaZxssRssRxgos*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssRssRxgos*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_688_1579 = Coupling(name = 'UVGC_688_1579',
                         value = '(cw*ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_688_1580 = Coupling(name = 'UVGC_688_1580',
                         value = '(cw*ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_688_1581 = Coupling(name = 'UVGC_688_1581',
                         value = '(cw*ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_688_1582 = Coupling(name = 'UVGC_688_1582',
                         value = '(cw*ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_688_1583 = Coupling(name = 'UVGC_688_1583',
                         value = '-(cw*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_688_1584 = Coupling(name = 'UVGC_688_1584',
                         value = '-(cw*ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_689_1585 = Coupling(name = 'UVGC_689_1585',
                         value = '-(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxstL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_689_1586 = Coupling(name = 'UVGC_689_1586',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) - (FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_689_1587 = Coupling(name = 'UVGC_689_1587',
                         value = '-(FRCTdeltaZxstLstLxgot*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstLstLxgot*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_690_1588 = Coupling(name = 'UVGC_690_1588',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) - (FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_690_1589 = Coupling(name = 'UVGC_690_1589',
                         value = '-(FRCTdeltaZxstRstRxgot*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstRstRxgot*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_691_1590 = Coupling(name = 'UVGC_691_1590',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) + (FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_691_1591 = Coupling(name = 'UVGC_691_1591',
                         value = '(FRCTdeltaZxstRstRxgot*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxgot*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_692_1592 = Coupling(name = 'UVGC_692_1592',
                         value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxstL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_692_1593 = Coupling(name = 'UVGC_692_1593',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) + (FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_692_1594 = Coupling(name = 'UVGC_692_1594',
                         value = '(FRCTdeltaZxstLstLxgot*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstLxgot*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_693_1595 = Coupling(name = 'UVGC_693_1595',
                         value = '-(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxstR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_693_1596 = Coupling(name = 'UVGC_693_1596',
                         value = '-(FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_693_1597 = Coupling(name = 'UVGC_693_1597',
                         value = '-(FRCTdeltaZxstRstRxgot*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstRstRxgot*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_694_1598 = Coupling(name = 'UVGC_694_1598',
                         value = '-(FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_694_1599 = Coupling(name = 'UVGC_694_1599',
                         value = '-(FRCTdeltaZxstLstLxgot*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstLstLxgot*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_695_1600 = Coupling(name = 'UVGC_695_1600',
                         value = '(FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_695_1601 = Coupling(name = 'UVGC_695_1601',
                         value = '(FRCTdeltaZxstLstLxgot*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstLxgot*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_696_1602 = Coupling(name = 'UVGC_696_1602',
                         value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxstR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_696_1603 = Coupling(name = 'UVGC_696_1603',
                         value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_696_1604 = Coupling(name = 'UVGC_696_1604',
                         value = '(FRCTdeltaZxstRstRxgot*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxgot*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_697_1605 = Coupling(name = 'UVGC_697_1605',
                         value = '-(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_699_1606 = Coupling(name = 'UVGC_699_1606',
                         value = '-(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_701_1607 = Coupling(name = 'UVGC_701_1607',
                         value = '(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_703_1608 = Coupling(name = 'UVGC_703_1608',
                         value = '(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_705_1609 = Coupling(name = 'UVGC_705_1609',
                         value = '-(FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_711_1610 = Coupling(name = 'UVGC_711_1610',
                         value = '(FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_721_1611 = Coupling(name = 'UVGC_721_1611',
                         value = '(ee*FRCTdeltaZxccLxcG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_721_1612 = Coupling(name = 'UVGC_721_1612',
                         value = '(ee*FRCTdeltaZxssLxsG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_721_1613 = Coupling(name = 'UVGC_721_1613',
                         value = '(ee*FRCTdeltaZxccLxgoscL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_721_1614 = Coupling(name = 'UVGC_721_1614',
                         value = '(ee*FRCTdeltaZxssLxgossL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_726_1615 = Coupling(name = 'UVGC_726_1615',
                         value = '(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(2.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_726_1616 = Coupling(name = 'UVGC_726_1616',
                         value = '(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_727_1617 = Coupling(name = 'UVGC_727_1617',
                         value = '(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_727_1618 = Coupling(name = 'UVGC_727_1618',
                         value = '(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(2.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_728_1619 = Coupling(name = 'UVGC_728_1619',
                         value = '(ee*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_728_1620 = Coupling(name = 'UVGC_728_1620',
                         value = '(ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_728_1621 = Coupling(name = 'UVGC_728_1621',
                         value = '(ee*FRCTdeltaZxstLstLxGstL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_728_1622 = Coupling(name = 'UVGC_728_1622',
                         value = '(ee*FRCTdeltaZxstLstLxgot*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_729_1623 = Coupling(name = 'UVGC_729_1623',
                         value = '-(ee*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_729_1624 = Coupling(name = 'UVGC_729_1624',
                         value = '-(ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_729_1625 = Coupling(name = 'UVGC_729_1625',
                         value = '-(ee*FRCTdeltaZxstLstLxGstL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_729_1626 = Coupling(name = 'UVGC_729_1626',
                         value = '-(ee*FRCTdeltaZxstLstLxgot*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_730_1627 = Coupling(name = 'UVGC_730_1627',
                         value = '(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(6.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_730_1628 = Coupling(name = 'UVGC_730_1628',
                         value = '(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(6.*sw*cmath.sqrt(2)) - (ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_730_1629 = Coupling(name = 'UVGC_730_1629',
                         value = '(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(6.*sw*cmath.sqrt(2)) + (ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_730_1630 = Coupling(name = 'UVGC_730_1630',
                         value = '(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(6.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_731_1631 = Coupling(name = 'UVGC_731_1631',
                         value = '(ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_731_1632 = Coupling(name = 'UVGC_731_1632',
                         value = '(ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G)/(sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_731_1633 = Coupling(name = 'UVGC_731_1633',
                         value = '(ee*FRCTdeltaZxstLstLxGstL*complex(0,1)*G)/(sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_731_1634 = Coupling(name = 'UVGC_731_1634',
                         value = '(ee*FRCTdeltaZxstLstLxgot*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_736_1635 = Coupling(name = 'UVGC_736_1635',
                         value = '(cw*ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_736_1636 = Coupling(name = 'UVGC_736_1636',
                         value = '(cw*ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_736_1637 = Coupling(name = 'UVGC_736_1637',
                         value = '(cw*ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_736_1638 = Coupling(name = 'UVGC_736_1638',
                         value = '(cw*ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_737_1639 = Coupling(name = 'UVGC_737_1639',
                         value = '-(FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_740_1640 = Coupling(name = 'UVGC_740_1640',
                         value = '-(FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_742_1641 = Coupling(name = 'UVGC_742_1641',
                         value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (5*complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (5*complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_743_1642 = Coupling(name = 'UVGC_743_1642',
                         value = '(FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_746_1643 = Coupling(name = 'UVGC_746_1643',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) - (FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_747_1644 = Coupling(name = 'UVGC_747_1644',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) + (FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_753_1645 = Coupling(name = 'UVGC_753_1645',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) - (FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_753_1646 = Coupling(name = 'UVGC_753_1646',
                         value = '-(FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_754_1647 = Coupling(name = 'UVGC_754_1647',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) + (FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_754_1648 = Coupling(name = 'UVGC_754_1648',
                         value = '(FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_755_1649 = Coupling(name = 'UVGC_755_1649',
                         value = '-(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsuL*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_755_1650 = Coupling(name = 'UVGC_755_1650',
                         value = '-(FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_755_1651 = Coupling(name = 'UVGC_755_1651',
                         value = '-(FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_756_1652 = Coupling(name = 'UVGC_756_1652',
                         value = '-(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (FRCTdeltaxaSxsuR*complex(0,1)*G**2*sw**2)/(6.*aS*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_756_1653 = Coupling(name = 'UVGC_756_1653',
                         value = '-(FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_756_1654 = Coupling(name = 'UVGC_756_1654',
                         value = '-(FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_757_1655 = Coupling(name = 'UVGC_757_1655',
                         value = '-(FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_758_1656 = Coupling(name = 'UVGC_758_1656',
                         value = '(FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_759_1657 = Coupling(name = 'UVGC_759_1657',
                         value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsuL*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_759_1658 = Coupling(name = 'UVGC_759_1658',
                         value = '(FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_759_1659 = Coupling(name = 'UVGC_759_1659',
                         value = '(FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuLsuLxgou*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_760_1660 = Coupling(name = 'UVGC_760_1660',
                         value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (FRCTdeltaxaSxsuR*complex(0,1)*G**2*sw**2)/(2.*aS*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**4)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_760_1661 = Coupling(name = 'UVGC_760_1661',
                         value = '(FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**4*invFREps*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**4*invFREps*sw**4)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':4})

UVGC_760_1662 = Coupling(name = 'UVGC_760_1662',
                         value = '(FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuRsuRxgou*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':4})

UVGC_761_1663 = Coupling(name = 'UVGC_761_1663',
                         value = '(11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2) - (FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2)/(6.*(-1 + sw)*(1 + sw)) + (FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2*sw**2)/(6.*(-1 + sw)*(1 + sw)) - (complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**4*invFREps*sw**2)/(576.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_762_1664 = Coupling(name = 'UVGC_762_1664',
                         value = '(5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2) + (FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2*sw**2)/(2.*(-1 + sw)*(1 + sw)) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**4*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':4})

UVGC_769_1665 = Coupling(name = 'UVGC_769_1665',
                         value = '(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(2.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_769_1666 = Coupling(name = 'UVGC_769_1666',
                         value = '(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_770_1667 = Coupling(name = 'UVGC_770_1667',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_770_1668 = Coupling(name = 'UVGC_770_1668',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(2.*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_771_1669 = Coupling(name = 'UVGC_771_1669',
                         value = '(ee*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_771_1670 = Coupling(name = 'UVGC_771_1670',
                         value = '(ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_771_1671 = Coupling(name = 'UVGC_771_1671',
                         value = '(ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_771_1672 = Coupling(name = 'UVGC_771_1672',
                         value = '(ee*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_772_1673 = Coupling(name = 'UVGC_772_1673',
                         value = '-(ee*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_772_1674 = Coupling(name = 'UVGC_772_1674',
                         value = '-(ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_772_1675 = Coupling(name = 'UVGC_772_1675',
                         value = '-(ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_772_1676 = Coupling(name = 'UVGC_772_1676',
                         value = '-(ee*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_773_1677 = Coupling(name = 'UVGC_773_1677',
                         value = '(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(6.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_773_1678 = Coupling(name = 'UVGC_773_1678',
                         value = '(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(6.*sw*cmath.sqrt(2)) - (ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_773_1679 = Coupling(name = 'UVGC_773_1679',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(6.*sw*cmath.sqrt(2)) + (ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_773_1680 = Coupling(name = 'UVGC_773_1680',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(6.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_774_1681 = Coupling(name = 'UVGC_774_1681',
                         value = '(ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_774_1682 = Coupling(name = 'UVGC_774_1682',
                         value = '(ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G)/(sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_774_1683 = Coupling(name = 'UVGC_774_1683',
                         value = '(ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G)/(sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**3*invFREps)/(64.*cmath.pi**2*sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_774_1684 = Coupling(name = 'UVGC_774_1684',
                         value = '(ee*FRCTdeltaZxsuLsuLxgou*complex(0,1)*G)/(sw*cmath.sqrt(2))',
                         order = {'QCD':3,'QED':1})

UVGC_783_1685 = Coupling(name = 'UVGC_783_1685',
                         value = '(cw*ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_783_1686 = Coupling(name = 'UVGC_783_1686',
                         value = '(cw*ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_783_1687 = Coupling(name = 'UVGC_783_1687',
                         value = '(cw*ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (cw*ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) - (cw*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_783_1688 = Coupling(name = 'UVGC_783_1688',
                         value = '(cw*ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(6.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':2})

UVGC_800_1689 = Coupling(name = 'UVGC_800_1689',
                         value = '(ee*FRCTdeltaZxbbLxbG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_800_1690 = Coupling(name = 'UVGC_800_1690',
                         value = '(ee*FRCTdeltaZxttLxtG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_800_1691 = Coupling(name = 'UVGC_800_1691',
                         value = '(ee*FRCTdeltaZxbbLxgosbL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_800_1692 = Coupling(name = 'UVGC_800_1692',
                         value = '(ee*FRCTdeltaZxttLxgostL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_800_1693 = Coupling(name = 'UVGC_800_1693',
                         value = '(ee*FRCTdeltaZxttLxgostR*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_801_1694 = Coupling(name = 'UVGC_801_1694',
                         value = '(ee*FRCTdeltaZxddLxdG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_801_1695 = Coupling(name = 'UVGC_801_1695',
                         value = '(ee*FRCTdeltaZxuuLxuG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_801_1696 = Coupling(name = 'UVGC_801_1696',
                         value = '(ee*FRCTdeltaZxddLxgosdL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_801_1697 = Coupling(name = 'UVGC_801_1697',
                         value = '(ee*FRCTdeltaZxuuLxgosuL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_802_1698 = Coupling(name = 'UVGC_802_1698',
                         value = '(119*ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)**2) - (7*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1699 = Coupling(name = 'UVGC_802_1699',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1700 = Coupling(name = 'UVGC_802_1700',
                         value = '(complex(0,1)*G**2*invFREps*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1701 = Coupling(name = 'UVGC_802_1701',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1702 = Coupling(name = 'UVGC_802_1702',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1703 = Coupling(name = 'UVGC_802_1703',
                         value = '(-4*ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_802_1704 = Coupling(name = 'UVGC_802_1704',
                         value = '(-4*ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw)) - (17*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1705 = Coupling(name = 'UVGC_802_1705',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1706 = Coupling(name = 'UVGC_802_1706',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1707 = Coupling(name = 'UVGC_802_1707',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1708 = Coupling(name = 'UVGC_802_1708',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1709 = Coupling(name = 'UVGC_802_1709',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2*sw**2)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1710 = Coupling(name = 'UVGC_802_1710',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2*sw**2)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1711 = Coupling(name = 'UVGC_802_1711',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2*sw**2)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1712 = Coupling(name = 'UVGC_802_1712',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2*sw**2)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_802_1713 = Coupling(name = 'UVGC_802_1713',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_803_1714 = Coupling(name = 'UVGC_803_1714',
                         value = '(-7*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_803_1715 = Coupling(name = 'UVGC_803_1715',
                         value = '(2*ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_803_1716 = Coupling(name = 'UVGC_803_1716',
                         value = '(2*ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_803_1717 = Coupling(name = 'UVGC_803_1717',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_803_1718 = Coupling(name = 'UVGC_803_1718',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_803_1719 = Coupling(name = 'UVGC_803_1719',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_803_1720 = Coupling(name = 'UVGC_803_1720',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_803_1721 = Coupling(name = 'UVGC_803_1721',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_803_1722 = Coupling(name = 'UVGC_803_1722',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_803_1723 = Coupling(name = 'UVGC_803_1723',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_804_1724 = Coupling(name = 'UVGC_804_1724',
                         value = '(-4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (8*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (4*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_804_1725 = Coupling(name = 'UVGC_804_1725',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_804_1726 = Coupling(name = 'UVGC_804_1726',
                         value = '(-4*ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_804_1727 = Coupling(name = 'UVGC_804_1727',
                         value = '(-4*ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw)) - (17*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_804_1728 = Coupling(name = 'UVGC_804_1728',
                         value = '(2*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (4*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_804_1729 = Coupling(name = 'UVGC_804_1729',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_804_1730 = Coupling(name = 'UVGC_804_1730',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_804_1731 = Coupling(name = 'UVGC_804_1731',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2*sw**2)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_804_1732 = Coupling(name = 'UVGC_804_1732',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2*sw**2)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_804_1733 = Coupling(name = 'UVGC_804_1733',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2*sw**2)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_804_1734 = Coupling(name = 'UVGC_804_1734',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(6.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2*sw**2)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_804_1735 = Coupling(name = 'UVGC_804_1735',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_805_1736 = Coupling(name = 'UVGC_805_1736',
                         value = '(-7*ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_805_1737 = Coupling(name = 'UVGC_805_1737',
                         value = '(8*ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_805_1738 = Coupling(name = 'UVGC_805_1738',
                         value = '(8*ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (2*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_805_1739 = Coupling(name = 'UVGC_805_1739',
                         value = '(-4*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_805_1740 = Coupling(name = 'UVGC_805_1740',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_805_1741 = Coupling(name = 'UVGC_805_1741',
                         value = '(4*cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_805_1742 = Coupling(name = 'UVGC_805_1742',
                         value = '(4*cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_805_1743 = Coupling(name = 'UVGC_805_1743',
                         value = '(4*cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_805_1744 = Coupling(name = 'UVGC_805_1744',
                         value = '(4*cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_805_1745 = Coupling(name = 'UVGC_805_1745',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_806_1746 = Coupling(name = 'UVGC_806_1746',
                         value = '(-4*ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_806_1747 = Coupling(name = 'UVGC_806_1747',
                         value = '(-4*ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw)) - (17*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_807_1748 = Coupling(name = 'UVGC_807_1748',
                         value = '(2*ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_807_1749 = Coupling(name = 'UVGC_807_1749',
                         value = '(2*ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_808_1750 = Coupling(name = 'UVGC_808_1750',
                         value = '(-4*ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw)) - (17*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_808_1751 = Coupling(name = 'UVGC_808_1751',
                         value = '(-4*ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_809_1752 = Coupling(name = 'UVGC_809_1752',
                         value = '(2*ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_809_1753 = Coupling(name = 'UVGC_809_1753',
                         value = '(2*ee**2*FRCTdeltaZxssRssRxgos*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_810_1754 = Coupling(name = 'UVGC_810_1754',
                         value = '(-4*ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw)) - (17*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_810_1755 = Coupling(name = 'UVGC_810_1755',
                         value = '(-4*ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_811_1756 = Coupling(name = 'UVGC_811_1756',
                         value = '(8*ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (2*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_811_1757 = Coupling(name = 'UVGC_811_1757',
                         value = '(8*ee**2*FRCTdeltaZxstRstRxgot*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_812_1758 = Coupling(name = 'UVGC_812_1758',
                         value = '(-4*ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw)) - (17*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_812_1759 = Coupling(name = 'UVGC_812_1759',
                         value = '(-4*ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_813_1760 = Coupling(name = 'UVGC_813_1760',
                         value = '(8*ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (2*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_813_1761 = Coupling(name = 'UVGC_813_1761',
                         value = '(8*ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_814_1762 = Coupling(name = 'UVGC_814_1762',
                         value = '-(ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(4.*(-1 + sw**2)) + (ee**2*FRCTdeltaZxstLstRxgot*complex(0,1))/(4.*sw**2*(-1 + sw**2)) + (FRCTdeltaZxstRstLxgot*complex(0,1)*yu3x3**2)/(2.*(-1 + sw**2)) - (FRCTdeltaZxstRstLxgot*complex(0,1)*sw**2*yu3x3**2)/(2.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_814_1763 = Coupling(name = 'UVGC_814_1763',
                         value = '(ee*complex(0,1)*G**2*invFREps*VV1x1*VV1x2*yu3x3)/(24.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_814_1764 = Coupling(name = 'UVGC_814_1764',
                         value = '(ee*complex(0,1)*G**2*invFREps*VV2x1*VV2x2*yu3x3)/(24.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_814_1765 = Coupling(name = 'UVGC_814_1765',
                         value = '-(ee*complex(0,1)*G**2*invFREps*VV1x1*VV1x2*yu3x3)/(24.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_814_1766 = Coupling(name = 'UVGC_814_1766',
                         value = '-(ee*complex(0,1)*G**2*invFREps*VV2x1*VV2x2*yu3x3)/(24.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_815_1767 = Coupling(name = 'UVGC_815_1767',
                         value = '-(ee*complex(0,1)*G**2*invFREps*VV1x1*VV1x2*yu3x3)/(8.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_815_1768 = Coupling(name = 'UVGC_815_1768',
                         value = '-(ee*complex(0,1)*G**2*invFREps*VV2x1*VV2x2*yu3x3)/(8.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_815_1769 = Coupling(name = 'UVGC_815_1769',
                         value = '(ee*complex(0,1)*G**2*invFREps*VV1x1*VV1x2*yu3x3)/(8.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_815_1770 = Coupling(name = 'UVGC_815_1770',
                         value = '(ee*complex(0,1)*G**2*invFREps*VV2x1*VV2x2*yu3x3)/(8.*cmath.pi**2*sw)',
                         order = {'QCD':2,'QED':2})

UVGC_820_1771 = Coupling(name = 'UVGC_820_1771',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_820_1772 = Coupling(name = 'UVGC_820_1772',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_820_1773 = Coupling(name = 'UVGC_820_1773',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_820_1774 = Coupling(name = 'UVGC_820_1774',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_820_1775 = Coupling(name = 'UVGC_820_1775',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_820_1776 = Coupling(name = 'UVGC_820_1776',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_820_1777 = Coupling(name = 'UVGC_820_1777',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_820_1778 = Coupling(name = 'UVGC_820_1778',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*VV1x1**2)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_820_1779 = Coupling(name = 'UVGC_820_1779',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*VV2x1**2)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_820_1780 = Coupling(name = 'UVGC_820_1780',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*UU1x1**2)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_820_1781 = Coupling(name = 'UVGC_820_1781',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*UU2x1**2)/(24.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_821_1782 = Coupling(name = 'UVGC_821_1782',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_821_1783 = Coupling(name = 'UVGC_821_1783',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_821_1784 = Coupling(name = 'UVGC_821_1784',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_821_1785 = Coupling(name = 'UVGC_821_1785',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_821_1786 = Coupling(name = 'UVGC_821_1786',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_821_1787 = Coupling(name = 'UVGC_821_1787',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_821_1788 = Coupling(name = 'UVGC_821_1788',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*VV1x1**2)/(8.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_821_1789 = Coupling(name = 'UVGC_821_1789',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*VV2x1**2)/(8.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_821_1790 = Coupling(name = 'UVGC_821_1790',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*UU1x1**2)/(8.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_821_1791 = Coupling(name = 'UVGC_821_1791',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*UU2x1**2)/(8.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1792 = Coupling(name = 'UVGC_826_1792',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(3456.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1793 = Coupling(name = 'UVGC_826_1793',
                         value = '(-5*ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1794 = Coupling(name = 'UVGC_826_1794',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1795 = Coupling(name = 'UVGC_826_1795',
                         value = '(complex(0,1)*G**2*invFREps*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1796 = Coupling(name = 'UVGC_826_1796',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1797 = Coupling(name = 'UVGC_826_1797',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_826_1798 = Coupling(name = 'UVGC_826_1798',
                         value = '(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1))/(18.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_826_1799 = Coupling(name = 'UVGC_826_1799',
                         value = '(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_826_1800 = Coupling(name = 'UVGC_826_1800',
                         value = '(ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_826_1801 = Coupling(name = 'UVGC_826_1801',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1802 = Coupling(name = 'UVGC_826_1802',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1803 = Coupling(name = 'UVGC_826_1803',
                         value = '(7*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(1152.*cmath.pi**2*(-1 + sw**2)**3) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**3) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**3) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(864.*cmath.pi**2*(-1 + sw**2)**3)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1804 = Coupling(name = 'UVGC_826_1804',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**3) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(864.*cmath.pi**2*(-1 + sw**2)**3)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1805 = Coupling(name = 'UVGC_826_1805',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1806 = Coupling(name = 'UVGC_826_1806',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1807 = Coupling(name = 'UVGC_826_1807',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1808 = Coupling(name = 'UVGC_826_1808',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1809 = Coupling(name = 'UVGC_826_1809',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1810 = Coupling(name = 'UVGC_826_1810',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1811 = Coupling(name = 'UVGC_826_1811',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_826_1812 = Coupling(name = 'UVGC_826_1812',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1813 = Coupling(name = 'UVGC_827_1813',
                         value = '(-17*ee**2*complex(0,1)*G**2*invFREps)/(1152.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1814 = Coupling(name = 'UVGC_827_1814',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1815 = Coupling(name = 'UVGC_827_1815',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1816 = Coupling(name = 'UVGC_827_1816',
                         value = '-(complex(0,1)*G**2*invFREps*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1817 = Coupling(name = 'UVGC_827_1817',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1818 = Coupling(name = 'UVGC_827_1818',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_827_1819 = Coupling(name = 'UVGC_827_1819',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1820 = Coupling(name = 'UVGC_827_1820',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1821 = Coupling(name = 'UVGC_827_1821',
                         value = '(-7*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*(-1 + sw**2)**3) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**3) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**3) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**3)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1822 = Coupling(name = 'UVGC_827_1822',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**3) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**3)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1823 = Coupling(name = 'UVGC_827_1823',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1824 = Coupling(name = 'UVGC_827_1824',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1825 = Coupling(name = 'UVGC_827_1825',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1826 = Coupling(name = 'UVGC_827_1826',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1827 = Coupling(name = 'UVGC_827_1827',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1828 = Coupling(name = 'UVGC_827_1828',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1829 = Coupling(name = 'UVGC_827_1829',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_827_1830 = Coupling(name = 'UVGC_827_1830',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_828_1831 = Coupling(name = 'UVGC_828_1831',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(864.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_828_1832 = Coupling(name = 'UVGC_828_1832',
                         value = '(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(18.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_828_1833 = Coupling(name = 'UVGC_828_1833',
                         value = '(ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1))/(18.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_828_1834 = Coupling(name = 'UVGC_828_1834',
                         value = '(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_828_1835 = Coupling(name = 'UVGC_828_1835',
                         value = '(ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_828_1836 = Coupling(name = 'UVGC_828_1836',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_828_1837 = Coupling(name = 'UVGC_828_1837',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_828_1838 = Coupling(name = 'UVGC_828_1838',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_828_1839 = Coupling(name = 'UVGC_828_1839',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_829_1840 = Coupling(name = 'UVGC_829_1840',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_829_1841 = Coupling(name = 'UVGC_829_1841',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_829_1842 = Coupling(name = 'UVGC_829_1842',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_829_1843 = Coupling(name = 'UVGC_829_1843',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_829_1844 = Coupling(name = 'UVGC_829_1844',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_829_1845 = Coupling(name = 'UVGC_829_1845',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_829_1846 = Coupling(name = 'UVGC_829_1846',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_830_1847 = Coupling(name = 'UVGC_830_1847',
                         value = '(ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_830_1848 = Coupling(name = 'UVGC_830_1848',
                         value = '(ee**2*FRCTdeltaZxssRssRxgos*complex(0,1))/(18.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_832_1849 = Coupling(name = 'UVGC_832_1849',
                         value = '(-169*ee**2*complex(0,1)*G**2*invFREps)/(3456.*cmath.pi**2*(-1 + sw**2)**2) + (3*ee**2*complex(0,1)*G**2*invFREps)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (11*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1850 = Coupling(name = 'UVGC_832_1850',
                         value = '-(complex(0,1)*G**2*invFREps*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1851 = Coupling(name = 'UVGC_832_1851',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1852 = Coupling(name = 'UVGC_832_1852',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_832_1853 = Coupling(name = 'UVGC_832_1853',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_832_1854 = Coupling(name = 'UVGC_832_1854',
                         value = '(5*ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_832_1855 = Coupling(name = 'UVGC_832_1855',
                         value = '(5*ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_832_1856 = Coupling(name = 'UVGC_832_1856',
                         value = '(5*ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) - (5*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_832_1857 = Coupling(name = 'UVGC_832_1857',
                         value = '(5*ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) - (5*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_832_1858 = Coupling(name = 'UVGC_832_1858',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1859 = Coupling(name = 'UVGC_832_1859',
                         value = '(19*ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1860 = Coupling(name = 'UVGC_832_1860',
                         value = '(-7*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(1152.*cmath.pi**2*(-1 + sw**2)**3) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**3) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**3) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(864.*cmath.pi**2*(-1 + sw**2)**3)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1861 = Coupling(name = 'UVGC_832_1861',
                         value = '(-11*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(1152.*cmath.pi**2*(-1 + sw**2)**3) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**3) + (5*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**3) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(216.*cmath.pi**2*(-1 + sw**2)**3)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1862 = Coupling(name = 'UVGC_832_1862',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1863 = Coupling(name = 'UVGC_832_1863',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1864 = Coupling(name = 'UVGC_832_1864',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1865 = Coupling(name = 'UVGC_832_1865',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1866 = Coupling(name = 'UVGC_832_1866',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1867 = Coupling(name = 'UVGC_832_1867',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1868 = Coupling(name = 'UVGC_832_1868',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_832_1869 = Coupling(name = 'UVGC_832_1869',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1870 = Coupling(name = 'UVGC_833_1870',
                         value = '(17*ee**2*complex(0,1)*G**2*invFREps)/(1152.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1871 = Coupling(name = 'UVGC_833_1871',
                         value = '(complex(0,1)*G**2*invFREps*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1872 = Coupling(name = 'UVGC_833_1872',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1873 = Coupling(name = 'UVGC_833_1873',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_833_1874 = Coupling(name = 'UVGC_833_1874',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_833_1875 = Coupling(name = 'UVGC_833_1875',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1876 = Coupling(name = 'UVGC_833_1876',
                         value = '(-19*ee**2*complex(0,1)*G**2*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1877 = Coupling(name = 'UVGC_833_1877',
                         value = '(7*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*(-1 + sw**2)**3) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**3) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**3) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**3)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1878 = Coupling(name = 'UVGC_833_1878',
                         value = '(11*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*(-1 + sw**2)**3) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**3) - (5*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**3) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(72.*cmath.pi**2*(-1 + sw**2)**3)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1879 = Coupling(name = 'UVGC_833_1879',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1880 = Coupling(name = 'UVGC_833_1880',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1881 = Coupling(name = 'UVGC_833_1881',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1882 = Coupling(name = 'UVGC_833_1882',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1883 = Coupling(name = 'UVGC_833_1883',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1884 = Coupling(name = 'UVGC_833_1884',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1885 = Coupling(name = 'UVGC_833_1885',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_833_1886 = Coupling(name = 'UVGC_833_1886',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_834_1887 = Coupling(name = 'UVGC_834_1887',
                         value = '(ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1))/(18.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_834_1888 = Coupling(name = 'UVGC_834_1888',
                         value = '(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(18.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_834_1889 = Coupling(name = 'UVGC_834_1889',
                         value = '(ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_834_1890 = Coupling(name = 'UVGC_834_1890',
                         value = '(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_834_1891 = Coupling(name = 'UVGC_834_1891',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_834_1892 = Coupling(name = 'UVGC_834_1892',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_834_1893 = Coupling(name = 'UVGC_834_1893',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_834_1894 = Coupling(name = 'UVGC_834_1894',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_834_1895 = Coupling(name = 'UVGC_834_1895',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_834_1896 = Coupling(name = 'UVGC_834_1896',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_834_1897 = Coupling(name = 'UVGC_834_1897',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_834_1898 = Coupling(name = 'UVGC_834_1898',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_834_1899 = Coupling(name = 'UVGC_834_1899',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_835_1900 = Coupling(name = 'UVGC_835_1900',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_835_1901 = Coupling(name = 'UVGC_835_1901',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_835_1902 = Coupling(name = 'UVGC_835_1902',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_835_1903 = Coupling(name = 'UVGC_835_1903',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_835_1904 = Coupling(name = 'UVGC_835_1904',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_835_1905 = Coupling(name = 'UVGC_835_1905',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_835_1906 = Coupling(name = 'UVGC_835_1906',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_835_1907 = Coupling(name = 'UVGC_835_1907',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_835_1908 = Coupling(name = 'UVGC_835_1908',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_835_1909 = Coupling(name = 'UVGC_835_1909',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_835_1910 = Coupling(name = 'UVGC_835_1910',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1911 = Coupling(name = 'UVGC_836_1911',
                         value = '(49*ee**2*complex(0,1)*G**2*invFREps)/(3456.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1912 = Coupling(name = 'UVGC_836_1912',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1913 = Coupling(name = 'UVGC_836_1913',
                         value = '(2*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (4*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) + (2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1914 = Coupling(name = 'UVGC_836_1914',
                         value = '-(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_836_1915 = Coupling(name = 'UVGC_836_1915',
                         value = '-(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1916 = Coupling(name = 'UVGC_836_1916',
                         value = '-(ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1917 = Coupling(name = 'UVGC_836_1917',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1918 = Coupling(name = 'UVGC_836_1918',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1919 = Coupling(name = 'UVGC_836_1919',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1920 = Coupling(name = 'UVGC_836_1920',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1921 = Coupling(name = 'UVGC_836_1921',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1922 = Coupling(name = 'UVGC_836_1922',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1923 = Coupling(name = 'UVGC_836_1923',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_836_1924 = Coupling(name = 'UVGC_836_1924',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_837_1925 = Coupling(name = 'UVGC_837_1925',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_837_1926 = Coupling(name = 'UVGC_837_1926',
                         value = '(-2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (4*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) - (2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_837_1927 = Coupling(name = 'UVGC_837_1927',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*(-1 + sw**2)**2) - (2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_837_1928 = Coupling(name = 'UVGC_837_1928',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_837_1929 = Coupling(name = 'UVGC_837_1929',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_837_1930 = Coupling(name = 'UVGC_837_1930',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_837_1931 = Coupling(name = 'UVGC_837_1931',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(6.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(6.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_837_1932 = Coupling(name = 'UVGC_837_1932',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(6.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(6.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_837_1933 = Coupling(name = 'UVGC_837_1933',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(6.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(6.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_837_1934 = Coupling(name = 'UVGC_837_1934',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(6.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(6.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_837_1935 = Coupling(name = 'UVGC_837_1935',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_837_1936 = Coupling(name = 'UVGC_837_1936',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_838_1937 = Coupling(name = 'UVGC_838_1937',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(864.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_838_1938 = Coupling(name = 'UVGC_838_1938',
                         value = '(5*ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_838_1939 = Coupling(name = 'UVGC_838_1939',
                         value = '(5*ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (19*ee**2*complex(0,1)*G**2*invFREps)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_838_1940 = Coupling(name = 'UVGC_838_1940',
                         value = '(5*ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (19*ee**2*complex(0,1)*G**2*invFREps)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_838_1941 = Coupling(name = 'UVGC_838_1941',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_838_1942 = Coupling(name = 'UVGC_838_1942',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_839_1943 = Coupling(name = 'UVGC_839_1943',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**4)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_839_1944 = Coupling(name = 'UVGC_839_1944',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_839_1945 = Coupling(name = 'UVGC_839_1945',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_842_1946 = Coupling(name = 'UVGC_842_1946',
                         value = '(-97*ee**2*complex(0,1)*G**2*invFREps)/(3456.*cmath.pi**2*(-1 + sw**2)**2) + (5*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (13*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_842_1947 = Coupling(name = 'UVGC_842_1947',
                         value = '(5*ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) - (13*ee**2*complex(0,1)*G**2*invFREps)/(1728.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_842_1948 = Coupling(name = 'UVGC_842_1948',
                         value = '(5*ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) - (13*ee**2*complex(0,1)*G**2*invFREps)/(1728.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_842_1949 = Coupling(name = 'UVGC_842_1949',
                         value = '(5*ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_842_1950 = Coupling(name = 'UVGC_842_1950',
                         value = '(73*ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (37*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_842_1951 = Coupling(name = 'UVGC_842_1951',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_842_1952 = Coupling(name = 'UVGC_842_1952',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_842_1953 = Coupling(name = 'UVGC_842_1953',
                         value = '(3*ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_842_1954 = Coupling(name = 'UVGC_842_1954',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2)',
                         order = {'QCD':2,'QED':2})

UVGC_843_1955 = Coupling(name = 'UVGC_843_1955',
                         value = '(41*ee**2*complex(0,1)*G**2*invFREps)/(1152.*cmath.pi**2*(-1 + sw**2)**2) - (7*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_843_1956 = Coupling(name = 'UVGC_843_1956',
                         value = '-(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(2.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_843_1957 = Coupling(name = 'UVGC_843_1957',
                         value = '-(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(2.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_843_1958 = Coupling(name = 'UVGC_843_1958',
                         value = '-(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(2.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_843_1959 = Coupling(name = 'UVGC_843_1959',
                         value = '-(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(2.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_843_1960 = Coupling(name = 'UVGC_843_1960',
                         value = '(23*ee**2*complex(0,1)*G**2*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (11*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_843_1961 = Coupling(name = 'UVGC_843_1961',
                         value = '(3*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)) - (3*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_843_1962 = Coupling(name = 'UVGC_843_1962',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_846_1963 = Coupling(name = 'UVGC_846_1963',
                         value = '(119*ee**2*complex(0,1)*G**2*invFREps)/(3456.*cmath.pi**2*(-1 + sw**2)**2) - (7*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_846_1964 = Coupling(name = 'UVGC_846_1964',
                         value = '(2*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_846_1965 = Coupling(name = 'UVGC_846_1965',
                         value = '(-2*ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_846_1966 = Coupling(name = 'UVGC_846_1966',
                         value = '(-2*ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_846_1967 = Coupling(name = 'UVGC_846_1967',
                         value = '(-2*ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_846_1968 = Coupling(name = 'UVGC_846_1968',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_846_1969 = Coupling(name = 'UVGC_846_1969',
                         value = '(-2*ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_846_1970 = Coupling(name = 'UVGC_846_1970',
                         value = '(-17*ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_846_1971 = Coupling(name = 'UVGC_846_1971',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_846_1972 = Coupling(name = 'UVGC_846_1972',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_846_1973 = Coupling(name = 'UVGC_846_1973',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_846_1974 = Coupling(name = 'UVGC_846_1974',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_846_1975 = Coupling(name = 'UVGC_846_1975',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_846_1976 = Coupling(name = 'UVGC_846_1976',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_847_1977 = Coupling(name = 'UVGC_847_1977',
                         value = '(-2*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_847_1978 = Coupling(name = 'UVGC_847_1978',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(3.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_847_1979 = Coupling(name = 'UVGC_847_1979',
                         value = '(17*ee**2*complex(0,1)*G**2*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_847_1980 = Coupling(name = 'UVGC_847_1980',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_847_1981 = Coupling(name = 'UVGC_847_1981',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_847_1982 = Coupling(name = 'UVGC_847_1982',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_847_1983 = Coupling(name = 'UVGC_847_1983',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_847_1984 = Coupling(name = 'UVGC_847_1984',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_847_1985 = Coupling(name = 'UVGC_847_1985',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_847_1986 = Coupling(name = 'UVGC_847_1986',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_848_1987 = Coupling(name = 'UVGC_848_1987',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_848_1988 = Coupling(name = 'UVGC_848_1988',
                         value = '-(ee**2*FRCTdeltaZxscLscLxgoc*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_848_1989 = Coupling(name = 'UVGC_848_1989',
                         value = '-(ee**2*FRCTdeltaZxscLscLxGscL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_848_1990 = Coupling(name = 'UVGC_848_1990',
                         value = '-(ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_848_1991 = Coupling(name = 'UVGC_848_1991',
                         value = '-(ee**2*FRCTdeltaZxstRstRxgot*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_848_1992 = Coupling(name = 'UVGC_848_1992',
                         value = '(11*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(1152.*cmath.pi**2*(-1 + sw**2)**3) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**3) - (5*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**3) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(216.*cmath.pi**2*(-1 + sw**2)**3)',
                         order = {'QCD':2,'QED':2})

UVGC_848_1993 = Coupling(name = 'UVGC_848_1993',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**3) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(216.*cmath.pi**2*(-1 + sw**2)**3)',
                         order = {'QCD':2,'QED':2})

UVGC_849_1994 = Coupling(name = 'UVGC_849_1994',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_849_1995 = Coupling(name = 'UVGC_849_1995',
                         value = '(-11*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*(-1 + sw**2)**3) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**3) + (5*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**3) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(72.*cmath.pi**2*(-1 + sw**2)**3)',
                         order = {'QCD':2,'QED':2})

UVGC_849_1996 = Coupling(name = 'UVGC_849_1996',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**3) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(72.*cmath.pi**2*(-1 + sw**2)**3)',
                         order = {'QCD':2,'QED':2})

UVGC_850_1997 = Coupling(name = 'UVGC_850_1997',
                         value = '-(ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_850_1998 = Coupling(name = 'UVGC_850_1998',
                         value = '-(ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_852_1999 = Coupling(name = 'UVGC_852_1999',
                         value = '-(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_852_2000 = Coupling(name = 'UVGC_852_2000',
                         value = '-(ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_852_2001 = Coupling(name = 'UVGC_852_2001',
                         value = '-(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_852_2002 = Coupling(name = 'UVGC_852_2002',
                         value = '-(ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_852_2003 = Coupling(name = 'UVGC_852_2003',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_852_2004 = Coupling(name = 'UVGC_852_2004',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_852_2005 = Coupling(name = 'UVGC_852_2005',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_852_2006 = Coupling(name = 'UVGC_852_2006',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_852_2007 = Coupling(name = 'UVGC_852_2007',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_852_2008 = Coupling(name = 'UVGC_852_2008',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(18.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_852_2009 = Coupling(name = 'UVGC_852_2009',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_853_2010 = Coupling(name = 'UVGC_853_2010',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_853_2011 = Coupling(name = 'UVGC_853_2011',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_853_2012 = Coupling(name = 'UVGC_853_2012',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(6.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(6.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_853_2013 = Coupling(name = 'UVGC_853_2013',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(6.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(6.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_853_2014 = Coupling(name = 'UVGC_853_2014',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(6.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(6.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_853_2015 = Coupling(name = 'UVGC_853_2015',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(18.*cmath.pi**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(6.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(6.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_853_2016 = Coupling(name = 'UVGC_853_2016',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_854_2017 = Coupling(name = 'UVGC_854_2017',
                         value = '(17*ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)**2) - (17*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_854_2018 = Coupling(name = 'UVGC_854_2018',
                         value = '(5*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_854_2019 = Coupling(name = 'UVGC_854_2019',
                         value = '(-2*ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_854_2020 = Coupling(name = 'UVGC_854_2020',
                         value = '(-2*ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_854_2021 = Coupling(name = 'UVGC_854_2021',
                         value = '(-2*ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_854_2022 = Coupling(name = 'UVGC_854_2022',
                         value = '(-2*ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_854_2023 = Coupling(name = 'UVGC_854_2023',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_854_2024 = Coupling(name = 'UVGC_854_2024',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_854_2025 = Coupling(name = 'UVGC_854_2025',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_854_2026 = Coupling(name = 'UVGC_854_2026',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_855_2027 = Coupling(name = 'UVGC_855_2027',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(288.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_855_2028 = Coupling(name = 'UVGC_855_2028',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_855_2029 = Coupling(name = 'UVGC_855_2029',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_855_2030 = Coupling(name = 'UVGC_855_2030',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(288.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_855_2031 = Coupling(name = 'UVGC_855_2031',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_855_2032 = Coupling(name = 'UVGC_855_2032',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_855_2033 = Coupling(name = 'UVGC_855_2033',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_855_2034 = Coupling(name = 'UVGC_855_2034',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_855_2035 = Coupling(name = 'UVGC_855_2035',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_855_2036 = Coupling(name = 'UVGC_855_2036',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_855_2037 = Coupling(name = 'UVGC_855_2037',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(36.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_856_2038 = Coupling(name = 'UVGC_856_2038',
                         value = '-(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_856_2039 = Coupling(name = 'UVGC_856_2039',
                         value = '-(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_858_2040 = Coupling(name = 'UVGC_858_2040',
                         value = '(-2*ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_858_2041 = Coupling(name = 'UVGC_858_2041',
                         value = '(-2*ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_860_2042 = Coupling(name = 'UVGC_860_2042',
                         value = '-(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_860_2043 = Coupling(name = 'UVGC_860_2043',
                         value = '-(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_862_2044 = Coupling(name = 'UVGC_862_2044',
                         value = '(-2*ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_862_2045 = Coupling(name = 'UVGC_862_2045',
                         value = '(-2*ee**2*FRCTdeltaZxssRssRxgos*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_864_2046 = Coupling(name = 'UVGC_864_2046',
                         value = '(4*ee**2*FRCTdeltaZxscRscRxgoc*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_864_2047 = Coupling(name = 'UVGC_864_2047',
                         value = '(4*ee**2*FRCTdeltaZxscRscRxGscR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_864_2048 = Coupling(name = 'UVGC_864_2048',
                         value = '(4*ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_864_2049 = Coupling(name = 'UVGC_864_2049',
                         value = '(2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_864_2050 = Coupling(name = 'UVGC_864_2050',
                         value = '(4*ee**2*FRCTdeltaZxstRstRxgot*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_864_2051 = Coupling(name = 'UVGC_864_2051',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_864_2052 = Coupling(name = 'UVGC_864_2052',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_864_2053 = Coupling(name = 'UVGC_864_2053',
                         value = '(-2*cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_864_2054 = Coupling(name = 'UVGC_864_2054',
                         value = '(-2*cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_864_2055 = Coupling(name = 'UVGC_864_2055',
                         value = '(-2*cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_864_2056 = Coupling(name = 'UVGC_864_2056',
                         value = '(-2*cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_865_2057 = Coupling(name = 'UVGC_865_2057',
                         value = '(-2*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_865_2058 = Coupling(name = 'UVGC_865_2058',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_865_2059 = Coupling(name = 'UVGC_865_2059',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_865_2060 = Coupling(name = 'UVGC_865_2060',
                         value = '(2*cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_865_2061 = Coupling(name = 'UVGC_865_2061',
                         value = '(2*cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_865_2062 = Coupling(name = 'UVGC_865_2062',
                         value = '(2*cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_865_2063 = Coupling(name = 'UVGC_865_2063',
                         value = '(2*cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(9.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_865_2064 = Coupling(name = 'UVGC_865_2064',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_866_2065 = Coupling(name = 'UVGC_866_2065',
                         value = '(-2*ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_866_2066 = Coupling(name = 'UVGC_866_2066',
                         value = '(-2*ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_866_2067 = Coupling(name = 'UVGC_866_2067',
                         value = '(-2*ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) - (17*ee**2*complex(0,1)*G**2*invFREps)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_866_2068 = Coupling(name = 'UVGC_866_2068',
                         value = '(-2*ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) - (17*ee**2*complex(0,1)*G**2*invFREps)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_866_2069 = Coupling(name = 'UVGC_866_2069',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_866_2070 = Coupling(name = 'UVGC_866_2070',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_866_2071 = Coupling(name = 'UVGC_866_2071',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_866_2072 = Coupling(name = 'UVGC_866_2072',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_866_2073 = Coupling(name = 'UVGC_866_2073',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_866_2074 = Coupling(name = 'UVGC_866_2074',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(36.*cmath.pi**2*sw*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(36.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2*sw**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_867_2075 = Coupling(name = 'UVGC_867_2075',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_867_2076 = Coupling(name = 'UVGC_867_2076',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_867_2077 = Coupling(name = 'UVGC_867_2077',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN1x1*NN1x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN1x2**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_867_2078 = Coupling(name = 'UVGC_867_2078',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN2x1*NN2x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN2x2**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_867_2079 = Coupling(name = 'UVGC_867_2079',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN3x1*NN3x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN3x2**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_867_2080 = Coupling(name = 'UVGC_867_2080',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)**2) + (cw*ee**2*complex(0,1)*G**2*invFREps*NN4x1*NN4x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*NN4x2**2*sw**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_867_2081 = Coupling(name = 'UVGC_867_2081',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_868_2082 = Coupling(name = 'UVGC_868_2082',
                         value = '(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(18.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_868_2083 = Coupling(name = 'UVGC_868_2083',
                         value = '(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_870_2084 = Coupling(name = 'UVGC_870_2084',
                         value = '(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1))/(18.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_872_2085 = Coupling(name = 'UVGC_872_2085',
                         value = '(-2*ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) - (17*ee**2*complex(0,1)*G**2*invFREps)/(432.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_872_2086 = Coupling(name = 'UVGC_872_2086',
                         value = '(-2*ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_876_2087 = Coupling(name = 'UVGC_876_2087',
                         value = '(-7*ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)**2) + (7*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(864.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_876_2088 = Coupling(name = 'UVGC_876_2088',
                         value = '(ee**2*FRCTdeltaZxsbRsbRxgob*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_876_2089 = Coupling(name = 'UVGC_876_2089',
                         value = '(ee**2*FRCTdeltaZxsdRsdRxgod*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_876_2090 = Coupling(name = 'UVGC_876_2090',
                         value = '(ee**2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_876_2091 = Coupling(name = 'UVGC_876_2091',
                         value = '(ee**2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_876_2092 = Coupling(name = 'UVGC_876_2092',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_876_2093 = Coupling(name = 'UVGC_876_2093',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_876_2094 = Coupling(name = 'UVGC_876_2094',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_876_2095 = Coupling(name = 'UVGC_876_2095',
                         value = '-(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_877_2096 = Coupling(name = 'UVGC_877_2096',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN1x1**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_877_2097 = Coupling(name = 'UVGC_877_2097',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN2x1**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_877_2098 = Coupling(name = 'UVGC_877_2098',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN3x1**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_877_2099 = Coupling(name = 'UVGC_877_2099',
                         value = '(cw**2*ee**2*complex(0,1)*G**2*invFREps*NN4x1**2)/(18.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_878_2100 = Coupling(name = 'UVGC_878_2100',
                         value = '(ee**2*FRCTdeltaZxssRssRxGssR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_878_2101 = Coupling(name = 'UVGC_878_2101',
                         value = '(ee**2*FRCTdeltaZxssRssRxgos*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_880_2102 = Coupling(name = 'UVGC_880_2102',
                         value = '(-2*ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_880_2103 = Coupling(name = 'UVGC_880_2103',
                         value = '(-2*ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_880_2104 = Coupling(name = 'UVGC_880_2104',
                         value = '(3*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*(6*sw - 6*sw**3)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(2.*cmath.pi**2*(6*sw - 6*sw**3)**2) + (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(6.*cmath.pi**2*(6*sw - 6*sw**3)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_881_2105 = Coupling(name = 'UVGC_881_2105',
                         value = '(-9*cw**2*ee**2*complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2*(6*sw - 6*sw**3)**2) + (3*cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(2.*cmath.pi**2*(6*sw - 6*sw**3)**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps*sw**4)/(2.*cmath.pi**2*(6*sw - 6*sw**3)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_882_2106 = Coupling(name = 'UVGC_882_2106',
                         value = '(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_882_2107 = Coupling(name = 'UVGC_882_2107',
                         value = '(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(18.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_886_2108 = Coupling(name = 'UVGC_886_2108',
                         value = '(ee**2*FRCTdeltaZxssLssLxgos*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxssRssRxgos*complex(0,1))/(18.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_890_2109 = Coupling(name = 'UVGC_890_2109',
                         value = '-(complex(0,1)*G**2*invFREps*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_890_2110 = Coupling(name = 'UVGC_890_2110',
                         value = '(5*ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) - (13*ee**2*complex(0,1)*G**2*invFREps)/(1728.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_890_2111 = Coupling(name = 'UVGC_890_2111',
                         value = '(5*ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) - (13*ee**2*complex(0,1)*G**2*invFREps)/(1728.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_890_2112 = Coupling(name = 'UVGC_890_2112',
                         value = '(5*ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_891_2113 = Coupling(name = 'UVGC_891_2113',
                         value = '(complex(0,1)*G**2*invFREps*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(8.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_891_2114 = Coupling(name = 'UVGC_891_2114',
                         value = '-(ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(2.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsbLsbLxgob*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_891_2115 = Coupling(name = 'UVGC_891_2115',
                         value = '-(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(2.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_891_2116 = Coupling(name = 'UVGC_891_2116',
                         value = '-(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(2.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_891_2117 = Coupling(name = 'UVGC_891_2117',
                         value = '-(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(2.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_892_2118 = Coupling(name = 'UVGC_892_2118',
                         value = '(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_892_2119 = Coupling(name = 'UVGC_892_2119',
                         value = '(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(18.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_894_2120 = Coupling(name = 'UVGC_894_2120',
                         value = '-(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_894_2121 = Coupling(name = 'UVGC_894_2121',
                         value = '-(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_896_2122 = Coupling(name = 'UVGC_896_2122',
                         value = '(5*ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (19*ee**2*complex(0,1)*G**2*invFREps)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_900_2123 = Coupling(name = 'UVGC_900_2123',
                         value = '(5*ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) - (5*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_900_2124 = Coupling(name = 'UVGC_900_2124',
                         value = '(5*ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) - (5*ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)) + (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_904_2125 = Coupling(name = 'UVGC_904_2125',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_904_2126 = Coupling(name = 'UVGC_904_2126',
                         value = '(49*ee**2*complex(0,1)*G**2*invFREps)/(3456.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(384.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_904_2127 = Coupling(name = 'UVGC_904_2127',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_904_2128 = Coupling(name = 'UVGC_904_2128',
                         value = '-(ee**2*FRCTdeltaZxstLstLxGstL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**2*invFREps*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_904_2129 = Coupling(name = 'UVGC_904_2129',
                         value = '-(ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**2*invFREps*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_904_2130 = Coupling(name = 'UVGC_904_2130',
                         value = '-(ee**2*FRCTdeltaZxstLstLxgot*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxstRstRxgot*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_904_2131 = Coupling(name = 'UVGC_904_2131',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(8.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_904_2132 = Coupling(name = 'UVGC_904_2132',
                         value = '(complex(0,1)*G**2*invFREps*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_905_2133 = Coupling(name = 'UVGC_905_2133',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(64.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_905_2134 = Coupling(name = 'UVGC_905_2134',
                         value = '(-17*ee**2*complex(0,1)*G**2*invFREps)/(1152.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(128.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(144.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_905_2135 = Coupling(name = 'UVGC_905_2135',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) - (complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_905_2136 = Coupling(name = 'UVGC_905_2136',
                         value = '(FRCTdeltaZxstLstLxGstL*complex(0,1)*yu3x3**2)/((-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstLxGstL*complex(0,1)*sw**2*yu3x3**2)/((-1 + sw)*(1 + sw)) + (complex(0,1)*G**2*invFREps*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_905_2137 = Coupling(name = 'UVGC_905_2137',
                         value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*yu3x3**2)/((-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxGstR*complex(0,1)*sw**2*yu3x3**2)/((-1 + sw)*(1 + sw)) + (complex(0,1)*G**2*invFREps*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_905_2138 = Coupling(name = 'UVGC_905_2138',
                         value = '(2*FRCTdeltaxMTxtG*complex(0,1)*yu3x3**2)/(MT*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaxMTxtG*complex(0,1)*sw**2*yu3x3**2)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_905_2139 = Coupling(name = 'UVGC_905_2139',
                         value = '(2*FRCTdeltaxMTxgostL*complex(0,1)*yu3x3**2)/(MT*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaxMTxgostL*complex(0,1)*sw**2*yu3x3**2)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_905_2140 = Coupling(name = 'UVGC_905_2140',
                         value = '(2*FRCTdeltaxMTxgostR*complex(0,1)*yu3x3**2)/(MT*(-1 + sw)*(1 + sw)) - (2*FRCTdeltaxMTxgostR*complex(0,1)*sw**2*yu3x3**2)/(MT*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_905_2141 = Coupling(name = 'UVGC_905_2141',
                         value = '(FRCTdeltaZxstLstLxgot*complex(0,1)*yu3x3**2)/((-1 + sw)*(1 + sw)) + (FRCTdeltaZxstRstRxgot*complex(0,1)*yu3x3**2)/((-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstLxgot*complex(0,1)*sw**2*yu3x3**2)/((-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxgot*complex(0,1)*sw**2*yu3x3**2)/((-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_905_2142 = Coupling(name = 'UVGC_905_2142',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(72.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**2*invFREps*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2) - (7*complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(24.*cmath.pi**2*(-1 + sw**2)**2) + (7*complex(0,1)*G**2*invFREps*sw**4*yu3x3**2)/(48.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_905_2143 = Coupling(name = 'UVGC_905_2143',
                         value = '(-3*complex(0,1)*G**2*invFREps*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2)) + (3*complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(16.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_908_2144 = Coupling(name = 'UVGC_908_2144',
                         value = '-(ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**2*invFREps*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2)) + (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(32.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_908_2145 = Coupling(name = 'UVGC_908_2145',
                         value = '(complex(0,1)*G**2*invFREps*VV1x2**2*yu3x3**2)/(8.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_908_2146 = Coupling(name = 'UVGC_908_2146',
                         value = '(complex(0,1)*G**2*invFREps*VV2x2**2*yu3x3**2)/(8.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_908_2147 = Coupling(name = 'UVGC_908_2147',
                         value = '-(complex(0,1)*G**2*invFREps*VV1x2**2*yu3x3**2)/(4.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_908_2148 = Coupling(name = 'UVGC_908_2148',
                         value = '-(complex(0,1)*G**2*invFREps*VV2x2**2*yu3x3**2)/(4.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_909_2149 = Coupling(name = 'UVGC_909_2149',
                         value = '(FRCTdeltaZxsbLsbLxgob*complex(0,1)*yu3x3**2)/((-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbLsbLxgob*complex(0,1)*sw**2*yu3x3**2)/((-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_909_2150 = Coupling(name = 'UVGC_909_2150',
                         value = '(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*yu3x3**2)/((-1 + sw)*(1 + sw)) - (FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*sw**2*yu3x3**2)/((-1 + sw)*(1 + sw)) + (complex(0,1)*G**2*invFREps*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2)) - (complex(0,1)*G**2*invFREps*sw**2*yu3x3**2)/(96.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_909_2151 = Coupling(name = 'UVGC_909_2151',
                         value = '(FRCTdeltaZxstRstRxgot*complex(0,1)*yu3x3**2)/((-1 + sw)*(1 + sw)) - (FRCTdeltaZxstRstRxgot*complex(0,1)*sw**2*yu3x3**2)/((-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_909_2152 = Coupling(name = 'UVGC_909_2152',
                         value = '-(complex(0,1)*G**2*invFREps*VV1x2**2*yu3x3**2)/(24.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_909_2153 = Coupling(name = 'UVGC_909_2153',
                         value = '-(complex(0,1)*G**2*invFREps*VV2x2**2*yu3x3**2)/(24.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_909_2154 = Coupling(name = 'UVGC_909_2154',
                         value = '(complex(0,1)*G**2*invFREps*VV1x2**2*yu3x3**2)/(12.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_909_2155 = Coupling(name = 'UVGC_909_2155',
                         value = '(complex(0,1)*G**2*invFREps*VV2x2**2*yu3x3**2)/(12.*cmath.pi**2)',
                         order = {'QCD':2,'QED':2})

UVGC_910_2156 = Coupling(name = 'UVGC_910_2156',
                         value = '(-2*ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_910_2157 = Coupling(name = 'UVGC_910_2157',
                         value = '(-2*ee**2*FRCTdeltaZxstRstRxgot*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_912_2158 = Coupling(name = 'UVGC_912_2158',
                         value = '-(ee**2*FRCTdeltaZxstRstRxGstR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_916_2159 = Coupling(name = 'UVGC_916_2159',
                         value = '-(ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_920_2160 = Coupling(name = 'UVGC_920_2160',
                         value = '(5*ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (19*ee**2*complex(0,1)*G**2*invFREps)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_920_2161 = Coupling(name = 'UVGC_920_2161',
                         value = '(5*ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (19*ee**2*complex(0,1)*G**2*invFREps)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_920_2162 = Coupling(name = 'UVGC_920_2162',
                         value = '(5*ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_922_2163 = Coupling(name = 'UVGC_922_2163',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(216.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_922_2164 = Coupling(name = 'UVGC_922_2164',
                         value = '(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(18.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_924_2165 = Coupling(name = 'UVGC_924_2165',
                         value = '(-2*ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_924_2166 = Coupling(name = 'UVGC_924_2166',
                         value = '(-2*ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_926_2167 = Coupling(name = 'UVGC_926_2167',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_926_2168 = Coupling(name = 'UVGC_926_2168',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_928_2169 = Coupling(name = 'UVGC_928_2169',
                         value = '(5*ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (11*ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (13*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(1728.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_928_2170 = Coupling(name = 'UVGC_928_2170',
                         value = '(5*ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (11*ee**2*complex(0,1)*G**2*invFREps)/(864.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (13*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(1728.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_928_2171 = Coupling(name = 'UVGC_928_2171',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(2.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(4.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(4.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_928_2172 = Coupling(name = 'UVGC_928_2172',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(96.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_928_2173 = Coupling(name = 'UVGC_928_2173',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*VV1x1**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*VV1x1**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2*VV1x1**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_928_2174 = Coupling(name = 'UVGC_928_2174',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*UU1x1**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*UU1x1**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2*UU1x1**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_928_2175 = Coupling(name = 'UVGC_928_2175',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*VV2x1**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*VV2x1**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2*VV2x1**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_928_2176 = Coupling(name = 'UVGC_928_2176',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps*UU2x1**2)/(4.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*UU2x1**2)/(8.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2*UU2x1**2)/(8.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_928_2177 = Coupling(name = 'UVGC_928_2177',
                         value = '(ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_928_2178 = Coupling(name = 'UVGC_928_2178',
                         value = '(-3*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*(-1 + sw**2)**2) + (3*ee**2*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (3*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(64.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_929_2179 = Coupling(name = 'UVGC_929_2179',
                         value = '-(ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(2.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsdLsdLxgod*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_929_2180 = Coupling(name = 'UVGC_929_2180',
                         value = '-(ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(2.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_929_2181 = Coupling(name = 'UVGC_929_2181',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(2.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(192.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(192.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_929_2182 = Coupling(name = 'UVGC_929_2182',
                         value = '-(ee**2*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(12.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_929_2183 = Coupling(name = 'UVGC_929_2183',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(2.*(-1 + sw)*(1 + sw)) + (ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(2.*(-1 + sw)*sw**2*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_929_2184 = Coupling(name = 'UVGC_929_2184',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*VV1x1**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*VV1x1**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2*VV1x1**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_929_2185 = Coupling(name = 'UVGC_929_2185',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*UU1x1**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*UU1x1**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2*UU1x1**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_929_2186 = Coupling(name = 'UVGC_929_2186',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*VV2x1**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*VV2x1**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2*VV2x1**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_929_2187 = Coupling(name = 'UVGC_929_2187',
                         value = '(ee**2*complex(0,1)*G**2*invFREps*UU2x1**2)/(12.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*UU2x1**2)/(24.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2*UU2x1**2)/(24.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_929_2188 = Coupling(name = 'UVGC_929_2188',
                         value = '(-3*ee**2*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2*(-1 + sw**2)**2) + (3*ee**2*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2*sw**2*(-1 + sw**2)**2) + (3*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(32.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_932_2189 = Coupling(name = 'UVGC_932_2189',
                         value = '(5*ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(18.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxssLssLxGssL*complex(0,1))/(4.*(-1 + sw)*sw**2*(1 + sw)) + (19*ee**2*complex(0,1)*G**2*invFREps)/(432.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2*sw**2*(-1 + sw**2)**2) - (5*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(216.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_940_2190 = Coupling(name = 'UVGC_940_2190',
                         value = '-(ee**2*FRCTdeltaZxsuLsuLxgou*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_942_2191 = Coupling(name = 'UVGC_942_2191',
                         value = '-(ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(108.*cmath.pi**2*(-1 + sw**2))',
                         order = {'QCD':2,'QED':2})

UVGC_944_2192 = Coupling(name = 'UVGC_944_2192',
                         value = '(-2*ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) - (ee**2*complex(0,1)*G**2*invFREps)/(54.*cmath.pi**2*(-1 + sw**2)**2) + (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(54.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_944_2193 = Coupling(name = 'UVGC_944_2193',
                         value = '(-2*ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_946_2194 = Coupling(name = 'UVGC_946_2194',
                         value = '(4*ee**2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1))/(9.*(-1 + sw)*(1 + sw)) + (ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2*(-1 + sw**2)**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cmath.pi**2*(-1 + sw**2)**2)',
                         order = {'QCD':2,'QED':2})

UVGC_946_2195 = Coupling(name = 'UVGC_946_2195',
                         value = '(4*ee**2*FRCTdeltaZxsuRsuRxgou*complex(0,1))/(9.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':2})

UVGC_960_2196 = Coupling(name = 'UVGC_960_2196',
                         value = '-((FRCTdeltaxaSxgo*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2197 = Coupling(name = 'UVGC_960_2197',
                         value = '-((FRCTdeltaxaSxsbL*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2198 = Coupling(name = 'UVGC_960_2198',
                         value = '-((FRCTdeltaxaSxsbR*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2199 = Coupling(name = 'UVGC_960_2199',
                         value = '-((FRCTdeltaxaSxscL*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2200 = Coupling(name = 'UVGC_960_2200',
                         value = '-((FRCTdeltaxaSxscR*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2201 = Coupling(name = 'UVGC_960_2201',
                         value = '-((FRCTdeltaxaSxsdL*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2202 = Coupling(name = 'UVGC_960_2202',
                         value = '-((FRCTdeltaxaSxsdR*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2203 = Coupling(name = 'UVGC_960_2203',
                         value = '-((FRCTdeltaxaSxssL*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2204 = Coupling(name = 'UVGC_960_2204',
                         value = '-((FRCTdeltaxaSxssR*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2205 = Coupling(name = 'UVGC_960_2205',
                         value = '-((FRCTdeltaxaSxstL*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2206 = Coupling(name = 'UVGC_960_2206',
                         value = '-((FRCTdeltaxaSxstR*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2207 = Coupling(name = 'UVGC_960_2207',
                         value = '-((FRCTdeltaxaSxsuL*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2208 = Coupling(name = 'UVGC_960_2208',
                         value = '-((FRCTdeltaxaSxsuR*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2209 = Coupling(name = 'UVGC_960_2209',
                         value = '-((FRCTdeltaxaSxt*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                         order = {'QCD':3})

UVGC_960_2210 = Coupling(name = 'UVGC_960_2210',
                         value = '-((FRCTdeltaZxbbLxbG*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2211 = Coupling(name = 'UVGC_960_2211',
                         value = '-((FRCTdeltaZxsbLsbLxgob*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2212 = Coupling(name = 'UVGC_960_2212',
                         value = '-((FRCTdeltaZxgogoLxbsbL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2213 = Coupling(name = 'UVGC_960_2213',
                         value = '-((FRCTdeltaZxgogoLxbsbR*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2214 = Coupling(name = 'UVGC_960_2214',
                         value = '-((FRCTdeltaZxgogoLxcscL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2215 = Coupling(name = 'UVGC_960_2215',
                         value = '-((FRCTdeltaZxgogoLxcscR*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2216 = Coupling(name = 'UVGC_960_2216',
                         value = '-((FRCTdeltaZxgogoLxdsdL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2217 = Coupling(name = 'UVGC_960_2217',
                         value = '-((FRCTdeltaZxgogoLxdsdR*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2218 = Coupling(name = 'UVGC_960_2218',
                         value = '-((FRCTdeltaZxgogoLxgoG*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2219 = Coupling(name = 'UVGC_960_2219',
                         value = '-((FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2220 = Coupling(name = 'UVGC_960_2220',
                         value = '-((FRCTdeltaZxbbLxgosbL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2221 = Coupling(name = 'UVGC_960_2221',
                         value = '-((FRCTdeltaZxgogoLxsssL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2222 = Coupling(name = 'UVGC_960_2222',
                         value = '-((FRCTdeltaZxgogoLxsssR*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2223 = Coupling(name = 'UVGC_960_2223',
                         value = '-((FRCTdeltaZxgogoLxtstL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2224 = Coupling(name = 'UVGC_960_2224',
                         value = '-((FRCTdeltaZxgogoLxtstR*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2225 = Coupling(name = 'UVGC_960_2225',
                         value = '-((FRCTdeltaZxgogoLxusuL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2226 = Coupling(name = 'UVGC_960_2226',
                         value = '-((FRCTdeltaZxgogoLxusuR*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2227 = Coupling(name = 'UVGC_960_2227',
                         value = '(3*complex(0,1)*G**3*invFREps)/(4.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2228 = Coupling(name = 'UVGC_960_2228',
                         value = '-(complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_960_2229 = Coupling(name = 'UVGC_960_2229',
                         value = '(3*complex(0,1)*G**3*invFREps)/(16.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_961_2230 = Coupling(name = 'UVGC_961_2230',
                         value = '-((FRCTdeltaZxccLxcG*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_961_2231 = Coupling(name = 'UVGC_961_2231',
                         value = '-((FRCTdeltaZxscLscLxgoc*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_961_2232 = Coupling(name = 'UVGC_961_2232',
                         value = '-((FRCTdeltaZxscLscLxGscL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_961_2233 = Coupling(name = 'UVGC_961_2233',
                         value = '-((FRCTdeltaZxccLxgoscL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_962_2234 = Coupling(name = 'UVGC_962_2234',
                         value = '-((FRCTdeltaZxddLxdG*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_962_2235 = Coupling(name = 'UVGC_962_2235',
                         value = '-((FRCTdeltaZxsdLsdLxgod*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_962_2236 = Coupling(name = 'UVGC_962_2236',
                         value = '-((FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_962_2237 = Coupling(name = 'UVGC_962_2237',
                         value = '-((FRCTdeltaZxddLxgosdL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_963_2238 = Coupling(name = 'UVGC_963_2238',
                         value = '-((FRCTdeltaZxssLxsG*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_963_2239 = Coupling(name = 'UVGC_963_2239',
                         value = '-((FRCTdeltaZxssLssLxGssL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_963_2240 = Coupling(name = 'UVGC_963_2240',
                         value = '-((FRCTdeltaZxssLssLxgos*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_963_2241 = Coupling(name = 'UVGC_963_2241',
                         value = '-((FRCTdeltaZxssLxgossL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_964_2242 = Coupling(name = 'UVGC_964_2242',
                         value = '-((FRCTdeltaZxstLstLxGstL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_964_2243 = Coupling(name = 'UVGC_964_2243',
                         value = '-((FRCTdeltaZxttLxtG*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_964_2244 = Coupling(name = 'UVGC_964_2244',
                         value = '-((FRCTdeltaZxttLxgostL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_964_2245 = Coupling(name = 'UVGC_964_2245',
                         value = '-((FRCTdeltaZxttLxgostR*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_964_2246 = Coupling(name = 'UVGC_964_2246',
                         value = '-((FRCTdeltaZxstLstLxgot*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_965_2247 = Coupling(name = 'UVGC_965_2247',
                         value = '-((FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_965_2248 = Coupling(name = 'UVGC_965_2248',
                         value = '-((FRCTdeltaZxuuLxuG*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_965_2249 = Coupling(name = 'UVGC_965_2249',
                         value = '-((FRCTdeltaZxuuLxgosuL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_965_2250 = Coupling(name = 'UVGC_965_2250',
                         value = '-((FRCTdeltaZxsuLsuLxgou*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2251 = Coupling(name = 'UVGC_966_2251',
                         value = '(FRCTdeltaxaSxgo*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2252 = Coupling(name = 'UVGC_966_2252',
                         value = '(FRCTdeltaxaSxsbL*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2253 = Coupling(name = 'UVGC_966_2253',
                         value = '(FRCTdeltaxaSxsbR*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2254 = Coupling(name = 'UVGC_966_2254',
                         value = '(FRCTdeltaxaSxscL*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2255 = Coupling(name = 'UVGC_966_2255',
                         value = '(FRCTdeltaxaSxscR*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2256 = Coupling(name = 'UVGC_966_2256',
                         value = '(FRCTdeltaxaSxsdL*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2257 = Coupling(name = 'UVGC_966_2257',
                         value = '(FRCTdeltaxaSxsdR*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2258 = Coupling(name = 'UVGC_966_2258',
                         value = '(FRCTdeltaxaSxssL*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2259 = Coupling(name = 'UVGC_966_2259',
                         value = '(FRCTdeltaxaSxssR*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2260 = Coupling(name = 'UVGC_966_2260',
                         value = '(FRCTdeltaxaSxstL*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2261 = Coupling(name = 'UVGC_966_2261',
                         value = '(FRCTdeltaxaSxstR*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2262 = Coupling(name = 'UVGC_966_2262',
                         value = '(FRCTdeltaxaSxsuL*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2263 = Coupling(name = 'UVGC_966_2263',
                         value = '(FRCTdeltaxaSxsuR*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2264 = Coupling(name = 'UVGC_966_2264',
                         value = '(FRCTdeltaxaSxt*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2265 = Coupling(name = 'UVGC_966_2265',
                         value = '(FRCTdeltaZxbbRxbG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2266 = Coupling(name = 'UVGC_966_2266',
                         value = '(FRCTdeltaZxsbRsbRxgob*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2267 = Coupling(name = 'UVGC_966_2267',
                         value = '(FRCTdeltaZxgogoLxbsbL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2268 = Coupling(name = 'UVGC_966_2268',
                         value = '(FRCTdeltaZxgogoLxbsbR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2269 = Coupling(name = 'UVGC_966_2269',
                         value = '(FRCTdeltaZxgogoLxcscL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2270 = Coupling(name = 'UVGC_966_2270',
                         value = '(FRCTdeltaZxgogoLxcscR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2271 = Coupling(name = 'UVGC_966_2271',
                         value = '(FRCTdeltaZxgogoLxdsdL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2272 = Coupling(name = 'UVGC_966_2272',
                         value = '(FRCTdeltaZxgogoLxdsdR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2273 = Coupling(name = 'UVGC_966_2273',
                         value = '(FRCTdeltaZxgogoLxgoG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2274 = Coupling(name = 'UVGC_966_2274',
                         value = '(FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2275 = Coupling(name = 'UVGC_966_2275',
                         value = '(FRCTdeltaZxbbRxgosbR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2276 = Coupling(name = 'UVGC_966_2276',
                         value = '(FRCTdeltaZxgogoLxsssL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2277 = Coupling(name = 'UVGC_966_2277',
                         value = '(FRCTdeltaZxgogoLxsssR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2278 = Coupling(name = 'UVGC_966_2278',
                         value = '(FRCTdeltaZxgogoLxtstL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2279 = Coupling(name = 'UVGC_966_2279',
                         value = '(FRCTdeltaZxgogoLxtstR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2280 = Coupling(name = 'UVGC_966_2280',
                         value = '(FRCTdeltaZxgogoLxusuL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2281 = Coupling(name = 'UVGC_966_2281',
                         value = '(FRCTdeltaZxgogoLxusuR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_966_2282 = Coupling(name = 'UVGC_966_2282',
                         value = '(-3*complex(0,1)*G**3*invFREps)/(4.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2283 = Coupling(name = 'UVGC_966_2283',
                         value = '(complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_966_2284 = Coupling(name = 'UVGC_966_2284',
                         value = '(-3*complex(0,1)*G**3*invFREps)/(16.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_967_2285 = Coupling(name = 'UVGC_967_2285',
                         value = '(FRCTdeltaZxccRxcG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_967_2286 = Coupling(name = 'UVGC_967_2286',
                         value = '(FRCTdeltaZxscRscRxgoc*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_967_2287 = Coupling(name = 'UVGC_967_2287',
                         value = '(FRCTdeltaZxscRscRxGscR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_967_2288 = Coupling(name = 'UVGC_967_2288',
                         value = '(FRCTdeltaZxccRxgoscR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_968_2289 = Coupling(name = 'UVGC_968_2289',
                         value = '(FRCTdeltaZxddRxdG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_968_2290 = Coupling(name = 'UVGC_968_2290',
                         value = '(FRCTdeltaZxsdRsdRxgod*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_968_2291 = Coupling(name = 'UVGC_968_2291',
                         value = '(FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_968_2292 = Coupling(name = 'UVGC_968_2292',
                         value = '(FRCTdeltaZxddRxgosdR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_969_2293 = Coupling(name = 'UVGC_969_2293',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_969_2294 = Coupling(name = 'UVGC_969_2294',
                         value = '(FRCTdeltaZxssRssRxGssR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_969_2295 = Coupling(name = 'UVGC_969_2295',
                         value = '(FRCTdeltaZxssRssRxgos*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_969_2296 = Coupling(name = 'UVGC_969_2296',
                         value = '(FRCTdeltaZxssRxgossR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_970_2297 = Coupling(name = 'UVGC_970_2297',
                         value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_970_2298 = Coupling(name = 'UVGC_970_2298',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_970_2299 = Coupling(name = 'UVGC_970_2299',
                         value = '(FRCTdeltaZxttRxgostL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_970_2300 = Coupling(name = 'UVGC_970_2300',
                         value = '(FRCTdeltaZxttRxgostR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_970_2301 = Coupling(name = 'UVGC_970_2301',
                         value = '(FRCTdeltaZxstRstRxgot*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_971_2302 = Coupling(name = 'UVGC_971_2302',
                         value = '(FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_971_2303 = Coupling(name = 'UVGC_971_2303',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_971_2304 = Coupling(name = 'UVGC_971_2304',
                         value = '(FRCTdeltaZxuuRxgosuR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_971_2305 = Coupling(name = 'UVGC_971_2305',
                         value = '(FRCTdeltaZxsuRsuRxgou*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_972_2306 = Coupling(name = 'UVGC_972_2306',
                         value = '(cw*ee*FRCTdeltaZxbbRxbG*complex(0,1)*NN1x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_972_2307 = Coupling(name = 'UVGC_972_2307',
                         value = '(cw*ee*FRCTdeltaZxsbRsbRxgob*complex(0,1)*NN1x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_972_2308 = Coupling(name = 'UVGC_972_2308',
                         value = '(cw*ee*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*NN1x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_972_2309 = Coupling(name = 'UVGC_972_2309',
                         value = '(cw*ee*FRCTdeltaZxbbRxgosbR*complex(0,1)*NN1x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_972_2310 = Coupling(name = 'UVGC_972_2310',
                         value = '-(cw*ee*complex(0,1)*G**2*invFREps*NN1x1)/(18.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_973_2311 = Coupling(name = 'UVGC_973_2311',
                         value = '(cw*ee*FRCTdeltaZxddRxdG*complex(0,1)*NN1x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_973_2312 = Coupling(name = 'UVGC_973_2312',
                         value = '(cw*ee*FRCTdeltaZxsdRsdRxgod*complex(0,1)*NN1x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_973_2313 = Coupling(name = 'UVGC_973_2313',
                         value = '(cw*ee*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*NN1x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_973_2314 = Coupling(name = 'UVGC_973_2314',
                         value = '(cw*ee*FRCTdeltaZxddRxgosdR*complex(0,1)*NN1x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_974_2315 = Coupling(name = 'UVGC_974_2315',
                         value = '(cw*ee*FRCTdeltaZxssRxsG*complex(0,1)*NN1x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_974_2316 = Coupling(name = 'UVGC_974_2316',
                         value = '(cw*ee*FRCTdeltaZxssRssRxGssR*complex(0,1)*NN1x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_974_2317 = Coupling(name = 'UVGC_974_2317',
                         value = '(cw*ee*FRCTdeltaZxssRssRxgos*complex(0,1)*NN1x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_974_2318 = Coupling(name = 'UVGC_974_2318',
                         value = '(cw*ee*FRCTdeltaZxssRxgossR*complex(0,1)*NN1x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_975_2319 = Coupling(name = 'UVGC_975_2319',
                         value = '-(cw*ee*FRCTdeltaZxccRxcG*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_975_2320 = Coupling(name = 'UVGC_975_2320',
                         value = '-(cw*ee*FRCTdeltaZxscRscRxgoc*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_975_2321 = Coupling(name = 'UVGC_975_2321',
                         value = '-(cw*ee*FRCTdeltaZxscRscRxGscR*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_975_2322 = Coupling(name = 'UVGC_975_2322',
                         value = '-(cw*ee*FRCTdeltaZxccRxgoscR*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_975_2323 = Coupling(name = 'UVGC_975_2323',
                         value = '(cw*ee*complex(0,1)*G**2*invFREps*NN1x1)/(9.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_976_2324 = Coupling(name = 'UVGC_976_2324',
                         value = '-(cw*ee*FRCTdeltaZxstRstRxGstR*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_976_2325 = Coupling(name = 'UVGC_976_2325',
                         value = '-(cw*ee*FRCTdeltaZxttRxtG*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_976_2326 = Coupling(name = 'UVGC_976_2326',
                         value = '-(cw*ee*FRCTdeltaZxttRxgostL*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_976_2327 = Coupling(name = 'UVGC_976_2327',
                         value = '-(cw*ee*FRCTdeltaZxttRxgostR*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_976_2328 = Coupling(name = 'UVGC_976_2328',
                         value = '(FRCTdeltaZxstLstRxgot*complex(0,1)*NN1x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstRxgot*complex(0,1)*NN1x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxstRstRxgot*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_977_2329 = Coupling(name = 'UVGC_977_2329',
                         value = '-(cw*ee*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_977_2330 = Coupling(name = 'UVGC_977_2330',
                         value = '-(cw*ee*FRCTdeltaZxuuRxuG*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_977_2331 = Coupling(name = 'UVGC_977_2331',
                         value = '-(cw*ee*FRCTdeltaZxuuRxgosuR*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_977_2332 = Coupling(name = 'UVGC_977_2332',
                         value = '-(cw*ee*FRCTdeltaZxsuRsuRxgou*complex(0,1)*NN1x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_978_2333 = Coupling(name = 'UVGC_978_2333',
                         value = '(cw*ee*FRCTdeltaZxbbRxbG*complex(0,1)*NN2x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_978_2334 = Coupling(name = 'UVGC_978_2334',
                         value = '(cw*ee*FRCTdeltaZxsbRsbRxgob*complex(0,1)*NN2x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_978_2335 = Coupling(name = 'UVGC_978_2335',
                         value = '(cw*ee*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*NN2x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_978_2336 = Coupling(name = 'UVGC_978_2336',
                         value = '(cw*ee*FRCTdeltaZxbbRxgosbR*complex(0,1)*NN2x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_978_2337 = Coupling(name = 'UVGC_978_2337',
                         value = '-(cw*ee*complex(0,1)*G**2*invFREps*NN2x1)/(18.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_979_2338 = Coupling(name = 'UVGC_979_2338',
                         value = '(cw*ee*FRCTdeltaZxddRxdG*complex(0,1)*NN2x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_979_2339 = Coupling(name = 'UVGC_979_2339',
                         value = '(cw*ee*FRCTdeltaZxsdRsdRxgod*complex(0,1)*NN2x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_979_2340 = Coupling(name = 'UVGC_979_2340',
                         value = '(cw*ee*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*NN2x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_979_2341 = Coupling(name = 'UVGC_979_2341',
                         value = '(cw*ee*FRCTdeltaZxddRxgosdR*complex(0,1)*NN2x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_980_2342 = Coupling(name = 'UVGC_980_2342',
                         value = '(cw*ee*FRCTdeltaZxssRxsG*complex(0,1)*NN2x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_980_2343 = Coupling(name = 'UVGC_980_2343',
                         value = '(cw*ee*FRCTdeltaZxssRssRxGssR*complex(0,1)*NN2x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_980_2344 = Coupling(name = 'UVGC_980_2344',
                         value = '(cw*ee*FRCTdeltaZxssRssRxgos*complex(0,1)*NN2x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_980_2345 = Coupling(name = 'UVGC_980_2345',
                         value = '(cw*ee*FRCTdeltaZxssRxgossR*complex(0,1)*NN2x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_981_2346 = Coupling(name = 'UVGC_981_2346',
                         value = '-(cw*ee*FRCTdeltaZxccRxcG*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_981_2347 = Coupling(name = 'UVGC_981_2347',
                         value = '-(cw*ee*FRCTdeltaZxscRscRxgoc*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_981_2348 = Coupling(name = 'UVGC_981_2348',
                         value = '-(cw*ee*FRCTdeltaZxscRscRxGscR*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_981_2349 = Coupling(name = 'UVGC_981_2349',
                         value = '-(cw*ee*FRCTdeltaZxccRxgoscR*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_981_2350 = Coupling(name = 'UVGC_981_2350',
                         value = '(cw*ee*complex(0,1)*G**2*invFREps*NN2x1)/(9.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_982_2351 = Coupling(name = 'UVGC_982_2351',
                         value = '-(cw*ee*FRCTdeltaZxstRstRxGstR*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_982_2352 = Coupling(name = 'UVGC_982_2352',
                         value = '-(cw*ee*FRCTdeltaZxttRxtG*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_982_2353 = Coupling(name = 'UVGC_982_2353',
                         value = '-(cw*ee*FRCTdeltaZxttRxgostL*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_982_2354 = Coupling(name = 'UVGC_982_2354',
                         value = '-(cw*ee*FRCTdeltaZxttRxgostR*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_982_2355 = Coupling(name = 'UVGC_982_2355',
                         value = '(FRCTdeltaZxstLstRxgot*complex(0,1)*NN2x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstRxgot*complex(0,1)*NN2x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxstRstRxgot*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_983_2356 = Coupling(name = 'UVGC_983_2356',
                         value = '-(cw*ee*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_983_2357 = Coupling(name = 'UVGC_983_2357',
                         value = '-(cw*ee*FRCTdeltaZxuuRxuG*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_983_2358 = Coupling(name = 'UVGC_983_2358',
                         value = '-(cw*ee*FRCTdeltaZxuuRxgosuR*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_983_2359 = Coupling(name = 'UVGC_983_2359',
                         value = '-(cw*ee*FRCTdeltaZxsuRsuRxgou*complex(0,1)*NN2x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_984_2360 = Coupling(name = 'UVGC_984_2360',
                         value = '(cw*ee*FRCTdeltaZxbbRxbG*complex(0,1)*NN3x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_984_2361 = Coupling(name = 'UVGC_984_2361',
                         value = '(cw*ee*FRCTdeltaZxsbRsbRxgob*complex(0,1)*NN3x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_984_2362 = Coupling(name = 'UVGC_984_2362',
                         value = '(cw*ee*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*NN3x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_984_2363 = Coupling(name = 'UVGC_984_2363',
                         value = '(cw*ee*FRCTdeltaZxbbRxgosbR*complex(0,1)*NN3x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_984_2364 = Coupling(name = 'UVGC_984_2364',
                         value = '-(cw*ee*complex(0,1)*G**2*invFREps*NN3x1)/(18.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_985_2365 = Coupling(name = 'UVGC_985_2365',
                         value = '(cw*ee*FRCTdeltaZxddRxdG*complex(0,1)*NN3x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_985_2366 = Coupling(name = 'UVGC_985_2366',
                         value = '(cw*ee*FRCTdeltaZxsdRsdRxgod*complex(0,1)*NN3x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_985_2367 = Coupling(name = 'UVGC_985_2367',
                         value = '(cw*ee*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*NN3x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_985_2368 = Coupling(name = 'UVGC_985_2368',
                         value = '(cw*ee*FRCTdeltaZxddRxgosdR*complex(0,1)*NN3x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_986_2369 = Coupling(name = 'UVGC_986_2369',
                         value = '(cw*ee*FRCTdeltaZxssRxsG*complex(0,1)*NN3x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_986_2370 = Coupling(name = 'UVGC_986_2370',
                         value = '(cw*ee*FRCTdeltaZxssRssRxGssR*complex(0,1)*NN3x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_986_2371 = Coupling(name = 'UVGC_986_2371',
                         value = '(cw*ee*FRCTdeltaZxssRssRxgos*complex(0,1)*NN3x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_986_2372 = Coupling(name = 'UVGC_986_2372',
                         value = '(cw*ee*FRCTdeltaZxssRxgossR*complex(0,1)*NN3x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_987_2373 = Coupling(name = 'UVGC_987_2373',
                         value = '-(cw*ee*FRCTdeltaZxccRxcG*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_987_2374 = Coupling(name = 'UVGC_987_2374',
                         value = '-(cw*ee*FRCTdeltaZxscRscRxgoc*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_987_2375 = Coupling(name = 'UVGC_987_2375',
                         value = '-(cw*ee*FRCTdeltaZxscRscRxGscR*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_987_2376 = Coupling(name = 'UVGC_987_2376',
                         value = '-(cw*ee*FRCTdeltaZxccRxgoscR*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_987_2377 = Coupling(name = 'UVGC_987_2377',
                         value = '(cw*ee*complex(0,1)*G**2*invFREps*NN3x1)/(9.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_988_2378 = Coupling(name = 'UVGC_988_2378',
                         value = '-(cw*ee*FRCTdeltaZxstRstRxGstR*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_988_2379 = Coupling(name = 'UVGC_988_2379',
                         value = '-(cw*ee*FRCTdeltaZxttRxtG*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_988_2380 = Coupling(name = 'UVGC_988_2380',
                         value = '-(cw*ee*FRCTdeltaZxttRxgostL*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_988_2381 = Coupling(name = 'UVGC_988_2381',
                         value = '-(cw*ee*FRCTdeltaZxttRxgostR*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_988_2382 = Coupling(name = 'UVGC_988_2382',
                         value = '(FRCTdeltaZxstLstRxgot*complex(0,1)*NN3x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstRxgot*complex(0,1)*NN3x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxstRstRxgot*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_989_2383 = Coupling(name = 'UVGC_989_2383',
                         value = '-(cw*ee*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_989_2384 = Coupling(name = 'UVGC_989_2384',
                         value = '-(cw*ee*FRCTdeltaZxuuRxuG*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_989_2385 = Coupling(name = 'UVGC_989_2385',
                         value = '-(cw*ee*FRCTdeltaZxuuRxgosuR*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_989_2386 = Coupling(name = 'UVGC_989_2386',
                         value = '-(cw*ee*FRCTdeltaZxsuRsuRxgou*complex(0,1)*NN3x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_990_2387 = Coupling(name = 'UVGC_990_2387',
                         value = '(cw*ee*FRCTdeltaZxbbRxbG*complex(0,1)*NN4x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_990_2388 = Coupling(name = 'UVGC_990_2388',
                         value = '(cw*ee*FRCTdeltaZxsbRsbRxgob*complex(0,1)*NN4x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_990_2389 = Coupling(name = 'UVGC_990_2389',
                         value = '(cw*ee*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*NN4x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_990_2390 = Coupling(name = 'UVGC_990_2390',
                         value = '(cw*ee*FRCTdeltaZxbbRxgosbR*complex(0,1)*NN4x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_990_2391 = Coupling(name = 'UVGC_990_2391',
                         value = '-(cw*ee*complex(0,1)*G**2*invFREps*NN4x1)/(18.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_991_2392 = Coupling(name = 'UVGC_991_2392',
                         value = '(cw*ee*FRCTdeltaZxddRxdG*complex(0,1)*NN4x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_991_2393 = Coupling(name = 'UVGC_991_2393',
                         value = '(cw*ee*FRCTdeltaZxsdRsdRxgod*complex(0,1)*NN4x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_991_2394 = Coupling(name = 'UVGC_991_2394',
                         value = '(cw*ee*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*NN4x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_991_2395 = Coupling(name = 'UVGC_991_2395',
                         value = '(cw*ee*FRCTdeltaZxddRxgosdR*complex(0,1)*NN4x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_992_2396 = Coupling(name = 'UVGC_992_2396',
                         value = '(cw*ee*FRCTdeltaZxssRxsG*complex(0,1)*NN4x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_992_2397 = Coupling(name = 'UVGC_992_2397',
                         value = '(cw*ee*FRCTdeltaZxssRssRxGssR*complex(0,1)*NN4x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_992_2398 = Coupling(name = 'UVGC_992_2398',
                         value = '(cw*ee*FRCTdeltaZxssRssRxgos*complex(0,1)*NN4x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_992_2399 = Coupling(name = 'UVGC_992_2399',
                         value = '(cw*ee*FRCTdeltaZxssRxgossR*complex(0,1)*NN4x1)/(3.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_993_2400 = Coupling(name = 'UVGC_993_2400',
                         value = '-(cw*ee*FRCTdeltaZxccRxcG*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_993_2401 = Coupling(name = 'UVGC_993_2401',
                         value = '-(cw*ee*FRCTdeltaZxscRscRxgoc*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_993_2402 = Coupling(name = 'UVGC_993_2402',
                         value = '-(cw*ee*FRCTdeltaZxscRscRxGscR*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_993_2403 = Coupling(name = 'UVGC_993_2403',
                         value = '-(cw*ee*FRCTdeltaZxccRxgoscR*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_993_2404 = Coupling(name = 'UVGC_993_2404',
                         value = '(cw*ee*complex(0,1)*G**2*invFREps*NN4x1)/(9.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_994_2405 = Coupling(name = 'UVGC_994_2405',
                         value = '-(cw*ee*FRCTdeltaZxstRstRxGstR*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_994_2406 = Coupling(name = 'UVGC_994_2406',
                         value = '-(cw*ee*FRCTdeltaZxttRxtG*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_994_2407 = Coupling(name = 'UVGC_994_2407',
                         value = '-(cw*ee*FRCTdeltaZxttRxgostL*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_994_2408 = Coupling(name = 'UVGC_994_2408',
                         value = '-(cw*ee*FRCTdeltaZxttRxgostR*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_994_2409 = Coupling(name = 'UVGC_994_2409',
                         value = '(FRCTdeltaZxstLstRxgot*complex(0,1)*NN4x4*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (FRCTdeltaZxstLstRxgot*complex(0,1)*NN4x4*sw**2*yu3x3)/(2.*(-1 + sw)*(1 + sw)) - (cw*ee*FRCTdeltaZxstRstRxgot*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw)*(1 + sw))',
                         order = {'QCD':2,'QED':1})

UVGC_995_2410 = Coupling(name = 'UVGC_995_2410',
                         value = '-(cw*ee*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_995_2411 = Coupling(name = 'UVGC_995_2411',
                         value = '-(cw*ee*FRCTdeltaZxuuRxuG*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_995_2412 = Coupling(name = 'UVGC_995_2412',
                         value = '-(cw*ee*FRCTdeltaZxuuRxgosuR*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_995_2413 = Coupling(name = 'UVGC_995_2413',
                         value = '-(cw*ee*FRCTdeltaZxsuRsuRxgou*complex(0,1)*NN4x1*cmath.sqrt(2))/(3.*(-1 + sw**2))',
                         order = {'QCD':2,'QED':1})

UVGC_996_2414 = Coupling(name = 'UVGC_996_2414',
                         value = '(cw*ee*FRCTdeltaZxbbLxbG*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxbbLxbG*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxbbLxbG*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_996_2415 = Coupling(name = 'UVGC_996_2415',
                         value = '(cw*ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsbLsbLxgob*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_996_2416 = Coupling(name = 'UVGC_996_2416',
                         value = '(cw*ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_996_2417 = Coupling(name = 'UVGC_996_2417',
                         value = '(cw*ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_996_2418 = Coupling(name = 'UVGC_996_2418',
                         value = '-(cw*ee*complex(0,1)*G**2*invFREps*NN1x1)/(36.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*invFREps*NN1x2)/(12.*cmath.pi**2*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*complex(0,1)*G**2*invFREps*NN1x2*sw)/(12.*cmath.pi**2*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_997_2419 = Coupling(name = 'UVGC_997_2419',
                         value = '(cw*ee*FRCTdeltaZxddLxdG*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxddLxdG*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxddLxdG*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_997_2420 = Coupling(name = 'UVGC_997_2420',
                         value = '(cw*ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsdLsdLxgod*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_997_2421 = Coupling(name = 'UVGC_997_2421',
                         value = '(cw*ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_997_2422 = Coupling(name = 'UVGC_997_2422',
                         value = '(cw*ee*FRCTdeltaZxddLxgosdL*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxddLxgosdL*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxddLxgosdL*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_998_2423 = Coupling(name = 'UVGC_998_2423',
                         value = '(cw*ee*FRCTdeltaZxssLxsG*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLxsG*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLxsG*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_998_2424 = Coupling(name = 'UVGC_998_2424',
                         value = '(cw*ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLssLxGssL*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_998_2425 = Coupling(name = 'UVGC_998_2425',
                         value = '(cw*ee*FRCTdeltaZxssLssLxgos*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLssLxgos*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLssLxgos*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_998_2426 = Coupling(name = 'UVGC_998_2426',
                         value = '(cw*ee*FRCTdeltaZxssLxgossL*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxssLxgossL*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxssLxgossL*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_999_2427 = Coupling(name = 'UVGC_999_2427',
                         value = '(cw*ee*FRCTdeltaZxccLxcG*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxccLxcG*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxccLxcG*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_999_2428 = Coupling(name = 'UVGC_999_2428',
                         value = '(cw*ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxscLscLxgoc*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_999_2429 = Coupling(name = 'UVGC_999_2429',
                         value = '(cw*ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxscLscLxGscL*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVGC_999_2430 = Coupling(name = 'UVGC_999_2430',
                         value = '(cw*ee*FRCTdeltaZxccLxgoscL*complex(0,1)*NN1x1)/(6.*(-1 + sw**2)*cmath.sqrt(2)) + (ee*FRCTdeltaZxccLxgoscL*complex(0,1)*NN1x2)/(2.*sw*(-1 + sw**2)*cmath.sqrt(2)) - (ee*FRCTdeltaZxccLxgoscL*complex(0,1)*NN1x2*sw)/(2.*(-1 + sw**2)*cmath.sqrt(2))',
                         order = {'QCD':2,'QED':1})

UVloopGC_1000_1 = Coupling(name = 'UVloopGC_1000_1',
                           value = '-(aS*cw*ee*complex(0,1)*NN1x1)/(18.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2)) - (aS*ee*complex(0,1)*NN1x2)/(6.*cmath.pi*(-1 + sw)*sw*(1 + sw)*cmath.sqrt(2)) + (aS*ee*complex(0,1)*NN1x2*sw)/(6.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                           order = {'QCD':2,'QED':1})

UVloopGC_1002_2 = Coupling(name = 'UVloopGC_1002_2',
                           value = '-(aS*cw*ee*complex(0,1)*NN2x1)/(18.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2)) + (aS*ee*complex(0,1)*NN2x2)/(6.*cmath.pi*(-1 + sw)*sw*(1 + sw)*cmath.sqrt(2)) - (aS*ee*complex(0,1)*NN2x2*sw)/(6.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                           order = {'QCD':2,'QED':1})

UVloopGC_1005_3 = Coupling(name = 'UVloopGC_1005_3',
                           value = '-(aS*cw*ee*complex(0,1)*NN2x1)/(18.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2)) - (aS*ee*complex(0,1)*NN2x2)/(6.*cmath.pi*(-1 + sw)*sw*(1 + sw)*cmath.sqrt(2)) + (aS*ee*complex(0,1)*NN2x2*sw)/(6.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                           order = {'QCD':2,'QED':1})

UVloopGC_1008_4 = Coupling(name = 'UVloopGC_1008_4',
                           value = '-(aS*cw*ee*complex(0,1)*NN3x1)/(18.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2)) + (aS*ee*complex(0,1)*NN3x2)/(6.*cmath.pi*(-1 + sw)*sw*(1 + sw)*cmath.sqrt(2)) - (aS*ee*complex(0,1)*NN3x2*sw)/(6.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                           order = {'QCD':2,'QED':1})

UVloopGC_1011_5 = Coupling(name = 'UVloopGC_1011_5',
                           value = '-(aS*cw*ee*complex(0,1)*NN3x1)/(18.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2)) - (aS*ee*complex(0,1)*NN3x2)/(6.*cmath.pi*(-1 + sw)*sw*(1 + sw)*cmath.sqrt(2)) + (aS*ee*complex(0,1)*NN3x2*sw)/(6.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                           order = {'QCD':2,'QED':1})

UVloopGC_1014_6 = Coupling(name = 'UVloopGC_1014_6',
                           value = '-(aS*cw*ee*complex(0,1)*NN4x1)/(18.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2)) + (aS*ee*complex(0,1)*NN4x2)/(6.*cmath.pi*(-1 + sw)*sw*(1 + sw)*cmath.sqrt(2)) - (aS*ee*complex(0,1)*NN4x2*sw)/(6.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                           order = {'QCD':2,'QED':1})

UVloopGC_1017_7 = Coupling(name = 'UVloopGC_1017_7',
                           value = '-(aS*cw*ee*complex(0,1)*NN4x1)/(18.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2)) - (aS*ee*complex(0,1)*NN4x2)/(6.*cmath.pi*(-1 + sw)*sw*(1 + sw)*cmath.sqrt(2)) + (aS*ee*complex(0,1)*NN4x2*sw)/(6.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                           order = {'QCD':2,'QED':1})

UVloopGC_1020_8 = Coupling(name = 'UVloopGC_1020_8',
                           value = '(aS*ee*complex(0,1)*UU1x1)/(6.*cmath.pi*sw)',
                           order = {'QCD':2,'QED':1})

UVloopGC_1023_9 = Coupling(name = 'UVloopGC_1023_9',
                           value = '(aS*ee*complex(0,1)*UU2x1)/(6.*cmath.pi*sw)',
                           order = {'QCD':2,'QED':1})

UVloopGC_1026_10 = Coupling(name = 'UVloopGC_1026_10',
                            value = '(aS*ee*complex(0,1)*VV1x1)/(6.*cmath.pi*sw)',
                            order = {'QCD':2,'QED':1})

UVloopGC_1029_11 = Coupling(name = 'UVloopGC_1029_11',
                            value = '(aS*ee*complex(0,1)*VV2x1)/(6.*cmath.pi*sw)',
                            order = {'QCD':2,'QED':1})

UVloopGC_1032_12 = Coupling(name = 'UVloopGC_1032_12',
                            value = '-(aS*complex(0,1)*NN1x4*yu3x3)/(6.*cmath.pi*(-1 + sw)*(1 + sw)) + (aS*complex(0,1)*NN1x4*sw**2*yu3x3)/(6.*cmath.pi*(-1 + sw)*(1 + sw))',
                            order = {'QCD':2,'QED':1})

UVloopGC_1034_13 = Coupling(name = 'UVloopGC_1034_13',
                            value = '-(aS*complex(0,1)*NN2x4*yu3x3)/(6.*cmath.pi*(-1 + sw)*(1 + sw)) + (aS*complex(0,1)*NN2x4*sw**2*yu3x3)/(6.*cmath.pi*(-1 + sw)*(1 + sw))',
                            order = {'QCD':2,'QED':1})

UVloopGC_1036_14 = Coupling(name = 'UVloopGC_1036_14',
                            value = '-(aS*complex(0,1)*NN3x4*yu3x3)/(6.*cmath.pi*(-1 + sw)*(1 + sw)) + (aS*complex(0,1)*NN3x4*sw**2*yu3x3)/(6.*cmath.pi*(-1 + sw)*(1 + sw))',
                            order = {'QCD':2,'QED':1})

UVloopGC_1038_15 = Coupling(name = 'UVloopGC_1038_15',
                            value = '-(aS*complex(0,1)*NN4x4*yu3x3)/(6.*cmath.pi*(-1 + sw)*(1 + sw)) + (aS*complex(0,1)*NN4x4*sw**2*yu3x3)/(6.*cmath.pi*(-1 + sw)*(1 + sw))',
                            order = {'QCD':2,'QED':1})

UVloopGC_1040_16 = Coupling(name = 'UVloopGC_1040_16',
                            value = '-(aS*complex(0,1)*VV1x2*yu3x3)/(6.*cmath.pi)',
                            order = {'QCD':2,'QED':1})

UVloopGC_1042_17 = Coupling(name = 'UVloopGC_1042_17',
                            value = '-(aS*complex(0,1)*VV2x2*yu3x3)/(6.*cmath.pi)',
                            order = {'QCD':2,'QED':1})

UVloopGC_958_18 = Coupling(name = 'UVloopGC_958_18',
                           value = '(aS*complex(0,1)*G**2)/(8.*cmath.pi)',
                           order = {'QCD':4})

UVloopGC_959_19 = Coupling(name = 'UVloopGC_959_19',
                           value = '(aS*complex(0,1)*G**2)/(4.*cmath.pi)',
                           order = {'QCD':4})

UVloopGC_960_20 = Coupling(name = 'UVloopGC_960_20',
                           value = '-(aS*complex(0,1)*G*cmath.sqrt(2))/(3.*cmath.pi)',
                           order = {'QCD':3})

UVloopGC_966_21 = Coupling(name = 'UVloopGC_966_21',
                           value = '(aS*complex(0,1)*G*cmath.sqrt(2))/(3.*cmath.pi)',
                           order = {'QCD':3})

UVloopGC_972_22 = Coupling(name = 'UVloopGC_972_22',
                           value = '-(aS*cw*ee*complex(0,1)*NN1x1)/(9.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                           order = {'QCD':2,'QED':1})

UVloopGC_975_23 = Coupling(name = 'UVloopGC_975_23',
                           value = '(aS*cw*ee*complex(0,1)*NN1x1*cmath.sqrt(2))/(9.*cmath.pi*(-1 + sw)*(1 + sw))',
                           order = {'QCD':2,'QED':1})

UVloopGC_978_24 = Coupling(name = 'UVloopGC_978_24',
                           value = '-(aS*cw*ee*complex(0,1)*NN2x1)/(9.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                           order = {'QCD':2,'QED':1})

UVloopGC_981_25 = Coupling(name = 'UVloopGC_981_25',
                           value = '(aS*cw*ee*complex(0,1)*NN2x1*cmath.sqrt(2))/(9.*cmath.pi*(-1 + sw)*(1 + sw))',
                           order = {'QCD':2,'QED':1})

UVloopGC_984_26 = Coupling(name = 'UVloopGC_984_26',
                           value = '-(aS*cw*ee*complex(0,1)*NN3x1)/(9.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                           order = {'QCD':2,'QED':1})

UVloopGC_987_27 = Coupling(name = 'UVloopGC_987_27',
                           value = '(aS*cw*ee*complex(0,1)*NN3x1*cmath.sqrt(2))/(9.*cmath.pi*(-1 + sw)*(1 + sw))',
                           order = {'QCD':2,'QED':1})

UVloopGC_990_28 = Coupling(name = 'UVloopGC_990_28',
                           value = '-(aS*cw*ee*complex(0,1)*NN4x1)/(9.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                           order = {'QCD':2,'QED':1})

UVloopGC_993_29 = Coupling(name = 'UVloopGC_993_29',
                           value = '(aS*cw*ee*complex(0,1)*NN4x1*cmath.sqrt(2))/(9.*cmath.pi*(-1 + sw)*(1 + sw))',
                           order = {'QCD':2,'QED':1})

UVloopGC_996_30 = Coupling(name = 'UVloopGC_996_30',
                           value = '-(aS*cw*ee*complex(0,1)*NN1x1)/(18.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2)) + (aS*ee*complex(0,1)*NN1x2)/(6.*cmath.pi*(-1 + sw)*sw*(1 + sw)*cmath.sqrt(2)) - (aS*ee*complex(0,1)*NN1x2*sw)/(6.*cmath.pi*(-1 + sw)*(1 + sw)*cmath.sqrt(2))',
                           order = {'QCD':2,'QED':1})

