# ----------------------------------------------------------------------  
# This model file was automatically created by SARAH version4.12.2 
# SARAH References: arXiv:0806.0538, arXiv:0909.2863, arXiv:1002.0840    
# (c) Florian Staub, 2011  
# ----------------------------------------------------------------------  
# File created at 13:15 on 30.10.2017   
# ----------------------------------------------------------------------  
 
 
from object_library import all_parameters,Parameter 
 
from function_library import complexconjugate,re,im,csc,sec,acsc,asec 
 
ZERO=Parameter(name='ZERO', 
                      nature='internal', 
                      type='real', 
                      value='0.0', 
                      texname='0') 
 
Mft = 	 Parameter(name = 'Mft', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1700., 
	 texname = '\\text{Mft}', 
	 lhablock = 'MASS', 
	 lhacode = [3035]) 
 
Wft = 	 Parameter(name = 'Wft', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wft}', 
	 lhablock = 'DECAY', 
	 lhacode = [3035]) 
 
Md1 = 	 Parameter(name = 'Md1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0035, 
	 texname = '\\text{Md1}', 
	 lhablock = 'MASS', 
	 lhacode = [1]) 
 
Md2 = 	 Parameter(name = 'Md2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.104, 
	 texname = '\\text{Md2}', 
	 lhablock = 'MASS', 
	 lhacode = [3]) 
 
Md3 = 	 Parameter(name = 'Md3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 4.2, 
	 texname = '\\text{Md3}', 
	 lhablock = 'MASS', 
	 lhacode = [5]) 
 
Mfdd1 = 	 Parameter(name = 'Mfdd1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1650., 
	 texname = '\\text{Mfdd1}', 
	 lhablock = 'MASS', 
	 lhacode = [3026]) 
 
Wfdd1 = 	 Parameter(name = 'Wfdd1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wfdd1}', 
	 lhablock = 'DECAY', 
	 lhacode = [3026]) 
 
Mfdd2 = 	 Parameter(name = 'Mfdd2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1660., 
	 texname = '\\text{Mfdd2}', 
	 lhablock = 'MASS', 
	 lhacode = [3027]) 
 
Wfdd2 = 	 Parameter(name = 'Wfdd2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wfdd2}', 
	 lhablock = 'DECAY', 
	 lhacode = [3027]) 
 
Mu1 = 	 Parameter(name = 'Mu1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0015, 
	 texname = '\\text{Mu1}', 
	 lhablock = 'MASS', 
	 lhacode = [2]) 
 
Mu2 = 	 Parameter(name = 'Mu2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1.27, 
	 texname = '\\text{Mu2}', 
	 lhablock = 'MASS', 
	 lhacode = [4]) 
 
Mu3 = 	 Parameter(name = 'Mu3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 171.2, 
	 texname = '\\text{Mu3}', 
	 lhablock = 'MASS', 
	 lhacode = [6]) 
 
Wu3 = 	 Parameter(name = 'Wu3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1.51, 
	 texname = '\\text{Wu3}', 
	 lhablock = 'DECAY', 
	 lhacode = [6]) 
 
Me1 = 	 Parameter(name = 'Me1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.000511, 
	 texname = '\\text{Me1}', 
	 lhablock = 'MASS', 
	 lhacode = [11]) 
 
Me2 = 	 Parameter(name = 'Me2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.105, 
	 texname = '\\text{Me2}', 
	 lhablock = 'MASS', 
	 lhacode = [13]) 
 
Me3 = 	 Parameter(name = 'Me3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1.776, 
	 texname = '\\text{Me3}', 
	 lhablock = 'MASS', 
	 lhacode = [15]) 
 
Mh1 = 	 Parameter(name = 'Mh1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 126.279, 
	 texname = '\\text{Mh1}', 
	 lhablock = 'MASS', 
	 lhacode = [25]) 
 
Wh1 = 	 Parameter(name = 'Wh1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wh1}', 
	 lhablock = 'DECAY', 
	 lhacode = [25]) 
 
Mh2 = 	 Parameter(name = 'Mh2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1804.36, 
	 texname = '\\text{Mh2}', 
	 lhablock = 'MASS', 
	 lhacode = [35]) 
 
Wh2 = 	 Parameter(name = 'Wh2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wh2}', 
	 lhablock = 'DECAY', 
	 lhacode = [35]) 
 
Mh3 = 	 Parameter(name = 'Mh3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 2474.02, 
	 texname = '\\text{Mh3}', 
	 lhablock = 'MASS', 
	 lhacode = [1045]) 
 
Wh3 = 	 Parameter(name = 'Wh3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wh3}', 
	 lhablock = 'DECAY', 
	 lhacode = [1045]) 
 
Mh4 = 	 Parameter(name = 'Mh4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 6499.83, 
	 texname = '\\text{Mh4}', 
	 lhablock = 'MASS', 
	 lhacode = [1055]) 
 
Wh4 = 	 Parameter(name = 'Wh4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wh4}', 
	 lhablock = 'DECAY', 
	 lhacode = [1055]) 
 
Mh5 = 	 Parameter(name = 'Mh5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 6528.11, 
	 texname = '\\text{Mh5}', 
	 lhablock = 'MASS', 
	 lhacode = [1065]) 
 
Wh5 = 	 Parameter(name = 'Wh5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{Wh5}', 
	 lhablock = 'DECAY', 
	 lhacode = [1065]) 
 
MAh3 = 	 Parameter(name = 'MAh3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1804.13, 
	 texname = '\\text{MAh3}', 
	 lhablock = 'MASS', 
	 lhacode = [36]) 
 
WAh3 = 	 Parameter(name = 'WAh3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WAh3}', 
	 lhablock = 'DECAY', 
	 lhacode = [36]) 
 
MAh4 = 	 Parameter(name = 'MAh4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 6495.98, 
	 texname = '\\text{MAh4}', 
	 lhablock = 'MASS', 
	 lhacode = [1046]) 
 
WAh4 = 	 Parameter(name = 'WAh4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WAh4}', 
	 lhablock = 'DECAY', 
	 lhacode = [1046]) 
 
MAh5 = 	 Parameter(name = 'MAh5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 6528.11, 
	 texname = '\\text{MAh5}', 
	 lhablock = 'MASS', 
	 lhacode = [1056]) 
 
WAh5 = 	 Parameter(name = 'WAh5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WAh5}', 
	 lhablock = 'DECAY', 
	 lhacode = [1056]) 
 
MHpm3 = 	 Parameter(name = 'MHpm3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1804.5, 
	 texname = '\\text{MHpm3}', 
	 lhablock = 'MASS', 
	 lhacode = [37]) 
 
WHpm3 = 	 Parameter(name = 'WHpm3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WHpm3}', 
	 lhablock = 'DECAY', 
	 lhacode = [37]) 
 
MHpm4 = 	 Parameter(name = 'MHpm4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1873.45, 
	 texname = '\\text{MHpm4}', 
	 lhablock = 'MASS', 
	 lhacode = [39]) 
 
WHpm4 = 	 Parameter(name = 'WHpm4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WHpm4}', 
	 lhablock = 'DECAY', 
	 lhacode = [39]) 
 
MHpm5 = 	 Parameter(name = 'MHpm5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 6498.11, 
	 texname = '\\text{MHpm5}', 
	 lhablock = 'MASS', 
	 lhacode = [1037]) 
 
WHpm5 = 	 Parameter(name = 'WHpm5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WHpm5}', 
	 lhablock = 'DECAY', 
	 lhacode = [1037]) 
 
MHpm6 = 	 Parameter(name = 'MHpm6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 6527.9, 
	 texname = '\\text{MHpm6}', 
	 lhablock = 'MASS', 
	 lhacode = [1039]) 
 
WHpm6 = 	 Parameter(name = 'WHpm6', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WHpm6}', 
	 lhablock = 'DECAY', 
	 lhacode = [1039]) 
 
MDpm2 = 	 Parameter(name = 'MDpm2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 878.269, 
	 texname = '\\text{MDpm2}', 
	 lhablock = 'MASS', 
	 lhacode = [43]) 
 
WDpm2 = 	 Parameter(name = 'WDpm2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WDpm2}', 
	 lhablock = 'DECAY', 
	 lhacode = [43]) 
 
MDpm3 = 	 Parameter(name = 'MDpm3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 6464.3, 
	 texname = '\\text{MDpm3}', 
	 lhablock = 'MASS', 
	 lhacode = [1051]) 
 
WDpm3 = 	 Parameter(name = 'WDpm3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WDpm3}', 
	 lhablock = 'DECAY', 
	 lhacode = [1051]) 
 
MDpm4 = 	 Parameter(name = 'MDpm4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 6527.74, 
	 texname = '\\text{MDpm4}', 
	 lhablock = 'MASS', 
	 lhacode = [1061]) 
 
WDpm4 = 	 Parameter(name = 'WDpm4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WDpm4}', 
	 lhablock = 'DECAY', 
	 lhacode = [1061]) 
 
MZ = 	 Parameter(name = 'MZ', 
	 nature = 'external', 
	 type = 'real', 
	 value = 91.1876, 
	 texname = '\\text{MZ}', 
	 lhablock = 'MASS', 
	 lhacode = [23]) 
 
WZ = 	 Parameter(name = 'WZ', 
	 nature = 'external', 
	 type = 'real', 
	 value = 2.4952, 
	 texname = '\\text{WZ}', 
	 lhablock = 'DECAY', 
	 lhacode = [23]) 
 
MZp = 	 Parameter(name = 'MZp', 
	 nature = 'external', 
	 type = 'real', 
	 value = 3247.62, 
	 texname = '\\text{MZp}', 
	 lhablock = 'MASS', 
	 lhacode = [2023]) 
 
WZp = 	 Parameter(name = 'WZp', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WZp}', 
	 lhablock = 'DECAY', 
	 lhacode = [2023]) 
 
MYpp = 	 Parameter(name = 'MYpp', 
	 nature = 'external', 
	 type = 'real', 
	 value = 878.269, 
	 texname = '\\text{MYpp}', 
	 lhablock = 'MASS', 
	 lhacode = [44]) 
 
WYpp = 	 Parameter(name = 'WYpp', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WYpp}', 
	 lhablock = 'DECAY', 
	 lhacode = [44]) 
 
WWp = 	 Parameter(name = 'WWp', 
	 nature = 'external', 
	 type = 'real', 
	 value = 2.141, 
	 texname = '\\text{WWp}', 
	 lhablock = 'DECAY', 
	 lhacode = [24]) 
 
MXp = 	 Parameter(name = 'MXp', 
	 nature = 'external', 
	 type = 'real', 
	 value = 881.76, 
	 texname = '\\text{MXp}', 
	 lhablock = 'MASS', 
	 lhacode = [224]) 
 
WXp = 	 Parameter(name = 'WXp', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{WXp}', 
	 lhablock = 'DECAY', 
	 lhacode = [224]) 
 
g1 = 	 Parameter(name='g1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1.14442, 
	 texname = '\\text{g1}', 
	 lhablock = 'GAUGE', 
	 lhacode = [1] ) 
 
ryl11 = 	 Parameter(name='ryl11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0000686619, 
	 texname = '\\text{yl11}', 
	 lhablock = 'YL', 
	 lhacode = [1, 1] ) 
 
iyl11 = 	 Parameter(name='iyl11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl11}', 
	 lhablock = 'IMYL', 
	 lhacode = [1, 1] ) 
 
ryl12 = 	 Parameter(name='ryl12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl12}', 
	 lhablock = 'YL', 
	 lhacode = [1, 2] ) 
 
iyl12 = 	 Parameter(name='iyl12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl12}', 
	 lhablock = 'IMYL', 
	 lhacode = [1, 2] ) 
 
ryl13 = 	 Parameter(name='ryl13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl13}', 
	 lhablock = 'YL', 
	 lhacode = [1, 3] ) 
 
iyl13 = 	 Parameter(name='iyl13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl13}', 
	 lhablock = 'IMYL', 
	 lhacode = [1, 3] ) 
 
ryl21 = 	 Parameter(name='ryl21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl21}', 
	 lhablock = 'YL', 
	 lhacode = [2, 1] ) 
 
iyl21 = 	 Parameter(name='iyl21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl21}', 
	 lhablock = 'IMYL', 
	 lhacode = [2, 1] ) 
 
ryl22 = 	 Parameter(name='ryl22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0141086, 
	 texname = '\\text{yl22}', 
	 lhablock = 'YL', 
	 lhacode = [2, 2] ) 
 
iyl22 = 	 Parameter(name='iyl22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl22}', 
	 lhablock = 'IMYL', 
	 lhacode = [2, 2] ) 
 
ryl23 = 	 Parameter(name='ryl23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl23}', 
	 lhablock = 'YL', 
	 lhacode = [2, 3] ) 
 
iyl23 = 	 Parameter(name='iyl23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl23}', 
	 lhablock = 'IMYL', 
	 lhacode = [2, 3] ) 
 
ryl31 = 	 Parameter(name='ryl31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl31}', 
	 lhablock = 'YL', 
	 lhacode = [3, 1] ) 
 
iyl31 = 	 Parameter(name='iyl31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl31}', 
	 lhablock = 'IMYL', 
	 lhacode = [3, 1] ) 
 
ryl32 = 	 Parameter(name='ryl32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl32}', 
	 lhablock = 'YL', 
	 lhacode = [3, 2] ) 
 
iyl32 = 	 Parameter(name='iyl32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl32}', 
	 lhablock = 'IMYL', 
	 lhacode = [3, 2] ) 
 
ryl33 = 	 Parameter(name='ryl33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.238637, 
	 texname = '\\text{yl33}', 
	 lhablock = 'YL', 
	 lhacode = [3, 3] ) 
 
iyl33 = 	 Parameter(name='iyl33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yl33}', 
	 lhablock = 'IMYL', 
	 lhacode = [3, 3] ) 
 
ryd11 = 	 Parameter(name='ryd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0000202965, 
	 texname = '\\text{yd11}', 
	 lhablock = 'YD1', 
	 lhacode = [1] ) 
 
iyd11 = 	 Parameter(name='iyd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd11}', 
	 lhablock = 'IMYD1', 
	 lhacode = [1] ) 
 
ryd12 = 	 Parameter(name='ryd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd12}', 
	 lhablock = 'YD1', 
	 lhacode = [2] ) 
 
iyd12 = 	 Parameter(name='iyd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd12}', 
	 lhablock = 'IMYD1', 
	 lhacode = [2] ) 
 
ryd13 = 	 Parameter(name='ryd13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd13}', 
	 lhablock = 'YD1', 
	 lhacode = [3] ) 
 
iyd13 = 	 Parameter(name='iyd13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd13}', 
	 lhablock = 'IMYD1', 
	 lhacode = [3] ) 
 
ryj11 = 	 Parameter(name='ryj11', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.865646, 
	 texname = '\\text{yj11}', 
	 lhablock = 'YJ1', 
	 lhacode = [1] ) 
 
iyj11 = 	 Parameter(name='iyj11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yj11}', 
	 lhablock = 'IMYJ1', 
	 lhacode = [1] ) 
 
ryj12 = 	 Parameter(name='ryj12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yj12}', 
	 lhablock = 'YJ1', 
	 lhacode = [2] ) 
 
iyj12 = 	 Parameter(name='iyj12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yj12}', 
	 lhablock = 'IMYJ1', 
	 lhacode = [2] ) 
 
ryu11 = 	 Parameter(name='ryu11', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.00005647, 
	 texname = '\\text{yu11}', 
	 lhablock = 'YU1', 
	 lhacode = [1] ) 
 
iyu11 = 	 Parameter(name='iyu11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu11}', 
	 lhablock = 'IMYU1', 
	 lhacode = [1] ) 
 
ryu12 = 	 Parameter(name='ryu12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu12}', 
	 lhablock = 'YU1', 
	 lhacode = [2] ) 
 
iyu12 = 	 Parameter(name='iyu12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu12}', 
	 lhablock = 'IMYU1', 
	 lhacode = [2] ) 
 
ryu13 = 	 Parameter(name='ryu13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu13}', 
	 lhablock = 'YU1', 
	 lhacode = [3] ) 
 
iyu13 = 	 Parameter(name='iyu13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu13}', 
	 lhablock = 'IMYU1', 
	 lhacode = [3] ) 
 
ryd21 = 	 Parameter(name='ryd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd21}', 
	 lhablock = 'YD2', 
	 lhacode = [1] ) 
 
iyd21 = 	 Parameter(name='iyd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd21}', 
	 lhablock = 'IMYD2', 
	 lhacode = [1] ) 
 
ryd22 = 	 Parameter(name='ryd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.000603095, 
	 texname = '\\text{yd22}', 
	 lhablock = 'YD2', 
	 lhacode = [2] ) 
 
iyd22 = 	 Parameter(name='iyd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd22}', 
	 lhablock = 'IMYD2', 
	 lhacode = [2] ) 
 
ryd23 = 	 Parameter(name='ryd23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd23}', 
	 lhablock = 'YD2', 
	 lhacode = [3] ) 
 
iyd23 = 	 Parameter(name='iyd23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd23}', 
	 lhablock = 'IMYD2', 
	 lhacode = [3] ) 
 
ryj21 = 	 Parameter(name='ryj21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yj21}', 
	 lhablock = 'YJ2', 
	 lhacode = [1] ) 
 
iyj21 = 	 Parameter(name='iyj21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yj21}', 
	 lhablock = 'IMYJ2', 
	 lhacode = [1] ) 
 
ryj22 = 	 Parameter(name='ryj22', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.870893, 
	 texname = '\\text{yj22}', 
	 lhablock = 'YJ2', 
	 lhacode = [2] ) 
 
iyj22 = 	 Parameter(name='iyj22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yj22}', 
	 lhablock = 'IMYJ2', 
	 lhacode = [2] ) 
 
ryu21 = 	 Parameter(name='ryu21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu21}', 
	 lhablock = 'YU2', 
	 lhacode = [1] ) 
 
iyu21 = 	 Parameter(name='iyu21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu21}', 
	 lhablock = 'IMYU2', 
	 lhacode = [1] ) 
 
ryu22 = 	 Parameter(name='ryu22', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0478113, 
	 texname = '\\text{yu22}', 
	 lhablock = 'YU2', 
	 lhacode = [2] ) 
 
iyu22 = 	 Parameter(name='iyu22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu22}', 
	 lhablock = 'IMYU2', 
	 lhacode = [2] ) 
 
ryu23 = 	 Parameter(name='ryu23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu23}', 
	 lhablock = 'YU2', 
	 lhacode = [3] ) 
 
iyu23 = 	 Parameter(name='iyu23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu23}', 
	 lhablock = 'IMYU2', 
	 lhacode = [3] ) 
 
ryd31 = 	 Parameter(name='ryd31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd31}', 
	 lhablock = 'YD3', 
	 lhacode = [1] ) 
 
iyd31 = 	 Parameter(name='iyd31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd31}', 
	 lhablock = 'IMYD3', 
	 lhacode = [1] ) 
 
ryd32 = 	 Parameter(name='ryd32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd32}', 
	 lhablock = 'YD3', 
	 lhacode = [2] ) 
 
iyd32 = 	 Parameter(name='iyd32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd32}', 
	 lhablock = 'IMYD3', 
	 lhacode = [2] ) 
 
ryd33 = 	 Parameter(name='ryd33', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.158116, 
	 texname = '\\text{yd33}', 
	 lhablock = 'YD3', 
	 lhacode = [3] ) 
 
iyd33 = 	 Parameter(name='iyd33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yd33}', 
	 lhablock = 'IMYD3', 
	 lhacode = [3] ) 
 
ryj3 = 	 Parameter(name='ryj3', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.891878, 
	 texname = '\\text{yj3}', 
	 lhablock = 'YJ3', 
	 lhacode = [1] ) 
 
iyj3 = 	 Parameter(name='iyj3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yj3}', 
	 lhablock = 'IMYJ3', 
	 lhacode = [1] ) 
 
ryu31 = 	 Parameter(name='ryu31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu31}', 
	 lhablock = 'YU3', 
	 lhacode = [1] ) 
 
iyu31 = 	 Parameter(name='iyu31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu31}', 
	 lhablock = 'IMYU3', 
	 lhacode = [1] ) 
 
ryu32 = 	 Parameter(name='ryu32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu32}', 
	 lhablock = 'YU3', 
	 lhacode = [2] ) 
 
iyu32 = 	 Parameter(name='iyu32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu32}', 
	 lhablock = 'IMYU3', 
	 lhacode = [2] ) 
 
ryu33 = 	 Parameter(name='ryu33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.992787, 
	 texname = '\\text{yu33}', 
	 lhablock = 'YU3', 
	 lhacode = [3] ) 
 
iyu33 = 	 Parameter(name='iyu33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{yu33}', 
	 lhablock = 'IMYU3', 
	 lhacode = [3] ) 
 
fcoup = 	 Parameter(name='fcoup', 
	 nature = 'external', 
	 type = 'real', 
	 value = 178.735, 
	 texname = '\\text{fcoup}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [5] ) 
 
rfsex = 	 Parameter(name='rfsex', 
	 nature = 'external', 
	 type = 'real', 
	 value = -2673.2, 
	 texname = '\\text{fsex}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [21] ) 
 
ifsex = 	 Parameter(name='ifsex', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{fsex}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [21] ) 
 
rl34T = 	 Parameter(name='rl34T', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.202838, 
	 texname = '\\text{l34T}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [22] ) 
 
il34T = 	 Parameter(name='il34T', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l34T}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [22] ) 
 
rl14T = 	 Parameter(name='rl14T', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0533771, 
	 texname = '\\text{l14T}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [23] ) 
 
il14T = 	 Parameter(name='il14T', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l14T}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [23] ) 
 
rl24T = 	 Parameter(name='rl24T', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.431491, 
	 texname = '\\text{l24T}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [24] ) 
 
il24T = 	 Parameter(name='il24T', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l24T}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [24] ) 
 
rl3 = 	 Parameter(name='rl3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0496244, 
	 texname = '\\text{l3}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [8] ) 
 
il3 = 	 Parameter(name='il3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l3}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [8] ) 
 
rl34 = 	 Parameter(name='rl34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.870525, 
	 texname = '\\text{l34}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [25] ) 
 
il34 = 	 Parameter(name='il34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l34}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [25] ) 
 
rl34t = 	 Parameter(name='rl34t', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.155722, 
	 texname = '\\text{l34t}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [26] ) 
 
il34t = 	 Parameter(name='il34t', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l34t}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [26] ) 
 
rl23t = 	 Parameter(name='rl23t', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0306102, 
	 texname = '\\text{l23t}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [14] ) 
 
il23t = 	 Parameter(name='il23t', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l23t}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [14] ) 
 
rl23 = 	 Parameter(name='rl23', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.813244, 
	 texname = '\\text{l23}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [13] ) 
 
il23 = 	 Parameter(name='il23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l23}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [13] ) 
 
rl2 = 	 Parameter(name='rl2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.181902, 
	 texname = '\\text{l2}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [7] ) 
 
il2 = 	 Parameter(name='il2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l2}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [7] ) 
 
rl24 = 	 Parameter(name='rl24', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.672969, 
	 texname = '\\text{l24}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [27] ) 
 
il24 = 	 Parameter(name='il24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l24}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [27] ) 
 
rl24t = 	 Parameter(name='rl24t', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.215339, 
	 texname = '\\text{l24t}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [28] ) 
 
il24t = 	 Parameter(name='il24t', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l24t}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [28] ) 
 
rl13t = 	 Parameter(name='rl13t', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.298875, 
	 texname = '\\text{l13t}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [15] ) 
 
il13t = 	 Parameter(name='il13t', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l13t}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [15] ) 
 
rl12t = 	 Parameter(name='rl12t', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.936633, 
	 texname = '\\text{l12t}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [16] ) 
 
il12t = 	 Parameter(name='il12t', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l12t}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [16] ) 
 
rl13 = 	 Parameter(name='rl13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.169069, 
	 texname = '\\text{l13}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [12] ) 
 
il13 = 	 Parameter(name='il13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l13}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [12] ) 
 
rl12 = 	 Parameter(name='rl12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.233378, 
	 texname = '\\text{l12}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [11] ) 
 
il12 = 	 Parameter(name='il12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l12}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [11] ) 
 
rl1 = 	 Parameter(name='rl1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.420734, 
	 texname = '\\text{l1}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [6] ) 
 
il1 = 	 Parameter(name='il1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l1}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [6] ) 
 
rl14 = 	 Parameter(name='rl14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.290522, 
	 texname = '\\text{l14}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [29] ) 
 
il14 = 	 Parameter(name='il14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l14}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [29] ) 
 
l14t = 	 Parameter(name='l14t', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.972327, 
	 texname = '\\text{l14t}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [30] ) 
 
rl44t = 	 Parameter(name='rl44t', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.882528, 
	 texname = '\\text{l44t}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [31] ) 
 
il44t = 	 Parameter(name='il44t', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l44t}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [31] ) 
 
rl44 = 	 Parameter(name='rl44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.429812, 
	 texname = '\\text{l44}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [32] ) 
 
il44 = 	 Parameter(name='il44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{l44}', 
	 lhablock = 'IMPOTENTIAL331', 
	 lhacode = [32] ) 
 
v1 = 	 Parameter(name='v1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 2695.62, 
	 texname = '\\text{v1}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [17] ) 
 
v2 = 	 Parameter(name='v2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 243.872, 
	 texname = '\\text{v2}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [18] ) 
 
v3 = 	 Parameter(name='v3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 37.5654, 
	 texname = '\\text{v3}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [19] ) 
 
v4 = 	 Parameter(name='v4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 7.44227, 
	 texname = '\\text{v4}', 
	 lhablock = 'POTENTIAL331', 
	 lhacode = [20] ) 
 
rphaset = 	 Parameter(name='rphaset', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{phaset}', 
	 lhablock = 'PHASET', 
	 lhacode = [1] ) 
 
iphaset = 	 Parameter(name='iphaset', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{phaset}', 
	 lhablock = 'IMPHASET', 
	 lhacode = [1] ) 
 
rZH11 = 	 Parameter(name='rZH11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.021802, 
	 texname = '\\text{ZH11}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [1, 1] ) 
 
iZH11 = 	 Parameter(name='iZH11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH11}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [1, 1] ) 
 
rZH12 = 	 Parameter(name='rZH12', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.987259, 
	 texname = '\\text{ZH12}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [1, 2] ) 
 
iZH12 = 	 Parameter(name='iZH12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH12}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [1, 2] ) 
 
rZH13 = 	 Parameter(name='rZH13', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.154637, 
	 texname = '\\text{ZH13}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [1, 3] ) 
 
iZH13 = 	 Parameter(name='iZH13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH13}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [1, 3] ) 
 
rZH14 = 	 Parameter(name='rZH14', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0305186, 
	 texname = '\\text{ZH14}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [1, 4] ) 
 
iZH14 = 	 Parameter(name='iZH14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH14}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [1, 4] ) 
 
rZH15 = 	 Parameter(name='rZH15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH15}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [1, 5] ) 
 
iZH15 = 	 Parameter(name='iZH15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH15}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [1, 5] ) 
 
rZH21 = 	 Parameter(name='rZH21', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0320964, 
	 texname = '\\text{ZH21}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [2, 1] ) 
 
iZH21 = 	 Parameter(name='iZH21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH21}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [2, 1] ) 
 
rZH22 = 	 Parameter(name='rZH22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.156882, 
	 texname = '\\text{ZH22}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [2, 2] ) 
 
iZH22 = 	 Parameter(name='iZH22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH22}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [2, 2] ) 
 
rZH23 = 	 Parameter(name='rZH23', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.969545, 
	 texname = '\\text{ZH23}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [2, 3] ) 
 
iZH23 = 	 Parameter(name='iZH23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH23}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [2, 3] ) 
 
rZH24 = 	 Parameter(name='rZH24', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.185309, 
	 texname = '\\text{ZH24}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [2, 4] ) 
 
iZH24 = 	 Parameter(name='iZH24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH24}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [2, 4] ) 
 
rZH25 = 	 Parameter(name='rZH25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH25}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [2, 5] ) 
 
iZH25 = 	 Parameter(name='iZH25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH25}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [2, 5] ) 
 
rZH31 = 	 Parameter(name='rZH31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.999242, 
	 texname = '\\text{ZH31}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [3, 1] ) 
 
iZH31 = 	 Parameter(name='iZH31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH31}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [3, 1] ) 
 
rZH32 = 	 Parameter(name='rZH32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0265764, 
	 texname = '\\text{ZH32}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [3, 2] ) 
 
iZH32 = 	 Parameter(name='iZH32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH32}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [3, 2] ) 
 
rZH33 = 	 Parameter(name='rZH33', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0283839, 
	 texname = '\\text{ZH33}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [3, 3] ) 
 
iZH33 = 	 Parameter(name='iZH33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH33}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [3, 3] ) 
 
rZH34 = 	 Parameter(name='rZH34', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.00206814, 
	 texname = '\\text{ZH34}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [3, 4] ) 
 
iZH34 = 	 Parameter(name='iZH34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH34}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [3, 4] ) 
 
rZH35 = 	 Parameter(name='rZH35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH35}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [3, 5] ) 
 
iZH35 = 	 Parameter(name='iZH35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH35}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [3, 5] ) 
 
rZH41 = 	 Parameter(name='rZH41', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.00327408, 
	 texname = '\\text{ZH41}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [4, 1] ) 
 
iZH41 = 	 Parameter(name='iZH41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH41}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [4, 1] ) 
 
rZH42 = 	 Parameter(name='rZH42', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.00102128, 
	 texname = '\\text{ZH42}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [4, 2] ) 
 
iZH42 = 	 Parameter(name='iZH42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH42}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [4, 2] ) 
 
rZH43 = 	 Parameter(name='rZH43', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.187786, 
	 texname = '\\text{ZH43}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [4, 3] ) 
 
iZH43 = 	 Parameter(name='iZH43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH43}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [4, 3] ) 
 
rZH44 = 	 Parameter(name='rZH44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.982204, 
	 texname = '\\text{ZH44}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [4, 4] ) 
 
iZH44 = 	 Parameter(name='iZH44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH44}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [4, 4] ) 
 
rZH45 = 	 Parameter(name='rZH45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZH45}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [4, 5] ) 
 
iZH45 = 	 Parameter(name='iZH45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH45}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [4, 5] ) 
 
rZH51 = 	 Parameter(name='rZH51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH51}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [5, 1] ) 
 
iZH51 = 	 Parameter(name='iZH51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH51}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [5, 1] ) 
 
rZH52 = 	 Parameter(name='rZH52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH52}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [5, 2] ) 
 
iZH52 = 	 Parameter(name='iZH52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH52}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [5, 2] ) 
 
rZH53 = 	 Parameter(name='rZH53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH53}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [5, 3] ) 
 
iZH53 = 	 Parameter(name='iZH53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH53}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [5, 3] ) 
 
rZH54 = 	 Parameter(name='rZH54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH54}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [5, 4] ) 
 
iZH54 = 	 Parameter(name='iZH54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH54}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [5, 4] ) 
 
rZH55 = 	 Parameter(name='rZH55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZH55}', 
	 lhablock = 'ZHMIX', 
	 lhacode = [5, 5] ) 
 
iZH55 = 	 Parameter(name='iZH55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZH55}', 
	 lhablock = 'IMZHMIX', 
	 lhacode = [5, 5] ) 
 
rZA11 = 	 Parameter(name='rZA11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.00212023, 
	 texname = '\\text{ZA11}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [1, 1] ) 
 
iZA11 = 	 Parameter(name='iZA11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA11}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [1, 1] ) 
 
rZA12 = 	 Parameter(name='rZA12', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.987896, 
	 texname = '\\text{ZA12}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [1, 2] ) 
 
iZA12 = 	 Parameter(name='iZA12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA12}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [1, 2] ) 
 
rZA13 = 	 Parameter(name='rZA13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.152143, 
	 texname = '\\text{ZA13}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [1, 3] ) 
 
iZA13 = 	 Parameter(name='iZA13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA13}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [1, 3] ) 
 
rZA14 = 	 Parameter(name='rZA14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0301477, 
	 texname = '\\text{ZA14}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [1, 4] ) 
 
iZA14 = 	 Parameter(name='iZA14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA14}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [1, 4] ) 
 
rZA15 = 	 Parameter(name='rZA15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA15}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [1, 5] ) 
 
iZA15 = 	 Parameter(name='iZA15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA15}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [1, 5] ) 
 
rZA21 = 	 Parameter(name='rZA21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.999903, 
	 texname = '\\text{ZA21}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [2, 1] ) 
 
iZA21 = 	 Parameter(name='iZA21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA21}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [2, 1] ) 
 
rZA22 = 	 Parameter(name='rZA22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA22}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [2, 2] ) 
 
iZA22 = 	 Parameter(name='iZA22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA22}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [2, 2] ) 
 
rZA23 = 	 Parameter(name='rZA23', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0139344, 
	 texname = '\\text{ZA23}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [2, 3] ) 
 
iZA23 = 	 Parameter(name='iZA23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA23}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [2, 3] ) 
 
rZA24 = 	 Parameter(name='rZA24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA24}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [2, 4] ) 
 
iZA24 = 	 Parameter(name='iZA24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA24}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [2, 4] ) 
 
rZA25 = 	 Parameter(name='rZA25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA25}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [2, 5] ) 
 
iZA25 = 	 Parameter(name='iZA25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA25}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [2, 5] ) 
 
rZA31 = 	 Parameter(name='rZA31', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0135206, 
	 texname = '\\text{ZA31}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [3, 1] ) 
 
iZA31 = 	 Parameter(name='iZA31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA31}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [3, 1] ) 
 
rZA32 = 	 Parameter(name='rZA32', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.155113, 
	 texname = '\\text{ZA32}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [3, 2] ) 
 
iZA32 = 	 Parameter(name='iZA32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA32}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [3, 2] ) 
 
rZA33 = 	 Parameter(name='rZA33', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.970212, 
	 texname = '\\text{ZA33}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [3, 3] ) 
 
iZA33 = 	 Parameter(name='iZA33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA33}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [3, 3] ) 
 
rZA34 = 	 Parameter(name='rZA34', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.185597, 
	 texname = '\\text{ZA34}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [3, 4] ) 
 
iZA34 = 	 Parameter(name='iZA34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA34}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [3, 4] ) 
 
rZA35 = 	 Parameter(name='rZA35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA35}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [3, 5] ) 
 
iZA35 = 	 Parameter(name='iZA35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA35}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [3, 5] ) 
 
rZA41 = 	 Parameter(name='rZA41', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.00262004, 
	 texname = '\\text{ZA41}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [4, 1] ) 
 
iZA41 = 	 Parameter(name='iZA41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA41}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [4, 1] ) 
 
rZA42 = 	 Parameter(name='rZA42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.00101244, 
	 texname = '\\text{ZA42}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [4, 2] ) 
 
iZA42 = 	 Parameter(name='iZA42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA42}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [4, 2] ) 
 
rZA43 = 	 Parameter(name='rZA43', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.188008, 
	 texname = '\\text{ZA43}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [4, 3] ) 
 
iZA43 = 	 Parameter(name='iZA43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA43}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [4, 3] ) 
 
rZA44 = 	 Parameter(name='rZA44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.982163, 
	 texname = '\\text{ZA44}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [4, 4] ) 
 
iZA44 = 	 Parameter(name='iZA44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA44}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [4, 4] ) 
 
rZA45 = 	 Parameter(name='rZA45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZA45}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [4, 5] ) 
 
iZA45 = 	 Parameter(name='iZA45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA45}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [4, 5] ) 
 
rZA51 = 	 Parameter(name='rZA51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA51}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [5, 1] ) 
 
iZA51 = 	 Parameter(name='iZA51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA51}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [5, 1] ) 
 
rZA52 = 	 Parameter(name='rZA52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA52}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [5, 2] ) 
 
iZA52 = 	 Parameter(name='iZA52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA52}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [5, 2] ) 
 
rZA53 = 	 Parameter(name='rZA53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA53}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [5, 3] ) 
 
iZA53 = 	 Parameter(name='iZA53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA53}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [5, 3] ) 
 
rZA54 = 	 Parameter(name='rZA54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA54}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [5, 4] ) 
 
iZA54 = 	 Parameter(name='iZA54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA54}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [5, 4] ) 
 
rZA55 = 	 Parameter(name='rZA55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZA55}', 
	 lhablock = 'ZAMIX', 
	 lhacode = [5, 5] ) 
 
iZA55 = 	 Parameter(name='iZA55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZA55}', 
	 lhablock = 'IMZAMIX', 
	 lhacode = [5, 5] ) 
 
rZP11 = 	 Parameter(name='rZP11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.995928, 
	 texname = '\\text{ZP11}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [1, 1] ) 
 
iZP11 = 	 Parameter(name='iZP11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP11}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [1, 1] ) 
 
rZP12 = 	 Parameter(name='rZP12', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0901015, 
	 texname = '\\text{ZP12}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [1, 2] ) 
 
iZP12 = 	 Parameter(name='iZP12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP12}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [1, 2] ) 
 
rZP13 = 	 Parameter(name='rZP13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.000981733, 
	 texname = '\\text{ZP13}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [1, 3] ) 
 
iZP13 = 	 Parameter(name='iZP13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP13}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [1, 3] ) 
 
rZP14 = 	 Parameter(name='rZP14', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.000151224, 
	 texname = '\\text{ZP14}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [1, 4] ) 
 
iZP14 = 	 Parameter(name='iZP14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP14}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [1, 4] ) 
 
rZP15 = 	 Parameter(name='rZP15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.00274963, 
	 texname = '\\text{ZP15}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [1, 5] ) 
 
iZP15 = 	 Parameter(name='iZP15', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP15}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [1, 5] ) 
 
rZP16 = 	 Parameter(name='rZP16', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0000299596, 
	 texname = '\\text{ZP16}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [1, 6] ) 
 
iZP16 = 	 Parameter(name='iZP16', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP16}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [1, 6] ) 
 
rZP21 = 	 Parameter(name='rZP21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.000989718, 
	 texname = '\\text{ZP21}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [2, 1] ) 
 
iZP21 = 	 Parameter(name='iZP21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP21}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [2, 1] ) 
 
rZP22 = 	 Parameter(name='rZP22', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0000895397, 
	 texname = '\\text{ZP22}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [2, 2] ) 
 
iZP22 = 	 Parameter(name='iZP22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP22}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [2, 2] ) 
 
rZP23 = 	 Parameter(name='rZP23', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.987894, 
	 texname = '\\text{ZP23}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [2, 3] ) 
 
iZP23 = 	 Parameter(name='iZP23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP23}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [2, 3] ) 
 
rZP24 = 	 Parameter(name='rZP24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.152172, 
	 texname = '\\text{ZP24}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [2, 4] ) 
 
iZP24 = 	 Parameter(name='iZP24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP24}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [2, 4] ) 
 
rZP25 = 	 Parameter(name='rZP25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP25}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [2, 5] ) 
 
iZP25 = 	 Parameter(name='iZP25', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP25}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [2, 5] ) 
 
rZP26 = 	 Parameter(name='rZP26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0301476, 
	 texname = '\\text{ZP26}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [2, 6] ) 
 
iZP26 = 	 Parameter(name='iZP26', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP26}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [2, 6] ) 
 
rZP31 = 	 Parameter(name='rZP31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP31}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [3, 1] ) 
 
iZP31 = 	 Parameter(name='iZP31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP31}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [3, 1] ) 
 
rZP32 = 	 Parameter(name='rZP32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP32}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [3, 2] ) 
 
iZP32 = 	 Parameter(name='iZP32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP32}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [3, 2] ) 
 
rZP33 = 	 Parameter(name='rZP33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.155127, 
	 texname = '\\text{ZP33}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [3, 3] ) 
 
iZP33 = 	 Parameter(name='iZP33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP33}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [3, 3] ) 
 
rZP34 = 	 Parameter(name='rZP34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.97033, 
	 texname = '\\text{ZP34}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [3, 4] ) 
 
iZP34 = 	 Parameter(name='iZP34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP34}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [3, 4] ) 
 
rZP35 = 	 Parameter(name='rZP35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP35}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [3, 5] ) 
 
iZP35 = 	 Parameter(name='iZP35', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP35}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [3, 5] ) 
 
rZP36 = 	 Parameter(name='rZP36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.185461, 
	 texname = '\\text{ZP36}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [3, 6] ) 
 
iZP36 = 	 Parameter(name='iZP36', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP36}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [3, 6] ) 
 
rZP41 = 	 Parameter(name='rZP41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0901011, 
	 texname = '\\text{ZP41}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [4, 1] ) 
 
iZP41 = 	 Parameter(name='iZP41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP41}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [4, 1] ) 
 
rZP42 = 	 Parameter(name='rZP42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.995933, 
	 texname = '\\text{ZP42}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [4, 2] ) 
 
iZP42 = 	 Parameter(name='iZP42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP42}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [4, 2] ) 
 
rZP43 = 	 Parameter(name='rZP43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP43}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [4, 3] ) 
 
iZP43 = 	 Parameter(name='iZP43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP43}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [4, 3] ) 
 
rZP44 = 	 Parameter(name='rZP44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP44}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [4, 4] ) 
 
iZP44 = 	 Parameter(name='iZP44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP44}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [4, 4] ) 
 
rZP45 = 	 Parameter(name='rZP45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.000286088, 
	 texname = '\\text{ZP45}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [4, 5] ) 
 
iZP45 = 	 Parameter(name='iZP45', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP45}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [4, 5] ) 
 
rZP46 = 	 Parameter(name='rZP46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP46}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [4, 6] ) 
 
iZP46 = 	 Parameter(name='iZP46', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP46}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [4, 6] ) 
 
rZP51 = 	 Parameter(name='rZP51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP51}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [5, 1] ) 
 
iZP51 = 	 Parameter(name='iZP51', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP51}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [5, 1] ) 
 
rZP52 = 	 Parameter(name='rZP52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP52}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [5, 2] ) 
 
iZP52 = 	 Parameter(name='iZP52', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP52}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [5, 2] ) 
 
rZP53 = 	 Parameter(name='rZP53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.00103105, 
	 texname = '\\text{ZP53}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [5, 3] ) 
 
iZP53 = 	 Parameter(name='iZP53', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP53}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [5, 3] ) 
 
rZP54 = 	 Parameter(name='rZP54', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.187893, 
	 texname = '\\text{ZP54}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [5, 4] ) 
 
iZP54 = 	 Parameter(name='iZP54', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP54}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [5, 4] ) 
 
rZP55 = 	 Parameter(name='rZP55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP55}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [5, 5] ) 
 
iZP55 = 	 Parameter(name='iZP55', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP55}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [5, 5] ) 
 
rZP56 = 	 Parameter(name='rZP56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.982189, 
	 texname = '\\text{ZP56}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [5, 6] ) 
 
iZP56 = 	 Parameter(name='iZP56', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP56}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [5, 6] ) 
 
rZP61 = 	 Parameter(name='rZP61', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.00276423, 
	 texname = '\\text{ZP61}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [6, 1] ) 
 
iZP61 = 	 Parameter(name='iZP61', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP61}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [6, 1] ) 
 
rZP62 = 	 Parameter(name='rZP62', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0000371778, 
	 texname = '\\text{ZP62}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [6, 2] ) 
 
iZP62 = 	 Parameter(name='iZP62', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP62}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [6, 2] ) 
 
rZP63 = 	 Parameter(name='rZP63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP63}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [6, 3] ) 
 
iZP63 = 	 Parameter(name='iZP63', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP63}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [6, 3] ) 
 
rZP64 = 	 Parameter(name='rZP64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP64}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [6, 4] ) 
 
iZP64 = 	 Parameter(name='iZP64', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP64}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [6, 4] ) 
 
rZP65 = 	 Parameter(name='rZP65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.999996, 
	 texname = '\\text{ZP65}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [6, 5] ) 
 
iZP65 = 	 Parameter(name='iZP65', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP65}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [6, 5] ) 
 
rZP66 = 	 Parameter(name='rZP66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP66}', 
	 lhablock = 'ZPMIX', 
	 lhacode = [6, 6] ) 
 
iZP66 = 	 Parameter(name='iZP66', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZP66}', 
	 lhablock = 'IMZPMIX', 
	 lhacode = [6, 6] ) 
 
rzd11 = 	 Parameter(name='rzd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.999888, 
	 texname = '\\text{zd11}', 
	 lhablock = 'ZD', 
	 lhacode = [1, 1] ) 
 
izd11 = 	 Parameter(name='izd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd11}', 
	 lhablock = 'IMZD', 
	 lhacode = [1, 1] ) 
 
rzd12 = 	 Parameter(name='rzd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0139342, 
	 texname = '\\text{zd12}', 
	 lhablock = 'ZD', 
	 lhacode = [1, 2] ) 
 
izd12 = 	 Parameter(name='izd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd12}', 
	 lhablock = 'IMZD', 
	 lhacode = [1, 2] ) 
 
rzd13 = 	 Parameter(name='rzd13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.00390403, 
	 texname = '\\text{zd13}', 
	 lhablock = 'ZD', 
	 lhacode = [1, 3] ) 
 
izd13 = 	 Parameter(name='izd13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd13}', 
	 lhablock = 'IMZD', 
	 lhacode = [1, 3] ) 
 
rzd14 = 	 Parameter(name='rzd14', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.00390403, 
	 texname = '\\text{zd14}', 
	 lhablock = 'ZD', 
	 lhacode = [1, 4] ) 
 
izd14 = 	 Parameter(name='izd14', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd14}', 
	 lhablock = 'IMZD', 
	 lhacode = [1, 4] ) 
 
rzd21 = 	 Parameter(name='rzd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0144688, 
	 texname = '\\text{zd21}', 
	 lhablock = 'ZD', 
	 lhacode = [2, 1] ) 
 
izd21 = 	 Parameter(name='izd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd21}', 
	 lhablock = 'IMZD', 
	 lhacode = [2, 1] ) 
 
rzd22 = 	 Parameter(name='rzd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.966301, 
	 texname = '\\text{zd22}', 
	 lhablock = 'ZD', 
	 lhacode = [2, 2] ) 
 
izd22 = 	 Parameter(name='izd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd22}', 
	 lhablock = 'IMZD', 
	 lhacode = [2, 2] ) 
 
rzd23 = 	 Parameter(name='rzd23', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.000193187, 
	 texname = '\\text{zd23}', 
	 lhablock = 'ZD', 
	 lhacode = [2, 3] ) 
 
izd23 = 	 Parameter(name='izd23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd23}', 
	 lhablock = 'IMZD', 
	 lhacode = [2, 3] ) 
 
rzd24 = 	 Parameter(name='rzd24', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.25701, 
	 texname = '\\text{zd24}', 
	 lhablock = 'ZD', 
	 lhacode = [2, 4] ) 
 
izd24 = 	 Parameter(name='izd24', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd24}', 
	 lhablock = 'IMZD', 
	 lhacode = [2, 4] ) 
 
rzd31 = 	 Parameter(name='rzd31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0000723748, 
	 texname = '\\text{zd31}', 
	 lhablock = 'ZD', 
	 lhacode = [3, 1] ) 
 
izd31 = 	 Parameter(name='izd31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd31}', 
	 lhablock = 'IMZD', 
	 lhacode = [3, 1] ) 
 
rzd32 = 	 Parameter(name='rzd32', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.256924, 
	 texname = '\\text{zd32}', 
	 lhablock = 'ZD', 
	 lhacode = [3, 2] ) 
 
izd32 = 	 Parameter(name='izd32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd32}', 
	 lhablock = 'IMZD', 
	 lhacode = [3, 2] ) 
 
rzd33 = 	 Parameter(name='rzd33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0304085, 
	 texname = '\\text{zd33}', 
	 lhablock = 'ZD', 
	 lhacode = [3, 3] ) 
 
izd33 = 	 Parameter(name='izd33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd33}', 
	 lhablock = 'IMZD', 
	 lhacode = [3, 3] ) 
 
rzd34 = 	 Parameter(name='rzd34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.965953, 
	 texname = '\\text{zd34}', 
	 lhablock = 'ZD', 
	 lhacode = [3, 4] ) 
 
izd34 = 	 Parameter(name='izd34', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd34}', 
	 lhablock = 'IMZD', 
	 lhacode = [3, 4] ) 
 
rzd41 = 	 Parameter(name='rzd41', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.00391043, 
	 texname = '\\text{zd41}', 
	 lhablock = 'ZD', 
	 lhacode = [4, 1] ) 
 
izd41 = 	 Parameter(name='izd41', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd41}', 
	 lhablock = 'IMZD', 
	 lhacode = [4, 1] ) 
 
rzd42 = 	 Parameter(name='rzd42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.00768403, 
	 texname = '\\text{zd42}', 
	 lhablock = 'ZD', 
	 lhacode = [4, 2] ) 
 
izd42 = 	 Parameter(name='izd42', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd42}', 
	 lhablock = 'IMZD', 
	 lhacode = [4, 2] ) 
 
rzd43 = 	 Parameter(name='rzd43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.99953, 
	 texname = '\\text{zd43}', 
	 lhablock = 'ZD', 
	 lhacode = [4, 3] ) 
 
izd43 = 	 Parameter(name='izd43', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd43}', 
	 lhablock = 'IMZD', 
	 lhacode = [4, 3] ) 
 
rzd44 = 	 Parameter(name='rzd44', 
	 nature = 'external', 
	 type = 'real', 
	 value = -0.0294215, 
	 texname = '\\text{zd44}', 
	 lhablock = 'ZD', 
	 lhacode = [4, 4] ) 
 
izd44 = 	 Parameter(name='izd44', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{zd44}', 
	 lhablock = 'IMZD', 
	 lhacode = [4, 4] ) 
 
rZDL11 = 	 Parameter(name='rZDL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZDL11}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [1, 1] ) 
 
iZDL11 = 	 Parameter(name='iZDL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL11}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [1, 1] ) 
 
rZDL12 = 	 Parameter(name='rZDL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL12}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [1, 2] ) 
 
iZDL12 = 	 Parameter(name='iZDL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL12}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [1, 2] ) 
 
rZDL13 = 	 Parameter(name='rZDL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL13}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [1, 3] ) 
 
iZDL13 = 	 Parameter(name='iZDL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL13}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [1, 3] ) 
 
rZDL21 = 	 Parameter(name='rZDL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL21}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [2, 1] ) 
 
iZDL21 = 	 Parameter(name='iZDL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL21}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [2, 1] ) 
 
rZDL22 = 	 Parameter(name='rZDL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZDL22}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [2, 2] ) 
 
iZDL22 = 	 Parameter(name='iZDL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL22}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [2, 2] ) 
 
rZDL23 = 	 Parameter(name='rZDL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL23}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [2, 3] ) 
 
iZDL23 = 	 Parameter(name='iZDL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL23}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [2, 3] ) 
 
rZDL31 = 	 Parameter(name='rZDL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL31}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [3, 1] ) 
 
iZDL31 = 	 Parameter(name='iZDL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL31}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [3, 1] ) 
 
rZDL32 = 	 Parameter(name='rZDL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL32}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [3, 2] ) 
 
iZDL32 = 	 Parameter(name='iZDL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL32}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [3, 2] ) 
 
rZDL33 = 	 Parameter(name='rZDL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZDL33}', 
	 lhablock = 'UDLMIX', 
	 lhacode = [3, 3] ) 
 
iZDL33 = 	 Parameter(name='iZDL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDL33}', 
	 lhablock = 'IMUDLMIX', 
	 lhacode = [3, 3] ) 
 
rZDR11 = 	 Parameter(name='rZDR11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZDR11}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [1, 1] ) 
 
iZDR11 = 	 Parameter(name='iZDR11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR11}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [1, 1] ) 
 
rZDR12 = 	 Parameter(name='rZDR12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR12}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [1, 2] ) 
 
iZDR12 = 	 Parameter(name='iZDR12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR12}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [1, 2] ) 
 
rZDR13 = 	 Parameter(name='rZDR13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR13}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [1, 3] ) 
 
iZDR13 = 	 Parameter(name='iZDR13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR13}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [1, 3] ) 
 
rZDR21 = 	 Parameter(name='rZDR21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR21}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [2, 1] ) 
 
iZDR21 = 	 Parameter(name='iZDR21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR21}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [2, 1] ) 
 
rZDR22 = 	 Parameter(name='rZDR22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZDR22}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [2, 2] ) 
 
iZDR22 = 	 Parameter(name='iZDR22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR22}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [2, 2] ) 
 
rZDR23 = 	 Parameter(name='rZDR23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR23}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [2, 3] ) 
 
iZDR23 = 	 Parameter(name='iZDR23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR23}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [2, 3] ) 
 
rZDR31 = 	 Parameter(name='rZDR31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR31}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [3, 1] ) 
 
iZDR31 = 	 Parameter(name='iZDR31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR31}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [3, 1] ) 
 
rZDR32 = 	 Parameter(name='rZDR32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR32}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [3, 2] ) 
 
iZDR32 = 	 Parameter(name='iZDR32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR32}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [3, 2] ) 
 
rZDR33 = 	 Parameter(name='rZDR33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZDR33}', 
	 lhablock = 'UDRMIX', 
	 lhacode = [3, 3] ) 
 
iZDR33 = 	 Parameter(name='iZDR33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZDR33}', 
	 lhablock = 'IMUDRMIX', 
	 lhacode = [3, 3] ) 
 
rvd11 = 	 Parameter(name='rvd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{vd11}', 
	 lhablock = 'VD', 
	 lhacode = [1, 1] ) 
 
ivd11 = 	 Parameter(name='ivd11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{vd11}', 
	 lhablock = 'IMVD', 
	 lhacode = [1, 1] ) 
 
rvd12 = 	 Parameter(name='rvd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{vd12}', 
	 lhablock = 'VD', 
	 lhacode = [1, 2] ) 
 
ivd12 = 	 Parameter(name='ivd12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{vd12}', 
	 lhablock = 'IMVD', 
	 lhacode = [1, 2] ) 
 
rvd21 = 	 Parameter(name='rvd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{vd21}', 
	 lhablock = 'VD', 
	 lhacode = [2, 1] ) 
 
ivd21 = 	 Parameter(name='ivd21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{vd21}', 
	 lhablock = 'IMVD', 
	 lhacode = [2, 1] ) 
 
rvd22 = 	 Parameter(name='rvd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{vd22}', 
	 lhablock = 'VD', 
	 lhacode = [2, 2] ) 
 
ivd22 = 	 Parameter(name='ivd22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{vd22}', 
	 lhablock = 'IMVD', 
	 lhacode = [2, 2] ) 
 
rud11 = 	 Parameter(name='rud11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ud11}', 
	 lhablock = 'UD', 
	 lhacode = [1, 1] ) 
 
iud11 = 	 Parameter(name='iud11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ud11}', 
	 lhablock = 'IMUD', 
	 lhacode = [1, 1] ) 
 
rud12 = 	 Parameter(name='rud12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ud12}', 
	 lhablock = 'UD', 
	 lhacode = [1, 2] ) 
 
iud12 = 	 Parameter(name='iud12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ud12}', 
	 lhablock = 'IMUD', 
	 lhacode = [1, 2] ) 
 
rud21 = 	 Parameter(name='rud21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ud21}', 
	 lhablock = 'UD', 
	 lhacode = [2, 1] ) 
 
iud21 = 	 Parameter(name='iud21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ud21}', 
	 lhablock = 'IMUD', 
	 lhacode = [2, 1] ) 
 
rud22 = 	 Parameter(name='rud22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ud22}', 
	 lhablock = 'UD', 
	 lhacode = [2, 2] ) 
 
iud22 = 	 Parameter(name='iud22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ud22}', 
	 lhablock = 'IMUD', 
	 lhacode = [2, 2] ) 
 
rZUL11 = 	 Parameter(name='rZUL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZUL11}', 
	 lhablock = 'UULMIX', 
	 lhacode = [1, 1] ) 
 
iZUL11 = 	 Parameter(name='iZUL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL11}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [1, 1] ) 
 
rZUL12 = 	 Parameter(name='rZUL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL12}', 
	 lhablock = 'UULMIX', 
	 lhacode = [1, 2] ) 
 
iZUL12 = 	 Parameter(name='iZUL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL12}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [1, 2] ) 
 
rZUL13 = 	 Parameter(name='rZUL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL13}', 
	 lhablock = 'UULMIX', 
	 lhacode = [1, 3] ) 
 
iZUL13 = 	 Parameter(name='iZUL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL13}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [1, 3] ) 
 
rZUL21 = 	 Parameter(name='rZUL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL21}', 
	 lhablock = 'UULMIX', 
	 lhacode = [2, 1] ) 
 
iZUL21 = 	 Parameter(name='iZUL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL21}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [2, 1] ) 
 
rZUL22 = 	 Parameter(name='rZUL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZUL22}', 
	 lhablock = 'UULMIX', 
	 lhacode = [2, 2] ) 
 
iZUL22 = 	 Parameter(name='iZUL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL22}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [2, 2] ) 
 
rZUL23 = 	 Parameter(name='rZUL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL23}', 
	 lhablock = 'UULMIX', 
	 lhacode = [2, 3] ) 
 
iZUL23 = 	 Parameter(name='iZUL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL23}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [2, 3] ) 
 
rZUL31 = 	 Parameter(name='rZUL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL31}', 
	 lhablock = 'UULMIX', 
	 lhacode = [3, 1] ) 
 
iZUL31 = 	 Parameter(name='iZUL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL31}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [3, 1] ) 
 
rZUL32 = 	 Parameter(name='rZUL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL32}', 
	 lhablock = 'UULMIX', 
	 lhacode = [3, 2] ) 
 
iZUL32 = 	 Parameter(name='iZUL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL32}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [3, 2] ) 
 
rZUL33 = 	 Parameter(name='rZUL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZUL33}', 
	 lhablock = 'UULMIX', 
	 lhacode = [3, 3] ) 
 
iZUL33 = 	 Parameter(name='iZUL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUL33}', 
	 lhablock = 'IMUULMIX', 
	 lhacode = [3, 3] ) 
 
rZUR11 = 	 Parameter(name='rZUR11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZUR11}', 
	 lhablock = 'UURMIX', 
	 lhacode = [1, 1] ) 
 
iZUR11 = 	 Parameter(name='iZUR11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR11}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [1, 1] ) 
 
rZUR12 = 	 Parameter(name='rZUR12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR12}', 
	 lhablock = 'UURMIX', 
	 lhacode = [1, 2] ) 
 
iZUR12 = 	 Parameter(name='iZUR12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR12}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [1, 2] ) 
 
rZUR13 = 	 Parameter(name='rZUR13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR13}', 
	 lhablock = 'UURMIX', 
	 lhacode = [1, 3] ) 
 
iZUR13 = 	 Parameter(name='iZUR13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR13}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [1, 3] ) 
 
rZUR21 = 	 Parameter(name='rZUR21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR21}', 
	 lhablock = 'UURMIX', 
	 lhacode = [2, 1] ) 
 
iZUR21 = 	 Parameter(name='iZUR21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR21}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [2, 1] ) 
 
rZUR22 = 	 Parameter(name='rZUR22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZUR22}', 
	 lhablock = 'UURMIX', 
	 lhacode = [2, 2] ) 
 
iZUR22 = 	 Parameter(name='iZUR22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR22}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [2, 2] ) 
 
rZUR23 = 	 Parameter(name='rZUR23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR23}', 
	 lhablock = 'UURMIX', 
	 lhacode = [2, 3] ) 
 
iZUR23 = 	 Parameter(name='iZUR23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR23}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [2, 3] ) 
 
rZUR31 = 	 Parameter(name='rZUR31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR31}', 
	 lhablock = 'UURMIX', 
	 lhacode = [3, 1] ) 
 
iZUR31 = 	 Parameter(name='iZUR31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR31}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [3, 1] ) 
 
rZUR32 = 	 Parameter(name='rZUR32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR32}', 
	 lhablock = 'UURMIX', 
	 lhacode = [3, 2] ) 
 
iZUR32 = 	 Parameter(name='iZUR32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR32}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [3, 2] ) 
 
rZUR33 = 	 Parameter(name='rZUR33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZUR33}', 
	 lhablock = 'UURMIX', 
	 lhacode = [3, 3] ) 
 
iZUR33 = 	 Parameter(name='iZUR33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZUR33}', 
	 lhablock = 'IMUURMIX', 
	 lhacode = [3, 3] ) 
 
rZEL11 = 	 Parameter(name='rZEL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZEL11}', 
	 lhablock = 'UELMIX', 
	 lhacode = [1, 1] ) 
 
iZEL11 = 	 Parameter(name='iZEL11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL11}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [1, 1] ) 
 
rZEL12 = 	 Parameter(name='rZEL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL12}', 
	 lhablock = 'UELMIX', 
	 lhacode = [1, 2] ) 
 
iZEL12 = 	 Parameter(name='iZEL12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL12}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [1, 2] ) 
 
rZEL13 = 	 Parameter(name='rZEL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL13}', 
	 lhablock = 'UELMIX', 
	 lhacode = [1, 3] ) 
 
iZEL13 = 	 Parameter(name='iZEL13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL13}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [1, 3] ) 
 
rZEL21 = 	 Parameter(name='rZEL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL21}', 
	 lhablock = 'UELMIX', 
	 lhacode = [2, 1] ) 
 
iZEL21 = 	 Parameter(name='iZEL21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL21}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [2, 1] ) 
 
rZEL22 = 	 Parameter(name='rZEL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZEL22}', 
	 lhablock = 'UELMIX', 
	 lhacode = [2, 2] ) 
 
iZEL22 = 	 Parameter(name='iZEL22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL22}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [2, 2] ) 
 
rZEL23 = 	 Parameter(name='rZEL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL23}', 
	 lhablock = 'UELMIX', 
	 lhacode = [2, 3] ) 
 
iZEL23 = 	 Parameter(name='iZEL23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL23}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [2, 3] ) 
 
rZEL31 = 	 Parameter(name='rZEL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL31}', 
	 lhablock = 'UELMIX', 
	 lhacode = [3, 1] ) 
 
iZEL31 = 	 Parameter(name='iZEL31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL31}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [3, 1] ) 
 
rZEL32 = 	 Parameter(name='rZEL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL32}', 
	 lhablock = 'UELMIX', 
	 lhacode = [3, 2] ) 
 
iZEL32 = 	 Parameter(name='iZEL32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL32}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [3, 2] ) 
 
rZEL33 = 	 Parameter(name='rZEL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZEL33}', 
	 lhablock = 'UELMIX', 
	 lhacode = [3, 3] ) 
 
iZEL33 = 	 Parameter(name='iZEL33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZEL33}', 
	 lhablock = 'IMUELMIX', 
	 lhacode = [3, 3] ) 
 
rZER11 = 	 Parameter(name='rZER11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZER11}', 
	 lhablock = 'UERMIX', 
	 lhacode = [1, 1] ) 
 
iZER11 = 	 Parameter(name='iZER11', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER11}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [1, 1] ) 
 
rZER12 = 	 Parameter(name='rZER12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER12}', 
	 lhablock = 'UERMIX', 
	 lhacode = [1, 2] ) 
 
iZER12 = 	 Parameter(name='iZER12', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER12}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [1, 2] ) 
 
rZER13 = 	 Parameter(name='rZER13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER13}', 
	 lhablock = 'UERMIX', 
	 lhacode = [1, 3] ) 
 
iZER13 = 	 Parameter(name='iZER13', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER13}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [1, 3] ) 
 
rZER21 = 	 Parameter(name='rZER21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER21}', 
	 lhablock = 'UERMIX', 
	 lhacode = [2, 1] ) 
 
iZER21 = 	 Parameter(name='iZER21', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER21}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [2, 1] ) 
 
rZER22 = 	 Parameter(name='rZER22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZER22}', 
	 lhablock = 'UERMIX', 
	 lhacode = [2, 2] ) 
 
iZER22 = 	 Parameter(name='iZER22', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER22}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [2, 2] ) 
 
rZER23 = 	 Parameter(name='rZER23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER23}', 
	 lhablock = 'UERMIX', 
	 lhacode = [2, 3] ) 
 
iZER23 = 	 Parameter(name='iZER23', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER23}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [2, 3] ) 
 
rZER31 = 	 Parameter(name='rZER31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER31}', 
	 lhablock = 'UERMIX', 
	 lhacode = [3, 1] ) 
 
iZER31 = 	 Parameter(name='iZER31', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER31}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [3, 1] ) 
 
rZER32 = 	 Parameter(name='rZER32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER32}', 
	 lhablock = 'UERMIX', 
	 lhacode = [3, 2] ) 
 
iZER32 = 	 Parameter(name='iZER32', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER32}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [3, 2] ) 
 
rZER33 = 	 Parameter(name='rZER33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 1., 
	 texname = '\\text{ZER33}', 
	 lhablock = 'UERMIX', 
	 lhacode = [3, 3] ) 
 
iZER33 = 	 Parameter(name='iZER33', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{ZER33}', 
	 lhablock = 'IMUERMIX', 
	 lhacode = [3, 3] ) 
 
aEWM1 = 	 Parameter(name='aEWM1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 137.035999679, 
	 texname = '\\text{aEWM1}', 
	 lhablock = 'SMINPUTS', 
	 lhacode = [1] ) 
 
aS = 	 Parameter(name='aS', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.119, 
	 texname = '\\text{aS}', 
	 lhablock = 'SMINPUTS', 
	 lhacode = [3] ) 
 
Gf = 	 Parameter(name='Gf', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0.0000116639, 
	 texname = '\\text{Gf}', 
	 lhablock = 'SMINPUTS', 
	 lhacode = [2] ) 
 
yl11 = 	 Parameter(name='yl11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryl11 + complex(0,1)*iyl11', 
	 texname = '\\text{yl11}' ) 
 
yl12 = 	 Parameter(name='yl12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryl12 + complex(0,1)*iyl12', 
	 texname = '\\text{yl12}' ) 
 
yl13 = 	 Parameter(name='yl13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryl13 + complex(0,1)*iyl13', 
	 texname = '\\text{yl13}' ) 
 
yl21 = 	 Parameter(name='yl21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryl21 + complex(0,1)*iyl21', 
	 texname = '\\text{yl21}' ) 
 
yl22 = 	 Parameter(name='yl22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryl22 + complex(0,1)*iyl22', 
	 texname = '\\text{yl22}' ) 
 
yl23 = 	 Parameter(name='yl23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryl23 + complex(0,1)*iyl23', 
	 texname = '\\text{yl23}' ) 
 
yl31 = 	 Parameter(name='yl31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryl31 + complex(0,1)*iyl31', 
	 texname = '\\text{yl31}' ) 
 
yl32 = 	 Parameter(name='yl32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryl32 + complex(0,1)*iyl32', 
	 texname = '\\text{yl32}' ) 
 
yl33 = 	 Parameter(name='yl33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryl33 + complex(0,1)*iyl33', 
	 texname = '\\text{yl33}' ) 
 
yd11 = 	 Parameter(name='yd11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryd11 + complex(0,1)*iyd11', 
	 texname = '\\text{yd11}' ) 
 
yd12 = 	 Parameter(name='yd12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryd12 + complex(0,1)*iyd12', 
	 texname = '\\text{yd12}' ) 
 
yd13 = 	 Parameter(name='yd13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryd13 + complex(0,1)*iyd13', 
	 texname = '\\text{yd13}' ) 
 
yj11 = 	 Parameter(name='yj11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryj11 + complex(0,1)*iyj11', 
	 texname = '\\text{yj11}' ) 
 
yj12 = 	 Parameter(name='yj12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryj12 + complex(0,1)*iyj12', 
	 texname = '\\text{yj12}' ) 
 
yu11 = 	 Parameter(name='yu11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryu11 + complex(0,1)*iyu11', 
	 texname = '\\text{yu11}' ) 
 
yu12 = 	 Parameter(name='yu12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryu12 + complex(0,1)*iyu12', 
	 texname = '\\text{yu12}' ) 
 
yu13 = 	 Parameter(name='yu13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryu13 + complex(0,1)*iyu13', 
	 texname = '\\text{yu13}' ) 
 
yd21 = 	 Parameter(name='yd21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryd21 + complex(0,1)*iyd21', 
	 texname = '\\text{yd21}' ) 
 
yd22 = 	 Parameter(name='yd22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryd22 + complex(0,1)*iyd22', 
	 texname = '\\text{yd22}' ) 
 
yd23 = 	 Parameter(name='yd23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryd23 + complex(0,1)*iyd23', 
	 texname = '\\text{yd23}' ) 
 
yj21 = 	 Parameter(name='yj21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryj21 + complex(0,1)*iyj21', 
	 texname = '\\text{yj21}' ) 
 
yj22 = 	 Parameter(name='yj22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryj22 + complex(0,1)*iyj22', 
	 texname = '\\text{yj22}' ) 
 
yu21 = 	 Parameter(name='yu21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryu21 + complex(0,1)*iyu21', 
	 texname = '\\text{yu21}' ) 
 
yu22 = 	 Parameter(name='yu22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryu22 + complex(0,1)*iyu22', 
	 texname = '\\text{yu22}' ) 
 
yu23 = 	 Parameter(name='yu23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryu23 + complex(0,1)*iyu23', 
	 texname = '\\text{yu23}' ) 
 
yd31 = 	 Parameter(name='yd31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryd31 + complex(0,1)*iyd31', 
	 texname = '\\text{yd31}' ) 
 
yd32 = 	 Parameter(name='yd32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryd32 + complex(0,1)*iyd32', 
	 texname = '\\text{yd32}' ) 
 
yd33 = 	 Parameter(name='yd33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryd33 + complex(0,1)*iyd33', 
	 texname = '\\text{yd33}' ) 
 
yj3 = 	 Parameter(name='yj3', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryj3 + complex(0,1)*iyj3', 
	 texname = '\\text{yj3}' ) 
 
yu31 = 	 Parameter(name='yu31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryu31 + complex(0,1)*iyu31', 
	 texname = '\\text{yu31}' ) 
 
yu32 = 	 Parameter(name='yu32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryu32 + complex(0,1)*iyu32', 
	 texname = '\\text{yu32}' ) 
 
yu33 = 	 Parameter(name='yu33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'ryu33 + complex(0,1)*iyu33', 
	 texname = '\\text{yu33}' ) 
 
fsex = 	 Parameter(name='fsex', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rfsex + complex(0,1)*ifsex', 
	 texname = '\\text{fsex}' ) 
 
l34T = 	 Parameter(name='l34T', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl34T + complex(0,1)*il34T', 
	 texname = '\\text{l34T}' ) 
 
l14T = 	 Parameter(name='l14T', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl14T + complex(0,1)*il14T', 
	 texname = '\\text{l14T}' ) 
 
l24T = 	 Parameter(name='l24T', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl24T + complex(0,1)*il24T', 
	 texname = '\\text{l24T}' ) 
 
l3 = 	 Parameter(name='l3', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl3 + complex(0,1)*il3', 
	 texname = '\\text{l3}' ) 
 
l34 = 	 Parameter(name='l34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl34 + complex(0,1)*il34', 
	 texname = '\\text{l34}' ) 
 
l34t = 	 Parameter(name='l34t', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl34t + complex(0,1)*il34t', 
	 texname = '\\text{l34t}' ) 
 
l23t = 	 Parameter(name='l23t', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl23t + complex(0,1)*il23t', 
	 texname = '\\text{l23t}' ) 
 
l23 = 	 Parameter(name='l23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl23 + complex(0,1)*il23', 
	 texname = '\\text{l23}' ) 
 
l2 = 	 Parameter(name='l2', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl2 + complex(0,1)*il2', 
	 texname = '\\text{l2}' ) 
 
l24 = 	 Parameter(name='l24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl24 + complex(0,1)*il24', 
	 texname = '\\text{l24}' ) 
 
l24t = 	 Parameter(name='l24t', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl24t + complex(0,1)*il24t', 
	 texname = '\\text{l24t}' ) 
 
l13t = 	 Parameter(name='l13t', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl13t + complex(0,1)*il13t', 
	 texname = '\\text{l13t}' ) 
 
l12t = 	 Parameter(name='l12t', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl12t + complex(0,1)*il12t', 
	 texname = '\\text{l12t}' ) 
 
l13 = 	 Parameter(name='l13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl13 + complex(0,1)*il13', 
	 texname = '\\text{l13}' ) 
 
l12 = 	 Parameter(name='l12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl12 + complex(0,1)*il12', 
	 texname = '\\text{l12}' ) 
 
l1 = 	 Parameter(name='l1', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl1 + complex(0,1)*il1', 
	 texname = '\\text{l1}' ) 
 
l14 = 	 Parameter(name='l14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl14 + complex(0,1)*il14', 
	 texname = '\\text{l14}' ) 
 
l44t = 	 Parameter(name='l44t', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl44t + complex(0,1)*il44t', 
	 texname = '\\text{l44t}' ) 
 
l44 = 	 Parameter(name='l44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rl44 + complex(0,1)*il44', 
	 texname = '\\text{l44}' ) 
 
phaset = 	 Parameter(name='phaset', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rphaset + complex(0,1)*iphaset', 
	 texname = '\\text{phaset}' ) 
 
ZH11 = 	 Parameter(name='ZH11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH11 + complex(0,1)*iZH11', 
	 texname = '\\text{ZH11}' ) 
 
ZH12 = 	 Parameter(name='ZH12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH12 + complex(0,1)*iZH12', 
	 texname = '\\text{ZH12}' ) 
 
ZH13 = 	 Parameter(name='ZH13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH13 + complex(0,1)*iZH13', 
	 texname = '\\text{ZH13}' ) 
 
ZH14 = 	 Parameter(name='ZH14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH14 + complex(0,1)*iZH14', 
	 texname = '\\text{ZH14}' ) 
 
ZH15 = 	 Parameter(name='ZH15', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH15 + complex(0,1)*iZH15', 
	 texname = '\\text{ZH15}' ) 
 
ZH21 = 	 Parameter(name='ZH21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH21 + complex(0,1)*iZH21', 
	 texname = '\\text{ZH21}' ) 
 
ZH22 = 	 Parameter(name='ZH22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH22 + complex(0,1)*iZH22', 
	 texname = '\\text{ZH22}' ) 
 
ZH23 = 	 Parameter(name='ZH23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH23 + complex(0,1)*iZH23', 
	 texname = '\\text{ZH23}' ) 
 
ZH24 = 	 Parameter(name='ZH24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH24 + complex(0,1)*iZH24', 
	 texname = '\\text{ZH24}' ) 
 
ZH25 = 	 Parameter(name='ZH25', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH25 + complex(0,1)*iZH25', 
	 texname = '\\text{ZH25}' ) 
 
ZH31 = 	 Parameter(name='ZH31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH31 + complex(0,1)*iZH31', 
	 texname = '\\text{ZH31}' ) 
 
ZH32 = 	 Parameter(name='ZH32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH32 + complex(0,1)*iZH32', 
	 texname = '\\text{ZH32}' ) 
 
ZH33 = 	 Parameter(name='ZH33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH33 + complex(0,1)*iZH33', 
	 texname = '\\text{ZH33}' ) 
 
ZH34 = 	 Parameter(name='ZH34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH34 + complex(0,1)*iZH34', 
	 texname = '\\text{ZH34}' ) 
 
ZH35 = 	 Parameter(name='ZH35', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH35 + complex(0,1)*iZH35', 
	 texname = '\\text{ZH35}' ) 
 
ZH41 = 	 Parameter(name='ZH41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH41 + complex(0,1)*iZH41', 
	 texname = '\\text{ZH41}' ) 
 
ZH42 = 	 Parameter(name='ZH42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH42 + complex(0,1)*iZH42', 
	 texname = '\\text{ZH42}' ) 
 
ZH43 = 	 Parameter(name='ZH43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH43 + complex(0,1)*iZH43', 
	 texname = '\\text{ZH43}' ) 
 
ZH44 = 	 Parameter(name='ZH44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH44 + complex(0,1)*iZH44', 
	 texname = '\\text{ZH44}' ) 
 
ZH45 = 	 Parameter(name='ZH45', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH45 + complex(0,1)*iZH45', 
	 texname = '\\text{ZH45}' ) 
 
ZH51 = 	 Parameter(name='ZH51', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH51 + complex(0,1)*iZH51', 
	 texname = '\\text{ZH51}' ) 
 
ZH52 = 	 Parameter(name='ZH52', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH52 + complex(0,1)*iZH52', 
	 texname = '\\text{ZH52}' ) 
 
ZH53 = 	 Parameter(name='ZH53', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH53 + complex(0,1)*iZH53', 
	 texname = '\\text{ZH53}' ) 
 
ZH54 = 	 Parameter(name='ZH54', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH54 + complex(0,1)*iZH54', 
	 texname = '\\text{ZH54}' ) 
 
ZH55 = 	 Parameter(name='ZH55', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZH55 + complex(0,1)*iZH55', 
	 texname = '\\text{ZH55}' ) 
 
ZA11 = 	 Parameter(name='ZA11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA11 + complex(0,1)*iZA11', 
	 texname = '\\text{ZA11}' ) 
 
ZA12 = 	 Parameter(name='ZA12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA12 + complex(0,1)*iZA12', 
	 texname = '\\text{ZA12}' ) 
 
ZA13 = 	 Parameter(name='ZA13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA13 + complex(0,1)*iZA13', 
	 texname = '\\text{ZA13}' ) 
 
ZA14 = 	 Parameter(name='ZA14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA14 + complex(0,1)*iZA14', 
	 texname = '\\text{ZA14}' ) 
 
ZA15 = 	 Parameter(name='ZA15', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA15 + complex(0,1)*iZA15', 
	 texname = '\\text{ZA15}' ) 
 
ZA21 = 	 Parameter(name='ZA21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA21 + complex(0,1)*iZA21', 
	 texname = '\\text{ZA21}' ) 
 
ZA22 = 	 Parameter(name='ZA22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA22 + complex(0,1)*iZA22', 
	 texname = '\\text{ZA22}' ) 
 
ZA23 = 	 Parameter(name='ZA23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA23 + complex(0,1)*iZA23', 
	 texname = '\\text{ZA23}' ) 
 
ZA24 = 	 Parameter(name='ZA24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA24 + complex(0,1)*iZA24', 
	 texname = '\\text{ZA24}' ) 
 
ZA25 = 	 Parameter(name='ZA25', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA25 + complex(0,1)*iZA25', 
	 texname = '\\text{ZA25}' ) 
 
ZA31 = 	 Parameter(name='ZA31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA31 + complex(0,1)*iZA31', 
	 texname = '\\text{ZA31}' ) 
 
ZA32 = 	 Parameter(name='ZA32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA32 + complex(0,1)*iZA32', 
	 texname = '\\text{ZA32}' ) 
 
ZA33 = 	 Parameter(name='ZA33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA33 + complex(0,1)*iZA33', 
	 texname = '\\text{ZA33}' ) 
 
ZA34 = 	 Parameter(name='ZA34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA34 + complex(0,1)*iZA34', 
	 texname = '\\text{ZA34}' ) 
 
ZA35 = 	 Parameter(name='ZA35', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA35 + complex(0,1)*iZA35', 
	 texname = '\\text{ZA35}' ) 
 
ZA41 = 	 Parameter(name='ZA41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA41 + complex(0,1)*iZA41', 
	 texname = '\\text{ZA41}' ) 
 
ZA42 = 	 Parameter(name='ZA42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA42 + complex(0,1)*iZA42', 
	 texname = '\\text{ZA42}' ) 
 
ZA43 = 	 Parameter(name='ZA43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA43 + complex(0,1)*iZA43', 
	 texname = '\\text{ZA43}' ) 
 
ZA44 = 	 Parameter(name='ZA44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA44 + complex(0,1)*iZA44', 
	 texname = '\\text{ZA44}' ) 
 
ZA45 = 	 Parameter(name='ZA45', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA45 + complex(0,1)*iZA45', 
	 texname = '\\text{ZA45}' ) 
 
ZA51 = 	 Parameter(name='ZA51', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA51 + complex(0,1)*iZA51', 
	 texname = '\\text{ZA51}' ) 
 
ZA52 = 	 Parameter(name='ZA52', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA52 + complex(0,1)*iZA52', 
	 texname = '\\text{ZA52}' ) 
 
ZA53 = 	 Parameter(name='ZA53', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA53 + complex(0,1)*iZA53', 
	 texname = '\\text{ZA53}' ) 
 
ZA54 = 	 Parameter(name='ZA54', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA54 + complex(0,1)*iZA54', 
	 texname = '\\text{ZA54}' ) 
 
ZA55 = 	 Parameter(name='ZA55', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZA55 + complex(0,1)*iZA55', 
	 texname = '\\text{ZA55}' ) 
 
ZP11 = 	 Parameter(name='ZP11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP11 + complex(0,1)*iZP11', 
	 texname = '\\text{ZP11}' ) 
 
ZP12 = 	 Parameter(name='ZP12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP12 + complex(0,1)*iZP12', 
	 texname = '\\text{ZP12}' ) 
 
ZP13 = 	 Parameter(name='ZP13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP13 + complex(0,1)*iZP13', 
	 texname = '\\text{ZP13}' ) 
 
ZP14 = 	 Parameter(name='ZP14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP14 + complex(0,1)*iZP14', 
	 texname = '\\text{ZP14}' ) 
 
ZP15 = 	 Parameter(name='ZP15', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP15 + complex(0,1)*iZP15', 
	 texname = '\\text{ZP15}' ) 
 
ZP16 = 	 Parameter(name='ZP16', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP16 + complex(0,1)*iZP16', 
	 texname = '\\text{ZP16}' ) 
 
ZP21 = 	 Parameter(name='ZP21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP21 + complex(0,1)*iZP21', 
	 texname = '\\text{ZP21}' ) 
 
ZP22 = 	 Parameter(name='ZP22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP22 + complex(0,1)*iZP22', 
	 texname = '\\text{ZP22}' ) 
 
ZP23 = 	 Parameter(name='ZP23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP23 + complex(0,1)*iZP23', 
	 texname = '\\text{ZP23}' ) 
 
ZP24 = 	 Parameter(name='ZP24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP24 + complex(0,1)*iZP24', 
	 texname = '\\text{ZP24}' ) 
 
ZP25 = 	 Parameter(name='ZP25', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP25 + complex(0,1)*iZP25', 
	 texname = '\\text{ZP25}' ) 
 
ZP26 = 	 Parameter(name='ZP26', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP26 + complex(0,1)*iZP26', 
	 texname = '\\text{ZP26}' ) 
 
ZP31 = 	 Parameter(name='ZP31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP31 + complex(0,1)*iZP31', 
	 texname = '\\text{ZP31}' ) 
 
ZP32 = 	 Parameter(name='ZP32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP32 + complex(0,1)*iZP32', 
	 texname = '\\text{ZP32}' ) 
 
ZP33 = 	 Parameter(name='ZP33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP33 + complex(0,1)*iZP33', 
	 texname = '\\text{ZP33}' ) 
 
ZP34 = 	 Parameter(name='ZP34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP34 + complex(0,1)*iZP34', 
	 texname = '\\text{ZP34}' ) 
 
ZP35 = 	 Parameter(name='ZP35', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP35 + complex(0,1)*iZP35', 
	 texname = '\\text{ZP35}' ) 
 
ZP36 = 	 Parameter(name='ZP36', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP36 + complex(0,1)*iZP36', 
	 texname = '\\text{ZP36}' ) 
 
ZP41 = 	 Parameter(name='ZP41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP41 + complex(0,1)*iZP41', 
	 texname = '\\text{ZP41}' ) 
 
ZP42 = 	 Parameter(name='ZP42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP42 + complex(0,1)*iZP42', 
	 texname = '\\text{ZP42}' ) 
 
ZP43 = 	 Parameter(name='ZP43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP43 + complex(0,1)*iZP43', 
	 texname = '\\text{ZP43}' ) 
 
ZP44 = 	 Parameter(name='ZP44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP44 + complex(0,1)*iZP44', 
	 texname = '\\text{ZP44}' ) 
 
ZP45 = 	 Parameter(name='ZP45', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP45 + complex(0,1)*iZP45', 
	 texname = '\\text{ZP45}' ) 
 
ZP46 = 	 Parameter(name='ZP46', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP46 + complex(0,1)*iZP46', 
	 texname = '\\text{ZP46}' ) 
 
ZP51 = 	 Parameter(name='ZP51', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP51 + complex(0,1)*iZP51', 
	 texname = '\\text{ZP51}' ) 
 
ZP52 = 	 Parameter(name='ZP52', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP52 + complex(0,1)*iZP52', 
	 texname = '\\text{ZP52}' ) 
 
ZP53 = 	 Parameter(name='ZP53', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP53 + complex(0,1)*iZP53', 
	 texname = '\\text{ZP53}' ) 
 
ZP54 = 	 Parameter(name='ZP54', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP54 + complex(0,1)*iZP54', 
	 texname = '\\text{ZP54}' ) 
 
ZP55 = 	 Parameter(name='ZP55', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP55 + complex(0,1)*iZP55', 
	 texname = '\\text{ZP55}' ) 
 
ZP56 = 	 Parameter(name='ZP56', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP56 + complex(0,1)*iZP56', 
	 texname = '\\text{ZP56}' ) 
 
ZP61 = 	 Parameter(name='ZP61', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP61 + complex(0,1)*iZP61', 
	 texname = '\\text{ZP61}' ) 
 
ZP62 = 	 Parameter(name='ZP62', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP62 + complex(0,1)*iZP62', 
	 texname = '\\text{ZP62}' ) 
 
ZP63 = 	 Parameter(name='ZP63', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP63 + complex(0,1)*iZP63', 
	 texname = '\\text{ZP63}' ) 
 
ZP64 = 	 Parameter(name='ZP64', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP64 + complex(0,1)*iZP64', 
	 texname = '\\text{ZP64}' ) 
 
ZP65 = 	 Parameter(name='ZP65', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP65 + complex(0,1)*iZP65', 
	 texname = '\\text{ZP65}' ) 
 
ZP66 = 	 Parameter(name='ZP66', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZP66 + complex(0,1)*iZP66', 
	 texname = '\\text{ZP66}' ) 
 
zd11 = 	 Parameter(name='zd11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd11 + complex(0,1)*izd11', 
	 texname = '\\text{zd11}' ) 
 
zd12 = 	 Parameter(name='zd12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd12 + complex(0,1)*izd12', 
	 texname = '\\text{zd12}' ) 
 
zd13 = 	 Parameter(name='zd13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd13 + complex(0,1)*izd13', 
	 texname = '\\text{zd13}' ) 
 
zd14 = 	 Parameter(name='zd14', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd14 + complex(0,1)*izd14', 
	 texname = '\\text{zd14}' ) 
 
zd21 = 	 Parameter(name='zd21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd21 + complex(0,1)*izd21', 
	 texname = '\\text{zd21}' ) 
 
zd22 = 	 Parameter(name='zd22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd22 + complex(0,1)*izd22', 
	 texname = '\\text{zd22}' ) 
 
zd23 = 	 Parameter(name='zd23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd23 + complex(0,1)*izd23', 
	 texname = '\\text{zd23}' ) 
 
zd24 = 	 Parameter(name='zd24', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd24 + complex(0,1)*izd24', 
	 texname = '\\text{zd24}' ) 
 
zd31 = 	 Parameter(name='zd31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd31 + complex(0,1)*izd31', 
	 texname = '\\text{zd31}' ) 
 
zd32 = 	 Parameter(name='zd32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd32 + complex(0,1)*izd32', 
	 texname = '\\text{zd32}' ) 
 
zd33 = 	 Parameter(name='zd33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd33 + complex(0,1)*izd33', 
	 texname = '\\text{zd33}' ) 
 
zd34 = 	 Parameter(name='zd34', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd34 + complex(0,1)*izd34', 
	 texname = '\\text{zd34}' ) 
 
zd41 = 	 Parameter(name='zd41', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd41 + complex(0,1)*izd41', 
	 texname = '\\text{zd41}' ) 
 
zd42 = 	 Parameter(name='zd42', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd42 + complex(0,1)*izd42', 
	 texname = '\\text{zd42}' ) 
 
zd43 = 	 Parameter(name='zd43', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd43 + complex(0,1)*izd43', 
	 texname = '\\text{zd43}' ) 
 
zd44 = 	 Parameter(name='zd44', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rzd44 + complex(0,1)*izd44', 
	 texname = '\\text{zd44}' ) 
 
ZDL11 = 	 Parameter(name='ZDL11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL11 + complex(0,1)*iZDL11', 
	 texname = '\\text{ZDL11}' ) 
 
ZDL12 = 	 Parameter(name='ZDL12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL12 + complex(0,1)*iZDL12', 
	 texname = '\\text{ZDL12}' ) 
 
ZDL13 = 	 Parameter(name='ZDL13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL13 + complex(0,1)*iZDL13', 
	 texname = '\\text{ZDL13}' ) 
 
ZDL21 = 	 Parameter(name='ZDL21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL21 + complex(0,1)*iZDL21', 
	 texname = '\\text{ZDL21}' ) 
 
ZDL22 = 	 Parameter(name='ZDL22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL22 + complex(0,1)*iZDL22', 
	 texname = '\\text{ZDL22}' ) 
 
ZDL23 = 	 Parameter(name='ZDL23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL23 + complex(0,1)*iZDL23', 
	 texname = '\\text{ZDL23}' ) 
 
ZDL31 = 	 Parameter(name='ZDL31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL31 + complex(0,1)*iZDL31', 
	 texname = '\\text{ZDL31}' ) 
 
ZDL32 = 	 Parameter(name='ZDL32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL32 + complex(0,1)*iZDL32', 
	 texname = '\\text{ZDL32}' ) 
 
ZDL33 = 	 Parameter(name='ZDL33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDL33 + complex(0,1)*iZDL33', 
	 texname = '\\text{ZDL33}' ) 
 
ZDR11 = 	 Parameter(name='ZDR11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR11 + complex(0,1)*iZDR11', 
	 texname = '\\text{ZDR11}' ) 
 
ZDR12 = 	 Parameter(name='ZDR12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR12 + complex(0,1)*iZDR12', 
	 texname = '\\text{ZDR12}' ) 
 
ZDR13 = 	 Parameter(name='ZDR13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR13 + complex(0,1)*iZDR13', 
	 texname = '\\text{ZDR13}' ) 
 
ZDR21 = 	 Parameter(name='ZDR21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR21 + complex(0,1)*iZDR21', 
	 texname = '\\text{ZDR21}' ) 
 
ZDR22 = 	 Parameter(name='ZDR22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR22 + complex(0,1)*iZDR22', 
	 texname = '\\text{ZDR22}' ) 
 
ZDR23 = 	 Parameter(name='ZDR23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR23 + complex(0,1)*iZDR23', 
	 texname = '\\text{ZDR23}' ) 
 
ZDR31 = 	 Parameter(name='ZDR31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR31 + complex(0,1)*iZDR31', 
	 texname = '\\text{ZDR31}' ) 
 
ZDR32 = 	 Parameter(name='ZDR32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR32 + complex(0,1)*iZDR32', 
	 texname = '\\text{ZDR32}' ) 
 
ZDR33 = 	 Parameter(name='ZDR33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZDR33 + complex(0,1)*iZDR33', 
	 texname = '\\text{ZDR33}' ) 
 
vd11 = 	 Parameter(name='vd11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rvd11 + complex(0,1)*ivd11', 
	 texname = '\\text{vd11}' ) 
 
vd12 = 	 Parameter(name='vd12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rvd12 + complex(0,1)*ivd12', 
	 texname = '\\text{vd12}' ) 
 
vd21 = 	 Parameter(name='vd21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rvd21 + complex(0,1)*ivd21', 
	 texname = '\\text{vd21}' ) 
 
vd22 = 	 Parameter(name='vd22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rvd22 + complex(0,1)*ivd22', 
	 texname = '\\text{vd22}' ) 
 
ud11 = 	 Parameter(name='ud11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rud11 + complex(0,1)*iud11', 
	 texname = '\\text{ud11}' ) 
 
ud12 = 	 Parameter(name='ud12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rud12 + complex(0,1)*iud12', 
	 texname = '\\text{ud12}' ) 
 
ud21 = 	 Parameter(name='ud21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rud21 + complex(0,1)*iud21', 
	 texname = '\\text{ud21}' ) 
 
ud22 = 	 Parameter(name='ud22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rud22 + complex(0,1)*iud22', 
	 texname = '\\text{ud22}' ) 
 
ZUL11 = 	 Parameter(name='ZUL11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL11 + complex(0,1)*iZUL11', 
	 texname = '\\text{ZUL11}' ) 
 
ZUL12 = 	 Parameter(name='ZUL12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL12 + complex(0,1)*iZUL12', 
	 texname = '\\text{ZUL12}' ) 
 
ZUL13 = 	 Parameter(name='ZUL13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL13 + complex(0,1)*iZUL13', 
	 texname = '\\text{ZUL13}' ) 
 
ZUL21 = 	 Parameter(name='ZUL21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL21 + complex(0,1)*iZUL21', 
	 texname = '\\text{ZUL21}' ) 
 
ZUL22 = 	 Parameter(name='ZUL22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL22 + complex(0,1)*iZUL22', 
	 texname = '\\text{ZUL22}' ) 
 
ZUL23 = 	 Parameter(name='ZUL23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL23 + complex(0,1)*iZUL23', 
	 texname = '\\text{ZUL23}' ) 
 
ZUL31 = 	 Parameter(name='ZUL31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL31 + complex(0,1)*iZUL31', 
	 texname = '\\text{ZUL31}' ) 
 
ZUL32 = 	 Parameter(name='ZUL32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL32 + complex(0,1)*iZUL32', 
	 texname = '\\text{ZUL32}' ) 
 
ZUL33 = 	 Parameter(name='ZUL33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUL33 + complex(0,1)*iZUL33', 
	 texname = '\\text{ZUL33}' ) 
 
ZUR11 = 	 Parameter(name='ZUR11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR11 + complex(0,1)*iZUR11', 
	 texname = '\\text{ZUR11}' ) 
 
ZUR12 = 	 Parameter(name='ZUR12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR12 + complex(0,1)*iZUR12', 
	 texname = '\\text{ZUR12}' ) 
 
ZUR13 = 	 Parameter(name='ZUR13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR13 + complex(0,1)*iZUR13', 
	 texname = '\\text{ZUR13}' ) 
 
ZUR21 = 	 Parameter(name='ZUR21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR21 + complex(0,1)*iZUR21', 
	 texname = '\\text{ZUR21}' ) 
 
ZUR22 = 	 Parameter(name='ZUR22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR22 + complex(0,1)*iZUR22', 
	 texname = '\\text{ZUR22}' ) 
 
ZUR23 = 	 Parameter(name='ZUR23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR23 + complex(0,1)*iZUR23', 
	 texname = '\\text{ZUR23}' ) 
 
ZUR31 = 	 Parameter(name='ZUR31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR31 + complex(0,1)*iZUR31', 
	 texname = '\\text{ZUR31}' ) 
 
ZUR32 = 	 Parameter(name='ZUR32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR32 + complex(0,1)*iZUR32', 
	 texname = '\\text{ZUR32}' ) 
 
ZUR33 = 	 Parameter(name='ZUR33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZUR33 + complex(0,1)*iZUR33', 
	 texname = '\\text{ZUR33}' ) 
 
ZEL11 = 	 Parameter(name='ZEL11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL11 + complex(0,1)*iZEL11', 
	 texname = '\\text{ZEL11}' ) 
 
ZEL12 = 	 Parameter(name='ZEL12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL12 + complex(0,1)*iZEL12', 
	 texname = '\\text{ZEL12}' ) 
 
ZEL13 = 	 Parameter(name='ZEL13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL13 + complex(0,1)*iZEL13', 
	 texname = '\\text{ZEL13}' ) 
 
ZEL21 = 	 Parameter(name='ZEL21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL21 + complex(0,1)*iZEL21', 
	 texname = '\\text{ZEL21}' ) 
 
ZEL22 = 	 Parameter(name='ZEL22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL22 + complex(0,1)*iZEL22', 
	 texname = '\\text{ZEL22}' ) 
 
ZEL23 = 	 Parameter(name='ZEL23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL23 + complex(0,1)*iZEL23', 
	 texname = '\\text{ZEL23}' ) 
 
ZEL31 = 	 Parameter(name='ZEL31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL31 + complex(0,1)*iZEL31', 
	 texname = '\\text{ZEL31}' ) 
 
ZEL32 = 	 Parameter(name='ZEL32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL32 + complex(0,1)*iZEL32', 
	 texname = '\\text{ZEL32}' ) 
 
ZEL33 = 	 Parameter(name='ZEL33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZEL33 + complex(0,1)*iZEL33', 
	 texname = '\\text{ZEL33}' ) 
 
ZER11 = 	 Parameter(name='ZER11', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER11 + complex(0,1)*iZER11', 
	 texname = '\\text{ZER11}' ) 
 
ZER12 = 	 Parameter(name='ZER12', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER12 + complex(0,1)*iZER12', 
	 texname = '\\text{ZER12}' ) 
 
ZER13 = 	 Parameter(name='ZER13', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER13 + complex(0,1)*iZER13', 
	 texname = '\\text{ZER13}' ) 
 
ZER21 = 	 Parameter(name='ZER21', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER21 + complex(0,1)*iZER21', 
	 texname = '\\text{ZER21}' ) 
 
ZER22 = 	 Parameter(name='ZER22', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER22 + complex(0,1)*iZER22', 
	 texname = '\\text{ZER22}' ) 
 
ZER23 = 	 Parameter(name='ZER23', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER23 + complex(0,1)*iZER23', 
	 texname = '\\text{ZER23}' ) 
 
ZER31 = 	 Parameter(name='ZER31', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER31 + complex(0,1)*iZER31', 
	 texname = '\\text{ZER31}' ) 
 
ZER32 = 	 Parameter(name='ZER32', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER32 + complex(0,1)*iZER32', 
	 texname = '\\text{ZER32}' ) 
 
ZER33 = 	 Parameter(name='ZER33', 
	 nature = 'internal', 
	 type = 'complex', 
	 value = 'rZER33 + complex(0,1)*iZER33', 
	 texname = '\\text{ZER33}' ) 
 
G = 	 Parameter(name='G', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)', 
	 texname = 'G') 
 
el = 	 Parameter(name='el', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '2*cmath.sqrt(1/aEWM1)*cmath.sqrt(cmath.pi)', 
	 texname = 'el') 
 
MWp = 	 Parameter(name='MWp', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (MZ**2*cmath.pi)/(cmath.sqrt(2)*aEWM1*Gf)))', 
	 texname = 'MWp') 
 
TW = 	 Parameter(name='TW', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'cmath.asin(cmath.sqrt(1 - MWp**2/MZ**2))', 
	 texname = 'TW') 
 
g2 = 	 Parameter(name='g2', 
	 nature = 'internal', 
	 type = 'real', 
	 value = 'el*1./cmath.sin(TW)', 
	 texname = 'g2') 
 
RXiWp = 	 Parameter(name='RXiWp', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '1.', 
	 texname = 'RXiWp') 
 
RXiXp = 	 Parameter(name='RXiXp', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '1.', 
	 texname = 'RXiXp') 
 
RXiYpp = 	 Parameter(name='RXiYpp', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '1.', 
	 texname = 'RXiYpp') 
 
RXiZ = 	 Parameter(name='RXiZ', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '1.', 
	 texname = 'RXiZ') 
 
RXiZp = 	 Parameter(name='RXiZp', 
	 nature = 'internal', 
	 type = 'real', 
	 value = '1.', 
	 texname = 'RXiZp') 
 
HPP1 = 	 Parameter(name='HPP1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{HPP1}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [25,22,22] ) 
 
HGG1 = 	 Parameter(name='HGG1', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{HGG1}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [25,21,21] ) 
 
HPP2 = 	 Parameter(name='HPP2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{HPP2}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [35,22,22] ) 
 
HGG2 = 	 Parameter(name='HGG2', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{HGG2}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [35,21,21] ) 
 
HPP3 = 	 Parameter(name='HPP3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{HPP3}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [1045,22,22] ) 
 
HGG3 = 	 Parameter(name='HGG3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{HGG3}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [1045,21,21] ) 
 
HPP4 = 	 Parameter(name='HPP4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{HPP4}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [1055,22,22] ) 
 
HGG4 = 	 Parameter(name='HGG4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{HGG4}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [1055,21,21] ) 
 
HPP5 = 	 Parameter(name='HPP5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{HPP5}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [1065,22,22] ) 
 
HGG5 = 	 Parameter(name='HGG5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{HGG5}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [1065,21,21] ) 
 
APP3 = 	 Parameter(name='APP3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{APP3}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [36,22,22] ) 
 
AGG3 = 	 Parameter(name='AGG3', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{AGG3}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [36,21,21] ) 
 
APP4 = 	 Parameter(name='APP4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{APP4}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [1046,22,22] ) 
 
AGG4 = 	 Parameter(name='AGG4', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{AGG4}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [1046,21,21] ) 
 
APP5 = 	 Parameter(name='APP5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{APP5}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [1056,22,22] ) 
 
AGG5 = 	 Parameter(name='AGG5', 
	 nature = 'external', 
	 type = 'real', 
	 value = 0., 
	 texname = '\\text{AGG5}', 
	 lhablock = 'EFFHIGGSCOUPLINGS', 
	 lhacode = [1056,21,21] ) 
 
