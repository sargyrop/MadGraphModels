# This file was automatically created by FeynRules 2.3.34
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Fri 15 Nov 2019 18:52:49


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

CLFV_eq = CouplingOrder(name = 'CLFV_eq',
                        expansion_order = 99,
                        hierarchy = 1)

CLFV_eu = CouplingOrder(name = 'CLFV_eu',
                        expansion_order = 99,
                        hierarchy = 1)

CLFV_lequ1 = CouplingOrder(name = 'CLFV_lequ1',
                           expansion_order = 99,
                           hierarchy = 1)

CLFV_lequ3 = CouplingOrder(name = 'CLFV_lequ3',
                           expansion_order = 99,
                           hierarchy = 1)

CLFV_lu = CouplingOrder(name = 'CLFV_lu',
                        expansion_order = 99,
                        hierarchy = 1)

CLFV_Ql = CouplingOrder(name = 'CLFV_Ql',
                        expansion_order = 99,
                        hierarchy = 1)

