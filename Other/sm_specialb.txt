Requestor: Andrej Saibel
Contents: Modified Standard Model with an additional (second) b-quark with mass. This is to be used in Madspin samples in which the b-quarks from top-quark decay need to have mass to be properly interfaced with parton showers. 
Paper: None
Source: olivier.mattelaer@uclouvain.be, privately given to fix the issue in Madspin seen in top-quark samples
Jira: https://its.cern.ch/jira/browse/AGENE-2229
