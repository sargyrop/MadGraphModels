# This file was automatically created by FeynRules 2.3.1
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Wed 27 May 2015 17:27:37


from object_library import all_lorentz, Lorentz

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot
try:
   import form_factors as ForFac 
except ImportError:
   pass


UUS16 = Lorentz(name = 'UUS16',
                spins = [ -1, -1, 1 ],
                structure = '1')

UUV23 = Lorentz(name = 'UUV23',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,2) + P(3,3)')

SSS16 = Lorentz(name = 'SSS16',
                spins = [ 1, 1, 1 ],
                structure = '1')

FFS32 = Lorentz(name = 'FFS32',
                spins = [ 2, 2, 1 ],
                structure = 'ProjM(2,1)')

FFS33 = Lorentz(name = 'FFS33',
                spins = [ 2, 2, 1 ],
                structure = 'ProjP(2,1)')

FFV65 = Lorentz(name = 'FFV65',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,1)')

FFV66 = Lorentz(name = 'FFV66',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1)')

FFV67 = Lorentz(name = 'FFV67',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjP(-1,1)')

VSS16 = Lorentz(name = 'VSS16',
                spins = [ 3, 1, 1 ],
                structure = 'P(1,2) - P(1,3)')

VVS16 = Lorentz(name = 'VVS16',
                spins = [ 3, 3, 1 ],
                structure = 'Metric(1,2)')

VVV16 = Lorentz(name = 'VVV16',
                spins = [ 3, 3, 3 ],
                structure = 'P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

SSSS16 = Lorentz(name = 'SSSS16',
                 spins = [ 1, 1, 1, 1 ],
                 structure = '1')

FFVV23 = Lorentz(name = 'FFVV23',
                 spins = [ 2, 2, 3, 3 ],
                 structure = 'P(3,4)*P(4,3)*Gamma5(2,1) - P(-1,3)*P(-1,4)*Gamma5(2,1)*Metric(3,4)')

VVSS16 = Lorentz(name = 'VVSS16',
                 spins = [ 3, 3, 1, 1 ],
                 structure = 'Metric(1,2)')

VVVV76 = Lorentz(name = 'VVVV76',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4)')

VVVV77 = Lorentz(name = 'VVVV77',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) + Metric(1,3)*Metric(2,4) - 2*Metric(1,2)*Metric(3,4)')

VVVV78 = Lorentz(name = 'VVVV78',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,2)*Metric(3,4)')

VVVV79 = Lorentz(name = 'VVVV79',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,3)*Metric(2,4) - Metric(1,2)*Metric(3,4)')

VVVV80 = Lorentz(name = 'VVVV80',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - (Metric(1,3)*Metric(2,4))/2. - (Metric(1,2)*Metric(3,4))/2.')

FFVVV14 = Lorentz(name = 'FFVVV14',
                  spins = [ 2, 2, 3, 3, 3 ],
                  structure = 'P(5,3)*Gamma5(2,1)*Metric(3,4) - P(5,4)*Gamma5(2,1)*Metric(3,4) - P(4,3)*Gamma5(2,1)*Metric(3,5) + P(4,5)*Gamma5(2,1)*Metric(3,5) + P(3,4)*Gamma5(2,1)*Metric(4,5) - P(3,5)*Gamma5(2,1)*Metric(4,5)')

FFVVVV15 = Lorentz(name = 'FFVVVV15',
                   spins = [ 2, 2, 3, 3, 3, 3 ],
                   structure = 'Gamma5(2,1)*Metric(3,6)*Metric(4,5) + Gamma5(2,1)*Metric(3,5)*Metric(4,6) - 2*Gamma5(2,1)*Metric(3,4)*Metric(5,6)')

FFVVVV16 = Lorentz(name = 'FFVVVV16',
                   spins = [ 2, 2, 3, 3, 3, 3 ],
                   structure = 'Gamma5(2,1)*Metric(3,6)*Metric(4,5) - (Gamma5(2,1)*Metric(3,5)*Metric(4,6))/2. - (Gamma5(2,1)*Metric(3,4)*Metric(5,6))/2.')

