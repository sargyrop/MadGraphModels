# This file was automatically created by FeynRules 2.3.1
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Wed 27 May 2015 17:31:08


from object_library import all_lorentz, Lorentz

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot
try:
   import form_factors as ForFac 
except ImportError:
   pass


UUS18 = Lorentz(name = 'UUS18',
                spins = [ -1, -1, 1 ],
                structure = '1')

UUV25 = Lorentz(name = 'UUV25',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,2) + P(3,3)')

SSS18 = Lorentz(name = 'SSS18',
                spins = [ 1, 1, 1 ],
                structure = '1')

FFS36 = Lorentz(name = 'FFS36',
                spins = [ 2, 2, 1 ],
                structure = 'ProjM(2,1)')

FFS37 = Lorentz(name = 'FFS37',
                spins = [ 2, 2, 1 ],
                structure = 'ProjP(2,1)')

FFV71 = Lorentz(name = 'FFV71',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,1)')

FFV72 = Lorentz(name = 'FFV72',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1)')

FFV73 = Lorentz(name = 'FFV73',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjP(-1,1)')

VSS18 = Lorentz(name = 'VSS18',
                spins = [ 3, 1, 1 ],
                structure = 'P(1,2) - P(1,3)')

VVS18 = Lorentz(name = 'VVS18',
                spins = [ 3, 3, 1 ],
                structure = 'Metric(1,2)')

VVV18 = Lorentz(name = 'VVV18',
                spins = [ 3, 3, 3 ],
                structure = 'P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

SSSS18 = Lorentz(name = 'SSSS18',
                 spins = [ 1, 1, 1, 1 ],
                 structure = '1')

FFVV25 = Lorentz(name = 'FFVV25',
                 spins = [ 2, 2, 3, 3 ],
                 structure = '-(Epsilon(3,4,-1,-2)*P(-2,4)*P(-1,3)*Gamma5(2,1)) + Epsilon(3,4,-1,-2)*P(-2,3)*P(-1,4)*Gamma5(2,1)')

VVSS18 = Lorentz(name = 'VVSS18',
                 spins = [ 3, 3, 1, 1 ],
                 structure = 'Metric(1,2)')

VVVV86 = Lorentz(name = 'VVVV86',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4)')

VVVV87 = Lorentz(name = 'VVVV87',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) + Metric(1,3)*Metric(2,4) - 2*Metric(1,2)*Metric(3,4)')

VVVV88 = Lorentz(name = 'VVVV88',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,2)*Metric(3,4)')

VVVV89 = Lorentz(name = 'VVVV89',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,3)*Metric(2,4) - Metric(1,2)*Metric(3,4)')

VVVV90 = Lorentz(name = 'VVVV90',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - (Metric(1,3)*Metric(2,4))/2. - (Metric(1,2)*Metric(3,4))/2.')

FFVVV16 = Lorentz(name = 'FFVVV16',
                  spins = [ 2, 2, 3, 3, 3 ],
                  structure = '-(Epsilon(3,4,5,-1)*P(-1,3)*Gamma5(2,1)) - Epsilon(3,4,5,-1)*P(-1,4)*Gamma5(2,1) - Epsilon(3,4,5,-1)*P(-1,5)*Gamma5(2,1)')

