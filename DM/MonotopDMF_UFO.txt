Requestor: Renjie Wang
Content: Mono-top production for searches based on DM models
Paper: https://arxiv.org/abs/1407.7529
Download: https://svnweb.cern.ch/cern/wsvn/LHCDMF/trunk/models/HF_SingleTop/
JIRA: https://its.cern.ch/jira/browse/AGENE-1237