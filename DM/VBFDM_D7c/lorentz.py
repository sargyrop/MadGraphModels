# This file was automatically created by FeynRules 2.3.1
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Wed 27 May 2015 17:29:30


from object_library import all_lorentz, Lorentz

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot
try:
   import form_factors as ForFac 
except ImportError:
   pass


UUS17 = Lorentz(name = 'UUS17',
                spins = [ -1, -1, 1 ],
                structure = '1')

UUV24 = Lorentz(name = 'UUV24',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,2) + P(3,3)')

SSS17 = Lorentz(name = 'SSS17',
                spins = [ 1, 1, 1 ],
                structure = '1')

FFS34 = Lorentz(name = 'FFS34',
                spins = [ 2, 2, 1 ],
                structure = 'ProjM(2,1)')

FFS35 = Lorentz(name = 'FFS35',
                spins = [ 2, 2, 1 ],
                structure = 'ProjP(2,1)')

FFV68 = Lorentz(name = 'FFV68',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,1)')

FFV69 = Lorentz(name = 'FFV69',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1)')

FFV70 = Lorentz(name = 'FFV70',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjP(-1,1)')

VSS17 = Lorentz(name = 'VSS17',
                spins = [ 3, 1, 1 ],
                structure = 'P(1,2) - P(1,3)')

VVS17 = Lorentz(name = 'VVS17',
                spins = [ 3, 3, 1 ],
                structure = 'Metric(1,2)')

VVV17 = Lorentz(name = 'VVV17',
                spins = [ 3, 3, 3 ],
                structure = 'P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

SSSS17 = Lorentz(name = 'SSSS17',
                 spins = [ 1, 1, 1, 1 ],
                 structure = '1')

FFVV24 = Lorentz(name = 'FFVV24',
                 spins = [ 2, 2, 3, 3 ],
                 structure = '-(Epsilon(3,4,-1,-2)*P(-2,4)*P(-1,3)*Identity(2,1)) + Epsilon(3,4,-1,-2)*P(-2,3)*P(-1,4)*Identity(2,1)')

VVSS17 = Lorentz(name = 'VVSS17',
                 spins = [ 3, 3, 1, 1 ],
                 structure = 'Metric(1,2)')

VVVV81 = Lorentz(name = 'VVVV81',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4)')

VVVV82 = Lorentz(name = 'VVVV82',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) + Metric(1,3)*Metric(2,4) - 2*Metric(1,2)*Metric(3,4)')

VVVV83 = Lorentz(name = 'VVVV83',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,2)*Metric(3,4)')

VVVV84 = Lorentz(name = 'VVVV84',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,3)*Metric(2,4) - Metric(1,2)*Metric(3,4)')

VVVV85 = Lorentz(name = 'VVVV85',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - (Metric(1,3)*Metric(2,4))/2. - (Metric(1,2)*Metric(3,4))/2.')

FFVVV15 = Lorentz(name = 'FFVVV15',
                  spins = [ 2, 2, 3, 3, 3 ],
                  structure = '-(Epsilon(3,4,5,-1)*P(-1,3)*Identity(2,1)) - Epsilon(3,4,5,-1)*P(-1,4)*Identity(2,1) - Epsilon(3,4,5,-1)*P(-1,5)*Identity(2,1)')

