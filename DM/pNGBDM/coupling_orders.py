# This file was automatically created by FeynRules 2.0.31
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (November 20, 2012)
# Date: Tue 8 Dec 2020 10:18:03


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

NPk = CouplingOrder(name = 'NPk',
                    expansion_order = 99,
                    hierarchy = 1)

NPm = CouplingOrder(name = 'NPm',
                    expansion_order = 99,
                    hierarchy = 1)

NPy = CouplingOrder(name = 'NPy',
                    expansion_order = 99,
                    hierarchy = 1)

NPc = CouplingOrder(name = 'NPc',
                    expansion_order = 99,
                    hierarchy = 1)

