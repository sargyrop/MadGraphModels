Requestor: Xiangyang Ju
Content: Dark matter searches in mono-Higgs channel
Paper: http://arxiv.org/abs/1402.7074
Download: https://svnweb.cern.ch/cern/wsvn/LHCDMF/trunk/models/EW_Higgs_2HDM/Zp2HDM_UFO_V1.1.zip
JIRA: https://its.cern.ch/jira/browse/AGENE-955

Update Requestor: Sam Meehan
JIRA: https://its.cern.ch/jira/browse/AGENE-1117
Note: This is an effective update to v1.2 of the model (see README in the model directory)