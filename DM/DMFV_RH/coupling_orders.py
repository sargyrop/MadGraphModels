# This file was automatically created by FeynRules 2.3.24
# Mathematica version: 10.4.0 for Linux x86 (64-bit) (February 26, 2016)
# Date: Wed 5 Oct 2016 13:23:59


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

NP = CouplingOrder(name = 'NP',
                   expansion_order = 99,
                   hierarchy = 1)

