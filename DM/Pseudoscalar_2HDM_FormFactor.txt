Requestor: Yizhou Cai
Content: 2HDM+a model with FormFactor
Source: https://gitlab.cern.ch/yuchen/Pseudoscalar_2HDM_FormFactor
Paper: https://arxiv.org/abs/1701.07427
JIRA: https://its.cern.ch/jira/browse/AGENE-1949
