Requestor: Marco Sessa
Contents: Z boson decays to hidden sector (Dark Higgs + Dark Photon)
Paper: https://arxiv.org/abs/1710.07635v2
Source: Private communication with Brian Shuve
JIRA: https://its.cern.ch/jira/browse/AGENE-1720