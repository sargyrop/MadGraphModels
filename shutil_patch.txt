Requestor: Hannes Mildner
*NOT A MODEL* patch for shutil python module.
When copying files from cvmfs to some local file systems, a "no space left on device" error was called raised by the _copyxattr function of shutil.
Furthermore, permission errors have been observed on some filesystems.
This patched version of shutil.py addresses these issues. It is identical to the one form python 3.9.6, with the exception of L323 and L332, where we no longer raise an exception in case of errno.EACCES or, in the latter line, case errno.ENOSPC.
In MadGraphControl we add the patched module to the PYTHONPATH environment variable.
