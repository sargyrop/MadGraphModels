Requestor: Yury Smirnov
Content: Mulit-charged particles
Run 1 Search (1): http://arxiv.org/abs/1301.5272
Run 1 Search (2): http://arxiv.org/abs/1504.04188
JIRA: https://its.cern.ch/jira/browse/AGENE-988
Update: Anthony Lionti
Update JIRA: https://its.cern.ch/jira/browse/AGENE-1340
Update: Wen Yi Song
Update JIRA: https://its.cern.ch/jira/browse/AGENE-1827
