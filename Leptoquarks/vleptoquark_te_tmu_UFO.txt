Requestor: Julian Nickel
Content: Vector-like quark model with top-electron and top-muon decays
Source: https://github.com/ymzhong/vector_leptoquark
Paper 1: https://arxiv.org/abs/1706.05033
Paper 2: https://arxiv.org/abs/1810.10017
JIRA: https://its.cern.ch/jira/browse/AGENE-1900