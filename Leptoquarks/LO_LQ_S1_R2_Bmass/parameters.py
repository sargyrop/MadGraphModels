# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 8.0 for Mac OS X x86 (64-bit) (October 5, 2011)
# Date: Fri 5 Aug 2022 15:53:21



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
MR2 = Parameter(name = 'MR2',
                nature = 'external',
                type = 'real',
                value = 1600.,
                texname = '\\text{MR2}',
                lhablock = 'LQPARAMR2',
                lhacode = [ 1 ])

MS1 = Parameter(name = 'MS1',
                nature = 'external',
                type = 'real',
                value = 1600.,
                texname = '\\text{MS1}',
                lhablock = 'LQPARAMS1',
                lhacode = [ 1 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

yLRR21x1 = Parameter(name = 'yLRR21x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLRR21x1}',
                     lhablock = 'YUKR2LR',
                     lhacode = [ 1, 1 ])

yLRR21x2 = Parameter(name = 'yLRR21x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLRR21x2}',
                     lhablock = 'YUKR2LR',
                     lhacode = [ 1, 2 ])

yLRR21x3 = Parameter(name = 'yLRR21x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLRR21x3}',
                     lhablock = 'YUKR2LR',
                     lhacode = [ 1, 3 ])

yLRR22x1 = Parameter(name = 'yLRR22x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLRR22x1}',
                     lhablock = 'YUKR2LR',
                     lhacode = [ 2, 1 ])

yLRR22x2 = Parameter(name = 'yLRR22x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLRR22x2}',
                     lhablock = 'YUKR2LR',
                     lhacode = [ 2, 2 ])

yLRR22x3 = Parameter(name = 'yLRR22x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLRR22x3}',
                     lhablock = 'YUKR2LR',
                     lhacode = [ 2, 3 ])

yLRR23x1 = Parameter(name = 'yLRR23x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLRR23x1}',
                     lhablock = 'YUKR2LR',
                     lhacode = [ 3, 1 ])

yLRR23x2 = Parameter(name = 'yLRR23x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLRR23x2}',
                     lhablock = 'YUKR2LR',
                     lhacode = [ 3, 2 ])

yLRR23x3 = Parameter(name = 'yLRR23x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLRR23x3}',
                     lhablock = 'YUKR2LR',
                     lhacode = [ 3, 3 ])

yRLR21x1 = Parameter(name = 'yRLR21x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRLR21x1}',
                     lhablock = 'YUKR2RL',
                     lhacode = [ 1, 1 ])

yRLR21x2 = Parameter(name = 'yRLR21x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRLR21x2}',
                     lhablock = 'YUKR2RL',
                     lhacode = [ 1, 2 ])

yRLR21x3 = Parameter(name = 'yRLR21x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRLR21x3}',
                     lhablock = 'YUKR2RL',
                     lhacode = [ 1, 3 ])

yRLR22x1 = Parameter(name = 'yRLR22x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRLR22x1}',
                     lhablock = 'YUKR2RL',
                     lhacode = [ 2, 1 ])

yRLR22x2 = Parameter(name = 'yRLR22x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRLR22x2}',
                     lhablock = 'YUKR2RL',
                     lhacode = [ 2, 2 ])

yRLR22x3 = Parameter(name = 'yRLR22x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRLR22x3}',
                     lhablock = 'YUKR2RL',
                     lhacode = [ 2, 3 ])

yRLR23x1 = Parameter(name = 'yRLR23x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRLR23x1}',
                     lhablock = 'YUKR2RL',
                     lhacode = [ 3, 1 ])

yRLR23x2 = Parameter(name = 'yRLR23x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRLR23x2}',
                     lhablock = 'YUKR2RL',
                     lhacode = [ 3, 2 ])

yRLR23x3 = Parameter(name = 'yRLR23x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRLR23x3}',
                     lhablock = 'YUKR2RL',
                     lhacode = [ 3, 3 ])

yLLS11x1 = Parameter(name = 'yLLS11x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLLS11x1}',
                     lhablock = 'YUKS1LL',
                     lhacode = [ 1, 1 ])

yLLS11x2 = Parameter(name = 'yLLS11x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLLS11x2}',
                     lhablock = 'YUKS1LL',
                     lhacode = [ 1, 2 ])

yLLS11x3 = Parameter(name = 'yLLS11x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLLS11x3}',
                     lhablock = 'YUKS1LL',
                     lhacode = [ 1, 3 ])

yLLS12x1 = Parameter(name = 'yLLS12x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLLS12x1}',
                     lhablock = 'YUKS1LL',
                     lhacode = [ 2, 1 ])

yLLS12x2 = Parameter(name = 'yLLS12x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLLS12x2}',
                     lhablock = 'YUKS1LL',
                     lhacode = [ 2, 2 ])

yLLS12x3 = Parameter(name = 'yLLS12x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLLS12x3}',
                     lhablock = 'YUKS1LL',
                     lhacode = [ 2, 3 ])

yLLS13x1 = Parameter(name = 'yLLS13x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLLS13x1}',
                     lhablock = 'YUKS1LL',
                     lhacode = [ 3, 1 ])

yLLS13x2 = Parameter(name = 'yLLS13x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLLS13x2}',
                     lhablock = 'YUKS1LL',
                     lhacode = [ 3, 2 ])

yLLS13x3 = Parameter(name = 'yLLS13x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yLLS13x3}',
                     lhablock = 'YUKS1LL',
                     lhacode = [ 3, 3 ])

yRRS11x1 = Parameter(name = 'yRRS11x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRRS11x1}',
                     lhablock = 'YUKS1RR',
                     lhacode = [ 1, 1 ])

yRRS11x2 = Parameter(name = 'yRRS11x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRRS11x2}',
                     lhablock = 'YUKS1RR',
                     lhacode = [ 1, 2 ])

yRRS11x3 = Parameter(name = 'yRRS11x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRRS11x3}',
                     lhablock = 'YUKS1RR',
                     lhacode = [ 1, 3 ])

yRRS12x1 = Parameter(name = 'yRRS12x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRRS12x1}',
                     lhablock = 'YUKS1RR',
                     lhacode = [ 2, 1 ])

yRRS12x2 = Parameter(name = 'yRRS12x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRRS12x2}',
                     lhablock = 'YUKS1RR',
                     lhacode = [ 2, 2 ])

yRRS12x3 = Parameter(name = 'yRRS12x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRRS12x3}',
                     lhablock = 'YUKS1RR',
                     lhacode = [ 2, 3 ])

yRRS13x1 = Parameter(name = 'yRRS13x1',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRRS13x1}',
                     lhablock = 'YUKS1RR',
                     lhacode = [ 3, 1 ])

yRRS13x2 = Parameter(name = 'yRRS13x2',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRRS13x2}',
                     lhablock = 'YUKS1RR',
                     lhacode = [ 3, 2 ])

yRRS13x3 = Parameter(name = 'yRRS13x3',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{yRRS13x3}',
                     lhablock = 'YUKS1RR',
                     lhacode = [ 3, 3 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WR223 = Parameter(name = 'WR223',
                  nature = 'internal',
                  type = 'real',
                  value = '(MR2*(abs(yLRR21x1)**2 + abs(yLRR21x2)**2 + abs(yLRR21x3)**2 + abs(yLRR22x1)**2 + abs(yLRR22x2)**2 + abs(yLRR22x3)**2 + abs(yLRR23x1)**2 + abs(yLRR23x2)**2 + abs(yLRR23x3)**2 + abs(yRLR21x1)**2 + abs(yRLR21x2)**2 + abs(yRLR21x3)**2 + abs(yRLR22x1)**2 + abs(yRLR22x2)**2 + abs(yRLR22x3)**2))/(16.*cmath.pi) + ((MR2**2 - ymt**2)**2*(abs(yRLR23x1)**2 + abs(yRLR23x2)**2 + abs(yRLR23x3)**2))/(16.*cmath.pi*MR2**3)',
                  texname = '\\text{WR223}')

WR253 = Parameter(name = 'WR253',
                  nature = 'internal',
                  type = 'real',
                  value = '(MR2*(abs(yLRR21x1)**2 + abs(yLRR21x2)**2 + abs(yLRR22x1)**2 + abs(yLRR22x2)**2 + abs(yLRR23x1)**2 + abs(yLRR23x2)**2 + abs(yRLR21x1)**2 + abs(yRLR21x2)**2 + abs(yRLR21x3)**2 + abs(yRLR22x1)**2 + abs(yRLR22x2)**2 + abs(yRLR22x3)**2) + ((MR2**2 - ymt**2)**2*(abs(yLRR21x3)**2 + abs(yLRR22x3)**2 + abs(yLRR23x3)**2 + abs(yRLR23x1)**2 + abs(yRLR23x2)**2 + abs(yRLR23x3)**2))/MR2**3)/(16.*cmath.pi)',
                  texname = '\\text{WR253}')

WS1 = Parameter(name = 'WS1',
                nature = 'internal',
                type = 'real',
                value = '(MS1*(2*abs(yLLS11x1)**2 + 2*abs(yLLS11x2)**2 + 2*abs(yLLS11x3)**2 + 2*abs(yLLS12x1)**2 + 2*abs(yLLS12x2)**2 + 2*abs(yLLS12x3)**2 + abs(yLLS13x1)**2 + abs(yLLS13x2)**2 + abs(yLLS13x3)**2 + abs(yRRS11x1)**2 + abs(yRRS11x2)**2 + abs(yRRS11x3)**2 + abs(yRRS12x1)**2 + abs(yRRS12x2)**2 + abs(yRRS12x3)**2) + ((MS1**2 - ymt**2)**2*(abs(yLLS13x1)**2 + abs(yLLS13x2)**2 + abs(yLLS13x3)**2 + abs(yRRS13x1)**2 + abs(yRRS13x2)**2 + abs(yRRS13x3)**2))/MS1**3)/(16.*cmath.pi)',
                texname = '\\text{WS1}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

