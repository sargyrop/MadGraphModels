# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 8.0 for Mac OS X x86 (64-bit) (October 5, 2011)
# Date: Fri 9 Feb 2018 16:12:13



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default Parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined Parameters.
MS1 = Parameter(name = 'MS1',
                nature = 'external',
                type = 'real',
                value = 650.,
                texname = '\\text{MS1}',
                lhablock = 'MASS',
                lhacode = [ 9000005 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

yLL1x1 = Parameter(name = 'yLL1x1',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{yLL1x1}',
                   lhablock = 'YUKS1LL',
                   lhacode = [ 1, 1 ])

yLL1x2 = Parameter(name = 'yLL1x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yLL1x2}',
                   lhablock = 'YUKS1LL',
                   lhacode = [ 1, 2 ])

yLL1x3 = Parameter(name = 'yLL1x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yLL1x3}',
                   lhablock = 'YUKS1LL',
                   lhacode = [ 1, 3 ])

yLL2x1 = Parameter(name = 'yLL2x1',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yLL2x1}',
                   lhablock = 'YUKS1LL',
                   lhacode = [ 2, 1 ])

yLL2x2 = Parameter(name = 'yLL2x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yLL2x2}',
                   lhablock = 'YUKS1LL',
                   lhacode = [ 2, 2 ])

yLL2x3 = Parameter(name = 'yLL2x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yLL2x3}',
                   lhablock = 'YUKS1LL',
                   lhacode = [ 2, 3 ])

yLL3x1 = Parameter(name = 'yLL3x1',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yLL3x1}',
                   lhablock = 'YUKS1LL',
                   lhacode = [ 3, 1 ])

yLL3x2 = Parameter(name = 'yLL3x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yLL3x2}',
                   lhablock = 'YUKS1LL',
                   lhacode = [ 3, 2 ])

yLL3x3 = Parameter(name = 'yLL3x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yLL3x3}',
                   lhablock = 'YUKS1LL',
                   lhacode = [ 3, 3 ])

yRR1x1 = Parameter(name = 'yRR1x1',
                   nature = 'external',
                   type = 'real',
                   value = 0.1,
                   texname = '\\text{yRR1x1}',
                   lhablock = 'YUKS1RR',
                   lhacode = [ 1, 1 ])

yRR1x2 = Parameter(name = 'yRR1x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yRR1x2}',
                   lhablock = 'YUKS1RR',
                   lhacode = [ 1, 2 ])

yRR1x3 = Parameter(name = 'yRR1x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yRR1x3}',
                   lhablock = 'YUKS1RR',
                   lhacode = [ 1, 3 ])

yRR2x1 = Parameter(name = 'yRR2x1',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yRR2x1}',
                   lhablock = 'YUKS1RR',
                   lhacode = [ 2, 1 ])

yRR2x2 = Parameter(name = 'yRR2x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yRR2x2}',
                   lhablock = 'YUKS1RR',
                   lhacode = [ 2, 2 ])

yRR2x3 = Parameter(name = 'yRR2x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yRR2x3}',
                   lhablock = 'YUKS1RR',
                   lhacode = [ 2, 3 ])

yRR3x1 = Parameter(name = 'yRR3x1',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yRR3x1}',
                   lhablock = 'YUKS1RR',
                   lhacode = [ 3, 1 ])

yRR3x2 = Parameter(name = 'yRR3x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yRR3x2}',
                   lhablock = 'YUKS1RR',
                   lhacode = [ 3, 2 ])

yRR3x3 = Parameter(name = 'yRR3x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{yRR3x3}',
                   lhablock = 'YUKS1RR',
                   lhacode = [ 3, 3 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])
#MH02 = Parameter(name = 'MH02',
#                 nature = 'external',
#                 type = 'real',
#                 value = 399.960116,
#                 texname = '\\text{MH02}',
#                 lhablock = 'MASS',
#                 lhacode = [ 35 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WS1 = Parameter(name = 'WS1',
                nature = 'internal',
                type = 'real',
                value = '(MS1*(2*abs(yLL1x1)**2 + 2*abs(yLL1x2)**2 + 2*abs(yLL1x3)**2 + 2*abs(yLL2x1)**2 + 2*abs(yLL2x2)**2 + 2*abs(yLL2x3)**2 + abs(yLL3x1)**2 + abs(yLL3x2)**2 + abs(yLL3x3)**2 + abs(yRR1x1)**2 + abs(yRR1x2)**2 + abs(yRR1x3)**2 + abs(yRR2x1)**2 + abs(yRR2x2)**2 + abs(yRR2x3)**2) + ((MS1**2 - ymt**2)**2*(abs(yLL3x1)**2 + abs(yLL3x2)**2 + abs(yLL3x3)**2 + abs(yRR3x1)**2 + abs(yRR3x2)**2 + abs(yRR3x3)**2))/MS1**3)/(16.*cmath.pi)',
                texname = '\\text{WS1}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')


Wgo = Parameter(name = 'Wgo',
                nature = 'external',
                type = 'real',
                value = 5.50675438,
                texname = '\\text{Wgo}',
                lhablock = 'DECAY',
                lhacode = [ 1000021 ])


# This file was automatically created by FeynRules 2.3.2
# Mathematica version: 10.0 for Linux x86 (64-bit) (September 9, 2014)
# Date: Tue 9 Jun 2015 02:27:01



# User-defined Parameters.
RRd1x1 = Parameter(name = 'RRd1x1',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{RRd1x1}',
                   lhablock = 'DSQMIX',
                   lhacode = [ 1, 1 ])

RRd2x2 = Parameter(name = 'RRd2x2',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{RRd2x2}',
                   lhablock = 'DSQMIX',
                   lhacode = [ 2, 2 ])

RRd3x3 = Parameter(name = 'RRd3x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.938737896,
                   texname = '\\text{RRd3x3}',
                   lhablock = 'DSQMIX',
                   lhacode = [ 3, 3 ])

RRd3x6 = Parameter(name = 'RRd3x6',
                   nature = 'external',
                   type = 'real',
                   value = 0.344631925,
                   texname = '\\text{RRd3x6}',
                   lhablock = 'DSQMIX',
                   lhacode = [ 3, 6 ])

#
#
#Mneu1 = Parameter(name = 'Mneu1',
#                  nature = 'external',
#                  type = 'real',
#                  value = 96.6880686,
#                  texname = '\\text{Mneu1}',
#                  lhablock = 'MASS',
#                  lhacode = [ 1000022 ])
#
#Mneu2 = Parameter(name = 'Mneu2',
#                  nature = 'external',
#                  type = 'real',
#                  value = 181.088157,
#                  texname = '\\text{Mneu2}',
#                  lhablock = 'MASS',
#                  lhacode = [ 1000023 ])
#
#Mneu3 = Parameter(name = 'Mneu3',
#                  nature = 'external',
#                  type = 'real',
#                  value = -363.756027,
#                  texname = '\\text{Mneu3}',
#                  lhablock = 'MASS',
#                  lhacode = [ 1000025 ])
#
#Mneu4 = Parameter(name = 'Mneu4',
#                  nature = 'external',
#                  type = 'real',
#                  value = 381.729382,
#                  texname = '\\text{Mneu4}',
#                  lhablock = 'MASS',
#                  lhacode = [ 1000035 ])
#
#Mch1 = Parameter(name = 'Mch1',
#                 nature = 'external',
#                 type = 'real',
#                 value = 181.696474,
#                 texname = '\\text{Mch1}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000024 ])
#
#Mch2 = Parameter(name = 'Mch2',
#                 nature = 'external',
#                 type = 'real',
#                 value = 379.93932,
#                 texname = '\\text{Mch2}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000037 ])
#
#Mgo = Parameter(name = 'Mgo',
#                nature = 'external',
#                type = 'real',
#                value = 607.713704,
#                texname = '\\text{Mgo}',
#                lhablock = 'MASS',
#                lhacode = [ 1000021 ])
#
#MA0 = Parameter(name = 'MA0',
#                nature = 'external',
#                type = 'real',
#                value = 399.583917,
#                texname = '\\text{MA0}',
#                lhablock = 'MASS',
#                lhacode = [ 36 ])
#
#Mta = Parameter(name = 'Mta',
#                nature = 'external',
#                type = 'real',
#                value = 1.777,
#                texname = '\\text{Mta}',
#                lhablock = 'MASS',
#                lhacode = [ 15 ])
#
#
#Msn1 = Parameter(name = 'Msn1',
#                 nature = 'external',
#                 type = 'real',
#                 value = 185.258326,
#                 texname = '\\text{Msn1}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000012 ])
#
#Msn2 = Parameter(name = 'Msn2',
#                 nature = 'external',
#                 type = 'real',
#                 value = 185.258326,
#                 texname = '\\text{Msn2}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000014 ])
#
#Msn3 = Parameter(name = 'Msn3',
#                 nature = 'external',
#                 type = 'real',
#                 value = 184.708464,
#                 texname = '\\text{Msn3}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000016 ])
#
#
#Msl1 = Parameter(name = 'Msl1',
#                 nature = 'external',
#                 type = 'real',
#                 value = 202.91569,
#                 texname = '\\text{Msl1}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000011 ])
#
#Msl2 = Parameter(name = 'Msl2',
#                 nature = 'external',
#                 type = 'real',
#                 value = 202.91569,
#                 texname = '\\text{Msl2}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000013 ])
#
#Msl3 = Parameter(name = 'Msl3',
#                 nature = 'external',
#                 type = 'real',
#                 value = 134.490864,
#                 texname = '\\text{Msl3}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000015 ])
#
#Msl4 = Parameter(name = 'Msl4',
#                 nature = 'external',
#                 type = 'real',
#                 value = 144.102799,
#                 texname = '\\text{Msl4}',
#                 lhablock = 'MASS',
#                 lhacode = [ 2000011 ])
#
#Msl5 = Parameter(name = 'Msl5',
#                 nature = 'external',
#                 type = 'real',
#                 value = 144.102799,
#                 texname = '\\text{Msl5}',
#                 lhablock = 'MASS',
#                 lhacode = [ 2000013 ])
#
#Msl6 = Parameter(name = 'Msl6',
#                 nature = 'external',
#                 type = 'real',
#                 value = 206.867805,
#                 texname = '\\text{Msl6}',
#                 lhablock = 'MASS',
#                 lhacode = [ 2000015 ])
#
#Msu1 = Parameter(name = 'Msu1',
#                 nature = 'external',
#                 type = 'real',
#                 value = 561.119014,
#                 texname = '\\text{Msu1}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000002 ])
#
#Msu2 = Parameter(name = 'Msu2',
#                 nature = 'external',
#                 type = 'real',
#                 value = 561.119014,
#                 texname = '\\text{Msu2}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000004 ])
#
#Msu3 = Parameter(name = 'Msu3',
#                 nature = 'external',
#                 type = 'real',
#                 value = 399.668493,
#                 texname = '\\text{Msu3}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000006 ])
#
#Msu4 = Parameter(name = 'Msu4',
#                 nature = 'external',
#                 type = 'real',
#                 value = 549.259265,
#                 texname = '\\text{Msu4}',
#                 lhablock = 'MASS',
#                 lhacode = [ 2000002 ])
#
#Msu5 = Parameter(name = 'Msu5',
#                 nature = 'external',
#                 type = 'real',
#                 value = 549.259265,
#                 texname = '\\text{Msu5}',
#                 lhablock = 'MASS',
#                 lhacode = [ 2000004 ])
#
#Msu6 = Parameter(name = 'Msu6',
#                 nature = 'external',
#                 type = 'real',
#                 value = 585.785818,
#                 texname = '\\text{Msu6}',
#                 lhablock = 'MASS',
#                 lhacode = [ 2000006 ])
#
#Msd1 = Parameter(name = 'Msd1',
#                 nature = 'external',
#                 type = 'real',
#                 value = 568.441109,
#                 texname = '\\text{Msd1}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000001 ])
#
#Msd2 = Parameter(name = 'Msd2',
#                 nature = 'external',
#                 type = 'real',
#                 value = 568.441109,
#                 texname = '\\text{Msd2}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000003 ])
#
#Msd3 = Parameter(name = 'Msd3',
#                 nature = 'external',
#                 type = 'real',
#                 value = 513.065179,
#                 texname = '\\text{Msd3}',
#                 lhablock = 'MASS',
#                 lhacode = [ 1000005 ])
#
#Msd4 = Parameter(name = 'Msd4',
#                 nature = 'external',
#                 type = 'real',
#                 value = 545.228462,
#                 texname = '\\text{Msd4}',
#                 lhablock = 'Mass',
#                 lhacode = [ 2000001 ])
#
#Msd5 = Parameter(name = 'Msd5',
#                 nature = 'external',
#                 type = 'real',
#                 value = 545.228462,
#                 texname = '\\text{Msd5}',
#                 lhablock = 'Mass',
#                 lhacode = [ 2000003 ])
#
#Msd6 = Parameter(name = 'Msd6',
#                 nature = 'external',
#                 type = 'real',
#                 value = 543.726676,
#                 texname = '\\text{Msd6}',
#                 lhablock = 'Mass',
#                 lhacode = [ 2000005 ])
#
#
#Wneu1 = Parameter(name = 'Wneu1',
#                  nature = 'external',
#                  type = 'real',
#                  value = 0.0027770048,
#                  texname = '\\text{Wneu1}',
#                  lhablock = 'DECAY',
#                  lhacode = [ 1000022 ])
#
#Wneu2 = Parameter(name = 'Wneu2',
#                  nature = 'external',
#                  type = 'real',
#                  value = 0.0207770048,
#                  texname = '\\text{Wneu2}',
#                  lhablock = 'DECAY',
#                  lhacode = [ 1000023 ])
#
#Wneu3 = Parameter(name = 'Wneu3',
#                  nature = 'external',
#                  type = 'real',
#                  value = 1.91598495,
#                  texname = '\\text{Wneu3}',
#                  lhablock = 'DECAY',
#                  lhacode = [ 1000025 ])
#
#Wneu4 = Parameter(name = 'Wneu4',
#                  nature = 'external',
#                  type = 'real',
#                  value = 2.58585079,
#                  texname = '\\text{Wneu4}',
#                  lhablock = 'DECAY',
#                  lhacode = [ 1000035 ])
#
#Wch1 = Parameter(name = 'Wch1',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.0170414503,
#                 texname = '\\text{Wch1}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000024 ])
#
#
#Wch2 = Parameter(name = 'Wch2',
#                 nature = 'external',
#                 type = 'real',
#                 value = 2.4868951,
#                 texname = '\\text{Wch2}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000037 ])
#
#
#
#WH02 = Parameter(name = 'WH02',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.574801389,
#                 texname = '\\text{WH02}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 35 ])
#
#WA0 = Parameter(name = 'WA0',
#                nature = 'external',
#                type = 'real',
#                value = 0.632178488,
#                texname = '\\text{WA0}',
#                lhablock = 'DECAY',
#                lhacode = [ 36 ])
#
#Wsn1 = Parameter(name = 'Wsn1',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.149881634,
#                 texname = '\\text{Wsn1}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000012 ])
#
#Wsn2 = Parameter(name = 'Wsn2',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.149881634,
#                 texname = '\\text{Wsn2}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000014 ])
#
#Wsn3 = Parameter(name = 'Wsn3',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.147518977,
#                 texname = '\\text{Wsn3}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000016 ])
#
#Wsl1 = Parameter(name = 'Wsl1',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.213682161,
#                 texname = '\\text{Wsl1}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000011 ])
#
#Wsl2 = Parameter(name = 'Wsl2',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.213682161,
#                 texname = '\\text{Wsl2}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000013 ])
#
#Wsl3 = Parameter(name = 'Wsl3',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.148327268,
#                 texname = '\\text{Wsl3}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000015 ])
#
#Wsl4 = Parameter(name = 'Wsl4',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.216121626,
#                 texname = '\\text{Wsl4}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 2000011 ])
#
#Wsl5 = Parameter(name = 'Wsl5',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.216121626,
#                 texname = '\\text{Wsl5}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 2000013 ])
#
#Wsl6 = Parameter(name = 'Wsl6',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.269906096,
#                 texname = '\\text{Wsl6}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 2000015 ])
#
#Wsu1 = Parameter(name = 'Wsu1',
#                 nature = 'external',
#                 type = 'real',
#                 value = 5.47719539,
#                 texname = '\\text{Wsu1}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000002 ])
#
#Wsu2 = Parameter(name = 'Wsu2',
#                 nature = 'external',
#                 type = 'real',
#                 value = 5.47719539,
#                 texname = '\\text{Wsu2}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000004 ])
#
#Wsu3 = Parameter(name = 'Wsu3',
#                 nature = 'external',
#                 type = 'real',
#                 value = 2.02159578,
#                 texname = '\\text{Wsu3}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000006 ])
#
#Wsu4 = Parameter(name = 'Wsu4',
#                 nature = 'external',
#                 type = 'real',
#                 value = 1.15297292,
#                 texname = '\\text{Wsu4}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 2000002 ])
#
#Wsu5 = Parameter(name = 'Wsu5',
#                 nature = 'external',
#                 type = 'real',
#                 value = 1.15297292,
#                 texname = '\\text{Wsu5}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 2000004 ])
#
#Wsu6 = Parameter(name = 'Wsu6',
#                 nature = 'external',
#                 type = 'real',
#                 value = 7.37313275,
#                 texname = '\\text{Wsu6}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 2000006 ])
#
#Wsd1 = Parameter(name = 'Wsd1',
#                 nature = 'external',
#                 type = 'real',
#                 value = 5.31278772,
#                 texname = '\\text{Wsd1}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000001 ])
#
#Wsd2 = Parameter(name = 'Wsd2',
#                 nature = 'external',
#                 type = 'real',
#                 value = 5.31278772,
#                 texname = '\\text{Wsd2}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000003 ])
#
#Wsd3 = Parameter(name = 'Wsd3',
#                 nature = 'external',
#                 type = 'real',
#                 value = 3.73627601,
#                 texname = '\\text{Wsd3}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 1000005 ])
#
#Wsd4 = Parameter(name = 'Wsd4',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.285812308,
#                 texname = '\\text{Wsd4}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 2000001 ])
#
#Wsd5 = Parameter(name = 'Wsd5',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.285812308,
#                 texname = '\\text{Wsd5}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 2000003 ])
#
#Wsd6 = Parameter(name = 'Wsd6',
#                 nature = 'external',
#                 type = 'real',
#                 value = 0.801566294,
#                 texname = '\\text{Wsd6}',
#                 lhablock = 'DECAY',
#                 lhacode = [ 2000005 ])
