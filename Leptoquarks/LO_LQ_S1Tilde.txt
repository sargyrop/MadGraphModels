Requestor: Marcus Morgenstern
Content: Single leptoquark production at LO in the S1 representation
Paper: https://arxiv.org/abs/1801.07641
Source: https://lqnlo.hepforge.org/downloads/
JIRA: https://its.cern.ch/jira/browse/AGENE-1629