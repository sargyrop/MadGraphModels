# This file was automatically created by FeynRules =.2.4
# Mathematica version: 8.0 for Linux x86 (32-bit) (October 10, 2011)
# Date: Sat 31 Oct 2015 20:00:40


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV1, L.VVV2, L.VVV4, L.VVV6, L.VVV7, L.VVV8 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_255_80,(0,0,1):C.R2GC_255_81,(0,1,0):C.R2GC_258_82,(0,1,1):C.R2GC_258_83,(0,2,0):C.R2GC_258_82,(0,2,1):C.R2GC_258_83,(0,3,0):C.R2GC_255_80,(0,3,1):C.R2GC_255_81,(0,4,0):C.R2GC_255_80,(0,4,1):C.R2GC_255_81,(0,5,0):C.R2GC_258_82,(0,5,1):C.R2GC_258_83})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(2,0,0):C.R2GC_131_29,(2,0,1):C.R2GC_131_30,(0,0,0):C.R2GC_131_29,(0,0,1):C.R2GC_131_30,(4,0,0):C.R2GC_129_25,(4,0,1):C.R2GC_129_26,(3,0,0):C.R2GC_129_25,(3,0,1):C.R2GC_129_26,(8,0,0):C.R2GC_130_27,(8,0,1):C.R2GC_130_28,(7,0,0):C.R2GC_136_36,(7,0,1):C.R2GC_264_88,(6,0,0):C.R2GC_135_34,(6,0,1):C.R2GC_265_89,(5,0,0):C.R2GC_129_25,(5,0,1):C.R2GC_129_26,(1,0,0):C.R2GC_129_25,(1,0,1):C.R2GC_129_26,(11,0,0):C.R2GC_133_32,(11,0,1):C.R2GC_133_33,(10,0,0):C.R2GC_133_32,(10,0,1):C.R2GC_133_33,(9,0,1):C.R2GC_132_31,(2,1,0):C.R2GC_131_29,(2,1,1):C.R2GC_131_30,(0,1,0):C.R2GC_131_29,(0,1,1):C.R2GC_131_30,(6,1,0):C.R2GC_262_85,(6,1,1):C.R2GC_262_86,(4,1,0):C.R2GC_129_25,(4,1,1):C.R2GC_129_26,(3,1,0):C.R2GC_129_25,(3,1,1):C.R2GC_129_26,(8,1,0):C.R2GC_130_27,(8,1,1):C.R2GC_266_90,(7,1,0):C.R2GC_136_36,(7,1,1):C.R2GC_136_37,(5,1,0):C.R2GC_129_25,(5,1,1):C.R2GC_129_26,(1,1,0):C.R2GC_129_25,(1,1,1):C.R2GC_129_26,(11,1,0):C.R2GC_133_32,(11,1,1):C.R2GC_133_33,(10,1,0):C.R2GC_133_32,(10,1,1):C.R2GC_133_33,(9,1,1):C.R2GC_132_31,(2,2,0):C.R2GC_131_29,(2,2,1):C.R2GC_131_30,(0,2,0):C.R2GC_131_29,(0,2,1):C.R2GC_131_30,(4,2,0):C.R2GC_129_25,(4,2,1):C.R2GC_129_26,(3,2,0):C.R2GC_129_25,(3,2,1):C.R2GC_129_26,(8,2,0):C.R2GC_130_27,(8,2,1):C.R2GC_263_87,(6,2,0):C.R2GC_135_34,(6,2,1):C.R2GC_135_35,(7,2,0):C.R2GC_261_84,(7,2,1):C.R2GC_131_30,(5,2,0):C.R2GC_129_25,(5,2,1):C.R2GC_129_26,(1,2,0):C.R2GC_129_25,(1,2,1):C.R2GC_129_26,(11,2,0):C.R2GC_133_32,(11,2,1):C.R2GC_133_33,(10,2,0):C.R2GC_133_32,(10,2,1):C.R2GC_133_33,(9,2,1):C.R2GC_132_31})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.ta__minus__, P.t, P.lqsb__tilde__ ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.g, P.lqsb, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_232_64})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.vt__tilde__, P.b__tilde__, P.lqsb ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.b, P.g, P.lqsb] ] ],
               couplings = {(0,0,0):C.R2GC_191_50})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.vm__tilde__, P.c, P.lqsc__tilde__ ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.c, P.g, P.lqsc] ] ],
               couplings = {(0,0,0):C.R2GC_192_51})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.mu__plus__, P.s, P.lqsc__tilde__ ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.g, P.lqsc, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_229_62})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.ve__tilde__, P.d__tilde__, P.lqsd ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.d, P.g, P.lqsd] ] ],
               couplings = {(0,0,0):C.R2GC_196_54})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.mu__minus__, P.c, P.lqss__tilde__ ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.c, P.g, P.lqss] ] ],
               couplings = {(0,0,0):C.R2GC_193_52})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.vm__tilde__, P.s__tilde__, P.lqss ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.g, P.lqss, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_230_63})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.ta__plus__, P.b, P.lqst__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.b, P.g, P.lqst] ] ],
                couplings = {(0,0,0):C.R2GC_241_67})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.vt__tilde__, P.t, P.lqst__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqst, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_271_91})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.e__plus__, P.d, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.d, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_198_55})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.ve__tilde__, P.u, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqsu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_236_66})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.u, P.e__minus__, P.lqsd__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqsd, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_234_65})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_278_94,(0,1,0):C.R2GC_279_95})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_246_70})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_245_69})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_281_97})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_282_98})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_280_96,(0,1,0):C.R2GC_277_93})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.ta__plus__, P.t__tilde__, P.lqsb ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsb, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_232_64})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.s__tilde__, P.mu__minus__, P.lqsc ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsc, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_229_62})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.u__tilde__, P.e__plus__, P.lqsd ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_234_65})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.mu__plus__, P.c__tilde__, P.lqss ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.lqss] ] ],
                couplings = {(0,0,0):C.R2GC_193_52})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.b__tilde__, P.ta__minus__, P.lqst ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqst] ] ],
                couplings = {(0,0,0):C.R2GC_241_67})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.d__tilde__, P.e__minus__, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_198_55})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.vt, P.b, P.lqsb__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqsb] ] ],
                couplings = {(0,0,0):C.R2GC_191_50})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.c__tilde__, P.vm, P.lqsc ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.lqsc] ] ],
                couplings = {(0,0,0):C.R2GC_192_51})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.ve, P.d, P.lqsd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_196_54})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.vm, P.s, P.lqss__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqss, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_230_63})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.t__tilde__, P.vt, P.lqst ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqst, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_271_91})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.u__tilde__, P.ve, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_236_66})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.g, P.lqsu__tilde__, P.lqsu ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_150_47,(0,1,0):C.R2GC_149_46})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.g, P.g, P.lqsu__tilde__, P.lqsu ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsu] ] ],
                couplings = {(2,0,0):C.R2GC_151_48,(2,0,1):C.R2GC_151_49,(1,0,0):C.R2GC_151_48,(1,0,1):C.R2GC_151_49,(0,0,0):C.R2GC_133_33,(0,0,1):C.R2GC_147_44})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.g, P.lqsd__tilde__, P.lqsd ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_150_47,(0,1,0):C.R2GC_149_46})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.g, P.g, P.lqsd__tilde__, P.lqsd ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd] ] ],
                couplings = {(2,0,0):C.R2GC_151_48,(2,0,1):C.R2GC_151_49,(1,0,0):C.R2GC_151_48,(1,0,1):C.R2GC_151_49,(0,0,0):C.R2GC_133_33,(0,0,1):C.R2GC_147_44})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.g, P.lqsc__tilde__, P.lqsc ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqsc] ] ],
                couplings = {(0,0,0):C.R2GC_150_47,(0,1,0):C.R2GC_149_46})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.g, P.g, P.lqsc__tilde__, P.lqsc ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc] ] ],
                couplings = {(2,0,0):C.R2GC_151_48,(2,0,1):C.R2GC_151_49,(1,0,0):C.R2GC_151_48,(1,0,1):C.R2GC_151_49,(0,0,0):C.R2GC_133_33,(0,0,1):C.R2GC_147_44})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.g, P.lqss__tilde__, P.lqss ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqss] ] ],
                couplings = {(0,0,0):C.R2GC_150_47,(0,1,0):C.R2GC_149_46})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.g, P.g, P.lqss__tilde__, P.lqss ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqss] ] ],
                couplings = {(2,0,0):C.R2GC_151_48,(2,0,1):C.R2GC_151_49,(1,0,0):C.R2GC_151_48,(1,0,1):C.R2GC_151_49,(0,0,0):C.R2GC_133_33,(0,0,1):C.R2GC_147_44})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.g, P.lqst__tilde__, P.lqst ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqst] ] ],
                couplings = {(0,0,0):C.R2GC_150_47,(0,1,0):C.R2GC_149_46})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.g, P.g, P.lqst__tilde__, P.lqst ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqst] ] ],
                couplings = {(2,0,0):C.R2GC_151_48,(2,0,1):C.R2GC_151_49,(1,0,0):C.R2GC_151_48,(1,0,1):C.R2GC_151_49,(0,0,0):C.R2GC_133_33,(0,0,1):C.R2GC_147_44})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.g, P.lqsb__tilde__, P.lqsb ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqsb] ] ],
                couplings = {(0,0,0):C.R2GC_150_47,(0,1,0):C.R2GC_149_46})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.g, P.g, P.lqsb__tilde__, P.lqsb ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb] ] ],
                couplings = {(2,0,0):C.R2GC_151_48,(2,0,1):C.R2GC_151_49,(1,0,0):C.R2GC_151_48,(1,0,1):C.R2GC_151_49,(0,0,0):C.R2GC_133_33,(0,0,1):C.R2GC_147_44})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_140_41})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_140_41})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_140_41})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_138_39})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_138_39})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_138_39})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_195_53})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_195_53})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_195_53})

V_54 = CTVertex(name = 'V_54',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_112_21,(0,1,0):C.R2GC_102_2})

V_55 = CTVertex(name = 'V_55',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_112_21,(0,1,0):C.R2GC_102_2})

V_56 = CTVertex(name = 'V_56',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_112_21,(0,1,0):C.R2GC_102_2})

V_57 = CTVertex(name = 'V_57',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_137_38})

V_58 = CTVertex(name = 'V_58',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_137_38})

V_59 = CTVertex(name = 'V_59',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_137_38})

V_60 = CTVertex(name = 'V_60',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_138_39})

V_61 = CTVertex(name = 'V_61',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_138_39})

V_62 = CTVertex(name = 'V_62',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_138_39})

V_63 = CTVertex(name = 'V_63',
                type = 'R2',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_195_53})

V_64 = CTVertex(name = 'V_64',
                type = 'R2',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_195_53})

V_65 = CTVertex(name = 'V_65',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_195_53})

V_66 = CTVertex(name = 'V_66',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_113_22,(0,1,0):C.R2GC_100_1})

V_67 = CTVertex(name = 'V_67',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_113_22,(0,1,0):C.R2GC_100_1})

V_68 = CTVertex(name = 'V_68',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_113_22,(0,1,0):C.R2GC_100_1})

V_69 = CTVertex(name = 'V_69',
                type = 'R2',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF3, L.FF4, L.FF5, L.FF6 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_242_68,(0,2,0):C.R2GC_242_68,(0,1,0):C.R2GC_139_40,(0,3,0):C.R2GC_139_40})

V_70 = CTVertex(name = 'V_70',
                type = 'R2',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_139_40})

V_71 = CTVertex(name = 'V_71',
                type = 'R2',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_139_40})

V_72 = CTVertex(name = 'V_72',
                type = 'R2',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_139_40})

V_73 = CTVertex(name = 'V_73',
                type = 'R2',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF3, L.FF4, L.FF5, L.FF6 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_273_92,(0,2,0):C.R2GC_273_92,(0,1,0):C.R2GC_139_40,(0,3,0):C.R2GC_139_40})

V_74 = CTVertex(name = 'V_74',
                type = 'R2',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_139_40})

V_75 = CTVertex(name = 'V_75',
                type = 'R2',
                particles = [ P.lqsu__tilde__, P.lqsu ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_252_76,(0,1,0):C.R2GC_148_45})

V_76 = CTVertex(name = 'V_76',
                type = 'R2',
                particles = [ P.lqsd__tilde__, P.lqsd ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_249_73,(0,1,0):C.R2GC_148_45})

V_77 = CTVertex(name = 'V_77',
                type = 'R2',
                particles = [ P.lqsc__tilde__, P.lqsc ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.lqsc] ] ],
                couplings = {(0,0,0):C.R2GC_248_72,(0,1,0):C.R2GC_148_45})

V_78 = CTVertex(name = 'V_78',
                type = 'R2',
                particles = [ P.lqss__tilde__, P.lqss ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.lqss] ] ],
                couplings = {(0,0,0):C.R2GC_250_74,(0,1,0):C.R2GC_148_45})

V_79 = CTVertex(name = 'V_79',
                type = 'R2',
                particles = [ P.lqst__tilde__, P.lqst ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.lqst] ] ],
                couplings = {(0,0,0):C.R2GC_251_75,(0,1,0):C.R2GC_148_45})

V_80 = CTVertex(name = 'V_80',
                type = 'R2',
                particles = [ P.lqsb__tilde__, P.lqsb ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.lqsb] ] ],
                couplings = {(0,0,0):C.R2GC_247_71,(0,1,0):C.R2GC_148_45})

V_81 = CTVertex(name = 'V_81',
                type = 'R2',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV1, L.VV2, L.VV3 ],
                loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                couplings = {(0,0,2):C.R2GC_254_79,(0,1,0):C.R2GC_103_3,(0,1,3):C.R2GC_103_4,(0,2,1):C.R2GC_253_77,(0,2,2):C.R2GC_253_78})

V_82 = CTVertex(name = 'V_82',
                type = 'R2',
                particles = [ P.g, P.g, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_104_5,(0,0,1):C.R2GC_104_6})

V_83 = CTVertex(name = 'V_83',
                type = 'R2',
                particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_116_24,(0,1,0):C.R2GC_116_24,(0,2,0):C.R2GC_116_24})

V_84 = CTVertex(name = 'V_84',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.Z ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_108_13,(0,0,1):C.R2GC_108_14,(0,1,0):C.R2GC_108_13,(0,1,1):C.R2GC_108_14,(0,2,0):C.R2GC_108_13,(0,2,1):C.R2GC_108_14})

V_85 = CTVertex(name = 'V_85',
                type = 'R2',
                particles = [ P.g, P.g, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_111_19,(0,0,1):C.R2GC_111_20,(0,1,0):C.R2GC_111_19,(0,1,1):C.R2GC_111_20,(0,2,0):C.R2GC_111_19,(0,2,1):C.R2GC_111_20})

V_86 = CTVertex(name = 'V_86',
                type = 'R2',
                particles = [ P.a, P.a, P.g, P.g ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_106_9,(0,0,1):C.R2GC_106_10,(0,1,0):C.R2GC_106_9,(0,1,1):C.R2GC_106_10,(0,2,0):C.R2GC_106_9,(0,2,1):C.R2GC_106_10})

V_87 = CTVertex(name = 'V_87',
                type = 'R2',
                particles = [ P.g, P.g, P.g, P.Z ],
                color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(1,0,0):C.R2GC_110_17,(1,0,1):C.R2GC_110_18,(0,1,0):C.R2GC_109_15,(0,1,1):C.R2GC_109_16,(0,2,0):C.R2GC_109_15,(0,2,1):C.R2GC_109_16,(0,3,0):C.R2GC_109_15,(0,3,1):C.R2GC_109_16})

V_88 = CTVertex(name = 'V_88',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.g ],
                color = [ 'd(2,3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_107_11,(0,0,1):C.R2GC_107_12,(0,1,0):C.R2GC_107_11,(0,1,1):C.R2GC_107_12,(0,2,0):C.R2GC_107_11,(0,2,1):C.R2GC_107_12})

V_89 = CTVertex(name = 'V_89',
                type = 'R2',
                particles = [ P.g, P.g, P.H, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_105_7,(0,0,1):C.R2GC_105_8})

V_90 = CTVertex(name = 'V_90',
                type = 'R2',
                particles = [ P.g, P.g, P.G0, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_105_7,(0,0,1):C.R2GC_105_8})

V_91 = CTVertex(name = 'V_91',
                type = 'R2',
                particles = [ P.g, P.g, P.G__minus__, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_115_23})

V_92 = CTVertex(name = 'V_92',
                type = 'R2',
                particles = [ P.lqsu__tilde__, P.lqsu__tilde__, P.lqsu, P.lqsu ],
                color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsu] ] ],
                couplings = {(1,0,0):C.R2GC_146_42,(1,0,1):C.R2GC_146_43,(0,0,0):C.R2GC_146_42,(0,0,1):C.R2GC_146_43})

V_93 = CTVertex(name = 'V_93',
                type = 'R2',
                particles = [ P.lqsd__tilde__, P.lqsd, P.lqsu__tilde__, P.lqsu ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqsu] ], [ [P.g, P.lqsd, P.lqsu] ] ],
                couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_94 = CTVertex(name = 'V_94',
                type = 'R2',
                particles = [ P.lqsd__tilde__, P.lqsd__tilde__, P.lqsd, P.lqsd ],
                color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd] ] ],
                couplings = {(1,0,0):C.R2GC_146_42,(1,0,1):C.R2GC_146_43,(0,0,0):C.R2GC_146_42,(0,0,1):C.R2GC_146_43})

V_95 = CTVertex(name = 'V_95',
                type = 'R2',
                particles = [ P.lqsc__tilde__, P.lqsc, P.lqsu__tilde__, P.lqsu ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqsu] ], [ [P.g, P.lqsc, P.lqsu] ] ],
                couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_96 = CTVertex(name = 'V_96',
                type = 'R2',
                particles = [ P.lqsc__tilde__, P.lqsc, P.lqsd__tilde__, P.lqsd ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqsd] ], [ [P.g, P.lqsc, P.lqsd] ] ],
                couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_97 = CTVertex(name = 'V_97',
                type = 'R2',
                particles = [ P.lqsc__tilde__, P.lqsc__tilde__, P.lqsc, P.lqsc ],
                color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc] ] ],
                couplings = {(1,0,0):C.R2GC_146_42,(1,0,1):C.R2GC_146_43,(0,0,0):C.R2GC_146_42,(0,0,1):C.R2GC_146_43})

V_98 = CTVertex(name = 'V_98',
                type = 'R2',
                particles = [ P.lqss__tilde__, P.lqss, P.lqsu__tilde__, P.lqsu ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqss], [P.g, P.lqsu] ], [ [P.g, P.lqss, P.lqsu] ] ],
                couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_99 = CTVertex(name = 'V_99',
                type = 'R2',
                particles = [ P.lqsd__tilde__, P.lqsd, P.lqss__tilde__, P.lqss ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqss] ], [ [P.g, P.lqsd, P.lqss] ] ],
                couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_100 = CTVertex(name = 'V_100',
                 type = 'R2',
                 particles = [ P.lqsc__tilde__, P.lqsc, P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqss] ], [ [P.g, P.lqsc, P.lqss] ] ],
                 couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_101 = CTVertex(name = 'V_101',
                 type = 'R2',
                 particles = [ P.lqss__tilde__, P.lqss__tilde__, P.lqss, P.lqss ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqss] ] ],
                 couplings = {(1,0,0):C.R2GC_146_42,(1,0,1):C.R2GC_146_43,(0,0,0):C.R2GC_146_42,(0,0,1):C.R2GC_146_43})

V_102 = CTVertex(name = 'V_102',
                 type = 'R2',
                 particles = [ P.lqst__tilde__, P.lqst, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqst], [P.g, P.lqsu] ], [ [P.g, P.lqst, P.lqsu] ] ],
                 couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_103 = CTVertex(name = 'V_103',
                 type = 'R2',
                 particles = [ P.lqsd__tilde__, P.lqsd, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqst] ], [ [P.g, P.lqsd, P.lqst] ] ],
                 couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_104 = CTVertex(name = 'V_104',
                 type = 'R2',
                 particles = [ P.lqsc__tilde__, P.lqsc, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqst] ], [ [P.g, P.lqsc, P.lqst] ] ],
                 couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_105 = CTVertex(name = 'V_105',
                 type = 'R2',
                 particles = [ P.lqss__tilde__, P.lqss, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqss], [P.g, P.lqst] ], [ [P.g, P.lqss, P.lqst] ] ],
                 couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_106 = CTVertex(name = 'V_106',
                 type = 'R2',
                 particles = [ P.lqst__tilde__, P.lqst__tilde__, P.lqst, P.lqst ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqst] ] ],
                 couplings = {(1,0,0):C.R2GC_146_42,(1,0,1):C.R2GC_146_43,(0,0,0):C.R2GC_146_42,(0,0,1):C.R2GC_146_43})

V_107 = CTVertex(name = 'V_107',
                 type = 'R2',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqsu] ], [ [P.g, P.lqsb, P.lqsu] ] ],
                 couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_108 = CTVertex(name = 'V_108',
                 type = 'R2',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqsd] ], [ [P.g, P.lqsb, P.lqsd] ] ],
                 couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_109 = CTVertex(name = 'V_109',
                 type = 'R2',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqsc__tilde__, P.lqsc ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqsc] ], [ [P.g, P.lqsb, P.lqsc] ] ],
                 couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_110 = CTVertex(name = 'V_110',
                 type = 'R2',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqss] ], [ [P.g, P.lqsb, P.lqss] ] ],
                 couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_111 = CTVertex(name = 'V_111',
                 type = 'R2',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqst] ], [ [P.g, P.lqsb, P.lqst] ] ],
                 couplings = {(1,0,0):C.R2GC_200_59,(1,0,1):C.R2GC_200_60,(1,0,2):C.R2GC_200_61,(0,0,0):C.R2GC_199_56,(0,0,1):C.R2GC_199_57,(0,0,2):C.R2GC_199_58})

V_112 = CTVertex(name = 'V_112',
                 type = 'R2',
                 particles = [ P.lqsb__tilde__, P.lqsb__tilde__, P.lqsb, P.lqsb ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb] ] ],
                 couplings = {(1,0,0):C.R2GC_146_42,(1,0,1):C.R2GC_146_43,(0,0,0):C.R2GC_146_42,(0,0,1):C.R2GC_146_43})

V_113 = CTVertex(name = 'V_113',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g ],
                 color = [ 'f(1,2,3)' ],
                 lorentz = [ L.VVV1, L.VVV2, L.VVV3, L.VVV4, L.VVV5, L.VVV6, L.VVV7, L.VVV8 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsb] ], [ [P.lqsb], [P.lqsc], [P.lqsd], [P.lqss], [P.lqst], [P.lqsu] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_255_160,(0,0,1):C.UVGC_255_161,(0,0,2):C.UVGC_255_162,(0,0,3):C.UVGC_134_16,(0,0,4):C.UVGC_255_163,(0,0,6):C.UVGC_255_164,(0,0,7):C.UVGC_255_165,(0,0,8):C.UVGC_255_166,(0,0,9):C.UVGC_255_167,(0,0,10):C.UVGC_255_168,(0,0,11):C.UVGC_255_169,(0,1,0):C.UVGC_258_180,(0,1,1):C.UVGC_258_181,(0,1,2):C.UVGC_259_190,(0,1,3):C.UVGC_259_191,(0,1,4):C.UVGC_259_192,(0,1,6):C.UVGC_259_193,(0,1,7):C.UVGC_259_194,(0,1,8):C.UVGC_259_195,(0,1,9):C.UVGC_259_196,(0,1,10):C.UVGC_259_197,(0,1,11):C.UVGC_258_189,(0,3,0):C.UVGC_258_180,(0,3,1):C.UVGC_258_181,(0,3,2):C.UVGC_258_182,(0,3,3):C.UVGC_128_6,(0,3,4):C.UVGC_258_183,(0,3,6):C.UVGC_258_184,(0,3,7):C.UVGC_258_185,(0,3,8):C.UVGC_258_186,(0,3,9):C.UVGC_258_187,(0,3,10):C.UVGC_258_188,(0,3,11):C.UVGC_258_189,(0,5,0):C.UVGC_255_160,(0,5,1):C.UVGC_255_161,(0,5,2):C.UVGC_257_172,(0,5,3):C.UVGC_257_173,(0,5,4):C.UVGC_257_174,(0,5,6):C.UVGC_257_175,(0,5,7):C.UVGC_257_176,(0,5,8):C.UVGC_257_177,(0,5,9):C.UVGC_257_178,(0,5,10):C.UVGC_257_179,(0,5,11):C.UVGC_255_169,(0,6,0):C.UVGC_255_160,(0,6,1):C.UVGC_255_161,(0,6,2):C.UVGC_256_170,(0,6,3):C.UVGC_256_171,(0,6,4):C.UVGC_255_163,(0,6,6):C.UVGC_255_164,(0,6,7):C.UVGC_255_165,(0,6,8):C.UVGC_255_166,(0,6,9):C.UVGC_255_167,(0,6,10):C.UVGC_255_168,(0,6,11):C.UVGC_255_169,(0,7,0):C.UVGC_258_180,(0,7,1):C.UVGC_258_181,(0,7,2):C.UVGC_260_198,(0,7,3):C.UVGC_260_199,(0,7,4):C.UVGC_259_192,(0,7,6):C.UVGC_259_193,(0,7,7):C.UVGC_259_194,(0,7,8):C.UVGC_259_195,(0,7,9):C.UVGC_259_196,(0,7,10):C.UVGC_259_197,(0,7,11):C.UVGC_258_189,(0,2,2):C.UVGC_128_5,(0,2,3):C.UVGC_128_6,(0,4,2):C.UVGC_134_15,(0,4,3):C.UVGC_134_16,(0,4,5):C.UVGC_134_17})

V_114 = CTVertex(name = 'V_114',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g, P.g ],
                 color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                 loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.lqsb], [P.lqsc], [P.lqsd], [P.lqss], [P.lqst], [P.lqsu], [P.s], [P.t], [P.u] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsb] ], [ [P.lqsb], [P.lqsc], [P.lqsd], [P.lqss], [P.lqst], [P.lqsu] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,4):C.UVGC_130_10,(2,0,5):C.UVGC_130_9,(0,0,4):C.UVGC_130_10,(0,0,5):C.UVGC_130_9,(4,0,4):C.UVGC_129_7,(4,0,5):C.UVGC_129_8,(3,0,4):C.UVGC_129_7,(3,0,5):C.UVGC_129_8,(8,0,4):C.UVGC_130_9,(8,0,5):C.UVGC_130_10,(7,0,0):C.UVGC_264_229,(7,0,3):C.UVGC_151_57,(7,0,4):C.UVGC_264_230,(7,0,5):C.UVGC_264_231,(7,0,6):C.UVGC_264_232,(7,0,8):C.UVGC_264_233,(7,0,9):C.UVGC_264_234,(7,0,10):C.UVGC_264_235,(7,0,11):C.UVGC_264_236,(7,0,12):C.UVGC_264_237,(7,0,13):C.UVGC_264_238,(6,0,0):C.UVGC_264_229,(6,0,3):C.UVGC_151_57,(6,0,4):C.UVGC_265_239,(6,0,5):C.UVGC_265_240,(6,0,6):C.UVGC_264_232,(6,0,8):C.UVGC_264_233,(6,0,9):C.UVGC_264_234,(6,0,10):C.UVGC_264_235,(6,0,11):C.UVGC_264_236,(6,0,12):C.UVGC_264_237,(6,0,13):C.UVGC_264_238,(5,0,4):C.UVGC_129_7,(5,0,5):C.UVGC_129_8,(1,0,4):C.UVGC_129_7,(1,0,5):C.UVGC_129_8,(11,0,4):C.UVGC_133_13,(11,0,5):C.UVGC_133_14,(10,0,4):C.UVGC_133_13,(10,0,5):C.UVGC_133_14,(9,0,4):C.UVGC_132_11,(9,0,5):C.UVGC_132_12,(2,1,4):C.UVGC_130_10,(2,1,5):C.UVGC_130_9,(0,1,4):C.UVGC_130_10,(0,1,5):C.UVGC_130_9,(6,1,0):C.UVGC_261_200,(6,1,4):C.UVGC_262_210,(6,1,5):C.UVGC_262_211,(6,1,6):C.UVGC_262_212,(6,1,8):C.UVGC_262_213,(6,1,9):C.UVGC_262_214,(6,1,10):C.UVGC_262_215,(6,1,11):C.UVGC_262_216,(6,1,12):C.UVGC_262_217,(6,1,13):C.UVGC_261_209,(4,1,4):C.UVGC_129_7,(4,1,5):C.UVGC_129_8,(3,1,4):C.UVGC_129_7,(3,1,5):C.UVGC_129_8,(8,1,0):C.UVGC_266_241,(8,1,3):C.UVGC_266_242,(8,1,4):C.UVGC_266_243,(8,1,5):C.UVGC_266_244,(8,1,6):C.UVGC_266_245,(8,1,8):C.UVGC_266_246,(8,1,9):C.UVGC_266_247,(8,1,10):C.UVGC_266_248,(8,1,11):C.UVGC_266_249,(8,1,12):C.UVGC_266_250,(8,1,13):C.UVGC_266_251,(7,1,1):C.UVGC_135_18,(7,1,4):C.UVGC_136_21,(7,1,5):C.UVGC_136_22,(5,1,4):C.UVGC_129_7,(5,1,5):C.UVGC_129_8,(1,1,4):C.UVGC_129_7,(1,1,5):C.UVGC_129_8,(11,1,4):C.UVGC_133_13,(11,1,5):C.UVGC_133_14,(10,1,4):C.UVGC_133_13,(10,1,5):C.UVGC_133_14,(9,1,4):C.UVGC_132_11,(9,1,5):C.UVGC_132_12,(2,2,4):C.UVGC_130_10,(2,2,5):C.UVGC_130_9,(0,2,4):C.UVGC_130_10,(0,2,5):C.UVGC_130_9,(4,2,4):C.UVGC_129_7,(4,2,5):C.UVGC_129_8,(3,2,4):C.UVGC_129_7,(3,2,5):C.UVGC_129_8,(8,2,0):C.UVGC_263_218,(8,2,3):C.UVGC_263_219,(8,2,4):C.UVGC_263_220,(8,2,5):C.UVGC_263_221,(8,2,6):C.UVGC_263_222,(8,2,8):C.UVGC_263_223,(8,2,9):C.UVGC_263_224,(8,2,10):C.UVGC_263_225,(8,2,11):C.UVGC_263_226,(8,2,12):C.UVGC_263_227,(8,2,13):C.UVGC_263_228,(6,2,2):C.UVGC_135_18,(6,2,4):C.UVGC_135_19,(6,2,5):C.UVGC_132_11,(6,2,7):C.UVGC_135_20,(7,2,0):C.UVGC_261_200,(7,2,4):C.UVGC_261_201,(7,2,5):C.UVGC_261_202,(7,2,6):C.UVGC_261_203,(7,2,8):C.UVGC_261_204,(7,2,9):C.UVGC_261_205,(7,2,10):C.UVGC_261_206,(7,2,11):C.UVGC_261_207,(7,2,12):C.UVGC_261_208,(7,2,13):C.UVGC_261_209,(5,2,4):C.UVGC_129_7,(5,2,5):C.UVGC_129_8,(1,2,4):C.UVGC_129_7,(1,2,5):C.UVGC_129_8,(11,2,4):C.UVGC_133_13,(11,2,5):C.UVGC_133_14,(10,2,4):C.UVGC_133_13,(10,2,5):C.UVGC_133_14,(9,2,4):C.UVGC_132_11,(9,2,5):C.UVGC_132_12})

V_115 = CTVertex(name = 'V_115',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.t, P.lqsb__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsb] ], [ [P.g, P.lqsb, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_232_116,(0,0,2):C.UVGC_270_255,(0,0,1):C.UVGC_232_117})

V_116 = CTVertex(name = 'V_116',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.b__tilde__, P.lqsb ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.lqsb] ], [ [P.g, P.lqsb] ] ],
                 couplings = {(0,0,1):C.UVGC_191_88,(0,0,0):C.UVGC_191_89})

V_117 = CTVertex(name = 'V_117',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.c, P.lqsc__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsc] ], [ [P.g, P.lqsc] ] ],
                 couplings = {(0,0,0):C.UVGC_192_90,(0,0,2):C.UVGC_192_91,(0,0,1):C.UVGC_192_92})

V_118 = CTVertex(name = 'V_118',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.s, P.lqsc__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsc] ], [ [P.g, P.lqsc, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_229_110,(0,0,2):C.UVGC_229_111,(0,0,1):C.UVGC_229_112})

V_119 = CTVertex(name = 'V_119',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.d__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,1):C.UVGC_196_98,(0,0,0):C.UVGC_196_99})

V_120 = CTVertex(name = 'V_120',
                 type = 'UV',
                 particles = [ P.mu__minus__, P.c, P.lqss__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqss] ], [ [P.g, P.lqss] ] ],
                 couplings = {(0,0,0):C.UVGC_194_95,(0,0,2):C.UVGC_193_93,(0,0,1):C.UVGC_193_94})

V_121 = CTVertex(name = 'V_121',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.s__tilde__, P.lqss ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqss] ], [ [P.g, P.lqss, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_230_113,(0,0,1):C.UVGC_230_114})

V_122 = CTVertex(name = 'V_122',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.b, P.lqst__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqst] ], [ [P.g, P.lqst] ] ],
                 couplings = {(0,0,0):C.UVGC_241_128,(0,0,2):C.UVGC_241_129,(0,0,1):C.UVGC_241_130})

V_123 = CTVertex(name = 'V_123',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.t, P.lqst__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqst] ], [ [P.g, P.lqst, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_271_256,(0,0,2):C.UVGC_271_257,(0,0,1):C.UVGC_271_258})

V_124 = CTVertex(name = 'V_124',
                 type = 'UV',
                 particles = [ P.e__plus__, P.d, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_198_101,(0,0,2):C.UVGC_198_102,(0,0,1):C.UVGC_198_103})

V_125 = CTVertex(name = 'V_125',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.u, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_236_121,(0,0,2):C.UVGC_236_122,(0,0,1):C.UVGC_236_123})

V_126 = CTVertex(name = 'V_126',
                 type = 'UV',
                 particles = [ P.u, P.e__minus__, P.lqsd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_234_118,(0,0,1):C.UVGC_234_119})

V_127 = CTVertex(name = 'V_127',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_278_268,(0,0,2):C.UVGC_278_269,(0,0,1):C.UVGC_278_270,(0,1,0):C.UVGC_279_271,(0,1,2):C.UVGC_279_272,(0,1,1):C.UVGC_279_273})

V_128 = CTVertex(name = 'V_128',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.G0 ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_246_135})

V_129 = CTVertex(name = 'V_129',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_245_134})

V_130 = CTVertex(name = 'V_130',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.G0 ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_281_277})

V_131 = CTVertex(name = 'V_131',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_282_278})

V_132 = CTVertex(name = 'V_132',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_280_274,(0,0,2):C.UVGC_280_275,(0,0,1):C.UVGC_280_276,(0,1,0):C.UVGC_277_265,(0,1,2):C.UVGC_277_266,(0,1,1):C.UVGC_277_267})

V_133 = CTVertex(name = 'V_133',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.t__tilde__, P.lqsb ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsb] ], [ [P.g, P.lqsb, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_232_116,(0,0,1):C.UVGC_232_117})

V_134 = CTVertex(name = 'V_134',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.mu__minus__, P.lqsc ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsc] ], [ [P.g, P.lqsc, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_229_110,(0,0,2):C.UVGC_229_111,(0,0,1):C.UVGC_229_112})

V_135 = CTVertex(name = 'V_135',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.e__plus__, P.lqsd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_234_118,(0,0,2):C.UVGC_235_120,(0,0,1):C.UVGC_234_119})

V_136 = CTVertex(name = 'V_136',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.c__tilde__, P.lqss ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g, P.lqss] ], [ [P.g, P.lqss] ] ],
                 couplings = {(0,0,1):C.UVGC_193_93,(0,0,0):C.UVGC_193_94})

V_137 = CTVertex(name = 'V_137',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.ta__minus__, P.lqst ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqst] ], [ [P.g, P.lqst] ] ],
                 couplings = {(0,0,0):C.UVGC_241_128,(0,0,2):C.UVGC_241_129,(0,0,1):C.UVGC_241_130})

V_138 = CTVertex(name = 'V_138',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.e__minus__, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_198_101,(0,0,2):C.UVGC_198_102,(0,0,1):C.UVGC_198_103})

V_139 = CTVertex(name = 'V_139',
                 type = 'UV',
                 particles = [ P.vt, P.b, P.lqsb__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqsb] ], [ [P.g, P.lqsb] ] ],
                 couplings = {(0,0,0):C.UVGC_240_127,(0,0,2):C.UVGC_191_88,(0,0,1):C.UVGC_191_89})

V_140 = CTVertex(name = 'V_140',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.vm, P.lqsc ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsc] ], [ [P.g, P.lqsc] ] ],
                 couplings = {(0,0,0):C.UVGC_192_90,(0,0,2):C.UVGC_192_91,(0,0,1):C.UVGC_192_92})

V_141 = CTVertex(name = 'V_141',
                 type = 'UV',
                 particles = [ P.ve, P.d, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_197_100,(0,0,2):C.UVGC_196_98,(0,0,1):C.UVGC_196_99})

V_142 = CTVertex(name = 'V_142',
                 type = 'UV',
                 particles = [ P.vm, P.s, P.lqss__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqss] ], [ [P.g, P.lqss, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_230_113,(0,0,2):C.UVGC_231_115,(0,0,1):C.UVGC_230_114})

V_143 = CTVertex(name = 'V_143',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.vt, P.lqst ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqst] ], [ [P.g, P.lqst, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_271_256,(0,0,2):C.UVGC_271_257,(0,0,1):C.UVGC_271_258})

V_144 = CTVertex(name = 'V_144',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.ve, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_236_121,(0,0,2):C.UVGC_236_122,(0,0,1):C.UVGC_236_123})

V_145 = CTVertex(name = 'V_145',
                 type = 'UV',
                 particles = [ P.g, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsu] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_150_44,(0,0,1):C.UVGC_150_45,(0,0,2):C.UVGC_150_46,(0,0,3):C.UVGC_150_47,(0,0,5):C.UVGC_150_48,(0,0,6):C.UVGC_150_49,(0,0,7):C.UVGC_150_50,(0,0,8):C.UVGC_150_51,(0,0,9):C.UVGC_150_52,(0,0,10):C.UVGC_150_53,(0,0,11):C.UVGC_150_54,(0,0,4):C.UVGC_180_86,(0,1,0):C.UVGC_138_24,(0,1,1):C.UVGC_138_25,(0,1,2):C.UVGC_138_26,(0,1,3):C.UVGC_138_27,(0,1,5):C.UVGC_138_28,(0,1,6):C.UVGC_138_29,(0,1,7):C.UVGC_138_30,(0,1,8):C.UVGC_138_31,(0,1,9):C.UVGC_138_32,(0,1,10):C.UVGC_138_33,(0,1,11):C.UVGC_138_34,(0,1,4):C.UVGC_179_85})

V_146 = CTVertex(name = 'V_146',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsu] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_151_56,(2,0,1):C.UVGC_151_57,(2,0,2):C.UVGC_151_58,(2,0,3):C.UVGC_151_59,(2,0,5):C.UVGC_151_60,(2,0,6):C.UVGC_151_61,(2,0,7):C.UVGC_151_62,(2,0,8):C.UVGC_151_63,(2,0,9):C.UVGC_151_64,(2,0,10):C.UVGC_151_65,(2,0,11):C.UVGC_151_66,(2,0,4):C.UVGC_181_87,(1,0,0):C.UVGC_151_56,(1,0,1):C.UVGC_151_57,(1,0,2):C.UVGC_151_58,(1,0,3):C.UVGC_151_59,(1,0,5):C.UVGC_151_60,(1,0,6):C.UVGC_151_61,(1,0,7):C.UVGC_151_62,(1,0,8):C.UVGC_151_63,(1,0,9):C.UVGC_151_64,(1,0,10):C.UVGC_151_65,(1,0,11):C.UVGC_151_66,(1,0,4):C.UVGC_181_87,(0,0,2):C.UVGC_147_40,(0,0,4):C.UVGC_147_41})

V_147 = CTVertex(name = 'V_147',
                 type = 'UV',
                 particles = [ P.g, P.lqsd__tilde__, P.lqsd ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsd] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_150_44,(0,0,1):C.UVGC_150_45,(0,0,2):C.UVGC_150_46,(0,0,3):C.UVGC_150_47,(0,0,5):C.UVGC_150_48,(0,0,6):C.UVGC_150_49,(0,0,7):C.UVGC_150_50,(0,0,8):C.UVGC_150_51,(0,0,9):C.UVGC_150_52,(0,0,10):C.UVGC_150_53,(0,0,11):C.UVGC_150_54,(0,0,4):C.UVGC_162_74,(0,1,0):C.UVGC_138_24,(0,1,1):C.UVGC_138_25,(0,1,2):C.UVGC_138_26,(0,1,3):C.UVGC_138_27,(0,1,5):C.UVGC_138_28,(0,1,6):C.UVGC_138_29,(0,1,7):C.UVGC_138_30,(0,1,8):C.UVGC_138_31,(0,1,9):C.UVGC_138_32,(0,1,10):C.UVGC_138_33,(0,1,11):C.UVGC_138_34,(0,1,4):C.UVGC_161_73})

V_148 = CTVertex(name = 'V_148',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsd] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_151_56,(2,0,1):C.UVGC_151_57,(2,0,2):C.UVGC_151_58,(2,0,3):C.UVGC_151_59,(2,0,5):C.UVGC_151_60,(2,0,6):C.UVGC_151_61,(2,0,7):C.UVGC_151_62,(2,0,8):C.UVGC_151_63,(2,0,9):C.UVGC_151_64,(2,0,10):C.UVGC_151_65,(2,0,11):C.UVGC_151_66,(2,0,4):C.UVGC_163_75,(1,0,0):C.UVGC_151_56,(1,0,1):C.UVGC_151_57,(1,0,2):C.UVGC_151_58,(1,0,3):C.UVGC_151_59,(1,0,5):C.UVGC_151_60,(1,0,6):C.UVGC_151_61,(1,0,7):C.UVGC_151_62,(1,0,8):C.UVGC_151_63,(1,0,9):C.UVGC_151_64,(1,0,10):C.UVGC_151_65,(1,0,11):C.UVGC_151_66,(1,0,4):C.UVGC_163_75,(0,0,2):C.UVGC_147_40,(0,0,4):C.UVGC_147_41})

V_149 = CTVertex(name = 'V_149',
                 type = 'UV',
                 particles = [ P.g, P.lqsc__tilde__, P.lqsc ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsc] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_150_44,(0,0,1):C.UVGC_150_45,(0,0,2):C.UVGC_150_46,(0,0,3):C.UVGC_150_47,(0,0,5):C.UVGC_150_48,(0,0,6):C.UVGC_150_49,(0,0,7):C.UVGC_150_50,(0,0,8):C.UVGC_150_51,(0,0,9):C.UVGC_150_52,(0,0,10):C.UVGC_150_53,(0,0,11):C.UVGC_150_54,(0,0,4):C.UVGC_156_70,(0,1,0):C.UVGC_138_24,(0,1,1):C.UVGC_138_25,(0,1,2):C.UVGC_138_26,(0,1,3):C.UVGC_138_27,(0,1,5):C.UVGC_138_28,(0,1,6):C.UVGC_138_29,(0,1,7):C.UVGC_138_30,(0,1,8):C.UVGC_138_31,(0,1,9):C.UVGC_138_32,(0,1,10):C.UVGC_138_33,(0,1,11):C.UVGC_138_34,(0,1,4):C.UVGC_155_69})

V_150 = CTVertex(name = 'V_150',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqsc__tilde__, P.lqsc ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsc] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_151_56,(2,0,1):C.UVGC_151_57,(2,0,2):C.UVGC_151_58,(2,0,3):C.UVGC_151_59,(2,0,5):C.UVGC_151_60,(2,0,6):C.UVGC_151_61,(2,0,7):C.UVGC_151_62,(2,0,8):C.UVGC_151_63,(2,0,9):C.UVGC_151_64,(2,0,10):C.UVGC_151_65,(2,0,11):C.UVGC_151_66,(2,0,4):C.UVGC_157_71,(1,0,0):C.UVGC_151_56,(1,0,1):C.UVGC_151_57,(1,0,2):C.UVGC_151_58,(1,0,3):C.UVGC_151_59,(1,0,5):C.UVGC_151_60,(1,0,6):C.UVGC_151_61,(1,0,7):C.UVGC_151_62,(1,0,8):C.UVGC_151_63,(1,0,9):C.UVGC_151_64,(1,0,10):C.UVGC_151_65,(1,0,11):C.UVGC_151_66,(1,0,4):C.UVGC_157_71,(0,0,2):C.UVGC_147_40,(0,0,4):C.UVGC_147_41})

V_151 = CTVertex(name = 'V_151',
                 type = 'UV',
                 particles = [ P.g, P.lqss__tilde__, P.lqss ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqss] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_150_44,(0,0,1):C.UVGC_150_45,(0,0,2):C.UVGC_150_46,(0,0,3):C.UVGC_150_47,(0,0,5):C.UVGC_150_48,(0,0,6):C.UVGC_150_49,(0,0,7):C.UVGC_150_50,(0,0,8):C.UVGC_150_51,(0,0,9):C.UVGC_150_52,(0,0,10):C.UVGC_150_53,(0,0,11):C.UVGC_150_54,(0,0,4):C.UVGC_168_78,(0,1,0):C.UVGC_138_24,(0,1,1):C.UVGC_138_25,(0,1,2):C.UVGC_138_26,(0,1,3):C.UVGC_138_27,(0,1,5):C.UVGC_138_28,(0,1,6):C.UVGC_138_29,(0,1,7):C.UVGC_138_30,(0,1,8):C.UVGC_138_31,(0,1,9):C.UVGC_138_32,(0,1,10):C.UVGC_138_33,(0,1,11):C.UVGC_138_34,(0,1,4):C.UVGC_167_77})

V_152 = CTVertex(name = 'V_152',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqss] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_151_56,(2,0,1):C.UVGC_151_57,(2,0,2):C.UVGC_151_58,(2,0,3):C.UVGC_151_59,(2,0,5):C.UVGC_151_60,(2,0,6):C.UVGC_151_61,(2,0,7):C.UVGC_151_62,(2,0,8):C.UVGC_151_63,(2,0,9):C.UVGC_151_64,(2,0,10):C.UVGC_151_65,(2,0,11):C.UVGC_151_66,(2,0,4):C.UVGC_169_79,(1,0,0):C.UVGC_151_56,(1,0,1):C.UVGC_151_57,(1,0,2):C.UVGC_151_58,(1,0,3):C.UVGC_151_59,(1,0,5):C.UVGC_151_60,(1,0,6):C.UVGC_151_61,(1,0,7):C.UVGC_151_62,(1,0,8):C.UVGC_151_63,(1,0,9):C.UVGC_151_64,(1,0,10):C.UVGC_151_65,(1,0,11):C.UVGC_151_66,(1,0,4):C.UVGC_169_79,(0,0,2):C.UVGC_147_40,(0,0,4):C.UVGC_147_41})

V_153 = CTVertex(name = 'V_153',
                 type = 'UV',
                 particles = [ P.g, P.lqst__tilde__, P.lqst ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqst] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_150_44,(0,0,1):C.UVGC_150_45,(0,0,2):C.UVGC_150_46,(0,0,3):C.UVGC_150_47,(0,0,5):C.UVGC_150_48,(0,0,6):C.UVGC_150_49,(0,0,7):C.UVGC_150_50,(0,0,8):C.UVGC_150_51,(0,0,9):C.UVGC_150_52,(0,0,10):C.UVGC_150_53,(0,0,11):C.UVGC_150_54,(0,0,4):C.UVGC_174_82,(0,1,0):C.UVGC_138_24,(0,1,1):C.UVGC_138_25,(0,1,2):C.UVGC_138_26,(0,1,3):C.UVGC_138_27,(0,1,5):C.UVGC_138_28,(0,1,6):C.UVGC_138_29,(0,1,7):C.UVGC_138_30,(0,1,8):C.UVGC_138_31,(0,1,9):C.UVGC_138_32,(0,1,10):C.UVGC_138_33,(0,1,11):C.UVGC_138_34,(0,1,4):C.UVGC_173_81})

V_154 = CTVertex(name = 'V_154',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqst] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_151_56,(2,0,1):C.UVGC_151_57,(2,0,2):C.UVGC_151_58,(2,0,3):C.UVGC_151_59,(2,0,5):C.UVGC_151_60,(2,0,6):C.UVGC_151_61,(2,0,7):C.UVGC_151_62,(2,0,8):C.UVGC_151_63,(2,0,9):C.UVGC_151_64,(2,0,10):C.UVGC_151_65,(2,0,11):C.UVGC_151_66,(2,0,4):C.UVGC_175_83,(1,0,0):C.UVGC_151_56,(1,0,1):C.UVGC_151_57,(1,0,2):C.UVGC_151_58,(1,0,3):C.UVGC_151_59,(1,0,5):C.UVGC_151_60,(1,0,6):C.UVGC_151_61,(1,0,7):C.UVGC_151_62,(1,0,8):C.UVGC_151_63,(1,0,9):C.UVGC_151_64,(1,0,10):C.UVGC_151_65,(1,0,11):C.UVGC_151_66,(1,0,4):C.UVGC_175_83,(0,0,2):C.UVGC_147_40,(0,0,4):C.UVGC_147_41})

V_155 = CTVertex(name = 'V_155',
                 type = 'UV',
                 particles = [ P.g, P.lqsb__tilde__, P.lqsb ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsb] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_150_44,(0,0,1):C.UVGC_150_45,(0,0,2):C.UVGC_150_46,(0,0,3):C.UVGC_150_47,(0,0,5):C.UVGC_150_48,(0,0,6):C.UVGC_150_49,(0,0,7):C.UVGC_150_50,(0,0,8):C.UVGC_150_51,(0,0,9):C.UVGC_150_52,(0,0,10):C.UVGC_150_53,(0,0,11):C.UVGC_150_54,(0,0,4):C.UVGC_150_55,(0,1,0):C.UVGC_138_24,(0,1,1):C.UVGC_138_25,(0,1,2):C.UVGC_138_26,(0,1,3):C.UVGC_138_27,(0,1,5):C.UVGC_138_28,(0,1,6):C.UVGC_138_29,(0,1,7):C.UVGC_138_30,(0,1,8):C.UVGC_138_31,(0,1,9):C.UVGC_138_32,(0,1,10):C.UVGC_138_33,(0,1,11):C.UVGC_138_34,(0,1,4):C.UVGC_149_43})

V_156 = CTVertex(name = 'V_156',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqsb__tilde__, P.lqsb ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsb] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_151_56,(2,0,1):C.UVGC_151_57,(2,0,2):C.UVGC_151_58,(2,0,3):C.UVGC_151_59,(2,0,5):C.UVGC_151_60,(2,0,6):C.UVGC_151_61,(2,0,7):C.UVGC_151_62,(2,0,8):C.UVGC_151_63,(2,0,9):C.UVGC_151_64,(2,0,10):C.UVGC_151_65,(2,0,11):C.UVGC_151_66,(2,0,4):C.UVGC_151_67,(1,0,0):C.UVGC_151_56,(1,0,1):C.UVGC_151_57,(1,0,2):C.UVGC_151_58,(1,0,3):C.UVGC_151_59,(1,0,5):C.UVGC_151_60,(1,0,6):C.UVGC_151_61,(1,0,7):C.UVGC_151_62,(1,0,8):C.UVGC_151_63,(1,0,9):C.UVGC_151_64,(1,0,10):C.UVGC_151_65,(1,0,11):C.UVGC_151_66,(1,0,4):C.UVGC_151_67,(0,0,2):C.UVGC_147_40,(0,0,4):C.UVGC_147_41})

V_157 = CTVertex(name = 'V_157',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_140_37,(0,1,0):C.UVGC_119_2,(0,2,0):C.UVGC_119_2})

V_158 = CTVertex(name = 'V_158',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_140_37,(0,1,0):C.UVGC_119_2,(0,2,0):C.UVGC_119_2})

V_159 = CTVertex(name = 'V_159',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_140_37,(0,1,0):C.UVGC_268_253,(0,2,0):C.UVGC_268_253})

V_160 = CTVertex(name = 'V_160',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,4):C.UVGC_138_35,(0,1,0):C.UVGC_138_24,(0,1,1):C.UVGC_138_25,(0,1,2):C.UVGC_138_26,(0,1,3):C.UVGC_138_27,(0,1,5):C.UVGC_138_28,(0,1,6):C.UVGC_138_29,(0,1,7):C.UVGC_138_30,(0,1,8):C.UVGC_138_31,(0,1,9):C.UVGC_138_32,(0,1,10):C.UVGC_138_33,(0,1,11):C.UVGC_138_34,(0,1,4):C.UVGC_122_4,(0,2,0):C.UVGC_138_24,(0,2,1):C.UVGC_138_25,(0,2,2):C.UVGC_138_26,(0,2,3):C.UVGC_138_27,(0,2,5):C.UVGC_138_28,(0,2,6):C.UVGC_138_29,(0,2,7):C.UVGC_138_30,(0,2,8):C.UVGC_138_31,(0,2,9):C.UVGC_138_32,(0,2,10):C.UVGC_138_33,(0,2,11):C.UVGC_138_34,(0,2,4):C.UVGC_122_4})

V_161 = CTVertex(name = 'V_161',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,2):C.UVGC_138_35,(0,1,0):C.UVGC_138_24,(0,1,1):C.UVGC_138_25,(0,1,3):C.UVGC_138_26,(0,1,4):C.UVGC_138_27,(0,1,5):C.UVGC_138_28,(0,1,6):C.UVGC_138_29,(0,1,7):C.UVGC_138_30,(0,1,8):C.UVGC_138_31,(0,1,9):C.UVGC_138_32,(0,1,10):C.UVGC_138_33,(0,1,11):C.UVGC_138_34,(0,1,2):C.UVGC_122_4,(0,2,0):C.UVGC_138_24,(0,2,1):C.UVGC_138_25,(0,2,3):C.UVGC_138_26,(0,2,4):C.UVGC_138_27,(0,2,5):C.UVGC_138_28,(0,2,6):C.UVGC_138_29,(0,2,7):C.UVGC_138_30,(0,2,8):C.UVGC_138_31,(0,2,9):C.UVGC_138_32,(0,2,10):C.UVGC_138_33,(0,2,11):C.UVGC_138_34,(0,2,2):C.UVGC_122_4})

V_162 = CTVertex(name = 'V_162',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,4):C.UVGC_138_35,(0,1,0):C.UVGC_138_24,(0,1,1):C.UVGC_138_25,(0,1,2):C.UVGC_138_26,(0,1,3):C.UVGC_138_27,(0,1,5):C.UVGC_138_28,(0,1,6):C.UVGC_138_29,(0,1,7):C.UVGC_138_30,(0,1,8):C.UVGC_138_31,(0,1,9):C.UVGC_138_32,(0,1,10):C.UVGC_138_33,(0,1,11):C.UVGC_138_34,(0,1,4):C.UVGC_269_254,(0,2,0):C.UVGC_138_24,(0,2,1):C.UVGC_138_25,(0,2,2):C.UVGC_138_26,(0,2,3):C.UVGC_138_27,(0,2,5):C.UVGC_138_28,(0,2,6):C.UVGC_138_29,(0,2,7):C.UVGC_138_30,(0,2,8):C.UVGC_138_31,(0,2,9):C.UVGC_138_32,(0,2,10):C.UVGC_138_33,(0,2,11):C.UVGC_138_34,(0,2,4):C.UVGC_269_254})

V_163 = CTVertex(name = 'V_163',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_195_96,(0,0,1):C.UVGC_195_97})

V_164 = CTVertex(name = 'V_164',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_195_96,(0,0,1):C.UVGC_195_97})

V_165 = CTVertex(name = 'V_165',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_274_261,(0,0,2):C.UVGC_274_262,(0,0,1):C.UVGC_195_97})

V_166 = CTVertex(name = 'V_166',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_275_263,(0,1,0):C.UVGC_276_264})

V_167 = CTVertex(name = 'V_167',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_137_23,(0,1,0):C.UVGC_121_3,(0,2,0):C.UVGC_121_3})

V_168 = CTVertex(name = 'V_168',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_137_23,(0,1,0):C.UVGC_121_3,(0,2,0):C.UVGC_121_3})

V_169 = CTVertex(name = 'V_169',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_137_23,(0,1,0):C.UVGC_238_125,(0,2,0):C.UVGC_238_125})

V_170 = CTVertex(name = 'V_170',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_138_24,(0,0,1):C.UVGC_138_25,(0,0,3):C.UVGC_138_26,(0,0,4):C.UVGC_138_27,(0,0,5):C.UVGC_138_28,(0,0,6):C.UVGC_138_29,(0,0,7):C.UVGC_138_30,(0,0,8):C.UVGC_138_31,(0,0,9):C.UVGC_138_32,(0,0,10):C.UVGC_138_33,(0,0,11):C.UVGC_138_34,(0,0,2):C.UVGC_138_35,(0,1,2):C.UVGC_122_4,(0,2,2):C.UVGC_122_4})

V_171 = CTVertex(name = 'V_171',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_138_24,(0,0,1):C.UVGC_138_25,(0,0,2):C.UVGC_138_26,(0,0,3):C.UVGC_138_27,(0,0,5):C.UVGC_138_28,(0,0,6):C.UVGC_138_29,(0,0,7):C.UVGC_138_30,(0,0,8):C.UVGC_138_31,(0,0,9):C.UVGC_138_32,(0,0,10):C.UVGC_138_33,(0,0,11):C.UVGC_138_34,(0,0,4):C.UVGC_138_35,(0,1,4):C.UVGC_122_4,(0,2,4):C.UVGC_122_4})

V_172 = CTVertex(name = 'V_172',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.g] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_138_24,(0,0,2):C.UVGC_138_25,(0,0,3):C.UVGC_138_26,(0,0,4):C.UVGC_138_27,(0,0,5):C.UVGC_138_28,(0,0,6):C.UVGC_138_29,(0,0,7):C.UVGC_138_30,(0,0,8):C.UVGC_138_31,(0,0,9):C.UVGC_138_32,(0,0,10):C.UVGC_138_33,(0,0,11):C.UVGC_138_34,(0,0,1):C.UVGC_138_35,(0,1,1):C.UVGC_239_126,(0,2,1):C.UVGC_239_126})

V_173 = CTVertex(name = 'V_173',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_195_96,(0,0,1):C.UVGC_195_97})

V_174 = CTVertex(name = 'V_174',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_195_96,(0,0,1):C.UVGC_195_97})

V_175 = CTVertex(name = 'V_175',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_274_261,(0,0,2):C.UVGC_274_262,(0,0,1):C.UVGC_195_97})

V_176 = CTVertex(name = 'V_176',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_243_132,(0,1,0):C.UVGC_244_133})

V_177 = CTVertex(name = 'V_177',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF3, L.FF4, L.FF5, L.FF6 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_242_131,(0,2,0):C.UVGC_242_131,(0,1,0):C.UVGC_237_124,(0,3,0):C.UVGC_237_124})

V_178 = CTVertex(name = 'V_178',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF4, L.FF6 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_139_36,(0,1,0):C.UVGC_118_1,(0,2,0):C.UVGC_118_1})

V_179 = CTVertex(name = 'V_179',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF4, L.FF6 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_139_36,(0,1,0):C.UVGC_118_1,(0,2,0):C.UVGC_118_1})

V_180 = CTVertex(name = 'V_180',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF4, L.FF6 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_139_36,(0,1,0):C.UVGC_118_1,(0,2,0):C.UVGC_118_1})

V_181 = CTVertex(name = 'V_181',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5, L.FF6 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_273_260,(0,3,0):C.UVGC_273_260,(0,2,0):C.UVGC_267_252,(0,4,0):C.UVGC_267_252,(0,0,0):C.UVGC_272_259})

V_182 = CTVertex(name = 'V_182',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF4, L.FF6 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_139_36,(0,1,0):C.UVGC_118_1,(0,2,0):C.UVGC_118_1})

V_183 = CTVertex(name = 'V_183',
                 type = 'UV',
                 particles = [ P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_252_141,(0,1,0):C.UVGC_178_84})

V_184 = CTVertex(name = 'V_184',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_249_138,(0,1,0):C.UVGC_160_72})

V_185 = CTVertex(name = 'V_185',
                 type = 'UV',
                 particles = [ P.lqsc__tilde__, P.lqsc ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqsc] ] ],
                 couplings = {(0,0,0):C.UVGC_248_137,(0,1,0):C.UVGC_154_68})

V_186 = CTVertex(name = 'V_186',
                 type = 'UV',
                 particles = [ P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqss] ] ],
                 couplings = {(0,0,0):C.UVGC_250_139,(0,1,0):C.UVGC_166_76})

V_187 = CTVertex(name = 'V_187',
                 type = 'UV',
                 particles = [ P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqst] ] ],
                 couplings = {(0,0,0):C.UVGC_251_140,(0,1,0):C.UVGC_172_80})

V_188 = CTVertex(name = 'V_188',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqsb] ] ],
                 couplings = {(0,0,0):C.UVGC_247_136,(0,1,0):C.UVGC_148_42})

V_189 = CTVertex(name = 'V_189',
                 type = 'UV',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV1, L.VV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_254_150,(0,0,1):C.UVGC_254_151,(0,0,2):C.UVGC_254_152,(0,0,3):C.UVGC_254_153,(0,0,4):C.UVGC_254_154,(0,0,5):C.UVGC_254_155,(0,0,6):C.UVGC_254_156,(0,0,7):C.UVGC_254_157,(0,0,8):C.UVGC_254_158,(0,0,9):C.UVGC_254_159,(0,1,0):C.UVGC_253_142,(0,1,3):C.UVGC_253_143,(0,1,4):C.UVGC_253_144,(0,1,5):C.UVGC_253_145,(0,1,6):C.UVGC_253_146,(0,1,7):C.UVGC_253_147,(0,1,8):C.UVGC_253_148,(0,1,9):C.UVGC_253_149})

V_190 = CTVertex(name = 'V_190',
                 type = 'UV',
                 particles = [ P.lqsu__tilde__, P.lqsu__tilde__, P.lqsu, P.lqsu ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_146_38,(1,0,1):C.UVGC_146_39,(0,0,0):C.UVGC_146_38,(0,0,1):C.UVGC_146_39})

V_191 = CTVertex(name = 'V_191',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqsu] ], [ [P.g, P.lqsd, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_192 = CTVertex(name = 'V_192',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd__tilde__, P.lqsd, P.lqsd ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(1,0,0):C.UVGC_146_38,(1,0,1):C.UVGC_146_39,(0,0,0):C.UVGC_146_38,(0,0,1):C.UVGC_146_39})

V_193 = CTVertex(name = 'V_193',
                 type = 'UV',
                 particles = [ P.lqsc__tilde__, P.lqsc, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqsu] ], [ [P.g, P.lqsc, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_194 = CTVertex(name = 'V_194',
                 type = 'UV',
                 particles = [ P.lqsc__tilde__, P.lqsc, P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqsd] ], [ [P.g, P.lqsc, P.lqsd] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_195 = CTVertex(name = 'V_195',
                 type = 'UV',
                 particles = [ P.lqsc__tilde__, P.lqsc__tilde__, P.lqsc, P.lqsc ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc] ] ],
                 couplings = {(1,0,0):C.UVGC_146_38,(1,0,1):C.UVGC_146_39,(0,0,0):C.UVGC_146_38,(0,0,1):C.UVGC_146_39})

V_196 = CTVertex(name = 'V_196',
                 type = 'UV',
                 particles = [ P.lqss__tilde__, P.lqss, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqss], [P.g, P.lqsu] ], [ [P.g, P.lqss, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_197 = CTVertex(name = 'V_197',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd, P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqss] ], [ [P.g, P.lqsd, P.lqss] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_198 = CTVertex(name = 'V_198',
                 type = 'UV',
                 particles = [ P.lqsc__tilde__, P.lqsc, P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqss] ], [ [P.g, P.lqsc, P.lqss] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_199 = CTVertex(name = 'V_199',
                 type = 'UV',
                 particles = [ P.lqss__tilde__, P.lqss__tilde__, P.lqss, P.lqss ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqss] ] ],
                 couplings = {(1,0,0):C.UVGC_146_38,(1,0,1):C.UVGC_146_39,(0,0,0):C.UVGC_146_38,(0,0,1):C.UVGC_146_39})

V_200 = CTVertex(name = 'V_200',
                 type = 'UV',
                 particles = [ P.lqst__tilde__, P.lqst, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqst], [P.g, P.lqsu] ], [ [P.g, P.lqst, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_201 = CTVertex(name = 'V_201',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqst] ], [ [P.g, P.lqsd, P.lqst] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_202 = CTVertex(name = 'V_202',
                 type = 'UV',
                 particles = [ P.lqsc__tilde__, P.lqsc, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqst] ], [ [P.g, P.lqsc, P.lqst] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_203 = CTVertex(name = 'V_203',
                 type = 'UV',
                 particles = [ P.lqss__tilde__, P.lqss, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqss], [P.g, P.lqst] ], [ [P.g, P.lqss, P.lqst] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_204 = CTVertex(name = 'V_204',
                 type = 'UV',
                 particles = [ P.lqst__tilde__, P.lqst__tilde__, P.lqst, P.lqst ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqst] ] ],
                 couplings = {(1,0,0):C.UVGC_146_38,(1,0,1):C.UVGC_146_39,(0,0,0):C.UVGC_146_38,(0,0,1):C.UVGC_146_39})

V_205 = CTVertex(name = 'V_205',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqsu] ], [ [P.g, P.lqsb, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_206 = CTVertex(name = 'V_206',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqsd] ], [ [P.g, P.lqsb, P.lqsd] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_207 = CTVertex(name = 'V_207',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqsc__tilde__, P.lqsc ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqsc] ], [ [P.g, P.lqsb, P.lqsc] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_208 = CTVertex(name = 'V_208',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqss] ], [ [P.g, P.lqsb, P.lqss] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_209 = CTVertex(name = 'V_209',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqst] ], [ [P.g, P.lqsb, P.lqst] ] ],
                 couplings = {(1,0,0):C.UVGC_200_107,(1,0,1):C.UVGC_200_108,(1,0,2):C.UVGC_200_109,(0,0,0):C.UVGC_199_104,(0,0,1):C.UVGC_199_105,(0,0,2):C.UVGC_199_106})

V_210 = CTVertex(name = 'V_210',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb__tilde__, P.lqsb, P.lqsb ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb] ] ],
                 couplings = {(1,0,0):C.UVGC_146_38,(1,0,1):C.UVGC_146_39,(0,0,0):C.UVGC_146_38,(0,0,1):C.UVGC_146_39})

