# This file was automatically created by FeynRules =.2.4
# Mathematica version: 8.0 for Linux x86 (32-bit) (October 10, 2011)
# Date: Sat 3 Sep 2016 20:49:32


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_135_1 = Coupling(name = 'R2GC_135_1',
                      value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_136_2 = Coupling(name = 'R2GC_136_2',
                      value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_139_3 = Coupling(name = 'R2GC_139_3',
                      value = '-(complex(0,1)*G**2*MB**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_139_4 = Coupling(name = 'R2GC_139_4',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_140_5 = Coupling(name = 'R2GC_140_5',
                      value = '-(complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_140_6 = Coupling(name = 'R2GC_140_6',
                      value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_141_7 = Coupling(name = 'R2GC_141_7',
                      value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_141_8 = Coupling(name = 'R2GC_141_8',
                      value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_142_9 = Coupling(name = 'R2GC_142_9',
                      value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_142_10 = Coupling(name = 'R2GC_142_10',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_143_11 = Coupling(name = 'R2GC_143_11',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_143_12 = Coupling(name = 'R2GC_143_12',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_144_13 = Coupling(name = 'R2GC_144_13',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_144_14 = Coupling(name = 'R2GC_144_14',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_145_15 = Coupling(name = 'R2GC_145_15',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_145_16 = Coupling(name = 'R2GC_145_16',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_146_17 = Coupling(name = 'R2GC_146_17',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_146_18 = Coupling(name = 'R2GC_146_18',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_147_19 = Coupling(name = 'R2GC_147_19',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_147_20 = Coupling(name = 'R2GC_147_20',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_148_21 = Coupling(name = 'R2GC_148_21',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_149_22 = Coupling(name = 'R2GC_149_22',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_151_23 = Coupling(name = 'R2GC_151_23',
                       value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_152_24 = Coupling(name = 'R2GC_152_24',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_165_25 = Coupling(name = 'R2GC_165_25',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_165_26 = Coupling(name = 'R2GC_165_26',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_166_27 = Coupling(name = 'R2GC_166_27',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_166_28 = Coupling(name = 'R2GC_166_28',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_167_29 = Coupling(name = 'R2GC_167_29',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_167_30 = Coupling(name = 'R2GC_167_30',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_168_31 = Coupling(name = 'R2GC_168_31',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_169_32 = Coupling(name = 'R2GC_169_32',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_169_33 = Coupling(name = 'R2GC_169_33',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_171_34 = Coupling(name = 'R2GC_171_34',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_171_35 = Coupling(name = 'R2GC_171_35',
                       value = '(complex(0,1)*G**4)/(4.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_172_36 = Coupling(name = 'R2GC_172_36',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_172_37 = Coupling(name = 'R2GC_172_37',
                       value = '(-23*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_173_38 = Coupling(name = 'R2GC_173_38',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_174_39 = Coupling(name = 'R2GC_174_39',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_175_40 = Coupling(name = 'R2GC_175_40',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_176_41 = Coupling(name = 'R2GC_176_41',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_182_42 = Coupling(name = 'R2GC_182_42',
                       value = '(-13*complex(0,1)*G**4)/(144.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_182_43 = Coupling(name = 'R2GC_182_43',
                       value = '(13*complex(0,1)*G**4)/(1728.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_183_44 = Coupling(name = 'R2GC_183_44',
                       value = '(13*complex(0,1)*G**4)/(384.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_184_45 = Coupling(name = 'R2GC_184_45',
                       value = '-(complex(0,1)*G**2)/(72.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_185_46 = Coupling(name = 'R2GC_185_46',
                       value = '(-53*complex(0,1)*G**3)/(576.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_186_47 = Coupling(name = 'R2GC_186_47',
                       value = '(53*complex(0,1)*G**3)/(576.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_187_48 = Coupling(name = 'R2GC_187_48',
                       value = '(-3*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_187_49 = Coupling(name = 'R2GC_187_49',
                       value = '(-79*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_203_50 = Coupling(name = 'R2GC_203_50',
                       value = '-(complex(0,1)*G**2*gsbVEL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_204_51 = Coupling(name = 'R2GC_204_51',
                       value = '-(complex(0,1)*G**2*gsbVML)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_205_52 = Coupling(name = 'R2GC_205_52',
                       value = '-(complex(0,1)*G**2*gsbVTL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_206_53 = Coupling(name = 'R2GC_206_53',
                       value = '-(complex(0,1)*G**2*gscEL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_208_54 = Coupling(name = 'R2GC_208_54',
                       value = '-(complex(0,1)*G**2*gscER)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_210_55 = Coupling(name = 'R2GC_210_55',
                       value = '-(complex(0,1)*G**2*gscML)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_212_56 = Coupling(name = 'R2GC_212_56',
                       value = '-(complex(0,1)*G**2*gscMR)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_214_57 = Coupling(name = 'R2GC_214_57',
                       value = '-(complex(0,1)*G**2*gscTL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_216_58 = Coupling(name = 'R2GC_216_58',
                       value = '-(complex(0,1)*G**2*gscTR)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_218_59 = Coupling(name = 'R2GC_218_59',
                       value = '-(complex(0,1)*G**2*gscVEL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_219_60 = Coupling(name = 'R2GC_219_60',
                       value = '-(complex(0,1)*G**2*gscVML)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_220_61 = Coupling(name = 'R2GC_220_61',
                       value = '-(complex(0,1)*G**2*gscVTL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_221_62 = Coupling(name = 'R2GC_221_62',
                       value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_222_63 = Coupling(name = 'R2GC_222_63',
                       value = '-(complex(0,1)*G**2*gsdVEL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_224_64 = Coupling(name = 'R2GC_224_64',
                       value = '-(complex(0,1)*G**2*gsdVML)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_226_65 = Coupling(name = 'R2GC_226_65',
                       value = '-(complex(0,1)*G**2*gsdVTL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_228_66 = Coupling(name = 'R2GC_228_66',
                       value = '-(complex(0,1)*G**2*gsdEL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_229_67 = Coupling(name = 'R2GC_229_67',
                       value = '-(complex(0,1)*G**2*gsdER)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_230_68 = Coupling(name = 'R2GC_230_68',
                       value = '-(complex(0,1)*G**2*gsdML)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_231_69 = Coupling(name = 'R2GC_231_69',
                       value = '-(complex(0,1)*G**2*gsdMR)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_232_70 = Coupling(name = 'R2GC_232_70',
                       value = '-(complex(0,1)*G**2*gsdTL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_233_71 = Coupling(name = 'R2GC_233_71',
                       value = '-(complex(0,1)*G**2*gsdTR)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_234_72 = Coupling(name = 'R2GC_234_72',
                       value = '(-11*complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_234_73 = Coupling(name = 'R2GC_234_73',
                       value = '(11*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_234_74 = Coupling(name = 'R2GC_234_74',
                       value = '(-55*complex(0,1)*G**4)/(3456.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_235_75 = Coupling(name = 'R2GC_235_75',
                       value = '(-5*complex(0,1)*G**4)/(96.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_235_76 = Coupling(name = 'R2GC_235_76',
                       value = '(5*complex(0,1)*G**4)/(384.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_235_77 = Coupling(name = 'R2GC_235_77',
                       value = '(-25*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_236_78 = Coupling(name = 'R2GC_236_78',
                       value = '-(complex(0,1)*G**2*gssVEL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_238_79 = Coupling(name = 'R2GC_238_79',
                       value = '-(complex(0,1)*G**2*gssVML)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_240_80 = Coupling(name = 'R2GC_240_80',
                       value = '-(complex(0,1)*G**2*gssVTL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_242_81 = Coupling(name = 'R2GC_242_81',
                       value = '-(complex(0,1)*G**2*gssEL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_243_82 = Coupling(name = 'R2GC_243_82',
                       value = '-(complex(0,1)*G**2*gssER)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_244_83 = Coupling(name = 'R2GC_244_83',
                       value = '-(complex(0,1)*G**2*gssML)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_245_84 = Coupling(name = 'R2GC_245_84',
                       value = '-(complex(0,1)*G**2*gssMR)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_246_85 = Coupling(name = 'R2GC_246_85',
                       value = '-(complex(0,1)*G**2*gssTL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_247_86 = Coupling(name = 'R2GC_247_86',
                       value = '-(complex(0,1)*G**2*gssTR)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_248_87 = Coupling(name = 'R2GC_248_87',
                       value = '-(complex(0,1)*G**2*gstEL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_249_88 = Coupling(name = 'R2GC_249_88',
                       value = '-(complex(0,1)*G**2*gstER)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_250_89 = Coupling(name = 'R2GC_250_89',
                       value = '-(complex(0,1)*G**2*gstML)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_251_90 = Coupling(name = 'R2GC_251_90',
                       value = '-(complex(0,1)*G**2*gstMR)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_252_91 = Coupling(name = 'R2GC_252_91',
                       value = '-(complex(0,1)*G**2*gstTL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_253_92 = Coupling(name = 'R2GC_253_92',
                       value = '-(complex(0,1)*G**2*gstTR)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_255_93 = Coupling(name = 'R2GC_255_93',
                       value = '-(complex(0,1)*G**2*gsuEL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_257_94 = Coupling(name = 'R2GC_257_94',
                       value = '-(complex(0,1)*G**2*gsuER)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_259_95 = Coupling(name = 'R2GC_259_95',
                       value = '-(complex(0,1)*G**2*gsuML)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_261_96 = Coupling(name = 'R2GC_261_96',
                       value = '-(complex(0,1)*G**2*gsuMR)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_263_97 = Coupling(name = 'R2GC_263_97',
                       value = '-(complex(0,1)*G**2*gsuTL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_265_98 = Coupling(name = 'R2GC_265_98',
                       value = '-(complex(0,1)*G**2*gsuTR)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_267_99 = Coupling(name = 'R2GC_267_99',
                       value = '-(complex(0,1)*G**2*gsuVEL)/(24.*cmath.pi**2)',
                       order = {'QCD':2,'QLD':1})

R2GC_268_100 = Coupling(name = 'R2GC_268_100',
                        value = '-(complex(0,1)*G**2*gsuVML)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QLD':1})

R2GC_269_101 = Coupling(name = 'R2GC_269_101',
                        value = '-(complex(0,1)*G**2*gsuVTL)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QLD':1})

R2GC_273_102 = Coupling(name = 'R2GC_273_102',
                        value = '-(complex(0,1)*G**2*gsbEL)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QLD':1})

R2GC_274_103 = Coupling(name = 'R2GC_274_103',
                        value = '-(complex(0,1)*G**2*gsbER)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QLD':1})

R2GC_275_104 = Coupling(name = 'R2GC_275_104',
                        value = '-(complex(0,1)*G**2*gsbML)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QLD':1})

R2GC_276_105 = Coupling(name = 'R2GC_276_105',
                        value = '-(complex(0,1)*G**2*gsbMR)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QLD':1})

R2GC_277_106 = Coupling(name = 'R2GC_277_106',
                        value = '-(complex(0,1)*G**2*gsbTL)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QLD':1})

R2GC_278_107 = Coupling(name = 'R2GC_278_107',
                        value = '-(complex(0,1)*G**2*gsbTR)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QLD':1})

R2GC_283_108 = Coupling(name = 'R2GC_283_108',
                        value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_286_109 = Coupling(name = 'R2GC_286_109',
                        value = '(complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_287_110 = Coupling(name = 'R2GC_287_110',
                        value = '-(G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_288_111 = Coupling(name = 'R2GC_288_111',
                        value = '(complex(0,1)*G**2*Msd**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_289_112 = Coupling(name = 'R2GC_289_112',
                        value = '(complex(0,1)*G**2*Msu**2)/(24.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_290_113 = Coupling(name = 'R2GC_290_113',
                        value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_290_114 = Coupling(name = 'R2GC_290_114',
                        value = '(3*complex(0,1)*G**2)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_291_115 = Coupling(name = 'R2GC_291_115',
                        value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_292_116 = Coupling(name = 'R2GC_292_116',
                        value = 'G**3/(24.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_292_117 = Coupling(name = 'R2GC_292_117',
                        value = '(11*G**3)/(64.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_295_118 = Coupling(name = 'R2GC_295_118',
                        value = '-G**3/(24.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_295_119 = Coupling(name = 'R2GC_295_119',
                        value = '(-11*G**3)/(64.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_298_120 = Coupling(name = 'R2GC_298_120',
                        value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_299_121 = Coupling(name = 'R2GC_299_121',
                        value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_299_122 = Coupling(name = 'R2GC_299_122',
                        value = '(19*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_300_123 = Coupling(name = 'R2GC_300_123',
                        value = '(31*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_301_124 = Coupling(name = 'R2GC_301_124',
                        value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_302_125 = Coupling(name = 'R2GC_302_125',
                        value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_303_126 = Coupling(name = 'R2GC_303_126',
                        value = '(7*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_313_127 = Coupling(name = 'R2GC_313_127',
                        value = '-(complex(0,1)*G**2*gstVEL)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QLD':1})

R2GC_314_128 = Coupling(name = 'R2GC_314_128',
                        value = '-(complex(0,1)*G**2*gstVML)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QLD':1})

R2GC_315_129 = Coupling(name = 'R2GC_315_129',
                        value = '-(complex(0,1)*G**2*gstVTL)/(24.*cmath.pi**2)',
                        order = {'QCD':2,'QLD':1})

R2GC_316_130 = Coupling(name = 'R2GC_316_130',
                        value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_320_131 = Coupling(name = 'R2GC_320_131',
                        value = '(G**2*yb)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_321_132 = Coupling(name = 'R2GC_321_132',
                        value = '-(G**2*yb)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_322_133 = Coupling(name = 'R2GC_322_133',
                        value = '(G**2*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_323_134 = Coupling(name = 'R2GC_323_134',
                        value = '-(G**2*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_324_135 = Coupling(name = 'R2GC_324_135',
                        value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_325_136 = Coupling(name = 'R2GC_325_136',
                        value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_154_1 = Coupling(name = 'UVGC_154_1',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_155_2 = Coupling(name = 'UVGC_155_2',
                      value = {-1:'(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_157_3 = Coupling(name = 'UVGC_157_3',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_158_4 = Coupling(name = 'UVGC_158_4',
                      value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_164_5 = Coupling(name = 'UVGC_164_5',
                      value = {-1:'(9*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_164_6 = Coupling(name = 'UVGC_164_6',
                      value = {-1:'-G**3/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_165_7 = Coupling(name = 'UVGC_165_7',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_165_8 = Coupling(name = 'UVGC_165_8',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_166_9 = Coupling(name = 'UVGC_166_9',
                      value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_166_10 = Coupling(name = 'UVGC_166_10',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_168_11 = Coupling(name = 'UVGC_168_11',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_168_12 = Coupling(name = 'UVGC_168_12',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_169_13 = Coupling(name = 'UVGC_169_13',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_169_14 = Coupling(name = 'UVGC_169_14',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_170_15 = Coupling(name = 'UVGC_170_15',
                       value = {-1:'(-9*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_170_16 = Coupling(name = 'UVGC_170_16',
                       value = {-1:'G**3/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_170_17 = Coupling(name = 'UVGC_170_17',
                       value = {-1:'G**3/(96.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_171_18 = Coupling(name = 'UVGC_171_18',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_171_19 = Coupling(name = 'UVGC_171_19',
                       value = {-1:'(47*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_171_20 = Coupling(name = 'UVGC_171_20',
                       value = {-1:'(complex(0,1)*G**4)/(48.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_172_21 = Coupling(name = 'UVGC_172_21',
                       value = {-1:'(-253*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_172_22 = Coupling(name = 'UVGC_172_22',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_173_23 = Coupling(name = 'UVGC_173_23',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_174_24 = Coupling(name = 'UVGC_174_24',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_174_25 = Coupling(name = 'UVGC_174_25',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_174_26 = Coupling(name = 'UVGC_174_26',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_174_27 = Coupling(name = 'UVGC_174_27',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_174_28 = Coupling(name = 'UVGC_174_28',
                       value = {-1:'( 0 if Msd else (complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_174_29 = Coupling(name = 'UVGC_174_29',
                       value = {-1:'( 0 if Msu else (complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_174_30 = Coupling(name = 'UVGC_174_30',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_174_31 = Coupling(name = 'UVGC_174_31',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_175_32 = Coupling(name = 'UVGC_175_32',
                       value = {-1:'(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_176_33 = Coupling(name = 'UVGC_176_33',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_182_34 = Coupling(name = 'UVGC_182_34',
                       value = {-1:'(-13*complex(0,1)*G**4)/(72.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_182_35 = Coupling(name = 'UVGC_182_35',
                       value = {-1:'(13*complex(0,1)*G**4)/(288.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_183_36 = Coupling(name = 'UVGC_183_36',
                       value = {-1:'(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_183_37 = Coupling(name = 'UVGC_183_37',
                       value = {-1:'(3*complex(0,1)*G**4)/(64.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_184_38 = Coupling(name = 'UVGC_184_38',
                       value = {-1:'( -(complex(0,1)*G**2)/(6.*cmath.pi**2) if Msd else -(complex(0,1)*G**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2)/(6.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(72.*cmath.pi**2) if Msd else (complex(0,1)*G**2)/(72.*cmath.pi**2) ) - (complex(0,1)*G**2)/(72.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_185_39 = Coupling(name = 'UVGC_185_39',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if Msd else -(complex(0,1)*G**3)/(6.*cmath.pi**2) ) - (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( (complex(0,1)*G**3)/(72.*cmath.pi**2) if Msd else (complex(0,1)*G**3)/(72.*cmath.pi**2) ) - (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_186_40 = Coupling(name = 'UVGC_186_40',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_186_41 = Coupling(name = 'UVGC_186_41',
                       value = {-1:'-(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_186_42 = Coupling(name = 'UVGC_186_42',
                       value = {-1:'(19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_186_43 = Coupling(name = 'UVGC_186_43',
                       value = {-1:'(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_186_44 = Coupling(name = 'UVGC_186_44',
                       value = {-1:'( 0 if Msd else -(complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_186_45 = Coupling(name = 'UVGC_186_45',
                       value = {-1:'( 0 if Msu else -(complex(0,1)*G**3)/(192.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_186_46 = Coupling(name = 'UVGC_186_46',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_186_47 = Coupling(name = 'UVGC_186_47',
                       value = {-1:'( (complex(0,1)*G**3)/(6.*cmath.pi**2) if Msd else (complex(0,1)*G**3)/(6.*cmath.pi**2) ) + (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( -(complex(0,1)*G**3)/(72.*cmath.pi**2) if Msd else -(complex(0,1)*G**3)/(72.*cmath.pi**2) ) + (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_187_48 = Coupling(name = 'UVGC_187_48',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(24.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_187_49 = Coupling(name = 'UVGC_187_49',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_187_50 = Coupling(name = 'UVGC_187_50',
                       value = {-1:'(-7*complex(0,1)*G**4)/(16.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_187_51 = Coupling(name = 'UVGC_187_51',
                       value = {-1:'-(complex(0,1)*G**4)/(64.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_187_52 = Coupling(name = 'UVGC_187_52',
                       value = {-1:'( 0 if Msd else (complex(0,1)*G**4)/(96.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_187_53 = Coupling(name = 'UVGC_187_53',
                       value = {-1:'( 0 if Msu else (complex(0,1)*G**4)/(96.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_187_54 = Coupling(name = 'UVGC_187_54',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(24.*cmath.pi**2) )'},
                       order = {'QCD':4})

UVGC_187_55 = Coupling(name = 'UVGC_187_55',
                       value = {-1:'( -(complex(0,1)*G**4)/(6.*cmath.pi**2) if Msd else -(complex(0,1)*G**4)/(6.*cmath.pi**2) ) - (13*complex(0,1)*G**4)/(192.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(72.*cmath.pi**2) if Msd else (complex(0,1)*G**4)/(72.*cmath.pi**2) ) - (complex(0,1)*G**4)/(72.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_190_56 = Coupling(name = 'UVGC_190_56',
                       value = {-1:'( -(complex(0,1)*G**2)/(6.*cmath.pi**2) if Msu else -(complex(0,1)*G**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2)/(6.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(72.*cmath.pi**2) if Msu else (complex(0,1)*G**2)/(72.*cmath.pi**2) ) - (complex(0,1)*G**2)/(72.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_191_57 = Coupling(name = 'UVGC_191_57',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if Msu else -(complex(0,1)*G**3)/(6.*cmath.pi**2) ) - (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( (complex(0,1)*G**3)/(72.*cmath.pi**2) if Msu else (complex(0,1)*G**3)/(72.*cmath.pi**2) ) - (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_192_58 = Coupling(name = 'UVGC_192_58',
                       value = {-1:'( (complex(0,1)*G**3)/(6.*cmath.pi**2) if Msu else (complex(0,1)*G**3)/(6.*cmath.pi**2) ) + (complex(0,1)*G**3)/(48.*cmath.pi**2)',0:'( -(complex(0,1)*G**3)/(72.*cmath.pi**2) if Msu else -(complex(0,1)*G**3)/(72.*cmath.pi**2) ) + (complex(0,1)*G**3)/(72.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_193_59 = Coupling(name = 'UVGC_193_59',
                       value = {-1:'( -(complex(0,1)*G**4)/(6.*cmath.pi**2) if Msu else -(complex(0,1)*G**4)/(6.*cmath.pi**2) ) - (13*complex(0,1)*G**4)/(192.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(72.*cmath.pi**2) if Msu else (complex(0,1)*G**4)/(72.*cmath.pi**2) ) - (complex(0,1)*G**4)/(72.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_203_60 = Coupling(name = 'UVGC_203_60',
                       value = {-1:'( -(complex(0,1)*G**2*gsbVEL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsbVEL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsbVEL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsbVEL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsbVEL)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_203_61 = Coupling(name = 'UVGC_203_61',
                       value = {-1:'-(complex(0,1)*G**2*gsbVEL)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_204_62 = Coupling(name = 'UVGC_204_62',
                       value = {-1:'( -(complex(0,1)*G**2*gsbVML)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsbVML)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsbVML)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsbVML)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsbVML)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_204_63 = Coupling(name = 'UVGC_204_63',
                       value = {-1:'-(complex(0,1)*G**2*gsbVML)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_205_64 = Coupling(name = 'UVGC_205_64',
                       value = {-1:'( -(complex(0,1)*G**2*gsbVTL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsbVTL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsbVTL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsbVTL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsbVTL)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_205_65 = Coupling(name = 'UVGC_205_65',
                       value = {-1:'-(complex(0,1)*G**2*gsbVTL)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_206_66 = Coupling(name = 'UVGC_206_66',
                       value = {-1:'( -(complex(0,1)*G**2*gscEL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gscEL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gscEL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gscEL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gscEL)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_206_67 = Coupling(name = 'UVGC_206_67',
                       value = {-1:'-(complex(0,1)*G**2*gscEL)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_207_68 = Coupling(name = 'UVGC_207_68',
                       value = {-1:'(complex(0,1)*G**2*gscEL)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_208_69 = Coupling(name = 'UVGC_208_69',
                       value = {-1:'( -(complex(0,1)*G**2*gscER)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gscER)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gscER)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gscER)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gscER)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_208_70 = Coupling(name = 'UVGC_208_70',
                       value = {-1:'-(complex(0,1)*G**2*gscER)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_209_71 = Coupling(name = 'UVGC_209_71',
                       value = {-1:'(complex(0,1)*G**2*gscER)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_210_72 = Coupling(name = 'UVGC_210_72',
                       value = {-1:'( -(complex(0,1)*G**2*gscML)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gscML)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gscML)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gscML)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gscML)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_210_73 = Coupling(name = 'UVGC_210_73',
                       value = {-1:'-(complex(0,1)*G**2*gscML)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_211_74 = Coupling(name = 'UVGC_211_74',
                       value = {-1:'(complex(0,1)*G**2*gscML)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_212_75 = Coupling(name = 'UVGC_212_75',
                       value = {-1:'( -(complex(0,1)*G**2*gscMR)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gscMR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gscMR)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gscMR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gscMR)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_212_76 = Coupling(name = 'UVGC_212_76',
                       value = {-1:'-(complex(0,1)*G**2*gscMR)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_213_77 = Coupling(name = 'UVGC_213_77',
                       value = {-1:'(complex(0,1)*G**2*gscMR)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_214_78 = Coupling(name = 'UVGC_214_78',
                       value = {-1:'( -(complex(0,1)*G**2*gscTL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gscTL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gscTL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gscTL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gscTL)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_214_79 = Coupling(name = 'UVGC_214_79',
                       value = {-1:'-(complex(0,1)*G**2*gscTL)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_215_80 = Coupling(name = 'UVGC_215_80',
                       value = {-1:'(complex(0,1)*G**2*gscTL)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_216_81 = Coupling(name = 'UVGC_216_81',
                       value = {-1:'( -(complex(0,1)*G**2*gscTR)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gscTR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gscTR)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gscTR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gscTR)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_216_82 = Coupling(name = 'UVGC_216_82',
                       value = {-1:'-(complex(0,1)*G**2*gscTR)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_217_83 = Coupling(name = 'UVGC_217_83',
                       value = {-1:'(complex(0,1)*G**2*gscTR)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_218_84 = Coupling(name = 'UVGC_218_84',
                       value = {-1:'(complex(0,1)*G**2*gscVEL)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_218_85 = Coupling(name = 'UVGC_218_85',
                       value = {-1:'( -(complex(0,1)*G**2*gscVEL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gscVEL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gscVEL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gscVEL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gscVEL)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_218_86 = Coupling(name = 'UVGC_218_86',
                       value = {-1:'-(complex(0,1)*G**2*gscVEL)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_219_87 = Coupling(name = 'UVGC_219_87',
                       value = {-1:'(complex(0,1)*G**2*gscVML)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_219_88 = Coupling(name = 'UVGC_219_88',
                       value = {-1:'( -(complex(0,1)*G**2*gscVML)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gscVML)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gscVML)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gscVML)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gscVML)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_219_89 = Coupling(name = 'UVGC_219_89',
                       value = {-1:'-(complex(0,1)*G**2*gscVML)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_220_90 = Coupling(name = 'UVGC_220_90',
                       value = {-1:'(complex(0,1)*G**2*gscVTL)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_220_91 = Coupling(name = 'UVGC_220_91',
                       value = {-1:'( -(complex(0,1)*G**2*gscVTL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gscVTL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gscVTL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gscVTL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gscVTL)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_220_92 = Coupling(name = 'UVGC_220_92',
                       value = {-1:'-(complex(0,1)*G**2*gscVTL)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_221_93 = Coupling(name = 'UVGC_221_93',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_221_94 = Coupling(name = 'UVGC_221_94',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_222_95 = Coupling(name = 'UVGC_222_95',
                       value = {-1:'( -(complex(0,1)*G**2*gsdVEL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsdVEL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsdVEL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsdVEL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsdVEL)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_222_96 = Coupling(name = 'UVGC_222_96',
                       value = {-1:'-(complex(0,1)*G**2*gsdVEL)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_223_97 = Coupling(name = 'UVGC_223_97',
                       value = {-1:'(complex(0,1)*G**2*gsdVEL)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_224_98 = Coupling(name = 'UVGC_224_98',
                       value = {-1:'( -(complex(0,1)*G**2*gsdVML)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsdVML)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsdVML)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsdVML)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsdVML)/(144.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_224_99 = Coupling(name = 'UVGC_224_99',
                       value = {-1:'-(complex(0,1)*G**2*gsdVML)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QLD':1})

UVGC_225_100 = Coupling(name = 'UVGC_225_100',
                        value = {-1:'(complex(0,1)*G**2*gsdVML)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_226_101 = Coupling(name = 'UVGC_226_101',
                        value = {-1:'( -(complex(0,1)*G**2*gsdVTL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsdVTL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsdVTL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsdVTL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsdVTL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_226_102 = Coupling(name = 'UVGC_226_102',
                        value = {-1:'-(complex(0,1)*G**2*gsdVTL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_227_103 = Coupling(name = 'UVGC_227_103',
                        value = {-1:'(complex(0,1)*G**2*gsdVTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_228_104 = Coupling(name = 'UVGC_228_104',
                        value = {-1:'(complex(0,1)*G**2*gsdEL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_228_105 = Coupling(name = 'UVGC_228_105',
                        value = {-1:'( -(complex(0,1)*G**2*gsdEL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsdEL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsdEL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsdEL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsdEL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_228_106 = Coupling(name = 'UVGC_228_106',
                        value = {-1:'-(complex(0,1)*G**2*gsdEL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_229_107 = Coupling(name = 'UVGC_229_107',
                        value = {-1:'(complex(0,1)*G**2*gsdER)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_229_108 = Coupling(name = 'UVGC_229_108',
                        value = {-1:'( -(complex(0,1)*G**2*gsdER)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsdER)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsdER)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsdER)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsdER)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_229_109 = Coupling(name = 'UVGC_229_109',
                        value = {-1:'-(complex(0,1)*G**2*gsdER)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_230_110 = Coupling(name = 'UVGC_230_110',
                        value = {-1:'(complex(0,1)*G**2*gsdML)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_230_111 = Coupling(name = 'UVGC_230_111',
                        value = {-1:'( -(complex(0,1)*G**2*gsdML)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsdML)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsdML)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsdML)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsdML)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_230_112 = Coupling(name = 'UVGC_230_112',
                        value = {-1:'-(complex(0,1)*G**2*gsdML)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_231_113 = Coupling(name = 'UVGC_231_113',
                        value = {-1:'(complex(0,1)*G**2*gsdMR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_231_114 = Coupling(name = 'UVGC_231_114',
                        value = {-1:'( -(complex(0,1)*G**2*gsdMR)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsdMR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsdMR)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsdMR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsdMR)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_231_115 = Coupling(name = 'UVGC_231_115',
                        value = {-1:'-(complex(0,1)*G**2*gsdMR)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_232_116 = Coupling(name = 'UVGC_232_116',
                        value = {-1:'(complex(0,1)*G**2*gsdTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_232_117 = Coupling(name = 'UVGC_232_117',
                        value = {-1:'( -(complex(0,1)*G**2*gsdTL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsdTL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsdTL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsdTL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsdTL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_232_118 = Coupling(name = 'UVGC_232_118',
                        value = {-1:'-(complex(0,1)*G**2*gsdTL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_233_119 = Coupling(name = 'UVGC_233_119',
                        value = {-1:'(complex(0,1)*G**2*gsdTR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_233_120 = Coupling(name = 'UVGC_233_120',
                        value = {-1:'( -(complex(0,1)*G**2*gsdTR)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsdTR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsdTR)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsdTR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsdTR)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_233_121 = Coupling(name = 'UVGC_233_121',
                        value = {-1:'-(complex(0,1)*G**2*gsdTR)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_234_122 = Coupling(name = 'UVGC_234_122',
                        value = {-1:'(-11*complex(0,1)*G**4)/(144.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_234_123 = Coupling(name = 'UVGC_234_123',
                        value = {-1:'(11*complex(0,1)*G**4)/(576.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_234_124 = Coupling(name = 'UVGC_234_124',
                        value = {-1:'(-11*complex(0,1)*G**4)/(576.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_235_125 = Coupling(name = 'UVGC_235_125',
                        value = {-1:'(-5*complex(0,1)*G**4)/(48.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_235_126 = Coupling(name = 'UVGC_235_126',
                        value = {-1:'(5*complex(0,1)*G**4)/(192.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_235_127 = Coupling(name = 'UVGC_235_127',
                        value = {-1:'(-5*complex(0,1)*G**4)/(192.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_236_128 = Coupling(name = 'UVGC_236_128',
                        value = {-1:'( -(complex(0,1)*G**2*gssVEL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gssVEL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gssVEL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gssVEL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gssVEL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_236_129 = Coupling(name = 'UVGC_236_129',
                        value = {-1:'-(complex(0,1)*G**2*gssVEL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_237_130 = Coupling(name = 'UVGC_237_130',
                        value = {-1:'(complex(0,1)*G**2*gssVEL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_238_131 = Coupling(name = 'UVGC_238_131',
                        value = {-1:'( -(complex(0,1)*G**2*gssVML)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gssVML)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gssVML)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gssVML)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gssVML)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_238_132 = Coupling(name = 'UVGC_238_132',
                        value = {-1:'-(complex(0,1)*G**2*gssVML)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_239_133 = Coupling(name = 'UVGC_239_133',
                        value = {-1:'(complex(0,1)*G**2*gssVML)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_240_134 = Coupling(name = 'UVGC_240_134',
                        value = {-1:'( -(complex(0,1)*G**2*gssVTL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gssVTL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gssVTL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gssVTL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gssVTL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_240_135 = Coupling(name = 'UVGC_240_135',
                        value = {-1:'-(complex(0,1)*G**2*gssVTL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_241_136 = Coupling(name = 'UVGC_241_136',
                        value = {-1:'(complex(0,1)*G**2*gssVTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_242_137 = Coupling(name = 'UVGC_242_137',
                        value = {-1:'( -(complex(0,1)*G**2*gssEL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gssEL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gssEL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gssEL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gssEL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_242_138 = Coupling(name = 'UVGC_242_138',
                        value = {-1:'(complex(0,1)*G**2*gssEL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_242_139 = Coupling(name = 'UVGC_242_139',
                        value = {-1:'-(complex(0,1)*G**2*gssEL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_243_140 = Coupling(name = 'UVGC_243_140',
                        value = {-1:'( -(complex(0,1)*G**2*gssER)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gssER)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gssER)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gssER)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gssER)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_243_141 = Coupling(name = 'UVGC_243_141',
                        value = {-1:'(complex(0,1)*G**2*gssER)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_243_142 = Coupling(name = 'UVGC_243_142',
                        value = {-1:'-(complex(0,1)*G**2*gssER)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_244_143 = Coupling(name = 'UVGC_244_143',
                        value = {-1:'( -(complex(0,1)*G**2*gssML)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gssML)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gssML)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gssML)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gssML)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_244_144 = Coupling(name = 'UVGC_244_144',
                        value = {-1:'(complex(0,1)*G**2*gssML)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_244_145 = Coupling(name = 'UVGC_244_145',
                        value = {-1:'-(complex(0,1)*G**2*gssML)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_245_146 = Coupling(name = 'UVGC_245_146',
                        value = {-1:'( -(complex(0,1)*G**2*gssMR)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gssMR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gssMR)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gssMR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gssMR)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_245_147 = Coupling(name = 'UVGC_245_147',
                        value = {-1:'(complex(0,1)*G**2*gssMR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_245_148 = Coupling(name = 'UVGC_245_148',
                        value = {-1:'-(complex(0,1)*G**2*gssMR)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_246_149 = Coupling(name = 'UVGC_246_149',
                        value = {-1:'( -(complex(0,1)*G**2*gssTL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gssTL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gssTL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gssTL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gssTL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_246_150 = Coupling(name = 'UVGC_246_150',
                        value = {-1:'(complex(0,1)*G**2*gssTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_246_151 = Coupling(name = 'UVGC_246_151',
                        value = {-1:'-(complex(0,1)*G**2*gssTL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_247_152 = Coupling(name = 'UVGC_247_152',
                        value = {-1:'( -(complex(0,1)*G**2*gssTR)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gssTR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gssTR)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gssTR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gssTR)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_247_153 = Coupling(name = 'UVGC_247_153',
                        value = {-1:'(complex(0,1)*G**2*gssTR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_247_154 = Coupling(name = 'UVGC_247_154',
                        value = {-1:'-(complex(0,1)*G**2*gssTR)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_248_155 = Coupling(name = 'UVGC_248_155',
                        value = {-1:'( -(complex(0,1)*G**2*gstEL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gstEL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gstEL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gstEL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gstEL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_248_156 = Coupling(name = 'UVGC_248_156',
                        value = {-1:'-(complex(0,1)*G**2*gstEL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_249_157 = Coupling(name = 'UVGC_249_157',
                        value = {-1:'( -(complex(0,1)*G**2*gstER)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gstER)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gstER)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gstER)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gstER)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_249_158 = Coupling(name = 'UVGC_249_158',
                        value = {-1:'-(complex(0,1)*G**2*gstER)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_250_159 = Coupling(name = 'UVGC_250_159',
                        value = {-1:'( -(complex(0,1)*G**2*gstML)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gstML)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gstML)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gstML)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gstML)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_250_160 = Coupling(name = 'UVGC_250_160',
                        value = {-1:'-(complex(0,1)*G**2*gstML)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_251_161 = Coupling(name = 'UVGC_251_161',
                        value = {-1:'( -(complex(0,1)*G**2*gstMR)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gstMR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gstMR)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gstMR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gstMR)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_251_162 = Coupling(name = 'UVGC_251_162',
                        value = {-1:'-(complex(0,1)*G**2*gstMR)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_252_163 = Coupling(name = 'UVGC_252_163',
                        value = {-1:'( -(complex(0,1)*G**2*gstTL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gstTL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gstTL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gstTL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gstTL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_252_164 = Coupling(name = 'UVGC_252_164',
                        value = {-1:'-(complex(0,1)*G**2*gstTL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_253_165 = Coupling(name = 'UVGC_253_165',
                        value = {-1:'( -(complex(0,1)*G**2*gstTR)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gstTR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gstTR)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gstTR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gstTR)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_253_166 = Coupling(name = 'UVGC_253_166',
                        value = {-1:'-(complex(0,1)*G**2*gstTR)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_255_167 = Coupling(name = 'UVGC_255_167',
                        value = {-1:'( -(complex(0,1)*G**2*gsuEL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsuEL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsuEL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsuEL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsuEL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_255_168 = Coupling(name = 'UVGC_255_168',
                        value = {-1:'-(complex(0,1)*G**2*gsuEL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_256_169 = Coupling(name = 'UVGC_256_169',
                        value = {-1:'(complex(0,1)*G**2*gsuEL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_257_170 = Coupling(name = 'UVGC_257_170',
                        value = {-1:'( -(complex(0,1)*G**2*gsuER)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsuER)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsuER)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsuER)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsuER)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_257_171 = Coupling(name = 'UVGC_257_171',
                        value = {-1:'-(complex(0,1)*G**2*gsuER)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_258_172 = Coupling(name = 'UVGC_258_172',
                        value = {-1:'(complex(0,1)*G**2*gsuER)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_259_173 = Coupling(name = 'UVGC_259_173',
                        value = {-1:'( -(complex(0,1)*G**2*gsuML)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsuML)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsuML)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsuML)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsuML)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_259_174 = Coupling(name = 'UVGC_259_174',
                        value = {-1:'-(complex(0,1)*G**2*gsuML)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_260_175 = Coupling(name = 'UVGC_260_175',
                        value = {-1:'(complex(0,1)*G**2*gsuML)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_261_176 = Coupling(name = 'UVGC_261_176',
                        value = {-1:'( -(complex(0,1)*G**2*gsuMR)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsuMR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsuMR)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsuMR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsuMR)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_261_177 = Coupling(name = 'UVGC_261_177',
                        value = {-1:'-(complex(0,1)*G**2*gsuMR)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_262_178 = Coupling(name = 'UVGC_262_178',
                        value = {-1:'(complex(0,1)*G**2*gsuMR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_263_179 = Coupling(name = 'UVGC_263_179',
                        value = {-1:'( -(complex(0,1)*G**2*gsuTL)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsuTL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsuTL)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsuTL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsuTL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_263_180 = Coupling(name = 'UVGC_263_180',
                        value = {-1:'-(complex(0,1)*G**2*gsuTL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_264_181 = Coupling(name = 'UVGC_264_181',
                        value = {-1:'(complex(0,1)*G**2*gsuTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_265_182 = Coupling(name = 'UVGC_265_182',
                        value = {-1:'( -(complex(0,1)*G**2*gsuTR)/(12.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*gsuTR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsuTR)/(144.*cmath.pi**2) if Msd else (complex(0,1)*G**2*gsuTR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsuTR)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_265_183 = Coupling(name = 'UVGC_265_183',
                        value = {-1:'-(complex(0,1)*G**2*gsuTR)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_266_184 = Coupling(name = 'UVGC_266_184',
                        value = {-1:'(complex(0,1)*G**2*gsuTR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_267_185 = Coupling(name = 'UVGC_267_185',
                        value = {-1:'( -(complex(0,1)*G**2*gsuVEL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsuVEL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsuVEL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsuVEL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsuVEL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_267_186 = Coupling(name = 'UVGC_267_186',
                        value = {-1:'(complex(0,1)*G**2*gsuVEL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_267_187 = Coupling(name = 'UVGC_267_187',
                        value = {-1:'-(complex(0,1)*G**2*gsuVEL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_268_188 = Coupling(name = 'UVGC_268_188',
                        value = {-1:'( -(complex(0,1)*G**2*gsuVML)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsuVML)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsuVML)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsuVML)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsuVML)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_268_189 = Coupling(name = 'UVGC_268_189',
                        value = {-1:'(complex(0,1)*G**2*gsuVML)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_268_190 = Coupling(name = 'UVGC_268_190',
                        value = {-1:'-(complex(0,1)*G**2*gsuVML)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_269_191 = Coupling(name = 'UVGC_269_191',
                        value = {-1:'( -(complex(0,1)*G**2*gsuVTL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsuVTL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsuVTL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsuVTL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsuVTL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_269_192 = Coupling(name = 'UVGC_269_192',
                        value = {-1:'(complex(0,1)*G**2*gsuVTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_269_193 = Coupling(name = 'UVGC_269_193',
                        value = {-1:'-(complex(0,1)*G**2*gsuVTL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_270_194 = Coupling(name = 'UVGC_270_194',
                        value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_271_195 = Coupling(name = 'UVGC_271_195',
                        value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2)/(36.*cmath.pi**2) )',0:'( (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) ) - (ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_272_196 = Coupling(name = 'UVGC_272_196',
                        value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MB else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_273_197 = Coupling(name = 'UVGC_273_197',
                        value = {-1:'( -(complex(0,1)*G**2*gsbEL)/(12.*cmath.pi**2) if MB else (complex(0,1)*G**2*gsbEL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gsbEL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gsbEL*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(complex(0,1)*G**2*gsbEL)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gsbEL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_273_198 = Coupling(name = 'UVGC_273_198',
                        value = {-1:'( -(complex(0,1)*G**2*gsbEL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsbEL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsbEL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsbEL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsbEL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_273_199 = Coupling(name = 'UVGC_273_199',
                        value = {-1:'-(complex(0,1)*G**2*gsbEL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_274_200 = Coupling(name = 'UVGC_274_200',
                        value = {-1:'( -(complex(0,1)*G**2*gsbER)/(12.*cmath.pi**2) if MB else (complex(0,1)*G**2*gsbER)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gsbER)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gsbER*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(complex(0,1)*G**2*gsbER)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gsbER)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_274_201 = Coupling(name = 'UVGC_274_201',
                        value = {-1:'( -(complex(0,1)*G**2*gsbER)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsbER)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsbER)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsbER)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsbER)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_274_202 = Coupling(name = 'UVGC_274_202',
                        value = {-1:'-(complex(0,1)*G**2*gsbER)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_275_203 = Coupling(name = 'UVGC_275_203',
                        value = {-1:'( -(complex(0,1)*G**2*gsbML)/(12.*cmath.pi**2) if MB else (complex(0,1)*G**2*gsbML)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gsbML)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gsbML*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(complex(0,1)*G**2*gsbML)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gsbML)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_275_204 = Coupling(name = 'UVGC_275_204',
                        value = {-1:'( -(complex(0,1)*G**2*gsbML)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsbML)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsbML)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsbML)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsbML)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_275_205 = Coupling(name = 'UVGC_275_205',
                        value = {-1:'-(complex(0,1)*G**2*gsbML)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_276_206 = Coupling(name = 'UVGC_276_206',
                        value = {-1:'( -(complex(0,1)*G**2*gsbMR)/(12.*cmath.pi**2) if MB else (complex(0,1)*G**2*gsbMR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gsbMR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gsbMR*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(complex(0,1)*G**2*gsbMR)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gsbMR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_276_207 = Coupling(name = 'UVGC_276_207',
                        value = {-1:'( -(complex(0,1)*G**2*gsbMR)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsbMR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsbMR)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsbMR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsbMR)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_276_208 = Coupling(name = 'UVGC_276_208',
                        value = {-1:'-(complex(0,1)*G**2*gsbMR)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_277_209 = Coupling(name = 'UVGC_277_209',
                        value = {-1:'( -(complex(0,1)*G**2*gsbTL)/(12.*cmath.pi**2) if MB else (complex(0,1)*G**2*gsbTL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gsbTL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gsbTL*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(complex(0,1)*G**2*gsbTL)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gsbTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_277_210 = Coupling(name = 'UVGC_277_210',
                        value = {-1:'( -(complex(0,1)*G**2*gsbTL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsbTL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsbTL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsbTL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsbTL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_277_211 = Coupling(name = 'UVGC_277_211',
                        value = {-1:'-(complex(0,1)*G**2*gsbTL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_278_212 = Coupling(name = 'UVGC_278_212',
                        value = {-1:'( -(complex(0,1)*G**2*gsbTR)/(12.*cmath.pi**2) if MB else (complex(0,1)*G**2*gsbTR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gsbTR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gsbTR*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(complex(0,1)*G**2*gsbTR)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gsbTR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_278_213 = Coupling(name = 'UVGC_278_213',
                        value = {-1:'( -(complex(0,1)*G**2*gsbTR)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gsbTR)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gsbTR)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gsbTR)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gsbTR)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_278_214 = Coupling(name = 'UVGC_278_214',
                        value = {-1:'-(complex(0,1)*G**2*gsbTR)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_279_215 = Coupling(name = 'UVGC_279_215',
                        value = {-1:'( -(complex(0,1)*G**2*gsbVEL)/(12.*cmath.pi**2) if MB else (complex(0,1)*G**2*gsbVEL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gsbVEL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gsbVEL*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(complex(0,1)*G**2*gsbVEL)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gsbVEL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_280_216 = Coupling(name = 'UVGC_280_216',
                        value = {-1:'( -(complex(0,1)*G**2*gsbVML)/(12.*cmath.pi**2) if MB else (complex(0,1)*G**2*gsbVML)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gsbVML)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gsbVML*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(complex(0,1)*G**2*gsbVML)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gsbVML)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_281_217 = Coupling(name = 'UVGC_281_217',
                        value = {-1:'( -(complex(0,1)*G**2*gsbVTL)/(12.*cmath.pi**2) if MB else (complex(0,1)*G**2*gsbVTL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gsbVTL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gsbVTL*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(complex(0,1)*G**2*gsbVTL)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gsbVTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_282_218 = Coupling(name = 'UVGC_282_218',
                        value = {0:'( (complex(0,1)*G**2*MB)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':2})

UVGC_283_219 = Coupling(name = 'UVGC_283_219',
                        value = {-1:'( (complex(0,1)*G**2*MB)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MB)/(3.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2*MB)/(12.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MB)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_284_220 = Coupling(name = 'UVGC_284_220',
                        value = {-1:'( (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MB else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(12.*cw*cmath.pi**2) if MB else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_285_221 = Coupling(name = 'UVGC_285_221',
                        value = {-1:'( -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) if MB else (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',0:'( (-5*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_286_222 = Coupling(name = 'UVGC_286_222',
                        value = {-1:'( (complex(0,1)*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_287_223 = Coupling(name = 'UVGC_287_223',
                        value = {-1:'( -(G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) + (G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else -(G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_288_224 = Coupling(name = 'UVGC_288_224',
                        value = {-1:'( (complex(0,1)*G**2*Msd**2)/(6.*cmath.pi**2) if Msd else (complex(0,1)*G**2*Msd**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2*Msd**2)/(12.*cmath.pi**2)',0:'( (41*complex(0,1)*G**2*Msd**2)/(72.*cmath.pi**2) - (complex(0,1)*G**2*Msd**2*reglog(Msd/MU_R))/(2.*cmath.pi**2) if Msd else -(complex(0,1)*G**2*Msd**2)/(72.*cmath.pi**2) ) + (complex(0,1)*G**2*Msd**2)/(72.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_289_225 = Coupling(name = 'UVGC_289_225',
                        value = {-1:'( (complex(0,1)*G**2*Msu**2)/(6.*cmath.pi**2) if Msu else (complex(0,1)*G**2*Msu**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2*Msu**2)/(12.*cmath.pi**2)',0:'( (41*complex(0,1)*G**2*Msu**2)/(72.*cmath.pi**2) - (complex(0,1)*G**2*Msu**2*reglog(Msu/MU_R))/(2.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*Msu**2)/(72.*cmath.pi**2) ) + (complex(0,1)*G**2*Msu**2)/(72.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_290_226 = Coupling(name = 'UVGC_290_226',
                        value = {-1:'( 0 if MB else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':2})

UVGC_290_227 = Coupling(name = 'UVGC_290_227',
                        value = {-1:'( 0 if Msd else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**2)/(96.*cmath.pi**2) - (complex(0,1)*G**2*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_290_228 = Coupling(name = 'UVGC_290_228',
                        value = {-1:'( 0 if Msu else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**2)/(96.*cmath.pi**2) - (complex(0,1)*G**2*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else -(complex(0,1)*G**2)/(96.*cmath.pi**2) ) + (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_290_229 = Coupling(name = 'UVGC_290_229',
                        value = {-1:'( 0 if MT else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':2})

UVGC_291_230 = Coupling(name = 'UVGC_291_230',
                        value = {-1:'( 0 if MB else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':2})

UVGC_291_231 = Coupling(name = 'UVGC_291_231',
                        value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_291_232 = Coupling(name = 'UVGC_291_232',
                        value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_291_233 = Coupling(name = 'UVGC_291_233',
                        value = {-1:'( 0 if Msd else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(96.*cmath.pi**2) + (complex(0,1)*G**2*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_291_234 = Coupling(name = 'UVGC_291_234',
                        value = {-1:'( 0 if Msu else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**2)/(96.*cmath.pi**2) + (complex(0,1)*G**2*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else (complex(0,1)*G**2)/(96.*cmath.pi**2) ) - (complex(0,1)*G**2)/(96.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_291_235 = Coupling(name = 'UVGC_291_235',
                        value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':2})

UVGC_292_236 = Coupling(name = 'UVGC_292_236',
                        value = {-1:'( 0 if MB else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':3})

UVGC_292_237 = Coupling(name = 'UVGC_292_237',
                        value = {-1:'-G**3/(48.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_292_238 = Coupling(name = 'UVGC_292_238',
                        value = {-1:'(51*G**3)/(128.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_292_239 = Coupling(name = 'UVGC_292_239',
                        value = {-1:'( 0 if Msd else -G**3/(64.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_292_240 = Coupling(name = 'UVGC_292_240',
                        value = {-1:'( 0 if Msu else -G**3/(64.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_292_241 = Coupling(name = 'UVGC_292_241',
                        value = {-1:'( 0 if MT else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':3})

UVGC_293_242 = Coupling(name = 'UVGC_293_242',
                        value = {-1:'(21*G**3)/(64.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_293_243 = Coupling(name = 'UVGC_293_243',
                        value = {-1:'G**3/(64.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_294_244 = Coupling(name = 'UVGC_294_244',
                        value = {-1:'(33*G**3)/(128.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_294_245 = Coupling(name = 'UVGC_294_245',
                        value = {-1:'(3*G**3)/(128.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_294_246 = Coupling(name = 'UVGC_294_246',
                        value = {-1:'( 0 if Msd else -G**3/(64.*cmath.pi**2) ) + G**3/(48.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_294_247 = Coupling(name = 'UVGC_294_247',
                        value = {-1:'( 0 if Msu else -G**3/(64.*cmath.pi**2) ) + G**3/(48.*cmath.pi**2)',0:'( -G**3/(96.*cmath.pi**2) - (G**3*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else -G**3/(96.*cmath.pi**2) ) + G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_295_248 = Coupling(name = 'UVGC_295_248',
                        value = {-1:'( 0 if MB else G**3/(16.*cmath.pi**2) ) - G**3/(24.*cmath.pi**2)',0:'( (G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':3})

UVGC_295_249 = Coupling(name = 'UVGC_295_249',
                        value = {-1:'G**3/(48.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_295_250 = Coupling(name = 'UVGC_295_250',
                        value = {-1:'(-51*G**3)/(128.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_295_251 = Coupling(name = 'UVGC_295_251',
                        value = {-1:'( 0 if Msd else G**3/(64.*cmath.pi**2) )',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_295_252 = Coupling(name = 'UVGC_295_252',
                        value = {-1:'( 0 if Msu else G**3/(64.*cmath.pi**2) )',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_295_253 = Coupling(name = 'UVGC_295_253',
                        value = {-1:'( 0 if MT else G**3/(16.*cmath.pi**2) ) - G**3/(24.*cmath.pi**2)',0:'( (G**3*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':3})

UVGC_296_254 = Coupling(name = 'UVGC_296_254',
                        value = {-1:'(-33*G**3)/(128.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_296_255 = Coupling(name = 'UVGC_296_255',
                        value = {-1:'(-3*G**3)/(128.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_296_256 = Coupling(name = 'UVGC_296_256',
                        value = {-1:'( 0 if Msd else G**3/(64.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_296_257 = Coupling(name = 'UVGC_296_257',
                        value = {-1:'( 0 if Msu else G**3/(64.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)',0:'( G**3/(96.*cmath.pi**2) + (G**3*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else G**3/(96.*cmath.pi**2) ) - G**3/(96.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_297_258 = Coupling(name = 'UVGC_297_258',
                        value = {-1:'(-21*G**3)/(64.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_297_259 = Coupling(name = 'UVGC_297_259',
                        value = {-1:'-G**3/(64.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_298_260 = Coupling(name = 'UVGC_298_260',
                        value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':4})

UVGC_298_261 = Coupling(name = 'UVGC_298_261',
                        value = {-1:'(147*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_298_262 = Coupling(name = 'UVGC_298_262',
                        value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_298_263 = Coupling(name = 'UVGC_298_263',
                        value = {-1:'( 0 if Msd else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_298_264 = Coupling(name = 'UVGC_298_264',
                        value = {-1:'( 0 if Msu else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_298_265 = Coupling(name = 'UVGC_298_265',
                        value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_299_266 = Coupling(name = 'UVGC_299_266',
                        value = {-1:'(147*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_299_267 = Coupling(name = 'UVGC_299_267',
                        value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_299_268 = Coupling(name = 'UVGC_299_268',
                        value = {-1:'( 0 if Msd else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (5*complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_299_269 = Coupling(name = 'UVGC_299_269',
                        value = {-1:'( 0 if Msu else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (5*complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_300_270 = Coupling(name = 'UVGC_300_270',
                        value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':4})

UVGC_300_271 = Coupling(name = 'UVGC_300_271',
                        value = {-1:'-(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_300_272 = Coupling(name = 'UVGC_300_272',
                        value = {-1:'(523*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_300_273 = Coupling(name = 'UVGC_300_273',
                        value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_300_274 = Coupling(name = 'UVGC_300_274',
                        value = {-1:'( 0 if Msd else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_300_275 = Coupling(name = 'UVGC_300_275',
                        value = {-1:'( 0 if Msu else -(complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( -(complex(0,1)*G**4)/(96.*cmath.pi**2) - (complex(0,1)*G**4*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else -(complex(0,1)*G**4)/(96.*cmath.pi**2) ) + (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_300_276 = Coupling(name = 'UVGC_300_276',
                        value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_301_277 = Coupling(name = 'UVGC_301_277',
                        value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':4})

UVGC_301_278 = Coupling(name = 'UVGC_301_278',
                        value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_301_279 = Coupling(name = 'UVGC_301_279',
                        value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_301_280 = Coupling(name = 'UVGC_301_280',
                        value = {-1:'( 0 if Msd else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_301_281 = Coupling(name = 'UVGC_301_281',
                        value = {-1:'( 0 if Msu else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_301_282 = Coupling(name = 'UVGC_301_282',
                        value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_302_283 = Coupling(name = 'UVGC_302_283',
                        value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_302_284 = Coupling(name = 'UVGC_302_284',
                        value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_303_285 = Coupling(name = 'UVGC_303_285',
                        value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':4})

UVGC_303_286 = Coupling(name = 'UVGC_303_286',
                        value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_303_287 = Coupling(name = 'UVGC_303_287',
                        value = {-1:'(-85*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_303_288 = Coupling(name = 'UVGC_303_288',
                        value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_303_289 = Coupling(name = 'UVGC_303_289',
                        value = {-1:'( 0 if Msd else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Msd/MU_R))/(48.*cmath.pi**2) if Msd else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_303_290 = Coupling(name = 'UVGC_303_290',
                        value = {-1:'( 0 if Msu else (complex(0,1)*G**4)/(48.*cmath.pi**2) ) + (complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'( (complex(0,1)*G**4)/(96.*cmath.pi**2) + (complex(0,1)*G**4*reglog(Msu/MU_R))/(48.*cmath.pi**2) if Msu else (complex(0,1)*G**4)/(96.*cmath.pi**2) ) - (complex(0,1)*G**4)/(96.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_303_291 = Coupling(name = 'UVGC_303_291',
                        value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_304_292 = Coupling(name = 'UVGC_304_292',
                        value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_305_293 = Coupling(name = 'UVGC_305_293',
                        value = {-1:'( -(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) if MT else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2)/(18.*cmath.pi**2) ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_306_294 = Coupling(name = 'UVGC_306_294',
                        value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_307_295 = Coupling(name = 'UVGC_307_295',
                        value = {-1:'( -(complex(0,1)*G**2*gstEL)/(12.*cmath.pi**2) if MT else (complex(0,1)*G**2*gstEL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gstEL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gstEL*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gstEL)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gstEL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_308_296 = Coupling(name = 'UVGC_308_296',
                        value = {-1:'( -(complex(0,1)*G**2*gstER)/(12.*cmath.pi**2) if MT else (complex(0,1)*G**2*gstER)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gstER)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gstER*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gstER)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gstER)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_309_297 = Coupling(name = 'UVGC_309_297',
                        value = {-1:'( -(complex(0,1)*G**2*gstML)/(12.*cmath.pi**2) if MT else (complex(0,1)*G**2*gstML)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gstML)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gstML*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gstML)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gstML)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_310_298 = Coupling(name = 'UVGC_310_298',
                        value = {-1:'( -(complex(0,1)*G**2*gstMR)/(12.*cmath.pi**2) if MT else (complex(0,1)*G**2*gstMR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gstMR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gstMR*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gstMR)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gstMR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_311_299 = Coupling(name = 'UVGC_311_299',
                        value = {-1:'( -(complex(0,1)*G**2*gstTL)/(12.*cmath.pi**2) if MT else (complex(0,1)*G**2*gstTL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gstTL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gstTL*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gstTL)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gstTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_312_300 = Coupling(name = 'UVGC_312_300',
                        value = {-1:'( -(complex(0,1)*G**2*gstTR)/(12.*cmath.pi**2) if MT else (complex(0,1)*G**2*gstTR)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gstTR)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gstTR*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gstTR)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gstTR)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_313_301 = Coupling(name = 'UVGC_313_301',
                        value = {-1:'( -(complex(0,1)*G**2*gstVEL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gstVEL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gstVEL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gstVEL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gstVEL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_313_302 = Coupling(name = 'UVGC_313_302',
                        value = {-1:'( -(complex(0,1)*G**2*gstVEL)/(12.*cmath.pi**2) if MT else (complex(0,1)*G**2*gstVEL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gstVEL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gstVEL*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gstVEL)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gstVEL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_313_303 = Coupling(name = 'UVGC_313_303',
                        value = {-1:'-(complex(0,1)*G**2*gstVEL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_314_304 = Coupling(name = 'UVGC_314_304',
                        value = {-1:'( -(complex(0,1)*G**2*gstVML)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gstVML)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gstVML)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gstVML)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gstVML)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_314_305 = Coupling(name = 'UVGC_314_305',
                        value = {-1:'( -(complex(0,1)*G**2*gstVML)/(12.*cmath.pi**2) if MT else (complex(0,1)*G**2*gstVML)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gstVML)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gstVML*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gstVML)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gstVML)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_314_306 = Coupling(name = 'UVGC_314_306',
                        value = {-1:'-(complex(0,1)*G**2*gstVML)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_315_307 = Coupling(name = 'UVGC_315_307',
                        value = {-1:'( -(complex(0,1)*G**2*gstVTL)/(12.*cmath.pi**2) if Msu else -(complex(0,1)*G**2*gstVTL)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*gstVTL)/(144.*cmath.pi**2) if Msu else (complex(0,1)*G**2*gstVTL)/(144.*cmath.pi**2) ) - (complex(0,1)*G**2*gstVTL)/(144.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_315_308 = Coupling(name = 'UVGC_315_308',
                        value = {-1:'( -(complex(0,1)*G**2*gstVTL)/(12.*cmath.pi**2) if MT else (complex(0,1)*G**2*gstVTL)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*gstVTL)/(24.*cmath.pi**2) + (complex(0,1)*G**2*gstVTL*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(complex(0,1)*G**2*gstVTL)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*gstVTL)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_315_309 = Coupling(name = 'UVGC_315_309',
                        value = {-1:'-(complex(0,1)*G**2*gstVTL)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QLD':1})

UVGC_316_310 = Coupling(name = 'UVGC_316_310',
                        value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MT)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2 if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_317_311 = Coupling(name = 'UVGC_317_311',
                        value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_317_312 = Coupling(name = 'UVGC_317_312',
                        value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_318_313 = Coupling(name = 'UVGC_318_313',
                        value = {-1:'( -(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MT else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (-5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) if MT else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_319_314 = Coupling(name = 'UVGC_319_314',
                        value = {-1:'( (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_320_315 = Coupling(name = 'UVGC_320_315',
                        value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MB else -(G**2*yb)/(24.*cmath.pi**2) )',0:'( (13*G**2*yb)/(24.*cmath.pi**2) - (3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yb)/(24.*cmath.pi**2) ) - (G**2*yb)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_320_316 = Coupling(name = 'UVGC_320_316',
                        value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MT else -(G**2*yb)/(24.*cmath.pi**2) )',0:'( (5*G**2*yb)/(24.*cmath.pi**2) - (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yb)/(24.*cmath.pi**2) ) - (G**2*yb)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_320_317 = Coupling(name = 'UVGC_320_317',
                        value = {-1:'(G**2*yb)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_321_318 = Coupling(name = 'UVGC_321_318',
                        value = {-1:'( -(G**2*yb)/(12.*cmath.pi**2) if MB else (G**2*yb)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yb)/(24.*cmath.pi**2) + (3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yb)/(24.*cmath.pi**2) ) + (G**2*yb)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_321_319 = Coupling(name = 'UVGC_321_319',
                        value = {-1:'( -(G**2*yb)/(12.*cmath.pi**2) if MT else (G**2*yb)/(24.*cmath.pi**2) )',0:'( (-5*G**2*yb)/(24.*cmath.pi**2) + (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yb)/(24.*cmath.pi**2) ) + (G**2*yb)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_321_320 = Coupling(name = 'UVGC_321_320',
                        value = {-1:'-(G**2*yb)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_322_321 = Coupling(name = 'UVGC_322_321',
                        value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MB else -(G**2*yt)/(24.*cmath.pi**2) )',0:'( (5*G**2*yt)/(24.*cmath.pi**2) - (G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_322_322 = Coupling(name = 'UVGC_322_322',
                        value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) )',0:'( (13*G**2*yt)/(24.*cmath.pi**2) - (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_322_323 = Coupling(name = 'UVGC_322_323',
                        value = {-1:'(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_323_324 = Coupling(name = 'UVGC_323_324',
                        value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MB else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-5*G**2*yt)/(24.*cmath.pi**2) + (G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yt)/(24.*cmath.pi**2) ) + (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_323_325 = Coupling(name = 'UVGC_323_325',
                        value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yt)/(24.*cmath.pi**2) + (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) ) + (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_323_326 = Coupling(name = 'UVGC_323_326',
                        value = {-1:'-(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_324_327 = Coupling(name = 'UVGC_324_327',
                        value = {-1:'( (G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_325_328 = Coupling(name = 'UVGC_325_328',
                        value = {-1:'( (complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

