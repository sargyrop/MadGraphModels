Requestor: Martina Javurkova
Content: SM model with Z bosons in different polarization states
Source: Provided by Richard Ruiz [rruiz@ifj.edu.pl], MadGraph author
Paper: https://arxiv.org/abs/1912.01725
JIRA: https://its.cern.ch/jira/browse/AGENE-2175
