Requestor: Hannes Mildner
Contents: EFT model implementing operators affecting TGCs in HISZ basis
Webpage: https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/Models/EWdim6
Source: http://madgraph.physics.illinois.edu/Downloads/models/EWdim6.tgz
JIRA: https://its.cern.ch/jira/browse/AGENE-1851
