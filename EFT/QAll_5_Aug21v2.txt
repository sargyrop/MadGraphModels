Requestor: Karolos Potamianos
Content: August 2021 version of Eboli model: two new quartic operators added (T3 and T4) previously not considered. Supersedes SM_LSMT_Ind5_UFOv2 and SM_Ltotal_Ind5v2020v2_UFO.
Original URL: https://feynrules.irmp.ucl.ac.be/raw-attachment/wiki/AnomalousGaugeCoupling/quartic21v2.tgz
Website: https://feynrules.irmp.ucl.ac.be/wiki/AnomalousGaugeCoupling
