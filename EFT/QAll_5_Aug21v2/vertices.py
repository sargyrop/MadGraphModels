# This file was automatically created by FeynRules 2.3.47
# Mathematica version: 12.0.0 for Mac OS X x86 (64-bit) (April 7, 2019)
# Date: Thu 19 Aug 2021 11:22:49


from object_library import all_vertices, Vertex
import particles as P
import couplings as C
import lorentz as L


V_1 = Vertex(name = 'V_1',
             particles = [ P.H, P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1, L.SSSS2 ],
             couplings = {(0,0):C.GC_98,(0,1):C.GC_31})

V_2 = Vertex(name = 'V_2',
             particles = [ P.H, P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS2 ],
             couplings = {(0,0):C.GC_32})

V_3 = Vertex(name = 'V_3',
             particles = [ P.H, P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS2 ],
             couplings = {(0,0):C.GC_33})

V_4 = Vertex(name = 'V_4',
             particles = [ P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSS1 ],
             couplings = {(0,0):C.GC_473})

V_5 = Vertex(name = 'V_5',
             particles = [ P.ghG, P.ghG__tilde__, P.g ],
             color = [ 'f(1,2,3)' ],
             lorentz = [ L.UUV1 ],
             couplings = {(0,0):C.GC_95})

V_6 = Vertex(name = 'V_6',
             particles = [ P.g, P.g, P.g ],
             color = [ 'f(1,2,3)' ],
             lorentz = [ L.VVV1 ],
             couplings = {(0,0):C.GC_95})

V_7 = Vertex(name = 'V_7',
             particles = [ P.g, P.g, P.g, P.g ],
             color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
             lorentz = [ L.VVVV13, L.VVVV14, L.VVVV3 ],
             couplings = {(1,0):C.GC_97,(0,2):C.GC_97,(2,1):C.GC_97})

V_8 = Vertex(name = 'V_8',
             particles = [ P.ta__plus__, P.ta__minus__, P.H ],
             color = [ '1' ],
             lorentz = [ L.FFS1 ],
             couplings = {(0,0):C.GC_755})

V_9 = Vertex(name = 'V_9',
             particles = [ P.t__tilde__, P.t, P.H ],
             color = [ 'Identity(1,2)' ],
             lorentz = [ L.FFS1 ],
             couplings = {(0,0):C.GC_754})

V_10 = Vertex(name = 'V_10',
              particles = [ P.a, P.W__minus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVV1 ],
              couplings = {(0,0):C.GC_4})

V_11 = Vertex(name = 'V_11',
              particles = [ P.W__minus__, P.W__plus__, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3, L.VVSS5, L.VVSS6 ],
              couplings = {(0,0):C.GC_602,(0,1):C.GC_159,(0,3):C.GC_10,(0,4):C.GC_6,(0,2):C.GC_603})

V_12 = Vertex(name = 'V_12',
              particles = [ P.W__minus__, P.W__plus__, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS5 ],
              couplings = {(0,0):C.GC_604,(0,1):C.GC_24})

V_13 = Vertex(name = 'V_13',
              particles = [ P.W__minus__, P.W__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_494})

V_14 = Vertex(name = 'V_14',
              particles = [ P.a, P.a, P.W__minus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVVV12, L.VVVV17, L.VVVV38, L.VVVV40, L.VVVV5, L.VVVV7, L.VVVV8 ],
              couplings = {(0,6):C.GC_81,(0,5):C.GC_70,(0,2):C.GC_86,(0,3):C.GC_75,(0,4):C.GC_549,(0,0):C.GC_5,(0,1):C.GC_548})

V_15 = Vertex(name = 'V_15',
              particles = [ P.a, P.a, P.W__minus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVVV17, L.VVVV38, L.VVVV40, L.VVVV5, L.VVVV7, L.VVVV8 ],
              couplings = {(0,5):C.GC_356,(0,4):C.GC_364,(0,1):C.GC_360,(0,2):C.GC_352,(0,3):C.GC_556,(0,0):C.GC_588})

V_16 = Vertex(name = 'V_16',
              particles = [ P.a, P.a, P.W__minus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVVV17, L.VVVV5 ],
              couplings = {(0,1):C.GC_589,(0,0):C.GC_610})

V_17 = Vertex(name = 'V_17',
              particles = [ P.a, P.a, P.W__minus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVVV5 ],
              couplings = {(0,0):C.GC_613})

V_18 = Vertex(name = 'V_18',
              particles = [ P.W__minus__, P.W__plus__, P.Z ],
              color = [ '1' ],
              lorentz = [ L.VVV1 ],
              couplings = {(0,0):C.GC_230})

V_19 = Vertex(name = 'V_19',
              particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVVV12, L.VVVV25, L.VVVV26, L.VVVV36, L.VVVV38, L.VVVV4, L.VVVV43, L.VVVV6, L.VVVV8, L.VVVV9 ],
              couplings = {(0,8):C.GC_34,(0,6):C.GC_43,(0,3):C.GC_52,(0,4):C.GC_61,(0,2):C.GC_584,(0,1):C.GC_596,(0,7):C.GC_580,(0,0):C.GC_160,(0,5):C.GC_633,(0,9):C.GC_632})

V_20 = Vertex(name = 'V_20',
              particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVVV4 ],
              couplings = {(0,0):C.GC_634})

V_21 = Vertex(name = 'V_21',
              particles = [ P.a, P.W__minus__, P.W__plus__, P.Z ],
              color = [ '1' ],
              lorentz = [ L.VVVV1, L.VVVV15, L.VVVV18, L.VVVV19, L.VVVV2, L.VVVV20, L.VVVV21, L.VVVV22, L.VVVV23, L.VVVV24, L.VVVV35, L.VVVV37, L.VVVV41 ],
              couplings = {(0,4):C.GC_307,(0,10):C.GC_315,(0,12):C.GC_311,(0,11):C.GC_319,(0,9):C.GC_554,(0,8):C.GC_593,(0,7):C.GC_615,(0,5):C.GC_623,(0,6):C.GC_606,(0,2):C.GC_552,(0,3):C.GC_590,(0,0):C.GC_605,(0,1):C.GC_231})

V_22 = Vertex(name = 'V_22',
              particles = [ P.a, P.W__minus__, P.W__plus__, P.Z ],
              color = [ '1' ],
              lorentz = [ L.VVVV1, L.VVVV2, L.VVVV21, L.VVVV35, L.VVVV37, L.VVVV41 ],
              couplings = {(0,1):C.GC_327,(0,3):C.GC_336,(0,5):C.GC_332,(0,4):C.GC_323,(0,2):C.GC_608,(0,0):C.GC_607})

V_23 = Vertex(name = 'V_23',
              particles = [ P.Z, P.Z, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS2, L.VVSS3, L.VVSS4, L.VVSS5, L.VVSS6 ],
              couplings = {(0,2):C.GC_718,(0,0):C.GC_409,(0,3):C.GC_11,(0,4):C.GC_7,(0,1):C.GC_719})

V_24 = Vertex(name = 'V_24',
              particles = [ P.Z, P.Z, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS3, L.VVSS5, L.VVSS6 ],
              couplings = {(0,1):C.GC_25,(0,2):C.GC_295,(0,0):C.GC_720})

V_25 = Vertex(name = 'V_25',
              particles = [ P.Z, P.Z, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS5, L.VVSS6 ],
              couplings = {(0,0):C.GC_300,(0,1):C.GC_345})

V_26 = Vertex(name = 'V_26',
              particles = [ P.Z, P.Z, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS5 ],
              couplings = {(0,0):C.GC_346})

V_27 = Vertex(name = 'V_27',
              particles = [ P.Z, P.Z, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_653})

V_28 = Vertex(name = 'V_28',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z ],
              color = [ '1' ],
              lorentz = [ L.VVVV10, L.VVVV11, L.VVVV12, L.VVVV17, L.VVVV26, L.VVVV27, L.VVVV28, L.VVVV29, L.VVVV31, L.VVVV32, L.VVVV33, L.VVVV38, L.VVVV4, L.VVVV40, L.VVVV5, L.VVVV6, L.VVVV7, L.VVVV8, L.VVVV9 ],
              couplings = {(0,17):C.GC_44,(0,16):C.GC_62,(0,11):C.GC_53,(0,13):C.GC_35,(0,14):C.GC_711,(0,7):C.GC_557,(0,1):C.GC_585,(0,5):C.GC_597,(0,6):C.GC_612,(0,4):C.GC_621,(0,8):C.GC_550,(0,10):C.GC_581,(0,9):C.GC_609,(0,15):C.GC_619,(0,0):C.GC_551,(0,2):C.GC_161,(0,12):C.GC_748,(0,3):C.GC_707,(0,18):C.GC_749})

V_29 = Vertex(name = 'V_29',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z ],
              color = [ '1' ],
              lorentz = [ L.VVVV38, L.VVVV4, L.VVVV40, L.VVVV5, L.VVVV7, L.VVVV8 ],
              couplings = {(0,5):C.GC_378,(0,4):C.GC_368,(0,0):C.GC_383,(0,2):C.GC_373,(0,3):C.GC_627,(0,1):C.GC_750})

V_30 = Vertex(name = 'V_30',
              particles = [ P.e__plus__, P.e__minus__, P.a ],
              color = [ '1' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

V_31 = Vertex(name = 'V_31',
              particles = [ P.mu__plus__, P.mu__minus__, P.a ],
              color = [ '1' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

V_32 = Vertex(name = 'V_32',
              particles = [ P.ta__plus__, P.ta__minus__, P.a ],
              color = [ '1' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

V_33 = Vertex(name = 'V_33',
              particles = [ P.c__tilde__, P.c, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_2})

V_34 = Vertex(name = 'V_34',
              particles = [ P.t__tilde__, P.t, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_2})

V_35 = Vertex(name = 'V_35',
              particles = [ P.u__tilde__, P.u, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_2})

V_36 = Vertex(name = 'V_36',
              particles = [ P.b__tilde__, P.b, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_1})

V_37 = Vertex(name = 'V_37',
              particles = [ P.d__tilde__, P.d, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_1})

V_38 = Vertex(name = 'V_38',
              particles = [ P.s__tilde__, P.s, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_1})

V_39 = Vertex(name = 'V_39',
              particles = [ P.c__tilde__, P.c, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_96})

V_40 = Vertex(name = 'V_40',
              particles = [ P.t__tilde__, P.t, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_96})

V_41 = Vertex(name = 'V_41',
              particles = [ P.u__tilde__, P.u, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_96})

V_42 = Vertex(name = 'V_42',
              particles = [ P.b__tilde__, P.b, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_96})

V_43 = Vertex(name = 'V_43',
              particles = [ P.d__tilde__, P.d, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_96})

V_44 = Vertex(name = 'V_44',
              particles = [ P.s__tilde__, P.s, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_96})

V_45 = Vertex(name = 'V_45',
              particles = [ P.d__tilde__, P.c, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_226})

V_46 = Vertex(name = 'V_46',
              particles = [ P.s__tilde__, P.c, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_227})

V_47 = Vertex(name = 'V_47',
              particles = [ P.b__tilde__, P.t, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_223})

V_48 = Vertex(name = 'V_48',
              particles = [ P.d__tilde__, P.u, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_224})

V_49 = Vertex(name = 'V_49',
              particles = [ P.s__tilde__, P.u, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_225})

V_50 = Vertex(name = 'V_50',
              particles = [ P.t__tilde__, P.b, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_223})

V_51 = Vertex(name = 'V_51',
              particles = [ P.c__tilde__, P.d, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_758})

V_52 = Vertex(name = 'V_52',
              particles = [ P.u__tilde__, P.d, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_756})

V_53 = Vertex(name = 'V_53',
              particles = [ P.c__tilde__, P.s, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_759})

V_54 = Vertex(name = 'V_54',
              particles = [ P.u__tilde__, P.s, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_757})

V_55 = Vertex(name = 'V_55',
              particles = [ P.e__plus__, P.ve, P.W__minus__ ],
              color = [ '1' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_223})

V_56 = Vertex(name = 'V_56',
              particles = [ P.mu__plus__, P.vm, P.W__minus__ ],
              color = [ '1' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_223})

V_57 = Vertex(name = 'V_57',
              particles = [ P.ta__plus__, P.vt, P.W__minus__ ],
              color = [ '1' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_223})

V_58 = Vertex(name = 'V_58',
              particles = [ P.ve__tilde__, P.e__minus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_223})

V_59 = Vertex(name = 'V_59',
              particles = [ P.vm__tilde__, P.mu__minus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_223})

V_60 = Vertex(name = 'V_60',
              particles = [ P.vt__tilde__, P.ta__minus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_223})

V_61 = Vertex(name = 'V_61',
              particles = [ P.c__tilde__, P.c, P.Z ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV5 ],
              couplings = {(0,0):C.GC_229,(0,1):C.GC_289})

V_62 = Vertex(name = 'V_62',
              particles = [ P.t__tilde__, P.t, P.Z ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV5 ],
              couplings = {(0,0):C.GC_229,(0,1):C.GC_289})

V_63 = Vertex(name = 'V_63',
              particles = [ P.u__tilde__, P.u, P.Z ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV5 ],
              couplings = {(0,0):C.GC_229,(0,1):C.GC_289})

V_64 = Vertex(name = 'V_64',
              particles = [ P.b__tilde__, P.b, P.Z ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3 ],
              couplings = {(0,0):C.GC_228,(0,1):C.GC_289})

V_65 = Vertex(name = 'V_65',
              particles = [ P.d__tilde__, P.d, P.Z ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3 ],
              couplings = {(0,0):C.GC_228,(0,1):C.GC_289})

V_66 = Vertex(name = 'V_66',
              particles = [ P.s__tilde__, P.s, P.Z ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3 ],
              couplings = {(0,0):C.GC_228,(0,1):C.GC_289})

V_67 = Vertex(name = 'V_67',
              particles = [ P.ve__tilde__, P.ve, P.Z ],
              color = [ '1' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_402})

V_68 = Vertex(name = 'V_68',
              particles = [ P.vm__tilde__, P.vm, P.Z ],
              color = [ '1' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_402})

V_69 = Vertex(name = 'V_69',
              particles = [ P.vt__tilde__, P.vt, P.Z ],
              color = [ '1' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_402})

V_70 = Vertex(name = 'V_70',
              particles = [ P.e__plus__, P.e__minus__, P.Z ],
              color = [ '1' ],
              lorentz = [ L.FFV2, L.FFV4 ],
              couplings = {(0,0):C.GC_228,(0,1):C.GC_290})

V_71 = Vertex(name = 'V_71',
              particles = [ P.mu__plus__, P.mu__minus__, P.Z ],
              color = [ '1' ],
              lorentz = [ L.FFV2, L.FFV4 ],
              couplings = {(0,0):C.GC_228,(0,1):C.GC_290})

V_72 = Vertex(name = 'V_72',
              particles = [ P.ta__plus__, P.ta__minus__, P.Z ],
              color = [ '1' ],
              lorentz = [ L.FFV2, L.FFV4 ],
              couplings = {(0,0):C.GC_228,(0,1):C.GC_290})

V_73 = Vertex(name = 'V_73',
              particles = [ P.W__minus__, P.W__plus__, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSSSS1, L.VVSSSS3 ],
              couplings = {(0,0):C.GC_184,(0,1):C.GC_185})

V_74 = Vertex(name = 'V_74',
              particles = [ P.W__minus__, P.W__plus__, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSSSS1 ],
              couplings = {(0,0):C.GC_186})

V_75 = Vertex(name = 'V_75',
              particles = [ P.W__minus__, P.W__plus__, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSSS1, L.VVSSS3 ],
              couplings = {(0,0):C.GC_517,(0,1):C.GC_518})

V_76 = Vertex(name = 'V_76',
              particles = [ P.W__minus__, P.W__plus__, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSSS1 ],
              couplings = {(0,0):C.GC_519})

V_77 = Vertex(name = 'V_77',
              particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVVSSSS1, L.VVVVSSSS2 ],
              couplings = {(0,0):C.GC_118,(0,1):C.GC_117})

V_78 = Vertex(name = 'V_78',
              particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVVSSSS1 ],
              couplings = {(0,0):C.GC_119})

V_79 = Vertex(name = 'V_79',
              particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVVSSS1, L.VVVVSSS2 ],
              couplings = {(0,0):C.GC_481,(0,1):C.GC_480})

V_80 = Vertex(name = 'V_80',
              particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVVSSS1 ],
              couplings = {(0,0):C.GC_482})

V_81 = Vertex(name = 'V_81',
              particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVVSS19, L.VVVVSS2, L.VVVVSS32, L.VVVVSS37, L.VVVVSS5 ],
              couplings = {(0,0):C.GC_166,(0,2):C.GC_178,(0,3):C.GC_162,(0,1):C.GC_567,(0,4):C.GC_566})

V_82 = Vertex(name = 'V_82',
              particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVVSS2 ],
              couplings = {(0,0):C.GC_568})

V_83 = Vertex(name = 'V_83',
              particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVVS16, L.VVVVS2, L.VVVVS30, L.VVVVS4, L.VVVVS5 ],
              couplings = {(0,0):C.GC_499,(0,2):C.GC_511,(0,3):C.GC_495,(0,1):C.GC_630,(0,4):C.GC_629})

V_84 = Vertex(name = 'V_84',
              particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVVS2 ],
              couplings = {(0,0):C.GC_631})

V_85 = Vertex(name = 'V_85',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVSSSS1 ],
              couplings = {(0,0):C.GC_109})

V_86 = Vertex(name = 'V_86',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVSSSS1 ],
              couplings = {(0,0):C.GC_110})

V_87 = Vertex(name = 'V_87',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVSSS1 ],
              couplings = {(0,0):C.GC_645})

V_88 = Vertex(name = 'V_88',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVSSS1 ],
              couplings = {(0,0):C.GC_646})

V_89 = Vertex(name = 'V_89',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVSS10, L.VVVSS2, L.VVVSS3, L.VVVSS5, L.VVVSS6, L.VVVSS8, L.VVVSS9 ],
              couplings = {(0,5):C.GC_246,(0,2):C.GC_304,(0,3):C.GC_21,(0,0):C.GC_18,(0,4):C.GC_234,(0,1):C.GC_698,(0,6):C.GC_232})

V_90 = Vertex(name = 'V_90',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVSS2 ],
              couplings = {(0,0):C.GC_699})

V_91 = Vertex(name = 'V_91',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVS2, L.VVVS3, L.VVVS4, L.VVVS7, L.VVVS8 ],
              couplings = {(0,3):C.GC_532,(0,2):C.GC_541,(0,4):C.GC_465,(0,1):C.GC_462,(0,0):C.GC_743})

V_92 = Vertex(name = 'V_92',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVS2 ],
              couplings = {(0,0):C.GC_744})

V_93 = Vertex(name = 'V_93',
              particles = [ P.Z, P.Z, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSSSS2, L.VVSSSS3 ],
              couplings = {(0,0):C.GC_424,(0,1):C.GC_425})

V_94 = Vertex(name = 'V_94',
              particles = [ P.Z, P.Z, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSSSS3 ],
              couplings = {(0,0):C.GC_426})

V_95 = Vertex(name = 'V_95',
              particles = [ P.Z, P.Z, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSSS2, L.VVSSS3 ],
              couplings = {(0,0):C.GC_666,(0,1):C.GC_667})

V_96 = Vertex(name = 'V_96',
              particles = [ P.Z, P.Z, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSSS3 ],
              couplings = {(0,0):C.GC_668})

V_97 = Vertex(name = 'V_97',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVVSSSS1, L.VVVVSSSS2 ],
              couplings = {(0,0):C.GC_106,(0,1):C.GC_107})

V_98 = Vertex(name = 'V_98',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVVSSSS1 ],
              couplings = {(0,0):C.GC_108})

V_99 = Vertex(name = 'V_99',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVVVSSS1, L.VVVVSSS2 ],
              couplings = {(0,0):C.GC_642,(0,1):C.GC_643})

V_100 = Vertex(name = 'V_100',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.H, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSSS1 ],
               couplings = {(0,0):C.GC_644})

V_101 = Vertex(name = 'V_101',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS11, L.VVVVSS18, L.VVVVSS2, L.VVVVSS21, L.VVVVSS22, L.VVVVSS28, L.VVVVSS29, L.VVVVSS3, L.VVVVSS30, L.VVVVSS35, L.VVVVSS4, L.VVVVSS5, L.VVVVSS6, L.VVVVSS8 ],
               couplings = {(0,7):C.GC_415,(0,5):C.GC_28,(0,13):C.GC_167,(0,8):C.GC_179,(0,6):C.GC_243,(0,1):C.GC_301,(0,3):C.GC_15,(0,9):C.GC_163,(0,4):C.GC_240,(0,10):C.GC_297,(0,12):C.GC_17,(0,2):C.GC_695,(0,0):C.GC_411,(0,11):C.GC_696})

V_102 = Vertex(name = 'V_102',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,1):C.GC_350,(0,0):C.GC_697})

V_103 = Vertex(name = 'V_103',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS10, L.VVVVS16, L.VVVVS18, L.VVVVS19, L.VVVVS2, L.VVVVS20, L.VVVVS26, L.VVVVS27, L.VVVVS28, L.VVVVS3, L.VVVVS4, L.VVVVS5, L.VVVVS6, L.VVVVS7 ],
               couplings = {(0,9):C.GC_659,(0,6):C.GC_470,(0,13):C.GC_500,(0,8):C.GC_512,(0,7):C.GC_529,(0,1):C.GC_539,(0,2):C.GC_460,(0,5):C.GC_496,(0,3):C.GC_526,(0,10):C.GC_537,(0,12):C.GC_461,(0,4):C.GC_740,(0,0):C.GC_655,(0,11):C.GC_741})

V_104 = Vertex(name = 'V_104',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS2, L.VVVVS3 ],
               couplings = {(0,1):C.GC_546,(0,0):C.GC_742})

V_105 = Vertex(name = 'V_105',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H, P.H, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSSSS3 ],
               couplings = {(0,0):C.GC_451})

V_106 = Vertex(name = 'V_106',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H, P.H, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSSSS3 ],
               couplings = {(0,0):C.GC_452})

V_107 = Vertex(name = 'V_107',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H, P.H, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSSSS3 ],
               couplings = {(0,0):C.GC_453})

V_108 = Vertex(name = 'V_108',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSSS3 ],
               couplings = {(0,0):C.GC_685})

V_109 = Vertex(name = 'V_109',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSSS3 ],
               couplings = {(0,0):C.GC_686})

V_110 = Vertex(name = 'V_110',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSSS3 ],
               couplings = {(0,0):C.GC_687})

V_111 = Vertex(name = 'V_111',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS20, L.VVVVSS23, L.VVVVSS9 ],
               couplings = {(0,0):C.GC_414,(0,1):C.GC_410,(0,2):C.GC_737})

V_112 = Vertex(name = 'V_112',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS20, L.VVVVSS23, L.VVVVSS9 ],
               couplings = {(0,0):C.GC_447,(0,1):C.GC_446,(0,2):C.GC_738})

V_113 = Vertex(name = 'V_113',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS20, L.VVVVSS23, L.VVVVSS9 ],
               couplings = {(0,0):C.GC_422,(0,1):C.GC_432,(0,2):C.GC_739})

V_114 = Vertex(name = 'V_114',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS20 ],
               couplings = {(0,0):C.GC_433})

V_115 = Vertex(name = 'V_115',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS17, L.VVVVS21, L.VVVVS8 ],
               couplings = {(0,0):C.GC_658,(0,1):C.GC_654,(0,2):C.GC_745})

V_116 = Vertex(name = 'V_116',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS17, L.VVVVS21, L.VVVVS8 ],
               couplings = {(0,0):C.GC_681,(0,1):C.GC_680,(0,2):C.GC_746})

V_117 = Vertex(name = 'V_117',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS17, L.VVVVS21, L.VVVVS8 ],
               couplings = {(0,0):C.GC_664,(0,1):C.GC_674,(0,2):C.GC_747})

V_118 = Vertex(name = 'V_118',
               particles = [ P.Z, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS17 ],
               couplings = {(0,0):C.GC_675})

V_119 = Vertex(name = 'V_119',
               particles = [ P.Z, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV16, L.VVVV30, L.VVVV34, L.VVVV39, L.VVVV42 ],
               couplings = {(0,4):C.GC_36,(0,3):C.GC_54,(0,1):C.GC_710,(0,2):C.GC_706,(0,0):C.GC_751})

V_120 = Vertex(name = 'V_120',
               particles = [ P.Z, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV16, L.VVVV30, L.VVVV34, L.VVVV39, L.VVVV42 ],
               couplings = {(0,4):C.GC_45,(0,3):C.GC_63,(0,1):C.GC_733,(0,2):C.GC_732,(0,0):C.GC_752})

V_121 = Vertex(name = 'V_121',
               particles = [ P.Z, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV16, L.VVVV30, L.VVVV34, L.VVVV39, L.VVVV42 ],
               couplings = {(0,4):C.GC_375,(0,3):C.GC_370,(0,1):C.GC_716,(0,2):C.GC_726,(0,0):C.GC_753})

V_122 = Vertex(name = 'V_122',
               particles = [ P.Z, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV30, L.VVVV39, L.VVVV42 ],
               couplings = {(0,2):C.GC_380,(0,1):C.GC_385,(0,0):C.GC_727})

V_123 = Vertex(name = 'V_123',
               particles = [ P.Z, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV39, L.VVVV42 ],
               couplings = {(0,1):C.GC_400,(0,0):C.GC_401})

V_124 = Vertex(name = 'V_124',
               particles = [ P.a, P.a, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS5, L.VVSS6 ],
               couplings = {(0,0):C.GC_16,(0,1):C.GC_14})

V_125 = Vertex(name = 'V_125',
               particles = [ P.a, P.a, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS5, L.VVSS6 ],
               couplings = {(0,0):C.GC_299,(0,1):C.GC_296})

V_126 = Vertex(name = 'V_126',
               particles = [ P.a, P.a, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS5, L.VVSS6 ],
               couplings = {(0,0):C.GC_344,(0,1):C.GC_343})

V_127 = Vertex(name = 'V_127',
               particles = [ P.a, P.a, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS5 ],
               couplings = {(0,0):C.GC_349})

V_128 = Vertex(name = 'V_128',
               particles = [ P.a, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS5, L.VVSS6 ],
               couplings = {(0,0):C.GC_421,(0,1):C.GC_420})

V_129 = Vertex(name = 'V_129',
               particles = [ P.a, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS5, L.VVSS6 ],
               couplings = {(0,0):C.GC_292,(0,1):C.GC_291})

V_130 = Vertex(name = 'V_130',
               particles = [ P.a, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS5, L.VVSS6 ],
               couplings = {(0,0):C.GC_294,(0,1):C.GC_293})

V_131 = Vertex(name = 'V_131',
               particles = [ P.a, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS5 ],
               couplings = {(0,0):C.GC_303})

V_132 = Vertex(name = 'V_132',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS4, L.VVVSS6, L.VVVSS7, L.VVVSS9 ],
               couplings = {(0,3):C.GC_26,(0,1):C.GC_242,(0,0):C.GC_238,(0,2):C.GC_12,(0,4):C.GC_8})

V_133 = Vertex(name = 'V_133',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS10, L.VVVVSS11, L.VVVVSS25, L.VVVVSS27, L.VVVVSS3, L.VVVVSS34, L.VVVVSS7 ],
               couplings = {(0,6):C.GC_13,(0,2):C.GC_27,(0,4):C.GC_171,(0,3):C.GC_244,(0,0):C.GC_239,(0,5):C.GC_9,(0,1):C.GC_170})

V_134 = Vertex(name = 'V_134',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS10, L.VVVVS23, L.VVVVS25, L.VVVVS3, L.VVVVS9 ],
               couplings = {(0,3):C.GC_459,(0,1):C.GC_469,(0,2):C.GC_530,(0,4):C.GC_525,(0,0):C.GC_458})

V_135 = Vertex(name = 'V_135',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS10, L.VVVVS3 ],
               couplings = {(0,1):C.GC_504,(0,0):C.GC_503})

V_136 = Vertex(name = 'V_136',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS13, L.VVVVSS14, L.VVVVSS15, L.VVVVSS16, L.VVVVSS24, L.VVVVSS26, L.VVVVSS31, L.VVVVSS33, L.VVVVSS36 ],
               couplings = {(0,8):C.GC_22,(0,5):C.GC_175,(0,7):C.GC_247,(0,6):C.GC_305,(0,4):C.GC_235,(0,3):C.GC_237,(0,1):C.GC_172,(0,2):C.GC_19,(0,9):C.GC_233,(0,0):C.GC_236})

V_137 = Vertex(name = 'V_137',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS1, L.VVVVS12, L.VVVVS13, L.VVVVS14, L.VVVVS22, L.VVVVS24, L.VVVVS29, L.VVVVS31 ],
               couplings = {(0,7):C.GC_466,(0,4):C.GC_508,(0,6):C.GC_533,(0,5):C.GC_542,(0,3):C.GC_521,(0,1):C.GC_505,(0,2):C.GC_463,(0,0):C.GC_520})

V_138 = Vertex(name = 'V_138',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS1, L.VVVVS14 ],
               couplings = {(0,1):C.GC_523,(0,0):C.GC_522})

V_139 = Vertex(name = 'V_139',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVSS1, L.VVVVVSS29, L.VVVVVSS3, L.VVVVVSS30 ],
               couplings = {(0,3):C.GC_168,(0,1):C.GC_180,(0,0):C.GC_136,(0,2):C.GC_164})

V_140 = Vertex(name = 'V_140',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVSS1 ],
               couplings = {(0,0):C.GC_138})

V_141 = Vertex(name = 'V_141',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVS1, L.VVVVVS29, L.VVVVVS3, L.VVVVVS30 ],
               couplings = {(0,3):C.GC_501,(0,1):C.GC_513,(0,0):C.GC_487,(0,2):C.GC_497})

V_142 = Vertex(name = 'V_142',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVS1 ],
               couplings = {(0,0):C.GC_489})

V_143 = Vertex(name = 'V_143',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVV12, L.VVVVV4, L.VVVVV45, L.VVVVV46, L.VVVVV61, L.VVVVV62, L.VVVVV68, L.VVVVV7 ],
               couplings = {(0,4):C.GC_55,(0,5):C.GC_64,(0,0):C.GC_37,(0,6):C.GC_46,(0,3):C.GC_586,(0,2):C.GC_598,(0,1):C.GC_573,(0,7):C.GC_582})

V_144 = Vertex(name = 'V_144',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVV4 ],
               couplings = {(0,0):C.GC_575})

V_145 = Vertex(name = 'V_145',
               particles = [ P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS1, L.VVVVVVSS6, L.VVVVVVSS7 ],
               couplings = {(0,2):C.GC_169,(0,1):C.GC_182,(0,0):C.GC_165})

V_146 = Vertex(name = 'V_146',
               particles = [ P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS1, L.VVVVVVS6, L.VVVVVVS7 ],
               couplings = {(0,2):C.GC_502,(0,1):C.GC_515,(0,0):C.GC_498})

V_147 = Vertex(name = 'V_147',
               particles = [ P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV1, L.VVVVVV16, L.VVVVVV26, L.VVVVVV27, L.VVVVVV39, L.VVVVVV40, L.VVVVVV55, L.VVVVVV56, L.VVVVVV69, L.VVVVVV8 ],
               couplings = {(0,5):C.GC_39,(0,8):C.GC_48,(0,7):C.GC_57,(0,6):C.GC_66,(0,0):C.GC_215,(0,1):C.GC_221,(0,3):C.GC_587,(0,2):C.GC_600,(0,9):C.GC_583,(0,4):C.GC_217})

V_148 = Vertex(name = 'V_148',
               particles = [ P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV1 ],
               couplings = {(0,0):C.GC_219})

V_149 = Vertex(name = 'V_149',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVSS17, L.VVVVVSS18, L.VVVVVSS19, L.VVVVVSS20, L.VVVVVSS28 ],
               couplings = {(0,3):C.GC_132,(0,1):C.GC_134,(0,4):C.GC_140,(0,0):C.GC_248,(0,2):C.GC_173})

V_150 = Vertex(name = 'V_150',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVSS19 ],
               couplings = {(0,0):C.GC_176})

V_151 = Vertex(name = 'V_151',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVS17, L.VVVVVS18, L.VVVVVS19, L.VVVVVS20, L.VVVVVS28 ],
               couplings = {(0,3):C.GC_483,(0,1):C.GC_485,(0,4):C.GC_491,(0,0):C.GC_534,(0,2):C.GC_506})

V_152 = Vertex(name = 'V_152',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVS19 ],
               couplings = {(0,0):C.GC_509})

V_153 = Vertex(name = 'V_153',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVV33, L.VVVVV34, L.VVVVV35, L.VVVVV36, L.VVVVV44, L.VVVVV47, L.VVVVV54, L.VVVVV55, L.VVVVV66 ],
               couplings = {(0,6):C.GC_263,(0,7):C.GC_269,(0,8):C.GC_257,(0,5):C.GC_251,(0,3):C.GC_569,(0,1):C.GC_571,(0,4):C.GC_577,(0,0):C.GC_616,(0,2):C.GC_591})

V_154 = Vertex(name = 'V_154',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVV35 ],
               couplings = {(0,0):C.GC_594})

V_155 = Vertex(name = 'V_155',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS12 ],
               couplings = {(0,0):C.GC_111})

V_156 = Vertex(name = 'V_156',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS12 ],
               couplings = {(0,0):C.GC_113})

V_157 = Vertex(name = 'V_157',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS12 ],
               couplings = {(0,0):C.GC_115})

V_158 = Vertex(name = 'V_158',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS12 ],
               couplings = {(0,0):C.GC_474})

V_159 = Vertex(name = 'V_159',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS12 ],
               couplings = {(0,0):C.GC_476})

V_160 = Vertex(name = 'V_160',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS12 ],
               couplings = {(0,0):C.GC_478})

V_161 = Vertex(name = 'V_161',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV19, L.VVVVVV32, L.VVVVVV45, L.VVVVVV54 ],
               couplings = {(0,3):C.GC_201,(0,0):C.GC_194,(0,2):C.GC_187,(0,1):C.GC_560})

V_162 = Vertex(name = 'V_162',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV19, L.VVVVVV32 ],
               couplings = {(0,0):C.GC_208,(0,1):C.GC_562})

V_163 = Vertex(name = 'V_163',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV32 ],
               couplings = {(0,0):C.GC_564})

V_164 = Vertex(name = 'V_164',
               particles = [ P.a, P.a, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS11, L.VVVVSS3 ],
               couplings = {(0,1):C.GC_445,(0,0):C.GC_444})

V_165 = Vertex(name = 'V_165',
               particles = [ P.a, P.a, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS11, L.VVVVSS3 ],
               couplings = {(0,1):C.GC_419,(0,0):C.GC_418})

V_166 = Vertex(name = 'V_166',
               particles = [ P.a, P.a, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS11, L.VVVVSS3 ],
               couplings = {(0,1):C.GC_450,(0,0):C.GC_431})

V_167 = Vertex(name = 'V_167',
               particles = [ P.a, P.a, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS3 ],
               couplings = {(0,0):C.GC_434})

V_168 = Vertex(name = 'V_168',
               particles = [ P.a, P.a, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS10, L.VVVVS3 ],
               couplings = {(0,1):C.GC_679,(0,0):C.GC_678})

V_169 = Vertex(name = 'V_169',
               particles = [ P.a, P.a, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS10, L.VVVVS3 ],
               couplings = {(0,1):C.GC_663,(0,0):C.GC_662})

V_170 = Vertex(name = 'V_170',
               particles = [ P.a, P.a, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS10, L.VVVVS3 ],
               couplings = {(0,1):C.GC_684,(0,0):C.GC_673})

V_171 = Vertex(name = 'V_171',
               particles = [ P.a, P.a, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS3 ],
               couplings = {(0,0):C.GC_676})

V_172 = Vertex(name = 'V_172',
               particles = [ P.a, P.a, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV17, L.VVVV36, L.VVVV38, L.VVVV39, L.VVVV40, L.VVVV42, L.VVVV43, L.VVVV5, L.VVVV7, L.VVVV8 ],
               couplings = {(0,9):C.GC_456,(0,5):C.GC_353,(0,6):C.GC_379,(0,8):C.GC_454,(0,3):C.GC_361,(0,1):C.GC_384,(0,2):C.GC_457,(0,4):C.GC_455,(0,7):C.GC_731,(0,0):C.GC_730})

V_173 = Vertex(name = 'V_173',
               particles = [ P.a, P.a, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV17, L.VVVV38, L.VVVV39, L.VVVV42, L.VVVV5, L.VVVV8 ],
               couplings = {(0,5):C.GC_374,(0,3):C.GC_357,(0,2):C.GC_365,(0,1):C.GC_369,(0,4):C.GC_715,(0,0):C.GC_714})

V_174 = Vertex(name = 'V_174',
               particles = [ P.a, P.a, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV17, L.VVVV39, L.VVVV42, L.VVVV5 ],
               couplings = {(0,2):C.GC_388,(0,1):C.GC_389,(0,3):C.GC_736,(0,0):C.GC_725})

V_175 = Vertex(name = 'V_175',
               particles = [ P.a, P.a, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV5 ],
               couplings = {(0,0):C.GC_728})

V_176 = Vertex(name = 'V_176',
               particles = [ P.a, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS12, L.VVVVSS17 ],
               couplings = {(0,1):C.GC_449,(0,0):C.GC_448})

V_177 = Vertex(name = 'V_177',
               particles = [ P.a, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS12, L.VVVVSS17 ],
               couplings = {(0,1):C.GC_428,(0,0):C.GC_427})

V_178 = Vertex(name = 'V_178',
               particles = [ P.a, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS12, L.VVVVSS17 ],
               couplings = {(0,1):C.GC_430,(0,0):C.GC_429})

V_179 = Vertex(name = 'V_179',
               particles = [ P.a, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS17 ],
               couplings = {(0,0):C.GC_435})

V_180 = Vertex(name = 'V_180',
               particles = [ P.a, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS11, L.VVVVS15 ],
               couplings = {(0,1):C.GC_683,(0,0):C.GC_682})

V_181 = Vertex(name = 'V_181',
               particles = [ P.a, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS11, L.VVVVS15 ],
               couplings = {(0,1):C.GC_670,(0,0):C.GC_669})

V_182 = Vertex(name = 'V_182',
               particles = [ P.a, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS11, L.VVVVS15 ],
               couplings = {(0,1):C.GC_672,(0,0):C.GC_671})

V_183 = Vertex(name = 'V_183',
               particles = [ P.a, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS15 ],
               couplings = {(0,0):C.GC_677})

V_184 = Vertex(name = 'V_184',
               particles = [ P.a, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV18, L.VVVV24, L.VVVV39, L.VVVV42 ],
               couplings = {(0,3):C.GC_308,(0,2):C.GC_316,(0,1):C.GC_735,(0,0):C.GC_734})

V_185 = Vertex(name = 'V_185',
               particles = [ P.a, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV18, L.VVVV24, L.VVVV39, L.VVVV42 ],
               couplings = {(0,3):C.GC_312,(0,2):C.GC_320,(0,1):C.GC_722,(0,0):C.GC_721})

V_186 = Vertex(name = 'V_186',
               particles = [ P.a, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV18, L.VVVV24, L.VVVV39, L.VVVV42 ],
               couplings = {(0,3):C.GC_439,(0,2):C.GC_437,(0,1):C.GC_724,(0,0):C.GC_723})

V_187 = Vertex(name = 'V_187',
               particles = [ P.a, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV24, L.VVVV39, L.VVVV42 ],
               couplings = {(0,2):C.GC_441,(0,1):C.GC_443,(0,0):C.GC_729})

V_188 = Vertex(name = 'V_188',
               particles = [ P.a, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV39, L.VVVV42 ],
               couplings = {(0,1):C.GC_394,(0,0):C.GC_395})

V_189 = Vertex(name = 'V_189',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVSS10, L.VVVVVSS11, L.VVVVVSS15, L.VVVVVSS2, L.VVVVVSS4, L.VVVVVSS5, L.VVVVVSS6, L.VVVVVSS7, L.VVVVVSS8, L.VVVVVSS9 ],
               couplings = {(0,3):C.GC_416,(0,1):C.GC_29,(0,0):C.GC_181,(0,6):C.GC_137,(0,4):C.GC_139,(0,5):C.GC_241,(0,8):C.GC_245,(0,9):C.GC_302,(0,7):C.GC_298,(0,2):C.GC_412})

V_190 = Vertex(name = 'V_190',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVSS2 ],
               couplings = {(0,0):C.GC_351})

V_191 = Vertex(name = 'V_191',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVS10, L.VVVVVS11, L.VVVVVS15, L.VVVVVS2, L.VVVVVS4, L.VVVVVS5, L.VVVVVS6, L.VVVVVS7, L.VVVVVS8, L.VVVVVS9 ],
               couplings = {(0,3):C.GC_660,(0,1):C.GC_471,(0,0):C.GC_514,(0,6):C.GC_488,(0,4):C.GC_490,(0,5):C.GC_527,(0,8):C.GC_531,(0,9):C.GC_540,(0,7):C.GC_538,(0,2):C.GC_656})

V_192 = Vertex(name = 'V_192',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVS2 ],
               couplings = {(0,0):C.GC_547})

V_193 = Vertex(name = 'V_193',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVV10, L.VVVVV11, L.VVVVV15, L.VVVVV16, L.VVVVV17, L.VVVVV18, L.VVVVV19, L.VVVVV20, L.VVVVV21, L.VVVVV22, L.VVVVV23, L.VVVVV24, L.VVVVV25, L.VVVVV26, L.VVVVV31, L.VVVVV5, L.VVVVV50, L.VVVVV59, L.VVVVV6, L.VVVVV60, L.VVVVV69, L.VVVVV70 ],
               couplings = {(0,11):C.GC_65,(0,10):C.GC_72,(0,19):C.GC_56,(0,16):C.GC_87,(0,13):C.GC_47,(0,12):C.GC_82,(0,17):C.GC_386,(0,21):C.GC_38,(0,20):C.GC_376,(0,0):C.GC_371,(0,1):C.GC_381,(0,15):C.GC_77,(0,18):C.GC_712,(0,7):C.GC_558,(0,6):C.GC_599,(0,4):C.GC_574,(0,2):C.GC_576,(0,3):C.GC_611,(0,8):C.GC_614,(0,9):C.GC_622,(0,5):C.GC_620,(0,14):C.GC_708})

V_194 = Vertex(name = 'V_194',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVV6 ],
               couplings = {(0,0):C.GC_628})

V_195 = Vertex(name = 'V_195',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS2, L.VVVVVVSS5 ],
               couplings = {(0,0):C.GC_417,(0,1):C.GC_413})

V_196 = Vertex(name = 'V_196',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS2 ],
               couplings = {(0,0):C.GC_423})

V_197 = Vertex(name = 'V_197',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS2, L.VVVVVVS5 ],
               couplings = {(0,0):C.GC_661,(0,1):C.GC_657})

V_198 = Vertex(name = 'V_198',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS2 ],
               couplings = {(0,0):C.GC_665})

V_199 = Vertex(name = 'V_199',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV1, L.VVVVVV14, L.VVVVVV15, L.VVVVVV16, L.VVVVVV23, L.VVVVVV25, L.VVVVVV39, L.VVVVVV49, L.VVVVVV5, L.VVVVVV50, L.VVVVVV65, L.VVVVVV67, L.VVVVVV7, L.VVVVVV9 ],
               couplings = {(0,1):C.GC_49,(0,0):C.GC_216,(0,9):C.GC_58,(0,7):C.GC_91,(0,11):C.GC_40,(0,10):C.GC_377,(0,2):C.GC_74,(0,4):C.GC_387,(0,12):C.GC_372,(0,3):C.GC_222,(0,8):C.GC_80,(0,13):C.GC_713,(0,6):C.GC_218,(0,5):C.GC_709})

V_200 = Vertex(name = 'V_200',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV1, L.VVVVVV14, L.VVVVVV15, L.VVVVVV7, L.VVVVVV9 ],
               couplings = {(0,1):C.GC_67,(0,0):C.GC_220,(0,2):C.GC_85,(0,3):C.GC_382,(0,4):C.GC_717})

V_201 = Vertex(name = 'V_201',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVSS14, L.VVVVVSS16, L.VVVVVSS21, L.VVVVVSS22, L.VVVVVSS23, L.VVVVVSS24, L.VVVVVSS25, L.VVVVVSS26, L.VVVVVSS27 ],
               couplings = {(0,5):C.GC_20,(0,3):C.GC_405,(0,6):C.GC_141,(0,7):C.GC_174,(0,4):C.GC_177,(0,8):C.GC_249,(0,2):C.GC_348,(0,1):C.GC_403,(0,0):C.GC_347})

V_202 = Vertex(name = 'V_202',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVSS22, L.VVVVVSS24 ],
               couplings = {(0,1):C.GC_23,(0,0):C.GC_306})

V_203 = Vertex(name = 'V_203',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVS14, L.VVVVVS16, L.VVVVVS21, L.VVVVVS22, L.VVVVVS23, L.VVVVVS24, L.VVVVVS25, L.VVVVVS26, L.VVVVVS27 ],
               couplings = {(0,5):C.GC_464,(0,3):C.GC_649,(0,6):C.GC_492,(0,7):C.GC_507,(0,4):C.GC_510,(0,8):C.GC_535,(0,2):C.GC_545,(0,1):C.GC_647,(0,0):C.GC_544})

V_204 = Vertex(name = 'V_204',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVS22, L.VVVVVS24 ],
               couplings = {(0,1):C.GC_467,(0,0):C.GC_543})

V_205 = Vertex(name = 'V_205',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVV30, L.VVVVV32, L.VVVVV37, L.VVVVV38, L.VVVVV39, L.VVVVV40, L.VVVVV41, L.VVVVV42, L.VVVVV43, L.VVVVV58, L.VVVVV63, L.VVVVV67, L.VVVVV71 ],
               couplings = {(0,12):C.GC_252,(0,10):C.GC_264,(0,9):C.GC_270,(0,11):C.GC_258,(0,5):C.GC_553,(0,3):C.GC_702,(0,6):C.GC_578,(0,7):C.GC_592,(0,4):C.GC_595,(0,8):C.GC_617,(0,2):C.GC_626,(0,1):C.GC_700,(0,0):C.GC_625})

V_206 = Vertex(name = 'V_206',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVV38, L.VVVVV40, L.VVVVV58, L.VVVVV63, L.VVVVV67, L.VVVVV71 ],
               couplings = {(0,5):C.GC_328,(0,3):C.GC_337,(0,2):C.GC_324,(0,4):C.GC_334,(0,1):C.GC_555,(0,0):C.GC_624})

V_207 = Vertex(name = 'V_207',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS10, L.VVVVVVSS16, L.VVVVVVSS2, L.VVVVVVSS5, L.VVVVVVSS8, L.VVVVVVSS9 ],
               couplings = {(0,2):C.GC_101,(0,5):C.GC_114,(0,4):C.GC_116,(0,0):C.GC_183,(0,1):C.GC_112,(0,3):C.GC_99})

V_208 = Vertex(name = 'V_208',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS2 ],
               couplings = {(0,0):C.GC_30})

V_209 = Vertex(name = 'V_209',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS10, L.VVVVVVS16, L.VVVVVVS2, L.VVVVVVS5, L.VVVVVVS8, L.VVVVVVS9 ],
               couplings = {(0,2):C.GC_637,(0,5):C.GC_477,(0,4):C.GC_479,(0,0):C.GC_516,(0,1):C.GC_475,(0,3):C.GC_635})

V_210 = Vertex(name = 'V_210',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS2 ],
               couplings = {(0,0):C.GC_472})

V_211 = Vertex(name = 'V_211',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV23, L.VVVVVV25, L.VVVVVV28, L.VVVVVV29, L.VVVVVV30, L.VVVVVV36, L.VVVVVV42, L.VVVVVV61, L.VVVVVV65, L.VVVVVV66, L.VVVVVV7, L.VVVVVV70, L.VVVVVV9 ],
               couplings = {(0,9):C.GC_188,(0,11):C.GC_195,(0,6):C.GC_202,(0,7):C.GC_209,(0,8):C.GC_78,(0,0):C.GC_89,(0,10):C.GC_73,(0,12):C.GC_690,(0,3):C.GC_563,(0,2):C.GC_565,(0,4):C.GC_601,(0,5):C.GC_561,(0,1):C.GC_688})

V_212 = Vertex(name = 'V_212',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV7, L.VVVVVV9 ],
               couplings = {(0,0):C.GC_84,(0,1):C.GC_559})

V_213 = Vertex(name = 'V_213',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS11, L.VVVVVVSS14, L.VVVVVVSS15, L.VVVVVVSS3 ],
               couplings = {(0,2):C.GC_133,(0,3):C.GC_135,(0,0):C.GC_142,(0,1):C.GC_250})

V_214 = Vertex(name = 'V_214',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS11, L.VVVVVVS14, L.VVVVVVS15, L.VVVVVVS3 ],
               couplings = {(0,2):C.GC_484,(0,3):C.GC_486,(0,0):C.GC_493,(0,1):C.GC_536})

V_215 = Vertex(name = 'V_215',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV10, L.VVVVVV12, L.VVVVVV3, L.VVVVVV31, L.VVVVVV34, L.VVVVVV35, L.VVVVVV44, L.VVVVVV48, L.VVVVVV53, L.VVVVVV59, L.VVVVVV64 ],
               couplings = {(0,10):C.GC_259,(0,9):C.GC_265,(0,8):C.GC_271,(0,7):C.GC_286,(0,6):C.GC_253,(0,0):C.GC_276,(0,2):C.GC_279,(0,5):C.GC_570,(0,1):C.GC_572,(0,3):C.GC_579,(0,4):C.GC_618})

V_216 = Vertex(name = 'V_216',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV10 ],
               couplings = {(0,0):C.GC_283})

V_217 = Vertex(name = 'V_217',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS13 ],
               couplings = {(0,0):C.GC_404})

V_218 = Vertex(name = 'V_218',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS13 ],
               couplings = {(0,0):C.GC_406})

V_219 = Vertex(name = 'V_219',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS13 ],
               couplings = {(0,0):C.GC_408})

V_220 = Vertex(name = 'V_220',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS13 ],
               couplings = {(0,0):C.GC_648})

V_221 = Vertex(name = 'V_221',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS13 ],
               couplings = {(0,0):C.GC_650})

V_222 = Vertex(name = 'V_222',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS13 ],
               couplings = {(0,0):C.GC_652})

V_223 = Vertex(name = 'V_223',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV11, L.VVVVVV13, L.VVVVVV20, L.VVVVVV21, L.VVVVVV33, L.VVVVVV47, L.VVVVVV57, L.VVVVVV58, L.VVVVVV62, L.VVVVVV63 ],
               couplings = {(0,6):C.GC_266,(0,5):C.GC_288,(0,7):C.GC_340,(0,9):C.GC_254,(0,8):C.GC_331,(0,2):C.GC_260,(0,0):C.GC_277,(0,3):C.GC_326,(0,1):C.GC_280,(0,4):C.GC_701})

V_224 = Vertex(name = 'V_224',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV11, L.VVVVVV20, L.VVVVVV21, L.VVVVVV33 ],
               couplings = {(0,1):C.GC_272,(0,0):C.GC_284,(0,2):C.GC_335,(0,3):C.GC_703})

V_225 = Vertex(name = 'V_225',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV33 ],
               couplings = {(0,0):C.GC_705})

V_226 = Vertex(name = 'V_226',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS4 ],
               couplings = {(0,0):C.GC_100})

V_227 = Vertex(name = 'V_227',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS4 ],
               couplings = {(0,0):C.GC_102})

V_228 = Vertex(name = 'V_228',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVSS4 ],
               couplings = {(0,0):C.GC_105})

V_229 = Vertex(name = 'V_229',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS4 ],
               couplings = {(0,0):C.GC_636})

V_230 = Vertex(name = 'V_230',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS4 ],
               couplings = {(0,0):C.GC_638})

V_231 = Vertex(name = 'V_231',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVS4 ],
               couplings = {(0,0):C.GC_641})

V_232 = Vertex(name = 'V_232',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV22, L.VVVVVV24, L.VVVVVV60, L.VVVVVV68 ],
               couplings = {(0,0):C.GC_74,(0,2):C.GC_90,(0,3):C.GC_79,(0,1):C.GC_689})

V_233 = Vertex(name = 'V_233',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV22, L.VVVVVV24, L.VVVVVV60, L.VVVVVV68 ],
               couplings = {(0,0):C.GC_85,(0,2):C.GC_203,(0,3):C.GC_189,(0,1):C.GC_691})

V_234 = Vertex(name = 'V_234',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV22, L.VVVVVV24 ],
               couplings = {(0,0):C.GC_196,(0,1):C.GC_694})

V_235 = Vertex(name = 'V_235',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV22 ],
               couplings = {(0,0):C.GC_210})

V_236 = Vertex(name = 'V_236',
               particles = [ P.a, P.W__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVS1, L.VVVS5, L.VVVS6 ],
               couplings = {(0,1):C.GC_468,(0,2):C.GC_528,(0,0):C.GC_524})

V_237 = Vertex(name = 'V_237',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVSS12, L.VVVVVSS13, L.VVVVVSS31 ],
               couplings = {(0,1):C.GC_104,(0,2):C.GC_407,(0,0):C.GC_103})

V_238 = Vertex(name = 'V_238',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVVS12, L.VVVVVS13, L.VVVVVS31 ],
               couplings = {(0,1):C.GC_640,(0,2):C.GC_651,(0,0):C.GC_639})

V_239 = Vertex(name = 'V_239',
               particles = [ P.a, P.a, P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVV1, L.VVVVV13, L.VVVVV14, L.VVVVV2, L.VVVVV27, L.VVVVV28, L.VVVVV29, L.VVVVV3, L.VVVVV49, L.VVVVV51, L.VVVVV52, L.VVVVV56, L.VVVVV57, L.VVVVV64, L.VVVVV65 ],
               couplings = {(0,8):C.GC_285,(0,11):C.GC_317,(0,10):C.GC_321,(0,9):C.GC_325,(0,12):C.GC_338,(0,13):C.GC_313,(0,14):C.GC_333,(0,0):C.GC_275,(0,3):C.GC_282,(0,2):C.GC_309,(0,7):C.GC_329,(0,6):C.GC_693,(0,4):C.GC_704,(0,1):C.GC_278,(0,5):C.GC_692})

V_240 = Vertex(name = 'V_240',
               particles = [ P.a, P.a, P.a, P.a ],
               color = [ '1' ],
               lorentz = [ L.VVVV39, L.VVVV42 ],
               couplings = {(0,1):C.GC_93,(0,0):C.GC_94})

V_241 = Vertex(name = 'V_241',
               particles = [ P.a, P.a, P.a, P.a ],
               color = [ '1' ],
               lorentz = [ L.VVVV39, L.VVVV42 ],
               couplings = {(0,1):C.GC_375,(0,0):C.GC_370})

V_242 = Vertex(name = 'V_242',
               particles = [ P.a, P.a, P.a, P.a ],
               color = [ '1' ],
               lorentz = [ L.VVVV39, L.VVVV42 ],
               couplings = {(0,1):C.GC_380,(0,0):C.GC_385})

V_243 = Vertex(name = 'V_243',
               particles = [ P.a, P.a, P.a, P.a ],
               color = [ '1' ],
               lorentz = [ L.VVVV39, L.VVVV42 ],
               couplings = {(0,1):C.GC_396,(0,0):C.GC_398})

V_244 = Vertex(name = 'V_244',
               particles = [ P.a, P.a, P.a, P.a ],
               color = [ '1' ],
               lorentz = [ L.VVVV39, L.VVVV42 ],
               couplings = {(0,1):C.GC_397,(0,0):C.GC_399})

V_245 = Vertex(name = 'V_245',
               particles = [ P.a, P.a, P.a, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV39, L.VVVV42 ],
               couplings = {(0,1):C.GC_438,(0,0):C.GC_436})

V_246 = Vertex(name = 'V_246',
               particles = [ P.a, P.a, P.a, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV39, L.VVVV42 ],
               couplings = {(0,1):C.GC_440,(0,0):C.GC_442})

V_247 = Vertex(name = 'V_247',
               particles = [ P.a, P.a, P.a, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV39, L.VVVV42 ],
               couplings = {(0,1):C.GC_341,(0,0):C.GC_342})

V_248 = Vertex(name = 'V_248',
               particles = [ P.a, P.a, P.a, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV39, L.VVVV42 ],
               couplings = {(0,1):C.GC_390,(0,0):C.GC_392})

V_249 = Vertex(name = 'V_249',
               particles = [ P.a, P.a, P.a, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV39, L.VVVV42 ],
               couplings = {(0,1):C.GC_391,(0,0):C.GC_393})

V_250 = Vertex(name = 'V_250',
               particles = [ P.a, P.a, P.a, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVV48, L.VVVVV53, L.VVVVV8, L.VVVVV9 ],
               couplings = {(0,2):C.GC_71,(0,1):C.GC_88,(0,3):C.GC_83,(0,0):C.GC_76})

V_251 = Vertex(name = 'V_251',
               particles = [ P.a, P.a, P.a, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVV48, L.VVVVV53, L.VVVVV8, L.VVVVV9 ],
               couplings = {(0,2):C.GC_366,(0,1):C.GC_362,(0,3):C.GC_358,(0,0):C.GC_354})

V_252 = Vertex(name = 'V_252',
               particles = [ P.a, P.a, P.a, P.a, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV43, L.VVVVVV46, L.VVVVVV6 ],
               couplings = {(0,2):C.GC_74,(0,0):C.GC_92,(0,1):C.GC_79})

V_253 = Vertex(name = 'V_253',
               particles = [ P.a, P.a, P.a, P.a, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV43, L.VVVVVV46, L.VVVVVV6 ],
               couplings = {(0,2):C.GC_85,(0,0):C.GC_363,(0,1):C.GC_355})

V_254 = Vertex(name = 'V_254',
               particles = [ P.a, P.a, P.a, P.a, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV6 ],
               couplings = {(0,0):C.GC_359})

V_255 = Vertex(name = 'V_255',
               particles = [ P.a, P.a, P.a, P.a, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV6 ],
               couplings = {(0,0):C.GC_367})

V_256 = Vertex(name = 'V_256',
               particles = [ P.a, P.a, P.a, P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV17, L.VVVVVV18, L.VVVVVV2, L.VVVVVV37, L.VVVVVV38, L.VVVVVV4, L.VVVVVV41, L.VVVVVV51, L.VVVVVV52 ],
               couplings = {(0,2):C.GC_277,(0,0):C.GC_314,(0,6):C.GC_287,(0,7):C.GC_318,(0,8):C.GC_339,(0,1):C.GC_326,(0,3):C.GC_310,(0,5):C.GC_330,(0,4):C.GC_281})

V_257 = Vertex(name = 'V_257',
               particles = [ P.a, P.a, P.a, P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVV17, L.VVVVVV18, L.VVVVVV2 ],
               couplings = {(0,2):C.GC_284,(0,0):C.GC_322,(0,1):C.GC_335})

V_258 = Vertex(name = 'V_258',
               particles = [ P.a, P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVV1, L.VVVVVVV11, L.VVVVVVV17 ],
               couplings = {(0,0):C.GC_41,(0,2):C.GC_59,(0,1):C.GC_50})

V_259 = Vertex(name = 'V_259',
               particles = [ P.a, P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVV11 ],
               couplings = {(0,0):C.GC_68})

V_260 = Vertex(name = 'V_260',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVV3, L.VVVVVVV4, L.VVVVVVV8 ],
               couplings = {(0,0):C.GC_197,(0,1):C.GC_204,(0,2):C.GC_190})

V_261 = Vertex(name = 'V_261',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVV3 ],
               couplings = {(0,0):C.GC_211})

V_262 = Vertex(name = 'V_262',
               particles = [ P.a, P.a, P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV17, L.VVVVVVVV2, L.VVVVVVVV24 ],
               couplings = {(0,1):C.GC_42,(0,2):C.GC_51,(0,0):C.GC_60})

V_263 = Vertex(name = 'V_263',
               particles = [ P.a, P.a, P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV24 ],
               couplings = {(0,0):C.GC_69})

V_264 = Vertex(name = 'V_264',
               particles = [ P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVV12, L.VVVVVVV13, L.VVVVVVV6 ],
               couplings = {(0,0):C.GC_261,(0,1):C.GC_267,(0,2):C.GC_255})

V_265 = Vertex(name = 'V_265',
               particles = [ P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVV12 ],
               couplings = {(0,0):C.GC_273})

V_266 = Vertex(name = 'V_266',
               particles = [ P.a, P.a, P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV16, L.VVVVVVVV22, L.VVVVVVVV4 ],
               couplings = {(0,2):C.GC_199,(0,0):C.GC_206,(0,1):C.GC_192})

V_267 = Vertex(name = 'V_267',
               particles = [ P.a, P.a, P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV4 ],
               couplings = {(0,0):C.GC_213})

V_268 = Vertex(name = 'V_268',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVV10, L.VVVVVVV16, L.VVVVVVV18 ],
               couplings = {(0,1):C.GC_143,(0,2):C.GC_151,(0,0):C.GC_147})

V_269 = Vertex(name = 'V_269',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVV10 ],
               couplings = {(0,0):C.GC_155})

V_270 = Vertex(name = 'V_270',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV21 ],
               couplings = {(0,0):C.GC_120})

V_271 = Vertex(name = 'V_271',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV21 ],
               couplings = {(0,0):C.GC_123})

V_272 = Vertex(name = 'V_272',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV21 ],
               couplings = {(0,0):C.GC_126})

V_273 = Vertex(name = 'V_273',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV21 ],
               couplings = {(0,0):C.GC_129})

V_274 = Vertex(name = 'V_274',
               particles = [ P.a, P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV12, L.VVVVVVVV14, L.VVVVVVVV7 ],
               couplings = {(0,2):C.GC_262,(0,1):C.GC_268,(0,0):C.GC_256})

V_275 = Vertex(name = 'V_275',
               particles = [ P.a, P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV7 ],
               couplings = {(0,0):C.GC_274})

V_276 = Vertex(name = 'V_276',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVV2, L.VVVVVVV5, L.VVVVVVV7 ],
               couplings = {(0,0):C.GC_191,(0,1):C.GC_198,(0,2):C.GC_205})

V_277 = Vertex(name = 'V_277',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVV5 ],
               couplings = {(0,0):C.GC_212})

V_278 = Vertex(name = 'V_278',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV10, L.VVVVVVVV18, L.VVVVVVVV6 ],
               couplings = {(0,2):C.GC_145,(0,0):C.GC_153,(0,1):C.GC_149})

V_279 = Vertex(name = 'V_279',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV18 ],
               couplings = {(0,0):C.GC_157})

V_280 = Vertex(name = 'V_280',
               particles = [ P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV15, L.VVVVVVVV3, L.VVVVVVVV9 ],
               couplings = {(0,1):C.GC_193,(0,0):C.GC_200,(0,2):C.GC_207})

V_281 = Vertex(name = 'V_281',
               particles = [ P.a, P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV15 ],
               couplings = {(0,0):C.GC_214})

V_282 = Vertex(name = 'V_282',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVV14, L.VVVVVVV15, L.VVVVVVV9 ],
               couplings = {(0,0):C.GC_148,(0,1):C.GC_152,(0,2):C.GC_144})

V_283 = Vertex(name = 'V_283',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVV14 ],
               couplings = {(0,0):C.GC_156})

V_284 = Vertex(name = 'V_284',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV1, L.VVVVVVVV19, L.VVVVVVVV25 ],
               couplings = {(0,0):C.GC_124,(0,1):C.GC_127,(0,2):C.GC_121})

V_285 = Vertex(name = 'V_285',
               particles = [ P.W__minus__, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV1 ],
               couplings = {(0,0):C.GC_130})

V_286 = Vertex(name = 'V_286',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV11, L.VVVVVVVV13, L.VVVVVVVV8 ],
               couplings = {(0,2):C.GC_150,(0,1):C.GC_154,(0,0):C.GC_146})

V_287 = Vertex(name = 'V_287',
               particles = [ P.a, P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV8 ],
               couplings = {(0,0):C.GC_158})

V_288 = Vertex(name = 'V_288',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV20, L.VVVVVVVV23, L.VVVVVVVV5 ],
               couplings = {(0,2):C.GC_122,(0,1):C.GC_125,(0,0):C.GC_128})

V_289 = Vertex(name = 'V_289',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.Z, P.Z, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVVVVVV23 ],
               couplings = {(0,0):C.GC_131})

