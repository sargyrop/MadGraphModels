Requestor: Shu Li
Contents: Loop induced H-gamma via a Z'
Paper: https://arxiv.org/abs/1705.08433
Source: Private communication from Bogdan Dobrescu
JIRA: https://its.cern.ch/jira/browse/AGENE-1461