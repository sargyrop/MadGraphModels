# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 11.2.0 for Microsoft Windows (64-bit) (September 11, 2017)
# Date: Tue 12 Dec 2017 14:34:56


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_113_1 = Coupling(name = 'R2GC_113_1',
                      value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_114_2 = Coupling(name = 'R2GC_114_2',
                      value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_115_3 = Coupling(name = 'R2GC_115_3',
                      value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_116_4 = Coupling(name = 'R2GC_116_4',
                      value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                      order = {'QCD':3})

R2GC_118_5 = Coupling(name = 'R2GC_118_5',
                      value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_126_6 = Coupling(name = 'R2GC_126_6',
                      value = '-(complex(0,1)*G**2*MB**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_126_7 = Coupling(name = 'R2GC_126_7',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_127_8 = Coupling(name = 'R2GC_127_8',
                      value = '-(complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2)) + (cal*complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2)) - (chi*complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_127_9 = Coupling(name = 'R2GC_127_9',
                      value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2)) + (cal*complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2)) - (chi*complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_128_10 = Coupling(name = 'R2GC_128_10',
                       value = '(complex(0,1)*G**2*MB*sal*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_128_11 = Coupling(name = 'R2GC_128_11',
                       value = '(complex(0,1)*G**2*MT*sal*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_129_12 = Coupling(name = 'R2GC_129_12',
                       value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_129_13 = Coupling(name = 'R2GC_129_13',
                       value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_130_14 = Coupling(name = 'R2GC_130_14',
                       value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2) + (cal*complex(0,1)*G**2*yb**2)/(8.*cmath.pi**2) - (cal**2*complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2) - (chi*complex(0,1)*G**2*yb**2)/(8.*cmath.pi**2) + (cal*chi*complex(0,1)*G**2*yb**2)/(8.*cmath.pi**2) - (chi**2*complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_130_15 = Coupling(name = 'R2GC_130_15',
                       value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2) + (cal*complex(0,1)*G**2*yt**2)/(8.*cmath.pi**2) - (cal**2*complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2) - (chi*complex(0,1)*G**2*yt**2)/(8.*cmath.pi**2) + (cal*chi*complex(0,1)*G**2*yt**2)/(8.*cmath.pi**2) - (chi**2*complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_131_16 = Coupling(name = 'R2GC_131_16',
                       value = '(complex(0,1)*G**2*sal*yb**2)/(16.*cmath.pi**2) - (cal*complex(0,1)*G**2*sal*yb**2)/(16.*cmath.pi**2) + (chi*complex(0,1)*G**2*sal*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_131_17 = Coupling(name = 'R2GC_131_17',
                       value = '(complex(0,1)*G**2*sal*yt**2)/(16.*cmath.pi**2) - (cal*complex(0,1)*G**2*sal*yt**2)/(16.*cmath.pi**2) + (chi*complex(0,1)*G**2*sal*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_132_18 = Coupling(name = 'R2GC_132_18',
                       value = '-(complex(0,1)*G**2*sal**2*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_132_19 = Coupling(name = 'R2GC_132_19',
                       value = '-(complex(0,1)*G**2*sal**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_133_20 = Coupling(name = 'R2GC_133_20',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_134_21 = Coupling(name = 'R2GC_134_21',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_134_22 = Coupling(name = 'R2GC_134_22',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_135_23 = Coupling(name = 'R2GC_135_23',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_135_24 = Coupling(name = 'R2GC_135_24',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_136_25 = Coupling(name = 'R2GC_136_25',
                       value = '-(ee*complex(0,1)*G**3*szi)/(64.*cw*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**3*txi)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_136_26 = Coupling(name = 'R2GC_136_26',
                       value = '(ee*complex(0,1)*G**3*szi)/(64.*cw*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**3*txi)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_137_27 = Coupling(name = 'R2GC_137_27',
                       value = '-(ee**2*complex(0,1)*G**2*szi)/(288.*cw*cmath.pi**2*sw) + (ee**2*complex(0,1)*G**2*sw*szi)/(216.*cw*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*txi)/(288.*cw*cmath.pi**2) - (cw*czi*ee**2*complex(0,1)*G**2*txi)/(216.*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw**2*txi)/(216.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_137_28 = Coupling(name = 'R2GC_137_28',
                       value = '-(ee**2*complex(0,1)*G**2*szi)/(144.*cw*cmath.pi**2*sw) + (ee**2*complex(0,1)*G**2*sw*szi)/(54.*cw*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*txi)/(144.*cw*cmath.pi**2) - (cw*czi*ee**2*complex(0,1)*G**2*txi)/(54.*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw**2*txi)/(54.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_138_29 = Coupling(name = 'R2GC_138_29',
                       value = '(ee*complex(0,1)*G**3*szi)/(192.*cw*cmath.pi**2*sw) - (ee*complex(0,1)*G**3*sw*szi)/(144.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**3*txi)/(192.*cw*cmath.pi**2) + (cw*czi*ee*complex(0,1)*G**3*txi)/(144.*cmath.pi**2) + (czi*ee*complex(0,1)*G**3*sw**2*txi)/(144.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_138_30 = Coupling(name = 'R2GC_138_30',
                       value = '-(ee*complex(0,1)*G**3*szi)/(192.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw*szi)/(72.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**3*txi)/(192.*cw*cmath.pi**2) - (cw*czi*ee*complex(0,1)*G**3*txi)/(72.*cmath.pi**2) - (czi*ee*complex(0,1)*G**3*sw**2*txi)/(72.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_139_31 = Coupling(name = 'R2GC_139_31',
                       value = '(3*chi*ee*complex(0,1)*G**3)/(64.*cw*cmath.pi**2*sw) - (3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*czi*ee*complex(0,1)*G**3)/(64.*cw*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2) - (3*ee*complex(0,1)*G**3*szi*txi)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_139_32 = Coupling(name = 'R2GC_139_32',
                       value = '(-3*chi*ee*complex(0,1)*G**3)/(64.*cw*cmath.pi**2*sw) + (3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*czi*ee*complex(0,1)*G**3)/(64.*cw*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2) + (3*ee*complex(0,1)*G**3*szi*txi)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_140_33 = Coupling(name = 'R2GC_140_33',
                       value = '-(chi*ee**2*complex(0,1)*G**2)/(288.*cw*cmath.pi**2*sw) + (cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) + (czi*ee**2*complex(0,1)*G**2)/(288.*cw*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2) + (chi*ee**2*complex(0,1)*G**2*sw)/(216.*cw*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw)/(216.*cw*cmath.pi**2) + (ee**2*complex(0,1)*G**2*szi*txi)/(288.*cw*cmath.pi**2) - (cw*ee**2*complex(0,1)*G**2*szi*txi)/(216.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2*szi*txi)/(216.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_140_34 = Coupling(name = 'R2GC_140_34',
                       value = '-(chi*ee**2*complex(0,1)*G**2)/(144.*cw*cmath.pi**2*sw) + (cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) + (czi*ee**2*complex(0,1)*G**2)/(144.*cw*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2) + (chi*ee**2*complex(0,1)*G**2*sw)/(54.*cw*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw)/(54.*cw*cmath.pi**2) + (ee**2*complex(0,1)*G**2*szi*txi)/(144.*cw*cmath.pi**2) - (cw*ee**2*complex(0,1)*G**2*szi*txi)/(54.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2*szi*txi)/(54.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_141_35 = Coupling(name = 'R2GC_141_35',
                       value = '(chi*ee*complex(0,1)*G**3)/(192.*cw*cmath.pi**2*sw) - (cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**3)/(192.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**3*sw)/(144.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**3*sw)/(144.*cw*cmath.pi**2) - (ee*complex(0,1)*G**3*szi*txi)/(192.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**3*szi*txi)/(144.*cmath.pi**2) + (ee*complex(0,1)*G**3*sw**2*szi*txi)/(144.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_141_36 = Coupling(name = 'R2GC_141_36',
                       value = '-(chi*ee*complex(0,1)*G**3)/(192.*cw*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**3)/(192.*cw*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**3*sw)/(72.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**3*sw)/(72.*cw*cmath.pi**2) + (ee*complex(0,1)*G**3*szi*txi)/(192.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**3*szi*txi)/(72.*cmath.pi**2) - (ee*complex(0,1)*G**3*sw**2*szi*txi)/(72.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_142_37 = Coupling(name = 'R2GC_142_37',
                       value = '-(ee**2*complex(0,1)*G**2*szi**2)/(144.*cw**2*cmath.pi**2) + (ee**2*complex(0,1)*G**2*szi**2)/(192.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2*szi**2)/(216.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*szi*txi)/(144.*cmath.pi**2*sw) - (czi*ee**2*complex(0,1)*G**2*szi*txi)/(96.*cw**2*cmath.pi**2*sw) - (czi*ee**2*complex(0,1)*G**2*sw*szi*txi)/(108.*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw*szi*txi)/(72.*cw**2*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw**3*szi*txi)/(108.*cw**2*cmath.pi**2) - (czi**2*ee**2*complex(0,1)*G**2*txi**2)/(144.*cmath.pi**2) + (czi**2*ee**2*complex(0,1)*G**2*txi**2)/(192.*cw**2*cmath.pi**2) + (cw**2*czi**2*ee**2*complex(0,1)*G**2*txi**2)/(216.*cmath.pi**2) + (czi**2*ee**2*complex(0,1)*G**2*sw**2*txi**2)/(108.*cmath.pi**2) - (czi**2*ee**2*complex(0,1)*G**2*sw**2*txi**2)/(144.*cw**2*cmath.pi**2) + (czi**2*ee**2*complex(0,1)*G**2*sw**4*txi**2)/(216.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_142_38 = Coupling(name = 'R2GC_142_38',
                       value = '-(ee**2*complex(0,1)*G**2*szi**2)/(72.*cw**2*cmath.pi**2) + (ee**2*complex(0,1)*G**2*szi**2)/(192.*cw**2*cmath.pi**2*sw**2) + (ee**2*complex(0,1)*G**2*sw**2*szi**2)/(54.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*szi*txi)/(72.*cmath.pi**2*sw) - (czi*ee**2*complex(0,1)*G**2*szi*txi)/(96.*cw**2*cmath.pi**2*sw) - (czi*ee**2*complex(0,1)*G**2*sw*szi*txi)/(27.*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw*szi*txi)/(36.*cw**2*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw**3*szi*txi)/(27.*cw**2*cmath.pi**2) - (czi**2*ee**2*complex(0,1)*G**2*txi**2)/(72.*cmath.pi**2) + (czi**2*ee**2*complex(0,1)*G**2*txi**2)/(192.*cw**2*cmath.pi**2) + (cw**2*czi**2*ee**2*complex(0,1)*G**2*txi**2)/(54.*cmath.pi**2) + (czi**2*ee**2*complex(0,1)*G**2*sw**2*txi**2)/(27.*cmath.pi**2) - (czi**2*ee**2*complex(0,1)*G**2*sw**2*txi**2)/(72.*cw**2*cmath.pi**2) + (czi**2*ee**2*complex(0,1)*G**2*sw**4*txi**2)/(54.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_143_39 = Coupling(name = 'R2GC_143_39',
                       value = '(ee**2*complex(0,1)*G**2*szi)/(288.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*szi)/(576.*cw**2*cmath.pi**2) - (chi*ee**2*complex(0,1)*G**2*szi)/(144.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*szi)/(144.*cw**2*cmath.pi**2) - (ee**2*complex(0,1)*G**2*szi)/(192.*cmath.pi**2*sw**2) + (chi*ee**2*complex(0,1)*G**2*szi)/(192.*cw**2*cmath.pi**2*sw**2) - (czi*ee**2*complex(0,1)*G**2*szi)/(192.*cw**2*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2*szi)/(864.*cw**2*cmath.pi**2) + (chi*ee**2*complex(0,1)*G**2*sw**2*szi)/(216.*cw**2*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw**2*szi)/(216.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*txi)/(192.*cmath.pi**2*sw) + (chi*czi*ee**2*complex(0,1)*G**2*txi)/(288.*cmath.pi**2*sw) - (chi*czi*ee**2*complex(0,1)*G**2*txi)/(192.*cw**2*cmath.pi**2*sw) - (cw**2*czi*ee**2*complex(0,1)*G**2*txi)/(288.*cmath.pi**2*sw) - (czi**2*ee**2*complex(0,1)*G**2*txi)/(288.*cmath.pi**2*sw) + (czi**2*ee**2*complex(0,1)*G**2*txi)/(192.*cw**2*cmath.pi**2*sw) - (czi*ee**2*complex(0,1)*G**2*sw*txi)/(432.*cmath.pi**2) - (chi*czi*ee**2*complex(0,1)*G**2*sw*txi)/(216.*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw*txi)/(576.*cw**2*cmath.pi**2) + (chi*czi*ee**2*complex(0,1)*G**2*sw*txi)/(144.*cw**2*cmath.pi**2) + (czi**2*ee**2*complex(0,1)*G**2*sw*txi)/(216.*cmath.pi**2) - (czi**2*ee**2*complex(0,1)*G**2*sw*txi)/(144.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw**3*txi)/(864.*cw**2*cmath.pi**2) - (chi*czi*ee**2*complex(0,1)*G**2*sw**3*txi)/(216.*cw**2*cmath.pi**2) + (czi**2*ee**2*complex(0,1)*G**2*sw**3*txi)/(216.*cw**2*cmath.pi**2) + (ee**2*complex(0,1)*G**2*szi**2*txi)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*szi**2*txi)/(192.*cw**2*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw*szi**2*txi)/(216.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw*szi**2*txi)/(144.*cw**2*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**3*szi**2*txi)/(216.*cw**2*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*szi*txi**2)/(144.*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*szi*txi**2)/(192.*cw**2*cmath.pi**2) + (cw**2*czi*ee**2*complex(0,1)*G**2*szi*txi**2)/(216.*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw**2*szi*txi**2)/(108.*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw**2*szi*txi**2)/(144.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw**4*szi*txi**2)/(216.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_143_40 = Coupling(name = 'R2GC_143_40',
                       value = '(ee**2*complex(0,1)*G**2*szi)/(144.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*szi)/(576.*cw**2*cmath.pi**2) - (chi*ee**2*complex(0,1)*G**2*szi)/(72.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*szi)/(72.*cw**2*cmath.pi**2) - (ee**2*complex(0,1)*G**2*szi)/(192.*cmath.pi**2*sw**2) + (chi*ee**2*complex(0,1)*G**2*szi)/(192.*cw**2*cmath.pi**2*sw**2) - (czi*ee**2*complex(0,1)*G**2*szi)/(192.*cw**2*cmath.pi**2*sw**2) - (5*ee**2*complex(0,1)*G**2*sw**2*szi)/(432.*cw**2*cmath.pi**2) + (chi*ee**2*complex(0,1)*G**2*sw**2*szi)/(54.*cw**2*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw**2*szi)/(54.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*txi)/(192.*cmath.pi**2*sw) + (chi*czi*ee**2*complex(0,1)*G**2*txi)/(144.*cmath.pi**2*sw) - (chi*czi*ee**2*complex(0,1)*G**2*txi)/(192.*cw**2*cmath.pi**2*sw) - (cw**2*czi*ee**2*complex(0,1)*G**2*txi)/(144.*cmath.pi**2*sw) - (czi**2*ee**2*complex(0,1)*G**2*txi)/(144.*cmath.pi**2*sw) + (czi**2*ee**2*complex(0,1)*G**2*txi)/(192.*cw**2*cmath.pi**2*sw) + (czi*ee**2*complex(0,1)*G**2*sw*txi)/(216.*cmath.pi**2) - (chi*czi*ee**2*complex(0,1)*G**2*sw*txi)/(54.*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw*txi)/(576.*cw**2*cmath.pi**2) + (chi*czi*ee**2*complex(0,1)*G**2*sw*txi)/(72.*cw**2*cmath.pi**2) + (czi**2*ee**2*complex(0,1)*G**2*sw*txi)/(54.*cmath.pi**2) - (czi**2*ee**2*complex(0,1)*G**2*sw*txi)/(72.*cw**2*cmath.pi**2) + (5*czi*ee**2*complex(0,1)*G**2*sw**3*txi)/(432.*cw**2*cmath.pi**2) - (chi*czi*ee**2*complex(0,1)*G**2*sw**3*txi)/(54.*cw**2*cmath.pi**2) + (czi**2*ee**2*complex(0,1)*G**2*sw**3*txi)/(54.*cw**2*cmath.pi**2) + (ee**2*complex(0,1)*G**2*szi**2*txi)/(144.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*szi**2*txi)/(192.*cw**2*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw*szi**2*txi)/(54.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw*szi**2*txi)/(72.*cw**2*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**3*szi**2*txi)/(54.*cw**2*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*szi*txi**2)/(72.*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*szi*txi**2)/(192.*cw**2*cmath.pi**2) + (cw**2*czi*ee**2*complex(0,1)*G**2*szi*txi**2)/(54.*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw**2*szi*txi**2)/(27.*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw**2*szi*txi**2)/(72.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw**4*szi*txi**2)/(54.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_144_41 = Coupling(name = 'R2GC_144_41',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (chi*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2) - (chi*ee**2*complex(0,1)*G**2)/(288.*cw**2*cmath.pi**2) - (chi**2*ee**2*complex(0,1)*G**2)/(144.*cw**2*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2)/(288.*cw**2*cmath.pi**2) + (chi*czi*ee**2*complex(0,1)*G**2)/(72.*cw**2*cmath.pi**2) - (czi**2*ee**2*complex(0,1)*G**2)/(144.*cw**2*cmath.pi**2) - (chi*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (chi**2*ee**2*complex(0,1)*G**2)/(192.*cw**2*cmath.pi**2*sw**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (czi*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) - (chi*czi*ee**2*complex(0,1)*G**2)/(96.*cw**2*cmath.pi**2*sw**2) + (czi**2*ee**2*complex(0,1)*G**2)/(192.*cw**2*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2) - (chi*ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2) + (chi**2*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw**2)/(432.*cw**2*cmath.pi**2) - (chi*czi*ee**2*complex(0,1)*G**2*sw**2)/(108.*cw**2*cmath.pi**2) + (czi**2*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2) + (ee**2*complex(0,1)*G**2*szi*txi)/(96.*cmath.pi**2*sw) + (chi*ee**2*complex(0,1)*G**2*szi*txi)/(144.*cmath.pi**2*sw) - (chi*ee**2*complex(0,1)*G**2*szi*txi)/(96.*cw**2*cmath.pi**2*sw) - (cw**2*ee**2*complex(0,1)*G**2*szi*txi)/(144.*cmath.pi**2*sw) - (czi*ee**2*complex(0,1)*G**2*szi*txi)/(144.*cmath.pi**2*sw) + (czi*ee**2*complex(0,1)*G**2*szi*txi)/(96.*cw**2*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw*szi*txi)/(216.*cmath.pi**2) - (chi*ee**2*complex(0,1)*G**2*sw*szi*txi)/(108.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw*szi*txi)/(288.*cw**2*cmath.pi**2) + (chi*ee**2*complex(0,1)*G**2*sw*szi*txi)/(72.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw*szi*txi)/(108.*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw*szi*txi)/(72.*cw**2*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**3*szi*txi)/(432.*cw**2*cmath.pi**2) - (chi*ee**2*complex(0,1)*G**2*sw**3*szi*txi)/(108.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw**3*szi*txi)/(108.*cw**2*cmath.pi**2) - (ee**2*complex(0,1)*G**2*szi**2*txi**2)/(144.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*szi**2*txi**2)/(192.*cw**2*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*szi**2*txi**2)/(216.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2*szi**2*txi**2)/(108.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2*szi**2*txi**2)/(144.*cw**2*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**4*szi**2*txi**2)/(216.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_144_42 = Coupling(name = 'R2GC_144_42',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (chi*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) + (chi*ee**2*complex(0,1)*G**2)/(288.*cw**2*cmath.pi**2) - (chi**2*ee**2*complex(0,1)*G**2)/(72.*cw**2*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2)/(72.*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2)/(288.*cw**2*cmath.pi**2) + (chi*czi*ee**2*complex(0,1)*G**2)/(36.*cw**2*cmath.pi**2) - (czi**2*ee**2*complex(0,1)*G**2)/(72.*cw**2*cmath.pi**2) - (chi*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) + (chi**2*ee**2*complex(0,1)*G**2)/(192.*cw**2*cmath.pi**2*sw**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (czi*ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2) - (chi*czi*ee**2*complex(0,1)*G**2)/(96.*cw**2*cmath.pi**2*sw**2) + (czi**2*ee**2*complex(0,1)*G**2)/(192.*cw**2*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2) - (5*chi*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2) + (chi**2*ee**2*complex(0,1)*G**2*sw**2)/(54.*cw**2*cmath.pi**2) + (5*czi*ee**2*complex(0,1)*G**2*sw**2)/(216.*cw**2*cmath.pi**2) - (chi*czi*ee**2*complex(0,1)*G**2*sw**2)/(27.*cw**2*cmath.pi**2) + (czi**2*ee**2*complex(0,1)*G**2*sw**2)/(54.*cw**2*cmath.pi**2) + (ee**2*complex(0,1)*G**2*szi*txi)/(96.*cmath.pi**2*sw) + (chi*ee**2*complex(0,1)*G**2*szi*txi)/(72.*cmath.pi**2*sw) - (chi*ee**2*complex(0,1)*G**2*szi*txi)/(96.*cw**2*cmath.pi**2*sw) - (cw**2*ee**2*complex(0,1)*G**2*szi*txi)/(72.*cmath.pi**2*sw) - (czi*ee**2*complex(0,1)*G**2*szi*txi)/(72.*cmath.pi**2*sw) + (czi*ee**2*complex(0,1)*G**2*szi*txi)/(96.*cw**2*cmath.pi**2*sw) + (ee**2*complex(0,1)*G**2*sw*szi*txi)/(108.*cmath.pi**2) - (chi*ee**2*complex(0,1)*G**2*sw*szi*txi)/(27.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw*szi*txi)/(288.*cw**2*cmath.pi**2) + (chi*ee**2*complex(0,1)*G**2*sw*szi*txi)/(36.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw*szi*txi)/(27.*cmath.pi**2) - (czi*ee**2*complex(0,1)*G**2*sw*szi*txi)/(36.*cw**2*cmath.pi**2) + (5*ee**2*complex(0,1)*G**2*sw**3*szi*txi)/(216.*cw**2*cmath.pi**2) - (chi*ee**2*complex(0,1)*G**2*sw**3*szi*txi)/(27.*cw**2*cmath.pi**2) + (czi*ee**2*complex(0,1)*G**2*sw**3*szi*txi)/(27.*cw**2*cmath.pi**2) - (ee**2*complex(0,1)*G**2*szi**2*txi**2)/(72.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*szi**2*txi**2)/(192.*cw**2*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2*szi**2*txi**2)/(54.*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**2*szi**2*txi**2)/(27.*cmath.pi**2) - (ee**2*complex(0,1)*G**2*sw**2*szi**2*txi**2)/(72.*cw**2*cmath.pi**2) + (ee**2*complex(0,1)*G**2*sw**4*szi**2*txi**2)/(54.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_145_43 = Coupling(name = 'R2GC_145_43',
                       value = '(ee*complex(0,1)*G**2*szi)/(12.*cw*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2*txi)/(12.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_146_44 = Coupling(name = 'R2GC_146_44',
                       value = '-(ee*complex(0,1)*G**2*sw*szi)/(9.*cw*cmath.pi**2) + (cw*czi*ee*complex(0,1)*G**2*txi)/(9.*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw**2*txi)/(9.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_147_45 = Coupling(name = 'R2GC_147_45',
                       value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi)/(9.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi)/(9.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_148_46 = Coupling(name = 'R2GC_148_46',
                       value = '(chi*ee*complex(0,1)*G**2)/(12.*cw*cmath.pi**2*sw) - (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2)/(12.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*szi*txi)/(12.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi)/(9.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi)/(9.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_149_47 = Coupling(name = 'R2GC_149_47',
                       value = '-(ee*complex(0,1)*G**2*szi)/(12.*cw*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2*txi)/(12.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_150_48 = Coupling(name = 'R2GC_150_48',
                       value = '(ee*complex(0,1)*G**2*sw*szi)/(18.*cw*cmath.pi**2) - (cw*czi*ee*complex(0,1)*G**2*txi)/(18.*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw**2*txi)/(18.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_151_49 = Coupling(name = 'R2GC_151_49',
                       value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_152_50 = Coupling(name = 'R2GC_152_50',
                       value = '-(chi*ee*complex(0,1)*G**2)/(12.*cw*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2)/(12.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*szi*txi)/(12.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_157_51 = Coupling(name = 'R2GC_157_51',
                       value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_158_52 = Coupling(name = 'R2GC_158_52',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_178_53 = Coupling(name = 'R2GC_178_53',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_178_54 = Coupling(name = 'R2GC_178_54',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_179_55 = Coupling(name = 'R2GC_179_55',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_179_56 = Coupling(name = 'R2GC_179_56',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_180_57 = Coupling(name = 'R2GC_180_57',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_180_58 = Coupling(name = 'R2GC_180_58',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_181_59 = Coupling(name = 'R2GC_181_59',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_182_60 = Coupling(name = 'R2GC_182_60',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_182_61 = Coupling(name = 'R2GC_182_61',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_184_62 = Coupling(name = 'R2GC_184_62',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_184_63 = Coupling(name = 'R2GC_184_63',
                       value = '(complex(0,1)*G**4)/(4.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_185_64 = Coupling(name = 'R2GC_185_64',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_185_65 = Coupling(name = 'R2GC_185_65',
                       value = '(-23*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_190_66 = Coupling(name = 'R2GC_190_66',
                       value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_195_67 = Coupling(name = 'R2GC_195_67',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_200_68 = Coupling(name = 'R2GC_200_68',
                       value = '-(G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_201_69 = Coupling(name = 'R2GC_201_69',
                       value = '(complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2)) - (cal*complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2)) + (chi*complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_202_70 = Coupling(name = 'R2GC_202_70',
                       value = '-(complex(0,1)*G**2*sal*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_204_71 = Coupling(name = 'R2GC_204_71',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_204_72 = Coupling(name = 'R2GC_204_72',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_207_73 = Coupling(name = 'R2GC_207_73',
                       value = '-G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_207_74 = Coupling(name = 'R2GC_207_74',
                       value = '(-11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_210_75 = Coupling(name = 'R2GC_210_75',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_210_76 = Coupling(name = 'R2GC_210_76',
                       value = '(19*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_211_77 = Coupling(name = 'R2GC_211_77',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_212_78 = Coupling(name = 'R2GC_212_78',
                       value = '(31*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_213_79 = Coupling(name = 'R2GC_213_79',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_214_80 = Coupling(name = 'R2GC_214_80',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_215_81 = Coupling(name = 'R2GC_215_81',
                       value = '(7*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_219_82 = Coupling(name = 'R2GC_219_82',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_225_83 = Coupling(name = 'R2GC_225_83',
                       value = '(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_226_84 = Coupling(name = 'R2GC_226_84',
                       value = '-(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_227_85 = Coupling(name = 'R2GC_227_85',
                       value = '(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_228_86 = Coupling(name = 'R2GC_228_86',
                       value = '-(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_229_87 = Coupling(name = 'R2GC_229_87',
                       value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_230_88 = Coupling(name = 'R2GC_230_88',
                       value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) - (cal*complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) + (chi*complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_231_89 = Coupling(name = 'R2GC_231_89',
                       value = '-(complex(0,1)*G**2*sal*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_163_1 = Coupling(name = 'UVGC_163_1',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_164_2 = Coupling(name = 'UVGC_164_2',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_165_3 = Coupling(name = 'UVGC_165_3',
                      value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_167_4 = Coupling(name = 'UVGC_167_4',
                      value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_175_5 = Coupling(name = 'UVGC_175_5',
                      value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_175_6 = Coupling(name = 'UVGC_175_6',
                      value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_176_7 = Coupling(name = 'UVGC_176_7',
                      value = {-1:'(9*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_176_8 = Coupling(name = 'UVGC_176_8',
                      value = {-1:'-G**3/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_177_9 = Coupling(name = 'UVGC_177_9',
                      value = {-1:'(-9*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_177_10 = Coupling(name = 'UVGC_177_10',
                       value = {-1:'G**3/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_178_11 = Coupling(name = 'UVGC_178_11',
                       value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_178_12 = Coupling(name = 'UVGC_178_12',
                       value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_179_13 = Coupling(name = 'UVGC_179_13',
                       value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_179_14 = Coupling(name = 'UVGC_179_14',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_181_15 = Coupling(name = 'UVGC_181_15',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_181_16 = Coupling(name = 'UVGC_181_16',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_182_17 = Coupling(name = 'UVGC_182_17',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_182_18 = Coupling(name = 'UVGC_182_18',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_183_19 = Coupling(name = 'UVGC_183_19',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_183_20 = Coupling(name = 'UVGC_183_20',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_183_21 = Coupling(name = 'UVGC_183_21',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_183_22 = Coupling(name = 'UVGC_183_22',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_183_23 = Coupling(name = 'UVGC_183_23',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_184_24 = Coupling(name = 'UVGC_184_24',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_184_25 = Coupling(name = 'UVGC_184_25',
                       value = {-1:'(47*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_185_26 = Coupling(name = 'UVGC_185_26',
                       value = {-1:'(-253*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_185_27 = Coupling(name = 'UVGC_185_27',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_190_28 = Coupling(name = 'UVGC_190_28',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_190_29 = Coupling(name = 'UVGC_190_29',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_192_30 = Coupling(name = 'UVGC_192_30',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_193_31 = Coupling(name = 'UVGC_193_31',
                       value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2)/(36.*cmath.pi**2) )',0:'( (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) ) - (ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_194_32 = Coupling(name = 'UVGC_194_32',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MB else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_195_33 = Coupling(name = 'UVGC_195_33',
                       value = {-1:'( (complex(0,1)*G**2*MB)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MB)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MB)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/cmath.pi**2 if MB else (complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MB)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_196_34 = Coupling(name = 'UVGC_196_34',
                       value = {-1:'( -(ee*complex(0,1)*G**2*szi)/(12.*cw*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2*txi)/(12.*cw*cmath.pi**2) if MB else (ee*complex(0,1)*G**2*szi)/(24.*cw*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2*txi)/(24.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*szi)/(24.*cw*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2*txi)/(24.*cw*cmath.pi**2)',0:'( (-5*ee*complex(0,1)*G**2*szi)/(24.*cw*cmath.pi**2*sw) + (5*czi*ee*complex(0,1)*G**2*txi)/(24.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*szi*reglog(MB/MU_R))/(4.*cw*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2*txi*reglog(MB/MU_R))/(4.*cw*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2*szi)/(24.*cw*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2*txi)/(24.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*szi)/(24.*cw*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2*txi)/(24.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_197_35 = Coupling(name = 'UVGC_197_35',
                       value = {-1:'( (ee*complex(0,1)*G**2*sw*szi)/(18.*cw*cmath.pi**2) - (cw*czi*ee*complex(0,1)*G**2*txi)/(18.*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw**2*txi)/(18.*cw*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2*sw*szi)/(36.*cw*cmath.pi**2) + (cw*czi*ee*complex(0,1)*G**2*txi)/(36.*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw**2*txi)/(36.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw*szi)/(36.*cw*cmath.pi**2) - (cw*czi*ee*complex(0,1)*G**2*txi)/(36.*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw**2*txi)/(36.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw*szi)/(36.*cw*cmath.pi**2) - (5*cw*czi*ee*complex(0,1)*G**2*txi)/(36.*cmath.pi**2) - (5*czi*ee*complex(0,1)*G**2*sw**2*txi)/(36.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*szi*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) + (cw*czi*ee*complex(0,1)*G**2*txi*reglog(MB/MU_R))/(6.*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw**2*txi*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) if MB else (ee*complex(0,1)*G**2*sw*szi)/(36.*cw*cmath.pi**2) - (cw*czi*ee*complex(0,1)*G**2*txi)/(36.*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw**2*txi)/(36.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw*szi)/(36.*cw*cmath.pi**2) + (cw*czi*ee*complex(0,1)*G**2*txi)/(36.*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw**2*txi)/(36.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_198_36 = Coupling(name = 'UVGC_198_36',
                       value = {-1:'( -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2) if MB else (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi)/(36.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi)/(36.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi)/(36.*cw*cmath.pi**2)',0:'( (-5*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (5*chi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (5*czi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (5*cw*ee*complex(0,1)*G**2*szi*txi)/(36.*cmath.pi**2) - (5*ee*complex(0,1)*G**2*sw**2*szi*txi)/(36.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi*reglog(MB/MU_R))/(6.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi)/(36.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi)/(36.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi)/(36.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_199_37 = Coupling(name = 'UVGC_199_37',
                       value = {-1:'( -(chi*ee*complex(0,1)*G**2)/(12.*cw*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2)/(12.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*szi*txi)/(12.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2) if MB else (chi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*szi*txi)/(24.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi)/(36.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi)/(36.*cw*cmath.pi**2) ) - (chi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*szi*txi)/(24.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi)/(36.*cw*cmath.pi**2)',0:'( (-5*chi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) + (5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*czi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (5*chi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (5*czi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (5*ee*complex(0,1)*G**2*szi*txi)/(24.*cw*cmath.pi**2) - (5*cw*ee*complex(0,1)*G**2*szi*txi)/(36.*cmath.pi**2) - (5*ee*complex(0,1)*G**2*sw**2*szi*txi)/(36.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cw*cmath.pi**2*sw) - (cw*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cw*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(12.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*szi*txi*reglog(MB/MU_R))/(4.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi*reglog(MB/MU_R))/(6.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) if MB else -(chi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*szi*txi)/(24.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi)/(36.*cw*cmath.pi**2) ) + (chi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*szi*txi)/(24.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi)/(36.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi)/(36.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_200_38 = Coupling(name = 'UVGC_200_38',
                       value = {-1:'( -(G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) + (G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else -(G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_201_39 = Coupling(name = 'UVGC_201_39',
                       value = {-1:'( (complex(0,1)*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) - (cal*complex(0,1)*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) + (chi*complex(0,1)*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) + (cal*complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) - (chi*complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2)) - (cal*complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2)) + (chi*complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) - (3*cal*complex(0,1)*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) + (3*chi*complex(0,1)*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) + (cal*complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) - (chi*complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) - (cal*complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) + (chi*complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) + (cal*complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) - (chi*complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_202_40 = Coupling(name = 'UVGC_202_40',
                       value = {-1:'( -(complex(0,1)*G**2*sal*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*sal*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*sal*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*complex(0,1)*G**2*sal*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*sal*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*sal*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*sal*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_203_41 = Coupling(name = 'UVGC_203_41',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_203_42 = Coupling(name = 'UVGC_203_42',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_204_43 = Coupling(name = 'UVGC_204_43',
                       value = {-1:'( 0 if MB else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':3})

UVGC_204_44 = Coupling(name = 'UVGC_204_44',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_204_45 = Coupling(name = 'UVGC_204_45',
                       value = {-1:'(51*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_204_46 = Coupling(name = 'UVGC_204_46',
                       value = {-1:'( 0 if MT else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_205_47 = Coupling(name = 'UVGC_205_47',
                       value = {-1:'(21*G**3)/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_205_48 = Coupling(name = 'UVGC_205_48',
                       value = {-1:'G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_206_49 = Coupling(name = 'UVGC_206_49',
                       value = {-1:'(33*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_206_50 = Coupling(name = 'UVGC_206_50',
                       value = {-1:'(3*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_207_51 = Coupling(name = 'UVGC_207_51',
                       value = {-1:'( 0 if MB else G**3/(16.*cmath.pi**2) ) - G**3/(24.*cmath.pi**2)',0:'( (G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':3})

UVGC_207_52 = Coupling(name = 'UVGC_207_52',
                       value = {-1:'G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_207_53 = Coupling(name = 'UVGC_207_53',
                       value = {-1:'(-33*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_207_54 = Coupling(name = 'UVGC_207_54',
                       value = {-1:'(-3*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_207_55 = Coupling(name = 'UVGC_207_55',
                       value = {-1:'( 0 if MT else G**3/(16.*cmath.pi**2) ) - G**3/(24.*cmath.pi**2)',0:'( (G**3*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_208_56 = Coupling(name = 'UVGC_208_56',
                       value = {-1:'(-21*G**3)/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_208_57 = Coupling(name = 'UVGC_208_57',
                       value = {-1:'-G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_209_58 = Coupling(name = 'UVGC_209_58',
                       value = {-1:'(-51*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_210_59 = Coupling(name = 'UVGC_210_59',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_210_60 = Coupling(name = 'UVGC_210_60',
                       value = {-1:'(147*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_210_61 = Coupling(name = 'UVGC_210_61',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_210_62 = Coupling(name = 'UVGC_210_62',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_211_63 = Coupling(name = 'UVGC_211_63',
                       value = {-1:'(147*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_211_64 = Coupling(name = 'UVGC_211_64',
                       value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_212_65 = Coupling(name = 'UVGC_212_65',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_212_66 = Coupling(name = 'UVGC_212_66',
                       value = {-1:'-(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_212_67 = Coupling(name = 'UVGC_212_67',
                       value = {-1:'(523*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_212_68 = Coupling(name = 'UVGC_212_68',
                       value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_212_69 = Coupling(name = 'UVGC_212_69',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_213_70 = Coupling(name = 'UVGC_213_70',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_213_71 = Coupling(name = 'UVGC_213_71',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_213_72 = Coupling(name = 'UVGC_213_72',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_213_73 = Coupling(name = 'UVGC_213_73',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_213_74 = Coupling(name = 'UVGC_213_74',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_214_75 = Coupling(name = 'UVGC_214_75',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_214_76 = Coupling(name = 'UVGC_214_76',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_215_77 = Coupling(name = 'UVGC_215_77',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_215_78 = Coupling(name = 'UVGC_215_78',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_215_79 = Coupling(name = 'UVGC_215_79',
                       value = {-1:'(-85*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_215_80 = Coupling(name = 'UVGC_215_80',
                       value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_215_81 = Coupling(name = 'UVGC_215_81',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_216_82 = Coupling(name = 'UVGC_216_82',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_217_83 = Coupling(name = 'UVGC_217_83',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) if MT else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2)/(18.*cmath.pi**2) ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_218_84 = Coupling(name = 'UVGC_218_84',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_219_85 = Coupling(name = 'UVGC_219_85',
                       value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MT)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2 if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_220_86 = Coupling(name = 'UVGC_220_86',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_220_87 = Coupling(name = 'UVGC_220_87',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_221_88 = Coupling(name = 'UVGC_221_88',
                       value = {-1:'( (ee*complex(0,1)*G**2*szi)/(12.*cw*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2*txi)/(12.*cw*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2*szi)/(24.*cw*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2*txi)/(24.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*szi)/(24.*cw*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2*txi)/(24.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*szi)/(24.*cw*cmath.pi**2*sw) - (5*czi*ee*complex(0,1)*G**2*txi)/(24.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*szi*reglog(MT/MU_R))/(4.*cw*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2*txi*reglog(MT/MU_R))/(4.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*szi)/(24.*cw*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2*txi)/(24.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*szi)/(24.*cw*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2*txi)/(24.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_222_89 = Coupling(name = 'UVGC_222_89',
                       value = {-1:'( -(ee*complex(0,1)*G**2*sw*szi)/(9.*cw*cmath.pi**2) + (cw*czi*ee*complex(0,1)*G**2*txi)/(9.*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw**2*txi)/(9.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw*szi)/(18.*cw*cmath.pi**2) - (cw*czi*ee*complex(0,1)*G**2*txi)/(18.*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw**2*txi)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw*szi)/(18.*cw*cmath.pi**2) + (cw*czi*ee*complex(0,1)*G**2*txi)/(18.*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw**2*txi)/(18.*cw*cmath.pi**2)',0:'( (-5*ee*complex(0,1)*G**2*sw*szi)/(18.*cw*cmath.pi**2) + (5*cw*czi*ee*complex(0,1)*G**2*txi)/(18.*cmath.pi**2) + (5*czi*ee*complex(0,1)*G**2*sw**2*txi)/(18.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*sw*szi*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) - (cw*czi*ee*complex(0,1)*G**2*txi*reglog(MT/MU_R))/(3.*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw**2*txi*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2*sw*szi)/(18.*cw*cmath.pi**2) + (cw*czi*ee*complex(0,1)*G**2*txi)/(18.*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw**2*txi)/(18.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw*szi)/(18.*cw*cmath.pi**2) - (cw*czi*ee*complex(0,1)*G**2*txi)/(18.*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw**2*txi)/(18.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_223_90 = Coupling(name = 'UVGC_223_90',
                       value = {-1:'( (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi)/(9.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi)/(9.*cw*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (5*chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (5*czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (5*cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) + (5*ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi*reglog(MT/MU_R))/(3.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_224_91 = Coupling(name = 'UVGC_224_91',
                       value = {-1:'( (chi*ee*complex(0,1)*G**2)/(12.*cw*cmath.pi**2*sw) - (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2)/(12.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*szi*txi)/(12.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi)/(9.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi)/(9.*cw*cmath.pi**2) if MT else -(chi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*szi*txi)/(24.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2) ) + (chi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*szi*txi)/(24.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2)',0:'( (5*chi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) - (5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (5*czi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (5*chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (5*czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (5*ee*complex(0,1)*G**2*szi*txi)/(24.*cw*cmath.pi**2) + (5*cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) + (5*ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cw*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cw*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*szi*txi*reglog(MT/MU_R))/(4.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi*reglog(MT/MU_R))/(3.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (chi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (czi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*szi*txi)/(24.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2) ) - (chi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (czi*ee*complex(0,1)*G**2)/(24.*cw*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (chi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (czi*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*szi*txi)/(24.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*szi*txi)/(18.*cmath.pi**2) - (ee*complex(0,1)*G**2*sw**2*szi*txi)/(18.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_225_92 = Coupling(name = 'UVGC_225_92',
                       value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MB else -(G**2*yb)/(24.*cmath.pi**2) )',0:'( (13*G**2*yb)/(24.*cmath.pi**2) - (3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yb)/(24.*cmath.pi**2) ) - (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_225_93 = Coupling(name = 'UVGC_225_93',
                       value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MT else -(G**2*yb)/(24.*cmath.pi**2) )',0:'( (5*G**2*yb)/(24.*cmath.pi**2) - (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yb)/(24.*cmath.pi**2) ) - (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_225_94 = Coupling(name = 'UVGC_225_94',
                       value = {-1:'(G**2*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_226_95 = Coupling(name = 'UVGC_226_95',
                       value = {-1:'( -(G**2*yb)/(12.*cmath.pi**2) if MB else (G**2*yb)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yb)/(24.*cmath.pi**2) + (3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yb)/(24.*cmath.pi**2) ) + (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_226_96 = Coupling(name = 'UVGC_226_96',
                       value = {-1:'( -(G**2*yb)/(12.*cmath.pi**2) if MT else (G**2*yb)/(24.*cmath.pi**2) )',0:'( (-5*G**2*yb)/(24.*cmath.pi**2) + (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yb)/(24.*cmath.pi**2) ) + (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_226_97 = Coupling(name = 'UVGC_226_97',
                       value = {-1:'-(G**2*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_227_98 = Coupling(name = 'UVGC_227_98',
                       value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MB else -(G**2*yt)/(24.*cmath.pi**2) )',0:'( (5*G**2*yt)/(24.*cmath.pi**2) - (G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_227_99 = Coupling(name = 'UVGC_227_99',
                       value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) )',0:'( (13*G**2*yt)/(24.*cmath.pi**2) - (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_227_100 = Coupling(name = 'UVGC_227_100',
                        value = {-1:'(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_228_101 = Coupling(name = 'UVGC_228_101',
                        value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MB else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-5*G**2*yt)/(24.*cmath.pi**2) + (G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yt)/(24.*cmath.pi**2) ) + (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_228_102 = Coupling(name = 'UVGC_228_102',
                        value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yt)/(24.*cmath.pi**2) + (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) ) + (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_228_103 = Coupling(name = 'UVGC_228_103',
                        value = {-1:'-(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_229_104 = Coupling(name = 'UVGC_229_104',
                        value = {-1:'( (G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_230_105 = Coupling(name = 'UVGC_230_105',
                        value = {-1:'( (complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) - (cal*complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) + (chi*complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) + (cal*complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) - (chi*complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) - (cal*complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) + (chi*complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (3*cal*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) + (3*chi*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) + (cal*complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) - (chi*complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) - (cal*complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) + (chi*complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) + (cal*complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) - (chi*complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_231_106 = Coupling(name = 'UVGC_231_106',
                        value = {-1:'( -(complex(0,1)*G**2*sal*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*sal*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*sal*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*complex(0,1)*G**2*sal*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*sal*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*sal*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*sal*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

