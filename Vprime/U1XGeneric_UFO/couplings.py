# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (November 20, 2012)
# Date: Fri 14 Jan 2022 00:06:59


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_7 = Coupling(name = 'GC_7',
                value = '-G',
                order = {'QCD':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'complex(0,1)*G',
                order = {'QCD':1})

GC_9 = Coupling(name = 'GC_9',
                value = 'complex(0,1)*G**2',
                order = {'QCD':2})

GC_10 = Coupling(name = 'GC_10',
                 value = 'I13a11',
                 order = {'QED':1})

GC_11 = Coupling(name = 'GC_11',
                 value = 'I13a12',
                 order = {'QED':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'I13a13',
                 order = {'QED':1})

GC_13 = Coupling(name = 'GC_13',
                 value = 'I13a21',
                 order = {'QED':1})

GC_14 = Coupling(name = 'GC_14',
                 value = 'I13a22',
                 order = {'QED':1})

GC_15 = Coupling(name = 'GC_15',
                 value = 'I13a23',
                 order = {'QED':1})

GC_16 = Coupling(name = 'GC_16',
                 value = 'I13a31',
                 order = {'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = 'I13a32',
                 order = {'QED':1})

GC_18 = Coupling(name = 'GC_18',
                 value = 'I13a33',
                 order = {'QED':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '-I14a11',
                 order = {'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '-I14a12',
                 order = {'QED':1})

GC_21 = Coupling(name = 'GC_21',
                 value = '-I14a13',
                 order = {'QED':1})

GC_22 = Coupling(name = 'GC_22',
                 value = '-I14a21',
                 order = {'QED':1})

GC_23 = Coupling(name = 'GC_23',
                 value = '-I14a22',
                 order = {'QED':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '-I14a23',
                 order = {'QED':1})

GC_25 = Coupling(name = 'GC_25',
                 value = '-I14a31',
                 order = {'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '-I14a32',
                 order = {'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '-I14a33',
                 order = {'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = 'I15a11',
                 order = {'QED':1})

GC_29 = Coupling(name = 'GC_29',
                 value = 'I15a12',
                 order = {'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = 'I15a13',
                 order = {'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = 'I15a21',
                 order = {'QED':1})

GC_32 = Coupling(name = 'GC_32',
                 value = 'I15a22',
                 order = {'QED':1})

GC_33 = Coupling(name = 'GC_33',
                 value = 'I15a23',
                 order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = 'I15a31',
                 order = {'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = 'I15a32',
                 order = {'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = 'I15a33',
                 order = {'QED':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '-I16a11',
                 order = {'QED':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '-I16a12',
                 order = {'QED':1})

GC_39 = Coupling(name = 'GC_39',
                 value = '-I16a13',
                 order = {'QED':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '-I16a21',
                 order = {'QED':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '-I16a22',
                 order = {'QED':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '-I16a23',
                 order = {'QED':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '-I16a31',
                 order = {'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '-I16a32',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-I16a33',
                 order = {'QED':1})

GC_46 = Coupling(name = 'GC_46',
                 value = 'I17a11',
                 order = {'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = 'I17a12',
                 order = {'QED':1})

GC_48 = Coupling(name = 'GC_48',
                 value = 'I17a13',
                 order = {'QED':1})

GC_49 = Coupling(name = 'GC_49',
                 value = 'I17a21',
                 order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = 'I17a22',
                 order = {'QED':1})

GC_51 = Coupling(name = 'GC_51',
                 value = 'I17a23',
                 order = {'QED':1})

GC_52 = Coupling(name = 'GC_52',
                 value = 'I17a31',
                 order = {'QED':1})

GC_53 = Coupling(name = 'GC_53',
                 value = 'I17a32',
                 order = {'QED':1})

GC_54 = Coupling(name = 'GC_54',
                 value = 'I17a33',
                 order = {'QED':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '-I18a11',
                 order = {'QED':1})

GC_56 = Coupling(name = 'GC_56',
                 value = '-I18a12',
                 order = {'QED':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '-I18a13',
                 order = {'QED':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '-I18a21',
                 order = {'QED':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '-I18a22',
                 order = {'QED':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '-I18a23',
                 order = {'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '-I18a31',
                 order = {'QED':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '-I18a32',
                 order = {'QED':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '-I18a33',
                 order = {'QED':1})

GC_64 = Coupling(name = 'GC_64',
                 value = 'I19a11',
                 order = {'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = 'I19a12',
                 order = {'QED':1})

GC_66 = Coupling(name = 'GC_66',
                 value = 'I19a13',
                 order = {'QED':1})

GC_67 = Coupling(name = 'GC_67',
                 value = 'I19a21',
                 order = {'QED':1})

GC_68 = Coupling(name = 'GC_68',
                 value = 'I19a22',
                 order = {'QED':1})

GC_69 = Coupling(name = 'GC_69',
                 value = 'I19a23',
                 order = {'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = 'I19a31',
                 order = {'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = 'I19a32',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = 'I19a33',
                 order = {'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '-I20a11',
                 order = {'QED':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '-I20a12',
                 order = {'QED':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '-I20a13',
                 order = {'QED':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '-I20a21',
                 order = {'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '-I20a22',
                 order = {'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '-I20a23',
                 order = {'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '-I20a31',
                 order = {'QED':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '-I20a32',
                 order = {'QED':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '-I20a33',
                 order = {'QED':1})

GC_82 = Coupling(name = 'GC_82',
                 value = 'I21a11',
                 order = {'QED':1})

GC_83 = Coupling(name = 'GC_83',
                 value = 'I21a12',
                 order = {'QED':1})

GC_84 = Coupling(name = 'GC_84',
                 value = 'I21a13',
                 order = {'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = 'I21a21',
                 order = {'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = 'I21a22',
                 order = {'QED':1})

GC_87 = Coupling(name = 'GC_87',
                 value = 'I21a23',
                 order = {'QED':1})

GC_88 = Coupling(name = 'GC_88',
                 value = 'I21a31',
                 order = {'QED':1})

GC_89 = Coupling(name = 'GC_89',
                 value = 'I21a32',
                 order = {'QED':1})

GC_90 = Coupling(name = 'GC_90',
                 value = 'I21a33',
                 order = {'QED':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '-I22a11',
                 order = {'QED':1})

GC_92 = Coupling(name = 'GC_92',
                 value = '-I22a12',
                 order = {'QED':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '-I22a13',
                 order = {'QED':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '-I22a21',
                 order = {'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '-I22a22',
                 order = {'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '-I22a23',
                 order = {'QED':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '-I22a31',
                 order = {'QED':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '-I22a32',
                 order = {'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '-I22a33',
                 order = {'QED':1})

GC_100 = Coupling(name = 'GC_100',
                  value = 'I23a11',
                  order = {'QED':1})

GC_101 = Coupling(name = 'GC_101',
                  value = 'I23a12',
                  order = {'QED':1})

GC_102 = Coupling(name = 'GC_102',
                  value = 'I23a13',
                  order = {'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = 'I23a21',
                  order = {'QED':1})

GC_104 = Coupling(name = 'GC_104',
                  value = 'I23a22',
                  order = {'QED':1})

GC_105 = Coupling(name = 'GC_105',
                  value = 'I23a23',
                  order = {'QED':1})

GC_106 = Coupling(name = 'GC_106',
                  value = 'I23a31',
                  order = {'QED':1})

GC_107 = Coupling(name = 'GC_107',
                  value = 'I23a32',
                  order = {'QED':1})

GC_108 = Coupling(name = 'GC_108',
                  value = 'I23a33',
                  order = {'QED':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '-I24a11',
                  order = {'QED':1})

GC_110 = Coupling(name = 'GC_110',
                  value = '-I24a12',
                  order = {'QED':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '-I24a13',
                  order = {'QED':1})

GC_112 = Coupling(name = 'GC_112',
                  value = '-I24a21',
                  order = {'QED':1})

GC_113 = Coupling(name = 'GC_113',
                  value = '-I24a22',
                  order = {'QED':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '-I24a23',
                  order = {'QED':1})

GC_115 = Coupling(name = 'GC_115',
                  value = '-I24a31',
                  order = {'QED':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '-I24a32',
                  order = {'QED':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '-I24a33',
                  order = {'QED':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '-4*complex(0,1)*lam1',
                  order = {'QED':2})

GC_119 = Coupling(name = 'GC_119',
                  value = '-(Ca*complex(0,1)*I30a11)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I32a11)/(2.*cmath.sqrt(2)) - (complex(0,1)*I25a11*Sa)/cmath.sqrt(2) - (complex(0,1)*I26a11*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_120 = Coupling(name = 'GC_120',
                  value = '-(Ca*complex(0,1)*I30a12)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I32a12)/(2.*cmath.sqrt(2)) - (complex(0,1)*I25a12*Sa)/cmath.sqrt(2) - (complex(0,1)*I26a12*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '-(Ca*complex(0,1)*I30a13)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I32a13)/(2.*cmath.sqrt(2)) - (complex(0,1)*I25a13*Sa)/cmath.sqrt(2) - (complex(0,1)*I26a13*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '-(Ca*complex(0,1)*I30a21)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I32a21)/(2.*cmath.sqrt(2)) - (complex(0,1)*I25a21*Sa)/cmath.sqrt(2) - (complex(0,1)*I26a21*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '-(Ca*complex(0,1)*I30a22)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I32a22)/(2.*cmath.sqrt(2)) - (complex(0,1)*I25a22*Sa)/cmath.sqrt(2) - (complex(0,1)*I26a22*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '-(Ca*complex(0,1)*I30a23)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I32a23)/(2.*cmath.sqrt(2)) - (complex(0,1)*I25a23*Sa)/cmath.sqrt(2) - (complex(0,1)*I26a23*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '-(Ca*complex(0,1)*I30a31)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I32a31)/(2.*cmath.sqrt(2)) - (complex(0,1)*I25a31*Sa)/cmath.sqrt(2) - (complex(0,1)*I26a31*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_126 = Coupling(name = 'GC_126',
                  value = '-(Ca*complex(0,1)*I30a32)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I32a32)/(2.*cmath.sqrt(2)) - (complex(0,1)*I25a32*Sa)/cmath.sqrt(2) - (complex(0,1)*I26a32*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '-(Ca*complex(0,1)*I30a33)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I32a33)/(2.*cmath.sqrt(2)) - (complex(0,1)*I25a33*Sa)/cmath.sqrt(2) - (complex(0,1)*I26a33*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '-(Ca*complex(0,1)*I29a11)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I31a11)/(2.*cmath.sqrt(2)) - (complex(0,1)*I27a11*Sa)/cmath.sqrt(2) - (complex(0,1)*I28a11*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_129 = Coupling(name = 'GC_129',
                  value = '-(Ca*complex(0,1)*I29a12)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I31a12)/(2.*cmath.sqrt(2)) - (complex(0,1)*I27a12*Sa)/cmath.sqrt(2) - (complex(0,1)*I28a12*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_130 = Coupling(name = 'GC_130',
                  value = '-(Ca*complex(0,1)*I29a13)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I31a13)/(2.*cmath.sqrt(2)) - (complex(0,1)*I27a13*Sa)/cmath.sqrt(2) - (complex(0,1)*I28a13*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_131 = Coupling(name = 'GC_131',
                  value = '-(Ca*complex(0,1)*I29a21)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I31a21)/(2.*cmath.sqrt(2)) - (complex(0,1)*I27a21*Sa)/cmath.sqrt(2) - (complex(0,1)*I28a21*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '-(Ca*complex(0,1)*I29a22)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I31a22)/(2.*cmath.sqrt(2)) - (complex(0,1)*I27a22*Sa)/cmath.sqrt(2) - (complex(0,1)*I28a22*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '-(Ca*complex(0,1)*I29a23)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I31a23)/(2.*cmath.sqrt(2)) - (complex(0,1)*I27a23*Sa)/cmath.sqrt(2) - (complex(0,1)*I28a23*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_134 = Coupling(name = 'GC_134',
                  value = '-(Ca*complex(0,1)*I29a31)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I31a31)/(2.*cmath.sqrt(2)) - (complex(0,1)*I27a31*Sa)/cmath.sqrt(2) - (complex(0,1)*I28a31*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_135 = Coupling(name = 'GC_135',
                  value = '-(Ca*complex(0,1)*I29a32)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I31a32)/(2.*cmath.sqrt(2)) - (complex(0,1)*I27a32*Sa)/cmath.sqrt(2) - (complex(0,1)*I28a32*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_136 = Coupling(name = 'GC_136',
                  value = '-(Ca*complex(0,1)*I29a33)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I31a33)/(2.*cmath.sqrt(2)) - (complex(0,1)*I27a33*Sa)/cmath.sqrt(2) - (complex(0,1)*I28a33*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_137 = Coupling(name = 'GC_137',
                  value = '-((Ca*complex(0,1)*I27a11)/cmath.sqrt(2)) - (Ca*complex(0,1)*I28a11)/cmath.sqrt(2) + (complex(0,1)*I29a11*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I31a11*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_138 = Coupling(name = 'GC_138',
                  value = '-((Ca*complex(0,1)*I27a12)/cmath.sqrt(2)) - (Ca*complex(0,1)*I28a12)/cmath.sqrt(2) + (complex(0,1)*I29a12*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I31a12*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_139 = Coupling(name = 'GC_139',
                  value = '-((Ca*complex(0,1)*I27a13)/cmath.sqrt(2)) - (Ca*complex(0,1)*I28a13)/cmath.sqrt(2) + (complex(0,1)*I29a13*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I31a13*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '-((Ca*complex(0,1)*I27a21)/cmath.sqrt(2)) - (Ca*complex(0,1)*I28a21)/cmath.sqrt(2) + (complex(0,1)*I29a21*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I31a21*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_141 = Coupling(name = 'GC_141',
                  value = '-((Ca*complex(0,1)*I27a22)/cmath.sqrt(2)) - (Ca*complex(0,1)*I28a22)/cmath.sqrt(2) + (complex(0,1)*I29a22*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I31a22*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_142 = Coupling(name = 'GC_142',
                  value = '-((Ca*complex(0,1)*I27a23)/cmath.sqrt(2)) - (Ca*complex(0,1)*I28a23)/cmath.sqrt(2) + (complex(0,1)*I29a23*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I31a23*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_143 = Coupling(name = 'GC_143',
                  value = '-((Ca*complex(0,1)*I27a31)/cmath.sqrt(2)) - (Ca*complex(0,1)*I28a31)/cmath.sqrt(2) + (complex(0,1)*I29a31*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I31a31*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '-((Ca*complex(0,1)*I27a32)/cmath.sqrt(2)) - (Ca*complex(0,1)*I28a32)/cmath.sqrt(2) + (complex(0,1)*I29a32*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I31a32*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_145 = Coupling(name = 'GC_145',
                  value = '-((Ca*complex(0,1)*I27a33)/cmath.sqrt(2)) - (Ca*complex(0,1)*I28a33)/cmath.sqrt(2) + (complex(0,1)*I29a33*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I31a33*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '-((Ca*complex(0,1)*I25a11)/cmath.sqrt(2)) - (Ca*complex(0,1)*I26a11)/cmath.sqrt(2) + (complex(0,1)*I30a11*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I32a11*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_147 = Coupling(name = 'GC_147',
                  value = '-((Ca*complex(0,1)*I25a12)/cmath.sqrt(2)) - (Ca*complex(0,1)*I26a12)/cmath.sqrt(2) + (complex(0,1)*I30a12*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I32a12*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '-((Ca*complex(0,1)*I25a13)/cmath.sqrt(2)) - (Ca*complex(0,1)*I26a13)/cmath.sqrt(2) + (complex(0,1)*I30a13*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I32a13*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_149 = Coupling(name = 'GC_149',
                  value = '-((Ca*complex(0,1)*I25a21)/cmath.sqrt(2)) - (Ca*complex(0,1)*I26a21)/cmath.sqrt(2) + (complex(0,1)*I30a21*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I32a21*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '-((Ca*complex(0,1)*I25a22)/cmath.sqrt(2)) - (Ca*complex(0,1)*I26a22)/cmath.sqrt(2) + (complex(0,1)*I30a22*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I32a22*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '-((Ca*complex(0,1)*I25a23)/cmath.sqrt(2)) - (Ca*complex(0,1)*I26a23)/cmath.sqrt(2) + (complex(0,1)*I30a23*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I32a23*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_152 = Coupling(name = 'GC_152',
                  value = '-((Ca*complex(0,1)*I25a31)/cmath.sqrt(2)) - (Ca*complex(0,1)*I26a31)/cmath.sqrt(2) + (complex(0,1)*I30a31*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I32a31*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_153 = Coupling(name = 'GC_153',
                  value = '-((Ca*complex(0,1)*I25a32)/cmath.sqrt(2)) - (Ca*complex(0,1)*I26a32)/cmath.sqrt(2) + (complex(0,1)*I30a32*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I32a32*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_154 = Coupling(name = 'GC_154',
                  value = '-((Ca*complex(0,1)*I25a33)/cmath.sqrt(2)) - (Ca*complex(0,1)*I26a33)/cmath.sqrt(2) + (complex(0,1)*I30a33*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I32a33*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_155 = Coupling(name = 'GC_155',
                  value = '-(Ca*complex(0,1)*I39a11)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I40a11)/(2.*cmath.sqrt(2)) - (complex(0,1)*I33a11*Sa)/cmath.sqrt(2) - (complex(0,1)*I34a11*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_156 = Coupling(name = 'GC_156',
                  value = '-(Ca*complex(0,1)*I39a12)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I39a21)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I40a12)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I40a21)/(4.*cmath.sqrt(2)) - (complex(0,1)*I33a12*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I33a21*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I34a12*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I34a21*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_157 = Coupling(name = 'GC_157',
                  value = '-(Ca*complex(0,1)*I39a22)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I40a22)/(2.*cmath.sqrt(2)) - (complex(0,1)*I33a22*Sa)/cmath.sqrt(2) - (complex(0,1)*I34a22*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_158 = Coupling(name = 'GC_158',
                  value = '-(Ca*complex(0,1)*I39a13)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I39a31)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I40a13)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I40a31)/(4.*cmath.sqrt(2)) - (complex(0,1)*I33a13*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I33a31*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I34a13*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I34a31*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_159 = Coupling(name = 'GC_159',
                  value = '-(Ca*complex(0,1)*I39a23)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I39a32)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I40a23)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I40a32)/(4.*cmath.sqrt(2)) - (complex(0,1)*I33a23*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I33a32*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I34a23*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I34a32*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_160 = Coupling(name = 'GC_160',
                  value = '-(Ca*complex(0,1)*I39a33)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I40a33)/(2.*cmath.sqrt(2)) - (complex(0,1)*I33a33*Sa)/cmath.sqrt(2) - (complex(0,1)*I34a33*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_161 = Coupling(name = 'GC_161',
                  value = '-(Ca*complex(0,1)*I37a11)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I38a11)/(2.*cmath.sqrt(2)) - (complex(0,1)*I35a11*Sa)/cmath.sqrt(2) - (complex(0,1)*I36a11*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_162 = Coupling(name = 'GC_162',
                  value = '-(Ca*complex(0,1)*I37a12)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I37a21)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I38a12)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I38a21)/(4.*cmath.sqrt(2)) - (complex(0,1)*I35a12*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I35a21*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I36a12*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I36a21*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_163 = Coupling(name = 'GC_163',
                  value = '-(Ca*complex(0,1)*I37a22)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I38a22)/(2.*cmath.sqrt(2)) - (complex(0,1)*I35a22*Sa)/cmath.sqrt(2) - (complex(0,1)*I36a22*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_164 = Coupling(name = 'GC_164',
                  value = '-(Ca*complex(0,1)*I37a13)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I37a31)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I38a13)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I38a31)/(4.*cmath.sqrt(2)) - (complex(0,1)*I35a13*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I35a31*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I36a13*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I36a31*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_165 = Coupling(name = 'GC_165',
                  value = '-(Ca*complex(0,1)*I37a23)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I37a32)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I38a23)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I38a32)/(4.*cmath.sqrt(2)) - (complex(0,1)*I35a23*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I35a32*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I36a23*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I36a32*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_166 = Coupling(name = 'GC_166',
                  value = '-(Ca*complex(0,1)*I37a33)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I38a33)/(2.*cmath.sqrt(2)) - (complex(0,1)*I35a33*Sa)/cmath.sqrt(2) - (complex(0,1)*I36a33*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_167 = Coupling(name = 'GC_167',
                  value = '-((Ca*complex(0,1)*I35a11)/cmath.sqrt(2)) - (Ca*complex(0,1)*I36a11)/cmath.sqrt(2) + (complex(0,1)*I37a11*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I38a11*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_168 = Coupling(name = 'GC_168',
                  value = '-(Ca*complex(0,1)*I35a12)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I35a21)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I36a12)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I36a21)/(2.*cmath.sqrt(2)) + (complex(0,1)*I37a12*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I37a21*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I38a12*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I38a21*Sa)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_169 = Coupling(name = 'GC_169',
                  value = '-((Ca*complex(0,1)*I35a22)/cmath.sqrt(2)) - (Ca*complex(0,1)*I36a22)/cmath.sqrt(2) + (complex(0,1)*I37a22*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I38a22*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '-(Ca*complex(0,1)*I35a13)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I35a31)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I36a13)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I36a31)/(2.*cmath.sqrt(2)) + (complex(0,1)*I37a13*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I37a31*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I38a13*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I38a31*Sa)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_171 = Coupling(name = 'GC_171',
                  value = '-(Ca*complex(0,1)*I35a23)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I35a32)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I36a23)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I36a32)/(2.*cmath.sqrt(2)) + (complex(0,1)*I37a23*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I37a32*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I38a23*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I38a32*Sa)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '-((Ca*complex(0,1)*I35a33)/cmath.sqrt(2)) - (Ca*complex(0,1)*I36a33)/cmath.sqrt(2) + (complex(0,1)*I37a33*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I38a33*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_173 = Coupling(name = 'GC_173',
                  value = '-((Ca*complex(0,1)*I33a11)/cmath.sqrt(2)) - (Ca*complex(0,1)*I34a11)/cmath.sqrt(2) + (complex(0,1)*I39a11*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I40a11*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_174 = Coupling(name = 'GC_174',
                  value = '-(Ca*complex(0,1)*I33a12)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I33a21)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I34a12)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I34a21)/(2.*cmath.sqrt(2)) + (complex(0,1)*I39a12*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I39a21*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I40a12*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I40a21*Sa)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '-((Ca*complex(0,1)*I33a22)/cmath.sqrt(2)) - (Ca*complex(0,1)*I34a22)/cmath.sqrt(2) + (complex(0,1)*I39a22*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I40a22*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_176 = Coupling(name = 'GC_176',
                  value = '-(Ca*complex(0,1)*I33a13)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I33a31)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I34a13)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I34a31)/(2.*cmath.sqrt(2)) + (complex(0,1)*I39a13*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I39a31*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I40a13*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I40a31*Sa)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_177 = Coupling(name = 'GC_177',
                  value = '-(Ca*complex(0,1)*I33a23)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I33a32)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I34a23)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I34a32)/(2.*cmath.sqrt(2)) + (complex(0,1)*I39a23*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I39a32*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I40a23*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I40a32*Sa)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_178 = Coupling(name = 'GC_178',
                  value = '-((Ca*complex(0,1)*I33a33)/cmath.sqrt(2)) - (Ca*complex(0,1)*I34a33)/cmath.sqrt(2) + (complex(0,1)*I39a33*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I40a33*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '-(Ca*complex(0,1)*I47a11)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I48a11)/(2.*cmath.sqrt(2)) - (complex(0,1)*I41a11*Sa)/cmath.sqrt(2) - (complex(0,1)*I42a11*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_180 = Coupling(name = 'GC_180',
                  value = '-(Ca*complex(0,1)*I47a12)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I47a21)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I48a12)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I48a21)/(4.*cmath.sqrt(2)) - (complex(0,1)*I41a12*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I41a21*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I42a12*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I42a21*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_181 = Coupling(name = 'GC_181',
                  value = '-(Ca*complex(0,1)*I47a22)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I48a22)/(2.*cmath.sqrt(2)) - (complex(0,1)*I41a22*Sa)/cmath.sqrt(2) - (complex(0,1)*I42a22*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_182 = Coupling(name = 'GC_182',
                  value = '-(Ca*complex(0,1)*I47a13)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I47a31)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I48a13)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I48a31)/(4.*cmath.sqrt(2)) - (complex(0,1)*I41a13*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I41a31*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I42a13*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I42a31*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '-(Ca*complex(0,1)*I47a23)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I47a32)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I48a23)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I48a32)/(4.*cmath.sqrt(2)) - (complex(0,1)*I41a23*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I41a32*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I42a23*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I42a32*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '-(Ca*complex(0,1)*I47a33)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I48a33)/(2.*cmath.sqrt(2)) - (complex(0,1)*I41a33*Sa)/cmath.sqrt(2) - (complex(0,1)*I42a33*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_185 = Coupling(name = 'GC_185',
                  value = '-(Ca*complex(0,1)*I45a11)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I46a11)/(2.*cmath.sqrt(2)) - (complex(0,1)*I43a11*Sa)/cmath.sqrt(2) - (complex(0,1)*I44a11*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_186 = Coupling(name = 'GC_186',
                  value = '-(Ca*complex(0,1)*I45a12)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I45a21)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I46a12)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I46a21)/(4.*cmath.sqrt(2)) - (complex(0,1)*I43a12*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I43a21*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I44a12*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I44a21*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_187 = Coupling(name = 'GC_187',
                  value = '-(Ca*complex(0,1)*I45a22)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I46a22)/(2.*cmath.sqrt(2)) - (complex(0,1)*I43a22*Sa)/cmath.sqrt(2) - (complex(0,1)*I44a22*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_188 = Coupling(name = 'GC_188',
                  value = '-(Ca*complex(0,1)*I45a13)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I45a31)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I46a13)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I46a31)/(4.*cmath.sqrt(2)) - (complex(0,1)*I43a13*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I43a31*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I44a13*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I44a31*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_189 = Coupling(name = 'GC_189',
                  value = '-(Ca*complex(0,1)*I45a23)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I45a32)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I46a23)/(4.*cmath.sqrt(2)) - (Ca*complex(0,1)*I46a32)/(4.*cmath.sqrt(2)) - (complex(0,1)*I43a23*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I43a32*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I44a23*Sa)/(2.*cmath.sqrt(2)) - (complex(0,1)*I44a32*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '-(Ca*complex(0,1)*I45a33)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I46a33)/(2.*cmath.sqrt(2)) - (complex(0,1)*I43a33*Sa)/cmath.sqrt(2) - (complex(0,1)*I44a33*Sa)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_191 = Coupling(name = 'GC_191',
                  value = '-((Ca*complex(0,1)*I43a11)/cmath.sqrt(2)) - (Ca*complex(0,1)*I44a11)/cmath.sqrt(2) + (complex(0,1)*I45a11*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I46a11*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_192 = Coupling(name = 'GC_192',
                  value = '-(Ca*complex(0,1)*I43a12)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I43a21)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I44a12)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I44a21)/(2.*cmath.sqrt(2)) + (complex(0,1)*I45a12*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I45a21*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I46a12*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I46a21*Sa)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '-((Ca*complex(0,1)*I43a22)/cmath.sqrt(2)) - (Ca*complex(0,1)*I44a22)/cmath.sqrt(2) + (complex(0,1)*I45a22*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I46a22*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_194 = Coupling(name = 'GC_194',
                  value = '-(Ca*complex(0,1)*I43a13)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I43a31)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I44a13)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I44a31)/(2.*cmath.sqrt(2)) + (complex(0,1)*I45a13*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I45a31*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I46a13*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I46a31*Sa)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_195 = Coupling(name = 'GC_195',
                  value = '-(Ca*complex(0,1)*I43a23)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I43a32)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I44a23)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I44a32)/(2.*cmath.sqrt(2)) + (complex(0,1)*I45a23*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I45a32*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I46a23*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I46a32*Sa)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_196 = Coupling(name = 'GC_196',
                  value = '-((Ca*complex(0,1)*I43a33)/cmath.sqrt(2)) - (Ca*complex(0,1)*I44a33)/cmath.sqrt(2) + (complex(0,1)*I45a33*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I46a33*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_197 = Coupling(name = 'GC_197',
                  value = '-((Ca*complex(0,1)*I41a11)/cmath.sqrt(2)) - (Ca*complex(0,1)*I42a11)/cmath.sqrt(2) + (complex(0,1)*I47a11*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I48a11*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_198 = Coupling(name = 'GC_198',
                  value = '-(Ca*complex(0,1)*I41a12)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I41a21)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I42a12)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I42a21)/(2.*cmath.sqrt(2)) + (complex(0,1)*I47a12*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I47a21*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I48a12*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I48a21*Sa)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_199 = Coupling(name = 'GC_199',
                  value = '-((Ca*complex(0,1)*I41a22)/cmath.sqrt(2)) - (Ca*complex(0,1)*I42a22)/cmath.sqrt(2) + (complex(0,1)*I47a22*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I48a22*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_200 = Coupling(name = 'GC_200',
                  value = '-(Ca*complex(0,1)*I41a13)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I41a31)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I42a13)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I42a31)/(2.*cmath.sqrt(2)) + (complex(0,1)*I47a13*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I47a31*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I48a13*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I48a31*Sa)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_201 = Coupling(name = 'GC_201',
                  value = '-(Ca*complex(0,1)*I41a23)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I41a32)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I42a23)/(2.*cmath.sqrt(2)) - (Ca*complex(0,1)*I42a32)/(2.*cmath.sqrt(2)) + (complex(0,1)*I47a23*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I47a32*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I48a23*Sa)/(4.*cmath.sqrt(2)) + (complex(0,1)*I48a32*Sa)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_202 = Coupling(name = 'GC_202',
                  value = '-((Ca*complex(0,1)*I41a33)/cmath.sqrt(2)) - (Ca*complex(0,1)*I42a33)/cmath.sqrt(2) + (complex(0,1)*I47a33*Sa)/(2.*cmath.sqrt(2)) + (complex(0,1)*I48a33*Sa)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_203 = Coupling(name = 'GC_203',
                  value = '-2*Ca*complex(0,1)*lam1*Sa + Ca*complex(0,1)*lam3*Sa',
                  order = {'QED':2})

GC_204 = Coupling(name = 'GC_204',
                  value = '-(Ca**2*complex(0,1)*lam3) - 2*complex(0,1)*lam1*Sa**2',
                  order = {'QED':2})

GC_205 = Coupling(name = 'GC_205',
                  value = '-2*Ca**2*complex(0,1)*lam1 - complex(0,1)*lam3*Sa**2',
                  order = {'QED':2})

GC_206 = Coupling(name = 'GC_206',
                  value = '-6*Ca**3*complex(0,1)*lam1*Sa + 3*Ca**3*complex(0,1)*lam3*Sa + 6*Ca*complex(0,1)*lam2*Sa**3 - 3*Ca*complex(0,1)*lam3*Sa**3',
                  order = {'QED':2})

GC_207 = Coupling(name = 'GC_207',
                  value = '6*Ca**3*complex(0,1)*lam2*Sa - 3*Ca**3*complex(0,1)*lam3*Sa - 6*Ca*complex(0,1)*lam1*Sa**3 + 3*Ca*complex(0,1)*lam3*Sa**3',
                  order = {'QED':2})

GC_208 = Coupling(name = 'GC_208',
                  value = '-6*Ca**4*complex(0,1)*lam2 - 6*Ca**2*complex(0,1)*lam3*Sa**2 - 6*complex(0,1)*lam1*Sa**4',
                  order = {'QED':2})

GC_209 = Coupling(name = 'GC_209',
                  value = '-6*Ca**4*complex(0,1)*lam1 - 6*Ca**2*complex(0,1)*lam3*Sa**2 - 6*complex(0,1)*lam2*Sa**4',
                  order = {'QED':2})

GC_210 = Coupling(name = 'GC_210',
                  value = '-(Ca**4*complex(0,1)*lam3) - 6*Ca**2*complex(0,1)*lam1*Sa**2 - 6*Ca**2*complex(0,1)*lam2*Sa**2 + 4*Ca**2*complex(0,1)*lam3*Sa**2 - complex(0,1)*lam3*Sa**4',
                  order = {'QED':2})

GC_211 = Coupling(name = 'GC_211',
                  value = '-(cg*I30a11)/(2.*cmath.sqrt(2)) - (cg*I32a11)/(2.*cmath.sqrt(2)) - (I25a11*sg)/cmath.sqrt(2) - (I26a11*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_212 = Coupling(name = 'GC_212',
                  value = '-(cg*I30a12)/(2.*cmath.sqrt(2)) - (cg*I32a12)/(2.*cmath.sqrt(2)) - (I25a12*sg)/cmath.sqrt(2) - (I26a12*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_213 = Coupling(name = 'GC_213',
                  value = '-(cg*I30a13)/(2.*cmath.sqrt(2)) - (cg*I32a13)/(2.*cmath.sqrt(2)) - (I25a13*sg)/cmath.sqrt(2) - (I26a13*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_214 = Coupling(name = 'GC_214',
                  value = '-(cg*I30a21)/(2.*cmath.sqrt(2)) - (cg*I32a21)/(2.*cmath.sqrt(2)) - (I25a21*sg)/cmath.sqrt(2) - (I26a21*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_215 = Coupling(name = 'GC_215',
                  value = '-(cg*I30a22)/(2.*cmath.sqrt(2)) - (cg*I32a22)/(2.*cmath.sqrt(2)) - (I25a22*sg)/cmath.sqrt(2) - (I26a22*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '-(cg*I30a23)/(2.*cmath.sqrt(2)) - (cg*I32a23)/(2.*cmath.sqrt(2)) - (I25a23*sg)/cmath.sqrt(2) - (I26a23*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_217 = Coupling(name = 'GC_217',
                  value = '-(cg*I30a31)/(2.*cmath.sqrt(2)) - (cg*I32a31)/(2.*cmath.sqrt(2)) - (I25a31*sg)/cmath.sqrt(2) - (I26a31*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_218 = Coupling(name = 'GC_218',
                  value = '-(cg*I30a32)/(2.*cmath.sqrt(2)) - (cg*I32a32)/(2.*cmath.sqrt(2)) - (I25a32*sg)/cmath.sqrt(2) - (I26a32*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_219 = Coupling(name = 'GC_219',
                  value = '-(cg*I30a33)/(2.*cmath.sqrt(2)) - (cg*I32a33)/(2.*cmath.sqrt(2)) - (I25a33*sg)/cmath.sqrt(2) - (I26a33*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_220 = Coupling(name = 'GC_220',
                  value = '(cg*I29a11)/(2.*cmath.sqrt(2)) + (cg*I31a11)/(2.*cmath.sqrt(2)) + (I27a11*sg)/cmath.sqrt(2) + (I28a11*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_221 = Coupling(name = 'GC_221',
                  value = '(cg*I29a12)/(2.*cmath.sqrt(2)) + (cg*I31a12)/(2.*cmath.sqrt(2)) + (I27a12*sg)/cmath.sqrt(2) + (I28a12*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_222 = Coupling(name = 'GC_222',
                  value = '(cg*I29a13)/(2.*cmath.sqrt(2)) + (cg*I31a13)/(2.*cmath.sqrt(2)) + (I27a13*sg)/cmath.sqrt(2) + (I28a13*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_223 = Coupling(name = 'GC_223',
                  value = '(cg*I29a21)/(2.*cmath.sqrt(2)) + (cg*I31a21)/(2.*cmath.sqrt(2)) + (I27a21*sg)/cmath.sqrt(2) + (I28a21*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_224 = Coupling(name = 'GC_224',
                  value = '(cg*I29a22)/(2.*cmath.sqrt(2)) + (cg*I31a22)/(2.*cmath.sqrt(2)) + (I27a22*sg)/cmath.sqrt(2) + (I28a22*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_225 = Coupling(name = 'GC_225',
                  value = '(cg*I29a23)/(2.*cmath.sqrt(2)) + (cg*I31a23)/(2.*cmath.sqrt(2)) + (I27a23*sg)/cmath.sqrt(2) + (I28a23*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_226 = Coupling(name = 'GC_226',
                  value = '(cg*I29a31)/(2.*cmath.sqrt(2)) + (cg*I31a31)/(2.*cmath.sqrt(2)) + (I27a31*sg)/cmath.sqrt(2) + (I28a31*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_227 = Coupling(name = 'GC_227',
                  value = '(cg*I29a32)/(2.*cmath.sqrt(2)) + (cg*I31a32)/(2.*cmath.sqrt(2)) + (I27a32*sg)/cmath.sqrt(2) + (I28a32*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_228 = Coupling(name = 'GC_228',
                  value = '(cg*I29a33)/(2.*cmath.sqrt(2)) + (cg*I31a33)/(2.*cmath.sqrt(2)) + (I27a33*sg)/cmath.sqrt(2) + (I28a33*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_229 = Coupling(name = 'GC_229',
                  value = '-((cg*I27a11)/cmath.sqrt(2)) - (cg*I28a11)/cmath.sqrt(2) + (I29a11*sg)/(2.*cmath.sqrt(2)) + (I31a11*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_230 = Coupling(name = 'GC_230',
                  value = '-((cg*I27a12)/cmath.sqrt(2)) - (cg*I28a12)/cmath.sqrt(2) + (I29a12*sg)/(2.*cmath.sqrt(2)) + (I31a12*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_231 = Coupling(name = 'GC_231',
                  value = '-((cg*I27a13)/cmath.sqrt(2)) - (cg*I28a13)/cmath.sqrt(2) + (I29a13*sg)/(2.*cmath.sqrt(2)) + (I31a13*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_232 = Coupling(name = 'GC_232',
                  value = '-((cg*I27a21)/cmath.sqrt(2)) - (cg*I28a21)/cmath.sqrt(2) + (I29a21*sg)/(2.*cmath.sqrt(2)) + (I31a21*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_233 = Coupling(name = 'GC_233',
                  value = '-((cg*I27a22)/cmath.sqrt(2)) - (cg*I28a22)/cmath.sqrt(2) + (I29a22*sg)/(2.*cmath.sqrt(2)) + (I31a22*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_234 = Coupling(name = 'GC_234',
                  value = '-((cg*I27a23)/cmath.sqrt(2)) - (cg*I28a23)/cmath.sqrt(2) + (I29a23*sg)/(2.*cmath.sqrt(2)) + (I31a23*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_235 = Coupling(name = 'GC_235',
                  value = '-((cg*I27a31)/cmath.sqrt(2)) - (cg*I28a31)/cmath.sqrt(2) + (I29a31*sg)/(2.*cmath.sqrt(2)) + (I31a31*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_236 = Coupling(name = 'GC_236',
                  value = '-((cg*I27a32)/cmath.sqrt(2)) - (cg*I28a32)/cmath.sqrt(2) + (I29a32*sg)/(2.*cmath.sqrt(2)) + (I31a32*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_237 = Coupling(name = 'GC_237',
                  value = '-((cg*I27a33)/cmath.sqrt(2)) - (cg*I28a33)/cmath.sqrt(2) + (I29a33*sg)/(2.*cmath.sqrt(2)) + (I31a33*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_238 = Coupling(name = 'GC_238',
                  value = '(cg*I25a11)/cmath.sqrt(2) + (cg*I26a11)/cmath.sqrt(2) - (I30a11*sg)/(2.*cmath.sqrt(2)) - (I32a11*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_239 = Coupling(name = 'GC_239',
                  value = '(cg*I25a12)/cmath.sqrt(2) + (cg*I26a12)/cmath.sqrt(2) - (I30a12*sg)/(2.*cmath.sqrt(2)) - (I32a12*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_240 = Coupling(name = 'GC_240',
                  value = '(cg*I25a13)/cmath.sqrt(2) + (cg*I26a13)/cmath.sqrt(2) - (I30a13*sg)/(2.*cmath.sqrt(2)) - (I32a13*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_241 = Coupling(name = 'GC_241',
                  value = '(cg*I25a21)/cmath.sqrt(2) + (cg*I26a21)/cmath.sqrt(2) - (I30a21*sg)/(2.*cmath.sqrt(2)) - (I32a21*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_242 = Coupling(name = 'GC_242',
                  value = '(cg*I25a22)/cmath.sqrt(2) + (cg*I26a22)/cmath.sqrt(2) - (I30a22*sg)/(2.*cmath.sqrt(2)) - (I32a22*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_243 = Coupling(name = 'GC_243',
                  value = '(cg*I25a23)/cmath.sqrt(2) + (cg*I26a23)/cmath.sqrt(2) - (I30a23*sg)/(2.*cmath.sqrt(2)) - (I32a23*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_244 = Coupling(name = 'GC_244',
                  value = '(cg*I25a31)/cmath.sqrt(2) + (cg*I26a31)/cmath.sqrt(2) - (I30a31*sg)/(2.*cmath.sqrt(2)) - (I32a31*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_245 = Coupling(name = 'GC_245',
                  value = '(cg*I25a32)/cmath.sqrt(2) + (cg*I26a32)/cmath.sqrt(2) - (I30a32*sg)/(2.*cmath.sqrt(2)) - (I32a32*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_246 = Coupling(name = 'GC_246',
                  value = '(cg*I25a33)/cmath.sqrt(2) + (cg*I26a33)/cmath.sqrt(2) - (I30a33*sg)/(2.*cmath.sqrt(2)) - (I32a33*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_247 = Coupling(name = 'GC_247',
                  value = '-(cg*I39a11)/(2.*cmath.sqrt(2)) - (cg*I40a11)/(2.*cmath.sqrt(2)) - (I33a11*sg)/cmath.sqrt(2) - (I34a11*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_248 = Coupling(name = 'GC_248',
                  value = '-(cg*I39a12)/(4.*cmath.sqrt(2)) - (cg*I39a21)/(4.*cmath.sqrt(2)) - (cg*I40a12)/(4.*cmath.sqrt(2)) - (cg*I40a21)/(4.*cmath.sqrt(2)) - (I33a12*sg)/(2.*cmath.sqrt(2)) - (I33a21*sg)/(2.*cmath.sqrt(2)) - (I34a12*sg)/(2.*cmath.sqrt(2)) - (I34a21*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_249 = Coupling(name = 'GC_249',
                  value = '-(cg*I39a22)/(2.*cmath.sqrt(2)) - (cg*I40a22)/(2.*cmath.sqrt(2)) - (I33a22*sg)/cmath.sqrt(2) - (I34a22*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '-(cg*I39a13)/(4.*cmath.sqrt(2)) - (cg*I39a31)/(4.*cmath.sqrt(2)) - (cg*I40a13)/(4.*cmath.sqrt(2)) - (cg*I40a31)/(4.*cmath.sqrt(2)) - (I33a13*sg)/(2.*cmath.sqrt(2)) - (I33a31*sg)/(2.*cmath.sqrt(2)) - (I34a13*sg)/(2.*cmath.sqrt(2)) - (I34a31*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_251 = Coupling(name = 'GC_251',
                  value = '-(cg*I39a23)/(4.*cmath.sqrt(2)) - (cg*I39a32)/(4.*cmath.sqrt(2)) - (cg*I40a23)/(4.*cmath.sqrt(2)) - (cg*I40a32)/(4.*cmath.sqrt(2)) - (I33a23*sg)/(2.*cmath.sqrt(2)) - (I33a32*sg)/(2.*cmath.sqrt(2)) - (I34a23*sg)/(2.*cmath.sqrt(2)) - (I34a32*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_252 = Coupling(name = 'GC_252',
                  value = '-(cg*I39a33)/(2.*cmath.sqrt(2)) - (cg*I40a33)/(2.*cmath.sqrt(2)) - (I33a33*sg)/cmath.sqrt(2) - (I34a33*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_253 = Coupling(name = 'GC_253',
                  value = '(cg*I37a11)/(2.*cmath.sqrt(2)) + (cg*I38a11)/(2.*cmath.sqrt(2)) + (I35a11*sg)/cmath.sqrt(2) + (I36a11*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_254 = Coupling(name = 'GC_254',
                  value = '(cg*I37a12)/(4.*cmath.sqrt(2)) + (cg*I37a21)/(4.*cmath.sqrt(2)) + (cg*I38a12)/(4.*cmath.sqrt(2)) + (cg*I38a21)/(4.*cmath.sqrt(2)) + (I35a12*sg)/(2.*cmath.sqrt(2)) + (I35a21*sg)/(2.*cmath.sqrt(2)) + (I36a12*sg)/(2.*cmath.sqrt(2)) + (I36a21*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_255 = Coupling(name = 'GC_255',
                  value = '(cg*I37a22)/(2.*cmath.sqrt(2)) + (cg*I38a22)/(2.*cmath.sqrt(2)) + (I35a22*sg)/cmath.sqrt(2) + (I36a22*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '(cg*I37a13)/(4.*cmath.sqrt(2)) + (cg*I37a31)/(4.*cmath.sqrt(2)) + (cg*I38a13)/(4.*cmath.sqrt(2)) + (cg*I38a31)/(4.*cmath.sqrt(2)) + (I35a13*sg)/(2.*cmath.sqrt(2)) + (I35a31*sg)/(2.*cmath.sqrt(2)) + (I36a13*sg)/(2.*cmath.sqrt(2)) + (I36a31*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_257 = Coupling(name = 'GC_257',
                  value = '(cg*I37a23)/(4.*cmath.sqrt(2)) + (cg*I37a32)/(4.*cmath.sqrt(2)) + (cg*I38a23)/(4.*cmath.sqrt(2)) + (cg*I38a32)/(4.*cmath.sqrt(2)) + (I35a23*sg)/(2.*cmath.sqrt(2)) + (I35a32*sg)/(2.*cmath.sqrt(2)) + (I36a23*sg)/(2.*cmath.sqrt(2)) + (I36a32*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_258 = Coupling(name = 'GC_258',
                  value = '(cg*I37a33)/(2.*cmath.sqrt(2)) + (cg*I38a33)/(2.*cmath.sqrt(2)) + (I35a33*sg)/cmath.sqrt(2) + (I36a33*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_259 = Coupling(name = 'GC_259',
                  value = '-((cg*I35a11)/cmath.sqrt(2)) - (cg*I36a11)/cmath.sqrt(2) + (I37a11*sg)/(2.*cmath.sqrt(2)) + (I38a11*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_260 = Coupling(name = 'GC_260',
                  value = '-(cg*I35a12)/(2.*cmath.sqrt(2)) - (cg*I35a21)/(2.*cmath.sqrt(2)) - (cg*I36a12)/(2.*cmath.sqrt(2)) - (cg*I36a21)/(2.*cmath.sqrt(2)) + (I37a12*sg)/(4.*cmath.sqrt(2)) + (I37a21*sg)/(4.*cmath.sqrt(2)) + (I38a12*sg)/(4.*cmath.sqrt(2)) + (I38a21*sg)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_261 = Coupling(name = 'GC_261',
                  value = '-((cg*I35a22)/cmath.sqrt(2)) - (cg*I36a22)/cmath.sqrt(2) + (I37a22*sg)/(2.*cmath.sqrt(2)) + (I38a22*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_262 = Coupling(name = 'GC_262',
                  value = '-(cg*I35a13)/(2.*cmath.sqrt(2)) - (cg*I35a31)/(2.*cmath.sqrt(2)) - (cg*I36a13)/(2.*cmath.sqrt(2)) - (cg*I36a31)/(2.*cmath.sqrt(2)) + (I37a13*sg)/(4.*cmath.sqrt(2)) + (I37a31*sg)/(4.*cmath.sqrt(2)) + (I38a13*sg)/(4.*cmath.sqrt(2)) + (I38a31*sg)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_263 = Coupling(name = 'GC_263',
                  value = '-(cg*I35a23)/(2.*cmath.sqrt(2)) - (cg*I35a32)/(2.*cmath.sqrt(2)) - (cg*I36a23)/(2.*cmath.sqrt(2)) - (cg*I36a32)/(2.*cmath.sqrt(2)) + (I37a23*sg)/(4.*cmath.sqrt(2)) + (I37a32*sg)/(4.*cmath.sqrt(2)) + (I38a23*sg)/(4.*cmath.sqrt(2)) + (I38a32*sg)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_264 = Coupling(name = 'GC_264',
                  value = '-((cg*I35a33)/cmath.sqrt(2)) - (cg*I36a33)/cmath.sqrt(2) + (I37a33*sg)/(2.*cmath.sqrt(2)) + (I38a33*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_265 = Coupling(name = 'GC_265',
                  value = '(cg*I33a11)/cmath.sqrt(2) + (cg*I34a11)/cmath.sqrt(2) - (I39a11*sg)/(2.*cmath.sqrt(2)) - (I40a11*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_266 = Coupling(name = 'GC_266',
                  value = '(cg*I33a12)/(2.*cmath.sqrt(2)) + (cg*I33a21)/(2.*cmath.sqrt(2)) + (cg*I34a12)/(2.*cmath.sqrt(2)) + (cg*I34a21)/(2.*cmath.sqrt(2)) - (I39a12*sg)/(4.*cmath.sqrt(2)) - (I39a21*sg)/(4.*cmath.sqrt(2)) - (I40a12*sg)/(4.*cmath.sqrt(2)) - (I40a21*sg)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_267 = Coupling(name = 'GC_267',
                  value = '(cg*I33a22)/cmath.sqrt(2) + (cg*I34a22)/cmath.sqrt(2) - (I39a22*sg)/(2.*cmath.sqrt(2)) - (I40a22*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_268 = Coupling(name = 'GC_268',
                  value = '(cg*I33a13)/(2.*cmath.sqrt(2)) + (cg*I33a31)/(2.*cmath.sqrt(2)) + (cg*I34a13)/(2.*cmath.sqrt(2)) + (cg*I34a31)/(2.*cmath.sqrt(2)) - (I39a13*sg)/(4.*cmath.sqrt(2)) - (I39a31*sg)/(4.*cmath.sqrt(2)) - (I40a13*sg)/(4.*cmath.sqrt(2)) - (I40a31*sg)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_269 = Coupling(name = 'GC_269',
                  value = '(cg*I33a23)/(2.*cmath.sqrt(2)) + (cg*I33a32)/(2.*cmath.sqrt(2)) + (cg*I34a23)/(2.*cmath.sqrt(2)) + (cg*I34a32)/(2.*cmath.sqrt(2)) - (I39a23*sg)/(4.*cmath.sqrt(2)) - (I39a32*sg)/(4.*cmath.sqrt(2)) - (I40a23*sg)/(4.*cmath.sqrt(2)) - (I40a32*sg)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_270 = Coupling(name = 'GC_270',
                  value = '(cg*I33a33)/cmath.sqrt(2) + (cg*I34a33)/cmath.sqrt(2) - (I39a33*sg)/(2.*cmath.sqrt(2)) - (I40a33*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_271 = Coupling(name = 'GC_271',
                  value = '-(cg*I47a11)/(2.*cmath.sqrt(2)) - (cg*I48a11)/(2.*cmath.sqrt(2)) - (I41a11*sg)/cmath.sqrt(2) - (I42a11*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_272 = Coupling(name = 'GC_272',
                  value = '-(cg*I47a12)/(4.*cmath.sqrt(2)) - (cg*I47a21)/(4.*cmath.sqrt(2)) - (cg*I48a12)/(4.*cmath.sqrt(2)) - (cg*I48a21)/(4.*cmath.sqrt(2)) - (I41a12*sg)/(2.*cmath.sqrt(2)) - (I41a21*sg)/(2.*cmath.sqrt(2)) - (I42a12*sg)/(2.*cmath.sqrt(2)) - (I42a21*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_273 = Coupling(name = 'GC_273',
                  value = '-(cg*I47a22)/(2.*cmath.sqrt(2)) - (cg*I48a22)/(2.*cmath.sqrt(2)) - (I41a22*sg)/cmath.sqrt(2) - (I42a22*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_274 = Coupling(name = 'GC_274',
                  value = '-(cg*I47a13)/(4.*cmath.sqrt(2)) - (cg*I47a31)/(4.*cmath.sqrt(2)) - (cg*I48a13)/(4.*cmath.sqrt(2)) - (cg*I48a31)/(4.*cmath.sqrt(2)) - (I41a13*sg)/(2.*cmath.sqrt(2)) - (I41a31*sg)/(2.*cmath.sqrt(2)) - (I42a13*sg)/(2.*cmath.sqrt(2)) - (I42a31*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_275 = Coupling(name = 'GC_275',
                  value = '-(cg*I47a23)/(4.*cmath.sqrt(2)) - (cg*I47a32)/(4.*cmath.sqrt(2)) - (cg*I48a23)/(4.*cmath.sqrt(2)) - (cg*I48a32)/(4.*cmath.sqrt(2)) - (I41a23*sg)/(2.*cmath.sqrt(2)) - (I41a32*sg)/(2.*cmath.sqrt(2)) - (I42a23*sg)/(2.*cmath.sqrt(2)) - (I42a32*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_276 = Coupling(name = 'GC_276',
                  value = '-(cg*I47a33)/(2.*cmath.sqrt(2)) - (cg*I48a33)/(2.*cmath.sqrt(2)) - (I41a33*sg)/cmath.sqrt(2) - (I42a33*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_277 = Coupling(name = 'GC_277',
                  value = '(cg*I45a11)/(2.*cmath.sqrt(2)) + (cg*I46a11)/(2.*cmath.sqrt(2)) + (I43a11*sg)/cmath.sqrt(2) + (I44a11*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_278 = Coupling(name = 'GC_278',
                  value = '(cg*I45a12)/(4.*cmath.sqrt(2)) + (cg*I45a21)/(4.*cmath.sqrt(2)) + (cg*I46a12)/(4.*cmath.sqrt(2)) + (cg*I46a21)/(4.*cmath.sqrt(2)) + (I43a12*sg)/(2.*cmath.sqrt(2)) + (I43a21*sg)/(2.*cmath.sqrt(2)) + (I44a12*sg)/(2.*cmath.sqrt(2)) + (I44a21*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_279 = Coupling(name = 'GC_279',
                  value = '(cg*I45a22)/(2.*cmath.sqrt(2)) + (cg*I46a22)/(2.*cmath.sqrt(2)) + (I43a22*sg)/cmath.sqrt(2) + (I44a22*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_280 = Coupling(name = 'GC_280',
                  value = '(cg*I45a13)/(4.*cmath.sqrt(2)) + (cg*I45a31)/(4.*cmath.sqrt(2)) + (cg*I46a13)/(4.*cmath.sqrt(2)) + (cg*I46a31)/(4.*cmath.sqrt(2)) + (I43a13*sg)/(2.*cmath.sqrt(2)) + (I43a31*sg)/(2.*cmath.sqrt(2)) + (I44a13*sg)/(2.*cmath.sqrt(2)) + (I44a31*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_281 = Coupling(name = 'GC_281',
                  value = '(cg*I45a23)/(4.*cmath.sqrt(2)) + (cg*I45a32)/(4.*cmath.sqrt(2)) + (cg*I46a23)/(4.*cmath.sqrt(2)) + (cg*I46a32)/(4.*cmath.sqrt(2)) + (I43a23*sg)/(2.*cmath.sqrt(2)) + (I43a32*sg)/(2.*cmath.sqrt(2)) + (I44a23*sg)/(2.*cmath.sqrt(2)) + (I44a32*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_282 = Coupling(name = 'GC_282',
                  value = '(cg*I45a33)/(2.*cmath.sqrt(2)) + (cg*I46a33)/(2.*cmath.sqrt(2)) + (I43a33*sg)/cmath.sqrt(2) + (I44a33*sg)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_283 = Coupling(name = 'GC_283',
                  value = '-((cg*I43a11)/cmath.sqrt(2)) - (cg*I44a11)/cmath.sqrt(2) + (I45a11*sg)/(2.*cmath.sqrt(2)) + (I46a11*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_284 = Coupling(name = 'GC_284',
                  value = '-(cg*I43a12)/(2.*cmath.sqrt(2)) - (cg*I43a21)/(2.*cmath.sqrt(2)) - (cg*I44a12)/(2.*cmath.sqrt(2)) - (cg*I44a21)/(2.*cmath.sqrt(2)) + (I45a12*sg)/(4.*cmath.sqrt(2)) + (I45a21*sg)/(4.*cmath.sqrt(2)) + (I46a12*sg)/(4.*cmath.sqrt(2)) + (I46a21*sg)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_285 = Coupling(name = 'GC_285',
                  value = '-((cg*I43a22)/cmath.sqrt(2)) - (cg*I44a22)/cmath.sqrt(2) + (I45a22*sg)/(2.*cmath.sqrt(2)) + (I46a22*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_286 = Coupling(name = 'GC_286',
                  value = '-(cg*I43a13)/(2.*cmath.sqrt(2)) - (cg*I43a31)/(2.*cmath.sqrt(2)) - (cg*I44a13)/(2.*cmath.sqrt(2)) - (cg*I44a31)/(2.*cmath.sqrt(2)) + (I45a13*sg)/(4.*cmath.sqrt(2)) + (I45a31*sg)/(4.*cmath.sqrt(2)) + (I46a13*sg)/(4.*cmath.sqrt(2)) + (I46a31*sg)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_287 = Coupling(name = 'GC_287',
                  value = '-(cg*I43a23)/(2.*cmath.sqrt(2)) - (cg*I43a32)/(2.*cmath.sqrt(2)) - (cg*I44a23)/(2.*cmath.sqrt(2)) - (cg*I44a32)/(2.*cmath.sqrt(2)) + (I45a23*sg)/(4.*cmath.sqrt(2)) + (I45a32*sg)/(4.*cmath.sqrt(2)) + (I46a23*sg)/(4.*cmath.sqrt(2)) + (I46a32*sg)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_288 = Coupling(name = 'GC_288',
                  value = '-((cg*I43a33)/cmath.sqrt(2)) - (cg*I44a33)/cmath.sqrt(2) + (I45a33*sg)/(2.*cmath.sqrt(2)) + (I46a33*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_289 = Coupling(name = 'GC_289',
                  value = '(cg*I41a11)/cmath.sqrt(2) + (cg*I42a11)/cmath.sqrt(2) - (I47a11*sg)/(2.*cmath.sqrt(2)) - (I48a11*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_290 = Coupling(name = 'GC_290',
                  value = '(cg*I41a12)/(2.*cmath.sqrt(2)) + (cg*I41a21)/(2.*cmath.sqrt(2)) + (cg*I42a12)/(2.*cmath.sqrt(2)) + (cg*I42a21)/(2.*cmath.sqrt(2)) - (I47a12*sg)/(4.*cmath.sqrt(2)) - (I47a21*sg)/(4.*cmath.sqrt(2)) - (I48a12*sg)/(4.*cmath.sqrt(2)) - (I48a21*sg)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_291 = Coupling(name = 'GC_291',
                  value = '(cg*I41a22)/cmath.sqrt(2) + (cg*I42a22)/cmath.sqrt(2) - (I47a22*sg)/(2.*cmath.sqrt(2)) - (I48a22*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_292 = Coupling(name = 'GC_292',
                  value = '(cg*I41a13)/(2.*cmath.sqrt(2)) + (cg*I41a31)/(2.*cmath.sqrt(2)) + (cg*I42a13)/(2.*cmath.sqrt(2)) + (cg*I42a31)/(2.*cmath.sqrt(2)) - (I47a13*sg)/(4.*cmath.sqrt(2)) - (I47a31*sg)/(4.*cmath.sqrt(2)) - (I48a13*sg)/(4.*cmath.sqrt(2)) - (I48a31*sg)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_293 = Coupling(name = 'GC_293',
                  value = '(cg*I41a23)/(2.*cmath.sqrt(2)) + (cg*I41a32)/(2.*cmath.sqrt(2)) + (cg*I42a23)/(2.*cmath.sqrt(2)) + (cg*I42a32)/(2.*cmath.sqrt(2)) - (I47a23*sg)/(4.*cmath.sqrt(2)) - (I47a32*sg)/(4.*cmath.sqrt(2)) - (I48a23*sg)/(4.*cmath.sqrt(2)) - (I48a32*sg)/(4.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_294 = Coupling(name = 'GC_294',
                  value = '(cg*I41a33)/cmath.sqrt(2) + (cg*I42a33)/cmath.sqrt(2) - (I47a33*sg)/(2.*cmath.sqrt(2)) - (I48a33*sg)/(2.*cmath.sqrt(2))',
                  order = {'QED':1})

GC_295 = Coupling(name = 'GC_295',
                  value = '2*cg*complex(0,1)*lam1*sg - cg*complex(0,1)*lam3*sg',
                  order = {'QED':2})

GC_296 = Coupling(name = 'GC_296',
                  value = '2*Ca*cg*complex(0,1)*lam1*Sa*sg + 2*Ca*cg*complex(0,1)*lam2*Sa*sg - 2*Ca*cg*complex(0,1)*lam3*Sa*sg',
                  order = {'QED':2})

GC_297 = Coupling(name = 'GC_297',
                  value = '-2*Ca**2*cg*complex(0,1)*lam2*sg + Ca**2*cg*complex(0,1)*lam3*sg + 2*cg*complex(0,1)*lam1*Sa**2*sg - cg*complex(0,1)*lam3*Sa**2*sg',
                  order = {'QED':2})

GC_298 = Coupling(name = 'GC_298',
                  value = '2*Ca**2*cg*complex(0,1)*lam1*sg - Ca**2*cg*complex(0,1)*lam3*sg - 2*cg*complex(0,1)*lam2*Sa**2*sg + cg*complex(0,1)*lam3*Sa**2*sg',
                  order = {'QED':2})

GC_299 = Coupling(name = 'GC_299',
                  value = '-(cg**2*complex(0,1)*lam3) - 2*complex(0,1)*lam1*sg**2',
                  order = {'QED':2})

GC_300 = Coupling(name = 'GC_300',
                  value = '-2*cg**2*complex(0,1)*lam1 - complex(0,1)*lam3*sg**2',
                  order = {'QED':2})

GC_301 = Coupling(name = 'GC_301',
                  value = '-2*Ca*cg**2*complex(0,1)*lam1*Sa + Ca*cg**2*complex(0,1)*lam3*Sa + 2*Ca*complex(0,1)*lam2*Sa*sg**2 - Ca*complex(0,1)*lam3*Sa*sg**2',
                  order = {'QED':2})

GC_302 = Coupling(name = 'GC_302',
                  value = '2*Ca*cg**2*complex(0,1)*lam2*Sa - Ca*cg**2*complex(0,1)*lam3*Sa - 2*Ca*complex(0,1)*lam1*Sa*sg**2 + Ca*complex(0,1)*lam3*Sa*sg**2',
                  order = {'QED':2})

GC_303 = Coupling(name = 'GC_303',
                  value = '-2*Ca**2*cg**2*complex(0,1)*lam2 - cg**2*complex(0,1)*lam3*Sa**2 - Ca**2*complex(0,1)*lam3*sg**2 - 2*complex(0,1)*lam1*Sa**2*sg**2',
                  order = {'QED':2})

GC_304 = Coupling(name = 'GC_304',
                  value = '-2*Ca**2*cg**2*complex(0,1)*lam1 - cg**2*complex(0,1)*lam3*Sa**2 - Ca**2*complex(0,1)*lam3*sg**2 - 2*complex(0,1)*lam2*Sa**2*sg**2',
                  order = {'QED':2})

GC_305 = Coupling(name = 'GC_305',
                  value = '-(Ca**2*cg**2*complex(0,1)*lam3) - 2*cg**2*complex(0,1)*lam2*Sa**2 - 2*Ca**2*complex(0,1)*lam1*sg**2 - complex(0,1)*lam3*Sa**2*sg**2',
                  order = {'QED':2})

GC_306 = Coupling(name = 'GC_306',
                  value = '-(Ca**2*cg**2*complex(0,1)*lam3) - 2*cg**2*complex(0,1)*lam1*Sa**2 - 2*Ca**2*complex(0,1)*lam2*sg**2 - complex(0,1)*lam3*Sa**2*sg**2',
                  order = {'QED':2})

GC_307 = Coupling(name = 'GC_307',
                  value = '-6*cg**3*complex(0,1)*lam2*sg + 3*cg**3*complex(0,1)*lam3*sg + 6*cg*complex(0,1)*lam1*sg**3 - 3*cg*complex(0,1)*lam3*sg**3',
                  order = {'QED':2})

GC_308 = Coupling(name = 'GC_308',
                  value = '6*cg**3*complex(0,1)*lam1*sg - 3*cg**3*complex(0,1)*lam3*sg - 6*cg*complex(0,1)*lam2*sg**3 + 3*cg*complex(0,1)*lam3*sg**3',
                  order = {'QED':2})

GC_309 = Coupling(name = 'GC_309',
                  value = '-6*cg**4*complex(0,1)*lam2 - 6*cg**2*complex(0,1)*lam3*sg**2 - 6*complex(0,1)*lam1*sg**4',
                  order = {'QED':2})

GC_310 = Coupling(name = 'GC_310',
                  value = '-6*cg**4*complex(0,1)*lam1 - 6*cg**2*complex(0,1)*lam3*sg**2 - 6*complex(0,1)*lam2*sg**4',
                  order = {'QED':2})

GC_311 = Coupling(name = 'GC_311',
                  value = '-(cg**4*complex(0,1)*lam3) - 6*cg**2*complex(0,1)*lam1*sg**2 - 6*cg**2*complex(0,1)*lam2*sg**2 + 4*cg**2*complex(0,1)*lam3*sg**2 - complex(0,1)*lam3*sg**4',
                  order = {'QED':2})

GC_312 = Coupling(name = 'GC_312',
                  value = '(ee**2*complex(0,1))/(2.*sw**2)',
                  order = {'QED':2})

GC_313 = Coupling(name = 'GC_313',
                  value = '-((ee**2*complex(0,1))/sw**2)',
                  order = {'QED':2})

GC_314 = Coupling(name = 'GC_314',
                  value = '(Ca**2*ee**2*complex(0,1))/(2.*sw**2)',
                  order = {'QED':2})

GC_315 = Coupling(name = 'GC_315',
                  value = '(cg**2*ee**2*complex(0,1))/(2.*sw**2)',
                  order = {'QED':2})

GC_316 = Coupling(name = 'GC_316',
                  value = '(Cp**2*cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_317 = Coupling(name = 'GC_317',
                  value = '(Ca*ee**2*complex(0,1)*Sa)/(2.*sw**2)',
                  order = {'QED':2})

GC_318 = Coupling(name = 'GC_318',
                  value = '(ee**2*complex(0,1)*Sa**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_319 = Coupling(name = 'GC_319',
                  value = '-(cg*ee**2*complex(0,1)*sg)/(2.*sw**2)',
                  order = {'QED':2})

GC_320 = Coupling(name = 'GC_320',
                  value = '(ee**2*complex(0,1)*sg**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_321 = Coupling(name = 'GC_321',
                  value = '-((Cp*cw**2*ee**2*complex(0,1)*Sp)/sw**2)',
                  order = {'QED':2})

GC_322 = Coupling(name = 'GC_322',
                  value = '(cw**2*ee**2*complex(0,1)*Sp**2)/sw**2',
                  order = {'QED':2})

GC_323 = Coupling(name = 'GC_323',
                  value = '-(Ca*ee)/(2.*sw)',
                  order = {'QED':1})

GC_324 = Coupling(name = 'GC_324',
                  value = '-(cg*ee*complex(0,1))/(2.*sw)',
                  order = {'QED':1})

GC_325 = Coupling(name = 'GC_325',
                  value = '(cg*ee*complex(0,1))/(2.*sw)',
                  order = {'QED':1})

GC_326 = Coupling(name = 'GC_326',
                  value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_327 = Coupling(name = 'GC_327',
                  value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_328 = Coupling(name = 'GC_328',
                  value = '(CKM1x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_329 = Coupling(name = 'GC_329',
                  value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_330 = Coupling(name = 'GC_330',
                  value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_331 = Coupling(name = 'GC_331',
                  value = '(CKM2x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_332 = Coupling(name = 'GC_332',
                  value = '(CKM3x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_333 = Coupling(name = 'GC_333',
                  value = '(CKM3x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_334 = Coupling(name = 'GC_334',
                  value = '(CKM3x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_335 = Coupling(name = 'GC_335',
                  value = '-((Cp*cw*ee*complex(0,1))/sw)',
                  order = {'QED':1})

GC_336 = Coupling(name = 'GC_336',
                  value = '(Cp*cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_337 = Coupling(name = 'GC_337',
                  value = '-(Ca*ee**2)/(2.*sw)',
                  order = {'QED':2})

GC_338 = Coupling(name = 'GC_338',
                  value = '(Ca*ee**2)/(2.*sw)',
                  order = {'QED':2})

GC_339 = Coupling(name = 'GC_339',
                  value = '-(cg*ee**2*complex(0,1))/(2.*sw)',
                  order = {'QED':2})

GC_340 = Coupling(name = 'GC_340',
                  value = '(-2*Cp*cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_341 = Coupling(name = 'GC_341',
                  value = '-(ee*Sa)/(2.*sw)',
                  order = {'QED':1})

GC_342 = Coupling(name = 'GC_342',
                  value = '-(ee**2*Sa)/(2.*sw)',
                  order = {'QED':2})

GC_343 = Coupling(name = 'GC_343',
                  value = '(ee**2*Sa)/(2.*sw)',
                  order = {'QED':2})

GC_344 = Coupling(name = 'GC_344',
                  value = '-(ee*complex(0,1)*sg)/(2.*sw)',
                  order = {'QED':1})

GC_345 = Coupling(name = 'GC_345',
                  value = '(ee*complex(0,1)*sg)/(2.*sw)',
                  order = {'QED':1})

GC_346 = Coupling(name = 'GC_346',
                  value = '(ee**2*complex(0,1)*sg)/(2.*sw)',
                  order = {'QED':2})

GC_347 = Coupling(name = 'GC_347',
                  value = '(ee*complex(0,1)*Sm1x1)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_348 = Coupling(name = 'GC_348',
                  value = '(ee*complex(0,1)*Sm1x2)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_349 = Coupling(name = 'GC_349',
                  value = '(ee*complex(0,1)*Sm1x3)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_350 = Coupling(name = 'GC_350',
                  value = '(ee*complex(0,1)*Sm2x1)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_351 = Coupling(name = 'GC_351',
                  value = '(ee*complex(0,1)*Sm2x2)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_352 = Coupling(name = 'GC_352',
                  value = '(ee*complex(0,1)*Sm2x3)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_353 = Coupling(name = 'GC_353',
                  value = '(ee*complex(0,1)*Sm3x1)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_354 = Coupling(name = 'GC_354',
                  value = '(ee*complex(0,1)*Sm3x2)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_355 = Coupling(name = 'GC_355',
                  value = '(ee*complex(0,1)*Sm3x3)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_356 = Coupling(name = 'GC_356',
                  value = '-((cw*ee*complex(0,1)*Sp)/sw)',
                  order = {'QED':1})

GC_357 = Coupling(name = 'GC_357',
                  value = '(cw*ee*complex(0,1)*Sp)/sw',
                  order = {'QED':1})

GC_358 = Coupling(name = 'GC_358',
                  value = '(2*cw*ee**2*complex(0,1)*Sp)/sw',
                  order = {'QED':2})

GC_359 = Coupling(name = 'GC_359',
                  value = '(ee*complex(0,1)*U11)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_360 = Coupling(name = 'GC_360',
                  value = '(ee*complex(0,1)*U12)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_361 = Coupling(name = 'GC_361',
                  value = '(ee*complex(0,1)*U13)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_362 = Coupling(name = 'GC_362',
                  value = '(ee*complex(0,1)*U21)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_363 = Coupling(name = 'GC_363',
                  value = '(ee*complex(0,1)*U22)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_364 = Coupling(name = 'GC_364',
                  value = '(ee*complex(0,1)*U23)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_365 = Coupling(name = 'GC_365',
                  value = '(ee*complex(0,1)*U31)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_366 = Coupling(name = 'GC_366',
                  value = '(ee*complex(0,1)*U32)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_367 = Coupling(name = 'GC_367',
                  value = '(ee*complex(0,1)*U33)/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_368 = Coupling(name = 'GC_368',
                  value = '-(Ca*ee**2*complex(0,1)*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_369 = Coupling(name = 'GC_369',
                  value = '(Ca*ee**2*complex(0,1)*vev)/(2.*sw**2)',
                  order = {'QED':1})

GC_370 = Coupling(name = 'GC_370',
                  value = '-(cg*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_371 = Coupling(name = 'GC_371',
                  value = '(cg*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_372 = Coupling(name = 'GC_372',
                  value = '-(ee**2*complex(0,1)*Sa*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_373 = Coupling(name = 'GC_373',
                  value = '(ee**2*complex(0,1)*Sa*vev)/(2.*sw**2)',
                  order = {'QED':1})

GC_374 = Coupling(name = 'GC_374',
                  value = '-(ee**2*sg*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_375 = Coupling(name = 'GC_375',
                  value = '(ee**2*sg*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_376 = Coupling(name = 'GC_376',
                  value = '-(ee**2*vev)/(2.*sw)',
                  order = {'QED':1})

GC_377 = Coupling(name = 'GC_377',
                  value = '(ee**2*vev)/(2.*sw)',
                  order = {'QED':1})

GC_378 = Coupling(name = 'GC_378',
                  value = '-(ee**2*Sp*vev)/(4.*cw) - (cw*ee**2*Sp*vev)/(4.*sw**2) - (Cp*ee*gt*vev)/(4.*sw)',
                  order = {'QED':1})

GC_379 = Coupling(name = 'GC_379',
                  value = '-(ee**2*Sp*vev)/(4.*cw) + (cw*ee**2*Sp*vev)/(4.*sw**2) - (Cp*ee*gt*vev)/(4.*sw)',
                  order = {'QED':1})

GC_380 = Coupling(name = 'GC_380',
                  value = '(ee**2*Sp*vev)/(4.*cw) - (cw*ee**2*Sp*vev)/(4.*sw**2) + (Cp*ee*gt*vev)/(4.*sw)',
                  order = {'QED':1})

GC_381 = Coupling(name = 'GC_381',
                  value = '(ee**2*Sp*vev)/(4.*cw) + (cw*ee**2*Sp*vev)/(4.*sw**2) + (Cp*ee*gt*vev)/(4.*sw)',
                  order = {'QED':1})

GC_382 = Coupling(name = 'GC_382',
                  value = '(Cp*ee**2*vev)/(4.*cw) - (Cp*cw*ee**2*vev)/(4.*sw**2) - (ee*gt*Sp*vev)/(4.*sw)',
                  order = {'QED':1})

GC_383 = Coupling(name = 'GC_383',
                  value = '(Cp*ee**2*vev)/(4.*cw) + (Cp*cw*ee**2*vev)/(4.*sw**2) - (ee*gt*Sp*vev)/(4.*sw)',
                  order = {'QED':1})

GC_384 = Coupling(name = 'GC_384',
                  value = '-(Cp*ee**2*vev)/(4.*cw) - (Cp*cw*ee**2*vev)/(4.*sw**2) + (ee*gt*Sp*vev)/(4.*sw)',
                  order = {'QED':1})

GC_385 = Coupling(name = 'GC_385',
                  value = '-(Cp*ee**2*vev)/(4.*cw) + (Cp*cw*ee**2*vev)/(4.*sw**2) + (ee*gt*Sp*vev)/(4.*sw)',
                  order = {'QED':1})

GC_386 = Coupling(name = 'GC_386',
                  value = '-(Cp**2*complex(0,1)*gt**2*Sa*vev)/4. - (ee**2*complex(0,1)*Sa*Sp**2*vev)/2. - (cw**2*ee**2*complex(0,1)*Sa*Sp**2*vev)/(4.*sw**2) - (Cp*cw*ee*complex(0,1)*gt*Sa*Sp*vev)/(2.*sw) - (Cp*ee*complex(0,1)*gt*Sa*Sp*sw*vev)/(2.*cw) - (ee**2*complex(0,1)*Sa*Sp**2*sw**2*vev)/(4.*cw**2) - 4*Ca*Cp**2*complex(0,1)*g1pp**2*x',
                  order = {'QED':1})

GC_387 = Coupling(name = 'GC_387',
                  value = '-2*complex(0,1)*lam1*Sa*vev - Ca*complex(0,1)*lam3*x',
                  order = {'QED':1})

GC_388 = Coupling(name = 'GC_388',
                  value = '-(Ca*Cp**2*complex(0,1)*gt**2*vev)/4. - (Ca*ee**2*complex(0,1)*Sp**2*vev)/2. - (Ca*cw**2*ee**2*complex(0,1)*Sp**2*vev)/(4.*sw**2) - (Ca*Cp*cw*ee*complex(0,1)*gt*Sp*vev)/(2.*sw) - (Ca*Cp*ee*complex(0,1)*gt*Sp*sw*vev)/(2.*cw) - (Ca*ee**2*complex(0,1)*Sp**2*sw**2*vev)/(4.*cw**2) + 4*Cp**2*complex(0,1)*g1pp**2*Sa*x',
                  order = {'QED':1})

GC_389 = Coupling(name = 'GC_389',
                  value = '-2*Ca*complex(0,1)*lam1*vev + complex(0,1)*lam3*Sa*x',
                  order = {'QED':1})

GC_390 = Coupling(name = 'GC_390',
                  value = '-6*Ca**2*complex(0,1)*lam1*Sa*vev + 2*Ca**2*complex(0,1)*lam3*Sa*vev - complex(0,1)*lam3*Sa**3*vev - Ca**3*complex(0,1)*lam3*x - 6*Ca*complex(0,1)*lam2*Sa**2*x + 2*Ca*complex(0,1)*lam3*Sa**2*x',
                  order = {'QED':1})

GC_391 = Coupling(name = 'GC_391',
                  value = '-3*Ca**2*complex(0,1)*lam3*Sa*vev - 6*complex(0,1)*lam1*Sa**3*vev - 6*Ca**3*complex(0,1)*lam2*x - 3*Ca*complex(0,1)*lam3*Sa**2*x',
                  order = {'QED':1})

GC_392 = Coupling(name = 'GC_392',
                  value = '-6*Ca**3*complex(0,1)*lam1*vev - 3*Ca*complex(0,1)*lam3*Sa**2*vev + 3*Ca**2*complex(0,1)*lam3*Sa*x + 6*complex(0,1)*lam2*Sa**3*x',
                  order = {'QED':1})

GC_393 = Coupling(name = 'GC_393',
                  value = '-(Ca**3*complex(0,1)*lam3*vev) - 6*Ca*complex(0,1)*lam1*Sa**2*vev + 2*Ca*complex(0,1)*lam3*Sa**2*vev + 6*Ca**2*complex(0,1)*lam2*Sa*x - 2*Ca**2*complex(0,1)*lam3*Sa*x + complex(0,1)*lam3*Sa**3*x',
                  order = {'QED':1})

GC_394 = Coupling(name = 'GC_394',
                  value = '2*cg*complex(0,1)*lam1*Sa*sg*vev - cg*complex(0,1)*lam3*Sa*sg*vev - 2*Ca*cg*complex(0,1)*lam2*sg*x + Ca*cg*complex(0,1)*lam3*sg*x',
                  order = {'QED':1})

GC_395 = Coupling(name = 'GC_395',
                  value = '2*Ca*cg*complex(0,1)*lam1*sg*vev - Ca*cg*complex(0,1)*lam3*sg*vev + 2*cg*complex(0,1)*lam2*Sa*sg*x - cg*complex(0,1)*lam3*Sa*sg*x',
                  order = {'QED':1})

GC_396 = Coupling(name = 'GC_396',
                  value = '-2*cg**2*complex(0,1)*lam1*Sa*vev - complex(0,1)*lam3*Sa*sg**2*vev - Ca*cg**2*complex(0,1)*lam3*x - 2*Ca*complex(0,1)*lam2*sg**2*x',
                  order = {'QED':1})

GC_397 = Coupling(name = 'GC_397',
                  value = '-(cg**2*complex(0,1)*lam3*Sa*vev) - 2*complex(0,1)*lam1*Sa*sg**2*vev - 2*Ca*cg**2*complex(0,1)*lam2*x - Ca*complex(0,1)*lam3*sg**2*x',
                  order = {'QED':1})

GC_398 = Coupling(name = 'GC_398',
                  value = '-2*Ca*cg**2*complex(0,1)*lam1*vev - Ca*complex(0,1)*lam3*sg**2*vev + cg**2*complex(0,1)*lam3*Sa*x + 2*complex(0,1)*lam2*Sa*sg**2*x',
                  order = {'QED':1})

GC_399 = Coupling(name = 'GC_399',
                  value = '-(Ca*cg**2*complex(0,1)*lam3*vev) - 2*Ca*complex(0,1)*lam1*sg**2*vev + 2*cg**2*complex(0,1)*lam2*Sa*x + complex(0,1)*lam3*Sa*sg**2*x',
                  order = {'QED':1})

GC_400 = Coupling(name = 'GC_400',
                  value = '(Cp*ee**2*complex(0,1)*Sa*Sp*vev)/2. - (Cp*complex(0,1)*gt**2*Sa*Sp*vev)/4. + (Cp*cw**2*ee**2*complex(0,1)*Sa*Sp*vev)/(4.*sw**2) + (Cp**2*cw*ee*complex(0,1)*gt*Sa*vev)/(4.*sw) - (cw*ee*complex(0,1)*gt*Sa*Sp**2*vev)/(4.*sw) + (Cp**2*ee*complex(0,1)*gt*Sa*sw*vev)/(4.*cw) - (ee*complex(0,1)*gt*Sa*Sp**2*sw*vev)/(4.*cw) + (Cp*ee**2*complex(0,1)*Sa*Sp*sw**2*vev)/(4.*cw**2) - 4*Ca*Cp*complex(0,1)*g1pp**2*Sp*x',
                  order = {'QED':1})

GC_401 = Coupling(name = 'GC_401',
                  value = '(Ca*Cp*ee**2*complex(0,1)*Sp*vev)/2. - (Ca*Cp*complex(0,1)*gt**2*Sp*vev)/4. + (Ca*Cp*cw**2*ee**2*complex(0,1)*Sp*vev)/(4.*sw**2) + (Ca*Cp**2*cw*ee*complex(0,1)*gt*vev)/(4.*sw) - (Ca*cw*ee*complex(0,1)*gt*Sp**2*vev)/(4.*sw) + (Ca*Cp**2*ee*complex(0,1)*gt*sw*vev)/(4.*cw) - (Ca*ee*complex(0,1)*gt*Sp**2*sw*vev)/(4.*cw) + (Ca*Cp*ee**2*complex(0,1)*Sp*sw**2*vev)/(4.*cw**2) + 4*Cp*complex(0,1)*g1pp**2*Sa*Sp*x',
                  order = {'QED':1})

GC_402 = Coupling(name = 'GC_402',
                  value = '-(Cp**2*ee**2*complex(0,1)*Sa*vev)/2. - (complex(0,1)*gt**2*Sa*Sp**2*vev)/4. - (Cp**2*cw**2*ee**2*complex(0,1)*Sa*vev)/(4.*sw**2) + (Cp*cw*ee*complex(0,1)*gt*Sa*Sp*vev)/(2.*sw) + (Cp*ee*complex(0,1)*gt*Sa*Sp*sw*vev)/(2.*cw) - (Cp**2*ee**2*complex(0,1)*Sa*sw**2*vev)/(4.*cw**2) - 4*Ca*complex(0,1)*g1pp**2*Sp**2*x',
                  order = {'QED':1})

GC_403 = Coupling(name = 'GC_403',
                  value = '-(Ca*Cp**2*ee**2*complex(0,1)*vev)/2. - (Ca*complex(0,1)*gt**2*Sp**2*vev)/4. - (Ca*Cp**2*cw**2*ee**2*complex(0,1)*vev)/(4.*sw**2) + (Ca*Cp*cw*ee*complex(0,1)*gt*Sp*vev)/(2.*sw) + (Ca*Cp*ee*complex(0,1)*gt*Sp*sw*vev)/(2.*cw) - (Ca*Cp**2*ee**2*complex(0,1)*sw**2*vev)/(4.*cw**2) + 4*complex(0,1)*g1pp**2*Sa*Sp**2*x',
                  order = {'QED':1})

GC_404 = Coupling(name = 'GC_404',
                  value = '(cw*ee*complex(0,1)*Sp)/(2.*sw) - (ee*complex(0,1)*Sp*sw)/(2.*cw) - (Cp*complex(0,1)*g1p*xH)/2.',
                  order = {'QED':1})

GC_405 = Coupling(name = 'GC_405',
                  value = '-((cw*ee**2*complex(0,1)*Sp)/sw) + (ee**2*complex(0,1)*Sp*sw)/cw + Cp*ee*complex(0,1)*g1p*xH',
                  order = {'QED':2})

GC_406 = Coupling(name = 'GC_406',
                  value = '-(Cp*cw*ee*complex(0,1))/(2.*sw) + (Cp*ee*complex(0,1)*sw)/(2.*cw) - (complex(0,1)*g1p*Sp*xH)/2.',
                  order = {'QED':1})

GC_407 = Coupling(name = 'GC_407',
                  value = '(Cp*cw*ee**2*complex(0,1))/sw - (Cp*ee**2*complex(0,1)*sw)/cw + ee*complex(0,1)*g1p*Sp*xH',
                  order = {'QED':2})

GC_408 = Coupling(name = 'GC_408',
                  value = '-(Ca*ee**2*Sp)/(2.*cw) - (Ca*Cp*ee*g1p*xH)/(2.*sw)',
                  order = {'QED':2})

GC_409 = Coupling(name = 'GC_409',
                  value = '(Ca*ee**2*Sp)/(2.*cw) + (Ca*Cp*ee*g1p*xH)/(2.*sw)',
                  order = {'QED':2})

GC_410 = Coupling(name = 'GC_410',
                  value = '-(cg*ee**2*complex(0,1)*Sp)/(2.*cw) - (cg*Cp*ee*complex(0,1)*g1p*xH)/(2.*sw)',
                  order = {'QED':2})

GC_411 = Coupling(name = 'GC_411',
                  value = '-(ee**2*Sa*Sp)/(2.*cw) - (Cp*ee*g1p*Sa*xH)/(2.*sw)',
                  order = {'QED':2})

GC_412 = Coupling(name = 'GC_412',
                  value = '(ee**2*Sa*Sp)/(2.*cw) + (Cp*ee*g1p*Sa*xH)/(2.*sw)',
                  order = {'QED':2})

GC_413 = Coupling(name = 'GC_413',
                  value = '(ee**2*complex(0,1)*sg*Sp)/(2.*cw) + (Cp*ee*complex(0,1)*g1p*sg*xH)/(2.*sw)',
                  order = {'QED':2})

GC_414 = Coupling(name = 'GC_414',
                  value = '(Ca*Cp*ee**2)/(2.*cw) - (Ca*ee*g1p*Sp*xH)/(2.*sw)',
                  order = {'QED':2})

GC_415 = Coupling(name = 'GC_415',
                  value = '-(Ca*Cp*ee**2)/(2.*cw) + (Ca*ee*g1p*Sp*xH)/(2.*sw)',
                  order = {'QED':2})

GC_416 = Coupling(name = 'GC_416',
                  value = '(cg*Cp*ee**2*complex(0,1))/(2.*cw) - (cg*ee*complex(0,1)*g1p*Sp*xH)/(2.*sw)',
                  order = {'QED':2})

GC_417 = Coupling(name = 'GC_417',
                  value = '(Cp*ee**2*Sa)/(2.*cw) - (ee*g1p*Sa*Sp*xH)/(2.*sw)',
                  order = {'QED':2})

GC_418 = Coupling(name = 'GC_418',
                  value = '-(Cp*ee**2*Sa)/(2.*cw) + (ee*g1p*Sa*Sp*xH)/(2.*sw)',
                  order = {'QED':2})

GC_419 = Coupling(name = 'GC_419',
                  value = '-(Cp*ee**2*complex(0,1)*sg)/(2.*cw) + (ee*complex(0,1)*g1p*sg*Sp*xH)/(2.*sw)',
                  order = {'QED':2})

GC_420 = Coupling(name = 'GC_420',
                  value = '-(ee**2*Sp*vev)/(2.*cw) - (Cp*ee*g1p*vev*xH)/(2.*sw)',
                  order = {'QED':1})

GC_421 = Coupling(name = 'GC_421',
                  value = '(ee**2*Sp*vev)/(2.*cw) + (Cp*ee*g1p*vev*xH)/(2.*sw)',
                  order = {'QED':1})

GC_422 = Coupling(name = 'GC_422',
                  value = '(Cp*ee**2*vev)/(2.*cw) - (ee*g1p*Sp*vev*xH)/(2.*sw)',
                  order = {'QED':1})

GC_423 = Coupling(name = 'GC_423',
                  value = '-(Cp*ee**2*vev)/(2.*cw) + (ee*g1p*Sp*vev*xH)/(2.*sw)',
                  order = {'QED':1})

GC_424 = Coupling(name = 'GC_424',
                  value = '-(ee**2*complex(0,1)*Sp**2) + (cw**2*ee**2*complex(0,1)*Sp**2)/(2.*sw**2) + (ee**2*complex(0,1)*Sp**2*sw**2)/(2.*cw**2) - (Cp*cw*ee*complex(0,1)*g1p*Sp*xH)/sw + (Cp*ee*complex(0,1)*g1p*Sp*sw*xH)/cw + (Cp**2*complex(0,1)*g1p**2*xH**2)/2.',
                  order = {'QED':2})

GC_425 = Coupling(name = 'GC_425',
                  value = 'Cp*ee**2*complex(0,1)*Sp - (Cp*cw**2*ee**2*complex(0,1)*Sp)/(2.*sw**2) - (Cp*ee**2*complex(0,1)*Sp*sw**2)/(2.*cw**2) + (Cp**2*cw*ee*complex(0,1)*g1p*xH)/(2.*sw) - (cw*ee*complex(0,1)*g1p*Sp**2*xH)/(2.*sw) - (Cp**2*ee*complex(0,1)*g1p*sw*xH)/(2.*cw) + (ee*complex(0,1)*g1p*Sp**2*sw*xH)/(2.*cw) + (Cp*complex(0,1)*g1p**2*Sp*xH**2)/2.',
                  order = {'QED':2})

GC_426 = Coupling(name = 'GC_426',
                  value = '-(Cp**2*ee**2*complex(0,1)) + (Cp**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (Cp**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (Cp*cw*ee*complex(0,1)*g1p*Sp*xH)/sw - (Cp*ee*complex(0,1)*g1p*Sp*sw*xH)/cw + (complex(0,1)*g1p**2*Sp**2*xH**2)/2.',
                  order = {'QED':2})

GC_427 = Coupling(name = 'GC_427',
                  value = '-(cw*ee*complex(0,1)*Sp)/(2.*sw) + (ee*complex(0,1)*Sp*sw)/(6.*cw) + (Cp*complex(0,1)*g1p*xH)/6. + (Cp*complex(0,1)*g1p*xPhi)/3.',
                  order = {'QED':1})

GC_428 = Coupling(name = 'GC_428',
                  value = '(cw*ee*complex(0,1)*Sp)/(2.*sw) + (ee*complex(0,1)*Sp*sw)/(6.*cw) + (Cp*complex(0,1)*g1p*xH)/6. + (Cp*complex(0,1)*g1p*xPhi)/3.',
                  order = {'QED':1})

GC_429 = Coupling(name = 'GC_429',
                  value = '-(ee*complex(0,1)*Sp*sw)/(3.*cw) - (Cp*complex(0,1)*g1p*xH)/3. + (Cp*complex(0,1)*g1p*xPhi)/3.',
                  order = {'QED':1})

GC_430 = Coupling(name = 'GC_430',
                  value = '(2*ee*complex(0,1)*Sp*sw)/(3.*cw) + (2*Cp*complex(0,1)*g1p*xH)/3. + (Cp*complex(0,1)*g1p*xPhi)/3.',
                  order = {'QED':1})

GC_431 = Coupling(name = 'GC_431',
                  value = '(cw*ee*complex(0,1)*Sp)/(2.*sw) - (ee*complex(0,1)*Sp*sw)/(2.*cw) - (Cp*complex(0,1)*g1p*xH)/2. - Cp*complex(0,1)*g1p*xPhi',
                  order = {'QED':1})

GC_432 = Coupling(name = 'GC_432',
                  value = '-((ee*complex(0,1)*Sp*sw)/cw) - Cp*complex(0,1)*g1p*xH - Cp*complex(0,1)*g1p*xPhi',
                  order = {'QED':1})

GC_433 = Coupling(name = 'GC_433',
                  value = '(cw*ee*Sa*sg*Sp)/(2.*sw) + (ee*Sa*sg*Sp*sw)/(2.*cw) + (Cp*g1p*Sa*sg*xH)/2. - 2*Ca*cg*Cp*g1p*xPhi',
                  order = {'QED':1})

GC_434 = Coupling(name = 'GC_434',
                  value = '(cw*ee*complex(0,1)*I1a11*Sp)/(4.*sw) + (cw*ee*complex(0,1)*I2a11*Sp)/(4.*sw) + (ee*complex(0,1)*I1a11*Sp*sw)/(4.*cw) + (ee*complex(0,1)*I2a11*Sp*sw)/(4.*cw) + (Cp*complex(0,1)*g1p*I1a11*xH)/4. + (Cp*complex(0,1)*g1p*I2a11*xH)/4. + (Cp*complex(0,1)*g1p*I1a11*xPhi)/2. + (Cp*complex(0,1)*g1p*I2a11*xPhi)/2. - (Cp*complex(0,1)*g1p*I3a11*xPhi)/2. - (Cp*complex(0,1)*g1p*I4a11*xPhi)/2.',
                  order = {'QED':1})

GC_435 = Coupling(name = 'GC_435',
                  value = '-(cw*ee*complex(0,1)*I1a11*Sp)/(4.*sw) - (cw*ee*complex(0,1)*I2a11*Sp)/(4.*sw) - (ee*complex(0,1)*I1a11*Sp*sw)/(4.*cw) - (ee*complex(0,1)*I2a11*Sp*sw)/(4.*cw) - (Cp*complex(0,1)*g1p*I1a11*xH)/4. - (Cp*complex(0,1)*g1p*I2a11*xH)/4. - (Cp*complex(0,1)*g1p*I1a11*xPhi)/2. - (Cp*complex(0,1)*g1p*I2a11*xPhi)/2. + (Cp*complex(0,1)*g1p*I3a11*xPhi)/2. + (Cp*complex(0,1)*g1p*I4a11*xPhi)/2.',
                  order = {'QED':1})

GC_436 = Coupling(name = 'GC_436',
                  value = '(cw*ee*complex(0,1)*I1a12*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I1a21*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I2a12*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I2a21*Sp)/(8.*sw) + (ee*complex(0,1)*I1a12*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I1a21*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I2a12*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I2a21*Sp*sw)/(8.*cw) + (Cp*complex(0,1)*g1p*I1a12*xH)/8. + (Cp*complex(0,1)*g1p*I1a21*xH)/8. + (Cp*complex(0,1)*g1p*I2a12*xH)/8. + (Cp*complex(0,1)*g1p*I2a21*xH)/8. + (Cp*complex(0,1)*g1p*I1a12*xPhi)/4. + (Cp*complex(0,1)*g1p*I1a21*xPhi)/4. + (Cp*complex(0,1)*g1p*I2a12*xPhi)/4. + (Cp*complex(0,1)*g1p*I2a21*xPhi)/4. - (Cp*complex(0,1)*g1p*I3a12*xPhi)/4. - (Cp*complex(0,1)*g1p*I3a21*xPhi)/4. - (Cp*complex(0,1)*g1p*I4a12*xPhi)/4. - (Cp*complex(0,1)*g1p*I4a21*xPhi)/4.',
                  order = {'QED':1})

GC_437 = Coupling(name = 'GC_437',
                  value = '-(cw*ee*complex(0,1)*I1a12*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I1a21*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I2a12*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I2a21*Sp)/(8.*sw) - (ee*complex(0,1)*I1a12*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I1a21*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I2a12*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I2a21*Sp*sw)/(8.*cw) - (Cp*complex(0,1)*g1p*I1a12*xH)/8. - (Cp*complex(0,1)*g1p*I1a21*xH)/8. - (Cp*complex(0,1)*g1p*I2a12*xH)/8. - (Cp*complex(0,1)*g1p*I2a21*xH)/8. - (Cp*complex(0,1)*g1p*I1a12*xPhi)/4. - (Cp*complex(0,1)*g1p*I1a21*xPhi)/4. - (Cp*complex(0,1)*g1p*I2a12*xPhi)/4. - (Cp*complex(0,1)*g1p*I2a21*xPhi)/4. + (Cp*complex(0,1)*g1p*I3a12*xPhi)/4. + (Cp*complex(0,1)*g1p*I3a21*xPhi)/4. + (Cp*complex(0,1)*g1p*I4a12*xPhi)/4. + (Cp*complex(0,1)*g1p*I4a21*xPhi)/4.',
                  order = {'QED':1})

GC_438 = Coupling(name = 'GC_438',
                  value = '(cw*ee*complex(0,1)*I1a22*Sp)/(4.*sw) + (cw*ee*complex(0,1)*I2a22*Sp)/(4.*sw) + (ee*complex(0,1)*I1a22*Sp*sw)/(4.*cw) + (ee*complex(0,1)*I2a22*Sp*sw)/(4.*cw) + (Cp*complex(0,1)*g1p*I1a22*xH)/4. + (Cp*complex(0,1)*g1p*I2a22*xH)/4. + (Cp*complex(0,1)*g1p*I1a22*xPhi)/2. + (Cp*complex(0,1)*g1p*I2a22*xPhi)/2. - (Cp*complex(0,1)*g1p*I3a22*xPhi)/2. - (Cp*complex(0,1)*g1p*I4a22*xPhi)/2.',
                  order = {'QED':1})

GC_439 = Coupling(name = 'GC_439',
                  value = '-(cw*ee*complex(0,1)*I1a22*Sp)/(4.*sw) - (cw*ee*complex(0,1)*I2a22*Sp)/(4.*sw) - (ee*complex(0,1)*I1a22*Sp*sw)/(4.*cw) - (ee*complex(0,1)*I2a22*Sp*sw)/(4.*cw) - (Cp*complex(0,1)*g1p*I1a22*xH)/4. - (Cp*complex(0,1)*g1p*I2a22*xH)/4. - (Cp*complex(0,1)*g1p*I1a22*xPhi)/2. - (Cp*complex(0,1)*g1p*I2a22*xPhi)/2. + (Cp*complex(0,1)*g1p*I3a22*xPhi)/2. + (Cp*complex(0,1)*g1p*I4a22*xPhi)/2.',
                  order = {'QED':1})

GC_440 = Coupling(name = 'GC_440',
                  value = '(cw*ee*complex(0,1)*I1a13*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I1a31*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I2a13*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I2a31*Sp)/(8.*sw) + (ee*complex(0,1)*I1a13*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I1a31*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I2a13*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I2a31*Sp*sw)/(8.*cw) + (Cp*complex(0,1)*g1p*I1a13*xH)/8. + (Cp*complex(0,1)*g1p*I1a31*xH)/8. + (Cp*complex(0,1)*g1p*I2a13*xH)/8. + (Cp*complex(0,1)*g1p*I2a31*xH)/8. + (Cp*complex(0,1)*g1p*I1a13*xPhi)/4. + (Cp*complex(0,1)*g1p*I1a31*xPhi)/4. + (Cp*complex(0,1)*g1p*I2a13*xPhi)/4. + (Cp*complex(0,1)*g1p*I2a31*xPhi)/4. - (Cp*complex(0,1)*g1p*I3a13*xPhi)/4. - (Cp*complex(0,1)*g1p*I3a31*xPhi)/4. - (Cp*complex(0,1)*g1p*I4a13*xPhi)/4. - (Cp*complex(0,1)*g1p*I4a31*xPhi)/4.',
                  order = {'QED':1})

GC_441 = Coupling(name = 'GC_441',
                  value = '-(cw*ee*complex(0,1)*I1a13*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I1a31*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I2a13*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I2a31*Sp)/(8.*sw) - (ee*complex(0,1)*I1a13*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I1a31*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I2a13*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I2a31*Sp*sw)/(8.*cw) - (Cp*complex(0,1)*g1p*I1a13*xH)/8. - (Cp*complex(0,1)*g1p*I1a31*xH)/8. - (Cp*complex(0,1)*g1p*I2a13*xH)/8. - (Cp*complex(0,1)*g1p*I2a31*xH)/8. - (Cp*complex(0,1)*g1p*I1a13*xPhi)/4. - (Cp*complex(0,1)*g1p*I1a31*xPhi)/4. - (Cp*complex(0,1)*g1p*I2a13*xPhi)/4. - (Cp*complex(0,1)*g1p*I2a31*xPhi)/4. + (Cp*complex(0,1)*g1p*I3a13*xPhi)/4. + (Cp*complex(0,1)*g1p*I3a31*xPhi)/4. + (Cp*complex(0,1)*g1p*I4a13*xPhi)/4. + (Cp*complex(0,1)*g1p*I4a31*xPhi)/4.',
                  order = {'QED':1})

GC_442 = Coupling(name = 'GC_442',
                  value = '(cw*ee*complex(0,1)*I1a23*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I1a32*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I2a23*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I2a32*Sp)/(8.*sw) + (ee*complex(0,1)*I1a23*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I1a32*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I2a23*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I2a32*Sp*sw)/(8.*cw) + (Cp*complex(0,1)*g1p*I1a23*xH)/8. + (Cp*complex(0,1)*g1p*I1a32*xH)/8. + (Cp*complex(0,1)*g1p*I2a23*xH)/8. + (Cp*complex(0,1)*g1p*I2a32*xH)/8. + (Cp*complex(0,1)*g1p*I1a23*xPhi)/4. + (Cp*complex(0,1)*g1p*I1a32*xPhi)/4. + (Cp*complex(0,1)*g1p*I2a23*xPhi)/4. + (Cp*complex(0,1)*g1p*I2a32*xPhi)/4. - (Cp*complex(0,1)*g1p*I3a23*xPhi)/4. - (Cp*complex(0,1)*g1p*I3a32*xPhi)/4. - (Cp*complex(0,1)*g1p*I4a23*xPhi)/4. - (Cp*complex(0,1)*g1p*I4a32*xPhi)/4.',
                  order = {'QED':1})

GC_443 = Coupling(name = 'GC_443',
                  value = '-(cw*ee*complex(0,1)*I1a23*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I1a32*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I2a23*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I2a32*Sp)/(8.*sw) - (ee*complex(0,1)*I1a23*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I1a32*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I2a23*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I2a32*Sp*sw)/(8.*cw) - (Cp*complex(0,1)*g1p*I1a23*xH)/8. - (Cp*complex(0,1)*g1p*I1a32*xH)/8. - (Cp*complex(0,1)*g1p*I2a23*xH)/8. - (Cp*complex(0,1)*g1p*I2a32*xH)/8. - (Cp*complex(0,1)*g1p*I1a23*xPhi)/4. - (Cp*complex(0,1)*g1p*I1a32*xPhi)/4. - (Cp*complex(0,1)*g1p*I2a23*xPhi)/4. - (Cp*complex(0,1)*g1p*I2a32*xPhi)/4. + (Cp*complex(0,1)*g1p*I3a23*xPhi)/4. + (Cp*complex(0,1)*g1p*I3a32*xPhi)/4. + (Cp*complex(0,1)*g1p*I4a23*xPhi)/4. + (Cp*complex(0,1)*g1p*I4a32*xPhi)/4.',
                  order = {'QED':1})

GC_444 = Coupling(name = 'GC_444',
                  value = '(cw*ee*complex(0,1)*I1a33*Sp)/(4.*sw) + (cw*ee*complex(0,1)*I2a33*Sp)/(4.*sw) + (ee*complex(0,1)*I1a33*Sp*sw)/(4.*cw) + (ee*complex(0,1)*I2a33*Sp*sw)/(4.*cw) + (Cp*complex(0,1)*g1p*I1a33*xH)/4. + (Cp*complex(0,1)*g1p*I2a33*xH)/4. + (Cp*complex(0,1)*g1p*I1a33*xPhi)/2. + (Cp*complex(0,1)*g1p*I2a33*xPhi)/2. - (Cp*complex(0,1)*g1p*I3a33*xPhi)/2. - (Cp*complex(0,1)*g1p*I4a33*xPhi)/2.',
                  order = {'QED':1})

GC_445 = Coupling(name = 'GC_445',
                  value = '-(cw*ee*complex(0,1)*I1a33*Sp)/(4.*sw) - (cw*ee*complex(0,1)*I2a33*Sp)/(4.*sw) - (ee*complex(0,1)*I1a33*Sp*sw)/(4.*cw) - (ee*complex(0,1)*I2a33*Sp*sw)/(4.*cw) - (Cp*complex(0,1)*g1p*I1a33*xH)/4. - (Cp*complex(0,1)*g1p*I2a33*xH)/4. - (Cp*complex(0,1)*g1p*I1a33*xPhi)/2. - (Cp*complex(0,1)*g1p*I2a33*xPhi)/2. + (Cp*complex(0,1)*g1p*I3a33*xPhi)/2. + (Cp*complex(0,1)*g1p*I4a33*xPhi)/2.',
                  order = {'QED':1})

GC_446 = Coupling(name = 'GC_446',
                  value = '-(cw*ee*complex(0,1)*I6a11*Sp)/(2.*sw) - (ee*complex(0,1)*I6a11*Sp*sw)/(2.*cw) - (Cp*complex(0,1)*g1p*I6a11*xH)/2. + Cp*complex(0,1)*g1p*I5a11*xPhi - Cp*complex(0,1)*g1p*I6a11*xPhi',
                  order = {'QED':1})

GC_447 = Coupling(name = 'GC_447',
                  value = '-(cw*ee*complex(0,1)*I6a12*Sp)/(2.*sw) - (ee*complex(0,1)*I6a12*Sp*sw)/(2.*cw) - (Cp*complex(0,1)*g1p*I6a12*xH)/2. + Cp*complex(0,1)*g1p*I5a12*xPhi - Cp*complex(0,1)*g1p*I6a12*xPhi',
                  order = {'QED':1})

GC_448 = Coupling(name = 'GC_448',
                  value = '-(cw*ee*complex(0,1)*I6a13*Sp)/(2.*sw) - (ee*complex(0,1)*I6a13*Sp*sw)/(2.*cw) - (Cp*complex(0,1)*g1p*I6a13*xH)/2. + Cp*complex(0,1)*g1p*I5a13*xPhi - Cp*complex(0,1)*g1p*I6a13*xPhi',
                  order = {'QED':1})

GC_449 = Coupling(name = 'GC_449',
                  value = '-(cw*ee*complex(0,1)*I6a21*Sp)/(2.*sw) - (ee*complex(0,1)*I6a21*Sp*sw)/(2.*cw) - (Cp*complex(0,1)*g1p*I6a21*xH)/2. + Cp*complex(0,1)*g1p*I5a21*xPhi - Cp*complex(0,1)*g1p*I6a21*xPhi',
                  order = {'QED':1})

GC_450 = Coupling(name = 'GC_450',
                  value = '-(cw*ee*complex(0,1)*I6a22*Sp)/(2.*sw) - (ee*complex(0,1)*I6a22*Sp*sw)/(2.*cw) - (Cp*complex(0,1)*g1p*I6a22*xH)/2. + Cp*complex(0,1)*g1p*I5a22*xPhi - Cp*complex(0,1)*g1p*I6a22*xPhi',
                  order = {'QED':1})

GC_451 = Coupling(name = 'GC_451',
                  value = '-(cw*ee*complex(0,1)*I6a23*Sp)/(2.*sw) - (ee*complex(0,1)*I6a23*Sp*sw)/(2.*cw) - (Cp*complex(0,1)*g1p*I6a23*xH)/2. + Cp*complex(0,1)*g1p*I5a23*xPhi - Cp*complex(0,1)*g1p*I6a23*xPhi',
                  order = {'QED':1})

GC_452 = Coupling(name = 'GC_452',
                  value = '-(cw*ee*complex(0,1)*I6a31*Sp)/(2.*sw) - (ee*complex(0,1)*I6a31*Sp*sw)/(2.*cw) - (Cp*complex(0,1)*g1p*I6a31*xH)/2. + Cp*complex(0,1)*g1p*I5a31*xPhi - Cp*complex(0,1)*g1p*I6a31*xPhi',
                  order = {'QED':1})

GC_453 = Coupling(name = 'GC_453',
                  value = '-(cw*ee*complex(0,1)*I6a32*Sp)/(2.*sw) - (ee*complex(0,1)*I6a32*Sp*sw)/(2.*cw) - (Cp*complex(0,1)*g1p*I6a32*xH)/2. + Cp*complex(0,1)*g1p*I5a32*xPhi - Cp*complex(0,1)*g1p*I6a32*xPhi',
                  order = {'QED':1})

GC_454 = Coupling(name = 'GC_454',
                  value = '-(cw*ee*complex(0,1)*I6a33*Sp)/(2.*sw) - (ee*complex(0,1)*I6a33*Sp*sw)/(2.*cw) - (Cp*complex(0,1)*g1p*I6a33*xH)/2. + Cp*complex(0,1)*g1p*I5a33*xPhi - Cp*complex(0,1)*g1p*I6a33*xPhi',
                  order = {'QED':1})

GC_455 = Coupling(name = 'GC_455',
                  value = '(cw*ee*complex(0,1)*I7a11*Sp)/(2.*sw) + (ee*complex(0,1)*I7a11*Sp*sw)/(2.*cw) + (Cp*complex(0,1)*g1p*I7a11*xH)/2. + Cp*complex(0,1)*g1p*I7a11*xPhi - Cp*complex(0,1)*g1p*I8a11*xPhi',
                  order = {'QED':1})

GC_456 = Coupling(name = 'GC_456',
                  value = '(cw*ee*complex(0,1)*I7a12*Sp)/(2.*sw) + (ee*complex(0,1)*I7a12*Sp*sw)/(2.*cw) + (Cp*complex(0,1)*g1p*I7a12*xH)/2. + Cp*complex(0,1)*g1p*I7a12*xPhi - Cp*complex(0,1)*g1p*I8a12*xPhi',
                  order = {'QED':1})

GC_457 = Coupling(name = 'GC_457',
                  value = '(cw*ee*complex(0,1)*I7a13*Sp)/(2.*sw) + (ee*complex(0,1)*I7a13*Sp*sw)/(2.*cw) + (Cp*complex(0,1)*g1p*I7a13*xH)/2. + Cp*complex(0,1)*g1p*I7a13*xPhi - Cp*complex(0,1)*g1p*I8a13*xPhi',
                  order = {'QED':1})

GC_458 = Coupling(name = 'GC_458',
                  value = '(cw*ee*complex(0,1)*I7a21*Sp)/(2.*sw) + (ee*complex(0,1)*I7a21*Sp*sw)/(2.*cw) + (Cp*complex(0,1)*g1p*I7a21*xH)/2. + Cp*complex(0,1)*g1p*I7a21*xPhi - Cp*complex(0,1)*g1p*I8a21*xPhi',
                  order = {'QED':1})

GC_459 = Coupling(name = 'GC_459',
                  value = '(cw*ee*complex(0,1)*I7a22*Sp)/(2.*sw) + (ee*complex(0,1)*I7a22*Sp*sw)/(2.*cw) + (Cp*complex(0,1)*g1p*I7a22*xH)/2. + Cp*complex(0,1)*g1p*I7a22*xPhi - Cp*complex(0,1)*g1p*I8a22*xPhi',
                  order = {'QED':1})

GC_460 = Coupling(name = 'GC_460',
                  value = '(cw*ee*complex(0,1)*I7a23*Sp)/(2.*sw) + (ee*complex(0,1)*I7a23*Sp*sw)/(2.*cw) + (Cp*complex(0,1)*g1p*I7a23*xH)/2. + Cp*complex(0,1)*g1p*I7a23*xPhi - Cp*complex(0,1)*g1p*I8a23*xPhi',
                  order = {'QED':1})

GC_461 = Coupling(name = 'GC_461',
                  value = '(cw*ee*complex(0,1)*I7a31*Sp)/(2.*sw) + (ee*complex(0,1)*I7a31*Sp*sw)/(2.*cw) + (Cp*complex(0,1)*g1p*I7a31*xH)/2. + Cp*complex(0,1)*g1p*I7a31*xPhi - Cp*complex(0,1)*g1p*I8a31*xPhi',
                  order = {'QED':1})

GC_462 = Coupling(name = 'GC_462',
                  value = '(cw*ee*complex(0,1)*I7a32*Sp)/(2.*sw) + (ee*complex(0,1)*I7a32*Sp*sw)/(2.*cw) + (Cp*complex(0,1)*g1p*I7a32*xH)/2. + Cp*complex(0,1)*g1p*I7a32*xPhi - Cp*complex(0,1)*g1p*I8a32*xPhi',
                  order = {'QED':1})

GC_463 = Coupling(name = 'GC_463',
                  value = '(cw*ee*complex(0,1)*I7a33*Sp)/(2.*sw) + (ee*complex(0,1)*I7a33*Sp*sw)/(2.*cw) + (Cp*complex(0,1)*g1p*I7a33*xH)/2. + Cp*complex(0,1)*g1p*I7a33*xPhi - Cp*complex(0,1)*g1p*I8a33*xPhi',
                  order = {'QED':1})

GC_464 = Coupling(name = 'GC_464',
                  value = '(cw*ee*complex(0,1)*I11a11*Sp)/(4.*sw) + (cw*ee*complex(0,1)*I12a11*Sp)/(4.*sw) + (ee*complex(0,1)*I11a11*Sp*sw)/(4.*cw) + (ee*complex(0,1)*I12a11*Sp*sw)/(4.*cw) + (Cp*complex(0,1)*g1p*I11a11*xH)/4. + (Cp*complex(0,1)*g1p*I12a11*xH)/4. - (Cp*complex(0,1)*g1p*I10a11*xPhi)/2. + (Cp*complex(0,1)*g1p*I11a11*xPhi)/2. + (Cp*complex(0,1)*g1p*I12a11*xPhi)/2. - (Cp*complex(0,1)*g1p*I9a11*xPhi)/2.',
                  order = {'QED':1})

GC_465 = Coupling(name = 'GC_465',
                  value = '-(cw*ee*complex(0,1)*I11a11*Sp)/(4.*sw) - (cw*ee*complex(0,1)*I12a11*Sp)/(4.*sw) - (ee*complex(0,1)*I11a11*Sp*sw)/(4.*cw) - (ee*complex(0,1)*I12a11*Sp*sw)/(4.*cw) - (Cp*complex(0,1)*g1p*I11a11*xH)/4. - (Cp*complex(0,1)*g1p*I12a11*xH)/4. + (Cp*complex(0,1)*g1p*I10a11*xPhi)/2. - (Cp*complex(0,1)*g1p*I11a11*xPhi)/2. - (Cp*complex(0,1)*g1p*I12a11*xPhi)/2. + (Cp*complex(0,1)*g1p*I9a11*xPhi)/2.',
                  order = {'QED':1})

GC_466 = Coupling(name = 'GC_466',
                  value = '(cw*ee*complex(0,1)*I11a12*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I11a21*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I12a12*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I12a21*Sp)/(8.*sw) + (ee*complex(0,1)*I11a12*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I11a21*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I12a12*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I12a21*Sp*sw)/(8.*cw) + (Cp*complex(0,1)*g1p*I11a12*xH)/8. + (Cp*complex(0,1)*g1p*I11a21*xH)/8. + (Cp*complex(0,1)*g1p*I12a12*xH)/8. + (Cp*complex(0,1)*g1p*I12a21*xH)/8. - (Cp*complex(0,1)*g1p*I10a12*xPhi)/4. - (Cp*complex(0,1)*g1p*I10a21*xPhi)/4. + (Cp*complex(0,1)*g1p*I11a12*xPhi)/4. + (Cp*complex(0,1)*g1p*I11a21*xPhi)/4. + (Cp*complex(0,1)*g1p*I12a12*xPhi)/4. + (Cp*complex(0,1)*g1p*I12a21*xPhi)/4. - (Cp*complex(0,1)*g1p*I9a12*xPhi)/4. - (Cp*complex(0,1)*g1p*I9a21*xPhi)/4.',
                  order = {'QED':1})

GC_467 = Coupling(name = 'GC_467',
                  value = '-(cw*ee*complex(0,1)*I11a12*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I11a21*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I12a12*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I12a21*Sp)/(8.*sw) - (ee*complex(0,1)*I11a12*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I11a21*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I12a12*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I12a21*Sp*sw)/(8.*cw) - (Cp*complex(0,1)*g1p*I11a12*xH)/8. - (Cp*complex(0,1)*g1p*I11a21*xH)/8. - (Cp*complex(0,1)*g1p*I12a12*xH)/8. - (Cp*complex(0,1)*g1p*I12a21*xH)/8. + (Cp*complex(0,1)*g1p*I10a12*xPhi)/4. + (Cp*complex(0,1)*g1p*I10a21*xPhi)/4. - (Cp*complex(0,1)*g1p*I11a12*xPhi)/4. - (Cp*complex(0,1)*g1p*I11a21*xPhi)/4. - (Cp*complex(0,1)*g1p*I12a12*xPhi)/4. - (Cp*complex(0,1)*g1p*I12a21*xPhi)/4. + (Cp*complex(0,1)*g1p*I9a12*xPhi)/4. + (Cp*complex(0,1)*g1p*I9a21*xPhi)/4.',
                  order = {'QED':1})

GC_468 = Coupling(name = 'GC_468',
                  value = '(cw*ee*complex(0,1)*I11a22*Sp)/(4.*sw) + (cw*ee*complex(0,1)*I12a22*Sp)/(4.*sw) + (ee*complex(0,1)*I11a22*Sp*sw)/(4.*cw) + (ee*complex(0,1)*I12a22*Sp*sw)/(4.*cw) + (Cp*complex(0,1)*g1p*I11a22*xH)/4. + (Cp*complex(0,1)*g1p*I12a22*xH)/4. - (Cp*complex(0,1)*g1p*I10a22*xPhi)/2. + (Cp*complex(0,1)*g1p*I11a22*xPhi)/2. + (Cp*complex(0,1)*g1p*I12a22*xPhi)/2. - (Cp*complex(0,1)*g1p*I9a22*xPhi)/2.',
                  order = {'QED':1})

GC_469 = Coupling(name = 'GC_469',
                  value = '-(cw*ee*complex(0,1)*I11a22*Sp)/(4.*sw) - (cw*ee*complex(0,1)*I12a22*Sp)/(4.*sw) - (ee*complex(0,1)*I11a22*Sp*sw)/(4.*cw) - (ee*complex(0,1)*I12a22*Sp*sw)/(4.*cw) - (Cp*complex(0,1)*g1p*I11a22*xH)/4. - (Cp*complex(0,1)*g1p*I12a22*xH)/4. + (Cp*complex(0,1)*g1p*I10a22*xPhi)/2. - (Cp*complex(0,1)*g1p*I11a22*xPhi)/2. - (Cp*complex(0,1)*g1p*I12a22*xPhi)/2. + (Cp*complex(0,1)*g1p*I9a22*xPhi)/2.',
                  order = {'QED':1})

GC_470 = Coupling(name = 'GC_470',
                  value = '(cw*ee*complex(0,1)*I11a13*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I11a31*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I12a13*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I12a31*Sp)/(8.*sw) + (ee*complex(0,1)*I11a13*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I11a31*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I12a13*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I12a31*Sp*sw)/(8.*cw) + (Cp*complex(0,1)*g1p*I11a13*xH)/8. + (Cp*complex(0,1)*g1p*I11a31*xH)/8. + (Cp*complex(0,1)*g1p*I12a13*xH)/8. + (Cp*complex(0,1)*g1p*I12a31*xH)/8. - (Cp*complex(0,1)*g1p*I10a13*xPhi)/4. - (Cp*complex(0,1)*g1p*I10a31*xPhi)/4. + (Cp*complex(0,1)*g1p*I11a13*xPhi)/4. + (Cp*complex(0,1)*g1p*I11a31*xPhi)/4. + (Cp*complex(0,1)*g1p*I12a13*xPhi)/4. + (Cp*complex(0,1)*g1p*I12a31*xPhi)/4. - (Cp*complex(0,1)*g1p*I9a13*xPhi)/4. - (Cp*complex(0,1)*g1p*I9a31*xPhi)/4.',
                  order = {'QED':1})

GC_471 = Coupling(name = 'GC_471',
                  value = '-(cw*ee*complex(0,1)*I11a13*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I11a31*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I12a13*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I12a31*Sp)/(8.*sw) - (ee*complex(0,1)*I11a13*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I11a31*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I12a13*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I12a31*Sp*sw)/(8.*cw) - (Cp*complex(0,1)*g1p*I11a13*xH)/8. - (Cp*complex(0,1)*g1p*I11a31*xH)/8. - (Cp*complex(0,1)*g1p*I12a13*xH)/8. - (Cp*complex(0,1)*g1p*I12a31*xH)/8. + (Cp*complex(0,1)*g1p*I10a13*xPhi)/4. + (Cp*complex(0,1)*g1p*I10a31*xPhi)/4. - (Cp*complex(0,1)*g1p*I11a13*xPhi)/4. - (Cp*complex(0,1)*g1p*I11a31*xPhi)/4. - (Cp*complex(0,1)*g1p*I12a13*xPhi)/4. - (Cp*complex(0,1)*g1p*I12a31*xPhi)/4. + (Cp*complex(0,1)*g1p*I9a13*xPhi)/4. + (Cp*complex(0,1)*g1p*I9a31*xPhi)/4.',
                  order = {'QED':1})

GC_472 = Coupling(name = 'GC_472',
                  value = '(cw*ee*complex(0,1)*I11a23*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I11a32*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I12a23*Sp)/(8.*sw) + (cw*ee*complex(0,1)*I12a32*Sp)/(8.*sw) + (ee*complex(0,1)*I11a23*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I11a32*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I12a23*Sp*sw)/(8.*cw) + (ee*complex(0,1)*I12a32*Sp*sw)/(8.*cw) + (Cp*complex(0,1)*g1p*I11a23*xH)/8. + (Cp*complex(0,1)*g1p*I11a32*xH)/8. + (Cp*complex(0,1)*g1p*I12a23*xH)/8. + (Cp*complex(0,1)*g1p*I12a32*xH)/8. - (Cp*complex(0,1)*g1p*I10a23*xPhi)/4. - (Cp*complex(0,1)*g1p*I10a32*xPhi)/4. + (Cp*complex(0,1)*g1p*I11a23*xPhi)/4. + (Cp*complex(0,1)*g1p*I11a32*xPhi)/4. + (Cp*complex(0,1)*g1p*I12a23*xPhi)/4. + (Cp*complex(0,1)*g1p*I12a32*xPhi)/4. - (Cp*complex(0,1)*g1p*I9a23*xPhi)/4. - (Cp*complex(0,1)*g1p*I9a32*xPhi)/4.',
                  order = {'QED':1})

GC_473 = Coupling(name = 'GC_473',
                  value = '-(cw*ee*complex(0,1)*I11a23*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I11a32*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I12a23*Sp)/(8.*sw) - (cw*ee*complex(0,1)*I12a32*Sp)/(8.*sw) - (ee*complex(0,1)*I11a23*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I11a32*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I12a23*Sp*sw)/(8.*cw) - (ee*complex(0,1)*I12a32*Sp*sw)/(8.*cw) - (Cp*complex(0,1)*g1p*I11a23*xH)/8. - (Cp*complex(0,1)*g1p*I11a32*xH)/8. - (Cp*complex(0,1)*g1p*I12a23*xH)/8. - (Cp*complex(0,1)*g1p*I12a32*xH)/8. + (Cp*complex(0,1)*g1p*I10a23*xPhi)/4. + (Cp*complex(0,1)*g1p*I10a32*xPhi)/4. - (Cp*complex(0,1)*g1p*I11a23*xPhi)/4. - (Cp*complex(0,1)*g1p*I11a32*xPhi)/4. - (Cp*complex(0,1)*g1p*I12a23*xPhi)/4. - (Cp*complex(0,1)*g1p*I12a32*xPhi)/4. + (Cp*complex(0,1)*g1p*I9a23*xPhi)/4. + (Cp*complex(0,1)*g1p*I9a32*xPhi)/4.',
                  order = {'QED':1})

GC_474 = Coupling(name = 'GC_474',
                  value = '(cw*ee*complex(0,1)*I11a33*Sp)/(4.*sw) + (cw*ee*complex(0,1)*I12a33*Sp)/(4.*sw) + (ee*complex(0,1)*I11a33*Sp*sw)/(4.*cw) + (ee*complex(0,1)*I12a33*Sp*sw)/(4.*cw) + (Cp*complex(0,1)*g1p*I11a33*xH)/4. + (Cp*complex(0,1)*g1p*I12a33*xH)/4. - (Cp*complex(0,1)*g1p*I10a33*xPhi)/2. + (Cp*complex(0,1)*g1p*I11a33*xPhi)/2. + (Cp*complex(0,1)*g1p*I12a33*xPhi)/2. - (Cp*complex(0,1)*g1p*I9a33*xPhi)/2.',
                  order = {'QED':1})

GC_475 = Coupling(name = 'GC_475',
                  value = '-(cw*ee*complex(0,1)*I11a33*Sp)/(4.*sw) - (cw*ee*complex(0,1)*I12a33*Sp)/(4.*sw) - (ee*complex(0,1)*I11a33*Sp*sw)/(4.*cw) - (ee*complex(0,1)*I12a33*Sp*sw)/(4.*cw) - (Cp*complex(0,1)*g1p*I11a33*xH)/4. - (Cp*complex(0,1)*g1p*I12a33*xH)/4. + (Cp*complex(0,1)*g1p*I10a33*xPhi)/2. - (Cp*complex(0,1)*g1p*I11a33*xPhi)/2. - (Cp*complex(0,1)*g1p*I12a33*xPhi)/2. + (Cp*complex(0,1)*g1p*I9a33*xPhi)/2.',
                  order = {'QED':1})

GC_476 = Coupling(name = 'GC_476',
                  value = '(Ca*cw*ee*sg*Sp)/(2.*sw) + (Ca*ee*sg*Sp*sw)/(2.*cw) + (Ca*Cp*g1p*sg*xH)/2. + 2*cg*Cp*g1p*Sa*xPhi',
                  order = {'QED':1})

GC_477 = Coupling(name = 'GC_477',
                  value = '(cg*cw*ee*Sa*Sp)/(2.*sw) + (cg*ee*Sa*Sp*sw)/(2.*cw) + (cg*Cp*g1p*Sa*xH)/2. + 2*Ca*Cp*g1p*sg*xPhi',
                  order = {'QED':1})

GC_478 = Coupling(name = 'GC_478',
                  value = '(Ca*cg*cw*ee*Sp)/(2.*sw) + (Ca*cg*ee*Sp*sw)/(2.*cw) + (Ca*cg*Cp*g1p*xH)/2. - 2*Cp*g1p*Sa*sg*xPhi',
                  order = {'QED':1})

GC_479 = Coupling(name = 'GC_479',
                  value = '-(Cp*cw*ee*complex(0,1))/(2.*sw) - (Cp*ee*complex(0,1)*sw)/(6.*cw) + (complex(0,1)*g1p*Sp*xH)/6. + (complex(0,1)*g1p*Sp*xPhi)/3.',
                  order = {'QED':1})

GC_480 = Coupling(name = 'GC_480',
                  value = '(Cp*cw*ee*complex(0,1))/(2.*sw) - (Cp*ee*complex(0,1)*sw)/(6.*cw) + (complex(0,1)*g1p*Sp*xH)/6. + (complex(0,1)*g1p*Sp*xPhi)/3.',
                  order = {'QED':1})

GC_481 = Coupling(name = 'GC_481',
                  value = '(Cp*ee*complex(0,1)*sw)/(3.*cw) - (complex(0,1)*g1p*Sp*xH)/3. + (complex(0,1)*g1p*Sp*xPhi)/3.',
                  order = {'QED':1})

GC_482 = Coupling(name = 'GC_482',
                  value = '(-2*Cp*ee*complex(0,1)*sw)/(3.*cw) + (2*complex(0,1)*g1p*Sp*xH)/3. + (complex(0,1)*g1p*Sp*xPhi)/3.',
                  order = {'QED':1})

GC_483 = Coupling(name = 'GC_483',
                  value = '-(Cp*cw*ee*complex(0,1))/(2.*sw) + (Cp*ee*complex(0,1)*sw)/(2.*cw) - (complex(0,1)*g1p*Sp*xH)/2. - complex(0,1)*g1p*Sp*xPhi',
                  order = {'QED':1})

GC_484 = Coupling(name = 'GC_484',
                  value = '(Cp*ee*complex(0,1)*sw)/cw - complex(0,1)*g1p*Sp*xH - complex(0,1)*g1p*Sp*xPhi',
                  order = {'QED':1})

GC_485 = Coupling(name = 'GC_485',
                  value = '-(Cp*cw*ee*Sa*sg)/(2.*sw) - (Cp*ee*Sa*sg*sw)/(2.*cw) + (g1p*Sa*sg*Sp*xH)/2. - 2*Ca*cg*g1p*Sp*xPhi',
                  order = {'QED':1})

GC_486 = Coupling(name = 'GC_486',
                  value = '-(Cp*cw*ee*complex(0,1)*I1a11)/(4.*sw) - (Cp*cw*ee*complex(0,1)*I2a11)/(4.*sw) - (Cp*ee*complex(0,1)*I1a11*sw)/(4.*cw) - (Cp*ee*complex(0,1)*I2a11*sw)/(4.*cw) + (complex(0,1)*g1p*I1a11*Sp*xH)/4. + (complex(0,1)*g1p*I2a11*Sp*xH)/4. + (complex(0,1)*g1p*I1a11*Sp*xPhi)/2. + (complex(0,1)*g1p*I2a11*Sp*xPhi)/2. - (complex(0,1)*g1p*I3a11*Sp*xPhi)/2. - (complex(0,1)*g1p*I4a11*Sp*xPhi)/2.',
                  order = {'QED':1})

GC_487 = Coupling(name = 'GC_487',
                  value = '(Cp*cw*ee*complex(0,1)*I1a11)/(4.*sw) + (Cp*cw*ee*complex(0,1)*I2a11)/(4.*sw) + (Cp*ee*complex(0,1)*I1a11*sw)/(4.*cw) + (Cp*ee*complex(0,1)*I2a11*sw)/(4.*cw) - (complex(0,1)*g1p*I1a11*Sp*xH)/4. - (complex(0,1)*g1p*I2a11*Sp*xH)/4. - (complex(0,1)*g1p*I1a11*Sp*xPhi)/2. - (complex(0,1)*g1p*I2a11*Sp*xPhi)/2. + (complex(0,1)*g1p*I3a11*Sp*xPhi)/2. + (complex(0,1)*g1p*I4a11*Sp*xPhi)/2.',
                  order = {'QED':1})

GC_488 = Coupling(name = 'GC_488',
                  value = '-(Cp*cw*ee*complex(0,1)*I1a12)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I1a21)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I2a12)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I2a21)/(8.*sw) - (Cp*ee*complex(0,1)*I1a12*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I1a21*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I2a12*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I2a21*sw)/(8.*cw) + (complex(0,1)*g1p*I1a12*Sp*xH)/8. + (complex(0,1)*g1p*I1a21*Sp*xH)/8. + (complex(0,1)*g1p*I2a12*Sp*xH)/8. + (complex(0,1)*g1p*I2a21*Sp*xH)/8. + (complex(0,1)*g1p*I1a12*Sp*xPhi)/4. + (complex(0,1)*g1p*I1a21*Sp*xPhi)/4. + (complex(0,1)*g1p*I2a12*Sp*xPhi)/4. + (complex(0,1)*g1p*I2a21*Sp*xPhi)/4. - (complex(0,1)*g1p*I3a12*Sp*xPhi)/4. - (complex(0,1)*g1p*I3a21*Sp*xPhi)/4. - (complex(0,1)*g1p*I4a12*Sp*xPhi)/4. - (complex(0,1)*g1p*I4a21*Sp*xPhi)/4.',
                  order = {'QED':1})

GC_489 = Coupling(name = 'GC_489',
                  value = '(Cp*cw*ee*complex(0,1)*I1a12)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I1a21)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I2a12)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I2a21)/(8.*sw) + (Cp*ee*complex(0,1)*I1a12*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I1a21*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I2a12*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I2a21*sw)/(8.*cw) - (complex(0,1)*g1p*I1a12*Sp*xH)/8. - (complex(0,1)*g1p*I1a21*Sp*xH)/8. - (complex(0,1)*g1p*I2a12*Sp*xH)/8. - (complex(0,1)*g1p*I2a21*Sp*xH)/8. - (complex(0,1)*g1p*I1a12*Sp*xPhi)/4. - (complex(0,1)*g1p*I1a21*Sp*xPhi)/4. - (complex(0,1)*g1p*I2a12*Sp*xPhi)/4. - (complex(0,1)*g1p*I2a21*Sp*xPhi)/4. + (complex(0,1)*g1p*I3a12*Sp*xPhi)/4. + (complex(0,1)*g1p*I3a21*Sp*xPhi)/4. + (complex(0,1)*g1p*I4a12*Sp*xPhi)/4. + (complex(0,1)*g1p*I4a21*Sp*xPhi)/4.',
                  order = {'QED':1})

GC_490 = Coupling(name = 'GC_490',
                  value = '-(Cp*cw*ee*complex(0,1)*I1a22)/(4.*sw) - (Cp*cw*ee*complex(0,1)*I2a22)/(4.*sw) - (Cp*ee*complex(0,1)*I1a22*sw)/(4.*cw) - (Cp*ee*complex(0,1)*I2a22*sw)/(4.*cw) + (complex(0,1)*g1p*I1a22*Sp*xH)/4. + (complex(0,1)*g1p*I2a22*Sp*xH)/4. + (complex(0,1)*g1p*I1a22*Sp*xPhi)/2. + (complex(0,1)*g1p*I2a22*Sp*xPhi)/2. - (complex(0,1)*g1p*I3a22*Sp*xPhi)/2. - (complex(0,1)*g1p*I4a22*Sp*xPhi)/2.',
                  order = {'QED':1})

GC_491 = Coupling(name = 'GC_491',
                  value = '(Cp*cw*ee*complex(0,1)*I1a22)/(4.*sw) + (Cp*cw*ee*complex(0,1)*I2a22)/(4.*sw) + (Cp*ee*complex(0,1)*I1a22*sw)/(4.*cw) + (Cp*ee*complex(0,1)*I2a22*sw)/(4.*cw) - (complex(0,1)*g1p*I1a22*Sp*xH)/4. - (complex(0,1)*g1p*I2a22*Sp*xH)/4. - (complex(0,1)*g1p*I1a22*Sp*xPhi)/2. - (complex(0,1)*g1p*I2a22*Sp*xPhi)/2. + (complex(0,1)*g1p*I3a22*Sp*xPhi)/2. + (complex(0,1)*g1p*I4a22*Sp*xPhi)/2.',
                  order = {'QED':1})

GC_492 = Coupling(name = 'GC_492',
                  value = '-(Cp*cw*ee*complex(0,1)*I1a13)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I1a31)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I2a13)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I2a31)/(8.*sw) - (Cp*ee*complex(0,1)*I1a13*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I1a31*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I2a13*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I2a31*sw)/(8.*cw) + (complex(0,1)*g1p*I1a13*Sp*xH)/8. + (complex(0,1)*g1p*I1a31*Sp*xH)/8. + (complex(0,1)*g1p*I2a13*Sp*xH)/8. + (complex(0,1)*g1p*I2a31*Sp*xH)/8. + (complex(0,1)*g1p*I1a13*Sp*xPhi)/4. + (complex(0,1)*g1p*I1a31*Sp*xPhi)/4. + (complex(0,1)*g1p*I2a13*Sp*xPhi)/4. + (complex(0,1)*g1p*I2a31*Sp*xPhi)/4. - (complex(0,1)*g1p*I3a13*Sp*xPhi)/4. - (complex(0,1)*g1p*I3a31*Sp*xPhi)/4. - (complex(0,1)*g1p*I4a13*Sp*xPhi)/4. - (complex(0,1)*g1p*I4a31*Sp*xPhi)/4.',
                  order = {'QED':1})

GC_493 = Coupling(name = 'GC_493',
                  value = '(Cp*cw*ee*complex(0,1)*I1a13)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I1a31)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I2a13)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I2a31)/(8.*sw) + (Cp*ee*complex(0,1)*I1a13*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I1a31*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I2a13*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I2a31*sw)/(8.*cw) - (complex(0,1)*g1p*I1a13*Sp*xH)/8. - (complex(0,1)*g1p*I1a31*Sp*xH)/8. - (complex(0,1)*g1p*I2a13*Sp*xH)/8. - (complex(0,1)*g1p*I2a31*Sp*xH)/8. - (complex(0,1)*g1p*I1a13*Sp*xPhi)/4. - (complex(0,1)*g1p*I1a31*Sp*xPhi)/4. - (complex(0,1)*g1p*I2a13*Sp*xPhi)/4. - (complex(0,1)*g1p*I2a31*Sp*xPhi)/4. + (complex(0,1)*g1p*I3a13*Sp*xPhi)/4. + (complex(0,1)*g1p*I3a31*Sp*xPhi)/4. + (complex(0,1)*g1p*I4a13*Sp*xPhi)/4. + (complex(0,1)*g1p*I4a31*Sp*xPhi)/4.',
                  order = {'QED':1})

GC_494 = Coupling(name = 'GC_494',
                  value = '-(Cp*cw*ee*complex(0,1)*I1a23)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I1a32)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I2a23)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I2a32)/(8.*sw) - (Cp*ee*complex(0,1)*I1a23*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I1a32*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I2a23*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I2a32*sw)/(8.*cw) + (complex(0,1)*g1p*I1a23*Sp*xH)/8. + (complex(0,1)*g1p*I1a32*Sp*xH)/8. + (complex(0,1)*g1p*I2a23*Sp*xH)/8. + (complex(0,1)*g1p*I2a32*Sp*xH)/8. + (complex(0,1)*g1p*I1a23*Sp*xPhi)/4. + (complex(0,1)*g1p*I1a32*Sp*xPhi)/4. + (complex(0,1)*g1p*I2a23*Sp*xPhi)/4. + (complex(0,1)*g1p*I2a32*Sp*xPhi)/4. - (complex(0,1)*g1p*I3a23*Sp*xPhi)/4. - (complex(0,1)*g1p*I3a32*Sp*xPhi)/4. - (complex(0,1)*g1p*I4a23*Sp*xPhi)/4. - (complex(0,1)*g1p*I4a32*Sp*xPhi)/4.',
                  order = {'QED':1})

GC_495 = Coupling(name = 'GC_495',
                  value = '(Cp*cw*ee*complex(0,1)*I1a23)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I1a32)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I2a23)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I2a32)/(8.*sw) + (Cp*ee*complex(0,1)*I1a23*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I1a32*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I2a23*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I2a32*sw)/(8.*cw) - (complex(0,1)*g1p*I1a23*Sp*xH)/8. - (complex(0,1)*g1p*I1a32*Sp*xH)/8. - (complex(0,1)*g1p*I2a23*Sp*xH)/8. - (complex(0,1)*g1p*I2a32*Sp*xH)/8. - (complex(0,1)*g1p*I1a23*Sp*xPhi)/4. - (complex(0,1)*g1p*I1a32*Sp*xPhi)/4. - (complex(0,1)*g1p*I2a23*Sp*xPhi)/4. - (complex(0,1)*g1p*I2a32*Sp*xPhi)/4. + (complex(0,1)*g1p*I3a23*Sp*xPhi)/4. + (complex(0,1)*g1p*I3a32*Sp*xPhi)/4. + (complex(0,1)*g1p*I4a23*Sp*xPhi)/4. + (complex(0,1)*g1p*I4a32*Sp*xPhi)/4.',
                  order = {'QED':1})

GC_496 = Coupling(name = 'GC_496',
                  value = '-(Cp*cw*ee*complex(0,1)*I1a33)/(4.*sw) - (Cp*cw*ee*complex(0,1)*I2a33)/(4.*sw) - (Cp*ee*complex(0,1)*I1a33*sw)/(4.*cw) - (Cp*ee*complex(0,1)*I2a33*sw)/(4.*cw) + (complex(0,1)*g1p*I1a33*Sp*xH)/4. + (complex(0,1)*g1p*I2a33*Sp*xH)/4. + (complex(0,1)*g1p*I1a33*Sp*xPhi)/2. + (complex(0,1)*g1p*I2a33*Sp*xPhi)/2. - (complex(0,1)*g1p*I3a33*Sp*xPhi)/2. - (complex(0,1)*g1p*I4a33*Sp*xPhi)/2.',
                  order = {'QED':1})

GC_497 = Coupling(name = 'GC_497',
                  value = '(Cp*cw*ee*complex(0,1)*I1a33)/(4.*sw) + (Cp*cw*ee*complex(0,1)*I2a33)/(4.*sw) + (Cp*ee*complex(0,1)*I1a33*sw)/(4.*cw) + (Cp*ee*complex(0,1)*I2a33*sw)/(4.*cw) - (complex(0,1)*g1p*I1a33*Sp*xH)/4. - (complex(0,1)*g1p*I2a33*Sp*xH)/4. - (complex(0,1)*g1p*I1a33*Sp*xPhi)/2. - (complex(0,1)*g1p*I2a33*Sp*xPhi)/2. + (complex(0,1)*g1p*I3a33*Sp*xPhi)/2. + (complex(0,1)*g1p*I4a33*Sp*xPhi)/2.',
                  order = {'QED':1})

GC_498 = Coupling(name = 'GC_498',
                  value = '(Cp*cw*ee*complex(0,1)*I6a11)/(2.*sw) + (Cp*ee*complex(0,1)*I6a11*sw)/(2.*cw) - (complex(0,1)*g1p*I6a11*Sp*xH)/2. + complex(0,1)*g1p*I5a11*Sp*xPhi - complex(0,1)*g1p*I6a11*Sp*xPhi',
                  order = {'QED':1})

GC_499 = Coupling(name = 'GC_499',
                  value = '(Cp*cw*ee*complex(0,1)*I6a12)/(2.*sw) + (Cp*ee*complex(0,1)*I6a12*sw)/(2.*cw) - (complex(0,1)*g1p*I6a12*Sp*xH)/2. + complex(0,1)*g1p*I5a12*Sp*xPhi - complex(0,1)*g1p*I6a12*Sp*xPhi',
                  order = {'QED':1})

GC_500 = Coupling(name = 'GC_500',
                  value = '(Cp*cw*ee*complex(0,1)*I6a13)/(2.*sw) + (Cp*ee*complex(0,1)*I6a13*sw)/(2.*cw) - (complex(0,1)*g1p*I6a13*Sp*xH)/2. + complex(0,1)*g1p*I5a13*Sp*xPhi - complex(0,1)*g1p*I6a13*Sp*xPhi',
                  order = {'QED':1})

GC_501 = Coupling(name = 'GC_501',
                  value = '(Cp*cw*ee*complex(0,1)*I6a21)/(2.*sw) + (Cp*ee*complex(0,1)*I6a21*sw)/(2.*cw) - (complex(0,1)*g1p*I6a21*Sp*xH)/2. + complex(0,1)*g1p*I5a21*Sp*xPhi - complex(0,1)*g1p*I6a21*Sp*xPhi',
                  order = {'QED':1})

GC_502 = Coupling(name = 'GC_502',
                  value = '(Cp*cw*ee*complex(0,1)*I6a22)/(2.*sw) + (Cp*ee*complex(0,1)*I6a22*sw)/(2.*cw) - (complex(0,1)*g1p*I6a22*Sp*xH)/2. + complex(0,1)*g1p*I5a22*Sp*xPhi - complex(0,1)*g1p*I6a22*Sp*xPhi',
                  order = {'QED':1})

GC_503 = Coupling(name = 'GC_503',
                  value = '(Cp*cw*ee*complex(0,1)*I6a23)/(2.*sw) + (Cp*ee*complex(0,1)*I6a23*sw)/(2.*cw) - (complex(0,1)*g1p*I6a23*Sp*xH)/2. + complex(0,1)*g1p*I5a23*Sp*xPhi - complex(0,1)*g1p*I6a23*Sp*xPhi',
                  order = {'QED':1})

GC_504 = Coupling(name = 'GC_504',
                  value = '(Cp*cw*ee*complex(0,1)*I6a31)/(2.*sw) + (Cp*ee*complex(0,1)*I6a31*sw)/(2.*cw) - (complex(0,1)*g1p*I6a31*Sp*xH)/2. + complex(0,1)*g1p*I5a31*Sp*xPhi - complex(0,1)*g1p*I6a31*Sp*xPhi',
                  order = {'QED':1})

GC_505 = Coupling(name = 'GC_505',
                  value = '(Cp*cw*ee*complex(0,1)*I6a32)/(2.*sw) + (Cp*ee*complex(0,1)*I6a32*sw)/(2.*cw) - (complex(0,1)*g1p*I6a32*Sp*xH)/2. + complex(0,1)*g1p*I5a32*Sp*xPhi - complex(0,1)*g1p*I6a32*Sp*xPhi',
                  order = {'QED':1})

GC_506 = Coupling(name = 'GC_506',
                  value = '(Cp*cw*ee*complex(0,1)*I6a33)/(2.*sw) + (Cp*ee*complex(0,1)*I6a33*sw)/(2.*cw) - (complex(0,1)*g1p*I6a33*Sp*xH)/2. + complex(0,1)*g1p*I5a33*Sp*xPhi - complex(0,1)*g1p*I6a33*Sp*xPhi',
                  order = {'QED':1})

GC_507 = Coupling(name = 'GC_507',
                  value = '-(Cp*cw*ee*complex(0,1)*I7a11)/(2.*sw) - (Cp*ee*complex(0,1)*I7a11*sw)/(2.*cw) + (complex(0,1)*g1p*I7a11*Sp*xH)/2. + complex(0,1)*g1p*I7a11*Sp*xPhi - complex(0,1)*g1p*I8a11*Sp*xPhi',
                  order = {'QED':1})

GC_508 = Coupling(name = 'GC_508',
                  value = '-(Cp*cw*ee*complex(0,1)*I7a12)/(2.*sw) - (Cp*ee*complex(0,1)*I7a12*sw)/(2.*cw) + (complex(0,1)*g1p*I7a12*Sp*xH)/2. + complex(0,1)*g1p*I7a12*Sp*xPhi - complex(0,1)*g1p*I8a12*Sp*xPhi',
                  order = {'QED':1})

GC_509 = Coupling(name = 'GC_509',
                  value = '-(Cp*cw*ee*complex(0,1)*I7a13)/(2.*sw) - (Cp*ee*complex(0,1)*I7a13*sw)/(2.*cw) + (complex(0,1)*g1p*I7a13*Sp*xH)/2. + complex(0,1)*g1p*I7a13*Sp*xPhi - complex(0,1)*g1p*I8a13*Sp*xPhi',
                  order = {'QED':1})

GC_510 = Coupling(name = 'GC_510',
                  value = '-(Cp*cw*ee*complex(0,1)*I7a21)/(2.*sw) - (Cp*ee*complex(0,1)*I7a21*sw)/(2.*cw) + (complex(0,1)*g1p*I7a21*Sp*xH)/2. + complex(0,1)*g1p*I7a21*Sp*xPhi - complex(0,1)*g1p*I8a21*Sp*xPhi',
                  order = {'QED':1})

GC_511 = Coupling(name = 'GC_511',
                  value = '-(Cp*cw*ee*complex(0,1)*I7a22)/(2.*sw) - (Cp*ee*complex(0,1)*I7a22*sw)/(2.*cw) + (complex(0,1)*g1p*I7a22*Sp*xH)/2. + complex(0,1)*g1p*I7a22*Sp*xPhi - complex(0,1)*g1p*I8a22*Sp*xPhi',
                  order = {'QED':1})

GC_512 = Coupling(name = 'GC_512',
                  value = '-(Cp*cw*ee*complex(0,1)*I7a23)/(2.*sw) - (Cp*ee*complex(0,1)*I7a23*sw)/(2.*cw) + (complex(0,1)*g1p*I7a23*Sp*xH)/2. + complex(0,1)*g1p*I7a23*Sp*xPhi - complex(0,1)*g1p*I8a23*Sp*xPhi',
                  order = {'QED':1})

GC_513 = Coupling(name = 'GC_513',
                  value = '-(Cp*cw*ee*complex(0,1)*I7a31)/(2.*sw) - (Cp*ee*complex(0,1)*I7a31*sw)/(2.*cw) + (complex(0,1)*g1p*I7a31*Sp*xH)/2. + complex(0,1)*g1p*I7a31*Sp*xPhi - complex(0,1)*g1p*I8a31*Sp*xPhi',
                  order = {'QED':1})

GC_514 = Coupling(name = 'GC_514',
                  value = '-(Cp*cw*ee*complex(0,1)*I7a32)/(2.*sw) - (Cp*ee*complex(0,1)*I7a32*sw)/(2.*cw) + (complex(0,1)*g1p*I7a32*Sp*xH)/2. + complex(0,1)*g1p*I7a32*Sp*xPhi - complex(0,1)*g1p*I8a32*Sp*xPhi',
                  order = {'QED':1})

GC_515 = Coupling(name = 'GC_515',
                  value = '-(Cp*cw*ee*complex(0,1)*I7a33)/(2.*sw) - (Cp*ee*complex(0,1)*I7a33*sw)/(2.*cw) + (complex(0,1)*g1p*I7a33*Sp*xH)/2. + complex(0,1)*g1p*I7a33*Sp*xPhi - complex(0,1)*g1p*I8a33*Sp*xPhi',
                  order = {'QED':1})

GC_516 = Coupling(name = 'GC_516',
                  value = '-(Cp*cw*ee*complex(0,1)*I11a11)/(4.*sw) - (Cp*cw*ee*complex(0,1)*I12a11)/(4.*sw) - (Cp*ee*complex(0,1)*I11a11*sw)/(4.*cw) - (Cp*ee*complex(0,1)*I12a11*sw)/(4.*cw) + (complex(0,1)*g1p*I11a11*Sp*xH)/4. + (complex(0,1)*g1p*I12a11*Sp*xH)/4. - (complex(0,1)*g1p*I10a11*Sp*xPhi)/2. + (complex(0,1)*g1p*I11a11*Sp*xPhi)/2. + (complex(0,1)*g1p*I12a11*Sp*xPhi)/2. - (complex(0,1)*g1p*I9a11*Sp*xPhi)/2.',
                  order = {'QED':1})

GC_517 = Coupling(name = 'GC_517',
                  value = '(Cp*cw*ee*complex(0,1)*I11a11)/(4.*sw) + (Cp*cw*ee*complex(0,1)*I12a11)/(4.*sw) + (Cp*ee*complex(0,1)*I11a11*sw)/(4.*cw) + (Cp*ee*complex(0,1)*I12a11*sw)/(4.*cw) - (complex(0,1)*g1p*I11a11*Sp*xH)/4. - (complex(0,1)*g1p*I12a11*Sp*xH)/4. + (complex(0,1)*g1p*I10a11*Sp*xPhi)/2. - (complex(0,1)*g1p*I11a11*Sp*xPhi)/2. - (complex(0,1)*g1p*I12a11*Sp*xPhi)/2. + (complex(0,1)*g1p*I9a11*Sp*xPhi)/2.',
                  order = {'QED':1})

GC_518 = Coupling(name = 'GC_518',
                  value = '-(Cp*cw*ee*complex(0,1)*I11a12)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I11a21)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I12a12)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I12a21)/(8.*sw) - (Cp*ee*complex(0,1)*I11a12*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I11a21*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I12a12*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I12a21*sw)/(8.*cw) + (complex(0,1)*g1p*I11a12*Sp*xH)/8. + (complex(0,1)*g1p*I11a21*Sp*xH)/8. + (complex(0,1)*g1p*I12a12*Sp*xH)/8. + (complex(0,1)*g1p*I12a21*Sp*xH)/8. - (complex(0,1)*g1p*I10a12*Sp*xPhi)/4. - (complex(0,1)*g1p*I10a21*Sp*xPhi)/4. + (complex(0,1)*g1p*I11a12*Sp*xPhi)/4. + (complex(0,1)*g1p*I11a21*Sp*xPhi)/4. + (complex(0,1)*g1p*I12a12*Sp*xPhi)/4. + (complex(0,1)*g1p*I12a21*Sp*xPhi)/4. - (complex(0,1)*g1p*I9a12*Sp*xPhi)/4. - (complex(0,1)*g1p*I9a21*Sp*xPhi)/4.',
                  order = {'QED':1})

GC_519 = Coupling(name = 'GC_519',
                  value = '(Cp*cw*ee*complex(0,1)*I11a12)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I11a21)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I12a12)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I12a21)/(8.*sw) + (Cp*ee*complex(0,1)*I11a12*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I11a21*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I12a12*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I12a21*sw)/(8.*cw) - (complex(0,1)*g1p*I11a12*Sp*xH)/8. - (complex(0,1)*g1p*I11a21*Sp*xH)/8. - (complex(0,1)*g1p*I12a12*Sp*xH)/8. - (complex(0,1)*g1p*I12a21*Sp*xH)/8. + (complex(0,1)*g1p*I10a12*Sp*xPhi)/4. + (complex(0,1)*g1p*I10a21*Sp*xPhi)/4. - (complex(0,1)*g1p*I11a12*Sp*xPhi)/4. - (complex(0,1)*g1p*I11a21*Sp*xPhi)/4. - (complex(0,1)*g1p*I12a12*Sp*xPhi)/4. - (complex(0,1)*g1p*I12a21*Sp*xPhi)/4. + (complex(0,1)*g1p*I9a12*Sp*xPhi)/4. + (complex(0,1)*g1p*I9a21*Sp*xPhi)/4.',
                  order = {'QED':1})

GC_520 = Coupling(name = 'GC_520',
                  value = '-(Cp*cw*ee*complex(0,1)*I11a22)/(4.*sw) - (Cp*cw*ee*complex(0,1)*I12a22)/(4.*sw) - (Cp*ee*complex(0,1)*I11a22*sw)/(4.*cw) - (Cp*ee*complex(0,1)*I12a22*sw)/(4.*cw) + (complex(0,1)*g1p*I11a22*Sp*xH)/4. + (complex(0,1)*g1p*I12a22*Sp*xH)/4. - (complex(0,1)*g1p*I10a22*Sp*xPhi)/2. + (complex(0,1)*g1p*I11a22*Sp*xPhi)/2. + (complex(0,1)*g1p*I12a22*Sp*xPhi)/2. - (complex(0,1)*g1p*I9a22*Sp*xPhi)/2.',
                  order = {'QED':1})

GC_521 = Coupling(name = 'GC_521',
                  value = '(Cp*cw*ee*complex(0,1)*I11a22)/(4.*sw) + (Cp*cw*ee*complex(0,1)*I12a22)/(4.*sw) + (Cp*ee*complex(0,1)*I11a22*sw)/(4.*cw) + (Cp*ee*complex(0,1)*I12a22*sw)/(4.*cw) - (complex(0,1)*g1p*I11a22*Sp*xH)/4. - (complex(0,1)*g1p*I12a22*Sp*xH)/4. + (complex(0,1)*g1p*I10a22*Sp*xPhi)/2. - (complex(0,1)*g1p*I11a22*Sp*xPhi)/2. - (complex(0,1)*g1p*I12a22*Sp*xPhi)/2. + (complex(0,1)*g1p*I9a22*Sp*xPhi)/2.',
                  order = {'QED':1})

GC_522 = Coupling(name = 'GC_522',
                  value = '-(Cp*cw*ee*complex(0,1)*I11a13)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I11a31)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I12a13)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I12a31)/(8.*sw) - (Cp*ee*complex(0,1)*I11a13*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I11a31*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I12a13*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I12a31*sw)/(8.*cw) + (complex(0,1)*g1p*I11a13*Sp*xH)/8. + (complex(0,1)*g1p*I11a31*Sp*xH)/8. + (complex(0,1)*g1p*I12a13*Sp*xH)/8. + (complex(0,1)*g1p*I12a31*Sp*xH)/8. - (complex(0,1)*g1p*I10a13*Sp*xPhi)/4. - (complex(0,1)*g1p*I10a31*Sp*xPhi)/4. + (complex(0,1)*g1p*I11a13*Sp*xPhi)/4. + (complex(0,1)*g1p*I11a31*Sp*xPhi)/4. + (complex(0,1)*g1p*I12a13*Sp*xPhi)/4. + (complex(0,1)*g1p*I12a31*Sp*xPhi)/4. - (complex(0,1)*g1p*I9a13*Sp*xPhi)/4. - (complex(0,1)*g1p*I9a31*Sp*xPhi)/4.',
                  order = {'QED':1})

GC_523 = Coupling(name = 'GC_523',
                  value = '(Cp*cw*ee*complex(0,1)*I11a13)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I11a31)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I12a13)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I12a31)/(8.*sw) + (Cp*ee*complex(0,1)*I11a13*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I11a31*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I12a13*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I12a31*sw)/(8.*cw) - (complex(0,1)*g1p*I11a13*Sp*xH)/8. - (complex(0,1)*g1p*I11a31*Sp*xH)/8. - (complex(0,1)*g1p*I12a13*Sp*xH)/8. - (complex(0,1)*g1p*I12a31*Sp*xH)/8. + (complex(0,1)*g1p*I10a13*Sp*xPhi)/4. + (complex(0,1)*g1p*I10a31*Sp*xPhi)/4. - (complex(0,1)*g1p*I11a13*Sp*xPhi)/4. - (complex(0,1)*g1p*I11a31*Sp*xPhi)/4. - (complex(0,1)*g1p*I12a13*Sp*xPhi)/4. - (complex(0,1)*g1p*I12a31*Sp*xPhi)/4. + (complex(0,1)*g1p*I9a13*Sp*xPhi)/4. + (complex(0,1)*g1p*I9a31*Sp*xPhi)/4.',
                  order = {'QED':1})

GC_524 = Coupling(name = 'GC_524',
                  value = '-(Cp*cw*ee*complex(0,1)*I11a23)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I11a32)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I12a23)/(8.*sw) - (Cp*cw*ee*complex(0,1)*I12a32)/(8.*sw) - (Cp*ee*complex(0,1)*I11a23*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I11a32*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I12a23*sw)/(8.*cw) - (Cp*ee*complex(0,1)*I12a32*sw)/(8.*cw) + (complex(0,1)*g1p*I11a23*Sp*xH)/8. + (complex(0,1)*g1p*I11a32*Sp*xH)/8. + (complex(0,1)*g1p*I12a23*Sp*xH)/8. + (complex(0,1)*g1p*I12a32*Sp*xH)/8. - (complex(0,1)*g1p*I10a23*Sp*xPhi)/4. - (complex(0,1)*g1p*I10a32*Sp*xPhi)/4. + (complex(0,1)*g1p*I11a23*Sp*xPhi)/4. + (complex(0,1)*g1p*I11a32*Sp*xPhi)/4. + (complex(0,1)*g1p*I12a23*Sp*xPhi)/4. + (complex(0,1)*g1p*I12a32*Sp*xPhi)/4. - (complex(0,1)*g1p*I9a23*Sp*xPhi)/4. - (complex(0,1)*g1p*I9a32*Sp*xPhi)/4.',
                  order = {'QED':1})

GC_525 = Coupling(name = 'GC_525',
                  value = '(Cp*cw*ee*complex(0,1)*I11a23)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I11a32)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I12a23)/(8.*sw) + (Cp*cw*ee*complex(0,1)*I12a32)/(8.*sw) + (Cp*ee*complex(0,1)*I11a23*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I11a32*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I12a23*sw)/(8.*cw) + (Cp*ee*complex(0,1)*I12a32*sw)/(8.*cw) - (complex(0,1)*g1p*I11a23*Sp*xH)/8. - (complex(0,1)*g1p*I11a32*Sp*xH)/8. - (complex(0,1)*g1p*I12a23*Sp*xH)/8. - (complex(0,1)*g1p*I12a32*Sp*xH)/8. + (complex(0,1)*g1p*I10a23*Sp*xPhi)/4. + (complex(0,1)*g1p*I10a32*Sp*xPhi)/4. - (complex(0,1)*g1p*I11a23*Sp*xPhi)/4. - (complex(0,1)*g1p*I11a32*Sp*xPhi)/4. - (complex(0,1)*g1p*I12a23*Sp*xPhi)/4. - (complex(0,1)*g1p*I12a32*Sp*xPhi)/4. + (complex(0,1)*g1p*I9a23*Sp*xPhi)/4. + (complex(0,1)*g1p*I9a32*Sp*xPhi)/4.',
                  order = {'QED':1})

GC_526 = Coupling(name = 'GC_526',
                  value = '-(Cp*cw*ee*complex(0,1)*I11a33)/(4.*sw) - (Cp*cw*ee*complex(0,1)*I12a33)/(4.*sw) - (Cp*ee*complex(0,1)*I11a33*sw)/(4.*cw) - (Cp*ee*complex(0,1)*I12a33*sw)/(4.*cw) + (complex(0,1)*g1p*I11a33*Sp*xH)/4. + (complex(0,1)*g1p*I12a33*Sp*xH)/4. - (complex(0,1)*g1p*I10a33*Sp*xPhi)/2. + (complex(0,1)*g1p*I11a33*Sp*xPhi)/2. + (complex(0,1)*g1p*I12a33*Sp*xPhi)/2. - (complex(0,1)*g1p*I9a33*Sp*xPhi)/2.',
                  order = {'QED':1})

GC_527 = Coupling(name = 'GC_527',
                  value = '(Cp*cw*ee*complex(0,1)*I11a33)/(4.*sw) + (Cp*cw*ee*complex(0,1)*I12a33)/(4.*sw) + (Cp*ee*complex(0,1)*I11a33*sw)/(4.*cw) + (Cp*ee*complex(0,1)*I12a33*sw)/(4.*cw) - (complex(0,1)*g1p*I11a33*Sp*xH)/4. - (complex(0,1)*g1p*I12a33*Sp*xH)/4. + (complex(0,1)*g1p*I10a33*Sp*xPhi)/2. - (complex(0,1)*g1p*I11a33*Sp*xPhi)/2. - (complex(0,1)*g1p*I12a33*Sp*xPhi)/2. + (complex(0,1)*g1p*I9a33*Sp*xPhi)/2.',
                  order = {'QED':1})

GC_528 = Coupling(name = 'GC_528',
                  value = '-(Ca*Cp*cw*ee*sg)/(2.*sw) - (Ca*Cp*ee*sg*sw)/(2.*cw) + (Ca*g1p*sg*Sp*xH)/2. + 2*cg*g1p*Sa*Sp*xPhi',
                  order = {'QED':1})

GC_529 = Coupling(name = 'GC_529',
                  value = '-(cg*Cp*cw*ee*Sa)/(2.*sw) - (cg*Cp*ee*Sa*sw)/(2.*cw) + (cg*g1p*Sa*Sp*xH)/2. + 2*Ca*g1p*sg*Sp*xPhi',
                  order = {'QED':1})

GC_530 = Coupling(name = 'GC_530',
                  value = '-(Ca*cg*Cp*cw*ee)/(2.*sw) - (Ca*cg*Cp*ee*sw)/(2.*cw) + (Ca*cg*g1p*Sp*xH)/2. - 2*g1p*Sa*sg*Sp*xPhi',
                  order = {'QED':1})

GC_531 = Coupling(name = 'GC_531',
                  value = 'ee**2*complex(0,1)*Sa**2*Sp**2 + (cw**2*ee**2*complex(0,1)*Sa**2*Sp**2)/(2.*sw**2) + (ee**2*complex(0,1)*Sa**2*Sp**2*sw**2)/(2.*cw**2) + (Cp*cw*ee*complex(0,1)*g1p*Sa**2*Sp*xH)/sw + (Cp*ee*complex(0,1)*g1p*Sa**2*Sp*sw*xH)/cw + (Cp**2*complex(0,1)*g1p**2*Sa**2*xH**2)/2. + 8*Ca**2*Cp**2*complex(0,1)*g1p**2*xPhi**2',
                  order = {'QED':2})

GC_532 = Coupling(name = 'GC_532',
                  value = 'ee**2*complex(0,1)*sg**2*Sp**2 + (cw**2*ee**2*complex(0,1)*sg**2*Sp**2)/(2.*sw**2) + (ee**2*complex(0,1)*sg**2*Sp**2*sw**2)/(2.*cw**2) + (Cp*cw*ee*complex(0,1)*g1p*sg**2*Sp*xH)/sw + (Cp*ee*complex(0,1)*g1p*sg**2*Sp*sw*xH)/cw + (Cp**2*complex(0,1)*g1p**2*sg**2*xH**2)/2. + 8*cg**2*Cp**2*complex(0,1)*g1p**2*xPhi**2',
                  order = {'QED':2})

GC_533 = Coupling(name = 'GC_533',
                  value = 'Ca*ee**2*complex(0,1)*Sa*Sp**2 + (Ca*cw**2*ee**2*complex(0,1)*Sa*Sp**2)/(2.*sw**2) + (Ca*ee**2*complex(0,1)*Sa*Sp**2*sw**2)/(2.*cw**2) + (Ca*Cp*cw*ee*complex(0,1)*g1p*Sa*Sp*xH)/sw + (Ca*Cp*ee*complex(0,1)*g1p*Sa*Sp*sw*xH)/cw + (Ca*Cp**2*complex(0,1)*g1p**2*Sa*xH**2)/2. - 8*Ca*Cp**2*complex(0,1)*g1p**2*Sa*xPhi**2',
                  order = {'QED':2})

GC_534 = Coupling(name = 'GC_534',
                  value = 'Ca**2*ee**2*complex(0,1)*Sp**2 + (Ca**2*cw**2*ee**2*complex(0,1)*Sp**2)/(2.*sw**2) + (Ca**2*ee**2*complex(0,1)*Sp**2*sw**2)/(2.*cw**2) + (Ca**2*Cp*cw*ee*complex(0,1)*g1p*Sp*xH)/sw + (Ca**2*Cp*ee*complex(0,1)*g1p*Sp*sw*xH)/cw + (Ca**2*Cp**2*complex(0,1)*g1p**2*xH**2)/2. + 8*Cp**2*complex(0,1)*g1p**2*Sa**2*xPhi**2',
                  order = {'QED':2})

GC_535 = Coupling(name = 'GC_535',
                  value = '-(cg*ee**2*complex(0,1)*sg*Sp**2) - (cg*cw**2*ee**2*complex(0,1)*sg*Sp**2)/(2.*sw**2) - (cg*ee**2*complex(0,1)*sg*Sp**2*sw**2)/(2.*cw**2) - (cg*Cp*cw*ee*complex(0,1)*g1p*sg*Sp*xH)/sw - (cg*Cp*ee*complex(0,1)*g1p*sg*Sp*sw*xH)/cw - (cg*Cp**2*complex(0,1)*g1p**2*sg*xH**2)/2. + 8*cg*Cp**2*complex(0,1)*g1p**2*sg*xPhi**2',
                  order = {'QED':2})

GC_536 = Coupling(name = 'GC_536',
                  value = 'cg**2*ee**2*complex(0,1)*Sp**2 + (cg**2*cw**2*ee**2*complex(0,1)*Sp**2)/(2.*sw**2) + (cg**2*ee**2*complex(0,1)*Sp**2*sw**2)/(2.*cw**2) + (cg**2*Cp*cw*ee*complex(0,1)*g1p*Sp*xH)/sw + (cg**2*Cp*ee*complex(0,1)*g1p*Sp*sw*xH)/cw + (cg**2*Cp**2*complex(0,1)*g1p**2*xH**2)/2. + 8*Cp**2*complex(0,1)*g1p**2*sg**2*xPhi**2',
                  order = {'QED':2})

GC_537 = Coupling(name = 'GC_537',
                  value = '-(Cp*ee**2*complex(0,1)*Sa**2*Sp) - (Cp*cw**2*ee**2*complex(0,1)*Sa**2*Sp)/(2.*sw**2) - (Cp*ee**2*complex(0,1)*Sa**2*Sp*sw**2)/(2.*cw**2) - (Cp**2*cw*ee*complex(0,1)*g1p*Sa**2*xH)/(2.*sw) + (cw*ee*complex(0,1)*g1p*Sa**2*Sp**2*xH)/(2.*sw) - (Cp**2*ee*complex(0,1)*g1p*Sa**2*sw*xH)/(2.*cw) + (ee*complex(0,1)*g1p*Sa**2*Sp**2*sw*xH)/(2.*cw) + (Cp*complex(0,1)*g1p**2*Sa**2*Sp*xH**2)/2. + 8*Ca**2*Cp*complex(0,1)*g1p**2*Sp*xPhi**2',
                  order = {'QED':2})

GC_538 = Coupling(name = 'GC_538',
                  value = '-(Cp*ee**2*complex(0,1)*sg**2*Sp) - (Cp*cw**2*ee**2*complex(0,1)*sg**2*Sp)/(2.*sw**2) - (Cp*ee**2*complex(0,1)*sg**2*Sp*sw**2)/(2.*cw**2) - (Cp**2*cw*ee*complex(0,1)*g1p*sg**2*xH)/(2.*sw) + (cw*ee*complex(0,1)*g1p*sg**2*Sp**2*xH)/(2.*sw) - (Cp**2*ee*complex(0,1)*g1p*sg**2*sw*xH)/(2.*cw) + (ee*complex(0,1)*g1p*sg**2*Sp**2*sw*xH)/(2.*cw) + (Cp*complex(0,1)*g1p**2*sg**2*Sp*xH**2)/2. + 8*cg**2*Cp*complex(0,1)*g1p**2*Sp*xPhi**2',
                  order = {'QED':2})

GC_539 = Coupling(name = 'GC_539',
                  value = '-(Ca*Cp*ee**2*complex(0,1)*Sa*Sp) - (Ca*Cp*cw**2*ee**2*complex(0,1)*Sa*Sp)/(2.*sw**2) - (Ca*Cp*ee**2*complex(0,1)*Sa*Sp*sw**2)/(2.*cw**2) - (Ca*Cp**2*cw*ee*complex(0,1)*g1p*Sa*xH)/(2.*sw) + (Ca*cw*ee*complex(0,1)*g1p*Sa*Sp**2*xH)/(2.*sw) - (Ca*Cp**2*ee*complex(0,1)*g1p*Sa*sw*xH)/(2.*cw) + (Ca*ee*complex(0,1)*g1p*Sa*Sp**2*sw*xH)/(2.*cw) + (Ca*Cp*complex(0,1)*g1p**2*Sa*Sp*xH**2)/2. - 8*Ca*Cp*complex(0,1)*g1p**2*Sa*Sp*xPhi**2',
                  order = {'QED':2})

GC_540 = Coupling(name = 'GC_540',
                  value = '-(Ca**2*Cp*ee**2*complex(0,1)*Sp) - (Ca**2*Cp*cw**2*ee**2*complex(0,1)*Sp)/(2.*sw**2) - (Ca**2*Cp*ee**2*complex(0,1)*Sp*sw**2)/(2.*cw**2) - (Ca**2*Cp**2*cw*ee*complex(0,1)*g1p*xH)/(2.*sw) + (Ca**2*cw*ee*complex(0,1)*g1p*Sp**2*xH)/(2.*sw) - (Ca**2*Cp**2*ee*complex(0,1)*g1p*sw*xH)/(2.*cw) + (Ca**2*ee*complex(0,1)*g1p*Sp**2*sw*xH)/(2.*cw) + (Ca**2*Cp*complex(0,1)*g1p**2*Sp*xH**2)/2. + 8*Cp*complex(0,1)*g1p**2*Sa**2*Sp*xPhi**2',
                  order = {'QED':2})

GC_541 = Coupling(name = 'GC_541',
                  value = 'cg*Cp*ee**2*complex(0,1)*sg*Sp + (cg*Cp*cw**2*ee**2*complex(0,1)*sg*Sp)/(2.*sw**2) + (cg*Cp*ee**2*complex(0,1)*sg*Sp*sw**2)/(2.*cw**2) + (cg*Cp**2*cw*ee*complex(0,1)*g1p*sg*xH)/(2.*sw) - (cg*cw*ee*complex(0,1)*g1p*sg*Sp**2*xH)/(2.*sw) + (cg*Cp**2*ee*complex(0,1)*g1p*sg*sw*xH)/(2.*cw) - (cg*ee*complex(0,1)*g1p*sg*Sp**2*sw*xH)/(2.*cw) - (cg*Cp*complex(0,1)*g1p**2*sg*Sp*xH**2)/2. + 8*cg*Cp*complex(0,1)*g1p**2*sg*Sp*xPhi**2',
                  order = {'QED':2})

GC_542 = Coupling(name = 'GC_542',
                  value = '-(cg**2*Cp*ee**2*complex(0,1)*Sp) - (cg**2*Cp*cw**2*ee**2*complex(0,1)*Sp)/(2.*sw**2) - (cg**2*Cp*ee**2*complex(0,1)*Sp*sw**2)/(2.*cw**2) - (cg**2*Cp**2*cw*ee*complex(0,1)*g1p*xH)/(2.*sw) + (cg**2*cw*ee*complex(0,1)*g1p*Sp**2*xH)/(2.*sw) - (cg**2*Cp**2*ee*complex(0,1)*g1p*sw*xH)/(2.*cw) + (cg**2*ee*complex(0,1)*g1p*Sp**2*sw*xH)/(2.*cw) + (cg**2*Cp*complex(0,1)*g1p**2*Sp*xH**2)/2. + 8*Cp*complex(0,1)*g1p**2*sg**2*Sp*xPhi**2',
                  order = {'QED':2})

GC_543 = Coupling(name = 'GC_543',
                  value = 'Cp**2*ee**2*complex(0,1)*Sa**2 + (Cp**2*cw**2*ee**2*complex(0,1)*Sa**2)/(2.*sw**2) + (Cp**2*ee**2*complex(0,1)*Sa**2*sw**2)/(2.*cw**2) - (Cp*cw*ee*complex(0,1)*g1p*Sa**2*Sp*xH)/sw - (Cp*ee*complex(0,1)*g1p*Sa**2*Sp*sw*xH)/cw + (complex(0,1)*g1p**2*Sa**2*Sp**2*xH**2)/2. + 8*Ca**2*complex(0,1)*g1p**2*Sp**2*xPhi**2',
                  order = {'QED':2})

GC_544 = Coupling(name = 'GC_544',
                  value = 'Cp**2*ee**2*complex(0,1)*sg**2 + (Cp**2*cw**2*ee**2*complex(0,1)*sg**2)/(2.*sw**2) + (Cp**2*ee**2*complex(0,1)*sg**2*sw**2)/(2.*cw**2) - (Cp*cw*ee*complex(0,1)*g1p*sg**2*Sp*xH)/sw - (Cp*ee*complex(0,1)*g1p*sg**2*Sp*sw*xH)/cw + (complex(0,1)*g1p**2*sg**2*Sp**2*xH**2)/2. + 8*cg**2*complex(0,1)*g1p**2*Sp**2*xPhi**2',
                  order = {'QED':2})

GC_545 = Coupling(name = 'GC_545',
                  value = 'Ca*Cp**2*ee**2*complex(0,1)*Sa + (Ca*Cp**2*cw**2*ee**2*complex(0,1)*Sa)/(2.*sw**2) + (Ca*Cp**2*ee**2*complex(0,1)*Sa*sw**2)/(2.*cw**2) - (Ca*Cp*cw*ee*complex(0,1)*g1p*Sa*Sp*xH)/sw - (Ca*Cp*ee*complex(0,1)*g1p*Sa*Sp*sw*xH)/cw + (Ca*complex(0,1)*g1p**2*Sa*Sp**2*xH**2)/2. - 8*Ca*complex(0,1)*g1p**2*Sa*Sp**2*xPhi**2',
                  order = {'QED':2})

GC_546 = Coupling(name = 'GC_546',
                  value = 'Ca**2*Cp**2*ee**2*complex(0,1) + (Ca**2*Cp**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (Ca**2*Cp**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) - (Ca**2*Cp*cw*ee*complex(0,1)*g1p*Sp*xH)/sw - (Ca**2*Cp*ee*complex(0,1)*g1p*Sp*sw*xH)/cw + (Ca**2*complex(0,1)*g1p**2*Sp**2*xH**2)/2. + 8*complex(0,1)*g1p**2*Sa**2*Sp**2*xPhi**2',
                  order = {'QED':2})

GC_547 = Coupling(name = 'GC_547',
                  value = '-(cg*Cp**2*ee**2*complex(0,1)*sg) - (cg*Cp**2*cw**2*ee**2*complex(0,1)*sg)/(2.*sw**2) - (cg*Cp**2*ee**2*complex(0,1)*sg*sw**2)/(2.*cw**2) + (cg*Cp*cw*ee*complex(0,1)*g1p*sg*Sp*xH)/sw + (cg*Cp*ee*complex(0,1)*g1p*sg*Sp*sw*xH)/cw - (cg*complex(0,1)*g1p**2*sg*Sp**2*xH**2)/2. + 8*cg*complex(0,1)*g1p**2*sg*Sp**2*xPhi**2',
                  order = {'QED':2})

GC_548 = Coupling(name = 'GC_548',
                  value = 'cg**2*Cp**2*ee**2*complex(0,1) + (cg**2*Cp**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (cg**2*Cp**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) - (cg**2*Cp*cw*ee*complex(0,1)*g1p*Sp*xH)/sw - (cg**2*Cp*ee*complex(0,1)*g1p*Sp*sw*xH)/cw + (cg**2*complex(0,1)*g1p**2*Sp**2*xH**2)/2. + 8*complex(0,1)*g1p**2*sg**2*Sp**2*xPhi**2',
                  order = {'QED':2})

GC_549 = Coupling(name = 'GC_549',
                  value = 'ee**2*complex(0,1)*Sa*Sp**2*vev + (cw**2*ee**2*complex(0,1)*Sa*Sp**2*vev)/(2.*sw**2) + (ee**2*complex(0,1)*Sa*Sp**2*sw**2*vev)/(2.*cw**2) + (Cp*cw*ee*complex(0,1)*g1p*Sa*Sp*vev*xH)/sw + (Cp*ee*complex(0,1)*g1p*Sa*Sp*sw*vev*xH)/cw + (Cp**2*complex(0,1)*g1p**2*Sa*vev*xH**2)/2. + 8*Ca*Cp**2*complex(0,1)*g1p**2*x*xPhi**2',
                  order = {'QED':1})

GC_550 = Coupling(name = 'GC_550',
                  value = 'Ca*ee**2*complex(0,1)*Sp**2*vev + (Ca*cw**2*ee**2*complex(0,1)*Sp**2*vev)/(2.*sw**2) + (Ca*ee**2*complex(0,1)*Sp**2*sw**2*vev)/(2.*cw**2) + (Ca*Cp*cw*ee*complex(0,1)*g1p*Sp*vev*xH)/sw + (Ca*Cp*ee*complex(0,1)*g1p*Sp*sw*vev*xH)/cw + (Ca*Cp**2*complex(0,1)*g1p**2*vev*xH**2)/2. - 8*Cp**2*complex(0,1)*g1p**2*Sa*x*xPhi**2',
                  order = {'QED':1})

GC_551 = Coupling(name = 'GC_551',
                  value = '-(Cp*ee**2*complex(0,1)*Sa*Sp*vev) - (Cp*cw**2*ee**2*complex(0,1)*Sa*Sp*vev)/(2.*sw**2) - (Cp*ee**2*complex(0,1)*Sa*Sp*sw**2*vev)/(2.*cw**2) - (Cp**2*cw*ee*complex(0,1)*g1p*Sa*vev*xH)/(2.*sw) + (cw*ee*complex(0,1)*g1p*Sa*Sp**2*vev*xH)/(2.*sw) - (Cp**2*ee*complex(0,1)*g1p*Sa*sw*vev*xH)/(2.*cw) + (ee*complex(0,1)*g1p*Sa*Sp**2*sw*vev*xH)/(2.*cw) + (Cp*complex(0,1)*g1p**2*Sa*Sp*vev*xH**2)/2. + 8*Ca*Cp*complex(0,1)*g1p**2*Sp*x*xPhi**2',
                  order = {'QED':1})

GC_552 = Coupling(name = 'GC_552',
                  value = '-(Ca*Cp*ee**2*complex(0,1)*Sp*vev) - (Ca*Cp*cw**2*ee**2*complex(0,1)*Sp*vev)/(2.*sw**2) - (Ca*Cp*ee**2*complex(0,1)*Sp*sw**2*vev)/(2.*cw**2) - (Ca*Cp**2*cw*ee*complex(0,1)*g1p*vev*xH)/(2.*sw) + (Ca*cw*ee*complex(0,1)*g1p*Sp**2*vev*xH)/(2.*sw) - (Ca*Cp**2*ee*complex(0,1)*g1p*sw*vev*xH)/(2.*cw) + (Ca*ee*complex(0,1)*g1p*Sp**2*sw*vev*xH)/(2.*cw) + (Ca*Cp*complex(0,1)*g1p**2*Sp*vev*xH**2)/2. - 8*Cp*complex(0,1)*g1p**2*Sa*Sp*x*xPhi**2',
                  order = {'QED':1})

GC_553 = Coupling(name = 'GC_553',
                  value = 'Cp**2*ee**2*complex(0,1)*Sa*vev + (Cp**2*cw**2*ee**2*complex(0,1)*Sa*vev)/(2.*sw**2) + (Cp**2*ee**2*complex(0,1)*Sa*sw**2*vev)/(2.*cw**2) - (Cp*cw*ee*complex(0,1)*g1p*Sa*Sp*vev*xH)/sw - (Cp*ee*complex(0,1)*g1p*Sa*Sp*sw*vev*xH)/cw + (complex(0,1)*g1p**2*Sa*Sp**2*vev*xH**2)/2. + 8*Ca*complex(0,1)*g1p**2*Sp**2*x*xPhi**2',
                  order = {'QED':1})

GC_554 = Coupling(name = 'GC_554',
                  value = 'Ca*Cp**2*ee**2*complex(0,1)*vev + (Ca*Cp**2*cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (Ca*Cp**2*ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2) - (Ca*Cp*cw*ee*complex(0,1)*g1p*Sp*vev*xH)/sw - (Ca*Cp*ee*complex(0,1)*g1p*Sp*sw*vev*xH)/cw + (Ca*complex(0,1)*g1p**2*Sp**2*vev*xH**2)/2. - 8*complex(0,1)*g1p**2*Sa*Sp**2*x*xPhi**2',
                  order = {'QED':1})

GC_555 = Coupling(name = 'GC_555',
                  value = '-((Ca*complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_556 = Coupling(name = 'GC_556',
                  value = '-((cg*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_557 = Coupling(name = 'GC_557',
                  value = '(cg*yb)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_558 = Coupling(name = 'GC_558',
                  value = '-((complex(0,1)*Sa*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_559 = Coupling(name = 'GC_559',
                  value = '-((sg*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_560 = Coupling(name = 'GC_560',
                  value = '(sg*yb)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_561 = Coupling(name = 'GC_561',
                  value = '-((Ca*complex(0,1)*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_562 = Coupling(name = 'GC_562',
                  value = '-((cg*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_563 = Coupling(name = 'GC_563',
                  value = '(cg*yc)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_564 = Coupling(name = 'GC_564',
                  value = '-((complex(0,1)*Sa*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_565 = Coupling(name = 'GC_565',
                  value = '-((sg*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_566 = Coupling(name = 'GC_566',
                  value = '(sg*yc)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_567 = Coupling(name = 'GC_567',
                  value = '-((Ca*complex(0,1)*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_568 = Coupling(name = 'GC_568',
                  value = '-((cg*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_569 = Coupling(name = 'GC_569',
                  value = '(cg*ydo)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_570 = Coupling(name = 'GC_570',
                  value = '-((complex(0,1)*Sa*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_571 = Coupling(name = 'GC_571',
                  value = '-((sg*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_572 = Coupling(name = 'GC_572',
                  value = '(sg*ydo)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_573 = Coupling(name = 'GC_573',
                  value = '-((Ca*complex(0,1)*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_574 = Coupling(name = 'GC_574',
                  value = '-((cg*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_575 = Coupling(name = 'GC_575',
                  value = '(cg*ye)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_576 = Coupling(name = 'GC_576',
                  value = '-((complex(0,1)*Sa*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_577 = Coupling(name = 'GC_577',
                  value = '-((sg*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_578 = Coupling(name = 'GC_578',
                  value = '(sg*ye)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_579 = Coupling(name = 'GC_579',
                  value = '-((Ca*complex(0,1)*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_580 = Coupling(name = 'GC_580',
                  value = '-((cg*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_581 = Coupling(name = 'GC_581',
                  value = '(cg*ym)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_582 = Coupling(name = 'GC_582',
                  value = '-((complex(0,1)*Sa*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_583 = Coupling(name = 'GC_583',
                  value = '-((sg*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_584 = Coupling(name = 'GC_584',
                  value = '(sg*ym)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_585 = Coupling(name = 'GC_585',
                  value = '-((Ca*complex(0,1)*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_586 = Coupling(name = 'GC_586',
                  value = '-((cg*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_587 = Coupling(name = 'GC_587',
                  value = '(cg*ys)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_588 = Coupling(name = 'GC_588',
                  value = '-((complex(0,1)*Sa*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_589 = Coupling(name = 'GC_589',
                  value = '-((sg*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_590 = Coupling(name = 'GC_590',
                  value = '(sg*ys)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_591 = Coupling(name = 'GC_591',
                  value = '-((Ca*complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_592 = Coupling(name = 'GC_592',
                  value = '-((cg*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_593 = Coupling(name = 'GC_593',
                  value = '(cg*yt)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_594 = Coupling(name = 'GC_594',
                  value = '-((complex(0,1)*Sa*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_595 = Coupling(name = 'GC_595',
                  value = '-((sg*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_596 = Coupling(name = 'GC_596',
                  value = '(sg*yt)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_597 = Coupling(name = 'GC_597',
                  value = '-((Ca*complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_598 = Coupling(name = 'GC_598',
                  value = '-((cg*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_599 = Coupling(name = 'GC_599',
                  value = '(cg*ytau)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_600 = Coupling(name = 'GC_600',
                  value = '-((complex(0,1)*Sa*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_601 = Coupling(name = 'GC_601',
                  value = '-((sg*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_602 = Coupling(name = 'GC_602',
                  value = '(sg*ytau)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_603 = Coupling(name = 'GC_603',
                  value = '-((Ca*complex(0,1)*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_604 = Coupling(name = 'GC_604',
                  value = '-((cg*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_605 = Coupling(name = 'GC_605',
                  value = '(cg*yup)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_606 = Coupling(name = 'GC_606',
                  value = '-((complex(0,1)*Sa*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_607 = Coupling(name = 'GC_607',
                  value = '-((sg*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_608 = Coupling(name = 'GC_608',
                  value = '(sg*yup)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_609 = Coupling(name = 'GC_609',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_610 = Coupling(name = 'GC_610',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_611 = Coupling(name = 'GC_611',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_612 = Coupling(name = 'GC_612',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_613 = Coupling(name = 'GC_613',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_614 = Coupling(name = 'GC_614',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_615 = Coupling(name = 'GC_615',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_616 = Coupling(name = 'GC_616',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_617 = Coupling(name = 'GC_617',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_618 = Coupling(name = 'GC_618',
                  value = '(ee*complex(0,1)*complexconjugate(U11))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_619 = Coupling(name = 'GC_619',
                  value = '(ee*complex(0,1)*complexconjugate(U12))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_620 = Coupling(name = 'GC_620',
                  value = '(ee*complex(0,1)*complexconjugate(U13))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_621 = Coupling(name = 'GC_621',
                  value = '(ee*complex(0,1)*complexconjugate(U21))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_622 = Coupling(name = 'GC_622',
                  value = '(ee*complex(0,1)*complexconjugate(U22))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_623 = Coupling(name = 'GC_623',
                  value = '(ee*complex(0,1)*complexconjugate(U23))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_624 = Coupling(name = 'GC_624',
                  value = '(ee*complex(0,1)*complexconjugate(U31))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_625 = Coupling(name = 'GC_625',
                  value = '(ee*complex(0,1)*complexconjugate(U32))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_626 = Coupling(name = 'GC_626',
                  value = '(ee*complex(0,1)*complexconjugate(U33))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

