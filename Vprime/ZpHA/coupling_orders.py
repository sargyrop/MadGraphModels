# This file was automatically created by FeynRules 2.3.24
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (November 20, 2012)
# Date: Tue 8 Nov 2016 12:46:45


from object_library import all_orders, CouplingOrder


DMV = CouplingOrder(name = 'DMV',
                    expansion_order = 2,
                    hierarchy = 2)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

